\piecePartSpecs
#`((dessus #:score "score-dessus")
   (haute-contre)
   (haute-contre-sol)
   (taille)
   (quinte)
   (basse #:tag-notes part
          #:score-template "score"))

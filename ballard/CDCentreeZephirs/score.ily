\score {
  \new StaffGroup <<
    \new Staff << \global \keepWithTag #'dessus \includeNotes "dessus" >>
    \new Staff << \global \includeNotes "haute-contre" >>
    \new Staff << \global \includeNotes "taille" >>
    \new Staff << \global \includeNotes "quinte" >>
    \new Staff << \global \keepWithTag #'() \includeNotes "basse" >>
  >>
  \layout { }
  \midi { }
}

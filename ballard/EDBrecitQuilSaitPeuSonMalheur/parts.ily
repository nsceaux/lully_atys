\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix"
          #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Atys
    \livretVerse#12 { Qu’il sait peu son malheur ! et qu’il est déplorable ! }
    \livretVerse#12 { Son amour méritait un sort plus favorable : }
    \livretVerse#12 { J’ai pitié de l’erreur dont son cœur s’est flatté. }
    \livretPers Sangaride
    \livretVerse#12 { Épargnez-vous le soin d’être si pitoyable, }
    \livretVerse#12 { Son amour obtiendra ce qu’il a merité. }
    \livretPers Atys
    \livretVerse#12 { Dieux ! qu’est-ce que j’entends ! }
    \livretPers Sangaride
    \livretVerse#12 { \transparent { Dieux ! qu’est-ce que j’entends ! } Qu’il faut que je me venge, }
    \livretVerse#12 { Que j’aime enfin le roi, qu’il sera mon époux. }
    \livretPers Atys
    \livretVerse#12 { Sangaride, eh d’où vient ce changement étrange ? }
    \livretPers Sangaride
    \livretVerse#12 { N’est-ce pas vous ingrat qui voulez que je change ? }
    \livretPers Atys
    \livretVerse#12 { Moi ! }
    \livretPers Sangaride
    \livretVerse#12 { \transparent { Moi ! } Quelle trahison ! }
    \livretPers Atys
    \livretVerse#12 { \transparent { Moi ! Quelle trahison ! } Quel funeste couroux ! }
    \livretPers Atys et Sangaride
    \livretVerse#12 { Pourquoi m’abandonner pour une amour nouvelle ? }
    \livretVerse#12 { Ce n’est pas moi qui rompt une chaîne si belle. }
    \livretPers Atys
    \livretVerse#8 { Beauté trop cruelle, c’est vous, }
    \livretPers Sangaride
    \livretVerse#8 { Amant infidèle, c’est vous, }
    \livretPers Atys
    \livretVerse#8 { Ah ! c’est vous, beauté trop cruelle, }
    \livretPers Sangaride
    \livretVerse#8 { Ah ! c’est vous amant infidèle. }
    \livretPers Atys et Sangaride
    \livretVerse#8 { Beauté trop cruelle, c’est vous, }
    \livretVerse#8 { Amant infidèle, c’est vous, }
    \livretVerse#8 { Qui rompez des liens si doux. }
    \livretPers Sangaride
    \livretVerse#12 { Vous m’avez immolée à l’amour de Cybèle. }
  }
  \column {
    \livretPers Atys
    \livretVerse#12 { Il est vrai qu’à ses yeux, par un secret effroi, }
    \livretVerse#12 { J’ai voulu de nos cœurs cacher l’intelligence : }
    \livretVerse#12 { Mais ce n’est que pour vous que j’ai crains sa vengeance, }
    \livretVerse#8 { Et je ne la crains pas pour moi. }
    \livretVerse#12 { Cybèle m’aime en vain, et c’est vous que j’adore. }
    \livretPers Sangaride
    \livretVerse#8 { Après votre infidélité, }
    \livretVerse#8 { Auriez-vous bien la cruauté }
    \livretVerse#8 { De vouloir me tromper encore ? }
    \livretPers Atys
    \livretVerse#8 { Moi ! vous trahir ? vous le pensez ? }
    \livretVerse#8 { Ingrate, que vous m’offensez ! }
    \livretVerse#8 { Eh bien, il ne faut plus rien taire, }
    \livretVerse#12 { Je vais de la déesse attirer la colère, }
    \livretVerse#12 { M’offrir à sa fureur, puisque vous m’y forcez… }
    \livretPers Sangaride
    \livretVerse#12 { Ah ! demeurez, Atys, mes soupçons sont passés ; }
    \livretVerse#12 { Vous m’aimez, je le crois, j’en veux être certaine. }
    \livretVerse#6 { Je le souhaite assez, }
    \livretVerse#6 { Pour le croire sans peine. }
    \livretPers Atys
    \livretVerse#6 { Je jure, }
    \livretPers Sangaride
    \livretVerse#6 { \transparent { Je jure, } Je promets, }
    \livretPers Atys et Sangaride
    \livretVerse#6 { De ne changer jamais. }
    \livretPers Sangaride
    \livretVerse#12 { Quel tourment de cacher une si belle flamme. }
    \livretPers Atys
    \livretVerse#12 { Redoublons-en l’ardeur dans le fonds de notre âme. }
    \livretPers Atys et Sangaride
    \livretVerse#8 { Aimons en secret, aimons-nous : }
    \livretVerse#12 { Aimons plus que jamais, en dépit des jaloux. }
    \livretPers Sangaride
    \livretVerse#12 { Mon père vient ici, }
    \livretPers Atys
    \livretVerse#12 { \transparent { Mon père vient ici, } Que rien ne vous étonne ; }
    \livretVerse#12 { Servons-nous du pouvoir que Cybèle me donne, }
    \livretVerse#8 { Je vais préparer les Zéphirs }
    \livretVerse#6 { À suivre nos désirs. }
  }
}#}))

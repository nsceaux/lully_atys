%% ACTE 4 SCENE 4
% Atys
\tag #'(atys basse) {
  Qu’il sait peu son mal -- heur ! et qu’il est dé -- plo -- ra -- ble !
  Son a -- mour mé -- ri -- tait un sort plus fa -- vo -- ra -- ble :
  j’ai pi -- tié de l’er -- reur dont son cœur s’est flat -- té.
}
% Sangaride
\tag #'(sangaride basse) {
  É -- par -- gnez- vous le soin d’ê -- tre si pi -- toy -- a -- ble,
  son a -- mour ob -- tien -- dra ce qu’il a me -- ri -- té.
}
% Atys
\tag #'(atys basse) {
  Dieux ! qu’est- ce que j’en -- tends !
}
% Sangaride
\tag #'(sangaride basse) {
  Qu’il faut que je me ven -- ge,
  que j’aime en -- fin le roi, qu’il se -- ra mon é -- poux.
}
% Atys
\tag #'(atys basse) {
  San -- ga -- ride, eh d’où vient ce chan -- ge -- ment é -- tran -- ge ?
}
% Sangaride
\tag #'(sangaride basse) {
  N’est- ce pas vous in -- grat qui vou -- lez que je chan -- ge ?
}
% Atys
\tag #'(atys basse) {
  Moi !
}
% Sangaride
\tag #'(sangaride basse) {
  Quel -- le tra -- hi -- son !
}
% Atys
\tag #'(atys basse) {
  Quel fu -- nes -- te cou -- roux !
}
% Atys et Sangaride.
\tag #'(sangaride basse) {
  Pour -- quoi m’a -- ban -- don -- ner pour une a -- mour nou -- vel -- le ?
  Ce n’est pas moi qui rompt u -- ne chaî -- ne si bel -- le.
  Ce n’est pas moi,
  Ce n’est pas moi qui rompt u -- ne chaî -- ne si bel -- le.
}
\tag #'(atys) {
  Pour -- quoi m’a -- ban -- don -- ner pour une a -- mour nou -- vel -- le ?
  Ce n’est pas moi qui rompt u -- ne chaî -- ne si bel -- le.
  Ce n’est pas moi qui rompt u -- ne chaî -- ne si bel -- le.
}
% Atys
\tag #'(atys basse) {
  Beau -- té trop cru -- el -- le, c’est vous,
}
% Sangaride
\tag #'(sangaride basse) {
  A -- mant in -- fi -- dè -- le, c’est vous,
}
% Atys
\tag #'(atys basse) {
  Ah ! c’est vous, beau -- té trop cru -- el -- le,
}
% Sangaride
\tag #'(sangaride basse) {
  Ah ! c’est vous a -- mant in -- fi -- dè -- le.
}
% Atys, et Sangaride.
\tag #'(sangaride basse) {
  Ah ! c’est vous, a -- mant in -- fi -- dè -- le, c’est vous,
  qui rom -- pez des li -- ens si doux.
  C’est vous,
}
\tag #'basse { C’est vous, }
\tag #'(sangaride basse) {
  Ah ! c’est vous,
  qui rom -- pez des li -- ens si doux.
}
\tag #'(atys) {
  Beau -- té trop cru -- el -- le, c’est vous,
  qui rom -- pez des li -- ens si doux.
  C’est vous, ah ! c’est vous,
  qui rom -- pez des li -- ens si doux.
}
% Sangaride
\tag #'(sangaride basse) {
  Vous m’a -- vez im -- mo -- lée à l’a -- mour de Cy -- bè -- le.
}
% Atys
\tag #'(atys basse) {
  Il est vrai qu’à ses yeux, par un se -- cret ef -- froi,
  j’ai vou -- lu de nos cœurs ca -- cher l’in -- tel -- li -- gen -- ce :
  mais ce n’est que pour vous que j’ai crains sa ven -- gean -- ce,
  et je ne la crains pas pour moi.
  Cy -- bè -- le m’aime en vain, et c’est vous que j’a -- do -- re.
}
% Sangaride
\tag #'(sangaride basse) {
  A -- près votre in -- fi -- dé -- li -- té,
  au -- riez- vous bien la cru -- au -- té
  de vou -- loir me trom -- per en -- co -- re ?
}
% Atys
\tag #'(atys basse) {
  Moi ! vous tra -- hir ? vous le pen -- sez ?
  In -- gra -- te, que vous m’of -- fen -- sez !
  Eh bien, il ne faut plus rien tai -- re,
  je vais de la dé -- esse at -- ti -- rer la co -- lè -- re,
  m’of -- frir à sa fu -- reur, puis -- que vous m’y for -- cez…
}
% Sangaride
\tag #'(sangaride basse) {
  Ah ! de -- meu -- rez, A -- tys, mes soup -- çons sont pas -- sés ;
  vous m’ai -- mez, je le crois, j’en veux ê -- tre cer -- tai -- ne.
  Je le sou -- haite as -- sez,
  pour le croi -- re sans pei -- ne.
}
% Atys
\tag #'(atys basse) {
  Je ju -- re,
}
% Sangaride
\tag #'(sangaride basse) {
  Je pro -- mets,
}
% Atys
\tag #'(atys basse) {
  je ju -- re,
}
% Sangaride
\tag #'(sangaride basse) {
  je pro -- mets,
}
% Atys et Sangaride.
\tag #'(atys sangaride basse) {
  de ne chan -- ger ja -- mais.
}
\tag #'(atys basse) {
  Je ju -- re,
  \tag #'atys {
    je pro -- mets,
    de ne chan -- ger ja -- mais.
  }
}
\tag #'(sangaride basse) {
  Je pro -- mets,
  de ne chan -- ger ja -- mais.
}

% Sangaride
\tag #'(sangaride basse) {
  Quel tour -- ment de ca -- cher u -- ne si bel -- le flam -- me.
}
% Atys
\tag #'(atys basse) {
  Re -- dou -- blons- en l’ar -- deur dans le fonds de notre â -- me.
}
% Atys et Sangaride.
\tag #'(atys basse) {
  Ai -- mons en se -- cret, ai -- mons- nous,
  ai -- mons, ai -- mons __ en se -- cret, ai -- mons- nous, ai -- mons- nous ;
}
\tag #'(sangaride) {
  Ai -- mons en se -- cret, ai -- mons- nous,
  ai -- mons en se -- cret, ai -- mons- nous ;
}
\tag #'(atys sangaride basse) {
  ai -- mons plus que ja -- mais, en dé -- pit des ja -- loux.
  Ai -- mons en se -- cret, ai -- mons- nous ;
  ai -- mons plus que ja -- mais, en dé -- pit des ja -- loux.
  Ai -- mons plus que ja -- mais, en dé -- pit des ja -- loux.
}
% Sangaride
\tag #'(sangaride basse) {
  Mon pè -- re vient i -- ci,
}
% Atys
\tag #'(atys basse) {
  Que rien ne vous é -- ton -- ne ;
  ser -- vons- nous du pou -- voir que Cy -- bè -- le me don -- ne,
  je vais pré -- pa -- rer les Zé -- phirs
  à sui -- vre nos dé -- sirs.
}
\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new GrandStaff <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \withLyrics <<
      { s2.*10 s1*3 s2.*5 s1 s2.*4 s1*8 s2. s1 s2.*2 s1*4
        \footnoteHere #'(0 . 0) \markup\wordwrap {
          [Manuscrit] et [Baussen 1709] Jouer pour l'entr’acte
          l’entrée des nations (page \page-refIII #'CDBentreeNations .)
        }
      }
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
  \midi { }
}

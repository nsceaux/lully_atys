\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff \with { \haraKiriFirst \tinyStaff } \withTinyLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new GrandStaff <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
  >>
  \layout { }
}
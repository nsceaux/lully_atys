\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse #:score-template "score-basse-continue-voix"
          #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Cybèle
    \livretVerse#7 { Espoir si cher, et si doux, }
    \livretVerse#7 { Ah ! pourquoi me trompez-vous ? }
    \livretVerse#12 { Des suprêmes grandeurs vous m’avez fait descendre, }
    \livretVerse#12 { Mille cœurs m’adoraient, je les néglige tous, }
    \livretVerse#12 { Je n’en demande qu’un, il a peine à se rendre ; }
    \livretVerse#12 { Je ne sens que chagrin, et que soupçons jaloux ; }
    \livretVerse#12 { Est-ce le sort charmant que je devais attendre ? }
    \livretVerse#7 { Espoir si cher, et si doux, }
  }
  \column {
    \null
    \livretVerse#7 { Ah ! pourquoi me trompez-vous ? }
    \livretVerse#12 { Hélas ! par tant d’attraits fallait-il me surprendre ? }
    \livretVerse#12 { Heureuse, si toujours j’avais pu m’en défendre ! }
    \livretVerse#12 { L’Amour qui me flattait me cachait son couroux : }
    \livretVerse#12 { C’est donc pour me frapper des plus funestes coups, }
    \livretVerse#12 { Que le cruel Amour m’a fait un cœur si tendre ? }
    \livretVerse#7 { Espoir si cher, et si doux, }
    \livretVerse#7 { Ah ! pourquoi me trompez-vous ? }
  }
}#}))

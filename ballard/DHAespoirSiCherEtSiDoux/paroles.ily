%% ACTE 3 SCENE 8
% Cybele seule.
Es -- poir si cher, et si doux,
ah ! ah ! pour -- quoi me trom -- pez- vous ?
Des su -- prê -- mes gran -- deurs vous m’a -- vez fait des -- cen -- dre,
mil -- le cœurs m’a -- do -- raient, je les né -- gli -- ge tous,
je n’en de -- man -- de qu’un, il a peine à se ren -- dre ;
je ne sens que cha -- grin, et que soup -- çons ja -- loux ;
est- ce le sort char -- mant que je de -- vais at -- ten -- dre ?
Es -- poir si cher, et si doux,
ah ! ah ! pour -- quoi me trom -- pez- vous ?
Hé -- las ! par tant d’at -- traits fal -- lait- il me sur -- pren -- dre ?
Heu -- reu -- se, si tou -- jours j’a -- vais pu m’en dé -- fen -- dre !
L’a -- mour qui me flat -- tait me ca -- chait son cou -- roux :
c’est donc pour me frap -- per des plus fu -- nes -- tes coups,
que le cru -- el A -- mour m’a fait un cœur si ten -- dre ?
Es -- poir si cher, et si doux,
ah ! ah ! pour -- quoi me trom -- pez- vous ?

\clef "dessus" r4 sol''4. sol''8 |
fad''4\trill fad''4. sol''8 |
mi''4\trill mi''4. fad''8 |
sol''4 sol'' fad'' |
mi''4. fad''8 fad''8.\trill mi''16 |
red''2.\trill |
r4 si''4. si''8 |
si''4 la''4.\trill la''8 |
la''4 sol''4.\trill sol''8 |
sol''4 fad''4.\trill sol''8 |
mi''2 r |
\ru#2 { \allowPageTurn R1 }
\ru#5 { \allowPageTurn R2. }
\allowPageTurn R1
\ru#4 { \allowPageTurn R2. }
\ru#8 { \allowPageTurn R1 }
\allowPageTurn R2.
\allowPageTurn R1
\ru#2 { \allowPageTurn R2. }
\ru#5 { \allowPageTurn R1 }

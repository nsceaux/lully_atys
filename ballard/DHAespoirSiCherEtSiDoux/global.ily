\key mi \minor
\beginMark "Ritournelle"
\digitTime\time 3/4 \midiTempo#80 s2.*10
\time 4/4 s1*3
\digitTime\time 3/4 s2.*5
\time 4/4 s1
\digitTime\time 3/4 s2.*4
\time 4/4 s1*8
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.*2
\time 4/4 s1*5 \bar "|."

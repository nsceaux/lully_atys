\notesSection "Acteurs de la tragédie"
\markuplist\abs-fontsize-lines#8
\override-lines#'(column-padding . 10)
\page-columns-title \act ACTEURS DE LA TRAGÉDIE {
  \character-ambitus\wordwrap {
    \smallCaps Atys, \smaller\italic {
      parent de Sangaride, et favori de Célénus roi de Phrygie.
    }
  } "vhaute-contre" { mi la' }
  \character-ambitus\wordwrap {
    \smallCaps Idas, \smaller\italic {
      ami d’Atys, et frère de la nymphe Doris.
    }
  } "vbasse" { sol, mi' }
  \character-ambitus\wordwrap {
    \smallCaps Sangaride, \smaller\italic {
      nymphe, fille du Fleuve Sangar.
    }
  } "vbas-dessus" { re' sol'' }
  \character-ambitus\wordwrap {
    \smallCaps Doris, \smaller\italic {
      nymphe, amie de Sangaride, et sœur d’Idas.
    }
  } "vbas-dessus" { re' fa'' }
  \choir-ambitus\column {
    \wordwrap { Chœur de Phrygiens et de Phrygiennes }
    \wordwrap {
      Chœur et troupe de peuples différents qui viennent à la fête de Cybèle.
    }
    \wordwrap {
      Troupe de dieux de fleuves, et de ruisseaux, et de nymphes
      de fontaines, qui chantent et dansent.
    }
    \wordwrap {
      Troupe de divinités des bois et des eaux. Troupe de Corybantes.
    }
  }
  #'("vdessus" "vhaute-contre" "vtaille" "vbasse") {
    { mi' la'' }
    { la sib' }
    { mi fa' }
    { sol, mi' }
  }
  \wordwrap {
    Troupe de Phrygiens et de Phrygiennes qui dansent à la fête de Cybèle.
  } \vspace#1
  \character-ambitus\smallCaps La Déesse Cybèle "vbas-dessus" { re' sol'' }
  \character-ambitus\wordwrap {
    \smallCaps Mélisse, \smaller\italic {
      confidente et prêtresse de Cybèle.
    }
  } "vbas-dessus" { mi' sol'' }
  \wordwrap {
    Troupe de prêtresses de Cybèle.
  } \vspace#1
  \character-ambitus\wordwrap {
    \smallCaps Célénus, \smaller\italic {
      roi de Phrygie, fils de Neptune, et amant de Sangaride.
    }
  } "vbasse-taille" { la, mi' }
  \wordwrap {
    Troupe de suivants de Célénus.
  } \vspace#1
  \wordwrap {
    Troupe de Zéphirs chantants, dansants, et volants.
  } \vspace#1
  \character-ambitus\smallCaps Le Dieu du Sommeil "vhaute-contre" { la sol' }
  \character-ambitus\smallCaps Morphée "vhaute-contre" { sol la' }
  \character-ambitus\smallCaps Phantase "vtaille" { fad fa' }
  \character-ambitus\smallCaps Phobétor "vbasse" { sol, re' }
  \wordwrap {
    Troupe de songes agréables.
  } \vspace#1
  \choir-ambitus\wordwrap { Troupe des songes funestes. }
  #'("vhaute-contre" "vtaille" "vtaille" "vbasse") {
    { do' la' }
    { la fa' }
    { fa do' }
    { mi, re' }
  }
  \character-ambitus\wordwrap {
    \smallCaps { Le Dieu du Fleuve Sangar, }
    \smaller\italic { père de Sangaride. }
  } "vbasse" { sol, re' }
  Alecton \vspace#1
}

%% ACTE 1 SCENE 4
\tag #'(voix1 basse) {
  % Sangaride
  A -- tys est trop heu -- reux.
  % Doris
  L’a -- mi -- tié fut tou -- jours é -- gale en -- tre vous deux,
  et le sang d’as -- sez près vous li -- e :
  quel que soit son bon -- heur, lui por -- tez- vous en -- vi -- e ?
  Vous, qu’au -- jour -- d’hui l’hy -- men a -- vec de si beaux nœuds
  doit u -- nir au roi de Phry -- gi -- e ?
  % Sangaride
  A -- tys, est trop heu -- reux.
  Sou -- ve -- rain de son cœur, maî -- tre de tous ses vœux,
  sans crain -- te, sans mé -- lan -- co -- li -- e,
  il jou -- it en re -- pos des beaux jours de sa vi -- e ;
  A -- tys ne con -- naît point les tour -- ments a -- mou -- reux,
  A -- tys ne con -- naît point les tour -- ments a -- mou -- reux,
  A -- tys est trop heu -- reux.
  % Doris
  Quel mal vous fait l’a -- mour ? vo -- tre cha -- grin m’é -- ton -- ne.
  % Sangaride
  Je te fie un se -- cret qui n’est su de per -- son -- ne.
  Je de -- vrais ai -- mer un a -- mant
  qui m’offre u -- ne cou -- ron -- ne ;
  mais, hé -- las ! vai -- ne -- ment
  le de -- voir me l’or -- don -- ne,
  l’a -- mour, pour mon tour -- ment,
  en or -- donne au -- tre -- ment,
  l’a -- mour, pour mon tour -- ment,
  en or -- donne au -- tre -- ment.
  % Doris
  Ai -- me -- riez- vous A -- tys, lui dont l’in -- dif -- fé -- ren -- ce
  brave a -- vec tant d’or -- gueil l’a -- mour et sa puis -- san -- ce ?
  % Sangaride
  J’aime, A -- tys, en se -- cret, mon crime, est sans té -- moins.
  Pour vain -- cre mon a -- mour, je mets tout en u -- sa -- ge,
  j’ap -- pel -- le ma rai -- son, j’a -- ni -- me mon cou -- ra -- ge ;
  mais à quoi ser -- vent tous mes soins ?
  mon cœur en souf -- fre da -- van -- ta -- ge,
  et n’en ai -- me pas moins.
  % Doris
  C’est le com -- mun dé -- faut des bel -- les.
  L’ar -- deur des con -- quê -- tes nou -- vel -- les
  fait né -- gli -- ger les cœurs qu’on a trop tôt char -- més,
  et les in -- dif -- fé -- rents sont quel -- que -- fois ai -- més
  aux dé -- pents des a -- mants fi -- dè -- les,
  et les in -- dif -- fé -- rents sont quel -- que -- fois ai -- més
  aux dé -- pents des a -- mants fi -- dè -- les.
  Mais vous vous ex -- po -- sez à des pei -- nes cru -- el -- les.
  % Sangaride
  Tou -- jours aux yeux d’A -- tys je se -- rai sans ap -- pas ;
  je le sais, j’y con -- sens, je veux, s’il est pos -- si -- ble,
  qu’il soit en -- cor plus in -- sen -- si -- ble ;
  s’il me pou -- vait ai -- mer, que de -- vien -- drais-je ? hé -- las !
  c’est mon plus grand bon -- heur qu’A -- tys ne m’ai -- me pas.
  Je pré -- tends être heu -- reuse, au moins, en ap -- pa -- ren -- ce ;
  au des -- tin d’un grand roi je me vais at -- ta -- cher.
}
% Sangaride, et Doris.
Un a -- mour mal -- heu -- reux dont le de -- voir s’of -- fen -- se,
se doit con -- dam -- ner au si -- len -- ce ;
un a -- mour mal -- heu -- reux qu’on nous peut re -- pro -- cher,
ne sau -- rait trop bien se ca -- cher.
Un a -- mour mal -- heu -- reux qu’on nous peut re -- pro -- cher,
ne sau -- rait trop bien se ca -- cher,
ne sau -- rait trop bien se ca -- cher.

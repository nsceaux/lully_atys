\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Sangaride
    \livretVerse#6 { Atys est trop heureux. }
    \livretPers Doris
    \livretVerse#12 { L’amitié fut toujours égale entre vous deux, }
    \livretVerse#8 { Et le sang d’assez près vous lie : }
    \livretVerse#12 { Quel que soit son bonheur, lui portez-vous envie ? }
    \livretVerse#12 { Vous, qu’aujourd’hui l’hymen avec de si beaux nœuds }
    \livretVerse#8 { Doit unir au Roi de Phrygie ? }
    \livretPers Sangaride
    \livretVerse#6 { Atys, est trop heureux. }
    \livretVerse#12 { Souverain de son cœur, maître de tous ses vœux, }
    \livretVerse#8 { Sans crainte, sans mélancolie, }
    \livretVerse#12 { Il jouit en repos des beaux jours de sa vie ; }
    \livretVerse#12 { Atys ne connaît point les tourments amoureux, }
    \livretVerse#6 { Atys est trop heureux. }
    \livretPers Doris
    \livretVerse#12 { Quel mal vous fait l’Amour ? votre chagrin m’étonne. }
    \livretPers Sangaride
    \livretVerse#12 { Je te fie un secret qui n’est su de personne. }
    \livretVerse#8 { Je devrais aimer un amant }
    \livretVerse#6 { Qui m’offre une couronne ; }
    \livretVerse#6 { Mais, hélas ! vainement }
    \livretVerse#6 { Le Devoir me l’ordonne, }
    \livretVerse#6 { L’Amour, pour mon tourment, }
    \livretVerse#6 { En ordonne autrement. }
    \livretPers Doris
    \livretVerse#12 { Aimeriez-vous Atys, lui dont l’indifférence }
    \livretVerse#12 { Brave avec tant d’orgueil l’Amour et sa puissance ? }
  }
  \column {
    \livretPers Sangaride
    \livretVerse#12 { J’aime, Atys, en secret, mon crime, est sans témoins. }
    \livretVerse#12 { Pour vaincre mon amour, je mets tout en usage, }
    \livretVerse#12 { J’appelle ma raison, j’anime mon courage ; }
    \livretVerse#8 { Mais à quoi servent tous mes soins ? }
    \livretVerse#8 { Mon cœur en souffre davantage, }
    \livretVerse#6 { Et n’en aime pas moins. }
    \livretPers Doris
    \livretVerse#8 { C’est le commun défaut des belles. }
    \livretVerse#8 { L’ardeur des conquêtes nouvelles }
    \livretVerse#12 { Fait négliger les cœurs qu’on a trop tôt charmés, }
    \livretVerse#12 { Et les indifférents sont quelquefois aimés }
    \livretVerse#8 { Aux dépents des amants fidèles. }
    \livretVerse#12 { Mais vous vous exposez à des peines cruelles. }
    \livretPers Sangaride
    \livretVerse#12 { Toujours aux yeux d’Atys je serai sans appas ; }
    \livretVerse#12 { Je le sais, j’y consens, je veux, s’il est possible, }
    \livretVerse#8 { Qu’il soit encor plus insensible ; }
    \livretVerse#12 { S’il me pouvait aimer, que deviendrais-je ? hélas ! }
    \livretVerse#12 { C’est mon plus grand bonheur qu’Atys ne m’aime pas. }
    \livretVerse#12 { Je prétends être heureuse, au moins, en apparence ; }
    \livretVerse#12 { Au destin d’un grand Roi je me vais attacher. }
    \livretPers Sangaride, et Doris
    \livretVerse#12 { Un amour malheureux dont le devoir s’offense, }
    \livretVerse#8 { Se doit condamner au silence ; }
    \livretVerse#12 { Un amour malheureux qu’on nous peut reprocher, }
    \livretVerse#8 { Ne saurait trop bien se cacher. }
  }
}#}))

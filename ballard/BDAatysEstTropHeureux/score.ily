\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix1 \includeNotes "voix"
    >> \keepWithTag #'voix1 \includeLyrics "paroles"
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'voix2 \includeNotes "voix"
    >> \keepWithTag #'voix2 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion {
        s2.*8 s1 s1*4 s2.*2 s1 s2.*29 s1 s2. s1*2
        s2.*18 s1*16 s2.*20 s1*11 s1*5 s2.\break
      }
    >>
  >>
  \layout { }
  \midi { }
}

<<
  \tag #'voix2 {
    \clef "vbas-dessus" R2.*8 R1 R1*4 R2.*2 R1 R2.*29 R1 R2. R1*2
    R2.*18 R1*16 R2.*20 R1*11 R1*5 R2.
  }
  \tag #'(voix1 basse) {
    %% Sangaride
    \sangarideMark R2.*5 |
    r4 r la'4 |
    re''2. |
    r8 re'' re''4 dod'' |
    re''2 r |
    \dorisMark
    r8 fa'16 sol' la'8\trill la'16 \ficta sib'16 do''8. do''16 do'' sib' sib' la' |
    la'4\trill r8 do''16 do'' dod''8 dod''16 dod'' dod''8. re''16 |
    re''4 re'' r8 sib'16 sib' sib'8 sib'16 la' |
    la'8.\trill la'16 la' sol' fa' mi' fa'4 fa' |
    re'' r8 re''16 re'' la'8. \ficta sib'16 |
    do''8. do''16 do''8 do'' sib'\trill la' |
    sib'4 r8 sib'16 do'' %{ la'8\trill la' %} la'8.\trill la'16 re''8 re''16 sol' |
    la'4 la' r |
    R2. |
    \sangarideMark r4 r la'4 |
    re''2. |
    r8 re'' re''4 dod'' |
    re''2 la'8 \ficta sib'8 |
    do''4 do'' la' |
    re''2. |
    mi''4 mi''8 mi'' mi''[ re''16] mi'' |
    fa''2 sib'4 |
    sib'4.( la'8) la'4 |
    r la'8 la' la' sol' |
    la'4 la' mi''8 mi'' |
    fa''4 fa'' re'' |
    mi''2 mi''8 fa'' |
    re''2 dod''8 re'' |
    dod''4\trill %{ la'4. %} la'4 r8 mi'8 |
    fa'4. sol'8 la' \ficta sib'8 |
    do''4 la'4. la'8 |
    la'4 sol'8[ fa'] sol'4 |
    la'2. |
    r4 r sib'4 |
    sib'4. la'8 la' la' |
    la'4 sol'4.\trill sol'8 |
    sol'2 fa'8\trill mi' |
    fa'2. |
    r4 r la'4 |
    re''2. |
    r8 re'' re''4 dod'' |
    re'' 
    \dorisMark r8 re'' %{ si'4\trill si'8 si'16 si' %} si'8.\trill si'16 si'8. si'16 |
    sold'4 si'8 si'16 do'' re''8. mi''16 |
    dod''4 dod''
    \sangarideMark r8 la'16 la' la'8 la'16 la' |
    mi'8\trill mi'16 fa' sol'8 fa'16 mi' fa'4 fa' |
    r re''8 re'' la'8.\trill sib'16 |
    do''4 sib'4.\trill la'8 |
    sol'2\trill r8 do'' |
    re''4. re''8 %{ mi''8 fa'' %} mi''8. fa''16 |
    fa''2( mi''4\trill) |
    fa'' do''4. do''8 |
    dod''2\trill dod''8 re'' |
    mi''2 mi''8 fa'' |
    re''2\trill si'8 do'' |
    do''4( si'2)\trill |
    la'2 do''4 |
    do''4. do''8 do''[ si'16] do'' |
    si'2\trill si'8 dod'' |
    re''4 dod''4.\trill re''8 |
    re''2 r8 do'' |
    do''4. do''8 do''[ si'16] do'' |
    si'2\trill si'8 dod'' |
    re''4 dod''4.\trill re''8 |
    re''2
    \dorisMark la'8 la'16 la' la'8. la'16 |
    % fad'4 r re''8 re''16 re'' la'8 si' |
    fa'4 re'' re''8. re''16 la'8 \ficta sib' |
    do''4 do'' do''8 do''16 do'' re''8 mi'' |
    %{ fa''4. %} fa''4 r8 re''8 %{ si'8\trill si' %} si'8.\trill si'16 dod''8 re'' |
    dod''4 dod''
    \sangarideMark r4 la'8 la' |
    fad'4 fad'8 fad' sol'4. sol'8 |
    re''8 re'' re'' la' sib'4. re''8 |
    sol'4\trill sol'8 la'16 sib' do''8 do''16 do'' sol'8\trill la'16 sib' |
    la'4\trill la'8 r16 do'' %{ fa''8. fa''16 la'8. sib'16 %} fa''4 fa''8 la'16 \ficta sib' |
    % do''4. do''8 dod''4 dod''8 re''16 mi'' |
    do''4 r8 do'' dod''\trill dod'' re'' mi'' |
    fa''4 %{ fa''8 r %} fa''4 r8 la' la' si' |
    do'' do'' do'' re'' si'4\trill re'' |
    % mi''4 mi'' re''8 do'' si'8. la'16 |
    mi''4. mi''8 re'' do'' si' la' |
    sold'4\trill sold'8 si'16 do'' la'4 la'8 sold' |
    la'2
    \dorisMark fa'8 fa'16 fa' sol'8. la'16 |
    sol'4.\trill fa'8 mi'\trill mi' r la' |
    fa'4 sol' la' |
    sib' sol'4.\trill do''8 |
    la'4.\trill sol'8( fa'4) |
    do''4 do''8 do'' %{ dod''8. re''16 %} dod''8 re'' |
    mi''4. mi''8 re''\trill do'' |
    do''4( si'4.)\trill la'8 |
    la'2. |
    re''4 re''8 do'' sib' la' |
    sib'4. sib'8 do''4~ |
    do''8 re'' sib'4.\trill la'8 |
    sol'2\trill do''8 do'' |
    re''2 re''8 do'' |
    sib'\trill la' la'4( sol')\trill |
    fa'2. |
    sib'4 sib'8 la' %{ sol'8. fa'16 %} sol'8 fa' |
    mi'4.\trill mi'8 fa'4~ |
    fa'8 sol' sol'4.\trill la'8 |
    la'2 la'8 la' |
    si'2 si'8 si' |
    dod'' re'' re''4( dod'') |
    re''2 r8 la' la'16 la' re'' la' |
    sib'4 sib'8 sib' sib'4\trill sib'8. do''16 |
    la'4 la'8
    \sangarideMark do''8 do'' la' sib' do'' |
    re''4 sib'8 sib' sol'4\trill sol'8 sol' |
    mi'4\trill la'8 la' fa'4 sib'8 sib' |
    %{ sol'4.\trill %} sol'4\trill r8 do''8 do''8. do''16 %{ sib'8.\trill la'16 %} sib'8\trill la' |
    sib'8 sib' r16 sib' sib' %{ sib' %} do'' la'8 la' la' sol' |
    la'4 la' dod''8 dod''16 dod'' re''8. re''16 |
    si'4.\trill si'8 si' do'' re''8. mi''16 |
    dod''2\trill fa''8 fa''16 mi'' re''8. do''?16 |
    si'4.\trill si'8 dod'' re'' re'' dod'' |
    re''2 r4 la'8 la' |
    fa'4\trill sol'8 la' sib'4. sib'8 |
    la'\trill la' la' mi' fa'4 fa' |
    r la'8 si' do''4 do''8 la' |
    re''4 re'8 mi' fa'4 fa'8 sol' |
    la'2. |
  }
>>
<<
  \tag #'(voix1 basse) {
    r4 r fa''8 fa'' |
    mi''2\trill mi''8 fa'' |
    re''4.\trill re''8 re'' re'' |
    mi'' fa'' mi''4( re'')\trill |
    do''2 mi''4 |
    mi'' re''4.\trill do''8 |
    si'4 si'4. do''8 |
    do''4( si'2)\trill |
    la' mi''8 mi'' |
    fa''2 fa''8 mi'' |
    re''2\trill re''8 re'' |
    mi''4 re''4.\trill do''8 |
    do''2 do''8 do'' |
    do''2 sib'4 |
    la'2\trill la'8 sib' |
    sol'2\trill sib'8 do'' |
    la'2\trill la'8 sib' |
    do''2 do''8 do'' |
    re''2 %{ mi''8. fa''16 %} mi''8 fa'' |
    mi''2\trill do''8 do'' |
    sib'2\trill la'4 |
    sol'2\trill sol'8. la'16 |
    fa'2 do''8 do'' |
    dod''2\trill re''4 |
    %{ re''4.( dod''8) %} re''2 dod''8 re'' |
    re''2. |
  }
  \tag #'voix2 {
    r4 r la'8 si' |
    do''2 do''8 re'' |
    si'4.\trill si'8 si' si' |
    do'' re'' do''4( si') |
    do''2 do''4 |
    do''4 si'4. la'8 |
    sold'4 sold'4. la'8 |
    la'2( sold'4) | % la'4( sold'2)
    la'2 dod''8 dod'' |
    re''2 %{ la'8 %} re''8 la' |
    si'2 si'8 si' |
    do''4 si'4.\trill do''8 |
    do''2 mi'8 mi' |
    fad'2 sol'4 |
    sol'4.( fad'8) fad'\trill sol' |
    sol'2 re'8 mi' |
    fa'2 fa'8 sol' |
    la'2 la'8 la' |
    la'4.( sol'8) %{ sol'8. sol'16 %} sol'8 sol' |
    sol'2 la'8 la' |
    mi'2 fa'4 |
    fa'4.( mi'8) mi' fa' |
    fa'2 la'8 la' |
    sol'2\trill fa'4 |
    mi'2\trill mi'8 fa' |
    re'2. |
  }
>>
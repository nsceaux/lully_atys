\clef "basse" do4. do'8 si4 do' | % si4. do'8
la4. sol8 fad2 |
sol sol, |
fa, mi, |
re,4 re si,2 |
do la, |
sib, do4 do, |
fa,2 fa4. fa8 |
sol2 mi4. mi8 |
la2 fa4 re |
sol do sol,2 |
do1 |

\score {
  \new StaffGroup <<
    \new GrandStaff \with { instrumentName = "Flûtes" } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \with { \tinyStaff } <<
      \global \includeNotes "basse" \clef "treble"
    >>
  >>
  \layout { indent = \largeindent }
}

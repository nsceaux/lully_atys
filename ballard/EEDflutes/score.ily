\score {
  \new StaffGroup <<
    \new GrandStaff \with { instrumentName = "Flûtes" } <<
      \new Staff <<
        \footnoteHere #'(0 . 0) \markup\column {
          \line { [Philidor 1703] Les pièces se succèdent ainsi : }
          \line {
            \hspace #4 Premier trio de flûtes,
            \italic { La beauté la plus sévère }
          }
          \line {
            \hspace #4 Second trio de flûtes,
            \italic { L’hymen seul ne saurait plaire. }
          }
          \wordwrap {
            \hspace #4 Premier trio de flûtes,
            \italic { Il n’est point de résistance }
          }
          \wordwrap {
            \hspace #4 Second trio de flûtes,
            \italic { L’amour trouble tout le monde. }
          }
        }
        \global \includeNotes "dessus1"
      >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \with { instrumentName = "B.C." } <<
      \global \keepWithTag #'basse \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}

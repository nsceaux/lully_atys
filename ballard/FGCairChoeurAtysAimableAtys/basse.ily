\clef "basse"
<<
  \tag #'(basse-continue tous) {
    <<
      \tag #'tous <>^"B.C."
      do1
      \new CueVoice {
        \voiceTwo <>_"[Manuscrit]"
        do4. si,8 la,8. sol,16 fad,4 |
      }
    >>
    sol,4 sol8 fa mi fa mi re |
    do2 si,4\trill sib, |
    la,2 sol, |
    sol mi |
    fa4. re8 sol4. mi8 |
    la4 si8 do' sol sol, |
    do1 | \allowPageTurn
    do'4. do'8 do'4 si\trill |
    do'2 si\trill |
    la2 la8 si do' fa |
    sol2 do |
    re fad,\trill |
    sol, mib |
    re4. sib,8 do2 |
    re4. sib,8 mib do re re, |
    sol,2. sol4 |
    do'2 lab4 fa |
    sib4. sib8 si2\trill |
    do'2 re'4 re |
    sol2 mib |
    lab fa |
    sib si\trill |
    do'4. fa8 sol4 sol, |
    do2 do' |
    mi1 |
    fa2 fad\trill |
    sol2. mib4 |
    lab fa sol sol, |
    do2 do'~ |
    do' si\trill |
    do'1 |
    mi\trill |
    fa~ |
    fa2. mib4 |
    re1 |
    do\trill |
    sib,2 sib4 lab |
    sol2. fa4 |
    mib4. re8 do2~ |
    do4 sib, lab,2\trill |
    sol, sol |
    mi1 |
    fa2 fad\trill |
    sol2. mib4 |
    lab fa sol sol, |
    do2 do' |
    mi1 |
    fa2 fad\trill |
    sol2. mib4 |
    lab fa sol sol, |
    do1 | \allowPageTurn
    sol,2 sol |
    lab1 |
    la!\trill |
    sib2. sol4 |
    lab2 sib4 sib, |
    mib1 | \allowPageTurn
    mi |
    fa2 fad\trill |
    sol2. mib4 |
    lab fa sol sol, |
    do2 do' |
    mi1 |
    fa2 fad\trill |
    sol2. mib4 |
    lab fa sol sol, |
    do1 |
    sol2 sol4. sol8 |
    mi1\trill |
    fa2 
  }
  \tag #'basse {
    R1*6 R2. R1 R1*8 R1 R1*8 R1*26 R1 R1*15 R1*3 |
    r2
  }
>>
\twoVoices #'(basse basse-continue tous) <<
  { fa4 fa8 fa |
    sib4 sib r2 |
    r mib4 mib8 mib |
    lab4 lab fa fa8 fa |
    sib4 sib r2 |
    R1 |
    r2 re'4 re'8 re' |
    sol4 sol do'2 |
    r sol4 sol8 sol |
    do2 }
  { fa4. fa8 |
    sib2 sib, |
    mib2. mib4 |
    lab2 fa |
    sib sib, |
    do1 |
    re2 re' |
    sol do' |
    sol sol, |
    do }
>> <<
  \tag #'basse { r2 | R1*9 | r2 }
  \tag #'(basse-continue tous) {
    \tag #'tous <>^"B.C." do'2 |
    si\trill sib |
    la lab |
    sol1 |\allowPageTurn
    mi |
    fa4. mib8 re4 do |
    sib,1 |
    mib2 do |
    sol sol4. sol8 |
    mi1 |
    fa2 
  }
>> \twoVoices #'(basse basse-continue tous) <<
  { fa4 fa8 fa |
    sib4 sib r2 |
    r mib4 mib8 mib |
    lab4 lab fa fa8 fa |
    sib4 sib r2 |
    R1 |
    r2 re'4 re'8 re' |
    sol4 sol do'2 |
    r sol4 sol8 sol | }
  { fa4. fa8 |
    sib2 sib, |
    mib2. mib4 |
    lab2 fa |
    sib sib, |
    do1 |
    re2 re' |
    sol do' |
    sol sol, | }
>>
do1 | 
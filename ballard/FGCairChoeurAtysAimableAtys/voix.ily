<<
  %% Cybele
  \tag #'(cybele basse) {
    \cybeleMark r4 r8 sol' do''8. do''16 do''8. re''16 |
    si'4.\trill si'8 do''8. do''16 do''8. re''16 |
    mi''4 r8 sol' sol'4 sol'8 sol' |
    sol'4 sol'8 fad' sol'4 sol' |
    si'4. si'8 do''8. do''16 re''8 mi'' |
    la'8\trill la' r re'' si'4\trill si'8 mi'' |
    do'' do'' re'' mi'' re''8.\trill do''16 |
    do''2 r |
    sol'4 sol'8 sol'16 la' fa'4\trill fa'8. mi'16 |
    mi'8\trill mi' r sol' sol'8. sol'16 la'8 si' |
    do''4. do''8 do''8. re''16 mi''8. fa''16 |
    re''4\trill r8 re''16 re'' mi''8 mi''16 re'' do''8. si'16 |
    la'4\trill r8 re'' re''4 r8 la' |
    sib'4 sib'8 sib' sol'4 sol'8 la' |
    fad'8\trill fad' r re'' mib''4 r8 la' |
    fad'4 la'8. sib'16 sol'4 sol'8 fad' |
    sol'4 sol' <<
      \tag #'basse { s2 s1*7 s2 \cybeleMark }
      \tag #'cybele { r2 | R1*7 | r2 <>^\markup\character Cybele }
    >> r4 r8 sol'16 sol' |
    do''2 do''4. do''8 |
    la'2\trill re''4 re''8 re'' |
    si'2. mib''4 |
    do''4. do''8 do''4 si' |
    do'' do'' r do''8. do''16 |
    re''2 re''4. re''8 |
    mib''2. mib''4 |
    do''4. sib'8 sib'4( la'8) sib' |
    la'2.\trill do''4 |
    do''4. do''8 re''4 mib'' |
    fa''2. sib'4 |
    mib''4. mib''8 mib''4( re''8) mib'' |
    re''2\trill sib'4. sib'8 |
    si'2 si'4. si'8 |
    do''2. mib'4 |
    fa' sol' sol' fa' |
    sol' sol' r sol'8. sol'16 |
    do''2 do''4. do''8 |
    la'2\trill re''4 re''8 re'' |
    si'2. mib''4 |
    do''4.\trill do''8 do''4 si' |
    do'' do'' <<
      \tag #'basse { s2 s1*4 s2 \cybeleMark }
      \tag #'cybele { r2 | R1*4 | r2 }
    >> r8 sol' sol' la' |
    sib'2 si'4 si'8 si' |
    do''2. do''8 re'' |
    mib''4 do''8 do'' fa''4. fa''8 |
    re''2\trill r4 sib'8 sib' |
    lab'4\trill lab'8 sol' fa'4.\trill sol'8 |
    mib'4 mib' r sol'8. sol'16 |
    do''2 do''4. do''8 |
    la'2\trill re''4 re''8 re'' |
    si'2. mib''4 |
    do''4. do''8 do''4 si' |
    do'' do'' <<
      \tag #'basse { s2 s1*4 s2 \cybeleMark }
      \tag #'cybele { r2 | R1*4 | r2 }
    >> mib''4 mib''8 mib'' |
    si'2 <<
      \tag #'basse { s1 \cybeleMark }
      \tag #'cybele { r2 | r }
    >> sol''4 do''8 do'' |
    la'4\trill la' <<
      \tag #'basse { s1 \cybeleMark }
      \tag #'cybele { r2 | r }
    >> sib'4 sib'8 sib' |
    sol'2\trill <<
      \tag #'basse { s1*2 \cybeleMark }
      \tag #'cybele { r2 | R1 | r2 }
    >> re''2~ |
    re'' la'4. sib'8 |
    fad'2 <<
      \tag #'basse { s1*3 \cybeleMark }
      \tag #'cybele { r2 | R1*2 | r2 }
    >> r4 mib'' |
    re''2\trill re''4. mib''8 |
    do''2 do''4. re''8 |
    si'2 si'4 r8 re'' |
    sol'2 do''4 do''8 do'' |
    la'4\trill la'8 la' sib'4 do'' |
    re'' re''8 fa'' sib'4 fa'8 sib' |
    sol'4\trill sol' mib'' mib''8 mib'' |
    si'2 <<
      \tag #'basse { s1 \cybeleMark }
      \tag #'cybele { r2 | r }
    >> sol''4 do''8 do'' |
    la'4\trill la' <<
      \tag #'basse { s1 \cybeleMark }
      \tag #'cybele { r2 | r }
    >> sib'4 sib'8 sib' |
    sol'2\trill <<
      \tag #'basse { s1*2 \cybeleMark }
      \tag #'cybele { r2 | R1 | r2 }
    >> re''2~ |
    re'' la'4. sib'8 |
    fad'2
    \tag #'cybele { r2 | R1*3 }
  }
  %% Chœur
  \tag #'(vdessus basse) {
    <<
      \tag #'basse { s1*6 s2. s1*9 s2 \choeurMark }
      \tag #'vdessus { \clef "vbas-dessus" R1*6 | R2. | R1*9 | r2 }
    >> r4 re''8 re'' |
    mib''2 mib''8. mib''16 fa''8. fa''16 |
    re''4\trill r8 fa'' fa''4 r8 re'' |
    mib''4 do''8. sib'16 la'4\trill la'8. re''16 |
    si'4\trill si' r r8 mib''16 mib'' |
    do''2 fa''8. fa''16 fa''8. fa''16 |
    re''4\trill r8 fa'' fa''4 r8 re'' |
    mib''4 mib''8. fa''16 re''4\trill re''8 mib'' |
    do''4\trill do''
    <<
      \tag #'basse { s2 s1*21 s2 } \tag #'vdessus { r2 | R1*21 | r2 } >>
    \tag #'basse \choeurMark r4 sol'8. sol'16 |
    do''2 do''4. do''8 |
    la'2\trill re''4 re''8 re'' |
    si'2.\trill mib''4 |
    do''4. do''8 do''4 si' |
    do''4 do''
    <<
      \tag #'basse { s2 s1*10 s2 } \tag #'vdessus { r2 | R1*10 | r2 } >>
    \tag #'basse \choeurMark r4 sol'8. sol'16 |
    do''2 do''4. do''8 |
    la'2\trill re''4 re''8 re'' |
    si'2.\trill mib''4 |
    do''4. do''8 do''4 si' |
    do'' do''
    <<
      \tag #'basse { s1 \choeurMark }
      \tag #'vdessus { r2 | r }
    >> si'4 si'8 si' |
    do''2
    <<
      \tag #'basse { s1 \choeurMark }
      \tag #'vdessus { r2 | r }
    >> do''4 do''8 fa'' |
    re''4\trill re''
    <<
      \tag #'basse { s1 \choeurMark }
      \tag #'vdessus { r2 | r }
    >> mib''4 mib''8 mib'' |
    do''4\trill do'' fa'' fa''8 fa'' |
    re''4\trill re''
    <<
      \tag #'basse { s1*2 \choeurMark }
      \tag #'vdessus { r2 | R1 | r2 }
    >> la'4 la'8 re'' |
    si'4\trill si' mib''2 |
    r re''4\trill re''8 mib'' |
    do''2
    <<
      \tag #'basse { s1*8 \choeurMark }
      \tag #'vdessus { r2 | R1*7 | r2 }
    >> si'4 si'8 si' |
    do''2
    <<
      \tag #'basse { s1 \choeurMark }
      \tag #'vdessus { r2 | r }
    >> do''4 do''8 fa'' |
    re''4\trill re''
    <<
      \tag #'basse { s1 \choeurMark }
      \tag #'vdessus { r2 | r }
    >> mib''4 mib''8 mib'' |
    do''4\trill do'' fa'' fa''8 fa'' |
    re''4\trill re''
    <<
      \tag #'basse { s1*2 \choeurMark }
      \tag #'vdessus { r2 | R1 | r2 }
    >> la'4 la'8 re'' |
    si'4 si' mib''2 |
    r re''4 re''8 mib'' |
    do''1\trill |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" R1*6 | R2. | R1*9 |
    r2 r4 sol'8 sol' |
    sol'2 lab'8. lab'16 lab'8. lab'16 |
    fa'4 r8 sib' sol'4 r8 sol' |
    sol'4 sol'8 sol' sol'4 sol'8 fad' |
    sol'4 sol' r r8 sol'16 sol' |
    mib'2 lab'8. lab'16 lab'8. lab'16 |
    fa'4 r8 sib' sol'4 r8 sol' |
    sol'4 sol'8 lab' sol'4 sol'8 sol' |
    mib'4 mib' r2 |
    R1*21 |
    r2 r4 sol'8. sol'16 |
    sol'2 sol'4. sol'8 |
    fa'2 la'4 la'8 la' |
    re'2. sol'4 |
    mib'4 fa' re'4. re'8 |
    mib'4 mib' r2 |
    R1*10 |
    r2 r4 sol'8. sol'16 |
    sol'2 sol'4. sol'8 |
    fa'2 la'4 la'8 la' |
    re'2. sol'4 |
    mib' fa' re'4. re'8 |
    mib'4 mib' r2 |
    r sol'4 sol'8 sol' |
    sol'2 r2 |
    r fa'4 fa'8 fa' |
    fa'4 fa' r2 |
    r sol'4 sol'8 sol' |
    mib'4 mib' lab' lab'8 lab' |
    fa'4 fa' r2 |
    R1 |
    r2 fad'4 fad'8 fad' |
    sol'4 sol' sol'2 |
    r sol'4 sol'8 sol' |
    mib'2 r2 |
    R1*7 |
    r2 sol'4 sol'8 sol' |
    sol'2 r2 |
    r fa'4 fa'8 fa' |
    fa'4 fa' r2 |
    r sol'4 sol'8 sol' |
    mib'4 mib' lab' lab'8 lab' |
    fa'4 fa' r2 |
    R1 |
    r2 fad'4 fad'8 fad' |
    sol'4 sol' sol'2 |
    r sol'4 sol'8 sol' |
    mib'1 |
  }
  \tag #'vtaille {
    \clef "vtaille" R1*6 | R2. | R1*9 |
    r2 r4 si8 si |
    do'2 do'8. do'16 do'8. do'16 |
    sib4 r8 re' re'4 r8 re' |
    do'4 mib'8 mib' re'4 re'8 re' |
    re'4 re' r r8 sib16 sib |
    lab2 do'8. do'16 do'8. do'16 |
    sib4 r8 re' re'4 r8 re' |
    do'4 do'8 do' do'4 si8 si |
    do'4 do' r2 |
    R1*21 |
    r2 r4 do'8. do'16 |
    do'2 do'4. do'8 |
    do'2 la4 la8 la |
    sol2. sol4 |
    lab4. lab8 sol4. sol8 |
    sol4 sol r2 |
    R1*10 |
    r2 r4 do'8. do'16 |
    do'2 do'4. do'8 |
    do'2 la4 la8 la |
    sol2. sol4 |
    lab4 lab sol4. sol8 |
    sol4 sol r2 |
    r re'4 re'8 re' |
    do'2 r2 |
    r la4 la8 la |
    sib4 sib r2 |
    r sib4 sib8 sib |
    lab4 lab do' do'8 do' |
    sib4 sib r2 |
    R1 |
    r2 re'4 re'8 re' |
    re'4 re' do'2\trill |
    r si4 si8 do' |
    do'2 r2 |
    R1*7 |
    r2 re'4 re'8 re' |
    do'2 r2 |
    r la4 la8 la |
    sib4 sib r2 |
    r sib4 sib8 sib |
    lab4 lab do' do'8 do' |
    sib4 sib r2 |
    R1 |
    r2 re'4 re'8 re' |
    re'4 re' do'2 |
    r si4 si8 do' |
    do'1 |
  }
  \tag #'vbasse {
    \clef "vbasse" R1*6 | R2. | R1*9 |
    r2 r4 sol8 sol |
    do'2 lab8. lab16 fa8. fa16 |
    sib4 r8 sib si4\trill r8 si |
    do'4 do'8 do' re'4 re'8 re |
    sol4 sol r r8 mib16 mib |
    lab2 fa8. fa16 fa8. fa16 |
    sib4 r8 sib si4\trill r8 si |
    do'4 do'8 fa sol4 sol8 sol, |
    do4 do r2 |
    R1*21 |
    r2 r4 do'8. do'16 |
    mi2\trill mi4. mi8 |
    fa2 fad4\trill fad8 fad |
    sol2. mib4 |
    lab fa sol4. sol8 |
    do4 do r2 |
    R1*10 |
    r2 r4 do'8. do'16 |
    mi2 mi4. mi8 |
    fa2 fad4 fad8 fad |
    sol2. mib4 |
    lab fa sol4. sol8 |
    do4 do r2 |
    r sol4 sol8 sol |
    mi2 r2 |
    r fa4 fa8 fa |
    sib4 sib r2 |
    r mib4 mib8 mib |
    lab4 lab fa fa8 fa |
    sib4 sib r2 |
    R1 |
    r2 re'4 re'8 re' |
    sol4 sol do'2 |
    r sol4 sol8 sol |
    do2 r2 |
    R1*7 |
    r2 sol4 sol8 sol |
    mi2 r2 |
    r fa4 fa8 fa |
    sib4 sib r2 |
    r mib4 mib8 mib |
    lab4 lab fa fa8 fa |
    sib4 sib r2 |
    R1 |
    r2 re'4 re'8 re' |
    sol4 sol do'2 |
    r sol4 sol8 sol |
    do1 |
  }
>>
\piecePartSpecs
#`((dessus #:score-template "score-voix")
   (haute-contre #:score-template "score-voix")
   (haute-contre-sol #:score-template "score-voix")
   (taille #:score-template "score-voix")
   (quinte #:score-template "score-voix")
   (basse #:score "score-basse")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Cybèle
    \livretVerse#12 { Atys, l’aimable Atys, malgré tous ses attraits, }
    \livretVerse#8 { Descend dans la nuit éternelle ; }
    \livretVerse#7 { Mais malgré la mort cruelle, }
    \livretVerse#5 { L’amour de Cybèle }
    \livretVerse#5 { Ne mourra jamais. }
    \livretVerse#8 { Sous une nouvelle figure, }
    \livretVerse#12 { Atys est ranimé par mon pouvoir divin ; }
    \livretVerse#8 { Célébrez son nouveau destin, }
    \livretVerse#8 { Pleurez sa funeste aventure. }
    \livretPers Chœur des nymphes des eaux et des divinités des bois
    \livretVerse#8 { Célébrons son nouveau destin, }
    \livretVerse#8 { Pleurons sa funeste aventure. }
    \livretPers Cybèle
    \livretVerse#6 { Que cet arbre sacré }
    \livretVerse#4 { Soit révéré }
    \livretVerse#6 { De toute la nature. }
    \livretVerse#12 { Qu’il s’élève au-dessus des arbres les plus beaux : }
    \livretVerse#12 { Qu’il soit voisin des cieux, qu’il règne sur les eaux ; }
    \livretVerse#12 { Qu’il ne puisse brûler que d’une flamme pure. }
    \livretVerse#6 { Que cet Arbre sacré }
    \livretVerse#4 { Soit révéré }
    \livretVerse#6 { De toute la nature. }
    \livretDidasP\wordwrap { Le chœur répète ces trois derniers vers. }
    \livretPers Cybèle
    \livretVerse#8 { Que ses rameaux soient toujours verts : }
  }
  \column {
    \null
    \livretVerse#8 { Que les plus rigoureux hivers }
    \livretVerse#8 { Ne leur fassent jamais d’injure. }
    \livretVerse#6 { Que cet arbre sacré }
    \livretVerse#4 { Soit révéré }
    \livretVerse#6 { De toute la nature. }
    \livretDidasP\wordwrap { Le chœur répète ces trois derniers vers. }
    \livretPers Cybèle et le chœur des divinités des bois et des eaux
    \livretVerse#4 { Quelle douleur ! }
    \livretPers Cybèle et le chœur des corybantes
    \livretVerse#4 { Ah ! quelle rage ! }
    \livretPers Cybèle et les chœurs
    \livretVerse#4 { Ah ! quel malheur ! }
    \livretPers Cybèle
    \livretVerse#8 { Atys au printemps de son âge, }
    \livretVerse#6 { Périt comme une fleur }
    \livretVerse#5 { Qu’un soudain orage }
    \livretVerse#5 { Renverse et ravage. }
    \livretPers Cybèle et le chœur des divinités des bois et des eaux
    \livretVerse#4 { Quelle douleur ! }
    \livretPers Cybèle et le chœur des corybantes
    \livretVerse#4 { Ah ! quelle rage ! }
    \livretPers Cybèle et les chœurs
    \livretVerse#4 { Ah ! quel malheur ! }
  }
}#}))

%% Cybele
\tag #'(cybele basse) {
  A -- tys, l’ai -- mable A -- tys, mal -- gré tous ses at -- traits,
  des -- cend dans la nuit é -- ter -- nel -- le ;
  mais mal -- gré la mort cru -- el -- le,
  l’a -- mour de Cy -- bè -- le
  ne mour -- ra ja -- mais.
  Sous u -- ne nou -- vel -- le fi -- gu -- re,
  A -- tys est ra -- ni -- mé par mon pou -- voir di -- vin ;
  cé -- lé -- brez son nou -- veau des -- tin,
  pleu -- rez, pleu -- rez sa fu -- neste a -- ven -- tu -- re.
  Pleu -- rez, pleu -- rez sa fu -- neste a -- ven -- tu -- re.
}
% Choeur
\tag #'(choeur basse) {
  Cé -- lé -- brons son nou -- veau des -- tin,
  pleu -- rons, pleu -- rons sa fu -- neste a -- ven -- tu -- re.
  Cé -- lé -- brons son nou -- veau des -- tin,
  pleu -- rons, pleu -- rons sa fu -- neste a -- ven -- tu -- re.
}
% Cybele
\tag #'(cybele basse) {
  Que cet ar -- bre sa -- cré
  soit ré -- vé -- ré
  de tou -- te la na -- tu -- re.
  Qu’il s’é -- lève au- des -- sus des ar -- bres les plus beaux :
  qu’il soit voi -- sin des cieux, qu’il rè -- gne sur les eaux ;
  qu’il ne puis -- se brû -- ler que d’u -- ne flam -- me pu -- re.
  Que cet ar -- bre sa -- cré
  soit ré -- vé -- ré
  de tou -- te la na -- tu -- re.
}
% Choeur
\tag #'(choeur basse) {
  Que cet ar -- bre sa -- cré
  soit ré -- vé -- ré
  de tou -- te la na -- tu -- re.
}
% Cybele
\tag #'(cybele basse) {
  Que ses ra -- meaux soient tou -- jours verts :
  que les plus ri -- gou -- reux hi -- vers
  ne leur fas -- sent ja -- mais d’in -- ju -- re.
  Que cet ar -- bre sa -- cré
  soit ré -- vé -- ré
  de tou -- te la na -- tu -- re.
}
% Choeur
\tag #'(choeur basse) {
  Que cet ar -- bre sa -- cré
  soit ré -- vé -- ré
  de tou -- te la na -- tu -- re.
}
% Cybele
\tag #'(cybele basse) {
  Quel -- le dou -- leur !
}
% Choeur
\tag #'(choeur basse) {
  Quel -- le dou -- leur !
}
% Cybele
\tag #'(cybele basse) {
  Ah ! quel -- le ra -- ge !
}
% Choeur
\tag #'(choeur basse) {
  Ah ! quel -- le ra -- ge !
}
% Cybele
\tag #'(cybele basse) {
  Quel -- le dou -- leur !
}
% Choeur
\tag #'(choeur basse) {
  Ah ! quel -- le ra -- ge !
  Ah ! quel -- le ra -- ge !
}
% Cybele
\tag #'(cybele basse) {
  Ah ! __ quel mal -- heur !
}
% Choeur
\tag #'(choeur basse) {
  Ah ! quel -- le ra -- ge !
  Ah ! Ah ! quel mal -- heur !
}
% Cybele
\tag #'(cybele basse) {
  A -- tys au prin -- temps de son â -- ge,
  pé -- rit comme u -- ne fleur
  qu’un sou -- dain o -- ra -- ge
  ren -- verse et ra -- va -- ge.
  
  Quel -- le dou -- leur !
}
% Choeur
\tag #'(choeur basse) {
  Quel -- le dou -- leur !
}
% Cybele
\tag #'(cybele basse) {
  Ah ! quel -- le ra -- ge !
}
% Choeur
\tag #'(choeur basse) {
  Ah ! quel -- le ra -- ge !
}
% Cybele
\tag #'(cybele basse) {
  Quel -- le dou -- leur !
}
% Choeur
\tag #'(choeur basse) {
  Ah ! quel -- le ra -- ge !
  Ah ! quel -- le ra -- ge !
}
% Cybele
\tag #'(cybele basse) {
  Ah ! __ quel mal -- heur !
}
% Choeur
\tag #'(choeur basse) {
  Ah ! quel -- le ra -- ge !
  Ah ! Ah ! quel mal -- heur !
}

\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff << \global \keepWithTag #'basse \includeNotes "basse" >>
    \new Staff \with { \tinyStaff } \withTinyLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
}

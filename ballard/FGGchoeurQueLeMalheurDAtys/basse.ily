\clef "basse" <<
  \tag #'basse {
    R1*2 |
    r2 r4 fa8 fa |
    sib4 sib8 sib sol4. sol8 |
    la la fa re la,4 |
    re r r2 |
    R1 |
    r2 r4 sol8 sol |
    do'4 do'8 do' fa4. re8 |
    sol sol mi do sol,4 |
    do2 r |
    R1 |
    R1.*2 |
    R1 |
    R1. |
    r2 r8 sol sol sol |
    do'4 re'8 do' sib la sol fa |
    mi4. mi8 mi4. mi8 |
    fa2 fa4 fa8 fa |
    re2 re4 re |
    la2. la4 |
    fa8 mi fa sol la si la si |
    do'4. do'8 do'4. do8 |
    sol2 r8 sol sol sol |
    re'4 mi'8 re' do' si la sol |
    fad sol fad mi re mi re do |
    si,4. si,8 si,4. si,8 |
    mi2 mi4 mi8 mi |
    do2 do4 do |
    fa2. fa4 |
    re8 do re mi fa mi fa re |
    mi4. mi8 mi4. mi8 |
    la,2 r8 la la la |
    re'4. re'8 re'4. re'8 |
    sol2 sol4 sol8 sol |
    mi2 mi4 mi |
    fa2. re4 |
    sol4 la8 sol fa mi re do |
    si,4. si,8 do4. do8 |
    sol,2 r |
    R1 |
    r2 r4 fa8 fa |
    sib4 sib8 sib sol4. sol8 |
    la8 la fa re la,4 |
    re r r2 |
    R1 |
    r2 r4 sol8 sol |
    do'4 do'8 do' fa4. re8 |
    sol sol mi do sol,4 |
    do2 r |
    R1 |
    r2 r4 sol8 sol |
    do'4 do'8 do' fa4. re8 |
    sol8 sol mi do sol,4 |
    do2. |
  }
  \tag #'(basse-continue tous) {
    do2. do'4 |
    mi1 |
    fa2 fa4 fa8 fa |
    sib2 sol |
    la4 fa8 re la,4 |
    re2. do4 |
    si,2 fad,\trill |
    sol,2. sol4 |
    do'2 fa4. re8 |
    sol4 mi8 do sol,4 |
    do1 |
    fa2. re4 |
    mi2 la,4 sol, fad,\trill sol, |
    re,2 re sib, |
    do1 |
    re1. |
    sol,2 r8 sol sol sol |
    do'4 re'8 do' sib la sol fa |
    mi4. mi8 mi4. mi8 |
    fa2. fa4 |
    re2.\trill re4 |
    la2. la4 |
    fa8 mi fa sol la si la si |
    do'2. do4 |
    sol2. sol4 |
    re'4 mi'8 re' do' si la sol |
    fad sol fad mi re mi re do |
    si,2. si,4 |
    mi2. mi4 |
    do2. do4 |
    fa2. fa4 |
    re2. re4 |
    mi2 mi, |
    la,2. la4 |
    re'2. re'4 |
    sol2 sol4 sol8 sol |
    mi2.\trill mi4 |
    fa2. re4 |
    sol la8 sol fa mi re do |
    si,2\trill do4. do8 |
    sol,2. sol8 fa |
    mi1 |
    fa2 fa4. fa8 |
    sib4. sib8 sol2 |
    la4 fa8 re la,4 |
    re2. do4 |
    si,2 fad, |
    sol,2. sol4 |
    do'2 fa4. re8 |
    sol4 mi8 do sol,4 |
    do2 do'4 mi |
    fa2 re |
    sol2. sol4 |
    do'2 fa4. re8 |
    sol4 mi8 do sol,4 |
    do2. |
  }
>>

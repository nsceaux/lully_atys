\tag #'(vdessus basse) {
  Que le mal -- heur d’A -- tys af -- fli -- ge tout le mon -- de.
}
Que tout sente, i -- ci bas,
l’hor -- reur d’un si cru -- el tré -- pas.
\tag #'(vdessus basse) {
  Que le mal -- heur d’A -- tys af -- fli -- ge tout le mon -- de.
}
Que tout sente, i -- ci bas,
l’hor -- reur d’un si cru -- el tré -- pas.
\tag #'(vdessus basse) {
  Pé -- né -- trons tous les cœurs d’u -- ne dou -- leur pro -- fon -- de :
  que les bois, que les eaux, per -- dent tous leurs ap -- pas.
}

\tag #'(vdessus basse vbasse) {
  Que le ton -- ner -- re nous ré -- pon -- de :
  que la ter -- re fré -- misse, et trem -- ble sous nos pas.
}
\tag #'(vhaute-contre vtaille) {
  Que le ton -- ner -- re nous ré -- pon -- de, nous ré -- pon -- de :
  que la ter -- re fré -- misse, et trem -- ble, et trem -- ble sous nos pas.
}
\tag #'(vdessus vhaute-contre vtaille basse) {
  Que le ton -- ner -- re nous ré -- pon -- de,
}
\tag #'(vdessus vbasse basse) {
  Que le ton -- ner -- re nous ré -- pon -- de :
}
\tag #'(vhaute-contre vtaille) {
  nous ré -- pon -- de, nous ré -- pon -- de :
}
que la ter -- re fré -- misse,
\tag #'vhaute-contre { et tremble, }
et trem -- ble sous nos pas.
Que le ton -- ner -- re nous ré -- pon -- de :
\tag #'(vdessus vtaille basse) {
  que la ter -- re fré -- mis -- se,
  que la ter -- re fré -- misse, et trem -- ble sous nos pas.
}
\tag #'vhaute-contre {
  que la ter -- re fré -- misse, et tremble, et trem -- ble sous nos pas.
}
\tag #'vbasse {
  que la ter -- re fré -- misse, et trem -- ble sous nos pas.
}
\tag #'(vdessus basse) {
  Que le mal -- heur d’A -- tys af -- fli -- ge tout le mon -- de.
}
Que tout sente, i -- ci bas,
l’hor -- reur d’un si cru -- el tré -- pas.
\tag #'(vdessus basse) {
  Que le mal -- heur d’A -- tys af -- fli -- ge tout le mon -- de.
}
Que tout sente, i -- ci bas,
l’hor -- reur d’un si cru -- el tré -- pas.
Que le mal -- heur d’A -- tys af -- fli -- ge tout le mon -- de.
Que tout sente, i -- ci bas,
l’hor -- reur d’un si cru -- el tré -- pas.

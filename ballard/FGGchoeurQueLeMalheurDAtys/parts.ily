\piecePartSpecs
#`((dessus)
   (haute-contre)
   (haute-contre-sol)
   (taille)
   (quinte)
   (basse #:score "score-basse")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Cybèle et le chœur des divinités des bois et des eaux
    \livretVerse#12 { Que le malheur d’Atys afflige tout le monde. }
    \livretPers Cybèle et le chœur des Corybantes
    \livretVerse#6 { Que tout sente, ici bas, }
    \livretVerse#8 { L’horreur d’un si cruel trépas. }
    \livretPers Cybèle et le chœur des divinités des bois et des eaux
    \livretVerse#12 { Pénétrons tous les cœurs d’une douleur profonde : }
    \livretVerse#12 { Que les bois, que les eaux, perdent tous leurs appas. }
  }
  \column {
    \livretPers Cybèle et le chœur des Corybantes
    \livretVerse#8 { Que le tonnerre nous réponde : }
    \livretVerse#12 { Que la terre frémisse, et tremble sous nos pas. }
    \livretPers Cybèle et le chœur des divinités des bois et des eaux
    \livretVerse#12 { Que le malheur d’Atys afflige tout le monde. }
    \livretPers Tous ensemble
    \livretVerse#6 { Que tout sente, ici bas, }
    \livretVerse#8 { L’horreur d’un si cruel trépas. }
  }
}#}))

\score {
  \new StaffGroup <<
    \new Staff <<
      { s1*14 s2
        \footnoteHere #'(0 . 0) \markup\justify {
          [Manuscrit] et [Philidor 1703] Reprise des quatre dernières mesures.
        }
      }
      \global \includeNotes "dessus"
    >>
    \new Staff << \global \includeNotes "haute-contre" >>
    \new Staff << \global \includeNotes "taille" >>
    \new Staff << \global \includeNotes "quinte" >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
  \midi { }
}

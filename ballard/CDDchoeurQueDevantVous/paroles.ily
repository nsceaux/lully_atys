Que de -- vant vous tout s’a -- baisse, et tout trem -- ble ;
vi -- vez heu -- reux, vos jours sont notre es -- poir :
rien n’est si beau que de voir en -- sem -- ble
un grand mé -- rite a -- vec un grand pou -- voir.
Que l’on bé -- nis -- se
le ciel pro -- pi -- ce,
qui dans vos mains
met le sort des hu -- mains.

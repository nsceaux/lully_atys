\piecePartSpecs
#`((dessus #:score "score-dessus")
   (haute-contre)
   (haute-contre-sol)
   (taille)
   (quinte)
   (basse #:score "score-basse")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 {
\livretVerse#10 { Que devant vous tout s’abaisse, et tout tremble ; }
\livretVerse#10 { Vivez heureux, vos jours sont notre espoir : }
\livretVerse#9 { Rien n’est si beau que de voir ensemble }
\livretVerse#10 { Un grand mérite avec un grand pouvoir. }
\livretVerse#4 { Que l’on bénisse }
\livretVerse#4 { Le Ciel propice, }
\livretVerse#4 { Qui dans vos mains }
\livretVerse#6 { Met le sort des humains. }
}#}))

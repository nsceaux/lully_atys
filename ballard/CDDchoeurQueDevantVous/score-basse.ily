\score {
  \new StaffGroup <<
    \new Staff << \global \keepWithTag #'basse \includeNotes "basse" >>
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      { s2.*8\break s2.*8\break }
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
}

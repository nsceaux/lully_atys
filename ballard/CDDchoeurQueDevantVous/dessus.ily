\clef "dessus" re''4 re'' fa'' |
sib' sib' mib'' |
do''\trill do'' fa'' |
re''2\trill sib'4 |
do'' do'' re'' |
sib'4.\trill sib'8 do''4 |
re'' do''4.\trill re''8 |
re''2. |
re''4 re'' mi'' |
fa''2 fa''4 |
fa''4 mi''4.\trill %{ re''16 mi'' %} mi''8 |
fa''2 do''4 |
fa'' re'' sol'' |
mi'' fa'' re'' |
mi'' dod''4.\trill re''8 |
re''2. |
re''4 sol'' re'' |
mib''2 do''4 |
do'' fa'' do'' |
re''2 sib'4 |
do'' do'' re'' |
mib''4. re''8 do''4 |
sib'4 la'4.\trill sol'8 |
sol'2. |

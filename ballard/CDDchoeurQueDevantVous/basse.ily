\clef "basse" sol4 sol re |
\twoVoices #'(basse basse-continue tous) <<
  { mib4 mib do | fa fa fa | }
  { mib2 do4 | fa2 fa,4 | }
>>
sib,2 sib,4 |
\twoVoices #'(basse basse-continue tous) <<
  { fa4 fa re |
    sol4. sol8 sol4 |
    fa mib4. %{ re8 %} mib8 | }
  { fa2 re4 |
    sol2 sol4 |
    fa mib2\trill | }
>>
re2. |
sib4 sib sib |
la2\trill sib4 |
sol do' do |
fa2 fa4 |
re sol mi |
la fa sib |
sol la la, |
re2. |
sol4 sol sol |
do2 do4 |
fa fa fa |
sib,2 sib,4 |
fa mib re |
do sib, la, |
sol, re re, |
sol,2. |

\clef "taille" fa'2 fa'4 sib' |
sol'2 fa'4. fa'8 |
fa'2 sib'4. sib'8 |
la'4. la'8 la'2 |
sol' sol'4. sol'8 |
fa'2 fa'4 re' |
re'4. re'8 re'4 sib |
sib4. re'8 re'4. do'8 |
sib2. re'4 |
mib'4. sol'8 sol'4. sol'8 |
fa'2 fa'4. fa'8 |
fa'2 sol' |
sol' lab'4. lab'8 |
sol'4. sol'8 sol'4. fa'8 |
fa'1 |
fa'2. fa'4 |
sol'2 sol'4. sol'8 |
lab'2. fa'4 |
re'4. re'8 re'4. mib'8 |
fa'4 sol'2 sol'4 |
sol' fa' re'4. sol'8 |
mi'4 r16 mi' fa' sol' la'4. la'8 |
fa'2 fa'4. fa'8 |
sib4 r16 sib do' re' mib'4. mib'8 |
do'4 r16 do' re' mib' fa'4. fa'8 |
re'4 mib' do'4. sib8 |
sib4 fa' fa'4. fa'8 |
fa'2. fa'4 |
mib'2. sol'4 |
fa' fa' fa'4. mib'8 |
re'1 |

\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Morphée
    \livretVerse#12 { Écoute, écoute Atys la gloire qui t’appelle, }
    \livretVerse#12 { Sois sensible à l’honneur d’être aimé de Cybèle, }
    \livretVerse#12 { Jouis heureux Atys de ta félicité. }
    \livretPers Morphée, Phobétor et Phantase
    \livretVerse#8 { Mais souviens-toi que la beauté }
    \livretVerse#6 { Quand elle est immortelle, }
    \livretVerse#8 { Demande la fidelité }
    \livretVerse#6 { D’une amour éternelle. }
  }
  \column {
    \null
    \livretPers Phantase
    \livretVerse#6 { Que l’amour a d’attraits }
    \livretVerse#4 { Lorsqu’il commence }
    \livretVerse#8 { À faire sentir sa puissance, }
    \livretVerse#6 { Que l’Amour a d’attraits }
    \livretVerse#4 { Lorsqu’il commence }
    \livretVerse#6 { Pour ne finir jamais. }
  }
}#}))

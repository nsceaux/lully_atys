\tag #'(morphee basse) {
  % Morphée
  É -- coute, é -- coute A -- tys la gloi -- re qui t’ap -- pel -- le,
  sois sen -- sible à l’hon -- neur d’être ai -- mé de Cy -- bè -- le,
  jou -- is heu -- reux A -- tys de ta fé -- li -- ci -- té.
}
\tag #'(morphee phantase phobetor basse) {
  % Morphée, Phobetor, et Phantase.
  Mais sou -- viens- toi que la beau -- té
  quand elle est im -- mor -- tel -- le,
  de -- man -- de la fi -- de -- li -- té
  d’une a -- mour é -- ter -- nel -- le.
}
\tag #'(phantase basse) {
  % Phantase
  Que l’a -- mour a d’at -- traits
  lors -- qu’il com -- men -- ce
  à fai -- re sen -- tir sa puis -- san -- ce,
  que l’A -- mour a d’at -- traits
  lors -- qu’il com -- men -- ce
  pour ne fi -- nir ja -- mais.
}

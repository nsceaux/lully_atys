\tag #'(phobetor basse) {
  % Phobetor
  Goûte en paix cha -- que jour u -- ne dou -- ceur nou -- vel -- le,
  par -- ta -- ge l’heu -- reux sort d’u -- ne di -- vi -- ni -- té,
  ne van -- te plus la li -- ber -- té,
  il n’en est point du prix d’u -- ne chaî -- ne si bel -- le.
}
\tag #'(morphee phantase phobetor basse) {
  % Morphée, Phobetor, et Phantase.
  Mais sou -- viens- toi que la beau -- té
  quand elle est im -- mor -- tel -- le,
  de -- man -- de la fi -- dé -- li -- té
  d’une a -- mour é -- ter -- nel -- le.
}
\tag #'(phantase basse) {
  % Phantase
  Trop heu -- reux un a -- mant
  qu’A -- mour ex -- emp -- te
  des pei -- nes d’u -- ne longue at -- ten -- te !
  Trop heu -- reux un a -- mant
  qu’A -- mour ex -- emp -- te
  de crainte, et de tour -- ment !
}


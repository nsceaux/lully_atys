\clef "dessus" r2 re''4 sol'' |
sol''2 fa''4.\trill fa''8 |
fa''2 fa''4. fa''8 |
sib'4 sib'2 la'4\trill |
sib'2. fa''4 |
fad''4.\trill fad''8 fad''4 sol'' |
la''4. la''8 re''4 sib''8 la'' sol''4 la'' |
fad''2\trill re''4. mi''8 |
fad''4\trill fad'' sol'' sol''8 fa''? |
mi''4.\trill fa''8 sol''4 mi'' |
fa''2 re''4.\trill re''8 sol'' fa'' mi'' re'' |
dod''2\trill mi''4. mi''8 |
fa''4 la'' sol'' fa'' |
mi''4.\trill fad''8 sol''4. la''8 |
fad''2\trill r |
\ru#5 { \allowPageTurn R1 } |
\allowPageTurn R1. |
\ru#3 { \allowPageTurn R1 } |
\ru#17 { \allowPageTurn R2. }

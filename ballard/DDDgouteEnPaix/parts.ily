\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line{
  \column {
    \livretPers Morphée
    \livretVerse#12 { Goûte en paix chaque jour une douceur nouvelle, }
    \livretVerse#12 { Partage l’heureux sort d’une divinité, }
    \livretVerse#8 { Ne vante plus la liberté, }
    \livretVerse#12 { Il n’en est point du prix d’une chaîne si belle : }
    \livretPers Morphée, Phobétor et Phantase
    \livretVerse#8 { Mais souviens-toi que la beauté }
    \livretVerse#6 { Quand elle est immortelle, }
    \livretVerse#8 { Demande la fidélité }
    \livretVerse#6 { D’une amour éternelle. }
  }
  \column {
    \null
    \livretPers Phantase
    \livretVerse#6 { Trop heureux un amant }
    \livretVerse#4 { Qu’Amour exempte }
    \livretVerse#8 { Des peines d’une longue attente ! }
    \livretVerse#6 { Trop heureux un amant }
    \livretVerse#4 { Qu’Amour exempte }
    \livretVerse#6 { De crainte, et de tourment ! }
  }
}#}))

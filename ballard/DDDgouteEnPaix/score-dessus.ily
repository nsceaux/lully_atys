\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> { \set fontSize = -2 \keepWithTag #'basse \includeLyrics "paroles" }
    \new GrandStaff <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
  >>
  \layout { }
}

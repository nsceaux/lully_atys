%% ACTE 5 SCENE 6
\tag #'(voix1 basse) {
  vois !
  
  Il s’est per -- cé le sein, et mes soins pour sa vi -- e
  n’ont pu pré -- ve -- nir sa fu -- reur.
  
  Ah c’est ma bar -- ba -- ri -- e,
  c’est moi, qui lui per -- ce le cœur.
  
  Je meurs, l’a -- mour me gui -- de
  dans la nuit du tré -- pas ;
  je vais où se -- ra San -- ga -- ri -- de,
  in -- hu -- mai -- ne, je vais, où vous ne se -- rez pas.
  
  A -- tys, il est trop vrai, ma ri -- gueur est ex -- trê -- me,
  plai -- gnez- vous, je veux tout souf -- frir.
  Pour -- quoi suis-je im -- mor -- telle en vous voy -- ant pé -- rir ?
}

Il est doux de mou -- rir
a -- vec ce que l’on ai -- me.

\tag #'(voix1 basse) {
  Que mon a -- mour fu -- neste ar -- mé con -- tre moi- mê -- me,
  ne peut- il vous ven -- ger de tou -- tes mes ri -- gueurs.

  Je suis as -- sez ven -- gé, vous m’ai -- mez, et je meurs.
  
  Mal -- gré le des -- tin im -- pla -- ca -- ble
  qui rend de ton tré -- pas l’ar -- rêt ir -- ré -- vo -- ca -- ble,
  A -- tys, sois à ja -- mais l’ob -- jet de mes a -- mours :
  re -- prends un sort nou -- veau, de -- viens un arbre ai -- ma -- ble
  que Cy -- bèle ai -- me -- ra __ tou -- jours.
}

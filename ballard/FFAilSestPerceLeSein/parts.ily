\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix"
          #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPersDidas Idas soûtenant Atys
    \livretVerse#12 { Il s’est percé le sein, et mes soins pour sa vie }
    \livretVerse#8 { N’ont pu prévenir sa fureur. }
    \livretPers Cybèle
    \livretVerse#6 { Ah c’est ma barbarie, }
    \livretVerse#8 { C’est moi, qui lui perce le cœur. }
    \livretPers Atys
    \livretVerse#6 { Je meurs, l’amour me guide }
    \livretVerse#6 { Dans la nuit du trépas ; }
    \livretVerse#8 { Je vais où sera Sangaride, }
    \livretVerse#12 { Inhumaine, je vais, où vous ne serez pas. }
    \livretPers Cybèle
    \livretVerse#12 { Atys, il est trop vrai, ma rigueur est extrême, }
    \livretVerse#8 { Plaignez-vous, je veux tout souffrir. }
    \livretVerse#12 { Pourquoi suis-je immortelle en vous voyant périr ? }
  }
  \column {
    \livretPers Atys et Cybèle
    \livretVerse#6 { Il est doux de mourir }
    \livretVerse#6 { Avec ce que l’on aime. }
    \livretPers Cybèle
    \livretVerse#12 { Que mon amour funeste armé contre moi-même, }
    \livretVerse#12 { Ne peut-il vous venger de toutes mes rigueurs. }
    \livretPers Atys
    \livretVerse#12 { Je suis assez vengé, vous m’aimez, et je meurs. }
    \livretPers Cybèle
    \livretVerse#8 { Malgré le destin implacable }
    \livretVerse#12 { Qui rend de ton trépas l’arrêt irrévocable, }
    \livretVerse#12 { Atys, sois à jamais l’objet de mes amours : }
    \livretVerse#12 { Reprends un sort nouveau, deviens un arbre aimable }
    \livretVerse#8 { Que Cybèle aimera toujours. }
  }
}#}))

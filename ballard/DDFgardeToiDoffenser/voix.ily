\songeFunesteMark r4 sib8 sib re4\trill re8 mib |
fa4 fa8. sol16 lab4 lab8. sol16 |
sol4\trill mib8 fa sol4\trill sol8 la |
sib4 sib8 sib si4 si8 do' |
do'2 r8 mi mi mi |
fa4. fa8 fa4 mi |
fa fa r do8 re |
mib!4 mib8 fa sol4 sol8 la |
sib4. la16 sol fad4\trill r8 re' |
sib8. la16 sol8. fa16 mi4\trill r8 do' |
la8.\trill la16 la8 do' fa4 fa8 fa re8.\trill re16 re8 fa sib,4 sib, |
re8. re16 mib8. fa16 sol4 la8 sib |
la4\trill la8 fa16 fa sib4 sib8 la |
sib1 |

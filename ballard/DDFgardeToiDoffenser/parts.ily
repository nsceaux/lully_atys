\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \column {
\livretPers Un songe funeste
\livretVerse#12 { Garde-toi d’offenser un amour glorieux, }
\livretVerse#12 { C’est pour toi que Cybèle abandonne les cieux }
\livretVerse#8 { Ne trahis point son espérance. }
\livretVerse#12 { Il n’est point pour les Dieux de mépris innocent, }
\livretVerse#12 { Ils sont jaloux des cœurs, ils aiment la vengeance, }
\livretVerse#8 { Il est dangereux qu’on offense }
\livretVerse#6 { Un amour tout-puissant. }
} #}))

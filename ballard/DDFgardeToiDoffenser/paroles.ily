Gar -- de- toi d’of -- fen -- ser un a -- mour glo -- ri -- eux,
c’est pour toi que Cy -- bèle a -- ban -- don -- ne les cieux,
ne tra -- his point son es -- pé -- ran -- ce.
Il n’est point pour les dieux de mé -- pris in -- no -- cent,
ils sont ja -- loux des cœurs, ils ai -- ment la ven -- gean -- ce,
ils ai -- ment la ven -- gean -- ce,
il est dan -- ge -- reux qu’on of -- fen -- se
un a -- mour tout- puis -- sant.

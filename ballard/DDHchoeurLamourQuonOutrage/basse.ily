\clef "basse" sib,2 sib,4 sib, sib, sib, |
mib2 mib4 mib mib mib |
do2\trill do4 do do do |
re2 sib,4 do re2 |
sol,2 sol4 sol sol sol |
mi2\trill mi4 mi mi mi |
fa2 fa4 fa re re |
mib2 mib4 do fa fa, |
sib,1. |
sib2 sib4 sib sib sib |
fa2 fa4 fa fa re |
mib2 mib mib4 mib |
sib2 sib4 sib sib sib |
fa2. fa4 fa fa |
do2. do4 do do |
sol,1. |
sol4 sol8 sol sol4 sol sol sol |
do2 do r |
fa1 fa2 |
do'1 do'2 |
fa2 fa4 fa fa do |
fa1. |
do2 do4 do2 do4 |
sol2 sol r |
la1\trill la2 |
sib1 sib2 |
mib mib4 do fa fa, |
sib,1. |

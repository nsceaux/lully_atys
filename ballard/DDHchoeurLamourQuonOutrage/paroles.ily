% Choeur De Songes Funestes
\ru#2 {
  L’a -- mour qu’on ou -- tra -- ge
  se trans -- forme en rage
  et ne par -- don -- ne pas
  aux plus char -- mants ap -- pas.
}
Si tu n’ai -- mes point Cy -- bè -- le
d’une a -- mour fi -- dè -- le,
mal -- heu -- reux, que tu souf -- fri -- ras !
tu pé -- ri -- ras, tu pé -- ri -- ras :
\ru#2 {
  Crains u -- ne ven -- gean -- ce cru -- el -- le,
  trem -- ble, trem -- ble, crains un af -- freux tré -- pas.
}

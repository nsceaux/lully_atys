<<
  \tag #'(vhaute-contre basse) {
    \clef "vhaute-contre" <>^\markup\character "Chœur des Songes Funestes"
    r2 fa'4 fa' fa' fa' |
    mib' mib' mib' mib' mib' mib' |
    mib' mib' mib' mib' mib' mib' |
    re' re' re' mib' re'4. re'8 |
    re'2 re'4 re' re' re' |
    mi' mi' mi' fa' sol' sol' |
    fa' fa' fa' fa' fa' fa' |
    mib' mib' mib' sol'4 fa'4. fa'8 |
    fa'1. |
    fa'4 fa' fa' fa' fa' fa' |
    fa' fa' fa' fa' fa' fa' |
    mib'2 mib' sol'4 sol' |
    fa'2 fa'4 fa' fa' fa' |
    fa'2. do'4 do' re' |
    mib'2. mib'4 mib' mib' |
    re'1. |
    sol'4 sol'8 sol' sol'4 sol' sol' sol' |
    sol'2 sol' r |
    la'1 la'2 |
    sol'1 sol'2 |
    fa' fa'4 fa' fa' mi' |
    fa'1. |
    sol'4 sol'8 sol' sol'4 sol' sol' sol' |
    sol'2 sol' r |
    fa'1 fa'2 |
    fa'1 fa'2 |
    sol' sol'4 sol' fa'4. fa'8 |
    fa'1. |
  }
  \tag #'vtaille1 {
    \clef "vtaille" r2 re'4 re' re' re' |
    sib sib sib sib sib sib |
    do' do' do' do' do' do' |
    la la sib sib la4. re'8 |
    si2\trill si4 si si si |
    do' do' do' do' do' do' |
    do' do' do' do' re' re' |
    sib sib sib mib' do'4. fa'8 |
    re'1.\trill |
    re'4 re' re' re' re' re' |
    do' do' do' do' do' re' |
    sib2 sib mib'4 mib' |
    re'2\trill re'4 re' re' re' |
    do'2.\trill la4 la si |
    do'2. do'4 do' do' |
    sib1. |
    re'4 re'8 re' re'4 re' re' re' |
    mi'2 mi' r |
    fa'1 fa'2 |
    mi'1 mi'2 |
    do'2 do'4 do' do' do' |
    do'1. |
    mib'4 mib'8 mib' mib'4 mib' mib' mib' |
    re'2\trill re' r |
    do'1 do'2 |
    re'1 re'2 |
    mib'2 mib'4 mib' do'4. fa'8 |
    re'1.\trill |
  }
  \tag #'vtaille2 {
    \clef "vtaille" r2 sib4 sib sib sib |
    sol sol sol sol sol sol |
    la la la la la la |
    fad fad sol sol fad4.\trill sol8 |
    sol2 sol4 sol sol sol |
    sol sol sol la sib do' |
    la la la la sib sib |
    sol sol sol do' la4. sib8 |
    sib1. |
    sib4 sib sib sib sib sib |
    la\trill la la la la sib |
    sol2\trill sol sib4 sib |
    sib2 sib4 sib sib sib |
    la2.\trill fa4 fa fa |
    sol2. sol4 sol sol |
    sol1. |
    si4 si8 si si4 si si si |
    do'2 do' r |
    do'1 do'2 |
    do'1 do'2 |
    la2 la4 la la sol |
    la1. |
    do'4 do'8 do' do'4 do' do' do' |
    sib2 sib r |
    do'1 do'2 |
    sib1 sib2 |
    sib sib4 do' la4. sib8 |
    sib1. |
  }
  \tag #'vbasse {
    \clef "vbasse" r2 sib,4 sib, sib, sib, |
    mib mib mib mib mib mib |
    do do do do do do |
    re re sib, do re4. re8 |
    sol,2 sol4 sol sol sol |
    mi mi mi mi mi mi |
    fa fa fa fa re re |
    mib mib mib do fa4. fa8 |
    sib,1. |
    sib4 sib sib sib sib sib |
    fa fa fa fa fa re |
    mib2 mib mib4 mib |
    sib2 sib4 sib sib sib |
    fa2. fa4 fa fa |
    do2. do4 do do |
    sol,1. |
    sol4 sol8 sol sol4 sol sol sol |
    do2 do r |
    fa1 fa2 |
    do'1 do'2 |
    fa fa4 fa fa do |
    fa1. |
    do4 do8 do do4 do do do |
    sol2 sol r |
    la1 la2 |
    sib1 sib2 |
    mib2 mib4 do fa4. fa8 |
    sib,1. |
  }
>>

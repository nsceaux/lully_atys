\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'vhaute-contre \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'vtaille1 \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'vtaille2 \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'vbasse \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
  \midi { }
}

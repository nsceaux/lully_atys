\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \column {
\livretPers Chœur de songes funestes
\livretVerse#5 { L’amour qu’on outrage }
\livretVerse#5 { Se transforme en rage, }
\livretVerse#6 { Et ne pardonne pas }
\livretVerse#6 { Aux plus charmants appas. }
\livretVerse#7 { Si tu n’aimes point Cybèle }
\livretVerse#5 { D’une amour fidèle, }
\livretVerse#8 { Malheureux, que tu souffriras ! }
\livretVerse#4 { Tu périras : }
\livretVerse#8 { Crains une vengeance cruelle, }
\livretVerse#8 { Tremble, crains un affreux trépas. }
} #}))

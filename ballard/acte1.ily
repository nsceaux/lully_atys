\newBookPart #'()
\act "Acte Premier"
\sceneDescription\markup\wordwrap-center {
  [Le théâtre représente une montagne consacrée à Cybèle.]
}
\scene "Scène première" "SCÈNE 1 : Atys"
\sceneDescription\markup\smallCaps Atys.
%% 1-1
\pieceToc\markup\wordwrap { Air : \italic { Allons, allons, accourez tous } }
\includeScore "BAAairAllonsAllons"

\scene "scène II" "SCÈNE 2 : Atys, Idas"
\sceneDescription\markup\smallCaps { Atys, Idas. }
%% 1-2
\pieceToc\markup\wordwrap {
  Air, récit : \italic { Allons, allons, accourez tous }
}
\includeScore "BBAairAllonsAllons"

\scene "Scène III" "SCÈNE 3 : Sangaride, Doris, Atys, Idas"
\sceneDescription\markup\smallCaps { Sangaride, Doris, Atys, Idas. }
%% 1-3
\pieceToc\markup\wordwrap { Air : \italic { Allons, allons, accourez tous } }
\includeScore "BCAairAllonsAllons"

\scene "Scène IV" "SCÈNE 4 : Sangaride, Doris"
\sceneDescription\markup\smallCaps { Sangaride, Doris. }
%% 1-4
\pieceToc\markup\wordwrap { Air, récit : \italic { Atys est trop heureux } }
\includeScore "BDAatysEstTropHeureux"

\scene "Scène V" "SCÈNE 5 : Sangaride, Doris, Atys"
\sceneDescription\markup\smallCaps { Sangaride, Doris, Atys. }
%% 1-5
\pieceToc\markup\wordwrap { Récit : \italic { On voit dans ces campagnes } }
\includeScore "BEArecitOneVoitDansCesCampagnes"

\scene "Scène VI" "SCÈNE 6 : Sangaride, Atys"
\sceneDescription\markup \smallCaps { Sangaride, Atys. }
%% 1-6
\pieceToc\markup\wordwrap {
  Récit, air : \italic { Sangaride ce jour est un grand jour pour vous }
}
\includeScore "BFArecitSangarideCeJour"
\newBookPart#'(haute-contre haute-contre-sol taille quinte)

\scene "Scène VII" "SCÈNE 7 : Sangaride, Atys, chœur de Phrygiens"
\sceneDescription\markup\column {
  \wordwrap-center\smallCaps { Sangaride, Atys, les Phrygiens. }
  \justify {
    [Chœur de Phrygiens chantants. Chœur de Phrygiennes chantantes.
    Troupe de Phrygiens dansants. Troupe de Phrygiennes dansantes.]
  }
  \smaller\italic\justify {
    [Dix hommes phrygiens chantants conduits par Atys.
    Dix femmes phrygiennes chantantes conduites par Sangaride.
    Six Phrygiens dansants.
    Six nymphes phrygiennes dansantes.]
  }
}
%% 1-7
\pieceToc\markup\wordwrap {
  Air, chœur : \italic { Commençons de célébrer ici }
}
\includeScore "BGAairMaisDejaDeCeMontSacre"
\newBookPart #'(full-rehearsal)
%% 1-8
\pieceToc "Entrée de Phrygiens"
\includeScore "BGBEntreePhrygiens"
%% 1-9
\pieceToc "Second air des Phrygiens"
\includeScore "BGCSecondAirPhrygiens"
\newBookPart #'(full-rehearsal)

\scene "scène VIII" "SCÈNE 8 : Cybèle, chœur de Phrygiens"
\sceneDescription\markup\column {
  \wordwrap-center { \smallCaps Cybèle sur son char. }
  \justify {
    [La déesse Cybèle paraît sur son char, et les Phrygiens et les
    Phrygiennes lui témoignent leur joie et leur respect.]
  }
}
%% 1-10
\pieceToc "Prélude [pour Cybèle]"
\includeScore "BHApreludeCybele"
%% 1-11
\pieceToc\markup\wordwrap {
  Air, chœur : \italic { Vous devez vous animer }
}
\includeScore "BHBairChoeurVenezTousDansMonTemple"
\actEnd "FIN DU PREMIER ACTE"

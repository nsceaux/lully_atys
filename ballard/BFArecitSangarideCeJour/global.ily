\key la \minor
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.*3
\time 4/4 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\digitTime\time 3/4 s2.*2
\time 4/4 s1
\digitTime\time 3/4 s2.*5
\time 4/4 s1*10
\digitTime\time 3/4 s2.*6
\time 4/4 s1*6
\digitTime\time 3/4 s2.*2
\time 4/4 s1*4
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.*2
\time 4/4 s1*11
\digitTime\time 3/4 s2.*2
\time 4/4 s1*2
\digitTime\time 3/4 s2.*2
\time 4/4 s1*2
\digitTime\time 3/4 s2.*3
\time 2/2 \midiTempo#160 s1
\time 4/4 \midiTempo#80 s1*8
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 \midiTempo#120 s2.*36
\time 4/4 \midiTempo#80 s1*4
\digitTime\time 3/4 \midiTempo#120 s2.*20 \bar "|."

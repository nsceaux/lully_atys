\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix0 \includeNotes "voix"
    >> \keepWithTag #'voix0 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      { s1 s2.*3 s1*2 s2. s1*2 s2.*2 s1 s2.*5 s1*10
        s2.*6 s1*6 s2.*2 s1*4 s2.  s1 s2.*2 s1*11 s2.*2 s1*2 s2.*2 s1*2
        s2.*3 s1 s1*8 s2. s1 s2.*36 s1*4 s2.*7
        \footnoteHereFull #'(0 . 0) \markup\column {
          \wordwrap {
            [Manuscrit] les vers
            \italic { Aimons un bien plus durable que l’éclat de la beauté }
            sont chantés deux fois :
          }
          \score {
            \new StaffGroupNoBar <<
              \new Staff {
                \tinyQuote \clef "vhaute-contre" \digitTime\time 3/4
                r8 mi' do'4 la | re' re' si | do'2 la4 | r mi' fa' |
                sol'2 mi'4 | fa' re'2 | do'2. |
                r8 mi' do'4 la | re' re' si | do'2 la4 | r mi' fa' |
                sol'2 mi'4 | fa' re'2 | do' do'4 |
              } \addlyrics {
                Ai -- mons un bien plus du -- ra -- ble
                que l'é -- clat de la beau -- té.
                Ai -- mons un bien plus du -- ra -- ble
                que l'é -- clat de la beau -- "té :"
                rien
              }
              \new Staff <<
                { \clef "vbasse" la,2 la4~ | la sold2 | la4 la,4. si,8 | do2 re4 |
                  mi si, do | fa, sol,2 | do4. re8 do si, |
                  la,2 la4~ | la4 sold2 | la4 la,4. si,8 | do2 re4 |
                  mi4 si, do | fa, sol,2 | do do4 | }
                \figuremode { <_>2. <2 4>2. <_>2.*5  <_>2. <2 4>2. <_>2.*5 }
              >>
            >>
            \layout { indent = 0\cm }
          }
        }
      }
      \global \keepWithTag #'voix1 \includeNotes "voix"
    >> \keepWithTag #'voix1 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
  \midi { }
}
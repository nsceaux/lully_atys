%% ACTE 1 SCENE 6
\tag #'(voix1 basse) {
  San -- ga -- ri -- de, ce jour est un grand jour pour vous.

  Nous or -- don -- nons tous deux la fê -- te de Cy -- bè -- le,
  l’hon -- neur est é -- gal en -- tre nous.
  
  Ce jour même, un grand roi doit ê -- tre votre é -- poux,
  je ne vous vis ja -- mais si con -- tente et si bel -- le ;
  que le sort du roi se -- ra doux !
  
  L’in -- dif -- fé -- rent A -- tys n’en se -- ra point ja -- loux.
  
  Vi -- vez tous deux con -- tents, c’est ma plus chère en -- vi -- e ;
  j’ai pres -- sé votre hy -- men, j’ai ser -- vi vos a -- mours.
  Mais en -- fin ce grand jour, le plus beau de vos jours,
  se -- ra le der -- nier de ma vi -- e.
  
  O dieux !
  
  Ce n’est qu’à vous que je veux ré -- vé -- ler
  le se -- cret dé -- ses -- poir où mon mal -- heur me li -- vre ;
  je n’ai que trop su feindre, il est temps de par -- ler ;
  qui n’a plus qu’un mo -- ment à vi -- vre,
  n’a plus rien à dis -- si -- mu -- ler.
  
  Je fré -- mis, ma crainte est ex -- trê -- me ;
  A -- tys, par quel mal -- heur faut- il vous voir pé -- rir ?
  
  Vous me con -- dam -- ne -- rez vous mê -- me,
  et vous me lais -- se -- rez mou -- rir.
  
  J’ar -- me -- rai, s’il se faut, tout le pou -- voir su -- prê -- me…
  
  Non, rien ne peut me se -- cou -- rir,
  je meurs d’a -- mour pour vous, je n’en sau -- rais gué -- rir ;
  
  Quoy ? vous ?
  
  Il est trop vrai.
  
  Vous m’ai -- mez ?
  
  Je vous ai -- me.
  Vous me con -- dam -- ne -- rez vous mê -- me,
  et vous me lais -- se -- rez mou -- rir.
  J’ai mé -- ri -- té qu’on me pu -- nis -- se,
  j’of -- fense un ri -- val gé -- né -- reux,
  qui par mil -- le bien -- faits a pré -- ve -- nu mes vœux :
  mais je l’of -- fense en vain, vous lui ren -- dez jus -- ti -- ce ;
  ah ! que c’est un cru -- el sup -- pli -- ce
  d’a -- vou -- er qu’un ri -- val est di -- gne d’être heu -- reux !
  Pro -- non -- cez mon ar -- rêt, par -- lez sans vous con -- train -- dre.
  
  Hé -- las !
  
  Vous sou -- pi -- rez ? je vois cou -- ler vos pleurs ?
  D’un mal -- heu -- reux a -- mour plai -- gnez- vous les dou -- leurs ?

  A -- tys, que vous se -- riez à plain -- dre
  si vous sa -- viez tous vos mal -- heurs !
  
  Si je vous perds, et si je meurs,
  que puis-je en -- core a -- voir à crain -- dre ?
  
  C’est peu de perdre en moi ce qui vous a char -- mé,
  vous me per -- dez, A -- tys, et vous ê -- tes ai -- mé.
  
  Ai -- mé ! qu’en -- tends- je ? ô Ciel ! quel a -- veu fa -- vo -- ra -- ble !
  
  Vous en se -- rez plus mi -- sé -- ra -- ble.
  
  Mon mal -- heur en est plus af -- freux,
  le bon -- heur que je perds doit re -- dou -- bler ma ra -- ge ;
  mais n’im -- porte, ai -- mez- moi, s’il se peut, d’a -- van -- ta -- ge,
  quand j’en de -- vrais mou -- rir cent fois plus mal -- heu -- reux.
  
  Si vous cher -- chez la mort, il faut que je vous sui -- ve ;
  vi -- vez, c’est mon a -- mour qui vous en fait la loi.
  
  Eh com -- ment ! eh pour -- quoi
  vou -- lez- vous que je vi -- ve,
  si vous ne vi -- vez pas pour moi ?
}

% Atys et Sangaride.
Si l’hy -- men u -- nis -- sait mon des -- tin et le vô -- tre,
\tag #'voix1 { que ses nœuds, }
que ses nœuds au -- raient eu d’at -- traits !
L’a -- mour fit nos cœurs l’un pour l’au -- tre,
faut- il que le de -- voir les sé -- pare à ja -- mais ?
L’a -- mour fit nos cœurs l’un pour l’au -- tre,
faut- il que le de -- voir les sé -- pare à ja -- mais ?
\tag #'(voix0 basse) { faut- il que le de -- voir }
faut- il que le de -- voir les sé -- pare à ja -- mais, à ja -- mais ?
\tag #'(voix0 basse) { faut- il que le de -- voir }
faut- il que le de -- voir les sé -- pare à ja -- mais, à ja -- mais ?

\tag #'(voix1 basse) {
  De -- voir im -- pi -- toy -- a -- ble !
  Ah quel -- le cru -- au -- té !

  On vient, fei -- gnez en -- cor, crai -- gnez d’être é -- cou -- té.

  Ai -- mons un bien plus du -- ra -- ble
  que l’é -- clat de la beau -- té :
  rien n’est plus ai -- ma -- ble
  que la li -- ber -- té.
  Rien n’est plus ai -- ma -- ble
  que la li -- ber -- té.
  Rien n’est plus ai -- ma -- ble
  que la li -- ber -- té.
}

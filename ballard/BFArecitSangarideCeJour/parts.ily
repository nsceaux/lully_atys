\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix"
          #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#9 \fill-line {
  \column {
    \livretPers Atys
    \livretVerse#12 { Sangaride, ce jour est un grand jour pour vous. }
    \livretPers Sangaride
    \livretVerse#12 { Nous ordonnons tous deux la fête de Cybèle, }
    \livretVerse#8 { L’honneur est égal entre nous. }
    \livretPers Atys
    \livretVerse#12 { Ce jour même, un grand Roi doit être votre époux, }
    \livretVerse#12 { Je ne vous vis jamais si contente et si belle ; }
    \livretVerse#8 { Que le sort du Roi sera doux ! }
    \livretPers Sangaride
    \livretVerse#12 { L’indifférent Atys n’en sera point jaloux. }
    \livretPers Atys
    \livretVerse#12 { Vivez tous deux contents, c’est ma plus chère envie ; }
    \livretVerse#12 { J’ai pressé votre hymen, j’ai servi vos amours. }
    \livretVerse#12 { Mais enfin ce grand jour, le plus beau de vos jours, }
    \livretVerse#8 { Sera le dernier de ma vie. }
    \livretPers Sangaride
    \livretVerse#12 { O dieux ! }
    \livretPers Atys
    \livretVerse#12 { \transparent { O dieux ! } Ce n’est qu’à vous que je veux révéler }
    \livretVerse#12 { Le secret désespoir où mon malheur me livre ; }
    \livretVerse#12 { Je n’ai que trop su feindre, il est temps de parler ; }
    \livretVerse#8 { Qui n’a plus qu’un moment à vivre, }
    \livretVerse#8 { N’a plus rien à dissimuler. }
    \livretPers Sangaride
    \livretVerse#8 { Je frémis, ma crainte est extrême ; }
    \livretVerse#12 { Atys, par quel malheur faut-il vous voir périr ? }
    \livretPers Atys
    \livretVerse#8 { Vous me condamnerez vous même, }
    \livretVerse#8 { Et vous me laisserez mourir. }
    \livretPers Sangaride
    \livretVerse#12 { J’armerai, s’il se faut, tout le pouvoir suprême… }
    \livretPers Atys
    \livretVerse#8 { Non, rien ne peut me secourir, }
    \livretVerse#12 { Je meurs d’amour pour vous, je n’en saurais guérir ; }
    \livretPers Sangaride
    \livretVerse#12 { Quoy ? vous ? }
    \livretPers Atys
    \livretVerse#12 { \transparent { Quoy ? vous ? } Il est trop vrai. }
    \livretPers Sangaride
    \livretVerse#12 { \transparent { Quoy ? vous ? Il est trop vrai. } Vous m’aimez ? }
    \livretPers Atys
    \livretVerse#12 { \transparent { Quoy ? vous ? Il est trop vrai. Vous m’aimez ? } Je vous aime. }
    \livretVerse#8 { Vous me condamnerez vous même, }
    \livretVerse#8 { Et vous me laisserez mourir. }
    \livretVerse#8 { J’ai mérité qu’on me punisse, }
    \livretVerse#8 { J’offense un rival généreux, }
    \livretVerse#12 { Qui par mille bienfaits a prévenu mes vœux : }
    \livretVerse#12 { Mais je l’offense en vain, vous lui rendez justice ; }
  }
  \column {
    \livretVerse#8 { Ah ! que c’est un cruel supplice }
    \livretVerse#12 { D’avouer qu’un rival est digne d’être heureux ! }
    \livretVerse#12 { Prononcez mon arrêt, parlez sans vous contraindre. }
    \livretPers Sangaride
    \livretVerse#12 { Hélas ! }
    \livretPers Atys
    \livretVerse#12 { \transparent { Hélas ! } Vous soupirez ? je vois couler vos pleurs ? }
    \livretVerse#12 { D’un malheureux amour plaignez-vous les douleurs ? }
    \livretPers Sangaride
    \livretVerse#8 { Atys, que vous seriez à plaindre }
    \livretVerse#8 { Si vous saviez tous vos malheurs ! }
    \livretPers Atys
    \livretVerse#8 { Si je vous perds, et si je meurs, }
    \livretVerse#8 { Que puis-je encore avoir à craindre ? }
    \livretPers Sangaride
    \livretVerse#12 { C’est peu de perdre en moi ce qui vous a charmé, }
    \livretVerse#12 { Vous me perdez, Atys, et vous êtes aimé. }
    \livretPers Atys
    \livretVerse#12 { Aimé ! qu’entends-je ? ô Ciel ! quel aveu favorable ! }
    \livretPers Sangaride
    \livretVerse#8 { Vous en serez plus misérable. }
    \livretPers Atys
    \livretVerse#8 { Mon malheur en est plus affreux, }
    \livretVerse#12 { Le bonheur que je perds doit redoubler ma rage ; }
    \livretVerse#12 { Mais n’importe, aimez-moi, s’il se peut, d’avantage, }
    \livretVerse#12 { Quand j’en devrais mourir cent fois plus malheureux. }
    \livretPers Sangaride
    \livretVerse#12 { Si vous cherchez la mort, il faut que je vous suive ; }
    \livretVerse#12 { Vivez, c’est mon amour qui vous en fait la loi. }
    \livretPers Atys
    \livretVerse#6 { Eh comment ! eh pourquoi }
    \livretVerse#6 { Voulez-vous que je vive, }
    \livretVerse#8 { Si vous ne vivez pas pour moi ? }
    \livretPers Atys et Sangaride
    \livretVerse#12 { Si l’hymen unissait mon destin et le vôtre, }
    \livretVerse#8 { Que ses nœuds auraient eu d’attraits ! }
    \livretVerse#8 { L’Amour fit nos cœurs l’un pour l’autre, }
    \livretVerse#12 { Faut-il que le devoir les sépare à jamais ? }
    \livretPers Atys
    \livretVerse#6 { Devoir impitoyable ! }
    \livretVerse#6 { Ah quelle cruauté ! }
    \livretPers Sangaride
    \livretVerse#12 { On vient, feignez encor, craignez d’être écouté. }
    \livretPers Atys
    \livretVerse#7 { Aimons un bien plus durable }
    \livretVerse#7 { Que l’éclat de la beauté : }
    \livretVerse#5 { Rien n’est plus aimable }
    \livretVerse#5 { Que la liberté. }
  }
}#}))

%% Iris
\tag #'(iris basse) {
  Cy -- bè -- le veut que Flore au -- jour -- d’hui vous se -- con -- de.
  Il faut que les plai -- sirs vien -- nent de tou -- tes parts,
  dans l’em -- pi -- re puis -- sant, où règne un nou -- veau Mars,
  ils n’ont plus d’autre a -- sile au mon -- de.
  Ren -- dez- vous, s’il se peut, di -- gnes de ses re -- gards ;
  joi -- gnez la beau -- té vive et pu -- re
  dont bril -- le la na -- tu -- re,
  aux or -- ne -- ments des plus beaux arts.
}
\tag #'(melpomene flore basse) {
  Ren -- dons- nous, s’il se peut, di -- gnes de ses re -- gards ;
  joi -- gnons la beau -- té vive et pu -- re
  dont bril -- le la na -- tu -- re,
  aux or -- ne -- ments des plus beaux arts.
}
\tag #'choeur {
  Pré -- pa -- rez de nou -- vel -- les fê -- tes,
  pro -- fi -- tez du loi -- sir du plus grand des hé -- ros ;
}
\tag #'(temps basse) {
  Pré -- pa -- rons,
}
\tag #'(temps) {
  de nou -- vel -- les fê -- tes,
  pro -- fi -- tons du loi -- sir du plus grand, du plus grand des hé -- ros.
  Le temps des jeux, et du re -- pos,
  lui sert à mé -- di -- ter de nou -- vel -- les con -- quê -- tes.
}
\tag #'(melpomene flore basse) {
  Pré -- pa -- rons de nou -- vel -- les fê -- tes,
  pro -- fi -- tons du loi -- sir du plus grand des hé -- ros.
  Le temps des jeux, et du re -- pos,
  lui sert à mé -- di -- ter de nou -- vel -- les con -- quê -- tes.
}
\tag #'choeur {
  Le temps des jeux, et du re -- pos,
  lui sert à mé -- di -- ter de nou -- vel -- les con -- quê -- tes.
  Le temps des jeux
}
\tag #'(melpomene flore temps basse) {
  et du re -- pos.
}
\tag #'choeur {
  lui sert à mé -- di -- ter de nou -- vel -- les con -- quê -- tes.
  Le temps des jeux
}
\tag #'(melpomene flore basse) {
  Le temps des jeux et du re -- pos.
}
\tag #'(temps) {
  et du re -- pos. __
}
\tag #'choeur {
  lui sert à mé -- di -- ter de nou -- vel -- les con -- quê -- tes.
  Le temps des jeux
}
\tag #'(melpomene flore temps basse) {
  et du re -- pos.
}
\tag #'choeur {
  lui sert à mé -- di -- ter de nou -- vel -- les con -- quê -- tes.
  Le temps des jeux et du re -- pos,
  lui sert à mé -- di -- ter de nou -- vel -- les con -- quê -- tes.
  Le temps des jeux
}
\tag #'(melpomene flore basse) {
  Le temps des jeux et du re -- pos.
}
\tag #'(temps) {
  et du re -- pos. __
}
\tag #'choeur {
  lui sert à mé -- di -- ter de nou -- vel -- les con -- quê -- tes.
}

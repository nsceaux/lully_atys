\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new StaffGroupNoBracket <<
      \new GrandStaff <<
        \new Staff <<
          { s1*11 s2.*4 s1 s2.*27 \noHaraKiri }
          \global \keepWithTag #'dessus1 \includeNotes "dessus"
        >>
        \new Staff <<
          { s1*9 \startHaraKiri }
          \global \keepWithTag #'dessus2 \includeNotes "dessus"
        >>
      >>
      \new Staff <<
        { s1*11 s2.*4 s1 s2.*27 \noHaraKiri }
        \global \includeNotes "haute-contre"
      >>
      \new Staff <<
        { s1*11 s2.*4 s1 s2.*27 \noHaraKiri }
        \global \includeNotes "taille"
      >>
      \new Staff <<
        { s1*11 s2.*4 s1 s2.*27 \noHaraKiri }
        \global \includeNotes "quinte"
      >>
      \new Staff <<
        { s1*11 s2.*4 s1 s2.*27 \noHaraKiri }
        \global \keepWithTag #'basse \includeNotes "basse"
      >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        { s1*11 s2.*4 s1 s2.*27 \noHaraKiri }
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s1*11 s2.*4 s1 s2.*27 \noHaraKiri }
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s1*11 s2.*4 s1 s2.*27 \noHaraKiri }
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s1*11 s2.*4 s1 s2.*27 \noHaraKiri }
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'iris \includeNotes "voix"
      >> \keepWithTag #'iris \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s1*11 s2.*4 s1 s2.*14 \noHaraKiri }
        \global \keepWithTag #'flore \includeNotes "voix"
      >> \keepWithTag #'flore \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s1*11 s2.*4 s1 s2.*14 \noHaraKiri }
        \global \keepWithTag #'melpomene \includeNotes "voix"
      >> \keepWithTag #'melpomene \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s1*11 s2.*4 s1 s2.*27 s1*6 \noHaraKiri }
        \global \keepWithTag #'temps \includeNotes "voix"
      >> \keepWithTag #'temps \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
  \midi { }
}
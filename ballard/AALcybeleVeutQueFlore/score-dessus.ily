\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> { \set fontSize = -2 \keepWithTag #'basse \includeLyrics "paroles" }
    \new GrandStaff <<
      \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus" >>
      \new Staff <<
        { s1*9 \startHaraKiri }
        \global \keepWithTag #'dessus2 \includeNotes "dessus"
      >>
    >>
  >>
  \layout { }
}
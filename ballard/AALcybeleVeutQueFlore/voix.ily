<<
  %% Iris
  \tag #'(iris basse) {
    \clef "vbas-dessus" R1*8 |
    r2 r4 \tag #'iris <>^\markup\character-text "Iris" "[parlant à Melpomène]"
    \tag #'basse <>^\markup\character Iris r8 sol' |
    re'' re'' si'8.\trill si'16 do''8 do''16 do'' do''8 do''16 si' |
    si'8\trill si' r sol' do''8. do''16 do''8. do''16 %{ do''8 do'' %} |
    do''4 si'8\trill si'16 si' si'8. do''16 |
    la'4\trill r8 la'16 si' do''8 do''16 re'' |
    mi''8. mi''16 dod''8. dod''16 dod''8. re''16 |
    %{re''4.%} re''4 r8 la'8 la'8. si'16 |
    sol'4.\trill sol'8 sol'4( fad'8) sol' |
    fad'2\trill fad'4 |
    r4 r re''8. do''16 |
    si'4\trill si'4. do''8 |
    la'2.\trill |
    re''4 re''8 do'' si'8. la'16 %{ si'8 la' %} |
    sold'2 r8 sold' |
    la'4 la' si' |
    do'' do''( si'8) do'' |
    si'4\trill si' r8 re'' |
    mi''4. re''8 do'' si' |
    la'2\trill la'4 |
    si' do'' re'' |
    do''2\trill si'4 |
    si'( la'4.)\trill sol'8 |
    sol'2
    \tag #'(iris) {
      <>^\markup\override #'(line-width . 50) \italic\wordwrap {
        [Iris remonte au ciel sur son arc, et la suite de
        Melpomène s’accorde avec la suite de Flore.]
      }
      r4 |
      R2.*12 |
      R1*13 |
      R2.*62 |
    }
  }
  %% Flore
  \tag #'(flore basse) {
    << { s1*11 s2.*4 s1 s2.*14 s2 }
      \tag #'flore { \clef "vbas-dessus" R1*11 | R2.*4 | R1 | R2.*14 | r4 r } >>
    \tag #'flore <>^\markup\character "Flore"
    \tag #'basse \floreMark
    re''8.  re''16 |\noBreak
    mi''4 mi''4. mi''8 |
    do''2. |
    fa''4 fa''8 mi'' re''8. do''16 %{ re'' do'' %} |
    si'2\trill r8 si' |
    do''4 do''4. do''8 |
    si'4\trill si' do'' |
    re'' re''4. re''8 |
    mi''4. re''8 do''8. si'16 |
    la'2\trill la'4 |
    si'4 do'' re'' |
    do''2\trill si'4 |
    si'( la'4.)\trill sol'8 |
    sol'2 r2 |
    R1*5 |
    <<
      \tag #'basse { r2 s s2 \floreMark }
      \tag #'flore { R1 | r2 <>^\markup\character "[Flore]" }
    >> re''4. re''8 |\noBreak
    mi''4 mi''8 mi'' re''4.\trill do''8 |
    si'4\trill si' si'4. si'8 |
    mi''2 mi''4. mi''8 |
    re''2 re''4 do'' |
    si'2 %{ la'4. sol'8 %} la'4 sol' |
    fad'2.\trill |
    re''4 re'' mi'' |
    mi''2 re''4 |
    re''( dod''4.) re''8 |
    re''2 re''4 |
    si'4.\trill si'8 do'' re'' |
    mi''4 do''4.\trill si'8 |
    la'4.\trill si'8 do''4 |
    si'2\trill si'4 |
    R2.*10 |
    re''4 re'' mi'' |
    re''2\trill r4 |
    R2.*6 |
    la'4 la' si' |
    do''2. |
    si'4 do''8[ si'] la'[ sol'] |
    la'2 r4 |
    R2.*6 |
    mi''4 mi''4 fad'' | % mi''4. fa''8
    mi''2\trill r4 |
    R2.*14 |
    la'4 la' si' |
    do''2. |
    si'4 do''8[ si'] la'[ sol'] |
    la'2 r4 |
    R2.*5 |
  }
  %% Melpomène
  \tag #'(melpomene) {
    \clef "vbas-dessus" R1*11 | R2.*4 | R1 | R2.*14 |
    r4 r si'8.^\markup\character "Melpomène" si'16 |
    do''4 do''4. do''8 |
    la'2.\trill |
    re''4 re''8 do'' si'8. la'16 %{ si'8 la' %} |
    sold'2\trill r8 sold' |
    la'4 mi'4. fad'8 |
    sol'4 sol'4. la'8 %{ sol'4 la' %} |
    si'4 si'4.\trill si'8 |
    do''4. si'8 la'8. sol'16 |
    fad'2\trill fad'4 |
    sol' la' si' |
    la'2\trill sol'4 |
    sol'( fad'4.)\trill sol'8 |
    sol'2 r2 |
    R1*6 |
    r2 si'4.^\markup\character "[Melpomène]" si'8 |
    do''4 do''8 do'' si'4.\trill la'8 |
    sol'4 sol' sol'4. sol'8 |
    do''2 %{ do''4 do'' %} do''4. do''8 |
    si'2\trill si'4 do'' |
    re''2 %{ do''4. si'8 %} do''4\trill si' |
    la'2.\trill |
    si'4 si' do'' |
    la'2\trill %{ la'4 %} si'4 |
    sol'4.( fad'8) sol'4 |
    fad'2\trill si'4 |
    sol'4. sol'8 la' si' |
    do''4 la'4. sol'8 |
    sol'4. sol'8 fad'4\trill |
    sol'2 sol'4 |
    R2.*10 |
    si'4 si' do'' |
    si'2\trill r4 |
    R2.*6 |
    fad'4 fad' sol' |
    la'2. |
    sol'4 la'8[ sol'] fad'[ mi'] |
    fad'2\trill r4 |
    R2.*6 |
    dod''4 dod''4 re'' | % dod''4. re''8
    dod''2\trill r4 |
    R2.*14 |
    fad'4 fad' sol' |
    la'2. |
    sol'4 la'8[ sol'] fad'[ mi'] |
    fad'2\trill r4 |
    R2.*5 |
  }
  %% Temps
  \tag #'(temps basse) {
    << { s1*11 s2.*4 s1 s2.*27 s1*6 s2 }
      \tag #'temps { \clef "vbasse-taille" R1*11
        R2.*4 R1 R2.*27 R1*6 | r2^\markup\character "[Le Temps]" } >>
    \tag #'basse \tempsMark re4. re8 |
    sol4
    \tag #'temps {
      sol8 sol sol4. sol8 |
      do4 do re4. re8 |
      mi2 %{ mi4. mi8 %} mi4 mi |
      do2 do4 do |
      sol2 sol4 la |
      si2 si4 do' |
      re'2. |
      sol4 sol4. sol8 |
      fad2\trill fad4 |
      mi2 r8 re |
      re2 si,4 |
      mi4. mi8 mi re |
      do4 do do |
      re4. do8 re4 |
      sol,2 sol,4 |
      R2.*10 |
      sol4 sol4. sol8 |
      sol2 r4 |
      R2.*6 |
      re4 re4.\trill re8 |
      re2.~ |
      re~ |
      re2 r4 |
      R2.*6 |
      la4 la4. la8 |
      la2 r4 |
      R2.*14 |
      re4 re4. re8 |
      re2.~ |
      re~ |
      re2 r4 |
      R2.*5 |
    }
  }
  %% Chœur (dessus)
  \tag #'vdessus {
    \clef "vbas-dessus" R1*11 R2.*4 R1 R2.*27 |
    r2 <>^\markup\character "[Chœur des Heures]" re''4. re''8 | \noBreak
    mi''4 mi''8 mi'' do''4. do''8 |
    la'4\trill la' re''4. re''8 |
    re''2 do''4( si'8) do'' |
    si'2\trill re''4. re''8 |
    re''2 dod''4.\trill re''8 |
    re''2 r2 |
    R1*6 R2.*9 |
    re''4 re''4. re''8 |
    re''2. |
    si'4 si'4. si'8 |
    do''2 mi''4 |
    do''4. do''8 do'' do'' |
    fa''2 re''8 do'' |
    si'4.\trill dod''8 re''4 |
    dod''2\trill dod''4 |
    re''4 re''4. re''8 |
    si'2.\trill |
    R2. |
    r4 r re''4 |
    mi''4. mi''8 mi'' mi'' |
    mi''4 mi'' mi'' | % mi''4 mi''4. mi''8
    mi''4. mi''8 red''4 |
    mi''2 mi''4 |
    dod''4 dod''4. dod''8 |
    re''2. |
    R2.*3 |
    r4 r la'4 |
    si'4.\trill do''8 re'' si' |
    mi''4 do''4.\trill si'8 |
    la'4.\trill si'8 do''4 |
    si'2\trill si'4 |
    si' si'4. mi''8 |
    dod''2.\trill |
    R2. |
    r4 r mi''4 |
    fa''4. fa''8 fa'' fa'' |
    re''4 re'' re'' |
    re''4. re''8 dod''4\trill |
    re''2 re''4 |
    re'' re''4. re''8 |
    si'2.\trill |
    mi''4 mi''4. mi''8 |
    re''2\trill re''4 |
    mi''4. mi''8 mi'' mi'' |
    do''4 do'' do'' |
    do''4. do''8 si'4\trill |
    do''2 do''4 |
    si'4 si'4. si'8 |
    la'2.\trill |
    R2.*3 |
    r4 r la'4 |
    si'4.\trill si'8 do'' re'' |
    mi''4 do''4. re''8 |
    si'4\trill si' do'' |
    si'( la'2)\trill |
    sol'2. |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre2" R1*11 |
    R2.*4 |
    R1 |
    R2.*27 |
    r2 sol'4. sol'8 |
    sol'4 sol'8 sol' la'4. la'8 |
    fad'4 fad' fad'4. fad'8 |
    sol'2 la'4. la'8 |
    sol'2 sol'4. sol'8 |
    mi'2 mi'4. la'8 |
    fad'2 r |
    R1*6 |
    R2.*9 |
    sol'4 sol'4. sol'8 |
    fad'2. |
    sol'4 sol'4. sol'8 |
    sol'2 sol'4 |
    fa'4. fa'8 fa' fa' |
    fa'4 fa' fa' | % fa'2 fa'8 fa'
    mi'4. mi'8 mi'4 |
    mi'2 mi'4 |
    fad'4 fad'4. fad'8 |
    sol'2. |
    R2. |
    r4 r sol'4 |
    sol'4. sol'8 sol' sol' |
    la'4 la' la' |
    fad'4. sold'8 la'4 |
    sold'2 sold'4 |
    mi'4 mi'4. mi'8 |
    fad'2. |
    R2.*3 |
    r4 r fad'4 |
    sol'4. sol'8 sol' sol' |
    sol'4 sol' sol' |
    sol'4. sol'8 fad'4 |
    sol'2 sol'4 |
    sol' sol'4. sol'8 |
    mi'2. |
    R2. |
    r4 r la'4 |
    la'4. la'8 la' la' |
    fa'4 fa' sol' |
    mi'4. fad'8 sol'4 |
    fad'2 fad'4 |
    sol' sol' re' |
    mi'2. |
    sol'4 sol'4. sol'8 |
    sol'2 sol'4 |
    sol'4. sol'8 sol' sol' |
    fa'4 fa' fa' |
    re'4. mi'8 fa'4 |
    mi'2 mi'4 |
    re' re'4. re'8 |
    re'2. |
    R2.*3 |
    r4 r fad'4 |
    sol'4. sol'8 sol' sol' |
    sol'4 sol' la' |
    sol'4. sol'8 sol'4 | % sol'4 sol'4. sol'8
    sol'2( fad'4) | % sol'4( fad'2)
    sol'2. |
  }
  \tag #'vtaille {
    \clef "vtaille" R1*11 |
    R2.*4 |
    R1 |
    R2.*27 |
    r2 si4. si8 |
    do'4 mi'8 mi' mi'4. mi'8 |
    re'4 re' la4. la8 |
    sol2 re'4. re'8 |
    re'2 si4 si |
    la2 la4. la8 |
    la2 r |
    R1*6 |
    R2.*9 |
    si4 si4. si8 |
    la2.\trill |
    mi'4 mi'4. mi'8 |
    mi'2 do'4 |
    la4.\trill la8 la la |
    la4 la la | % la4 la4. la8
    la4. la8 sold4\trill |
    la2 la4 |
    la4 la4. re'8 |
    re'2. |
    R2. |
    r4 r si4 |
    do'4. do'8 do' do' |
    do'4 do' do' |
    si4.\trill si8 si4 |
    si2 si4 |
    la la4.\trill la8 |
    la2. |
    R2.*3 |
    r4 r re'4 |
    re'4. re'8 re' re' |
    do'4 mi'4. mi'8 |
    re'4.\trill re'8 re'4 |
    re'2 re'4 |
    mi' si4. si8 |
    la2.\trill |
    R2. |
    r4 r dod'4 |
    re'4. re'8 la la |
    sib4 sib sib |
    la4.\trill la8 la4 |
    la2 la4 |
    si4 si4. si8 |
    sold2. | % sol2.
    do'4 do'4. do'8 |
    si2\trill si4 |
    do'4. do'8 do' do' |
    la4 la la |
    sol4.\trill sol8 sol4 |
    sol2 sol4 |
    sol4 sol4. sol8 |
    fad2.\trill |
    R2.*3 |
    r4 r re'4 |
    re'4. re'8 do' si |
    do'4 mi'4. mi'8 |
    mi'4 mi' mi' |
    re'2. | % re'2( do'4)
    si2.\trill |
  }
  \tag #'vbasse {
    \clef "vbasse" R1*11 |
    R2.*4 |
    R1 |
    R2.*27 |
    r2 sol4. sol8 |
    do'4 do'8 do' la4.\trill la8 |
    re'4 re' re4. re8 |
    mi2 fad4.\trill fad8 |
    sol2 sol4 sol %{ sol4. sol8 %} |
    la2 la4. la8 |
    re2 r |
    R1*6 |
    R2.*9 |
    sol4 sol4. sol8 |
    re2. |
    mi4 mi4. mi8 |
    do2\trill do4 |
    fa4. fa8 fa fa |
    re4\trill re re |
    mi4. re8 mi4 |
    la,2 la,4 |
    re4 re4. re8 |
    sol2. |
    R2. |
    r4 r sol4 |
    do'4. do'8 do' do' |
    la4 la la |
    si4. la8 si4 |
    mi2 mi4 |
    la4 la4. la8 |
    re2. |
    R2.*3 |
    r4 r re4 |
    sol4. la8 si sol |
    do'4 do'4. do'8 | % do'4 do'
    re'4. re'8 re4 |
    sol2 sol4 |
    mi4 mi4. mi8 |
    la2. |
    R2. |
    r4 r la4 |
    fa4. fa8 fa fa |
    sib4 sib sol |
    la4. la8 la,4 | % la4. sol8 la4
    re2 re4 |
    si, si,4. si,8 |
    mi2. |
    do4 do4. do8 |
    sol,2 sol4 |
    mi4. mi8 mi mi |
    fa4 fa fa |
    sol4. fa8 sol4 |
    do2 do4 |
    sol4 sol4. sol8 |
    re2. |
    R2.*3 |
    r4 r re4 |
    sol4. fa8 mi re |
    do4 do la, |
    mi mi do |
    re2. |
    sol, |
  }
>>

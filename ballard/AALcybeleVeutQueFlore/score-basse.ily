\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new Staff \with { \tinyStaff } \withTinyLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse \includeLyrics "paroles"
    \new StaffGroupNoBracket <<
      \new Staff <<
        \global \keepWithTag #'basse \includeNotes "basse"
      >>
      \new Staff <<
        \global \keepWithTag #'basse-continue \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout { }
}
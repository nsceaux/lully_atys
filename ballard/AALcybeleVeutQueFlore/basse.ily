\clef "basse" <<
  \tag #'basse { R1*11 R2.*4 R1 R2.*27 r2 }
  \tag #'(basse-continue tous) {
    % sol,1~ |
    sol,4. sol8 sol4. sol8 |
    mi4. mi8 la4. la8 |
    re4. re8 mi4 mi8 re |
    dod4\trill re la,2 |
    %{re4. re'8%} re4 re' do'4.\trill si8 |
    la4 la, do la, |
    mi4. mi8 re4.\trill do8 %{ re4 do %} |
    si,4 do re re, |
    sol,1~ | \allowPageTurn
    \tag #'tous <>^"B.C."
    sol,4 sol mi fad\trill |
    sol2 mi |
    fad4 sol sol, |
    re4. do16 si, la,4 |
    sold,4\trill la,4. sol,8 |
    fad,2 fad,8 sol, |
    mi,2 la, |
    re, re8 mi |
    fad2.\trill |
    sol2 mi4 |
    fa4. sol8 fa mi |
    re4. mi8 fa re |
    mi2 re4 |
    do2\trill si,4 |
    la,4. si,8 do la, |
    mi2 si,4 |
    do4. si,8 la, sol, |
    re4 re' do' |
    si la sol |
    fad2\trill sol4 |
    do re re, |
    sol,2 %{%} sol4 |
    do2 do4 |
    fa4. sol8 fa mi |
    re4. mi8 fa re |
    mi2 mi,4 |
    la, la8 si do' la |
    mi'4 re' do' |
    si4.\trill la8 sol fad |
    mi re do2\trill |
    re4 re' do' |
    si la sol |
    fad2\trill sol4 |
    do re re, |
    sol,2
    \tag #'tous <>^"Tous"
  }
>>
%% Chœur
sol4. sol8 |
\twoVoices #'(basse basse-continue tous) <<
  { do'4 do'8 do' la4. la8 |
    re'4 re' re4. re8 |
    mi2 fad4. fad8 |
    sol2 sol4 sol |
    la2 %{ la4. la8 %} la4 la, |
    re2 r | }
  { do'2 la |
    re' %{ re2 %} re4. re8 |
    mi2 fad\trill |
    sol2. sol4 |
    la2 la, |
    re2. <>_"B.C." re4 | }
>>
<<
  \tag #'basse { R1*6 R2.*9 }
  \tag #'(basse-continue tous) {
    %{ sol2 sol4 %} sol2. sol4 |
    do2 re\trill |
    %{ mi2 mi4 %} mi2. mi4 |
    %{ do2 do4 %} do2. do4 |
    sol2 sol4 la |
    %{ si2 si4 %} si2.\trill do'4 |
    re'2. | % re'2 re4
    sol2. | % sol2 sol4
    fad2.\trill | % fad2 fad4
    mi2. |
    re2 si,4 |
    mi4. mi8 mi re | % mi2 mi8 re
    do2 do4 |
    re2 re,4 |
    sol,2. | % sol,2 sol,4
  }
>>
\tag #'tous <>^"Tous"
\twoVoices #'(basse basse-continue tous) << { sol4 sol4. sol8 | } { sol2 sol4 | } >>
re2. |
\twoVoices #'(basse basse-continue tous) << { mi4 mi4. mi8 | } { mi2 mi4 | } >>
do2 do4 |
\twoVoices #'(basse basse-continue tous) <<
  { fa4. fa8 fa fa |
    re4 re re |
    mi4. %{ mi8 mi,4 %} re8 mi4 | }
  { fa2 fa4 |
    re2 re4 |
    mi2 mi,4 | }
>>
la,2 la,4 |
re re4. re8 |
<<
  \tag #'basse { sol2. | R2. | r4 r }
  \tag #'(basse-continue tous) {
    sol2.~ | \allowPageTurn
    \tag #'tous <>^"B.C." sol~ |
    sol2 \tag #'tous <>^"Tous"
  }
>> sol4 |
do'4. do'8 do' do' |
la4 la la |
si4. la8 si4 |
mi2 mi4 |
la4 la4. la8 |
<<
  \tag #'basse { re2. | R2.*3 | r4 r }
  \tag #'(basse-continue tous) {
    re2.~ | \allowPageTurn
    \tag #'tous <>^"B.C." re~ | re~ | re~ | re2
    \tag #'tous <>^"Tous"
  }
>> re4 |
sol4. la8 si sol |
\twoVoices #'(basse basse-continue tous) <<
  { do'4 do' do' | re'4. re'8 re4 | }
  { do'2 do'4 | re'2 re4 | }
>>
sol2 sol4 |
mi mi4. mi8 |
<<
  \tag #'basse { la2. | R2. | r4 r }
  \tag #'(basse-continue tous) {
    la2.~ | \allowPageTurn
    \tag #'tous <>^"B.C." la~ |
    la2 \tag #'tous <>^"Tous"
  }
>> la4 |
\twoVoices #'(basse basse-continue tous) <<
  { fa4. fa8 fa fa |
    sib4 sib sol |
    la4. la8 la,4 | }
  { fa2 fa4 |
    sib2 sol4 |
    la2 la,4 | }
>>
re2 re4 |
\twoVoices #'(basse basse-continue tous) <<
  { si,4 si,4. si,8 |
    mi2. |
    do4 do4. do8 | }
  { si,2 si,4 |
    mi2. |
    do2 do4 | }
>>
sol,2 sol4 |
\twoVoices #'(basse basse-continue tous) <<
  { mi4. mi8 mi mi |
    fa4 fa fa |
    sol4. fa8 sol4 | }
  { mi2\trill mi4 |
    fa2 fa4 |
    sol4 sol,2 | }
>>
do2 do4 |
sol sol4. sol8 |
<<
  \tag #'basse { re2. | R2.*3 | r4 r }
  \tag #'(basse-continue tous) {
    re2.~ | \allowPageTurn
    \tag #'tous <>^"B.C." re~ | re~ | re~ | re2
    \tag #'tous <>^"Tous"
  }
>> re4 |
sol4. fa8 mi re |
\twoVoices #'(basse basse-continue tous) <<
  { do4 do la, | mi mi do | }
  { do2 la,4 | mi2 do4 | }
>>
re4 re,2 | % re2.
sol,2. |

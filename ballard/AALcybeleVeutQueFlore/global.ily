\key sol \major
\beginMark "Ritournelle"
\time 2/2 \midiTempo#160 s1*11
\digitTime\time 3/4 \midiTempo#80 s2.*4
\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#160 s2.*27
\time 2/2 \midiTempo#160 s1*13
\digitTime\time 3/4 s2.*62 \bar "|."

\piecePartSpecs
#`((dessus #:score "score-dessus")
   (haute-contre #:score-template "score-voix")
   (haute-contre-sol #:score-template "score-voix")
   (taille #:score-template "score-voix")
   (quinte #:score-template "score-voix")
   (basse #:score "score-basse")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPersDidas Iris parlant à Melpomène
    \livretVerse#12 { Cybèle veut que Flore aujourd’hui vous seconde. }
    \livretVerse#12 { Il faut que les plaisirs viennent de toutes parts, }
    \livretVerse#12 { Dans l’empire puissant, où règne un nouveau Mars, }
    \livretVerse#8 { Ils n’ont plus d’autre asile au monde. }
    \livretVerse#12 { Rendez-vous, s’il se peut, dignes de ses regards ; }
    \livretVerse#8 { Joignez la beauté vive et pure }
    \livretVerse#6 { Dont brille la nature, }
    \livretVerse#8 { Aux ornements des plus beaux arts. }
    \livretPers Melpomène et Flore
    \livretVerse#12 { Rendons-nous, s’il se peut, dignes de ses regards ; }
    \livretVerse#8 { Joignons la beauté vive et pure }
    \livretVerse#6 { Dont brille la nature, }
    \livretVerse#8 { Aux ornements des plus beaux arts. }
  }
  \column {
    \livretPers Le Temps, et le Chœur des Heures
    \livretVerse#8 { Préparez de nouvelles fêtes, }
    \livretVerse#12 { Profitez du loisir du plus grand des héros ; }
    \livretPers Le Temps, Melpomène et Flore
    \livretVerse#8 { Préparez/Préparons de nouvelles fêtes }
    \livretVerse#12 { Profitez/Profitons du loisir du plus grand des héros. }
    \livretPers Tous ensemble
    \livretVerse#8 { Le temps des jeux, et du repos, }
    \livretVerse#12 { Lui sert à méditer de nouvelles conquêtes. }
  }
}#}))
    
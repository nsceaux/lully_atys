\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Idas
    \livretVerse#8 { Peut-on ici parler sans feindre ? }
    \livretPers Atys
    \livretVerse#12 { Je commande en ces lieux, vous n’y devez rien craindre. }
    \livretPers Doris
    \livretVerse#12 { Mon frère est votre ami. }
    \livretPers Idas
    \livretVerse#12 { \transparent { Mon frère est votre ami. } Fiez-vous à ma sœur. }
    \livretPers Atys
    \livretVerse#12 { Vous devez avec moi partager mon bonheur. }
    \livretPers Idas et Doris
    \livretVerse#12 { Nous venons partager vos mortelles alarmes ; }
    \livretVerse#8 { Sangaride les yeux en larmes }
    \livretVerse#6 { Nous vient d’ouvrir son cœur. }
    \livretPers Atys
    \livretVerse#12 { L’heure approche où l’hymen voudra qu’elle se livre }
    \livretVerse#8 { Au pouvoir d’un heureux époux. }
    \livretPers Idas et Doris
    \livretVerse#5 { Elle ne peut vivre }
    \livretVerse#7 { Pour un autre que pour vous. }
    \livretPers Atys
    \livretVerse#12 { Qui peut la dégager du devoir qui la presse ? }
    \livretPers Idas et Doris
    \livretVerse#12 { Elle veut elle-même aux pieds de la Déesse }
    \livretVerse#12 { Déclarer hautement vos secrètes amours. }
  }
  \column {
    \livretPers Atys
    \livretVerse#8 { Cybèle pour moi s’intéresse, }
    \livretVerse#12 { J’ose tout espérer de son divin secours… }
    \livretVerse#12 { Mais quoi, trahir le roi ! tromper son espérance ! }
    \livretVerse#12 { De tant de biens reçus est-ce la récompense ? }
    \livretPers Idas et Doris
    \livretVerse#6 { Dans l’empire amoureux }
    \livretVerse#8 { Le Devoir n’a point de puissance ; }
    \livretVerse#4 { L’Amour dispense }
    \livretVerse#8 { Les rivaux d’être généreux ; }
    \livretVerse#10 { Il faut souvent pour devenir heureux }
    \livretVerse#8 { Qu’il en coûte un peu d’innocence. }
    \livretPers Atys
    \livretVerse#12 { Je souhaite, je crains, je veux, je me repens. }
    \livretPers Idas et Doris
    \livretVerse#12 { Verrez-vous un rival heureux à vos dépens ? }
    \livretPers Atys
    \livretVerse#12 { Je ne puis me résoudre à cette violence. }
    \livretPers Atys, Idas et Doris
    \livretVerse#10 { En vain, un cœur, incertain de son choix, }
    \livretVerse#8 { Met en balance mille fois }
    \livretVerse#8 { L’Amour et la Reconnaissance, }
    \livretVerse#10 { L’Amour toujours emporte la balance. }
    \livretPers Atys
    \livretVerse#12 { Le plus juste parti cède enfin au plus fort. }
    \livretVerse#8 { Allez, prenez soin de mon sort, }
    \livretVerse#12 { Que Sangaride ici se rende en diligence. }
  }
}#}))

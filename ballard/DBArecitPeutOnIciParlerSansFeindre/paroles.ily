\tag #'(idas basse) {
  % Idas
  Peut- on i -- ci par -- ler sans fein -- dre ?
}
\tag #'(atys basse) {
  % Atys
  Je com -- mande en ces lieux, vous n’y de -- vez rien crain -- dre.
}
\tag #'(doris basse) {
  % Doris
  Mon frère est votre a -- mi.
}
\tag #'(idas basse) {
  % Idas
  Fi -- ez- vous à ma sœur.
}
\tag #'(atys basse) {
  % Atys
  Vous de -- vez a -- vec moi par -- ta -- ger mon bon -- heur.
}
\tag #'(doris idas basse) {
  % Doris et Idas
  Nous ve -- nons par -- ta -- ger vos mor -- tel -- les a -- lar -- mes ;
  San -- ga -- ri -- de les yeux en lar -- mes
  nous vient d’ou -- vrir son cœur.
}
\tag #'(atys basse) {
  % Atys
  L’heure ap -- proche où l’hy -- men vou -- dra qu’el -- le se li -- vre
  au pou -- voir d’un heu -- reux é -- poux.
}
\tag #'(doris idas basse) {
  % Doris et Idas
  El -- le ne peut vi -- vre
  pour un au -- tre que pour vous.
}
\tag #'(atys basse) {
  % Atys
  Qui peut la dé -- ga -- ger du de -- voir qui la pres -- se ?
}
\tag #'(doris idas basse) {
  % Doris et Idas
  El -- le veut el -- le- même aux pieds de la dé -- es -- se
  dé -- cla -- rer hau -- te -- ment vos se -- crè -- tes a -- mours.
}
\tag #'(atys basse) {
  % Atys
  Cy -- bè -- le pour moi s’in -- té -- res -- se,
  j’o -- se tout es -- pé -- rer de son di -- vin se -- cours…
  mais quoi, tra -- hir le roi ! trom -- per son es -- pé -- ran -- ce !
  De tant de biens re -- çus est- ce la ré -- com -- pen -- se ?
}
\tag #'(doris idas basse) {
  % Doris et Idas
  Dans l’em -- pire a -- mou -- reux
  \tag #'(doris basse) { le de -- voir, }
  le de -- voir n’a point de puis -- san -- ce ;
  l’a -- mour dis -- pen -- se
  les ri -- vaux d’ê -- tre gé -- né -- reux ;
  il faut sou -- vent pour de -- ve -- nir heu -- reux
  qu’il en coûte un peu d’in -- no -- cen -- ce.
  Il faut sou -- vent pour de -- ve -- nir heu -- reux
  qu’il en coûte un peu d’in -- no -- cen -- ce,
  qu’il en coûte un peu d’in -- no -- cen -- ce..
}
\tag #'(atys basse) {
  % Atys
  Je sou -- hai -- te, je crains, je veux, je me re -- pens.
}
\tag #'(doris idas basse) {
  % Doris et Idas
  Ver -- rez- vous un ri -- val heu -- reux à vos dé -- pens ?
}
\tag #'(atys basse) {
  % Atys
  Je ne puis me ré -- soudre à cet -- te vi -- o -- len -- ce.
}
\tag #'(doris idas atys basse) {
  % Doris, Atys, Idas
  En vain, un cœur, in -- cer -- tain de son choix,
  met en ba -- lan -- ce mil -- le fois
  l’a -- mour et la re -- con -- nais -- san -- ce,
  l’a -- mour tou -- jours em -- por -- te la ba -- lan -- ce,
  l’a -- mour tou -- jours em -- por -- te la ba -- lan -- ce.
  \tag #'idas {
    L’a -- mour tou -- jours em -- por -- te la ba -- lan -- ce.
    L’a -- mour tou -- jours em -- por -- te la ba -- lan -- ce.
  }
  \tag #'(doris atys basse) {
    L’a -- mour,
    L’a -- mour tou -- jours em -- por -- te la ba -- lan -- ce.
  }
}
\tag #'(atys basse) {
  % Atys
  Le plus jus -- te par -- ti cède en -- fin au plus fort.
  Al -- lez, pre -- nez soin de mon sort,
  que San -- ga -- ride i -- ci se rende en di -- li -- gen -- ce.
}

\version "2.23.4"
\include "common.ily"

\paper {
  %% de la place sous la basse pour pouvoir noter les chiffrages
  score-markup-spacing.padding = #4
  system-system-spacing.padding = #4
  last-bottom-spacing.padding = #4
}
\layout {
  \context {
    \Lyrics
    %% de la place sous les paroles
    \override VerticalAxisGroup.nonstaff-unrelatedstaff-spacing.padding = #3
  }
}

\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header { title = \markup\smallCaps Atys }
  %% Title page
  \markup\null\pageBreak
  %% Table of contents
  \markuplist\abs-fontsize-lines #7
  \override-lines #'(use-rehearsal-numbers . #t)
  \override-lines #'(column-number . 2)
  \table-of-contents \pageBreak
  %% Notes
  \include "notes.ily" \pageBreak
  %% Livret
  \include "livret/livret.ily" \pageBreak
  %% Personnages
  \include "personnages-prologue.ily" \pageBreak
  \include "personnages.ily"
}
%% Musique
\include "prologue.ily"
\include "acte1.ily"
\include "acte2.ily"
\include "acte3.ily"
\include "acte4.ily"
\include "acte5.ily"

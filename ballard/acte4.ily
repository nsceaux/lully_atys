\newBookPart #'()
\act "Acte Quatrième"
\sceneDescription \markup \wordwrap-center {
  [Le théâtre change et représente le palais du Fleuve Sangar.]
}
\scene "Scène Premiere" "SCÈNE 1 : Sangaride, Doris, Idas"
\sceneDescription\markup\smallCaps { Sangaride, Doris, Idas. }
%% 4-1
\pieceToc\markup\wordwrap {
  Récit : \italic { Quoi, vous pleurez ? D’où vient votre peine nouvelle ? }
}
\includeScore "EAArecitQuoiVousPleurez"
\newBookPart#'(full-rehearsal)

\scene "Scène II" "SCÈNE 2 : Sangaride, Célénus"
\sceneDescription\markup\wordwrap-center {
  \smallCaps [Célénus, suivants de Célénus,
  \smallCaps { Sangaride, Idas, et Doris] }
}
%% 4-2
\pieceToc "[Entrée]"
\includeScore "EBAprelude"
\newBookPart#'(haute-contre haute-contre-sol taille quinte)
%% 4-3
\pieceToc\markup\wordwrap {
  Récit : \italic { Belle nymphe, l’hymen va suivre mon envie }
}
\includeScore "EBBbelleNymphe"

\scene "Scène III" "SCÈNE 3 : Atys, Sangaride, Célénus"
\sceneDescription\markup\center-column {
  \line\smallCaps { Célénus, Sangaride, Atys, }
  \line { \smallCaps { [Doris, Idas, } suivants de Célénus.] }
}
%% 4-4
\pieceToc\markup\wordwrap {
  Récit : \italic { Votre cœur se trouble, il soupire }
}
\includeScore "ECArecitVotreCoeurSeTrouble"
\newBookPart#'(full-rehearsal)

\scene "Scène IV" "SCÈNE 4 : Sangaride, Atys"
\sceneDescription\markup\smallCaps { Sangaride, Atys. }
%% 4-5
\pieceToc "Ritournelle"
\includeScore "EDAritournelle"
\newBookPart#'(dessus)
%% 4-6
\pieceToc\markup\wordwrap {
  Récit : \italic { Qu’il sait peu son malheur }
}
\includeScore "EDBrecitQuilSaitPeuSonMalheur"
\newBookPart#'(full-rehearsal)

\scene "Scène V" \markup \wordwrap {
  SCÉNE 5 : Le Fleuve Sangar, les Fleuves
}
\sceneDescription\markup\column {
  \wordwrap-center\smallCaps {
    [Sangaride, Célénus, le dieu du Fleuve Sangar,
  }
  \wordwrap-center\smallCaps {
    Troupe de dieux de fleuves, de ruisseaux,
    et de divinités de fontaines.]
  }
  \smaller\italic\justify {
    [Suite du Fleuve Sangar.
    Douze grands dieux de fleuves chantants.
    Cinq dieux de fleuves jouants de la flûte.
    Quatre divinités de fontaines.
    Deux dieux de fleuves.
    Deux dieux de fleuves dansants ensemble.
    Deux petits dieux de ruisseaux chantants et dansants.
    Quatre petits dieux de ruisseaux dansants.
    Six grands dieux de fleuves dansants.
    Deux vieux dieux de fleuves dansants.
    Deux vieilles nymphes de fontaines dansantes.]
  }
}
%% 4-7
\pieceToc "Prélude"
\includeScore "EEAprelude"
\newBookPart#'(dessus haute-contre haute-contre-sol taille quinte)
%% 4-8
\pieceToc\markup\wordwrap {
  Récit : \italic { Ô vous, qui prenez part au bien de ma famille }
}
\includeScore "EEBOVousQuiPrenezPart"
%% 4-9
\pieceToc\markup\wordwrap {
  Air, chœur : \italic { Que l’on chante, que l’on danse }
}
\includeScore "EECairQueLonChante"
\newBookPart#'(basse)
\sceneDescription\markup\wordwrap-center {
  [Dieux de fleuves, divinités de fontaines, et de ruisseaux,
  chantants et dansants ensemble.]
}
%% 4-10
\pieceToc\markup\wordwrap {
  Air : \italic { La beauté la plus sévère }
}
\includeScore "EEDflutes"
\includeScore "EEDairLaBeauteLaPlusSevere"
\newBookPart#'(basse)
%% 4-11
\pieceToc\markup\wordwrap {
  Air : \italic { L’hymen seul ne saurait plaire }
}
\includeScore "EEEflutes"
\includeScore "EEEairLhymenSeulNeSauraitPlaire"
\newBookPart#'(basse)
\sceneDescription\markup\wordwrap-center {
  [Un dieu de fleuve et une divinité de fontaine, dansent et chantent
  ensemble.]
}
%% 4-12
\pieceToc\markup\wordwrap {
  Air : \italic { D’une constance extrême }
}
\includeScore "EEFmenuet"
\includeScore "EEGduneConstanceExtreme"
%% 4-13
\pieceToc\markup\wordwrap {
  Chœur : \italic { Un grand calme est trop fâcheux }
}
\includeScore "EEHgavotte"
\includeScore "EEIchoeurUnGrandCalmeEstTropFacheux"
\newBookPart#'(full-rehearsal)

\scene "Scène VI" \markup \wordwrap {
  SCÈNE 6 : Atys, Célénus, Sangar, troupe de dieux des fleuves
}
\sceneDescription\markup\wordwrap-center {
  \smallCaps {
    Atys, [troupe de Zéphirs volants,] Sangaride, Célénus, [le Dieu du
    Fleuve Sangar, troupe de dieux de fleuves, de ruisseaux, et de
    diviniés de fontaines.]
  }
}
%% 4-14
\pieceToc\markup\wordwrap {
  Chœur, récit : \italic { Venez formez des nœuds charmants }
}
\includeScore "EFAchoeurVenezFormerDesNoeudsCharmants"
%% 4-15
\pieceToc "Entr’acte"
\includeScore "EFBgavotte"
\actEnd "FIN DU QUATRÈME ACTE"
\markupCond #(eqv? (ly:get-option 'part) 'basse) \markup\vspace#20
<<
  \tag #'(voix1 basse) {
    \cybeleMark re''4 r2 |
    R2.*3 |
    r4 r8 la' re'' re''16 fa'' re''8\trill re''16 mi'' |
    dod''4 dod'' r2 |
    \sangarideMark re''4 r8 la' sib'4 sib'8 sib' |
    sol'4\trill sol'8 la' fa'4\trill fa'8 sol' |
    mi'4\trill r16 la' la' la' fad'8. fad'16 fad'8.\trill sol'16 |
    sol'4 sol'16
    \cybeleMark re''16 re'' re'' sol'8. sol'16 la'8. sib'16 |
    la'4\trill la'8
    \sangarideMark la'8 la'8. la'16 la'8. si'16 |
    do''8. do''16 do''8. do''16 do''4. si'8 |
    do''4
    \cybeleMark r8 sol'16 la' sib'8 sib'16 do'' |
    re''8. re''16 sib'8.\trill sib'16 sib'8. sib'16 |
    la'8\trill la'
    \sangarideMark r8 re''16 re'' sib'8\trill sib'16 do'' |
    la'8.\trill la'16 sol'8.\trill sol'16 la'8. mi'16 |
    fa'8 fa' r re'' si'8.\trill si'16 |
    sold'4\trill sold' r do'' |
    si'8\trill la' la' la' la' sold' |
    la'4
    \cybeleMark r8 mi'' fa'' fa'' r re''16 re'' |
    sib'8. sib'16 sol'8\trill sol' r8 do''16 do'' fa''8 fa''16 do'' |
    re''8 do'' sib' la' sol'4.\trill la'8 |
    fa'4 fa'16
    \sangarideMark la' la' la' sol'8.\trill sol'16 sol'8 fa'16[ mi'] |
    fa'4 r8 re''16 la' sib'8 sib'16 la' sol'8.\trill fa'16 |
    mi'4\trill mi' r16 dod'' dod'' dod'' dod''8. dod''16 |
    re''4 re''8 fa'' dod''4\trill dod''8. re''16 |
    re''4 r
    \cybeleMark r8 fa'16 sol' la'8\trill la'16 si' |
    do''8. do''16 fa''8. si'16 si'8. do''16 |
    do''8 do'' r la' fad' fad'16 fad' sol'8 sol'16 la' |
    sib'8
    \sangarideMark re''16 re'' sib'8\trill sib'16 sib' sol'8.\trill sol'16 sol' sol' sol' la' |
    fa'8. fa'16 fa'8. sol'16 la'8. la'16 sib'8. do''16 |
    la'4\trill la'8 
    \cybeleMark la'8 sol'8.\trill sol'16 sol'8. la'16 |
    sib'4 r16 sib' sib' do'' la'4 re'' |
    dod''8
    \sangarideMark dod''16 dod'' dod''8 dod''16 dod''
    re''8
    \cybeleMark la'8 si'16\trill si' do''? re'' |
    mi''4 mi''8
    \sangarideMark do''8 sol'8. sol'16 sol'8 la' |
    sib'4 la'8. la'16 la'8 la'16 sol' |
    la'2 la' |
    la'8. la'16 sol'8 fa'\trill mi'4 mi'8.\trill fa'16 |
    re'2
    \cybeleMark r4 |
    r2 la'4 |
    re'' re''4. mi''8 |
    dod''2 mi''4 |
    fa'' fa''4. fa''8 |
    re''4\trill re'' re'' |
    mi'' mi''4. mi''8 |
    do''2 re''4 |
    si'4.\trill si'8 mi''4 |
    dod'' re'' mi'' |
    la' la' fa'' |
    re'' mi'' fa'' |
    mi''2\trill do''4 |
    la'4.\trill la'8 fa''4 |
    re''4. mi''8 dod''4 |
    re'' re'' r2 |%%
    <>^\markup\character Cybèle
    do''8. do''16 la'8\trill la'16 do'' fa'8 do''16 do'' fa''8 fa''16 fa'' |
    re''4\trill re''8 re''16 re'' re''8 fa'' |
    sib'4 sib'8 sib'16 sib' sib'4. la'8 |
    %{ do''4 do'' 1ere édition %} la'4\trill la' fa''8. fa''16 re''8\trill re''16 re'' |
    si'4 re''8 re''16 re'' sol'8. sol'16 |
    mi'8\trill mi' r do'' do''8. do''16 re''8. mib''16 |
    re''4\trill sib'8. re''16 sol'8\trill sol'16 la' |
    fa'2. |
  }
  \tag #'voix2 {
    \celaenusMark R2.*4 |
    r4 r8 re fa fa16 re sib8 sib16 sol |
    la4 la r2 |
    \atysMark fa'4 r8 fa' re'4\trill re'8 sol' |
    mi'4\trill mi'8 fa' re'4\trill re'8 mi' |
    dod'4 r16 dod' dod' dod' re'8. re'16 re'8. la16 |
    sib4 sib16
    \celaenusMark sol16 sol sol do'8. do'16 do'8 do |
    fa4 fa8
    \atysMark do'8 do'8. do'16 do'8 re' |
    mi'8. mi'16 mi'8. mi'16 fa'4. sol'8 |
    mi'4\trill
    \celaenusMark r8 do16 do sol8 sol16 la |
    sib8. sib16 sol8. la16 sib8 do' |
    re' re'
    \atysMark r8 fa'16 fa' re'8\trill re'16 mi' |
    fa'8. fa'16 mi'8.\trill mi'16 mi'8. mi'16 |
    la8 la r fa' re'8.\trill re'16 |
    si4\trill si r mi' |
    re'8\trill do' si8. si16 dod'8 re' |
    dod'4
    \celaenusMark r8 la re' re' r sib16 sib |
    sol8.\trill sol16 do'8 do' r la16 la la8 la16 la |
    sib8 la sol fa do'4. do8 |
    fa4 fa16
    \atysMark do'16 do' do' dod'8. dod'16 dod'8. dod'16 |
    re'4 r8 fa'16 fa' fa'8 fa'16 fa' mi'8.\trill re'16 |
    dod'4 dod' r16 mi' mi' mi' mi'8. mi'16 |
    fa'4 fa'8. la'16 mi'4\trill mi'8. fa'16 |
    re'4 r
    \celaenusMark r2 |
    R2. |
    r4 r8 do' do' do'16 do' sib8 sib16 la |
    sol8\trill
    \atysMark sol'16 sol' re'8\trill re'16 sol' mi'8.\trill mi'16 mi' mi' mi' fa' |
    re'8. re'16 re'8. mi'16 fa'8. fa'16 fa'8. mi'16 |
    fa'4 fa'8
    \celaenusMark fa8 do'8. do'16 do'8. do'16 |
    sol4 r16 sol sol sol re'4 re |
    la8
    \atysMark mi'16 mi' mi'8 mi'16 mi' fa'8
    \celaenusMark re8 sol16 sol sol sol |
    do4 do8
    \atysMark mi'8 mi'8. mi'16 mi'8 fad' |
    sol'4 fa'!8. mi'16 re'8 re'16 mi' |
    dod'2 dod' |
    fa'8. fa'16 mi'8.\trill re'16 re'4
    re'8. dod'16 |
    re'2
    \celaenusMark re4 |
    fa fa re |
    sib2 sol4 |
    la4. la8 la,4 |
    re4 re re |
    sol sol4. sol8 |
    do2 do4 |
    fa4. fa8 re4 |
    mi4. mi8 mi4 |
    la si dod' |
    re'2 re'4 si4.\trill si8 sol4 |
    do'4. do'8 do4 |
    fa4 fa fa |
    sib4. sol8 la4 |
    re4 re r2 |
    R1 |
    R2. |
    R1*2 |
    R2. |
    R1 |
    R2.*2 |
  }
>>
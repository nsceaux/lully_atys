\tag #'(voix1 basse) {
  - gé.
}
Ve -- nez vous li -- vrer au sup -- pli -- ce.

Quoi la terre et le ciel con -- tre nous sont ar -- més ?
Souf -- fri -- rez- vous qu’on nous pu -- nis -- se ?

Ou -- bli -- ez- vous votre in -- jus -- ti -- ce ?

Ne vous sou -- vient- il plus de nous a -- voir ai -- més ?

Vous chan -- gez mon a -- mour en hai -- ne lé -- gi -- ti -- me.

Pou -- vez- vous con -- dam -- ner
l’a -- mour qui nous a -- ni -- me ?
Si c’est un cri -- me,
quel crime est plus à par -- don -- ner ?

Per -- fi -- de, de -- viez- vous me tai -- re
que c’é -- tait vai -- ne -- ment que je vou -- lais vous plai -- re ?

Ne pou -- vant sui -- vre vos dé -- sirs,
nous croy -- ons ne pou -- voir mieux fai -- re
que de vous é -- par -- gner de mor -- tels dé -- plai -- sirs.

\tag #'(voix1 basse) {
  D’un sup -- pli -- ce cru -- el crai -- gnez l’hor -- reur ex -- trê -- me.
}
Crai -- gnez un fu -- nes -- te tré -- pas.

Ven -- gez- vous, s’il le faut, ne me par -- don -- nez pas,
mais par -- don -- nez à ce que j’ai -- me.

C’est peu de nous tra -- hir, vous nous bra -- vez, in -- grats ?

Se -- rez- vous sans pi -- tié ?

Per -- dez toute es -- pé -- ran -- ce.

L’a -- mour nous a for -- cé à vous faire une of -- fen -- se,
il de -- man -- de grâ -- ce pour nous.

L’a -- mour en cou -- roux
de -- man -- de ven -- gean -- ce.
L’a -- mour en cou -- roux
de -- man -- de \tag #'(voix1 basse) { ven -- gean -- ce, } ven -- gean -- ce.
L’a -- mour en cou -- roux
de -- man -- de ven -- gean -- ce,
\tag #'voix2 { ven -- gean -- ce, ven -- gean -- ce, }
ven -- gean -- ce.

\tag #'(voix1 basse) {
  Toi, qui por -- tes par -- tout et la rage et l’hor -- reur,
  ces -- se de tour -- men -- ter les cri -- mi -- nel -- les om -- bres,
  viens, cru -- elle A -- lec -- ton, sors des roy -- au -- mes som -- bres,
  ins -- pire au cœur d’A -- tys ta bar -- ba -- re fu -- reur.
}

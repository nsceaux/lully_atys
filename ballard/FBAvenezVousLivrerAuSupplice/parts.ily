\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix"
          #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Cybèle et Célénus
    \livretVerse#8 { Venez vous livrer au supplice. }
    \livretPers Atys et Sangaride
    \livretVerse#12 { Quoi la terre et le ciel contre nous sont armés ? }
    \livretVerse#8 { Souffrirez-vous qu’on nous punisse ? }
    \livretPers Cybèle et Célénus
    \livretVerse#8 { Oubliez-vous votre injustice ? }
    \livretPers Atys et Sangaride
    \livretVerse#12 { Ne vous souvient-il plus de nous avoir aimés ? }
    \livretPers Cybèle et Célénus
    \livretVerse#12 { Vous changez mon amour en haine légitime. }
    \livretPers Atys et Sangaride
    \livretVerse#6 { Pouvez-vous condamner }
    \livretVerse#6 { L’amour qui nous anime ? }
    \livretVerse#4 { Si c’est un crime, }
    \livretVerse#8 { Quel crime est plus à pardonner ? }
    \livretPers Cybèle et Célénus
    \livretVerse#8 { Perfide, deviez-vous me taire }
    \livretVerse#12 { Que c’était vainement que je voulais vous plaire ? }
    \livretPers Atys et Sangaride
    \livretVerse#8 { Ne pouvant suivre vos désirs, }
    \livretVerse#8 { Nous croyons ne pouvoir mieux faire }
    \livretVerse#12 { Que de vous épargner de mortels déplaisirs. }
  }
  \column {
    \livretPers Cybèle
    \livretVerse#12 { D’un supplice cruel craignez l’horreur extrême. }
    \livretPers Cibèle et Célénus
    \livretVerse#8 { Craignez un funeste trépas. }
    \livretPers Atys et Sangaride
    \livretVerse#12 { Vengez-vous, s’il le faut, ne me pardonnez pas, }
    \livretVerse#8 { Mais pardonnez à ce que j’aime. }
    \livretPers Cybèle et Célénus
    \livretVerse#12 { C’est peu de nous trahir, vous nous bravez, ingrats ? }
    \livretPers Atys et Sangaride
    \livretVerse#12 { Serez-vous sans pitié ? }
    \livretPers Cybèle et Célénus
    \livretVerse#12 { \transparent { Serez-vous sans pitié ? } Perdez toute espérance. }
    \livretPers Atys et Sangaride
    \livretVerse#11 { L’amour nous a forcé à vous faire une offense, }
    \livretVerse#8 { Il demande grâce pour nous. }
    \livretPers Cybèle et Célénus
    \livretVerse#5 { L’Amour en couroux }
    \livretVerse#5 { Demande vengeance. }
    \livretPers Cybèle
    \livretVerse#12 { Toi, qui portes partout et la rage et l’horreur, }
    \livretVerse#12 { Cesse de tourmenter les criminelles ombres, }
    \livretVerse#12 { Viens, cruelle Alecton, sors des royaumes sombres, }
    \livretVerse#12 { Inspire au cœur d’Atys ta barbare fureur. }
  }
}#}))

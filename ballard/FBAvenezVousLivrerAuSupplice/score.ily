\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix1 \includeNotes "voix"
    >> \keepWithTag #'voix1 \includeLyrics "paroles"
    \new Staff \with { \haraKiri } \withLyrics <<
      \global \keepWithTag #'voix2 \includeNotes "voix"
    >> \keepWithTag #'voix2 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion {
        s2.*4 s1*7 s1 s2.*5 s1 s2. s1*2 s1 s1*5 s2. s1*7 s2. s1 s1
        s2.*15 s1\break
      }
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}

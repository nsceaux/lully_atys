<<
  \tag #'(recit basse) {
    <<
      \tag #'basse { s1*11 s4 \atysMark }
      \tag #'recit { \clef "vhaute-contre" R1*11 | r4 <>^\markup\character Atys }
    >> r8 mi'16 sol' do'8. do'16 sol8 sol16 do' |
    la8\trill la r16 do' do' do' dod'8. dod'16 dod'8. re'16 |
    re'4 r8 fa'16 fa' la4\trill la8 si |
    do' mi' do'4\trill do'8 do'16 si |
    si4\trill re'16 re' re' mi' la8\trill la16 si |
    sol2 sol |
    <<
      \tag #'basse { s1 s4. }
      \tag #'recit { R1 | r4 r8 }
    >>
    \celaenusMark do'8 la8.\trill la16 la8 do' |
    fa fa fa fa fa mi |
    mi4\trill r8 mi' do' do'16 mi' la8 si16 do' |
    si4\trill si8
    \atysMark re'8 re'8. si16 mi' mi' mi' si |
    do'8 do' r do'16 do' do'8 re'16 mi' |
    fa'4 re'8. do'16 si4\trill si8. do'16 |
    la4
    \sangarMark r16 la la la re'4 fad8 fad16 la |
    re8 re r la16 la sib8 sib16 sib |
    sol8\trill sol sol sol sol la |
    fad4\trill fad8 re16 re sol4 sol8 fad |
    sol2
    <<
      \tag #'basse { s2 s1*6 \atysMark }
      \tag #'recit { r2 R1*6 \atysMarkText "[élevé sur un nuage]" }
    >>
    r4 r8 sol16 sol do'8. do'16 do'8. re'16 |
    mi'4 r8 mi'16 sol' do'8. do'16 do'8. do'16 |
    la4\trill la do'8 do'16 do' re'8. mi'16 |
    fa'4 fa'8. fa'16 mi'4\trill mi'8 fad' |
    sol'4 si8 si16 si mi'8. mi'16 mi'8. mi'16 |
    do'4 r8 fa' re'8.\trill sol'16 sol'8. sol'16 |
    do'4 r8 fa' re'8.\trill re'16 mi'8. fa'16 | \tag#'recit \noBreak
    mi'2\trill mi'4*3/4 \tag #'recit <>^\markup\italic\right-align\line {
      [Les Zéphirs volent, et enlèvent Atys et Sangaride]
    } s16 |
    \tag #'recit { R2.*6 }
  }
  %% Chœur (dessus)
  \tag #'(vdessus basse) {
    \choeurMark r2 r4 mi'' |
    re''4.\trill re''8 mi''4. mi''8 |
    re''4 mi'' do''( si'8) do'' |
    si'2.\trill mi''4 |
    do''2. do''4 |
    re''4. re''8 si'4.\trill si'8 |
    do''4. do''8 si'4. mi''8 |
    dod''2.\trill mi''4 |
    fa''2. fa''4 |
    re''4.\trill re''8 mi''4. sol''8 |
    fa''4. mi''8 re''4.\trill do''8 |
    do''4
    <<
      \tag #'basse { s2. s1*2 s2.*2 s1 \choeurMark }
      \tag #'vdessus { r4 r2 | R1*2 | R2.*2 | R1 | } >>
    mi''2 mi''8. mi''16 mi''8 mi'' |
    fa''4 <<
      \tag #'basse { fa''8 s s2 s2. s1*2 s2. s1*2 s2.*2 s1 s2 \choeurMark }
      \tag #'vdessus { fa''4 r2 | R2. | R1*2 | R2. | R1*2 | R2.*2 | R1 | r2 }
    >> r8 re'' re'' re'' |
    mi''4 r r8 mi'' mi'' mi'' |
    do'' do'' do'' re'' si'4.\trill mi''8 |
    dod''4\trill dod'' r8 dod'' dod'' dod'' |
    re''4 r r8 re'' re'' re''8 |
    si'\trill si' si' do'' la'4. re''8 |
    si'2\trill si' |
    <<
      \tag #'basse { s1*7 s2. \choeurMark }
      \tag #'vdessus { R1*7 | R2. | }
    >>
    mi''4 mi'' mi'' |
    do''2\trill do''4 |
    fa'' fa'' fa'' |
    re''2\trill re''4 |
    re'' re'' sol'' |
    mi''2\trill mi''4 |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" r2 r4 sol' |
    sol'4. sol'8 sol'4. sol'8 |
    sol'4 sol' sol' fad' |
    sol'2. sol'4 |
    fa'2. fa'4 |
    fa'4. fa'8 mi'4. mi'8 |
    mi'4 fa' mi'4. mi'8 |
    mi'2. la'4 |
    la'2. la'4 |
    sol'4. sol'8 sol'4. sol'8 |
    re'4 mi' sol' re' |
    mi' r r2 |
    R1*2 R2.*2 R1 |
    sol'2 sol'8. sol'16 sol'8 sol' |
    la'4 la' r2 |
    R2. R1*2 R2. R1*2 R2.*2 R1 |
    r2 r8 sol' sol' sol' |
    sol'4 r r8 sol' sol' sol' |
    fa' fa' fa' fa' mi'4. mi'8 |
    mi'4 mi' r8 la' la' la' |
    fad'4 r r8 fad' fad' fad' |
    sol' sol' sol' sol' sol'4. fad'8 |
    sol'2 sol' |
    R1*7 R2. |
    sol'4 sol' sol' |
    fa'2 fa'4 |
    la' la' la' |
    sol'2 sol'4 |
    sol' sol' sol' |
    sol'2 sol'4 |
  }
  \tag #'vtaille {
    \clef "vtaille" r2 r4 do' |
    si4. si8 do'4. do'8 |
    re'4 do' do'4. do'8 |
    re'2. do'4 |
    la2. la4 |
    si4. si8 sold4.\trill sold8 |
    la4. la8 la4 sold\trill |
    la2. dod'4 |
    re'2. re'4 |
    si4. si8 do'4. do'8 |
    si4 do' do' si |
    do' r r2 |
    R1*2 R2.*2 R1 |
    do'2 do'8. do'16 do'8 do' |
    do'4 do' r2 |
    R2. R1*2 R2. R1*2 R2.*2 R1 |
    r2 r8 si si si |
    do'4 r r8 do' do' do' |
    la la la la sold4.\trill sold8 |
    la4 la r8 mi' mi' mi' |
    re'4 r r8 la la la |
    sol mi' mi' mi' re'4.\trill re'8 |
    re'2\trill re' |
    R1*7 R2. |
    do'4 do' do' |
    la2\trill la4 |
    re' re' re' |
    si2\trill si4 |
    si si si |
    do'2 do'4 |
  }
  \tag #'vbasse {
    \clef "basse" r2 r4 do |
    sol4. sol8 do'4. do'8 |
    si4 do' la4.\trill sol8 |
    sol2. mi4 |
    fa2. fa4 |
    re4. re8 mi4. mi8 |
    do4 re mi4. mi8 |
    la,2. la4 |
    re2. re4 |
    sol4. fa8 mi4. mi8 |
    re4\trill do sol,4. do8 |
    do4 r r2 |
    R1*2 R2.*2 R1 |
    do'2 do8. do16 do8 do |
    fa4 fa r2 |
    R2. R1*2 R2. R1*2 R2.*2 R1 |
    r2 r8 sol sol sol |
    do'4 r r8 do do do |
    fa fa fa re mi4. mi8 |
    la,4 la, r8 la la la |
    re'4 r r8 re re re |
    mi mi mi do re4. re8 |
    sol,2 sol, |
    R1*7 R2. |
    do4 do do |
    fa2 fa4 |
    re re re |
    sol2 sol4 |
    sol sol sol |
    do'2 do'4 |
  }
>>
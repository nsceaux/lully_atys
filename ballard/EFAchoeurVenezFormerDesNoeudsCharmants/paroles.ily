%% ACTE 4 SCENE 6
% Choeur de dieux de fleuves, et de fontaines.
\tag #'(basse choeur) {
  Ve -- nez, ve -- nez for -- mer des nœuds char -- mants,
  A -- tys, ve -- nez u -- nir ces bien heu -- reux a -- mants.
  A -- tys, ve -- nez u -- nir ces bien heu -- reux a -- mants.
}
\tag #'(recit basse) {
  Cet hy -- men dé -- plaît à Cy -- bè -- le,
  el -- le dé -- fend de l’a -- che -- ver :
  San -- ga -- ride est un bien qu’il faut lui ré -- ser -- ver,
  et que je de -- man -- de pour el -- le.
}
% Choeur
\tag #'(basse choeur) {
  Ah quel -- le loi cru -- el -- le !
}
\tag #'(recit basse) {
  A -- tys peut s’en -- ga -- ger lui- même à me tra -- hir ?
  A -- tys con -- tre moi s’in -- te -- res -- se ?
  
  Sei -- gneur, je suis à la dé -- es -- se,
  dès qu’elle a com -- man -- dé, je ne puis qu’o -- bé -- ïr.
  
  Pour -- quoi faut- il qu’el -- le sé -- pa -- re
  deux il -- lus -- tres a -- mants pour qui l’hy -- men pré -- pa -- re
  ses li -- ens les plus doux ?
}
% Choeur
\tag #'(basse choeur) {
  Op -- po -- sons- nous,
  op -- po -- sons- nous
  à ce des -- sein bar -- ba -- re.
  Op -- po -- sons- nous,
  op -- po -- sons- nous
  à ce des -- sein bar -- ba -- re.
}
\tag #'(recit basse) {
  Ap -- pre -- nez, au -- da -- ci -- eux,
  qu’il n’est rien qui n’o -- bé -- ïs -- se
  aux sou -- ve -- rai -- nes lois de la rei -- ne des dieux.
  Qu’on nous en -- lè -- ve de ces lieux ;
  Zé -- phirs, que sans tar -- der mon or -- dre s’ac -- com -- plis -- se.
}
\tag #'(basse choeur) {
  Quelle in -- jus -- ti -- ce !
  Quelle in -- jus -- ti -- ce !
  Quelle in -- jus -- ti -- ce !
}

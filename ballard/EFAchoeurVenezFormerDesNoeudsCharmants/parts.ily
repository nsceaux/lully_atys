\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Chœur de dieux de fleuves et de fontaines
    \livretVerse#8 { Venez former des nœuds charmants, }
    \livretVerse#12 { Atys, venez unir ces bien heureux amants. }
    \livretPers Atys
    \livretVerse#8 { Cet hymen déplaît à Cybèle, }
    \livretVerse#8 { Elle défend de l’achever : }
    \livretVerse#12 { Sangaride est un bien qu’il faut lui réserver, }
    \livretVerse#8 { Et que je demande pour elle. }
    \livretPers Chœur
    \livretVerse#6 { Ah quelle loi cruelle ! }
    \livretPers Célénus
    \livretVerse#12 { Atys peut s’engager lui-même à me trahir ? }
    \livretVerse#8 { Atys contre moi s’interesse ? }
    \livretPers Atys
    \livretVerse#8 { Seigneur, je suis à la déesse, }
  }
  \column {
    \null
    \livretVerse#12 { Dès qu’elle a commandé, je ne puis qu’obéïr. }
    \livretPers Le Dieu du Fleuve Sangar
    \livretVerse#8 { Pourquoi faut-il qu’elle sépare }
    \livretVerse#12 { Deux illustres amants pour qui l’hymen prépare }
    \livretVerse#6 { Ses liens les plus doux ? }
    \livretPers Chœur
    \livretVerse#4 { Opposons-nous }
    \livretVerse#6 { À ce dessein barbare. }
    \livretPersDidas Atys élevé sur un nuage
    \livretVerse#7 { Apprenez, audacieux, }
    \livretVerse#7 { Qu’il n’est rien qui n’obéïsse }
    \livretVerse#12 { Aux souveraines lois de la reine des dieux. }
    \livretVerse#8 { Qu’on nous enlève de ces lieux ; }
    \livretVerse#12 { Zéphirs, que sans tarder mon ordre s’accomplisse. }
  }
}#}))

\score {
  \new StaffGroup <<
    \new GrandStaff <<
      \new Staff <<
        { s2.*4 s2
          \footnoteHere #'(0 . 0) \markup {
            [Manuscrit] et [Philidor 1703] Reprise des mesures 1 à 5.
          }
        }
        \global \includeNotes "dessus1"
      >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
  \midi { }
}

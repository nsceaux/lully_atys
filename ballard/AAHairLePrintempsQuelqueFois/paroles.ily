Le prin -- temps quel -- que fois est moins doux qu’il ne sem -- ble,
il fait trop pay -- er ses beaux jours ;
jours ;
il vient pour é -- car -- ter les jeux et les a -- mours,
et c’est l’hi -- ver qui les ras -- sem -- ble.
Il - ble.

\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \column {
\livretPers Un Zéphir
\livretVerse#12 { Le printemps quelque fois est moins doux qu’il ne semble, }
\livretVerse#8 { Il fait trop payer ses beaux jours ; }
\livretVerse#12 { Il vient pour écarter les jeux et les amours, }
\livretVerse#8 { Et c’est l’hiver qui les rassemble. }
}#}))


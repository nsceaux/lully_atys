%% ACTE 5 SCENE 3
\tag #'(recit basse) {
  Ciel ! quel -- le va -- peur m’en -- vi -- ron -- ne !
  Tous mes sens sont trou -- blés, je fré -- mis, je fris -- son -- ne,
  je tremble, et tout à coup, une in -- fer -- nale ar -- deur
  vient en -- flam -- mer mon sang, et dé -- vo -- rer mon cœur.
  Dieux ! que vois- je ? le ciel s’ar -- me con -- tre la ter -- re ?
  Quel dé -- sor -- dre ! quel bruit ! quel é -- clat de ton -- ner -- re !
  Quels a -- bî -- mes pro -- fonds sous mes pas sont ou -- verts !
  Que de fan -- tô -- mes vains sont sor -- tis des En -- fers !

  San -- ga -- ri -- de, ah fuy -- ez la mort que vous pré -- pa -- re
  u -- ne di -- vi -- ni -- té bar -- ba -- re :
  c’est vo -- tre seul pé -- ril qui cau -- se ma ter -- reur.

  A -- tys re -- con -- nais -- sez vo -- tre fu -- neste er -- reur.

  Quel mons -- tre vient à nous ! quel -- le fu -- reur le gui -- de !
  Ah res -- pec -- te, cru -- el, l’ai -- ma -- ble San -- ga -- ri -- de.

  A -- tys, mon cher A -- tys.

  Quels hur -- le -- ments af -- freux !

  Fuy -- ez, sau -- vez- vous de sa ra -- ge.

  Il faut com -- bat -- tre ; A -- mour, se -- con -- de mon cou -- ra -- ge.
}
\tag #'(choeur basse) {
  Ar -- rê -- te, ar -- rê -- te mal -- heu -- reux.
  Ar -- rê -- te, ar -- rê -- te mal -- heu -- reux.
}
\tag #'(recit basse) {
  A -- tys !
}
\tag #'(choeur basse) {
  Ô ciel ! ô ciel !
}
\tag #'(recit basse) {
  Je meurs.
}
\tag #'(choeur basse) {
  A -- tys, A -- tys lui- mê -- me,
  fait pé -- rir ce qu’il ai -- me !
}
\tag #'(recit basse) {
  Je n’ai pu re -- te -- nir ses ef -- forts fu -- ri -- eux,
  San -- ga -- ride ex -- pire à vos yeux.

  A -- tys me sa -- cri -- fie une in -- di -- gne ri -- va -- le.
  Par -- ta -- gez a -- vec moi la dou -- ceur sans é -- ga -- le,
  que l’on goûte en ven -- geant un a -- mour ou -- tra -- gé.
  Je vous l’a -- vais pro -- mis.

  Ô pro -- mes -- se fa -- ta -- le !
  San -- ga -- ri -- de n’est plus, et je suis trop ven -- gé.
}


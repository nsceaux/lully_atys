\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Atys
    \livretVerse#8 { Ciel ! quelle vapeur m’environne ! }
    \livretVerse#12 { Tous mes sens sont troublés, je frémis, je frissonne, }
    \livretVerse#12 { Je tremble, et tout à coup, une infernale ardeur }
    \livretVerse#12 { Vient enflammer mon sang, et dévorer mon cœur. }
    \livretVerse#12 { Dieux ! que vois-je ? le ciel s’arme contre la terre ? }
    \livretVerse#12 { Quel désordre ! quel bruit ! quel éclat de tonnerre ! }
    \livretVerse#12 { Quels abîmes profonds sous mes pas sont ouverts ! }
    \livretVerse#12 { Que de fantômes vains sont sortis des Enfers ! }
    \livretVerse#12 { Sangaride, ah fuyez la mort que vous prépare }
    \livretVerse#8 { Une divinité barbare : }
    \livretVerse#12 { C’est votre seul péril qui cause ma terreur. }
    \livretPers Sangaride
    \livretVerse#12 { Atys reconnaissez votre funeste erreur. }
    \livretPers Atys
    \livretVerse#12 { Quel monstre vient à nous ! quelle fureur le guide ! }
    \livretVerse#12 { Ah respecte, cruel, l’aimable Sangaride. }
    \livretPers Sangaride
    \livretVerse#12 { Atys, mon cher Atys. }
    \livretPers Atys
    \livretVerse#12 { \transparent { Atys, mon cher Atys. } Quels hurlements affreux ! }
    \livretPers Célénus
    \livretVerse#8 { Fuyez, sauvez-vous de sa rage. }
    \livretPers Atys
    \livretVerse#12 { Il faut combattre ; Amour, seconde mon courage. }
  }
  \column {
    \livretPers Célénus et le chœur
    \livretVerse#8 { Arrête, arrête malheureux. }
    \livretPers Sangaride
    \livretVerse#12 { Atys ! }
    \livretPers Le chœur
    \livretVerse#12 { \transparent { Atys ! } Ô ciel }
    \livretPers Sangaride
    \livretVerse#12 { \transparent { Atys ! Ô ciel } Je meurs. }
    \livretPers Le Chœur
    \livretVerse#12 { \transparent { Atys ! Ô ciel Je meurs. } Atys, Atys lui-même, }
    \livretVerse#6 { Fait périr ce qu’il aime ! }
    \livretPers Célénus
    \livretVerse#12 { Je n’ai pu retenir ses efforts furieux, }
    \livretVerse#8 { Sangaride expire à vos yeux. }
    \livretPers Cybèle
    \livretVerse#12 { Atys me sacrifie une indigne rivale. }
    \livretVerse#12 { Partagez avec moi la douceur sans égale, }
    \livretVerse#12 { Que l’on goûte en vengeant un amour outragé. }
    \livretVerse#12 { Je vous l’avais promis. }
    \livretPers Célénus
    \livretVerse#12 { \transparent { Je vous l’avais promis. } Ô promesse fatale ! }
    \livretVerse#12 { Sangaride n’est plus, et je suis trop vengé. }
  }
}#}))

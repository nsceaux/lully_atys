<<
  \tag #'(recit basse) {
    \atysMark fa'4 r la8\trill la16 sib do'8 do'16 sol |
    la4 la r8 do'16 do' do'8 re'16 mi' |
    fa'4 r8 re'16 re' si4 r8 si16 re' |
    sol8 sol r sol' mi'8.\trill mi'16 mi'8. mi'16 |
    do'4. do'8 do'8. do'16 sol8. la16 |
    sib4 re'8 re'16 re' mi'8\trill fa' |
    sol'4 mi'8 fa'16 sol' si8. do'16 |
    do'4 r sol' r8 mi' |
    do'4\trill do' r do' |
    fa' la8.\trill la16 la8 sib16 do' |
    fa8 fa r fa'16 fa' re'8\trill re' r fa' |
    sib4 r8 re'16 re' sol'8 sol'16 sol' |
    mi'8[ fa' mi' re' do' re' do' sib]( |
    la8\trill) la r re'16 re' si8 si16 re' |
    sol8 sol16 la sib8 sib16 la la\trill do' do' do' fa'8. fa'16 |
    re'8\trill re'16 fa' sib8 sib16 re' sol4
    \tag #'recit <>^\markup\italic {
      [Il parle à Cybèle, qu’il prend pour Sangaride]
    } r8 sol'16 sol' |
    \tag#'recit \noBreak
    mi'8\trill mi' fa'8. fa'16 re'8.\trill re'16 re' do' sib la |
    sib8 sib sib sib16 sib sib do' re' mi' |
    fa'8 fa' r16 re' re' re' sol'8. re'16 |
    mib'8. mib'16 si8. si16 si8. do'16 |
    do'8
    \sangarideMark sol''8 do''16 do'' do'' do'' sol'\trill sol' sol'
    la'16 sib'8. do''16 |
    la'4\trill
    \tag #'basse \atysMark
    \tag #'recit \atysMarkText\markup { [prenant Sangaride pour un monstre] }
    r8 do' fa'8. fa'16 fa'8. fa'16 | \tag #'recit \noBreak
    re'4\trill re'8 re'16 re' sol'8. sol'16 |
    mi'8\trill mi' r4 fa'8. fa'16 re'8\trill re'16 mi' |
    dod'8 fa' re'\trill re' re' dod' |
    re'4 re'8
    \sangarideMark fa''8 la'4 re''8 re''16 re'' |
    si'4\trill r4
    \atysMark sol'8
    sol'16 sol' si8. re'16 |
    sol4
    \celaenusMark r8 si do' do'16 do' sol8\trill la16 sib? |
    la4 la r2 |
    \tag #'basse \atysMark
    \tag #'recit \atysMarkText\markup {
      tenant à la main le couteau sacré qui sert aux sacrifices]
    }
    r8 fa' re' fa' sib sib r sol' | \tag#'recit \noBreak
    mi'4\trill  r8 la' fa'\trill fa' fa' mi' |
    fa' fa'
    <<
      \tag #'basse { s2. s1*2 s2 \sangarideMark }
      \tag #'recit {
        <>^\markup\italic\column {
          \line { [Atys court après Sangaride qui fuit }
          \line { dans un des côtés du théâtre] }
        } r4 r2 |
        R1 |
        <>^\markup\italic { [Célénus court après Atys] }
        R1 |
        r2 \sangarideMarkText\markup { [dans un des côtés du théâtre] }
      }
    >>
    r4 fa'' | \tag #'recit \noBreak
    re''
    <<
      \tag #'basse { s2. s2 \sangarideMark }
      \tag #'recit { r4 r2 | r2 }
    >>
    r4 r8 do'' |
    la'2\trill r |
    <<
      \tag #'basse { s1*7 \celaenusMark }
      \tag #'recit {
        R1*7 \celaenusMarkText\markup { [revenant sur le théâtre] }
      }
    >>
    r8 do16 do
    sol8 sol16 sol do'8 do'16 mi' do'8\trill do'16 do' |
    la4\trill r8 do'16 do' la8.\trill la16 fa8\trill fa16 fa |
    re8\trill
    \cybeleMark re''8 si'16\trill si' si' si' sol'8\trill sol'16 sol' re'8\trill re'16 mi' |
    fa'8 fa' r re''16 re'' la'8\trill la'16 si' |
    do''4 do''8 do'' do''4 re''8 mi'' |
    la'8\trill la'16 do'' do'' do'' re'' mi'' fa''8 fa''16 fa'' re''8\trill re''16 re'' |
    si'4\trill r r8 sol' sol'16 sol' la' si' |
    do''4
    \celaenusMark mi'8. do'16 sol8 sol16 do' |
    la8\trill la r la16 la re'8 re'16 re' |
    si4\trill mi'8 mi' do'4\trill do'8 si |
    do'1*7/8 s8-\tag #'recit ^\markup\right-align\italic\right-column {
      [Célénus se retire au côté du théâtre, où est Sangaride morte.]
    }
  }
  %% Chœur
  \tag #'(vdessus basse) {
    <<
      \tag #'basse {
        s1*5 s2.*2 s1*2 s2. s1 s2. s1 s2. s1*3 s2.*3 s1*2 s2. s1
        s2. s1*6 s4 \choeurMark
      }
      \tag #'vdessus {
        \clef "vbas-dessus" R1*5 R2.*2 R1*2 R2. R1 R2. R1 R2. R1*3
        R2.*3 R1*2 R2. R1 R2. R1*6 r4 <>^\markup\character Chœur
      }
    >>
    r8 fa''
    re''\trill re'' r re'' |
    sib' sib' sib'8. do''16 la'4\trill r8 fa'' |
    re''4\trill re''8 re'' mi'' mi'' mi''8. mi''16 |
    fa''2
    <<
      \tag #'basse { s2. \choeurMark }
      \tag #'vdessus { r2 | r4 }
    >>
    r8 re'' do''4 r8 fa'' |
    mi''2\trill
    <<
      \tag #'basse { s1. \choeurMark }
      \tag #'vdessus { r2 | R1 | }
    >>
    r2 r4 mib'' |
    re''2\trill r4 mib'' |
    do''2. do''4 |
    si'2\trill si' |
    r do''4. do''8 |
    do''2 si'4.\trill si'8 |
    do''2 do'' |
    \tag #'vdessus { R1*3 R2. R1*3 R2.*2 R1*2 }
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre2" R1*5 R2.*2 R1*2 R2. R1 R2. R1 R2. R1*3
    R2.*3 R1*2 R2. R1 R2. R1*6 |
    r4 r8 la' fa' fa' r fa' |
    sol' sol' sol'8. sol'16 fa'4 r8 la' |
    sol'4 sol'8 sol' sol' sol' sol'8. sol'16 |
    la'2 r |
    r4 r8 fa' fa'4 r8 la' |
    sol'2 r |
    R1 |
    r2 r4 sol' |
    sol'2 r4 sol' |
    sol'2. fa'4 |
    sol'2 sol' |
    r sol'4. sol'8 |
    re'2 re'4. re'8 |
    mib'2 mib' |
    R1*3 R2. R1*3 R2.*2 R1*2 |
  }
  \tag #'vtaille {
    \clef "vtaille" R1*5 R2.*2 R1*2 R2. R1 R2. R1 R2. R1*3
    R2.*3 R1*2 R2. R1 R2. R1*6 |
    r4 r8 do' sib sib r re' |
    re' re' re'8. re'16 re'4 r8 re' |
    si4 si8 si do'8. do'16 do'8. do'16 |
    do'2 r |
    r4 r8 sib la4\trill r8 do' |
    do'2 r |
    R1 |
    r2 r4 do' |
    re'2 r4 do' |
    do'2. fa'4 |
    re'2 re' |
    r do'4. sol8 |
    lab2 sol4. sol8 |
    sol2 sol |
    R1*3 R2. R1*3 R2.*2 R1*2 |
  }
  \tag #'vbasse {
    \clef "vbasse" R1*5 R2.*2 R1*2 R2. R1 R2. R1 R2. R1*3
    R2.*3 R1*2 R2. R1 R2. R1*6 |
    r4 r8 fa sib sib r sib |
    sol sol sol8. sol16 re'4 r8 re |
    sol4 sol8 sol do'8. do'16 do'8. do'16 |
    fa2 r |
    r4 r8 sib, fa4 r8 fa |
    do'2 r |
    R1 |
    r2 r4 do' |
    si2 r4 do' |
    lab2. lab4 |
    sol2 sol |
    r2 mib4. mib8 |
    fa2 sol4. sol8 |
    do2 do |
    R1*3 R2. R1*3 R2.*2 R1*2 |
  }
>>

<<
  \tag #'(vdessus basse) {
    \clef "vdessus" la'4. si'8 do''4 |
    re'' si'2\trill |
    do'' do''4 |
    si'4.\trill si'8 do''4 |
    la' si'2 |
    sold'2.\trill |
    la'4. la'8 re''4 |
    si' mi''2 |
    dod''\trill dod''4 |
    re''4. re''8 re''4 |
    re'' dod''2\trill |
    re''2. |
    do''!4. re''8 do''4 |
    si' la'2 |
    sold'2\trill sold'4 |
    do''4. si'8 la'4 |
    si' sold'2\trill |
    la'2. |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" do'4. re'8 mi'4 |
    fa' re'2\trill |
    mi' mi'4 |
    re'4. re'8 mi'4 |
    do' re'2 |
    si2.\trill |
    do'4. do'8 fa'4 |
    re' sol'2 |
    mi'\trill mi'4 |
    fa'4. sol'8 la'4 |
    sol'8[ fa'] mi'2\trill |
    re'2. |
    mi'4. fa'8 mi'4 |
    re' do'2 |
    si\trill si4 |
    mi'4. re'8 do'4 |
    re' si2\trill |
    la2. |
  }
  \tag #'vbasse {
    \clef "vbasse" <>^\markup { [Philidor 1703] }
    la4. la8 la4 |
    re4 sol2 |
    do2 do4 |
    sol4. fa8 mi4 |
    fa re2\trill |
    mi2. |
    la4. la8 fa4 |
    sol mi2 |
    la2 la4 |
    re4. mi8 fa4 |
    sol la( la,) |
    re2. |
    la4. si8 do'4 |
    sold\trill la2 |
    mi re4 |
    do4. do8 fa4 |
    re mi2 |
    la,2. |
  }
>>

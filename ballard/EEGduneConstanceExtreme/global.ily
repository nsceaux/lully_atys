\set Score.currentBarNumber = 16 \bar ""
\key la \minor
\digitTime\time 3/4 \midiTempo #160
s2.*6 \bar ":|."
s2.*6 \bar "|!:"
s2.*6 \bar "|."
\endMarkSmall\markup {
  [Manuscrit : On reprend le menuet et ensuite le second couplet.]
}

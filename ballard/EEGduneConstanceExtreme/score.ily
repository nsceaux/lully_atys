\score {
  \new StaffGroupNoBar <<
    \new Staff \withLyricsB <<
      \global \keepWithTag #'vdessus \includeNotes "voix"
    >>
    \keepWithTag #'vers1 \includeLyrics "paroles"
    \keepWithTag #'vers2 \includeLyrics "paroles"
    \new Staff \withLyricsB <<
      \global \keepWithTag #'vhaute-contre \includeNotes "voix"
    >>
    \keepWithTag #'vers1 \includeLyrics "paroles"
    \keepWithTag #'vers2 \includeLyrics "paroles"
    \new Staff \with { \tinyStaff } \withLyricsB <<
      \global \keepWithTag #'vbasse \includeNotes "voix"
    >>
    \keepWithTag #'vers1 \includeLyrics "paroles"
    \keepWithTag #'vers2 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}

\tag #'(vers1 basse) {
  D’u -- ne cons -- tance ex -- trê -- me,
  un ruis -- seau suit son cours ;
  il en se -- ra de mê -- me
  du choix de mes a -- mours,
  et du mo -- ment que j’ai -- me
  c’est pour ai -- mer tou -- jours.
}
\tag #'vers2 {
  \override Lyrics . LyricText . font-shape = #'italic
  %\set fontSize = #-1
  [Ja -- mais un cœur vo -- la -- ge
  ne trouve un heu -- reux sort,
  il n’a point l’a -- van -- ta -- ge
  d’ê -- tre long -- temps au port,
  il cherche en -- cor l’o -- ra -- ge
  au mo -- ment qu’il en sort.]
}
\piecePartSpecs
#`((basse #:indent 0
          #:system-count 3
          #:tag-notes basse
          #:score-template "score-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretVerse#6 { D’une constance extrême, }
    \livretVerse#6 { Un ruisseau suit son cours ; }
    \livretVerse#6 { Il en sera de même }
    \livretVerse#6 { Du choix de mes amours, }
    \livretVerse#6 { Et du moment que j’aime }
    \livretVerse#6 { C’est pour aimer toujours. }
  }
  \column {
    \livretVerse#6 { Jamais un cœur volage }
    \livretVerse#6 { Ne trouve un heureux sort, }
    \livretVerse#6 { Il n’a point l’avantage }
    \livretVerse#6 { D’être longtemps au port, }
    \livretVerse#6 { Il cherche encor l’orage }
    \livretVerse#6 { Au moment qu’il en sort. }
  }
}#}))

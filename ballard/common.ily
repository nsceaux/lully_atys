\header {
  copyrightYear = "2010"
  composer = "Jean-Baptiste Lully"
  poet = "Philippe Quinault"
  date = "1676"
  editions = "Version : édition Ballard 1689"
}

%% LilyPond options:
%%  urtext      if true, then print urtext score
%%  part        if a symbol, then print the separate part score
%%  ballard     if true, use Ballard edition source
%%  versailles  if true, use Versailles manuscript source

#(ly:set-option 'original-layout (eqv? (ly:get-option 'urtext) #t))
#(ly:set-option 'use-ancient-clef (eqv? (ly:get-option 'urtext) #t))
#(ly:set-option 'show-ancient-clef (not (symbol? (ly:get-option 'part))))
#(ly:set-option 'print-footnotes #t)
#(ly:set-option 'use-rehearsal-numbers #t)
#(ly:set-option 'apply-vertical-tweaks
                (and (not (eqv? #t (ly:get-option 'urtext)))
                     (not (symbol? (ly:get-option 'part)))))
#(ly:set-option 'print-footnotes
                (not (symbol? (ly:get-option 'part))))
%% Staff size
#(set-global-staff-size (if (symbol? (ly:get-option 'part)) 20 16))

%% Line/page breaking algorithm
%%  optimal   for lead sheets
%%  page-turn for instruments and vocal parts
\paper {
  #(define page-breaking (if (eqv? (ly:get-option 'part) #f)
                             ly:optimal-breaking
                             ly:page-turn-breaking))
}

\language "italiano"
\include "nenuvar-lib.ily"
\include "nenuvar-garamond.ily"

\opusPartSpecs
#`((dessus "Flûtes, hautbois, violons" ()
           (#:notes "dessus"))
   (haute-contre "Hautes-contre" ()
                 (#:notes "haute-contre" #:clef "alto"))
   (haute-contre-sol "Hautes-contre" ()
                     (#:notes "haute-contre" #:clef "treble"))
   (taille "Tailles" ()
           (#:notes "taille" #:clef "alto"))
   (quinte "Quintes" ()
           (#:notes "quinte" #:clef "alto"))
   (basse "Basses, bassons, basse continue" ()
          (#:notes "basse" #:clef "basse"
                   #:score-template "score-basse-continue")))

\opusTitle "Atys"

%% Répéter les tirets dans les paroles après un saut de ligne
\layout {
  \context {
    \Lyrics
    \override LyricHyphen.after-line-breaking = ##t
    %\override LyricHyphen.minimum-distance = #0.6 % default 0.1
  }
}

%%%
tempsMark =
#(define-music-function (parser location) ()
  (make-character-mark "vbasse-taille" "Le Temps"))

floreMark =
#(define-music-function (parser location) ()
  (make-character-mark "vbas-dessus" "Flore"))

zephirMark =
#(define-music-function (parser location) ()
  (make-character-mark "vhaute-contre" "Un Zéphir"))

melpomeneMark =
#(define-music-function (parser location) ()
  (make-character-mark "vbas-dessus" "Melpomène"))

melpomeneMarkText =
#(define-music-function (parser location text) (markup?)
  (make-character-mark-text "vbas-dessus" "Melpomène" text))

irisMark =
#(define-music-function (parser location) ()
  (make-character-mark "vbas-dessus" "Iris"))

%%
atysMark =
#(define-music-function (parser location) ()
  (make-character-mark "vhaute-contre" "Atys"))
atysMarkText =
#(define-music-function (parser location text) (markup?)
  (make-character-mark-text "vhaute-contre" "Atys" text))

idasMark =
#(define-music-function (parser location) ()
  (make-character-mark "vbasse" "Idas"))

idasMarkText =
#(define-music-function (parser location text) (markup?)
  (make-character-mark-text "vbasse" "Idas" text))

sangarideMark =
#(define-music-function (parser location) ()
  (make-character-mark "vbas-dessus" "Sangaride"))
sangarideMarkText =
#(define-music-function (parser location text) (markup?)
  (make-character-mark-text "vbas-dessus" "Sangaride" text))

dorisMark =
#(define-music-function (parser location) ()
  (make-character-mark "vbas-dessus" "Doris"))

dorisIdasMark =
#(define-music-function (parser location) ()
  (make-character-mark "vbas-dessus" "Doris et Idas"))

dorisAtysIdasMark =
#(define-music-function (parser location) ()
  (make-character-mark "vbas-dessus" "Doris, Atys et Idas"))

cybeleMark =
#(define-music-function (parser location) ()
  (make-character-mark "vbas-dessus" "Cybèle"))
cybeleMarkText =
#(define-music-function (parser location text) (markup?)
  (make-character-mark-text "vbas-dessus" "Cybèle" text))

melisseMark =
#(define-music-function (parser location) ()
  (make-character-mark "vbas-dessus" "Mélisse"))

celaenusMark =
#(define-music-function (parser location) ()
  (make-character-mark "vbasse-taille" "Célénus"))
celaenusMarkText =
#(define-music-function (parser location text) (markup?)
  (make-character-mark-text "vbasse-taille" "Célénus" text))

%%
morpheeMark =
#(define-music-function (parser location) ()
  (make-character-mark "vhaute-contre" "Morphée"))

sommeilMark =
#(define-music-function (parser location) ()
  (make-character-mark "vhaute-contre" "Le Sommeil"))

phantaseMark =
#(define-music-function (parser location) ()
  (make-character-mark "vtaille" "Phantase"))

phobetorMark =
#(define-music-function (parser location) ()
  (make-character-mark "vbasse" "Phobétor"))

songeFunesteMark =
#(define-music-function (parser location) ()
  (make-character-mark "vbasse" "Un songe funeste"))

%%
sangarMark =
#(define-music-function (parser location) ()
  (make-character-mark "vbasse" "Le Fleuve Sangar"))

choeurMark =
#(define-music-function (parser location) ()
  (make-character-mark "vbas-dessus" "Chœur"))

%%% Figured bass

%% Always use Ballard: \ballarFigures.  When there is no figured bass
%% in Ballard, use Baussen, with \baussenFigures.  \baussenFigureAlt
%% is to be used when there is a figured bass in Ballard, and an
%% alternative one in Baussen.

ballardFigures =
#(define-music-function (parser location figures) (ly:music?)
   figures)

baussenFigures =
#(define-music-function (parser location figures) (ly:music?)
   (if (eqv? (ly:get-option 'urtext) #t)
       (if (eqv? (ly:get-option 'debug-figures) #t)
           #{ \new FiguredBass \with {
                \override BassFigure #'color = #red
                \override BassFigureContinuation #'color = #red
             } $figures #}
           (make-music 'Music 'void #t))
       figures))

baussenFiguresAlt =
#(define-music-function (parser location figures) (ly:music?)
   (if (eqv? (ly:get-option 'urtext) #t)
       (if (eqv? (ly:get-option 'debug-figures) #t)
           #{ \new FiguredBass \with {
                \override BassFigure #'color = #red
                \override BassFigureContinuation #'color = #red
             } $figures #}
           (make-music 'Music 'void #t))
       (make-music 'Music 'void #t)))

%%% Footnotes
\layout {
  \context {
    \Voice
    \override Script.avoid-slur = #'outside
    %% no line from footnotes to grobs
    \override FootnoteItem.annotation-line = ##f

    \consists Balloon_engraver
    \override BalloonText.annotation-balloon = ##f
  }
  \context {
    \CueVoice
    \override Script.avoid-slur = #'outside
    %% no line from footnotes to grobs
    \override FootnoteItem.annotation-line = ##f
  }
}

footnoteHereFull =
#(define-music-function (parser this-location offset note)
     (number-pair? markup?)
   (if (not (symbol? (ly:get-option 'part)))
       (let ((foot-mus (make-music
                        'FootnoteEvent
                        'X-offset (car offset)
                        'Y-offset (cdr offset)
                        'automatically-numbered #t
                        'text (make-null-markup)
                        'footnote-text note)))
         ;(set! location #f)
         #{ <>-\tweak footnote-music #foot-mus ^\markup\transparent\box "1" #})
       (make-music 'Music 'void #t)))

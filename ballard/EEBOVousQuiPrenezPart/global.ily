\key do \major
\time 4/4 \midiTempo #80 s1*5
\digitTime\time 3/4 s2.
\time 4/4 s1*5
\time 2/2 \midiTempo #160 s1*2 \bar "||"
\time 4/4 \midiTempo #80 s1*3
\time 2/2 \midiTempo #160 s1
\digitTime\time 3/4 \midiTempo #80 s2.
\time 2/2 \midiTempo #160 s1
\digitTime\time 3/4 \midiTempo #80 s2.*2
\time 2/2 \midiTempo #160 s1
\time 6/4 s2. \bar "|."

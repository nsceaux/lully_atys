\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Le Dieu du Fleuve Sangar
    \livretVerse#12 { Ô vous, qui prenez part au bien de ma famille, }
    \livretVerse#12 { Vous, vénérables dieux des fleuves les plus grands, }
    \livretVerse#12 { Mes fidèles amis, et mes plus chers parents, }
    \livretVerse#12 { Voyez quel est l’époux que je donne à ma fille : }
    \livretVerse#12 { J’ai pris soin de choisir entre les plus grands rois. }
    \livretPers Chœur de dieux de fleuves
    \livretVerse#7 { Nous approuvons votre choix. }
  }
  \column {
    \livretPers Le Dieu du Fleuve Sangar
    \livretVerse#8 { Il a Neptune pour son père, }
    \livretVerse#8 { Les Phrygiens suivent ses lois ; }
    \livretVerse#6 { J’ai cru ne pouvoir faire }
    \livretVerse#8 { Un choix plus digne de vous plaire. }
    \livretPers Chœur de dieux de fleuves
    \livretVerse#7 { Tous, d’une commune voix, }
    \livretVerse#7 { Nous approuvons votre choix. }
  }
}#}))

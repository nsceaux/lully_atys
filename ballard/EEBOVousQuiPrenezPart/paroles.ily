%% ACTE 4 SCENE 5
% Le dieu du fleuve Sangar.
\tag #'(sangar basse) {
  Ô vous, qui pre -- nez part au bien de ma fa -- mil -- le,
  vous, vé -- né -- ra -- bles dieux des fleu -- ves les plus grands,
  mes fi -- dè -- les a -- mis, et mes plus chers pa -- rents,
  voy -- ez quel est l’é -- poux que je donne à ma fil -- le :
  j’ai pris soin de choi -- sir en -- tre les plus grands rois.
}
% Choeur de dieux de fleuves.
\tag #'(choeur basse) {
  Nous ap -- prou -- vons vo -- tre choix.
}
% Sangar
\tag #'(sangar basse) {
  Il a Nep -- tu -- ne pour son pè -- re,
  les Phry -- gi -- ens sui -- vent ses lois ;
  j’ai cru ne pou -- voir fai -- re
  un choix plus di -- gne de vous plai -- re.
}
% Choeur de dieux de fleuves.
\tag #'(choeur basse) {
  Tous, d’u -- ne com -- mu -- ne voix,
  nous ap -- prou -- vons vo -- tre choix.
}

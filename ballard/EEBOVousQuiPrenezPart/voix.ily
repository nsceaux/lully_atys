<<
  \tag #'(sangar basse) {
    \sangarMark r4 do' sol8. sol16 sol8. la16 |
    fa4.\trill fa8 fa8. fa16 fa8. mi16 |
    mi4\trill mi sol8 sol16 sol la8. si16 |
    do'4. la8 fad\trill fad fad8. sol16 |
    sol2 r8 re16 re re8 re16 mi |
    fa8. fa16 fa8. fa16 fa8. mi16 |
    mi4\trill r8 do' la8.\trill la16 mi8. fa16 |
    sol4 sol8. sol16 sol4 sol8 fad |
    fad4\trill fad r8 re16 re re8 mi16 fad |
    sol2 la8 la16 la si8 do' |
    si2\trill
    <<
      \tag #'sangar { r2 | R1*2 | <>^\markup\character Sangar }
      \tag #'basse { s2 | s1*2 \sangarMark }
    >>
    r8 do' do' do' mi8.\trill mi16 mi8. fa16 |
    fa8 fa la la16 la fa4\trill fa8 fa16 fa |
    re4\trill r8 sib sol8.\trill sol16 sol8. la16 |
    fad4\trill fad r re |
    sol8. sol16 sol8.\trill sol16 sol8. fad16 |
    sol2 sol |
    \tag #'sangar { R2.*2 | R1 | r2*3/2 }
  }
  \tag #'(vhaute-contre basse) {
    <<
      \tag #'basse { s1*5 s2. s1*4 s2 \ffclef "vhaute-contre" }
      \tag #'vhaute-contre { \clef "vhaute-contre" R1*5 R2. R1*4 r2 }
    >> r8^\markup\character "Chœur des Fleuves" sol' sol' sol' |
    sol'2 sol'4 fa' |
    mi'1\trill |
    <<
      \tag #'basse { s1*4 s2. s1 \ffclef "vhaute-contre" }
      \tag #'vhaute-contre { R1*4 R2. R1 }
    >>
    sol'4 mi'8 mi'16 mi' mi'8. fa'16 |
    re'4\trill r8 sol' sol' sol' |
    sol'2 sol'4 fa' |
    mi'2.\trill
  }
  \tag #'vtaille1 {
    \clef "vtaille" R1*5 R2. R1*4 |
    r2 r8 re'8 re' re' |
    mi'2 mi'4 do' |
    do'1 |
    R1*4 R2. R1 |
    mi'4 do'8 do'16 do' do'8. re'16 |
    si4\trill r8 re' re' re' |
    mi'2 mi'4 do' |
    do'2.
  }
  \tag #'vtaille2 {
    \clef "vtaille" R1*5 R2. R1*4 |
    r2 r8 si si si |
    do'2 do'4 la |
    sol1 |
    R1*4 R2. R1 |
    do'4 sol8 sol16 sol sol8. sol16 |
    sol4 r8 si si si |
    do'2 do'4 la |
    sol2.
  }
  \tag #'vbasse {
    \clef "vbasse" R1*5 R2. R1*4 |
    r2 r8 sol sol sol |
    mi2\trill mi4 fa |
    do1 |
    R1*4 R2. R1 |
    do'4 do8 do16 do do8. do16 |
    sol4 r8 sol sol sol |
    mi2\trill mi4 fa |
    do2.
  }
>>

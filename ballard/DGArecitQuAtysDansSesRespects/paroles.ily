%% ACTE 3 SCENE 7
\tag #'(voix1 basse) {
  Qu’A -- tys dans ses res -- pects mê -- le d’in -- dif -- fé -- ren -- ce !
  L’in -- grat A -- tys ne m’ai -- me pas ;
  l’a -- mour veut de l’a -- mour, tout au -- tre prix l’of -- fen -- se,
  et sou -- vent le res -- pect et la re -- con -- nais -- san -- ce
  sont l’ex -- cu -- se des cœurs in -- grats.
  Et sou -- vent le res -- pect et la re -- con -- nais -- san -- ce
  sont l’ex -- cu -- se des cœurs in -- grats.
  
  Ce n’est pas un si grand cri -- me
  de ne s’ex -- pri -- mer pas bien,
  un cœur qui n’ai -- ma ja -- mais rien
  sait peu com -- ment l’a -- mour s’ex -- pri -- me.
  Un cœur qui n’ai -- ma ja -- mais rien
  sait peu com -- ment l’a -- mour s’ex -- pri -- me.
  
  San -- ga -- ride est ai -- mable, A -- tys peut tout char -- mer,
  ils té -- moi -- gnent trop s’es -- ti -- mer,
  et de sim -- ples pa -- rents sont moins d’in -- tel -- li -- gen -- ce :
  ils se sont ai -- més dès l’en -- fan -- ce,
  ils pour -- raient en -- fin trop s’ai -- mer.
  Je crains une a -- mi -- tié que tant d’ar -- deur a -- ni -- me.
  Rien n’est si trom -- peur que l’es -- ti -- me :
  c’est un nom sup -- po -- sé
  qu’on don -- ne quel -- que -- fois à l’a -- mour dé -- gui -- sé.
  Je pré -- tends m’é -- clair -- cir, leur fein -- te se -- ra vai -- ne.
}
\tag #'(voix2 basse) {
  Quels se -- crets par les dieux ne sont point pé -- né -- trés ?
  Deux cœurs à fein -- dre pré -- pa -- rés
  ont beau ca -- cher leur chaî -- ne,
  deux cœurs à fein -- dre pré -- pa -- rés
  ont beau ca -- cher leur chaî -- ne,
  on a -- buse a -- vec pei -- ne
  les Dieux par l’a -- mour é -- clai -- rés,
  on a -- buse a -- vec pei -- ne
  les Dieux par l’a -- mour é -- clai -- rés.
  
  Va, Mé -- lis -- se, donne ordre à l’ai -- ma -- ble Zé -- phi -- re
  d’ac -- com -- plir promp -- te -- ment tout ce qu’A -- tys dé -- si -- re.
}

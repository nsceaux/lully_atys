\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix"
          #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Cybèle
    \livretVerse#12 { Qu’Atys dans ses respects mêle d’indifférence ! }
    \livretVerse#8 { L’ingrat Atys ne m’aime pas ; }
    \livretVerse#12 { L’Amour veut de l’amour, tout autre prix l’offense, }
    \livretVerse#12 { Et souvent le respect et la reconnaissance }
    \livretVerse#8 { Sont l’excuse des cœurs ingrats. }
    \livretPers Mélisse
    \livretVerse#7 { Ce n’est pas un si grand crime }
    \livretVerse#7 { De ne s’exprimer pas bien, }
    \livretVerse#8 { Un cœur qui n’aima jamais rien }
    \livretVerse#8 { Sait peu comment l’amour s’exprime. }
    \livretPers Cybèle
    \livretVerse#12 { Sangaride est aimable, Atys peut tout charmer, }
    \livretVerse#8 { Ils témoignent trop s’estimer, }
    \livretVerse#12 { Et de simples parents sont moins d’intelligence : }
    \livretVerse#8 { Ils se sont aimés dès l’enfance, }
  }
  \column {
    \null
    \livretVerse#8 { Ils pourraient enfin trop s’aimer. }
    \livretVerse#12 { Je crains une amitié que tant d’ardeur anime. }
    \livretVerse#8 { Rien n’est si trompeur que l’estime : }
    \livretVerse#6 { C’est un nom supposé }
    \livretVerse#12 { Qu’on donne quelquefois à l’amour déguisé. }
    \livretVerse#12 { Je prétends m’éclaircir leur feinte sera vaine. }
    \livretPers Mélisse
    \livretVerse#12 { Quels secrets par les dieux ne sont point pénétrés ? }
    \livretVerse#8 { Deux cœurs à feindre préparés }
    \livretVerse#6 { Ont beau cacher leur chaîne, }
    \livretVerse#6 { On abuse avec peine }
    \livretVerse#8 { Les Dieux par l’amour éclairés. }
    \livretPers Cybèle
    \livretVerse#12 { Va, Mélisse, donne ordre à l’aimable Zéphire }
    \livretVerse#12 { D’accomplir promptement tout ce qu’Atys désire. }
  }
}#}))

\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix1 \includeNotes "voix"
    >> \keepWithTag #'voix1 \includeLyrics "paroles"
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'voix2 \includeNotes "voix"
      { s1*11 s1. s1*4 s1. s1*3 s1*4
        \footnoteHere #'(0 . 0) \markup\column {
          \wordwrap {
            [Manuscrit] Les vers de Mélisse
            \italic { Ce n’est pas un si grand crime de ne s’exprimer pas bien }
            (mesures 21-25) sont chantés deux fois.
          }
          \score {
            \new ChoirStaff <<
              \new Staff \with { autoBeaming = ##f } {
                \tinyQuote \time 2/2 \key do \major \clef "vbas-dessus"
                r2 \bar "|:" do''4 re'' |
                mi'' si' do'' re'' |
                do'' la' do'' si' |
                la' sol' fad'4. mi'8 |
                \alternatives { mi'2*2 } { mi'2. si'4 | }
              }
              \addlyrics {
                Ce n'est pas un si grand cri -- me
                de ne s'ex -- pri -- mer pas bien. bien. Un
              }
              \new Staff {
                \key do \major \clef "basse"
                la,2 la,4 si, |
                do4 re mi mi, |
                la,2 la4 sol |
                fad mi si si, |
                mi8*2[ re do si,] |
                mi4 fa sol mi |
              }
            >>
            \layout { \quoteLayout }
          }
        }
      }
    >> \keepWithTag #'voix2 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
  \midi { }
}
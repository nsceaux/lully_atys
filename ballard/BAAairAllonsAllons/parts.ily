\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \column {
\livretPers Atys
\livretVerse#8 { Allons, allons, accourez tous, }
\livretVerse#6 { Cybèle va descendre. }
\livretVerse#12 { Trop heureux Phrygiens, venez ici l’attendre. }
\livretVerse#8 { Mille peuples seront jaloux }
\livretVerse#6 { Des faveurs que sur nous }
\livretVerse#6 { Sa bonté va répandre. }
}#}))



\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new GrandStaff <<
      \new Staff <<
        { s1*12 \footnoteHere #'(0 . 0) \markup\wordwrap {
            [Manuscrit] et [Philidor 1703] : reprise en fin de la ritournelle.
          }
        }
        \global \includeNotes "dessus1"
      >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
  \midi { }
}

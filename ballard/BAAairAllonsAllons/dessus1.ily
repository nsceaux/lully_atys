\clef "dessus" r4 r8 sol' sib'4. sol'8 |
re''4 mib''8 re'' do'' sib' do'' la' |
sib'4. re''8 re''4.\trill mib''8 |
fa''4 fa''8 sol'' fa'' mib'' re'' do'' |
sib'2 mib''8 fa'' mib'' re'' |
do''4. fa''8 fa''4 fa''8(\trill mib''16 fa'') |
sol''8 fa'' mib'' re'' do''4.\trill sib'8 |
sib'4 fa'' sib''4. sib''8 |
sib''2 la''4. sib''8 |
fad''4\trill sib''8 la'' sol'' fad'' sol'' la'' |
fad''4.\trill( mi''16 fad'') sol''4. re''8 | %fa''8
mib'' re'' do'' sib' la'4.\trill sol'8 |
%sol'4. sol'8 sib'4. sol'8 |
sol'4 r r2 | % sol'1
R1*11 R1*2 R1*7
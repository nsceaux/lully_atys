\cybeleMark r8 si'16 si' sol'8 sol'16 si' fad'8\trill fad'16 sol' la'8 si'16 fad' |
sol'4
\melisseMark r8 mi'' si'8.\trill si'16 si'8. do''16 |
re''8. re''16 re''8. re''16 do''8.\trill si'16 |
la'4\trill
\cybeleMark r8 fad' fad'16 fad' fad' sold' |
la'4 si'8 si'16 si' si'8. do''16 |
do''4 do''8
\melisseMark mi'' la'16\trill la' si' do'' |
si'8.\trill si'16 dod''8 re'' mi''[ red''?16] mi'' |
red''4\trill
\cybeleMark r8 fad'16 fad' si'8 si'16 si' dod''8. red''16 |
mi''8 mi'' r do''16 do'' do''8 do''16 re'' |
si'8.\trill si'16 %{ dod''8. re''16 %} dod''8 re'' re''8 dod'' |
re''4 r8 la' fad'16\trill fad' fad' fad' |
sol'4 sol'8 sol'16 sol' %{ sol'8 sol' %} sol'8. sol'16 |
mi'8\trill mi' do'' do''16 do'' do''8. do''16 |
la'8\trill la'16 la' mi'8 fad'16 sol' fad'4\trill fad' |
r8 re'16 mi' fad'8 fad'16 %{ sold' %} sol'16 la'8. la'16 la' si' do'' la' |
mi''2 si'8 si'16 si' si'8 si' |
mi'4 r8 do''16 do'' la'8\trill la'16 la' |
fad'2\trill si'4 |
sol' la' si' |
la'2\trill fad'4 |
sol'2 mi'4 |
r4 r8 mi'' %{ dod'' dod'' %} dod''8. dod''16 |
re''4 dod''4.\trill si'8 |
si'2 si'8 la' |
sold'2 sold'4 |
la' sol'4. %{ fad'8 %} sol'8 |
fad'2\trill r8 re'' |
re''4 do''4.\trill do''8 |
% do''2 si'8 do'' |
do''4 la' si' |
si'4( la'2)\trill |
sol'2 mi''8 mi'' |
mi''2 r8 re'' |
re''4 do''4.\trill do''8 |
do''2 r8 si' |
si'4 la'4.\trill la'8 |
% la'4( sol') fad'8 sol' |
la'4 fad' sol' |
sol'4( fad'2)\trill |
mi'4
\melisseMark r8 si'16 si' mi''8 mi''16 mi'' |
%{ si'8 r16 %} si'8. si'16 sol'4\trill sol'8 sol'16 fad' |
fad'2.\trill |
sol'4 la' si' |
fad'\trill sol' mi' |
si'2 mi''4 |
do''2 do''8. do''16 |
do''2 si'4 |
si'( la'2)\trill |
% sol'2. |
sol'2 si'8 do'' |
re''2 fad'4 |
sol'4.( fad'8) sol'4 |
fad'2\trill re''8. re''16 |
re''2 do''4 |
do''( si'2)\trill |
la' do''8. do''16 |
do''2 si'4 |
si'4( la'8\trill[ sol']) la'4 |
si'2. |
r4 si'4. si'8 |
do''2 r4 |
r dod''4. dod''8 |
red''2\trill mi''4 |
% mi''4( red''2) |
mi''2( red''4\trill) |
mi''4
\cybeleMark r8 si' sol'4\trill sol'8 la'16 si' |
do''2 do''8\trill do''16 do'' %{ do''8 si' %} do''8. si'16 |
si'4\trill r8 mi'' si'8. si'16 si'8. do''16 |
la'4.\trill la'8 fad'8.\trill fad'16 si'8. fad'16 |
sol'2 sol'4 |
\melisseMark r4 si' si' |
mi''4.( red''8) mi''4 |
red''2\trill si'4 |
fad'\trill sol' la' |
sol'2\trill mi'4 |
%{ r si' si' |
mi''4.( red''8) mi''4 |
red''2 si'4 |
fad'\trill sol' la' |
sol'2\trill mi'4 | %}
r2 mi''4 |
si'\trill si' do'' |
re''2. |
do''4\trill do'' la' |
si'4 si' do'' |
si'( la'2)\trill |
sol'2 si'4 |
%si' dod'' re'' | dod''2. |
si'4 do'' re'' |
do''2.\trill |
la'4 si' do'' |
si' la'4.\trill sol'8 |
sol'4( fad'2)\trill |
mi'4 r si' si'16 si' si' do'' |
la'4.\trill la'16 sol' fad'4\trill fad'8 sol' |
mi'4
\cybeleMark mi'' r dod''16 dod'' dod'' dod'' |
lad'4 lad'8 si' si'4 si'8 lad' |
si'2 mi''8 mi''16 mi'' do'' do'' do'' do'' |
la'8\trill la' r la'16 la' la'8 si'16 do'' |
re''8. re''16 re''8. do''16 do''8. si'16 |
si'4\trill r8 sol'16 la' si'8\trill si'16 si' do''8. re''16 |
mi''4 r8 do''16 mi'' do''8\trill do''16 do'' |
do''4 si'8. do''16 la'4\trill la'8. si'16 |
sol'8 sol' r sol' si'8. si'16 si'8 dod'' |
re''4 re''8. re''16 re''4 re''8 dod'' |
re''4 r8 si'16 si' sold'4 %{ sold'8. sold'16 %} sold'8 sold' |
la'4. la'8 re''8. %{ do''16 do''8\trill si' %} re''16 do''8.\trill si'16 |
do''8 do'' r mi''16 re'' do''8 do''16 si' |
la'8.\trill la'16 la'8. la'16 sol'8.\trill fad'16 |
sol'8 sol' r si' mi''8. mi''16 mi''8. si'16 |
do''4 do''8 si' la'4\trill la'8 sol' |
fad'4\trill r8 si' si'8. si'16 dod''8. re''16 |
dod''8. dod''16 dod''8. dod''16 red''8 mi'' mi''[ red''] |
mi''2 r |
r4 r8 si'16 re'' si'8\trill si'16 si' |
sol'4 r8 sol'16 sol' do''8 do''16 do'' |
la'4\trill la'16 la' la' si' do''8 do''16 re'' |
si'4\trill si' r si' |
dod''8 dod'' re'' re'' re'' dod'' |
re''4 r8 re' la' la'16 si' do''8 do''16 si' |
si'4\trill si'8 si'16 si' do''8. re''16 |
mi''4 r8 do''16 si'16 la'4\trill la'8. si'16 |
sol'4 sol' <>^\markup\italic { [Mélisse se retire] } r2 |
r sol'8 sol'16 sol' sol'8 sol' |
mi'4 r8 mi'16 fad' sol'8 sol'16 la' |
si'4 sol'8 sol'16 la' si'\trill si' do'' re'' |
mi''4 r8 do''16 mi'' do''8\trill do''16 si' la'8.\trill %{ sol'16 %} la'16 |
fad'2\trill fad'4 r8 re' |
la'2 la'4 si' |
do''2 do''4 la' |
mi''2 mi'' |
re''4 re''8 do'' si'4. la'8 |
sold'2 si'4. si'8 |
do''2 si'4.\trill la'8 |
la'2 r4 mi'' |
dod'' dod'' dod'' re'' |
re''2. dod''4 |
re''2 re''4 do'' |
si'2 la'4 sol' |
fad'2.\trill fad'4 |
sol'2 la'4 si' |
si'2( la')\trill |
sol'2 r4

\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Cybèle
    \livretRef#'CCArecitTuTetonnesMelisse
    \livretVerse#12 { Tu t’étonnes, Mélisse, et mon choix te surprend ? }
    \livretPers Mélisse
    \livretVerse#12 { Atys vous doit beaucoup, et son bonheur est grand. }
    \livretPers Cybèle
    \livretVerse#12 { J’ai fait encor pour lui plus que tu ne peux croire. }
    \livretPers Mélisse
    \livretVerse#12 { Est-il pour un mortel un rang plus glorieux ? }
    \livretPers Cybèle
    \livretVerse#8 { Tu ne vois que sa moindre gloire ; }
    \livretVerse#12 { Ce mortel dans mon cœur est au dessus des Dieux. }
    \livretVerse#12 { Ce fut au jour fatal de ma dernière fête }
    \livretVerse#12 { Que de l’aimable Atys je devins la conquête : }
    \livretVerse#12 { Je partis à regret pour retourner aux Cieux, }
    \livretVerse#12 { Tout m’y parut changé, rien n’y plut à mes yeux. }
    \livretVerse#7 { Je sens un plaisir extrême }
    \livretVerse#7 { À revenir dans ces lieux ; }
    \livretVerse#8 { Où peut-on jamais être mieux, }
    \livretVerse#8 { Qu’aux lieux où l’on voit ce qu’on aime. }
    \livretPers Mélisse
    \livretVerse#12 { Tous les Dieux ont aimé, Cybèle aime à son tour. }
    \livretVerse#7 { Vous méprisiez trop l’Amour, }
    \livretVerse#7 { Son nom vous semblait étrange, }
    \livretVerse#7 { À la fin il vient un jour }
    \livretVerse#5 { Où l’Amour se venge. }
    \livretPers Cybèle
    \livretVerse#12 { J’ai crû me faire un cœur maître de tout son sort, }
    \livretVerse#12 { Un cœur toujours exempt de trouble et de tendresse. }
  }
  \column {
    \livretPers Mélisse
    \livretVerse#5 { Vous braviez à tort }
    \livretVerse#5 { L’Amour qui vous blesse ; }
    \livretVerse#5 { Le cœur le plus fort }
    \livretVerse#7 { À des moments de faiblesse. }
    \livretVerse#12 { Mais vous pouviez aimer, et descendre moins bas. }
    \livretPers Cybèle
    \livretVerse#12 { Non, trop d’égalité rend l’amour sans appas. }
    \livretVerse#8 { Quel plus haut rang ai-je à prétendre ? }
    \livretVerse#12 { Et de quoi mon pouvoir ne vient-il point à bout ? }
    \livretVerse#8 { Lorsqu’on est au dessus de tout, }
    \livretVerse#12 { On se fait pour aimer un plaisir de descendre. }
    \livretVerse#12 { Je laisse aux Dieux les biens dans le Ciel preparés, }
    \livretVerse#12 { Pour Atys, pour son cœur, je quitte tout sans peine, }
    \livretVerse#12 { S’il m’oblige à descendre, un doux penchant m’entraîne ; }
    \livretVerse#12 { Les cœurs que le destin à le plus separés, }
    \livretVerse#12 { Sont ceux qu’Amour unit d’une plus forte chaîne. }
    \livretVerse#12 { Fais venir le Sommeil ; que lui-même en ce jour, }
    \livretVerse#8 { Prenne soin ici de conduire }
    \livretVerse#8 { Les Songes qui lui font la cour ; }
    \livretVerse#8 { Atys ne sait point mon amour, }
    \livretVerse#12 { Par un moyen nouveau je prétends l’en instruire. }

    \livretVerse#12 { Que les plus doux Zéphirs, que les peuples divers, }
    \livretVerse#8 { Qui des deux bouts de l’Univers }
    \livretVerse#8 { Sont venus me montrer leur zèle, }
    \livretVerse#8 { Célèbrent la gloire immortelle }
    \livretVerse#12 { Du sacrificateur dont Cybèle a fait choix, }
    \livretVerse#8 { Atys doit dispenser mes lois, }
    \livretVerse#8 { Honorez le choix de Cybèle. }
  }
}#}))

%% ACTE 2 SCENE 3
Tu t’é -- ton -- nes, Mé -- lisse, et mon choix te sur -- prend ?

A -- tys vous doit beau -- coup, et son bon -- heur est grand.

J’ai fait en -- cor pour lui plus que tu ne peux croi -- re.

Est- il pour un mor -- tel un rang plus glo -- ri -- eux ?

Tu ne vois que sa moin -- dre gloi -- re ;
ce mor -- tel dans mon cœur est au des -- sus des dieux.
Ce fut au jour fa -- tal de ma der -- niè -- re fê -- te
que de l’ai -- mable A -- tys je de -- vins la con -- quê -- te :
je par -- tis à re -- gret pour re -- tour -- ner aux cieux,
tout m’y pa -- rut chan -- gé, rien n’y plut à mes yeux.
Je sens un plai -- sir ex -- trê -- me
à re -- ve -- nir dans ces lieux ;
où peut- on ja -- mais ê -- tre mieux,
qu’aux lieux où l’on voit ce qu’on ai -- me.
Où peut- on ja -- mais ê -- tre mieux,
qu’aux lieux où l’on voit ce qu’on ai -- me.

Tous les Dieux ont ai -- mé, Cy -- bèle aime à son tour.
Vous mé -- pri -- siez trop l’a -- mour,
son nom vous sem -- blait é -- tran -- ge,
à la fin il vient un jour
où l’a -- mour se van -- ge.
À la fin il vient un jour
où l’a -- mour,
où l’a -- mour se ven -- ge.

J’ai crû me faire un cœur maî -- tre de tout son sort,
un cœur tou -- jours ex -- empt de trouble et de ten -- dres -- se.

Vous bra -- viez à tort
l’a -- mour qui vous bles -- se ;
le cœur le plus fort
à des mo -- ments de fai -- bles -- se.
Le cœur le plus fort
à des mo -- ments de fai -- bles -- se.
Mais vous pou -- viez ai -- mer, et des -- cen -- dre moins bas.

Non, trop d’é -- ga -- li -- té rend l’a -- mour sans ap -- pas.
Quel plus haut rang ai-je à pré -- ten -- dre ?
et de quoi mon pou -- voir ne vient- il point à bout ?
Lors -- qu’on est au des -- sus de tout,
on se fait pour ai -- mer un plai -- sir de des -- cen -- dre.
Je laisse aux dieux les biens dans le ciel pre -- pa -- rés,
pour A -- tys, pour son cœur, je quit -- te tout sans pei -- ne,
s’il m’o -- blige à des -- cendre, un doux pen -- chant m’en -- traî -- ne ;
les cœurs que le des -- tin à le plus se -- pa -- rés,
sont ceux qu’A -- mour u -- nit d’u -- ne plus for -- te chaî -- ne.
Fais ve -- nir le Som -- meil ; que lui- même en ce jour,
pren -- ne soin i -- ci de con -- dui -- re
les son -- ges qui lui font la cour ;
A -- tys ne sait point mon a -- mour,
par un moy -- en nou -- veau je pré -- tends l’en ins -- trui -- re.

% Melisse se retire.
Que les plus doux Zé -- phirs, que les peu -- ples di -- vers,
qui des deux bouts de l’U -- ni -- vers
sont ve -- nus me mon -- trer leur zè -- le,
cé -- lè -- brent la gloire im -- mor -- tel -- le
du sa -- cri -- fi -- ca -- teur dont Cy -- bèle a fait choix,
A -- tys doit dis -- pen -- ser mes lois,
ho -- no -- rez, ho -- no -- rez le choix de Cy -- bè -- le.

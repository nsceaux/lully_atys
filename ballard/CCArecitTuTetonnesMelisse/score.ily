\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      { s2.*5 s1*3 s2.*39
        \footnoteHere #'(0 . 0) \markup\column {
          \wordwrap {
            [Manuscrit] Les vers
            \italic {
              Vous méprisiez trop l’amour,
              son nom vous semblait étrange
            }
            sont chantés deux fois.
          }
          \score {
            \new ChoirStaff <<
              \new Staff {
                \tinyQuote \time 3/4 \set autoBeaming = ##f
                \key sol \major \clef "vbas-dessus"
                \repeat volta 2 {
                  sol'4 la' si' | fad' sol' mi' |
                  si'2 mi''4 | do''2 do''8. do''16 |
                  do''2 si'4 | si'( la'2) |
                } \alternative { { sol'2. | \bar ":|" } { sol'2 si'8 do'' | } }
              } \addlyrics {
                Vous mé -- pri -- siez trop l’a -- mour,
                son nom vous sem -- bloit es -- tran -- ge, - ge, à la
              }
              \new Staff {
                \key sol \major \clef "vbasse"
                mi2. | red4 mi do |
                si, si8 la sold4 | la4. la,8 si, do |
                re2 sol,4~ | sol, re,2 |
                sol,4 sol8 la sol fad | sol,4 sol2 |
              }
            >>
            \layout { \quoteLayout indent = 5\mm }
          }
        }
        s2.*15 s1*4 s2.*5 s2
        \footnoteHere #'(0 . 0) \markup\column {
          \wordwrap {
            [Manuscrit] Les vers
            \italic { Vous braviez à tord l’amour qui vous blesse }
            sont chantés deux fois.
          }
          \score {
            \new ChoirStaff <<
              \new Staff {
                \tinyQuote \time 3/4 \set autoBeaming = ##f
                \key sol \major \clef "vbas-dessus"
                r4 si' si' | mi''4.( red''8) mi''4 |
                red''2 si'4 | fad' sol' la' |
                sol'2 mi'4 | r si' si' |
                mi''4.( red''8) mi''4 | red''2 si'4 |
                fad' sol' la' | sol'2 mi'4 |
              } \addlyrics {
                Vous bra -- viez à tort l'A -- mour qui vous bles -- se,
                Vous bra -- viez à tort l'A -- mour qui vous bles -- se,
              }
              \new Staff {
                \key sol \major \clef "vbasse"
                mi'2 re'4 | do'2. |
                si4. la8 sol la | si4 si,2 |
                mi2. | mi'2 re'4 |
                do'2. | si4. la8 sol la |
                si4 si,2 | mi,2. |
              }
            >>
            \layout { \quoteLayout indent = 5\mm }
          }
        }
      }
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
  \midi { }
}
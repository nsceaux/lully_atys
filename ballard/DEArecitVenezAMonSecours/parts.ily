\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Atys
    \livretVerse#12 { Venez à mon secours ô dieux ! ô justes dieux ! }
    \livretPers Cybèle
    \livretVerse#12 { Atys, ne craignez rien, Cybèle est en ces lieux. }
    \livretPers Atys
    \livretVerse#12 { Pardonnez au désordre où mon cœur s’abandonne ; }
    \livretVerse#12 { C’est un songe… }
    \livretPers Cybèle
    \livretVerse#12 { \transparent { C’est un songe… } Parlez, quel songe vous étonne ? }
    \livretVerse#8 { Expliquez-moi votre embarras. }
    \livretPers Atys
    \livretVerse#12 { Les songes sont trompeurs, et je ne les crois pas. }
    \livretVerse#6 { Les plaisirs et les peines }
    \livretVerse#8 { Dont en dormant on est séduit, }
    \livretVerse#6 { Sont des chimères vaines }
    \livretVerse#6 { Que le réveil détruit. }
    \livretPers Cybèle
    \livretVerse#8 { Ne méprisez pas tant les songes }
    \livretVerse#8 { L’Amour peut emprunter leur voix, }
  }
  \column {
    \null
    \livretVerse#7 { S’ils font souvent des mensonges }
    \livretVerse#7 { Ils disent vrai quelquefois. }
    \livretVerse#12 { Ils parlaient par mon ordre, et vous les devez croire. }
    \livretPers Atys
    \livretVerse#12 { O Ciel ? }
    \livretPers Cybèle
    \livretVerse#12 { \transparent { O Ciel ? } N’en doutez point, connaissez votre gloire. }
    \livretVerse#8 { Répondez avec liberté, }
    \livretVerse#12 { Je vous demande un cœur qui dépend de lui-même. }
    \livretPers Atys
    \livretVerse#8 { Une grande divinité }
    \livretVerse#12 { Doit d’assurer toujours de mon respect extrême. }
    \livretPers Cybèle
    \livretVerse#8 { Les dieux dans leur grandeur suprême }
    \livretVerse#12 { Reçoivent tant d’honneurs qu’ils en sont rebutés, }
    \livretVerse#12 { Ils se lassent souvent d’être trop respectés, }
    \livretVerse#8 { Ils sont plus contents qu’on les aime. }
  }
}#}))

\key fa \major
\time 4/4 \midiTempo#80 s1*4
\digitTime\time 3/4 s2.
\time 2/2 \midiTempo#160 s1
\time 4/4 \midiTempo#80 s1*5
\digitTime\time 3/4 \midiTempo#160 s2.*25
\time 4/4 \midiTempo#80 s1*7
\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2.
\time 2/2 \midiTempo#160 s1*18

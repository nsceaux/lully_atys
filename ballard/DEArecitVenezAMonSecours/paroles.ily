Ve -- nez à mon se -- cours ô dieux ! ô jus -- tes dieux !

A -- tys, ne crai -- gnez rien, Cy -- bèle est en ces lieux.

Par -- don -- nez au dé -- sordre où mon cœur s’a -- ban -- don -- ne ;
c’est un son -- ge…

Par -- lez, quel son -- ge vous é -- ton -- ne ?
Ex -- pli -- quez- moi votre em -- bar -- ras.

Les son -- ges sont trom -- peurs, et je ne les crois pas.
Les plai -- sirs et les pei -- nes
dont en dor -- mant on est sé -- duit,
sont des chi -- mè -- res vai -- nes
que le ré -- veil dé -- truit.

Ne mé -- pri -- sez pas tant les son -- ges
l’a -- mour peut em -- prun -- ter leur voix,
s’ils font sou -- vent des men -- son -- ges
ils di -- sent vrai quel -- que -- fois.
Ils par -- laient par mon ordre, et vous les de -- vez croi -- re.

O Ciel ?

N’en dou -- tez point, con -- nais -- sez vo -- tre gloi -- re.
Ré -- pon -- dez a -- vec li -- ber -- té,
je vous de -- mande un cœur qui dé -- pend de lui- mê -- me.

U -- ne gran -- de di -- vi -- ni -- té
doit d’as -- su -- rer tou -- jours de mon res -- pect ex -- trê -- me.

Les dieux dans leur gran -- deur su -- prê -- me
re -- çoi -- vent tant d’hon -- neurs qu’ils en sont re -- bu -- tés,
ils se las -- sent sou -- vent d’ê -- tre trop res -- pec -- tés,
ils sont plus con -- tents qu’on les ai -- me.
Ils sont plus con -- tents qu’on les ai -- me.

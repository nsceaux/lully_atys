\score {
  <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \keepWithTag #'() \global
      \keepWithTag #'part-dessus \includeNotes "voix"
    >> { \set fontSize = -2 \keepWithTag #'basse \includeLyrics "paroles" }
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff <<
        \keepWithTag #'() \global
        \keepWithTag #'() \includeNotes "dessus1"
      >>
      \new Staff <<
        \keepWithTag #'() \global
        \keepWithTag #'() \includeNotes "dessus2"
      >>
    >>
  >>
  \layout { }
}
\markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Atys
    \livretVerse#12 { Non tu dois mieux juger du parti que je prends. }
    \livretVerse#12 { Mon cœur veut fuir toujours les soins et les mystères ; }
    \livretVerse#12 { J’aime l’heureuse paix des cœurs indifférents ; }
    \livretVerse#8 { Si leurs plaisirs ne sont pas grands, }
    \livretVerse#8 { Au moins leurs peines sont légères. }
    \livretPers Idas
    \livretVerse#8 { Tôt ou tard l’amour est vainqueur, }
    \livretVerse#8 { En vain les plus fiers s’en défendent, }
    \livretVerse#8 { On ne peut refuser son cœur }
    \livretVerse#8 { À de beaux yeux qui le demandent. }
    \livretVerse#12 { Atys, ne feignez plus, je sais votre secret. }
    \livretVerse#8 { Ne craignez rien, je suis discret. }
    \livretVerse#8 { Dans un bois solitaire, et sombre, }
    \livretVerse#12 { L’indifférent Atys se croyait seul, un jour ; }
    \livretVerse#12 { Sous un feuillage épais où je rêvais à l’ombre, }
    \livretVerse#8 { Je l’entendis parler d’amour. }
  }
  \column {
    \livretPers Atys
    \livretVerse#12 { Si je parle d’amour, c’est contre son empire, }
    \livretVerse#8 { J’en fais mon plus doux entretien. }
    \livretPers Idas
    \livretVerse#8 { Tel se vante de n’aimer rien, }
    \livretVerse#8 { Dont le cœur en secret soupire. }
    \livretVerse#12 { J’entendis vos regrets, et je les sais si bien }
    \livretVerse#12 { Que si vous en doutez je vais vous les redire. }
    \livretVerse#12 { Amants qui vous plaignez, vous êtes trop heureux : }
    \livretVerse#12 { Mon cœur de tous les cœurs est le plus amoureux, }
    \livretVerse#12 { Et tout près d’expirer je suis réduit à feindre ; }
    \livretVerse#8 { Que c’est un tourment rigoureux }
    \livretVerse#8 { De mourir d’amour sans se plaindre ! }
    \livretVerse#12 { Amants qui vous plaignez, vous êtes trop heureux. }
    \livretPers Atys
    \livretVerse#12 { Idas, il est trop vrai, mon cœur n’est que trop tendre, }
    \livretVerse#12 { L’Amour me fait sentir ses plus funestes coups. }
    \livretVerse#12 { Qu’aucun autre que toi n’en puisse rien apprendre. }
  }
}

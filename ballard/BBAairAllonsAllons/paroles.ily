\tag #'(voix1 basse) {
  Al -- lons, al -- lons, ac -- cou -- rez tous,
  al -- lons, al -- lons, ac -- cou -- rez tous,
  Cy -- bè -- le va des -- cen -- dre.
  Al -- lons, al -- lons, ac -- cou -- rez tous,
  al -- lons, al -- lons, ac -- cou -- rez tous,
  Cy -- bè -- le va des -- cen -- dre.
}
\tag #'voix2 {
  Al -- lons, al -- lons, ac -- cou -- rez tous, ac -- cou -- rez tous,
  Cy -- bè -- le va des -- cen -- dre.
  Al -- lons, al -- lons, ac -- cou -- rez tous, ac -- cou -- rez tous,
  Cy -- bè -- le va des -- cen -- dre.
}
% Atys
Le so -- leil peint nos champs des plus vi -- ves cou -- leurs,
il a sé -- ché les pleurs
que sur l’é -- mail des prés a ré -- pan -- du l’au -- ro -- re ;
et ses ray -- ons nou -- veaux ont dé -- jà fait é -- clo -- re
mil -- le nou -- vel -- les fleurs.
Et ses ray -- ons nou -- veaux ont dé -- jà fait é -- clo -- re
mil -- le nou -- vel -- les fleurs.
% Idas
Vous veil -- lez lors -- que tout som -- meil -- le ;
vous nous é -- veil -- lez si ma -- tin
que vous fe -- rez croire à la fin
que c’est l’A -- mour qui vous é -- veil -- le,
que vous fe -- rez croire à la fin
que c’est l’A -- mour qui vous é -- veil -- le.
% Atys
Non tu dois mieux ju -- ger du par -- ti que je prends.
Mon cœur veut fuir tou -- jours les soins et les mys -- tè -- res ;
j’ai -- me l’heu -- reu -- se paix des cœurs in -- dif -- fé -- rents ;
si leurs plai -- sirs ne sont pas grands,
au moins leurs pei -- nes sont lé -- gè -- res.
Si leurs plai -- sirs ne sont pas grands,
au moins leurs pei -- nes sont lé -- gè -- res.
% Idas
Tôt ou tard l’a -- mour est vain -- queur,
en vain les plus fiers s’en dé -- fen -- dent,
tôt ou tard l’a -- mour est vain -- queur,
en vain les plus fiers s’en dé -- fen -- dent.
On ne peut re -- fu -- ser son cœur
à de beaux yeux qui le de -- man -- dent,
on ne peut re -- fu -- ser son cœur
à de beaux yeux qui le de -- man -- dent.
A -- tys, ne fei -- gnez plus, je sais vo -- tre se -- cret.
Ne crai -- gnez rien, je suis dis -- cret.
Dans un bois so -- li -- taire, et som -- bre,
l’in -- dif -- fé -- rent A -- tys se croy -- ait seul, un jour ;
sous un feuil -- lage é -- pais où je rê -- vais à l’om -- bre,
je l’en -- ten -- dis par -- ler d’a -- mour.
% Atys
Si je par -- le d’a -- mour, c’est con -- tre son em -- pi -- re,
j’en fais mon plus doux en -- tre -- tien.
% Idas
Tel se van -- te de n’ai -- mer rien,
dont le cœur en se -- cret sou -- pi -- re.
J’en -- ten -- dis vos re -- grets, et je les sais si bien
que si vous en dou -- tez je vais vous les re -- di -- re.
A -- mants qui vous plai -- gnez, vous ê -- tes trop heu -- reux :
mon cœur de tous les cœurs est le plus a -- mou -- reux,
et tout près d’ex -- pi -- rer je suis ré -- duit à fein -- dre ;
que c’est un tour -- ment ri -- gou -- reux
de mou -- rir d’a -- mour sans se plain -- dre !
A -- mants qui vous plai -- gnez, vous ê -- tes trop heu -- reux.
% Atys
I -- das, il est trop vrai, mon cœur n’est que trop ten -- dre,
l’a -- mour me fait sen -- tir ses plus fu -- nes -- tes coups.
Qu’au -- cun au -- tre que toi n’en puis -- se rien ap -- pren -- dre.

###
### Configuration
###
OUTPUT_DIR=out
DELIVERY_DIR=../delivery
PROJECT=Lully_Atys_Ballard

NENUVAR_LIB_PATH:=$(shell pwd)/../../../nenuvar-lib
LILYPOND_OPTIONS=--loglevel=WARN -ddelete-intermediate-files -dno-protected-scheme-parsing
LILYPOND_CMD=lilypond -I$(shell pwd) -I$(NENUVAR_LIB_PATH) $(LILYPOND_OPTIONS)

###
### Scores and output files
###
PO_SCORE=PO
PARTS_auto=dessus haute-contre haute-contre-sol taille quinte basse
PARTS=$(PARTS_auto)
ALL_SCORES=$(PARTS) $(PO_SCORE)

OUTPUT_FILES=$(patsubst %,$(PROJECT)_%.pdf,$(ALL_SCORES)) $(PROJECT)_livret.html $(PROJECT)_livret.txt
conducteur:
	$(LILYPOND_CMD) -o '$(OUTPUT_DIR)/$(PROJECT)_$(PO_SCORE)' main.ly
.PHONY: conducteur

define PART_template
 $(1):
	$(LILYPOND_CMD) -o '$(OUTPUT_DIR)/$(PROJECT)_$(1)' -dpart=$(1) part.ly
 .PHONY: $(1)
endef
$(foreach part,$(PARTS_auto),$(eval $(call PART_template,$(part))))

parts: $(PARTS)
.PHONY: parts

define DELIVER_FILE_template
	if [ -e '$(OUTPUT_DIR)/$(1)' ] ; then rm -f '$(DELIVERY_DIR)/$(1)'; mv -fv '$(OUTPUT_DIR)/$(1)' $(DELIVERY_DIR)/; fi;
endef

delivery:
	@mkdir -p $(DELIVERY_DIR)/
	@$(foreach file,$(OUTPUT_FILES),$(call DELIVER_FILE_template,$(file)))
.PHONY:  delivery

###
### Libretto rules
###
livret:
	cd livret && make all

lyrics:
	cd livret && make lyrics
.PHONY: livret lyrics

###
###
###
all: check parts conducteur delivery

check:
	@if [ ! -d $(NENUVAR_LIB_PATH) ]; then \
	  echo "Please install nenuvar-lib in grand-grand-parent directory:"; \
	  echo " cd ../../.. && git clone https://github.com/nsceaux/nenuvar-lib.git"; \
	  false; \
	fi
clean:
	@rm -f $(OUTPUT_DIR)/$(PROJECT)_* $(OUTPUT_DIR)/$(PROJECT).*

.PHONY: clean all check

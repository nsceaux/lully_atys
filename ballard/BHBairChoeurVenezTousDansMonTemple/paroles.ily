%% ACTE 1 SCENE 8
\tag #'(cybele basse) {
  % Cybele sur son char.
  Ve -- nez tous dans mon temple, et que cha -- cun ré -- vè -- re
  le sa -- cri -- fi -- ca -- teur dont je vais fai -- re choix :
  je m’ex -- pli -- que -- rai par sa voix,
  les vœux qu’il m’of -- fri -- ra se -- ront sûrs de me plai -- re.
  Je re -- çois vos res -- pects ; j’aime à voir les hon -- neurs
  dont vous me pre -- sen -- tez un é -- cla -- tant hom -- ma -- ge,
  mais l’hom -- ma -- ge des cœurs
  est ce que j’ai -- me da -- van -- ta -- ge.

  Vous de -- vez vous a -- ni -- mer
  d’une ar -- deur nou -- vel -- le.
  Vous de -- vez vous a -- ni -- mer
  d’une ar -- deur nou -- vel -- le.
  S’il faut ho -- no -- rer Cy -- bè -- le,
  il faut en -- cor plus l’ai -- mer.
  S’il faut ho -- no -- rer Cy -- bè -- le,
  il faut en -- cor plus l’ai -- mer.
  Il faut en -- cor plus l’ai -- mer.
  Il faut en -- cor plus l’ai -- mer.
}
\tag #'vdessus {
  % Les Choeurs
  Nous de -- vons nous a -- ni -- mer
  d’une ar -- deur nou -- vel -- le,
  nous de -- vons nous a -- ni -- mer
  d’une ar -- deur nou -- vel -- le.
  S’il faut ho -- no -- rer Cy -- bè -- le,
  il faut en -- cor plus l'ai -- mer.
  S’il faut ho -- no -- rer Cy -- bè -- le,
  il faut en -- cor plus l'ai -- mer,
  il faut en -- cor plus l'ai -- mer.
  Il faut en -- cor plus l'ai -- mer.
  Il faut en -- cor plus l'ai -- mer.
}
\tag #'vhaute-contre {
  % Les Choeurs
  Nous de -- vons nous a -- ni -- mer
  d’une ar -- deur nou -- vel -- le,
  nous de -- vons nous a -- ni -- mer
  d’une ar -- deur nou -- vel -- le.
  S’il faut ho -- no -- rer Cy -- bè -- le,
  il faut en -- cor plus l'ai -- mer.
  S’il faut ho -- no -- rer Cy -- bè -- le,
  il faut en -- cor plus l'ai -- mer,
  il faut en -- cor plus l'ai -- mer.
  Il faut en -- cor plus l'ai -- mer.
  Il faut en -- cor plus l'ai -- mer.
}
\tag #'vtaille {
  % Les Choeurs
  Nous de -- vons nous a -- ni -- mer
  d’une ar -- deur nou -- vel -- le,
  nous de -- vons nous a -- ni -- mer
  d’une ar -- deur nou -- vel -- le.
  S’il faut ho -- no -- rer Cy -- bè -- le,
  il faut en -- cor plus l'ai -- mer.
  S’il faut ho -- no -- rer Cy -- bè -- le,
  il faut en -- cor plus l'ai -- mer,
  il faut en -- cor plus l'ai -- mer.
  Il faut en -- cor plus l'ai -- mer.
  Il faut en -- cor plus l'ai -- mer.
}
\tag #'vbasse {
  % Les Choeurs
  Nous de -- vons nous a -- ni -- mer
  d’une ar -- deur nou -- vel -- le,
  nous de -- vons nous a -- ni -- mer
  d’une ar -- deur nou -- vel -- le.
  S’il faut ho -- no -- rer Cy -- bè -- le,
  il faut en -- cor plus l'ai -- mer.
  S’il faut ho -- no -- rer Cy -- bè -- le,
  il faut en -- cor plus l'ai -- mer,
  il faut en -- cor plus l'ai -- mer.
  Il faut en -- cor plus l'ai -- mer.
  Il faut en -- cor plus l'ai -- mer.
}

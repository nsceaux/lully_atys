\cybeleMark % r8 mi''16 mi'' do''8\trill do''16 mi'' la'8 la'16 la' mi'8 fa'16 sol' |
r8 mi''16 mi'' do''8 do''16 mi'' la'16 la' la' mi' fa'8. sol'16 |
mi'8\trill mi' r mi' mi'16 mi' fa' sol' |
la'4 la'8 la' re''4 re''8 re'' |
si'4\trill r mi''16 mi'' mi'' si' do''8 do''16 do'' |
la'4\trill r8 re'' la'8.\trill la'16 si'8. do''16 |
si'8\trill si'16 si' do''8 re''16 mi'' mi''4( re'')\trill |
do'' r8 sol'16 la' fa'4.\trill fa'16 mi' |
mi'4.\trill do''16 do'' sol'4 sol'8 la' |
si'8. sol'16 sol' la' si' dod'' re'' re'' re'' re'' mi''8. mi''16 |
la'4\trill la' re''8. %{ do''16 %} re''16 si'8\trill si'16 si' |
sold'4 do''8 re''16 mi'' si'8.\trill si'16 si'8. do''16 |
la'2\trill la' |
r %{ la'4 si' %} la'4. si'8 |
do''4. re''8 re''4.\trill mi''8 |
mi''2 si'4. si'8 |
do''4. re''8 do''4( si')\trill |
la'2 r |
R1 |
r2 mi''4. si'8 |
do''4. si'8 la'4.\trill sol'8 |
fad'2 si'4. do''8 |
la'4.\trill sol'8 fad'2\trill |
mi' r |
R1 |
r2 r4 si' |
do''2 do''4. si'8 |
la'2.\trill re''4 |
si'2\trill si'4 sol' |
do''2 re''4 mi'' |
mi''2( re''4.)\trill do''8 |
do''2 r |
r r4 do'' |
si'2\trill si'4. do''8 |
la'2. si'4 |
sold'2 sold'4 mi'' |
re''2\trill re''4 do'' |
do''2( si'4.)\trill la'8 |
la'2 r |
R1*2 |
r2 r4 do'' |
si'2\trill si'4 la' |
la'2( sold'4.)\trill la'8 |
la'2 r |
R1*2 |
r2 r4 mi'' |
fa''2 re''4.\trill do''8 |
do''2( si'4.)\trill la'8 |
la'2
<<
  \tag #'voix {
    do''4.^\markup\override #'(line-width . 70) \italic\wordwrap {
      [Cybèle portée par son char volant, se va rendre dans son temple.
      Tous les Phrygiens s’empressent d’y aller, et répètent les quatre
      derniers vers que la déesse a prononcés.]
    } do''8 |
    re''4. re''8 re''4.\trill re''8 |
    mi''2 re''4 si' |
    do''4. re''8 do''4( si')\trill |
    do''2 mi''4. mi''8 |
    re''4.\trill re''8 re''4. mi''8 |
    fa''2 fa''4. sol''8 |
    mi''4 mi'' mi''( re'') |
    mi''2 r |
    R1*2 |
    r2 r4 mi'' |
    mi''2 mi''4. mi''8 |
    fa''2. fa''4 |
    re''2\trill re''4 mi'' |
    re''2\trill re''4 do'' |
    do''2( si'4.)\trill do''8 |
    do''2 r |
    R1 |
    r2 r4 re'' |
    mi''2 mi''4. mi''8 |
    mi''2. fad''4 |
    red''2\trill red''4 si' |
    do''2 la'4.\trill sol'8 |
    fad'2( sold'4.) la'8 |
    sold'2.\trill mi''4 |
    mi''2 re''4.\trill do''8 |
    do''2( si'4.)\trill la'8 |
    la'2 r |
    R1*2 |
    r2 r4 do'' |
    do''2 si'4.\trill la'8 |
    la'2( sold'4.\trill) la'8 |
    la'2 r |
    R1*2 |
    r2 r4 mi'' |
    mi''2 re''4.\trill do''8 |
    do''2( si'4.)\trill la'8 |
    la'1 |
  }
  \tag #'basse { r2 R1*39 }
>>


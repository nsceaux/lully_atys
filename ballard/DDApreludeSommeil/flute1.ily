\clef "dessus" R1*5 |
r2 sib''4( la'') |
sol''( la'') sol''( fa'') |
mi''( fa'') sol''( la''8 sol'') |
fa''2. sol''8 la'' |
mi''4. mi''8 mi''4.\trill re''8 |
re''1 |
R1*3 |
r4 sol'' fa''( mib'') |
re''( sol'') fa''( mib'') |
re''1 |
\afterGrace mib''( fa''16) |
\afterGrace mib''1( fa''16) |
mib''4. re''8 re''4.\trill do''8 |
do''1 |
R1*3 |
re''4( mib'') re''( do'') |
sib'( la') sib'( do'') |
re''( do'') re''( mi'') |
fad''( sol'') la''2~ |
la''4 sib''8 la'' sol''2~ |
sol''4. la''8 fad''4.(\trill mi''16 fad'') |
sol''1 |
R1*3 |
r4 fa'' mib''( re'') |
do''( fa'') mib''( re'') |
do''( re'') mib''( fa''8 mib'') |
re''2. mib''8 re'' |
do''4. do''8 do''4.\trill sib'8 |
sib'2 r |
R1*4 |
r2 sib' |
\afterGrace sib'1( do''16) |
\afterGrace sib'1( do''16) |
la'4. la'8 la'4.\trill sol'8 |
sol'1 |
R1 |
r4 sib'' la''( sol'') |
fad''(\trill sib'') la''( sol'') |
fad''2.\trill re''4 |
\afterGrace sol''1( la''16) |
\afterGrace sol''1( la''16) |
sol''2 fad''4.( mi''16 fad'') |
sol''2 r |

\clef "taille" sib4( la) sib( do') |
\afterGrace re'1( mib'16) |
\afterGrace re'1( mib'16) |
re'4( do') sib2 |
la1 |
sol2 r |
R1*4 |
\afterGrace re'1( mib'16) |
re'1 |
mib'4( fa') mib'( re') |
do'2 fa'4 fa'4 |
re' r r2 |
R1*5 |
\new CueVoice { \afterGrace mib'1( fa'16) }
\afterGrace mib'1( fa'16) |
re'2. re'4 |
\afterGrace re'1( mib'16) |
re'2. re'4 |
re'2 r |
R1*4 |
re'2~ re'4. re'8 |
do'1 |
do' |
re'2 re' |
do'4 r r2 |
R1*4 |
fa'2 re' |
mi' mi' |
re'2. re'4 |
re'2 do'4 mib' |
re'2. re'4 |
re'2 r |
R1*3 |
\afterGrace re'1( mib'16) |
re'2 do'4 mib' |
re' r r2 |
R1 |
r2 r4 fad' |
sol'2 re' |
mib'2. mib'4 |
re' mib' re' do' |
sib2 r |

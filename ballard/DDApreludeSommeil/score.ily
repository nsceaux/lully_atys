\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new GrandStaff \with { instrumentName = "Flûtes" } <<
      \new Staff << \global \includeNotes "flute1" >>
      \new Staff << \global \includeNotes "flute2" >>
    >>
    \new StaffGroupNoBracket <<
      \new Staff \with { instrumentName = "Violons" } <<
        \global \includeNotes "dessus"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { [Hautes- contre] }
      } << \global \includeNotes "haute-contre" >>
      \new Staff \with { instrumentName = "[Tailles]" } <<
        \global \includeNotes "taille"
      >>
      \new Staff \with { instrumentName = "[Quintes]" } <<
        \global \includeNotes "quinte"
      >>
      \new Staff \with { instrumentName = "[Basses]" } <<
        \global \keepWithTag #'basse \includeNotes "basse"
      >>
    >>
    \new Staff \with { instrumentName = "B.C." } <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}

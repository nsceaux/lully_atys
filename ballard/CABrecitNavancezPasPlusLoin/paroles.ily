%% ACTE 2 SCENE 1
\tag #'(voix1 basse) {
  N’a -- van -- cez pas plus loin, ne sui -- vez point mes pas ;
  sor -- tez. Toi ne me quit -- te pas.
  A -- tys, il faut at -- tendre i -- ci que la dé -- es -- se
  nomme un grand sa -- cri -- fi -- ca -- teur.

  Son choix se -- ra pour vous, sei -- gneur ; quel -- le tris -- tes -- se
  semble a -- voir sur -- pris vo -- tre cœur ?

  Les rois les plus puis -- sants con -- nais -- sent l’im -- por -- tan -- ce
  d’un si glo -- ri -- eux choix :
  qui pour -- ra l’ob -- te -- nir é -- ten -- dra sa puis -- san -- ce
  par -- tout où de Cy -- bèle on ré -- vè -- re les lois.

  Elle ho -- nore au -- jour -- d’hui ces lieux de sa pré -- sen -- ce,
  c’est pour vous pré -- fé -- rer aux plus puis -- sants des rois.

  Mais quand j’ai vu tan -- tôt la beau -- té qui m’en -- chan -- te,
  n’as- tu point re -- mar -- qué comme elle é -- tait trem -- blan -- te ?

  À nos jeux, à nos chants, j’é -- tais trop ap -- pli -- qué,
  hors la fê -- te, sei -- gneur, je n’ai rien re -- mar -- qué.

  Son trou -- ble m’a sur -- pris. El -- le t’ou -- vre son â -- me ;
  n’y dé -- cou -- vres- tu point quel -- que se -- crè -- te flam -- me ?
  quel -- que ri -- val ca -- ché ?

  Sei -- gneur, que di -- tes- vous ?

  Le seul nom de ri -- val al -- lu -- me mon cou -- roux.
  J’ai bien peur que le ciel n’ait pu voir sans en -- vi -- e
  le bon -- heur de ma vi -- e,
  et si j’é -- tais ai -- mé mon sort se -- rait trop doux.
  Ne t’é -- ton -- ne point tant de voir la ja -- lou -- si -- e
  dont mon âme est sai -- si -- e
  on ne peut bien ai -- mer sans être un peu ja -- loux.

  Sei -- gneur, soy -- ez con -- tent, que rien ne vous a -- lar -- me ;
  l’hy -- men va vous don -- ner la beau -- té qui vous char -- me,
  vous se -- rez son heu -- reux é -- poux.

  Tu peux me ras -- su -- rer, A -- tys, je te veux croi -- re,
  c’est son cœur que je veux a -- voir,
  dis- moi s’il est en mon pou -- voir ?

  Son cœur suit a -- vec soin le de -- voir et la gloi -- re,
  et vous a -- vez pour vous la gloire et le de -- voir.

  Ne me dé -- gui -- se point ce que tu peux con -- naî -- tre.
  Si j’ai ce que j’aime en ce jour
  l’hy -- men seul m’en rend- il le maî -- tre ?
  La gloire et le de -- voir au -- ront tout fait, peut- ê -- tre,
  et ne lais -- sent pour moi rien à faire à l’a -- mour.

  Vous ai -- mez d’un a -- mour trop dé -- li -- cat, trop ten -- dre.
}
\tag #'(voix2 basse) {
  L’in -- dif -- fé -- rent A -- tys ne le sau -- rait com -- pren -- dre.
}
\tag #'(voix1 basse) {
  Qu’un in -- dif -- fé -- rent est heu -- reux !
  il jou -- it d’un des -- tin pai -- si -- ble.
  Le ciel fait un pré -- sent bien cher, bien dan -- geu -- reux,
  lors -- qu’il donne un cœur trop sen -- si -- ble.
  Le ciel fait un pré -- sent bien cher, bien dan -- geu -- reux,
  lors -- qu’il donne un cœur trop sen -- si -- ble.
  
  Quand on ai -- me bien ten -- dre -- ment
  on ne ces -- se ja -- mais de souf -- frir, et de crain -- dre ;
  dans le bon -- heur le plus char -- mant,
  on est in -- gé -- ni -- eux à se faire un tour -- ment,
  et l’on prend plai -- sir à se plain -- dre.
  On est in -- gé -- ni -- eux à se faire un tour -- ment,
  et l’on prend plai -- sir à se plain -- dre.
  Va songe à mon hy -- men, et vois si tout est prêt,
  lais -- se- moi seul i -- ci, la dé -- es -- se pa -- raît.
}

\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Célénus
    \livretVerse#12 { N’avancez pas plus loin, ne suivez point mes pas ; }
    \livretVerse#8 { Sortez. Toi ne me quitte pas. }
    \livretVerse#12 { Atys, il faut attendre ici que la Déesse }
    \livretVerse#8 { Nomme un grand sacrificateur. }
    \livretPers Atys
    \livretVerse#12 { Son choix sera pour vous, seigneur ; quelle tristesse }
    \livretVerse#8 { Semble avoir surpris votre cœur ? }
    \livretPers Célénus
    \livretVerse#12 { Les rois les plus puissants connaissent l’importance }
    \livretVerse#6 { D’un si glorieux choix : }
    \livretVerse#12 { Qui pourra l’obtenir étendra sa puissance }
    \livretVerse#12 { Partout où de Cybèle on révère les lois. }
    \livretPers Atys
    \livretVerse#12 { Elle honore aujourd’hui ces lieux de sa présence, }
    \livretVerse#12 { C’est pour vous préférer aux plus puissants des Rois. }
    \livretPers Célénus
    \livretVerse#12 { Mais quand j’ai vu tantôt la beauté qui m’enchante, }
    \livretVerse#12 { N’as-tu point remarqué comme elle était tremblante ? }
    \livretPers Atys
    \livretVerse#12 { À nos jeux, à nos chants, j’étais trop appliqué, }
    \livretVerse#12 { Hors la fête, Seigneur, je n’ai rien remarqué. }
    \livretPers Célénus
    \livretVerse#12 { Son trouble m’a surpris. Elle t’ouvre son âme ; }
    \livretVerse#12 { N’y découvres-tu point quelque secrète flamme ? }
    \livretVerse#12 { Quelque rival caché ? }
    \livretPers Atys
    \livretVerse#12 { \transparent { Quelque rival caché ? } Seigneur, que dites-vous ? }
    \livretPers Célénus
    \livretVerse#12 { Le seul nom de rival allume mon couroux. }
    \livretVerse#12 { J’ai bien peur que le Ciel n’ait pu voir sans envie }
    \livretVerse#6 { Le bonheur de ma vie, }
    \livretVerse#12 { Et si j’étais aimé mon sort serait trop doux. }
    \livretVerse#12 { Ne t’étonne point tant de voir la jalousie }
    \livretVerse#6 { Dont mon âme est saisie }
    \livretVerse#12 { On ne peut bien aimer sans être un peu jaloux. }
  }
  \column {
    \livretPers Atys
    \livretVerse#12 { Seigneur, soyez content, que rien ne vous alarme ; }
    \livretVerse#12 { L’hymen va vous donner la beauté qui vous charme, }
    \livretVerse#8 { Vous serez son heureux époux. }
    \livretPers Célénus
    \livretVerse#12 { Tu peux me rassurer, Atys, je te veux croire, }
    \livretVerse#8 { C’est son cœur que je veux avoir, }
    \livretVerse#8 { Dis-moi s’il est en mon pouvoir ? }
    \livretPers Atys
    \livretVerse#12 { Son cœur suit avec soin le devoir et la gloire, }
    \livretVerse#12 { Et vous avez pour vous la gloire et le devoir. }
    \livretPers Célénus
    \livretVerse#12 { Ne me déguise point ce que tu peux connaître. }
    \livretVerse#8 { Si j’ai ce que j’aime en ce jour }
    \livretVerse#8 { L’hymen seul m’en rend-il le maître ? }
    \livretVerse#12 { La gloire et le devoir auront tout fait, peut-être, }
    \livretVerse#12 { Et ne laissent pour moi rien à faire à l’amour. }
    \livretPers Atys
    \livretVerse#12 { Vous aimez d’un amour trop délicat, trop tendre. }
    \livretPers Célénus
    \livretVerse#12 { L’indifférent Atys ne le saurait comprendre. }
    \livretPers Atys
    \livretVerse#8 { Qu’un indifférent est heureux ! }
    \livretVerse#8 { Il jouit d’un destin paisible. }
    \livretVerse#12 { Le ciel fait un présent bien cher, bien dangeureux, }
    \livretVerse#8 { Lorsqu’il donne un cœur trop sensible. }
    \livretPers Célénus
    \livretVerse#8 { Quand on aime bien tendrement }
    \livretVerse#12 { On ne cesse jamais de souffrir, et de craindre ; }
    \livretVerse#8 { Dans le bonheur le plus charmant, }
    \livretVerse#12 { On est ingénieux à se faire un tourment, }
    \livretVerse#8 { Et l’on prend plaisir à se plaindre. }
    \livretVerse#12 { Va songe à mon hymen, et vois si tout est prêt, }
    \livretVerse#12 { Laisse-moi seul ici, la Déesse paraît. }
  }
}#}))

\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff \withLyrics <<
      { s1*14 s2. s1*16 s2.*3 s1*2 s2. s1*3 s2.*5 s1*17 s2.*2 s1*10
        \footnoteHereFull #'(0 . 0) \markup\column {
          \wordwrap {
            [Manuscrit] Les vers :
            \italic {
              Qu’un indifférent est heureux ! 
              il jouit d’un destin paisible
            }
            sont chantés deux fois.
          }
          \score {
            \new ChoirStaff <<
              \new Staff {
                \tinyQuote \time 2/2 \set autoBeaming = ##f
                \key do \major \clef "vhaute-contre"
                fa'4. fa'8 mi'4. fa'8 |
                re'2 re'4. mi'8 |
                dod'2 re'4. la8 |
                sib2 sib4. do'8 |
                la4. la8 la4( sol) |
                la1 |
                fa'4. fa'8 mi'4. fa'8 |
                re'2 re'4. mi'8 |
                dod'2 re'4. la8 |
                sib2 sib4. do'8 |
                la4. la8 la4( sol) |
                la2. mi'4 |
              } \addlyrics {
                Qu'un in -- dif -- fé -- rent est heu -- "reux !"
                Il jou -- it d'un des -- tin pai -- si -- ble.
                Qu'un in -- dif -- fé -- rent est heu -- "reux !"
                Il jou -- it d'un des -- tin pai -- si -- ble. Le
              }
              \new Staff {
                \clef "vbasse" re4 re' do' la |
                sib2. sol4 |
                la4. sol8 fad2 |
                sol sol, |
                re4 do sib,2 |
                la, la8 sol fa mi |
                re4 re' do' la |
                sib2. sol4 |
                la4. sol8 fad2 |
                sol sol, |
                re4 do sib,2 |
                la,2 la |
              }
            >>
            \layout { \quoteLayout ragged-right = ##f }
          }
        }
        s1*24
        \footnoteHereFull #'(0 . 0) \markup\column {
          \wordwrap {
            [Manuscrit] Les vers
            \italic {
              Quand on aime bien tendrement,
              on ne cesse jamais de souffrir et de craindre
            }
            sont chantés deux fois.
          }
          \score {
            \new ChoirStaff <<
              \new Staff {
                \tinyQuote \time 2/2 \set autoBeaming = ##f
                \key do \major \clef "varbaritone"
                \repeat volta 2 {
                  r2 fa4 re | la2 la4 la |
                  la2( sol4 fa8) sol | la2 dod'4. dod'8 |
                  re'2 re'4 la | sib2 sib4. sib8 |
                  sib2 la4. sib8 | la2( sol) |
                }
                \alternatives { fa2 r } { fa2 la4 la8 si }
              } \addlyrics {
                Quand on ai -- me bien ten -- dre -- ment,
                on ne ces -- se ja -- mais de souf -- frir et de crain -- "dre ;" "- dre ;"
                dans le bon-
              }
              \new Staff {
                \clef "bass" re1 | do |
                sib, | la,2 la4. sol8 |
                fad1 | sol2 sol4. fa8 |
                mi2 fa4 sib, | do2 do, |
                fa, fa8 sol fa mi | fa2 fa, |
              }
            >>
            \layout { \quoteLayout ragged-right = ##f }
          }
        }
      }
      \global \keepWithTag #'voix1 \includeNotes "voix"
    >> \keepWithTag #'voix1 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix2 \includeNotes "voix"
    >> \keepWithTag #'voix2 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
  \midi { }
}

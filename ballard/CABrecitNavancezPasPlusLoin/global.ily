\key la \minor
\time 4/4 \midiTempo#80 s1*14
\digitTime\time 3/4 s2.
\time 4/4 s1*16
\digitTime\time 3/4 s2.*3
\time 4/4 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1*3
\digitTime\time 3/4 s2.*5
\time 4/4 s1*17
\digitTime\time 3/4 s2.*2
\time 4/4 s1*4
\time 2/2 \midiTempo#160 s1*48
\time 4/4 \midiTempo#80 s1*5 \bar "|."

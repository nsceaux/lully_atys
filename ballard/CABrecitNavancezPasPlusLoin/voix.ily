<<
  \tag #'(voix1 basse) {
    \celaenusMark r2 r16 fa fa fa sol8. la16 |
    sol2\trill mi8 mi16 mi la8. mi16 |
    fa2 r4 r8 la |
    sib4 r mi8 mi16 mi mi8. fa16 |
    re4 r r r8 re' |
    la4.\trill la8 la8. la16 la8. sib16 |
    do'8 do' sib8. la16 sol8\trill sol r sol16 sol |
    do'8 do'16 sib sib8. la16 la4\trill
    \atysMark r8 do' |
    dod'8. dod'16 dod'8. re'16 re'4 r8 re' |
    sib4 sib8 sib16 la la4\trill la |
    la8. la16 si8 do' re'4 re'8. mi'16 |
    dod'4
    \celaenusMark r8 mi la8. la16 la8. la16 |
    re'4 r8 re' la8. la16 %{ la8 la %} la8. la16 |
    re4 re la8. la16 la8 si16 do' |
    si4\trill r8 sol16 sol sol8 la16 si |
    do'4 do'8 do' re'4 re'8[ do'16] re' |
    mi'4 mi'8 mi fad8. fad16 fad8. fad16 |
    sold4\trill sold8. sold16
    %{ la4 la8. sold16 %} la4. la16 sold |
    la4
    \atysMark dod'8 dod' re'4 re'8 re' |
    si4\trill r8 sol do'4 sib8 sib16 la |
    la8\trill la r la %{ la la %} la8. la16 sib8. do'16 |
    re'8. re'16 mi'8 fa' fa'4( mi'8.)\trill fa'16 |
    fa'4
    \celaenusMark la r8 la16 la la8. sib16 |
    sol8\trill sol16 sol sol8 fa16 mi fa4 fa |
    r8 la16 la re'8 re'16 re' si\trill si si si do'8. do'16 |
    sold8\trill sold
    \atysMark r8 mi'16 si %{ do'4. %} do'4 r8 do'16 do' |
    la4.\trill la16 si do'4. do'16 re' |
    si4.\trill %{ la16 %} sol16 sol do'8 do' r do' |
    la4\trill r8 la16 si do'4 do'8 si |
    do'4
    \celaenusMark r8 sol mi\trill mi mi sol |
    do4 r8 do'16 do' fa4 fa8. fa16 |
    re8\trill re r re'16 re' sib8 sib16 sib |
    % sol8\trill sol sol8. fa16 fa8. mi16 |
    sol4\trill sol8 sol16 fa fa8. mi16 |
    mi8\trill mi la la16 la %{ la8 mi %} la8. mi16 |
    fa8
    \atysMark re'8 la4 r8 la %{ re' la %} re'8. la16 |
    sib8
    \celaenusMark re16 mi fa8 fa16 sol la8. la16 re' re' re' mi' |
    dod'4\trill r8 la16 la la8 si16 dod' |
    re'8 re'16 re' sib8\trill sib16 sib sol\trill sol do' do' mi8 mi16 fa |
    fa4 fa do'8 do'16 do' la8.\trill la16 |
    fad4.\trill sib8 sol8.\trill sol16 sol8 fad |
    sol4 r8 sib16 re' sib8\trill sib16 sib |
    sol8.\trill sol16 re8. re16 %{ re8. mi16 %} re8 mi |
    fa8 fa fa8. la16 fa8\trill fa16 mi |
    mi8\trill mi r16 la la la re'8. re'16 |
    si8\trill mi' dod' re' %{ re'8. dod'16 %} re'8 dod' |
    re'4
    \atysMark r8 fa' la8. la16 la8 si |
    do'4 r8 %{ re' %} do'8 sol8.\trill sol16 sol8. do'16 |
    la8\trill la16 fa fa sol la si do'8 do'16 do' re'8 mi'16 fa' |
    mi'4\trill mi' r do'8. do'16 |
    do'4 sib8[ la16] sib sib4( la8.)\trill sol16 |
    sol4
    \celaenusMark r8 re' sib8. sib16 sib8. sib16 |
    % sol4.\trill do'8 do'4 do'8 sib16 la |
    sol4.\trill do'8 do'8. do'16 sib8. la16 |
    sib8 sib r sib16 la sol8\trill sol16 fa mi8. re16 |
    %{ dod8 r16 %} dod8.\trill la16 mi8.\trill mi16 fa8. fa16 fa8 sol |
    la4
    \atysMark r8 dod' dod'8. dod'16 dod'8. dod'16 |
    re'4 re'8. re'16 mi'4 mi'8. mi'16 |
    fa'4 fa' re'8 re'16 do' si8. la16 |
    %{ sold4. %} sold4 r8 si8 %{ mi' re' %} mi'8. re'16 re'8[ \ficta dod'?16] re' |
    dod'8
    \celaenusMark r16 la la la la la mi8.\trill mi16 mi fad sol la |
    fad8\trill fad r la sib sib16 la sol8\trill sol16 \ficta fa? |
    mi4\trill r8 do'16 do' sol8\trill sol16 sol la8. sib16 |
    la8\trill la r la re'8. re'16 re'8. re'16 |
    si8.\trill si16 si8. do'16 re'8. mi'16 |
    dod'8\trill dod' r la16 la re'8 re'16 la |
    sol4\trill sol8 fa mi4\trill mi8. fa16 |
    %{ re2 %} re4 r4
    \atysMark r8 fa'16 fa' re'8\trill re'16 re' |
    la4 la8 la16 la %{ sib4 r8 %} sib4. re'8 |
    <<
      \tag #'voix1 { la8\trill la r4 r2 | R1 | <>^\markup\character Atys }
      \tag #'basse { la16\trill la s8 s2. | s1 \atysMark }
    >>
    fa'4. fa'8 mi'4. fa'8 |
    re'2 re'4. mi'8 |
    dod'2 re'4. la8 |
    sib2 sib4. do'8 |
    la4. la8 la4( sol) |
    % la1 |
    la2. mi'4 |
    fa'2 re'4\trill re'8 do' |
    si2.\trill sol'4 |
    do'4. re'8 re'4.\trill do'8 |
    do'2 mi'4. mi'8 |
    mi'4 fa' re'2~ |
    re' do'4( si8) do' |
    do'2( si)\trill |
    la2. la4 |
    re'2 re'4 re'8 la |
    sib2. sib4 |
    sol4.\trill sol8 do'4. sib8 |
    la1\trill |
    fa'4. mi'8 re'4. do'8 |
    si2 dod'4 re' |
    re'2( dod') |
    re'2
    \celaenusMark fa4 re |
    la2 la4 la |
    la2( sol4\tr fa8) sol |
    la2 dod'4. dod'8 |
    re'2 re'4 la |
    sib2 sib4. sib8 |
    sib2 la4. sib8 |
    la2( sol)\trill |
    %fa2 r | r fa4 re |
    fa2 la4 la8 si |
    do'4. do'8 re'4( do'8) re' |
    mi'2. mi'4 |
    re'4.\trill re'8 re'4. do'8 |
    si2\trill %{ si4. do'8 %} si4 do' |
    % la2 la4( sold8) la |
    la2 la4 si4 |
    sold1\trill |
    do'4 do' re' mi' |
    sold2 la4( sold8\trill) la |
    la2( sold\trill) |
    la2. la4 |
    mi4.\trill mi8 mi4( re8) mi |
    fa2 la4. la8 |
    re'2 re'4. mi'8 |
    dod'1\trill |
    re'4. do'8 sib4.\trill la8 |
    sib2 sol4.\trill fa8 |
    fa2( mi)\trill |
    re2 la4 fa16 fa fa sol |
    mi4.\trill mi8 fa8. fa16 fa8. sol16 |
    % la4 la8. la16 la4 si8. do'16 |
    la4 r4 la8 la16 la si8. do'16 |
    si4\trill r8 sol16 fa mi4\trill %{ mi8 fa %} mi8. fa16 |
    re1 |
  }
  \tag #'voix2 {
    \clef "vbasse-taille" R1*14 R2. R1*16 R2.*3 R1*2 R2. R1*3 R2.*5
    R1*17 R2.*2 R1*3 s1*2 R1*52
  }
  \tag #'(voix2 basse) {
    s1*14 s2. s1*16 s2.*3 s1*2 s2. s1*3 s2.*5 s1*17 s2.*2 s1*3
    \tag #'basse { s8 \celaenusMark }
    \tag #'voix2 { <>^\markup\character Célénus r8 }
    %{ r16 fa %} fa8 fa16 sol la si do'8 la16 la re'8 re'16 sol |
    la2 la |
  }
>>

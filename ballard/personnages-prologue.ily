\notesSection "Acteurs du prologue"
\markuplist\abs-fontsize-lines #8 \with-line-width-ratio #0.5 {
  \act ACTEURS DU PROLOGUE
  \vspace#5
  \fill-line {
    \smallCaps Le Temps \ambitus "vbasse-taille" { sol, mib' }
  }
  \vspace #1.1
  \fill-line {
    \column {
      \line { Les douze heures du jour. }
      \line { Les douze heures de la nuit. }
      \wordwrap { Troupe de nymphes chantantes de la suite de Flore. }
      \wordwrap { Héros chantants de la suite de Melpomène. }
    }
    \ambiti #'("vdessus" "vhaute-contre" "vtaille" "vbasse") {
      { sol' sol'' }
      { sib sib' }
      { mi fa' }
      { sol, mib' }
    }
  }
  \vspace#1.1
  \fill-line {
    \smallCaps La Déesse Flore \ambitus "vbas-dessus" { mi' fa'' }
  }
  \vspace #1.1
  \fill-line {
    Un Zéphir \ambitus "vhaute-contre" { sol sol' }
  }
  \vspace #1.1
  \column {
    Suivants de Flore dansants Nymphes dansantes
  }
  \vspace #1.1
  \fill-line {
    \line { \smallCaps Melpomène, \smaller\italic { muse tragique } }
    \ambitus "vbas-dessus" { mi' fa'' }
  }
  \vspace #1.1
  \column {
    \wordwrap { Héros combattants et dansants de la suite de Melpomène }
    \wordwrap { Hercule Antæe Castor Pollux Lyncée Idas Étéocle Polinice }
  }
  \vspace #1.1
  \fill-line {
    \smallCaps La Déesse Iris \ambitus "vbas-dessus" { fa' mi'' }
  }
}

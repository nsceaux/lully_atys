\cybeleMark r2 r8 sol'16 la' si'8\trill si'16 do'' |
re''4. re''8 mi''8. mi''16 mi''8. mi''16 |
la'4\trill la' fad'8 fad'16 fad' fad'8. fad'16 |
sol'8 sol'16 si' sol'8\trill sol'16 sol' mi'4\trill r8 do''16 do'' |
% mi'8\trill mi'16 fad' sold'8. sold'16 la'8. si'16 do''8. re''16 |
mi'8\trill mi'16 fad' sol'8. sol'16 la'8. la'16 si'8. do''16 |
si'8\trill si' si' si'16 si' dod''8 re'' |
dod''4 re''8 re''16 re'' re''8 dod'' |
re''4 r la'8 la'16 la' la'8 si' |
do''8 la'16 la' mi'8\trill mi'16 fad' sol'8 sol' r si' |
si'16 si' si' do'' re''8. re''16 do''8. si'16 la'8. sol'16 |
fad'4\trill r8 re''16 re'' si'8\trill si'16 si' |
do''8. do''16 re''4 re''8 mi''16 fa'' |
mi''8\trill mi'' r do''16 do'' do''8 re''16 mi'' |
la'8.\trill la'16 la'8 si' la'8.\trill sol'16 |
sol'2 r |
r8 si' si'16 si' si' si' fad'8.\trill fad'16 la' la' sol' fad' |
sol'4 sol' r8 si' si'16 si' si' do'' |
re''8 re''16 re'' sol'8 sol'16 sol' mi'8\trill mi' r do''16 do'' |
la'4\trill r8 la'16 la' la'8. la'16 |
fad'4\trill
\celaenusMark re'8. re'16 sol2 |
do'8. do'16 do'8. mi'16 la8.\trill la16 si8. do'16 |
si4\trill r8 %{ do'16 do' %} re'16 re' sol8. sol16 re8 mi16 fa |
mi8\trill mi r mi' si8. si16 si8. si16 |
do'8. do'16 re'8. mi'16 sold8.\trill la16 |
la4 r8 do'16 do' do'8 do'16 do' do'8. si16 |
si8\trill si r si16 la sol8 sol16 fad |
mi8.\trill mi16 mi8. fad16 fad4( mi8.)\trill re16 |
re4
\cybeleMark r8 fad'16 fad' fad'8 fad'16 fad' |
sol'4 sol'8 la'16 si' do''8. re''16 |
si'4\trill si' r sol'8 sol' |
do''4 do''8 do'' %{ re''4( do''8) %} re''4. re''8 |
mi''4. do''8 la'4.\trill la'8 |
la'4. si'8 %{ do''4( si'8) %} do''4. do''8 |
si'4.\trill si'8 do''4. do''8 |
re''4. mi''8 mi''4( re'')\trill |
do''2 mi'4. fad'8 |
sol'4 sol'8 sol' la'4. la'8 |
si'4 si' la'\trill sol' |
re''1 |
r4 la' la' si' |
sol'4. sol'8 sol'4 fad' |
sol'2 mi'4. fad'8 |
sol'4 sol'8 sol' la'4. la'8 |
si'4 si' la'\trill sol' |
re''1 |
r4 la' la' si' |
sol'4. sol'8 sol'4 fad' |
sol'4.
\celaenusMark re'8 si8.\trill si16 si8. do'16 |
la4\trill la8. si16 sol4 fad8 sol |
fad\trill fad r la16 si do'8 do'16 do' do'8. si16 |
si4\trill r8 mi16 fad sol8. la16 si8\trill si16 do' |
re'4 re'8 si si8. si16 do'8. re'16 |
mi'4 mi'8. si16 si8. do'16 si8.\trill la16 |
la2 do'8 do'16 si la sol fad mi |
red8\trill red r si sold8. sold16 la8. si16 |
do'4 do'8 do'16 do' do'8. si16 |
si4\trill r8 re' sol8 sol16 sol re8 mi16 fa |
mi8\trill mi r mi' do'8.\trill si16 la8. sol16 |
fad4\trill la8. la16 si4 la8.\trill sol16 |
sol4
\cybeleMark r8 si' si'8. si'16 si'8. si'16 |
do''4 r8 do''16 mi'' do''8\trill do''16 do'' |
%{ la'8\trill la' %} la'8.\trill la'16 r8 re''16 do'' si'8\trill do''16 re'' |
mi''4 la'8 la'16 la' re''8. re''16 |
si'2\trill si' |
R1*5 |

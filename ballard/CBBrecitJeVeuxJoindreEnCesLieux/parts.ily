\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Cybèle
    \livretVerse#12 { Je veux joindre en ces lieux la gloire et l’abondance, }
    \livretVerse#12 { D’un sacrificateur je veux faire le choix, }
    \livretVerse#12 { Et le Roi de Phrygie aurait la préférence }
    \livretVerse#12 { Si je voulais choisir entre les plus grands rois. }
    \livretVerse#12 { Le puissant dieu des flots vous donna la naissance, }
    \livretVerse#12 { Un peuple renommé s’est mis sous votre loi ; }
    \livretVerse#12 { Vous avez sans mon choix, d’ailleurs, trop de puissance, }
    \livretVerse#12 { Je veux faire un bonheur qui ne soit dû qu’à moi. }
    \livretVerse#12 { Vous estimez Atys, et c’est avec justice, }
    \livretVerse#12 { Je prétends que mon choix à vos vœux soit propice, }
    \livretVerse#8 { C’est Atys que je veux choisir. }
    \livretPers Célénus
    \livretVerse#12 { J’aime Atys, et je vois sa gloire avec plaisir. }
    \livretVerse#8 { Je suis Roi, Neptune est mon père, }
    \livretVerse#12 { J’épouse une beauté qui va combler mes vœux : }
    \livretVerse#8 { Le souhait qui me reste à faire, }
    \livretVerse#12 { C’est de voir mon ami parfaitement heureux. }
    \livretPers Cybèle
    \livretVerse#12 { Il m’est doux que mon choix à vos désirs réponde ; }
  }
  \column {
    \livretVerse#8 { Une grande divinité }
    \livretVerse#8 { Doit faire sa félicité }
    \livretVerse#6 { Du bien de tout le monde. }
    \livretVerse#12 { Mais surtout le bonheur d’un roi chéri des cieux }
    \livretVerse#8 { Fait le plus doux plaisir des Dieux. }
    \livretPers Célénus
    \livretVerse#12 { Le sang approche Atys de la Nymphe que j’aime, }
    \livretVerse#8 { Son mérite l’égale aux rois : }
    \livretVerse#8 { Il soutiendra mieux que moi-même }
    \livretVerse#6 { La majesté suprême }
    \livretVerse#6 { De vos divines lois. }
    \livretVerse#8 { Rien ne pourra troubler son zèle, }
    \livretVerse#12 { Son cœur s’est conservé libre jusqu’à ce jour ; }
    \livretVerse#8 { Il faut tout un cœur pour Cybèle, }
    \livretVerse#12 { À peine tout le mien peut suffire à l’amour. }
    \livretPers Cybèle
    \livretVerse#12 { Portez à votre ami la première nouvelle }
    \livretVerse#12 { De l’honneur éclatant où ma faveur l’appelle. }
  }
}#}))

%% ACTE 2 SCENE 2
% Cybele
Je veux joindre en ces lieux la gloire et l’a -- bon -- dan -- ce,
d’un sa -- cri -- fi -- ca -- teur je veux fai -- re le choix,
et le roi de Phry -- gie au -- rait la pré -- fé -- ren -- ce
si je vou -- lais choi -- sir en -- tre les plus grands rois.
Le puis -- sant dieu des flots vous don -- na la nais -- san -- ce,
un peu -- ple re -- nom -- mé s’est mis sous vo -- tre loi ;
vous a -- vez sans mon choix, d’ail -- leurs, trop de puis -- san -- ce,
je veux faire un bon -- heur qui ne soit dû qu’à moi.
Vous es -- ti -- mez A -- tys, et c’est a -- vec jus -- ti -- ce,
je pré -- tends que mon choix à vos vœux soit pro -- pi -- ce,
c’est A -- tys que je veux choi -- sir.

J’aime A -- tys, et je vois sa gloire a -- vec plai -- sir.
Je suis roi, Nep -- tune est mon pè -- re,
j’é -- pouse u -- ne beau -- té qui va com -- bler mes vœux :
le sou -- hait qui me reste à fai -- re,
c’est de voir mon a -- mi par -- fai -- te -- ment heu -- reux.

Il m’est doux que mon choix à vos dé -- sirs ré -- pon -- de ;
u -- ne gran -- de di -- vi -- ni -- té
doit fai -- re sa fé -- li -- ci -- té
du bien de tout le mon -- de.
Mais sur -- tout le bon -- heur d’un roi ché -- ri des cieux
fait le plus doux plai -- sir des dieux.
Mais sur -- tout le bon -- heur d’un roi ché -- ri des cieux
fait le plus doux plai -- sir des dieux.

Le sang ap -- proche A -- tys de la nym -- phe que j’ai -- me,
son mé -- ri -- te l’é -- gale aux rois :
il sou -- tien -- dra mieux que moi- mê -- me
la ma -- jes -- té su -- prê -- me
de vos di -- vi -- nes lois.
Rien ne pour -- ra trou -- bler son zè -- le,
son cœur s’est con -- ser -- vé li -- bre jus -- qu’à ce jour ;
il faut tout un cœur pour Cy -- bè -- le,
à pei -- ne tout le mien peut suf -- fire à l’a -- mour.

Por -- tez à votre a -- mi la pre -- miè -- re nou -- vel -- le
de l’hon -- neur é -- cla -- tant où ma fa -- veur l’ap -- pel -- le.

\actn "Prologue"
%% 0-1
\pieceToc "Ouverture"
\includeScore "AAAouverture"
%% 0-2
\pieceToc\markup\wordwrap {
  Récit : \italic { En vain j’ai respecté la célèbre mémoire }
}
\includeScore "AABrecitEnVainJaiRespecte"
\newBookPart#'(full-rehearsal basse)
%% 0-3
\pieceToc\markup\wordwrap { Chœur : \italic { Ses justes lois } }
\includeScore "AACchoeurSesJustesLois"
\newBookPart#'(full-rehearsal)
%% 0-4
\sceneDescription\markup\justify {
  [La Déesse Flore conduite par un des Zéphirs s’avance avec une
  troupe de nymphes qui portent divers ornements de fleurs.]
}
\pieceToc "Air pour les nymphes de Flore"
\includeScore "AADairFlore"
\newBookPart#'(full-rehearsal)
%% 0-5
\pieceToc\markup\wordwrap {
  Récit : \italic { La saison des frimas peut-elle nous offrir }
}
\includeScore "AAErecitLaSaisonDesFrimas"
%% 0-6
\pieceToc\markup\wordwrap {
  Air, chœur : \italic { Les plaisirs à ses yeux ont beau se presenter }
}
\includeScore "AAFairChoeurLesPlaisirASesYeux"
\newBookPart#'(full-rehearsal)

\sceneDescription\markup\justify {
  [La suite de Flore commence des jeux mêlés de danses et de chants.]
}
%% 0-7
\pieceToc "Air pour la suite de Flore"
\includeScore "AAGgavotte"
%% 0-8
\pieceToc\markup\wordwrap {
  Air : \italic { Le printemps quelque fois est moins doux qu’il ne semble }
}
\includeScore "AAHairLePrintempsQuelqueFois"
%% 0-9
\sceneDescription\markup\justify {
  On reprend la gavotte précédente.
}
\pieceToc "Air pour la suite de Flore"
\reIncludeScore "AAGgavotte" "AAHgavotte"
\newBookPart#'(full-rehearsal)

\sceneDescription\markup\justify {
  \smallCaps [Melpomène, qui est la muse qui préside à la tragédie,
  vient accompagnée d’une troupe de héros, elle est suivie d’Hercule,
  d’Antæe, de Castor, de Pollux, de Lincée, d’Idas, d’Étéocle, et de Polinice.]
}
%% 0-10
\pieceToc "Prélude pour Melpomène"
\includeScore "AAIpreludeMelpomene"
%% 0-11
\pieceToc\markup {
  Récit : \italic { Retirez-vous, cessez de prévenir le Temps }
}
\includeScore "AAJrecitRetirezVous"

\sceneDescription\markup\column {
  \justify { [La suite de Melpomène prend la place de la suite de Flore. }
  \justify { Les héros recommencent leurs anciennes querelles. }
  \justify {
    Hercule combat et lutte contre Antæe, Castor et Pollux combattent
    contre Lyncée et Idas, et Étéocle combat contre son frère
    Polynice.]
  }
}
%% 0-12
\pieceToc "Air pour la Suite de Melpomène"
\includeScore "AAKairMelpomene"
\newBookPart#'(full-rehearsal)

\sceneDescription\markup\justify {
  \smallCaps [Iris, par l’ordre de Cybèle, descend assise sur son arc,
  pour accorder Melpomène et Flore.]
}
%% 0-13
\pieceToc\markup\wordwrap {
  Récit, air, chœur : \italic { Cybèle veut que Flore aujourd’hui vous seconde }
}
\includeScore "AALcybeleVeutQueFlore"
\newBookPart#'(full-rehearsal)
%% 0-14
\pieceToc "Menuet"
\includeScore "AAMmenuet"
%% 0-15
\pieceToc "Ouverture"
\reIncludeScore "AAAouverture" "AANouverture"
\actEnd "FIN DU PROLOGUE"

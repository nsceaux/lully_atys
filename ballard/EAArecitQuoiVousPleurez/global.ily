\key sol \major
\time 4/4 \midiTempo #80 s1*12
\digitTime\time 3/4 s2.
\time 4/4 s1*5
\digitTime\time 3/4 s2.*5
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1
\time 2/2 \midiTempo #160 s1
\time 4/4 \midiTempo #80 s1*2
\digitTime\time 3/4 \midiTempo #160 s2.*22
\time 4/4 \midiTempo #80 s1*5
\digitTime\time 3/4 s2.*4
\time 4/4 s1
\digitTime\time 3/4 \midiTempo #160 s2.*21
\time 4/4 \midiTempo #80 s1*3
\time 2/2 \midiTempo #160 s1*7
\digitTime\time 3/4 \midiTempo #80 s2.*5
\time 2/2 \midiTempo #160 s1*8
\time 3/2 s1.*2
\time 2/2 s1*15
\time 4/4 \midiTempo #80 s1*4
\time 2/2 \midiTempo #160 s1*27 \bar "|."

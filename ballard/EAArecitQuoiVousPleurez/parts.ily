\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Doris
    \livretVerse#12 { Quoi, vous pleurez ? }
    \livretPers Idas
    \livretVerse#12 { \transparent { Quoi, vous pleurez ? } D’où vient votre peine nouvelle ? }
    \livretPers Doris
    \livretVerse#12 { N’osez-vous découvrir votre amour à Cybèle ? }
    \livretPers Sangaride
    \livretVerse#12 { Hélas ! }
    \livretPers Doris et Idas
    \livretVerse#12 { \transparent { Hélas ! } Qui peut encor redoubler vos ennuis ? }
    \livretPers Sangaride
    \livretVerse#12 { Hélas ! j’aime… hélas ! j’aime… }
    \livretPers Doris et Idas
    \livretVerse#12 { \transparent { Hélas ! j’aime… hélas ! j’aime… } Achevez. }
    \livretPers Sangaride
    \livretVerse#12 { \transparent { Hélas ! j’aime… hélas ! j’aime… Achevez. } Je ne puis. }
    \livretPers Doris et Idas
    \livretVerse#12 { L’Amour n’est guère heureux lorsqu’il est trop timide. }
    \livretPers Sangaride
    \livretVerse#6 { Hélas ! j’aime un perfide }
    \livretVerse#6 { Qui trahit mon amour ; }
    \livretVerse#12 { La Déesse aime Atys, il change en moins d’un jour, }
    \livretVerse#12 { Atys comblé d’honneurs n’aime plus Sangaride. }
    \livretVerse#6 { Hélas ! j’aime un perfide }
    \livretVerse#6 { Qui trahit mon amour. }
    \livretPers Doris et Idas
    \livretVerse#12 { Il nous montrait tantôt un peu d’incertitude ; }
    \livretVerse#12 { Mais qui l’eut soupçonné de tant d’ingratitude ? }
    \livretPers Sangaride
    \livretVerse#12 { J’embarassais Atys, je l’ai vu se troubler : }
    \livretVerse#8 { Je croyais devoir révéler }
    \livretVerse#6 { Notre amour à Cybèle ; }
    \livretVerse#6 { Mais l’ingrat, l’infidèle, }
    \livretVerse#8 { M’empechait toujours de parler. }
    \livretPers Doris et Idas
    \livretVerse#12 { Peut-on changer si tôt quand l’amour est extrême ? }
  }
  \column {
    \null
    \livretVerse#6 { Gardez-vous, gardez-vous }
    \livretVerse#8 { De trop croire un transport jaloux. }
    \livretPers Sangaride
    \livretVerse#12 { Cybèle hautement déclare qu’elle l’aime, }
    \livretVerse#12 { Et l’ingrat n’a trouvé cet honneur que trop doux ; }
    \livretVerse#12 { Il change en un moment, je veux changer de même, }
    \livretVerse#12 { J’accepterai sans peine un glorieux époux, }
    \livretVerse#12 { Je ne veux plus aimer que la grandeur suprême. }
    \livretPers Doris et Idas
    \livretVerse#12 { Peut-on changer si tôt quand l’amour est extrême ? }
    \livretVerse#6 { Gardez-vous, gardez-vous }
    \livretVerse#8 { De trop croire un transport jaloux. }
    \livretPers Sangaride
    \livretVerse#8 { Trop heureux un cœur qui peut croire }
    \livretVerse#8 { Un dépit qui sert à sa gloire. }
    \livretVerse#12 { Revenez ma raison, revenez pour jamais, }
    \livretVerse#12 { Joignez-vous au dépit pour étouffer ma flamme, }
    \livretVerse#12 { Réparez, s’il se peut, les maux qu’Amour m’a faits, }
    \livretVerse#8 { Venez rétablir dans mon âme }
    \livretVerse#8 { Les douceurs d’une heureuse paix ; }
    \livretVerse#12 { Revenez, ma raison, revenez pour jamais. }
    \livretPers Idas et Doris
    \livretVerse#8 { Une infidelité cruelle }
    \livretVerse#8 { N’efface point tous les appas }
    \livretVerse#4 { D’un infidèle, }
    \livretVerse#8 { Et la raison ne revient pas }
    \livretVerse#6 { Si tôt qu’on l’a rappelle. }
    \livretPers Sangaride
    \livretVerse#7 { Après une trahison }
    \livretVerse#7 { Si la raison ne m’éclaire, }
    \livretVerse#7 { Le dépit et la colère }
    \livretVerse#7 { Me tiendront lieu de raison. }
    \livretPers Sangaride, Doris, Idas
    \livretVerse#8 { Qu’une première amour est belle ? }
    \livretVerse#8 { Qu’on a peine à s’en dégager ! }
    \livretVerse#8 { Que l’on doit plaindre un cœur fidèle }
    \livretVerse#8 { Lorsqu’il est forcé de changer. }
  }
}#}))

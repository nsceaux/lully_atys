%% ACTE 4 SCENE 1
\tag #'(voix1 basse) {
  Quoi, vous pleu -- rez ?

  D’où vient vo -- tre pei -- ne nou -- vel -- le ?
  
  N’o -- sez- vous dé -- cou -- vrir votre a -- mour à Cy -- bè -- le ?
  
  Hé -- las !
}
\tag #'(voix1 voix3 basse) {
  Qui peut en -- cor re -- dou -- bler vos en -- nuis ?
}
\tag #'(voix1 basse) {
  Hé -- las ! j’ai -- me… hé -- las ! j’ai -- me…
}
\tag #'(voix1 voix3 basse) {
  A -- che -- vez.
}
\tag #'(voix1 basse) {
  Je ne puis.
}
\tag #'(voix1 voix3 basse) {
  L’A -- mour n’est guère heu -- reux lors -- qu’il est trop ti -- mi -- de.
}
\tag #'(voix1 basse) {
  Hé -- las ! j’aime un per -- fi -- de
  qui tra -- hit mon a -- mour ;
  la dé -- esse aime A -- tys, il change en moins d’un jour,
  A -- tys com -- blé d’hon -- neurs n’ai -- me plus San -- ga -- ri -- de.
  Hé -- las ! j’aime un per -- fi -- de
  qui tra -- hit mon a -- mour.
}
\tag #'(voix1 voix3 basse) {
  Il nous mon -- trait tan -- tôt un peu d’in -- cer -- ti -- tu -- de ;
  mais qui l’eut soup -- çon -- né de tant d’in -- gra -- ti -- tu -- de ?
}
\tag #'(voix1 basse) {
  J’em -- ba -- ras -- sais A -- tys, je l’ai vu se trou -- bler :
  je croy -- ais de -- voir ré -- vé -- ler
  notre a -- mour à Cy -- bè -- le ;
  mais l’in -- grat, l’in -- fi -- dè -- le,
  m’em -- pe -- chait tou -- jours de par -- ler.
}
\tag #'(voix1 voix3 basse) {
  Peut- on chan -- ger si tôt quand l’a -- mour est ex -- trê -- me ?
  Gar -- dez- vous, gar -- dez- vous
  de trop croire un trans -- port ja -- loux.
  Gar -- dez- vous, gar -- dez- vous
  de trop croire un trans -- port ja -- loux.
  Gar -- dez- vous, gar -- dez- vous
  de trop croire un trans -- port ja -- loux.
}
\tag #'(voix1 basse) {
  Cy -- bè -- le hau -- te -- ment dé -- cla -- re qu’el -- le l’ai -- me,
  et l’in -- grat n’a trou -- vé cet hon -- neur que trop doux ;
  il change en un mo -- ment, je veux chan -- ger de mê -- me,
  j’ac -- cep -- te -- rai sans peine un glo -- ri -- eux é -- poux,
  je ne veux plus ai -- mer que la gran -- deur su -- prê -- me.
}
\tag #'(voix1 voix3 basse) {
  Peut- on chan -- ger si tôt quand l’a -- mour est ex -- trê -- me ?
  Gar -- dez- vous, gar -- dez- vous
  de trop croire un trans -- port ja -- loux.
  Gar -- dez- vous, gar -- dez- vous
  de trop croire un trans -- port ja -- loux.
  Gar -- dez- vous, gar -- dez- vous
  de trop croire un trans -- port ja -- loux.
}
% Sangaride
\tag #'(voix1 basse) {
  Trop heu -- reux un cœur qui peut croi -- re
  un dé -- pit qui sert à sa gloi -- re.
  Re -- ve -- nez ma rai -- son, re -- ve -- nez pour ja -- mais,
  re -- ve -- nez ma rai -- son, re -- ve -- nez pour ja -- mais,
  joi -- gnez- vous au dé -- pit pour é -- touf -- fer ma flam -- me,
  ré -- pa -- rez, s’il se peut, les maux qu’A -- mour m’a faits,
  ve -- nez ré -- ta -- blir dans mon â -- me
  les dou -- ceurs d’une heu -- reu -- se paix ;
  re -- ve -- nez, ma rai -- son, re -- ve -- nez pour ja -- mais,
  re -- ve -- nez, ma rai -- son, re -- ve -- nez pour ja -- mais.
}
\tag #'(voix1 voix3 basse) {
  Une in -- fi -- de -- li -- té cru -- el -- le
  n’ef -- fa -- ce point tous les ap -- pas
  d’un in -- fi -- dè -- le,
  et la rai -- son ne re -- vient pas
  si tôt qu’on l’a rap -- pel -- le.
  Et la rai -- son ne re -- vient pas,
  \tag #'voix3 { et la rai -- son, }
  et la rai -- son ne re -- vient pas
  si tôt qu’on l’a rap -- pel -- le.
}
\tag #'(voix1 basse) {
  A -- près u -- ne tra -- hi -- son
  si la rai -- son ne m’é -- clai -- re,
  le dé -- pit et la co -- lè -- re
  me tien -- dront lieu de rai -- son.
}
\tag #'(voix1 basse) {
  Qu’u -- ne pre -- mière a -- mour est bel -- le ?
  Qu’on a peine à s’en dé -- ga -- ger !
  Que l’on doit plaindre un cœur fi -- dè -- le
  lors -- qu’il est for -- cé de chan -- ger.
  Que l’on doit plaindre un cœur fi -- dè -- le
  lors -- qu’il est for -- cé de chan -- ger.
  Que l’on doit plaindre un cœur fi -- dè -- le
  que l’on doit plaindre un cœur fi -- dè -- le
  lors -- qu’il est for -- cé de chan -- ger.
  Que l’on doit plaindre un cœur fi -- dè -- le
  lors -- qu’il est for -- cé de chan -- ger.
}
\tag #'voix2 {
  Qu’u -- ne pre -- mière a -- mour est bel -- le ?
  Qu’on a peine à s’en dé -- ga -- ger !
  Que l’on doit plaindre un cœur fi -- dè -- le
  lors -- qu’il est for -- cé de chan -- ger,
  lors -- qu’il est for -- cé de chan -- ger.
  Que l’on doit plaindre un cœur fi -- dè -- le,
  Que l’on doit plaindre un cœur fi -- dè -- le
  lors -- qu’il est for -- cé de chan -- ger,
  qu’il est for -- cé de chan -- ger.
  Que l’on doit plaindre un cœur fi -- dè -- le
  lors -- qu’il est for -- cé de chan -- ger.
}
\tag #'voix3 {
  Qu’u -- ne pre -- mière a -- mour est bel -- le ?
  Qu’on a peine à s’en dé -- ga -- ger !
  Que l’on doit plaindre un cœur fi -- dè -- le
  lors -- qu’il est for -- cé de chan -- ger.
  Que l’on doit plaindre un cœur fi -- dè -- le,
  Que l’on doit plaindre un cœur fi -- dè -- le
  lors -- qu’il est for -- cé de chan -- ger.
  Que l’on doit plaindre un cœur fi -- dè -- le
  lors -- qu’il est for -- cé de chan -- ger.
  Que l’on doit plaindre un cœur fi -- dè -- le
  lors -- qu’il est for -- cé de chan -- ger.
}

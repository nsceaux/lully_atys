%% ACTE 5 SCENE 7
%% Cybele
Ve -- nez fu -- ri -- eux co -- ry -- ban -- tes,
ve -- nez joindre à mes cris vos cla -- meurs é -- cla -- tan -- tes ;
ve -- nez nym -- phes des eaux, ve -- nez dieux des fo -- rêts,
par vos plain -- tes les plus tou -- chan -- tes
se -- con -- der mes tris -- tes re -- grets.

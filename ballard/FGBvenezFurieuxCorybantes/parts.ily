\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \column {
  \livretPers Cybèle
  \livretVerse#8 { Venez furieux Corybantes, }
  \livretVerse#12 { Venez joindre à mes cris vos clameurs éclatantes ; }
  \livretVerse#12 { Venez nymphes des eaux, venez dieux des forêts, }
  \livretVerse#8 { Par vos plaintes les plus touchantes }
  \livretVerse#8 { Seconder mes tristes regrets. }
} #}))

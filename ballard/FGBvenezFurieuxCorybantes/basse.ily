\clef "basse" do1 |
fa,4 fa mi |
re2 dod\trill |
re1 |
la2 fad\trill |
sol mi |
fa1 |
sol4 fa4 sol sol, |
do1 |
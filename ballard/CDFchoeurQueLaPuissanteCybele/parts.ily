\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix"
          #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \column {
\livretPers \line { Chœurs des Peuples et des Zephirs. }
\livretVerse#7 { Que la puissante Cybèle }
\livretVerse#7 { Nous rende à jamais heureux. }
} #}))

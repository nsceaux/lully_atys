\score {
  \new StaffGroup <<
    \new Staff << \global \keepWithTag #'basse \includeNotes "basse" >>
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
}
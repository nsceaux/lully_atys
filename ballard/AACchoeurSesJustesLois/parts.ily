\piecePartSpecs
#`((dessus)
   (haute-contre)
   (haute-contre-sol)
   (taille)
   (quinte)
   (basse #:score "score-basse")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \column {
\livretVerse#4 { Ses justes lois, }
\livretVerse#4 { Ses grands exploits }
\livretVerse#8 { Rendent sa mémoire éternelle : }
\livretVerse#6 { Chaque jour, chaque instant }
\livretVerse#10 { Ajoute encore à son nom éclatant }
\livretVerse#6 { Une gloire nouvelle. }
}#}))

\clef "basse" <<
  \tag #'basse R2.*2
  \tag #'(basse-continue tous) {
    \tag #'tous <>^"B.C."
    re'4 re'4. re'8 |
    re'2. |
    \tag #'tous <>^"Tous"
  }
>>
sol4 sol8 la si sol |
do'2. |
<<
  \tag #'basse R2.*2
  \tag #'(basse-continue tous) {
    \tag #'tous <>^"B.C."
    fa'4 fa'4. fa'8 |
    fa'2. |
    \tag #'tous <>^"Tous"
  }
>>
fa4 fa8 sol la fa |
sib2. |
\twoVoices #'(basse basse-continue tous) <<
  { sol4. sol8 sol8. sol16 | }
  { sol2 sol4 | }
>>
fa4 fa sol |
mib2. |
re |
<<
  \tag #'basse R2.*2
  \tag #'(basse-continue tous) {
    \tag #'tous <>^"B.C."
    re'4 re'4. re'8 |
    re'2. |
    \tag #'tous <>^"Tous"
  }
>>
sol4 sol8 la si sol |
do'2. |
<<
  \tag #'basse R2.*2
  \tag #'(basse-continue tous) {
    \tag #'tous <>^"B.C."
    fa'4 fa'4. fa'8 |
    fa'2. |
    \tag #'tous <>^"Tous"
  }
>>
fa4 fa8 sol la fa |
sib2. |
\twoVoices #'(basse basse-continue tous) <<
  { sol4. sol8 sol8. sol16 | la4 la re4 | }
  { sol2 sol4 | la2 re4 | }
>>
la,2. |
re4 re'4. re'8 |
sol4 sol sol |
do do'4. do'8 |
fa4 fa fa |
sib,2 sib,4 |
fa2 re4 |
\twoVoices #'(basse basse-continue tous) <<
  { sol4 sol la | sib sib sol4 | }
  { sol2 la4 | sib2 sol4 | }
>>
re'2 \twoVoices #'(basse basse-continue tous) <<
  { re8 re | sol4. sol8 mib4 | }
  { re4 | sol2 mib4 | }
>>
fa4 fa,2 |
sib,4 <<
  \tag #'basse { r4 r | r4 }
  \tag #'(basse-continue tous) {
    \tag #'tous <>^"B.C."
    sib4. sib8 | fa4
    \tag #'tous <>^"Tous"
  }
>> fa4 fa |
do <<
  \tag #'basse { r4 r | r4 }
  \tag #'(basse-continue tous) {
    \tag #'tous <>^"B.C."
    do'4. do'8 | sol4
    \tag #'tous <>^"Tous"
  }
>> sol4. sol8 |
re2 re4 |
sol2 sol4 |
\twoVoices #'(basse basse-continue tous) <<
  { la4 la la |
    sib sib sol |
    re'2 re8 re |
    mib4 mib do |
    re2. | }
  { la2\trill la4 |
    sib2 sol4 |
    re'2 re4 |
    mib2 do4 |
    re4 re,2 | }
>>
sol,2. |

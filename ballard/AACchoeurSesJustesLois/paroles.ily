\ru#2 {
  \tag #'(vdessus vhaute-contre vtaille) {
    Ses jus -- tes lois,
    ses grands ex -- ploits
    Ses jus -- tes lois,
    ses grands ex -- ploits
  }
  \tag #'vbasse {
    Ses grands ex -- ploits
    ses grands ex -- ploits
  }
  ren -- dent sa mé -- moire é -- ter -- nel -- le.
}
Cha -- que jour, chaque ins -- tant,
cha -- que jour, chaque ins -- tant
a -- joute en -- core à son nom é -- cla -- tant
u -- ne gloi -- re nou -- vel -- le.
Cha -- que jour, chaque ins -- tant,
cha -- que jour, chaque ins -- tant
a -- joute en -- core à son nom é -- cla -- tant
u -- ne gloi -- re nou -- vel -- le.

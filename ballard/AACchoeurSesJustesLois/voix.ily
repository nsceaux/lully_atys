<<
  \tag #'vdessus {
    \clef "vbas-dessus" la'4^\markup\character "[Chœur des Heures]" la'4. sib'8 |
    la'2.\trill |
    % Manuscrit : re''4 re''4. sol''8 |
    re''4 re''4 sol'' |
    mi''2.\trill |
    do''4 do''4. re''8 |
    do''2.\trill |
    % Manuscrit : do''4 do''4. fa''8 |
    do''4 do'' fa'' |
    re''2.\trill |
    re''4. re''8 mib'' mib'' |
    mib''4 re''\trill re'' |
    re''2( do''4)\trill |
    re''2. |
    la'4 la'4. sib'8 |
    la'2.\trill |
    re''4 re''4. sol''8 |
    mi''2.\trill |
    do''4 do''4. re''8 |
    do''2.\trill |
    do''4 do''4. fa''8 |
    re''2.\trill |
    re''4. re''8 re''8. mi''16 |
    dod''4\trill dod'' re'' |
    re''2( dod''4)\trill |
    re''4 la'4. la'8 |
    si'4 si'\trill si' |
    do'' sol'4. sol'8 |
    la'4\trill la' la' |
    sib'2 re''4 |
    do''2\trill fa''4 |
    re''\trill sib' do'' |
    re'' mib''8[ re''] do''[ sib'] |
    la'2\trill re''8 re'' |
    re''4. re''8 mib''4 |
    re''4( do''2)\trill |
    sib'4 re''4. re''8 |
    do''4\trill do'' re'' |
    % mib''4 mib''4. fa''8 |
    mib''4 mib''4. mib''8 |
    re''4\trill re'' sol'' |
    fad''2\trill la'4 |
    sib'2 sib'4 |
    do''4 do'' do'' |
    re'' mib''8[ re''] do''[ sib'] |
    % la'2\trill re''8 re'' |
    la'2\trill re''8. re''16 |
    sib'4\trill sib' do'' |
    sib'( la'2)\trill |
    sol'2. |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" fad'4 fad'4. sol'8 |
    fad'2. |
    sol'4 sol'4. sol'8 |
    sol'2. |
    la'4 la'4. la'8 |
    la'2.\trill |
    % Manuscrit : fa'4 fa'4. fa'8 |
    fa'4 fa' fa' |
    fa'2. |
    sol'4. sol'8 sol'8. sol'16 |
    do'4 fa' re' |
    sol'2. |
    fad' |
    fad'4 fad'4. sol'8 |
    fad'2. |
    sol'4 sol'4. sol'8 |
    sol'2. |
    la'4 la'4. sib'8 |
    la'2. |
    fa'4 fa'4. fa'8 |
    fa'2. |
    sol'4. sol'8 sol'8. sol'16 |
    mi'4 mi' fa' |
    mi'2( fad'8)[ sol'] |
    fad'4 fad'4. fad'8 |
    sol'4 sol' sol' |
    sol' mi'4. mi'8 |
    fa'4 fa' fa' |
    fa'2 fa'4 |
    fa'2 la'4 |
    sol' sol' fad' |
    sol' sol'4. sol'8 |
    fad'2 fad'8 fad' |
    % sol'4 sol'4. sol'8 |
    sol'4. sol'8 sol'4 |
    fa'2( mib'4) |
    re' fa'4. fa'8 |
    fa'4 fa' fa' |
    sol'4 sol'4. sol'8 |
    sol'4 re' re' |
    re'2 fad'4 |
    sol'2 sol'4 |
    sol' fa'4. fa'8 |
    fa'4 fa' sol' |
    fad'2 fad'8 fad' |
    % sol'4 sol'4. sol'8 |
    sol'4 sol' sol' |
    sol'2( fad'4) |
    sol'2. |
  }
  \tag #'vtaille {
    \clef "vtaille" re'4 re'4. re'8 |
    re'2. |
    % Manuscrit : sib4 sib8[ do'] re'[ sib] |
    si4 si8[ do'] re'[ si] |
    do'2. |
    fa'4 fa'4. fa'8 |
    fa'2. |
    la4 la8[ sib] do'[ la] |
    sib2. |
    sib4. sib8 sib8. do'16 |
    la4 la sib |
    sib2( do'4) |
    % Manuscrit : re'2. |
    la2.\trill |
    re'4 re'4. re'8 |
    re'2. |
    % sib4 sib8[ do'] re'[ sib] |
    si4 si8[ do'] re'[ si] |
    do'2. |
    fa'4 fa'4. fa'8 |
    fa'2. |
    la4 la8[ sib] do'[ la] |
    sib2. |
    sib4. sib8 sib8. sib16 |
    la4\trill la la |
    la2. |
    la4 re'4. re'8 |
    re'4 re' re' |
    mi' do'4. do'8 |
    do'4 do' do' |
    re'2 sib4 |
    la2\trill re'4 |
    sib re' do' |
    sib re'4. re'8 |
    re'2 la8 la |
    sib4 sib sib |
    sib2( la4)\trill |
    sib4 sib4. sib8 |
    la4 la si |
    do'4 do'4. do'8 |
    sib!4 sib sib |
    la2\trill re'4 |
    re'2 re'4 |
    do' do' do' |
    sib re'4. re'8 |
    re'2 la8 la |
    sol4 sol mib' |
    % re'2( do'4) |
    re'2. |
    si2.\trill |
  }
  \tag #'vbasse {
    \clef "vbasse" R2.*2 |
    % Manuscrit : sol4 sol8[ la] sib[ sol] |
    sol4 sol8[ la] si[ sol] |
    do'2. |
    R2.*2 |
    fa4 fa8[ sol] la[ fa] |
    sib2. |
    sol4. sol8 sol8. sol16 |
    fa4 fa sol |
    mib2.\trill |
    re |
    R2.*2 |
    % sol4 sol8[ la] sib[ sol] |
    sol4 sol8[ la] si[ sol] |
    do'2. |
    R2.*2 |
    fa4 fa8[ sol] la[ fa] |
    sib2. |
    sol4. sol8 sol8. sol16 |
    la4 la re |
    la,2. |
    re4 re'4. re'8 |
    sol4 sol sol |
    do do'4. do'8 |
    fa4 fa fa |
    sib,2 sib,4 |
    fa2 re4 |
    sol4 sol la |
    sib sib sol |
    re'2 re8 re |
    sol4. sol8 mib4 |
    fa2. |
    sib,4 sib4. sib8 |
    fa4 fa fa |
    do do'4. do'8 |
    sol4 sol sol |
    re2 re4 |
    sol2 sol4 |
    la\trill la la |
    sib sib sol |
    re'2 re8 re |
    mib4 mib do |
    re2. |
    sol, |
  }
>>

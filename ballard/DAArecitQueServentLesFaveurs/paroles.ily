%% ACTE 3 SCENE 1
% Atys seul.
Que ser -- vent les fa -- veurs que nous fait la for -- tu -- ne
quand l’A -- mour nous rend mal -- heu -- reux ?
Je perds l’u -- ni -- que bien qui peut com -- bler mes vœux,
et tout au -- tre bien m’im -- por -- tu -- ne.
Que ser -- vent les fa -- veurs que nous fait la for -- tu -- ne
quand l’A -- mour nous rend mal -- heu -- reux ?

\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \column {
\livretPersDidas Atys seul
\livretVerse#12 { Que servent les faveurs que nous fait la fortune }
\livretVerse#8 { Quand l’Amour nous rend malheureux ? }
\livretVerse#12 { Je perds l’unique bien qui peut combler mes vœux, }
\livretVerse#8 { Et tout autre bien m’importune. }
\livretVerse#12 { Que servent les faveurs que nous fait la fortune }
\livretVerse#8 { Quand l’Amour nous rend malheureux ? }
} #}))

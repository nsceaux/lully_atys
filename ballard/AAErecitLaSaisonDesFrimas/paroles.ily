La sai -- son des fri -- mas peut- el -- le nous of -- frir
les fleurs que nous voy -- ons pa -- raî -- tre ?
Quel dieu les fait re -- naî -- tre
lors -- que l’hi -- ver les fait mou -- rir ?
Le froid cru -- el règne en -- co -- re ;
tout est gla -- cé dans les champs,
d’où vient que Flo -- re
de -- van -- ce le prin -- temps ?

Quand j’at -- tends les beaux jours, je viens tou -- jours trop tard,
plus le prin -- temps s’a -- vance, et plus il m’est con -- trai -- re ;
son re -- tour pres -- se le dé -- part
du hé -- ros à qui je veux plai -- re.
Pour lui fai -- re ma cour, mes soins ont en -- tre -- pris
de bra -- ver dé -- sor -- mais l’hi -- ver le plus ter -- ri -- ble,
dans l’ar -- deur de lui plaire on a bien -- tôt ap -- pris
à ne rien trou -- ver d’im -- pos -- si -- ble.
Dans l’ar -- deur de lui plaire on a bien -- tôt ap -- pris
à ne rien trou -- ver d’im -- pos -- si -- ble.

\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Le Temps
    \livretVerse#12 { La saison des frimas peut-elle nous offrir }
    \livretVerse#8 { Les fleurs que nous voyons paraître ? }
    \livretVerse#6 { Quel dieu les fait renaître }
    \livretVerse#8 { Lorsque l’hiver les fait mourir ? }
    \livretVerse#7 { Le froid cruel règne encore ; }
    \livretVerse#7 { Tout est glacé dans les champs, }
    \livretVerse#4 { D’où vient que Flore }
    \livretVerse#6 { Devance le printemps ? }
  }
  \column {
    \livretPers Flore
    \livretVerse#12 { Quand j’attends les beaux jours, je viens toujours trop tard, }
    \livretVerse#12 { Plus le printemps s’avance, et plus il m’est contraire ; }
    \livretVerse#8 { Son retour presse le départ }
    \livretVerse#8 { Du héros à qui je veux plaire. }
    \livretVerse#12 { Pour lui faire ma cour, mes soins ont entrepris }
    \livretVerse#12 { De braver désormais l’hiver le plus terrible, }
    \livretVerse#12 { Dans l’ardeur de lui plaire on a bientôt appris }
    \livretVerse#8 { À ne rien trouver d’impossible. }
  }
}#}))

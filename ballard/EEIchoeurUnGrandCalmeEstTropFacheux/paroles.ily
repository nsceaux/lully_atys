Un grand calme est trop fâ -- cheux,
nous ai -- mons mieux la tour -- men -- te.
Que sert un cœur qui s’ex -- emp -- te
de tous les soins a -- mou -- reux ?
À quoi sert une eau dor -- man -- te ?
Un grand calme est trop fâ -- cheux,
nous ai -- mons mieux la tour -- men -- te.

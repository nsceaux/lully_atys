\clef "basse" la4 mi |
fa do si, la, |
mi2 la4 mi |
fa do si, la, |
mi la, la4. la8 si4 do' fa do |
sol do do'4. do'8 |
si4 do' la re' |
sol2 sol4 mi |
la fa mi re |
la, re re4. re8 |
sol4 do \twoVoices #'(basse basse-continue tous) <<
  { fa4. fa8 }
  fa2
>> |
mi2 la4 mi |
fa do si, la, |
mi la,4

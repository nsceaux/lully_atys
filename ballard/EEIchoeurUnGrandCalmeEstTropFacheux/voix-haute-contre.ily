\clef "vhaute-contre2" mi'4 mi'4 |
do' do' re' mi' |
mi'2 mi'4 mi' |
do' do' re' mi' |
mi' do' la'4. la'8 |
sol'4 sol' la' sol' |
sol' mi' sol'4. sol'8 |
sol'4 sol' sol' fad' |
sol'2 sol'4 sol' |
mi' fa' sol' la' |
la' fa' la'4. la'8 |
sol'4 sol' la'4. la'8 |
sold'2 mi'4 mi'4 |
do' do' re' mi' |
mi' do'

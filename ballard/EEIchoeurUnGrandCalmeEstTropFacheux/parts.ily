\piecePartSpecs
#`((dessus #:indent 0)
   (haute-contre #:indent 0)
   (haute-contre-sol #:indent 0)
   (taille #:indent 0)
   (quinte #:indent 0)
   (basse #:indent 0
          #:tag-notes tous
          #:system-count 3)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \column {
  \livretPers Chœur de dieux de fleuves et de divinités de Fontaines
  \livretVerse#7 { Un grand calme est trop fâcheux, }
  \livretVerse#7 { Nous aimons mieux la tourmente. }
  \livretVerse#7 { Que sert un cœur qui s’exempte }
  \livretVerse#7 { De tous les soins amoureux ? }
  \livretVerse#7 { A quoi sert une eau dormante ? }
  \livretVerse#7 { Un grand calme est trop fâcheux, }
  \livretVerse#7 { Nous aimons mieux la tourmente. }
} #}))

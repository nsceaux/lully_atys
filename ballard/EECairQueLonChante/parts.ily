\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Le Dieu du Fleuve Sangar
    \livretVerse#7 { Que l’on chante, que l’on danse, }
    \livretVerse#7 { Rions tous lorsqu’il le faut ; }
    \livretVerse#6 { Ce n’est jamais trop tôt }
    \livretVerse#6 { Que le plaisir commence. }
    \livretVerse#7 { On trouve bientôt la fin }
    \livretVerse#7 { Des jours de réjouissance, }
    \livretVerse#8 { On a beau chasser le chagrin, }
    \livretVerse#8 { Il revient plutôt qu’on ne pense. }
  }
  \column {
    \livretPers Le Dieu du Fleuve Sangar, et le Chœur
    \livretVerse#7 { Que l’on chante, que l’on danse, }
    \livretVerse#7 { Rions tous lorsqu’il le faut ; }
    \livretVerse#6 { Ce n’est jamais trop tôt }
    \livretVerse#6 { Que le plaisir commence. }
    \livretVerse#7 { Que l’on chante, que l’on danse, }
    \livretVerse#7 { Rions tous lorsqu’il le faut. }
  }
}#}))

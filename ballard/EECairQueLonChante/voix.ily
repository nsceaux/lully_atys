<<
  \tag #'(vbasse basse) {
    \tag #'basse <>^\markup\character { Le Fleuve Sangar }
    \clef "vbasse" do4 do2 |
    sol2 sol4 mi mi2 |
    la2 la4 fa fa2 |
    do'2. do'4 si do' |
    sol2. sol4 sol2 |
    re2 re4 re re2 |
    la2 la4 mi mi2 |
    fa2. re4 re sol |
    do2. do4 do do |
    sol2 mi4 fa2. |
    re4 re re mi2 mi4 |
    la,2 la,4 la la la |
    re2 re4 sol2. |
    mi4 re\trill do sol2 sol4 |
    do2 do4 r4 r do'4 |
    la\trill la la sib2 sib4 |
    sol2\trill sol4 la2 la4 |
    fa mi re la2 la4 |
    r4 r la4 re' re' re' |
    si2\trill si4 do'2 do'4 |
    la2\trill la4 sib la sib |
    fa2 fa4 r fa re |
    mib2 mib4 do do do |
    re re sib, mib2 do4 |
    re do re sol,2 sol,4 |
    r sol sol mi2\trill mi4 |
    fa fa fa re re re |
    sol2 mi4 la fa sol |
    do2 do4 do do2 |
    sol2 sol4 mi mi2 |
    la2 la4 fa fa2 |
    do'2. do'4 si\trill do' |
    sol2. sol4 sol2 |
    re2 re4 re re2 |
    la2 la4 mi mi2 |
    fa2. re4 re sol |
    do2.
  }
  \tag #'vhaute-contre \clef "vhaute-contre"
  \tag #'(vtaille1 vtaille2) \clef "vtaille"
  \tag #'(vhaute-contre vtaille1 vtaille2) { r2*3/2 R1.*35 r2*3/2 }
>>
<<
  \tag #'(vhaute-contre basse) {
    \tag #'basse \ffclef "vhaute-contre"
    <>^\markup \character "Chœur des Fleuves" mi'4 mi'2 |
    re'\trill re'4 sol' sol'2 |
    mi' mi'4 la' la'2 |
    fad'2. sol'4 sol' fad' |
    sol'2. re'4 mi'2 |
    fa' fa'4 fa' fa'2 |
    mi'\trill mi'4 sol' sol'2 |
    fa'2. re'4 mi' fa' |
    mi'2.\trill mi'4 mi' mi' |
    re'2 mi'4 do'2. |
    fa'4 fa' fa' mi'2 mi'4 |
    mi'2 mi'4 mi' mi' mi' |
    fa'2 fa'4 re'2.\trill |
    sol'4 fa' mi' re'2 sol'4 |
    mi'2 mi'4 mi' mi'2 |
    re'2\trill re'4 sol' sol'2 |
    mi'2\trill mi'4 la' la'2 |
    fad'2. sol'4 sol' fad' |
    sol'2. re'4 mi'2 |
    fa'2 fa'4 fa' fa'2 |
    mi'2\trill mi'4 sol' sol'2 |
    fa'2. re'4 mi' fa' |
    mi'2.\trill
  }
  \tag #'vtaille1 {
    do'4 do'2 |
    si2\trill si4 mi' mi'2 |
    do'\trill do'4 mi' mi'2 |
    re'2.\trill si4 do' la |
    si2. si4 dod'2\trill |
    re'2 re'4 re' re'2 |
    do'2 do'4 mi' mi'2 |
    do'2. do'4 do' si |
    do'2. do'4 do' do' |
    si2 do'4 la2.\trill |
    re'4 re' re' si2 mi'4 |
    dod'2 dod'4 dod' dod' dod' |
    re'2 re'4 si2.\trill |
    do'4 si do' do'2 si4 |
    do'2 do'4 do' do'2 |
    si2\trill si4 mi' mi'2 |
    do' do'4 mi' mi'2 |
    re'2. si4 do' la |
    si2. si4 dod'2\trill |
    re'2 re'4 re' re'2 |
    do'2 do'4 mi' mi'2 |
    do'2. do'4 do' si |
    do'2.
  }
  \tag #'vtaille2 {
    sol4 sol2 |
    sol2 sol4 si si2 |
    la2\trill la4 do' do'2 |
    la2.\trill mi'4 mi' re' |
    re'2. sol4 sol2 |
    la la4 la si2 |
    do' do'4 do' do'2 |
    la2.\trill sol4 sol sol |
    sol2. sol4 sol sol |
    sol2 sol4 fa2. |
    la4 la la la2 sold4\trill |
    la2 la4 la la la |
    la2 la4 sol2. |
    sol4 sol sol sol2 sol4 |
    sol2 sol4 sol sol2 |
    sol sol4 si si2 |
    la la4 do' do'2 |
    la2.\trill mi'4 mi' re' |
    re'2. sol4 sol2 |
    la2 la4 la si2 |
    do' do'4 do' do'2 |
    la2.\trill sol4 sol sol |
    sol2.
  }
  \tag #'vbasse {
    do4 do2 |
    sol2 sol4 mi mi2 |
    la la4 la, la,2 |
    re2. mi4 do re |
    sol,2. sol4 sol2 |
    re re4 re re2 |
    la2 la4 mi mi2 |
    fa2. sol4 sol sol, |
    do2. do4 do do |
    sol2 mi4 fa2. |
    re4 re re mi2 mi4 |
    la,2 la,4 la la la |
    re2 re4 sol2. |
    mi4 re\trill do sol2 sol4 |
    do2 do4 do do2 |
    sol2 sol4 mi mi2 |
    la2 la4 la, la,2 |
    re2. mi4 do re |
    sol,2. sol4 sol2 |
    re2 re4 re re2 |
    la2 la4 mi mi2 |
    fa2. sol4 sol sol, |
    do2.
  }
>>

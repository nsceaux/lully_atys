\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new GrandStaff \with { instrumentName = "Violons" } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille1 \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille2 \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\center-column\smallCaps { [Le Fleuve Sangar] }
      } \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'(sangar choeur) \includeLyrics "paroles"
    >>
    \new Staff \with { instrumentName = "B.C." } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion { s2. s1.*35 s2. \bar "" \break }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
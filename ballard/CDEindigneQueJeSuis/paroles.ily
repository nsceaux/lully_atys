In -- di -- gne que je suis des hon -- neurs qu’on m’a -- dres -- se,
je dois les re -- ce -- voir au nom de la dé -- es -- se ;
j’o -- se, puis -- qu’il lui plaît, lui pré -- sen -- ter vos vœux :
pour le prix de vo -- tre zè -- le,
que la puis -- san -- te Cy -- bè -- le
vous rende à ja -- mais heu -- reux.

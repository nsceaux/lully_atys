\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \column {
\livretPers Atys
\livretVerse#12 { Indigne que je suis des honneurs qu’on m’adresse, }
\livretVerse#12 { Je dois les recevoir au nom de la Déesse ; }
\livretVerse#12 { J’ose, puisqu’il lui plaît, lui présenter vos vœux : }
\livretVerse#7 { Pour le prix de votre zèle, }
\livretVerse#7 { Que la puissante Cybèle }
\livretVerse#7 { Vous rende à jamais heureux. }
} #}))

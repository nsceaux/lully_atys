\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse ;#:score-template "score-basse-continue-voix"
          #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Le Sommeil
    \livretVerse#5 { Dormons, dormons tous ; }
    \livretVerse#7 { Ah que le repos est doux ! }
    \livretPers Morphée
    \livretVerse#12 { Régnez, divin Sommeil, régnez sur tout le monde, }
    \livretVerse#12 { Répandez vos pavots les plus assoupissants ; }
    \livretVerse#8 { Calmez les soins, charmez les sens, }
    \livretVerse#12 { Retenez tous les cœurs dans une paix profonde. }
  }
  \column {
    \livretPers Phobétor
    \livretVerse#8 { Ne vous faites point violence, }
    \livretVerse#8 { Coulez, murmurez, clairs ruisseaux, }
    \livretVerse#8 { Il n’est permis qu’au bruit des eaux }
    \livretVerse#12 { De troubler la douceur d’un si charmant silence. }
    \livretPers Le Sommeil, Morphée, Phobétor et Phantase
    \livretVerse#5 { Dormons, dormons tous, }
    \livretVerse#7 { Ah que le repos est doux ! }
    \line\italic { On reprend le prélude. }
  }
}#}))

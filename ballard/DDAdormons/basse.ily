\clef "basse" re4( mi) fad( re) |
sol( la) sib( sol) |
fad\trill( mi) fad( re) |
sol( fad4) sol( sol,) |
re( mib) re( do) |
sib,( la,) sib,( sol,) |
do( sib,) do( la,) |
re( do) sib,( sol,4) |
re( la) re'( do') |
sib( la) sib( sol) |
mib'( re') mib'( do') |
re'( do') re'( re) |
sol( la) sib( sol) |
fad( mi) fa4( re) |
mib( re) mib( do) |
re( do) si,2 |
do4( re) mib( do) |
sol2 fa4( mib) |
re2 do4\trill( sib,) |
fa( sol) la( fa) |
sib2 la\trill |
sol1 |
fa |
mib |
re4( la,) sib,( sol,) |
fad,1\trill |
sol,4( re) sol( fa) |
mi4( re) mi( do) |
fa( do) fa( mib) |
re1 |
sol4( re) sol( fa) |
mib( re) mib( do) |
sol( fa) sol( sol,) |
do( re) mib( fa) |
sol( sol,) sol( la) |
sib( sib,) sib,( do) |
re( sib,) re( mib) |
fa( mib) fa( fa,) |
sib,1 |
si,\trill |
do2 dod\trill |
re4( la,) re( do) |
sib,( la,) sib,( sol,) |
re( do) re( re,) |
sol4( fa) mib( re) |
do( sib,) la,( sol,) |
re( do) re( re,) |
sol,2 sol |
fa fa4 sol |
mib1\trill |
re |
re'4. do'8 sib4. la8 |
sol2 fa4. mib8 |
re2 do4\trill sib, |
fa1 |
fa,2. fa4 |
sib4. la8 sol4. fa8 |
mib2 re4. mib8 |
do2\trill fa4 fa, |
sib,1 |
sib2. sib4 |
la2\trill sol |
fa2. fa4 |
sol2. sol4 |
la2. la4 |
fa2 dod4 re |
la,1 |
re4. mi8 fad2 |
sol2. sol4 |
fa2. sol4 |
mib2 mib4 fa |
re2. re4 |
do2.\trill do4 |
sib,2. do4 |
re2 re, |
sol,4( la,) sib,( do) |
re( mi) fad( re) |
sol( la) sib( sol) |
fad\trill( mi) fad( re) |
sol( fad) sol( sol,) |
re( mib) re( do) |
sib,( la,) sib,( sol,) |
do( sib,) do( la,) |
re( do) sib,( sol,) |
re( la) re'( do') |
sib( la) sib( sol) |
mib'( re') mib'( do') |
re'( do') re'( re) |
sol2. sol4 |
sol1~ |
sol |
re |
la2. sol4 |
fad2 sol4. fad8 |
mi2 fa~ |
fa mib4 re8 do |
si,2.\trill si,4 |
do4 re mib fa |
sol2. sol4 |
sol1~ |
sol~ |
sol2 sol4 fa |
mib( re) do( sib,) |
la,2. la,4 |
sib,( do) re( mib) |
fa2. fa4 |
fa1~ |
fa~ |
fa2 fa, |
sib,1 |
do |
re |
mib |
re2. re4 |
re1~ |
re~ |
re |
sol |
fa |
mib |
re |
re, |
sol,1 |

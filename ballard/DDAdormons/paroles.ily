\tag #'(sommeil basse) {
  % Le Sommeil
  Dor -- mons, __ dor -- mons tous ;
  Ah ! ah ! que le re -- pos est doux !
  Ah ! que le re -- pos est doux !
}
\tag #'(morphee basse) {
  % Morphée
  Ré -- gnez, ré -- gnez, di -- vin Som -- meil,
  ré -- gnez, ré -- gnez sur tout le mon -- de,
  ré -- pan -- dez vos pa -- vots les plus as -- sou -- pis -- sants ;
  cal -- mez les soins, char -- mez les sens,
  cal -- mez les soins, char -- mez, char -- mez les sens,
  re -- te -- nez tous les cœurs dans u -- ne paix pro -- fon -- de,
  re -- te -- nez tous les cœurs dans u -- ne paix,
  dans u -- ne paix pro -- fon -- de.
  
}
\tag #'(phobetor basse) {
  % Phobetor
  Ne vous fai -- tes point vi -- o -- len -- ce,
  cou -- lez, __ mur -- mu -- rez, clairs ruis -- seaux,
  cou -- lez, __ mur -- mu -- rez, clairs ruis -- seaux,
  il n’est per -- mis qu’au bruit des eaux
  de trou -- bler la dou -- ceur d’un si char -- mant si -- len -- ce.
  Il n’est per -- mis qu’au bruit des eaux
  de trou -- bler la dou -- ceur d’un si char -- mant si -- len -- ce.
}
% Le Sommeil, Morphée, Phobetor, et Phantase.
\tag #'(sommeil basse) {
  Dor -- mons, __ dor -- mons tous,
  ah ! ah ! que le re -- pos est doux !
  Ah ! que le re -- pos \tag #'sommeil { est doux ! }
}
\tag #'(morphee basse) {
  Dor -- mons, dor -- mons tous,
  ah ! que le re -- pos est doux !
  Que le re -- pos, ah ! que le re -- pos, que le re -- pos __ est doux !
  Que le re -- pos, que le re -- pos __ est doux !
  Ah ! que le re -- pos __ est doux !
  Ah ! que le re -- pos, que le re -- pos __ est doux !
  Ah ! ah ! que le re -- pos, que le re -- pos __ est doux !
  Que le re -- pos, que le re -- pos,
  ah ! que le re -- pos,
  ah ! __ que le re -- pos,
  ah ! __ que le re -- pos __ est doux !
}
\tag #'phantase {
  Dor -- mons, dor -- mons, dor -- mons tous,
  ah ! __ que le re -- pos, que le re -- pos est doux !
  Dor -- mons, dor -- mons tous,
  dor -- mons, __ dor -- mons tous,
  dor -- mons, dor -- mons, dor -- mons, __ dor -- mons __ tous,
  dor -- mons, __ dor -- mons tous,
  dor -- mons, dor -- mons, dor -- mons, __ dor -- mons __ tous,
  ah ! __ que le re -- pos, que le re -- pos __ est doux !
  Dor -- mons, dor -- mons, dor -- mons, dor -- mons __ tous,
  ah ! que le re -- pos est doux !
  Que le re -- pos, que le re -- pos __ est doux !
}
\tag #'phobetor {
  Dor -- mons, dor -- mons __ tous,
  ah ! que le re -- pos est doux !
  Ah ! __ que le re -- pos, que le re -- pos est doux !
  Dor -- mons tous,
  ah ! que le re -- pos est doux !
  Que le re -- pos est doux !
  Dor -- mons, __ dor -- mons tous,
  ah ! ah ! que le re -- pos __ est doux !
  Dor -- mons __ tous,
  ah ! ah ! ah ! que le re -- pos __ est doux !
}

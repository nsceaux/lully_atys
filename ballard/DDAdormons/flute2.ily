\clef "dessus" R1*47 |
sib'4. sib'8 do''4 sib' |
la'2\trill la'4 sib' |
sol' sib' la'4.\trill sol'8 |
fad'2.\trill r8 fad' |
fad'4. fad'8 sol'4 la' |
sib'2 sib'4. do''8 |
re''2 la'4 sib' |
la'2\trill sib'~ |
sib'2 la'4.(\trill sol'16 la') |
sib'4. do''8 re''2 |
sol'4 sol'' fa''4. sol''8 |
mib''4. re''8 do''2\trill |
re''4 re'' mib''2~ |
mib'' re''4 mi''? |
fa''4 fa'' fa'' mi''\trill |
fa''2 fa''4 fa'' |
fa''2 mi''4.\trill re''8 |
dod''2.\trill dod''4 |
re'' re'' mi'' re'' |
re''2 dod''4.(\trill si'16 dod'') |
re''2 do''4. re''8 |
sib'2\trill do''4. sib'8 |
la'2\trill la''4 sib'' |
sol''2\trill sol''4 la'' |
fa''2\trill fa''4 sol'' |
mib''2\trill mib''4 fa'' |
re''2.\trill do''8 sib' |
la'4. la'8 la'4.\trill sol'8 |
sol'2 r |
R1*47 |

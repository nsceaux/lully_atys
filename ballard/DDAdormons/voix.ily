<<
  %% Le Sommeil
  \tag #'(sommeil basse) {
    \clef "vhaute-contre" r2 r4 re'^\markup\character "Le Sommeil" |
    re'1~ |
    re'2. la4 |
    sib1 |
    la |
    re'2. r8 fa'( |
    mi'2.) mi'4 |
    fad'4. fad'8 sol'4( fad'8) sol' |
    fad'1 |
    sol' |
    r4 sol' sol' sol' |
    sol'2( fad'4.) sol'8 |
    \footnoteHereFull #'(0 . 0) \markup {
      [Manuscrit] Reprise des mesures 58 à 70.
    }
    sol'1 |
    <<
      \tag #'basse { s1*63 \sommeilMark }
      \tag #'sommeil { R1*63 <>^\markup\character "Le Sommeil" }
    >>
    r2 r4 re' |
    re'1~ |
    re'2. la4 |
    sib1 |
    la |
    re'2. r8 fa'( |
    mi'2.) mi'4 |
    fad'4 fad' sol'( fad'8) sol' |
    fad'1 |
    sol' |
    r4 sol' sol'4. sol'8 |
    <<
      \tag #'basse { sol'2( fad'4) }
      \tag #'sommeil { sol'2( fad'4.) sol'8 | sol'1 | R1*34 | }
    >>
  }
  %% Morphée
  \tag #'(morphee basse) {
    <<
      \tag #'basse { s1*13 \morpheeMark }
      \tag #'morphee {
        \clef "vhaute-contre" R1*13 <>^\markup\character Morphée
      }
    >>
    r2 r4 re' |
    sol'2. la'4 |
    fad'4. fad'8 sol'4 re' |
    mib'2. mib'4 |
    re'2.\trill mib'4 |
    fa'4. fa'8 mib'4. re'8 |
    do'2\trill do' |
    r fa'4 fa' |
    sib2 sib4. do'8 |
    la2.\trill re'4 |
    re' re' re' do' |
    re'1 |
    r4 la la la |
    sib1 |
    r4 sib sib4( la8) sib |
    la1\trill |
    r4 la re'4. mib'8 |
    si2. si4 |
    do'2. do'4 |
    do'2( si4.) do'8 |
    do'2 sol4 la |
    sib2 sib4 do' |
    re'4. re'8 re'4 mib' |
    fa'2. sib4 |
    sib2( la) |
    sib2 re'4. re'8 |
    re'4. mi'8 fa'2 |
    mi'4.\trill mi'8 mi'4. mi'8 |
    fad'4. fad'8 fad'4. fad'8 |
    sol'2. sib4 |
    sib2( la)\trill |
    sol1 |
    R1*2 |
    <<
      \tag #'basse { s1*40 s2. \morpheeMark }
      \tag #'morphee { R1*40 r2 r4 <>^\markup\character Morphée }
    >>
    re'4 | \noBreak
    re'2. re'4 |
    mib'1 |
    re'2 sol'~ |
    sol'4. fa'8 fa'4( mi'8) fa' |
    mi'2\trill~ mi'4. re'8 |
    re'2. re'4 |
    do'4.\trill do'8 do'2 |
    re'4. re'8 mib'4. mib'8 |
    fa'4 fa' fa' re'4 |
    mib'2.( re'8) mib' |
    re'4\trill sol' fa' mib' |
    re' mib' fa' sol' |
    mib'2.( re'8) mib' |
    re'1 |
    sol'4 fa' mib'4. mib'8 |
    mib'2.( re'8) mib' |
    re'1\trill |
    do'4 fa' mib' re' |
    do' re' mib'4. fa'8 |
    re'2.~ re'8 mib' |
    do'1\trill |
    re'2 r |
    mi'4. mi'8 mi'4. mi'8 |
    fad'4. fad'8 fad'4. fad'8 |
    sol'2.( fad'8) sol' |
    fad'4 re' do' sib |
    la re' do' sib8[ la] |
    sib1 |
    la4. re'8 re'4. la8 |
    sib2 mib'~ |
    mib'4. re'8 re'4. re'8 |
    re'2 do'~ |
    do'4. do'8 sib4( la8) sib |
    sib2( la4.)\trill sol8 |
    sol1 |
  }
  %% Phantase
  \tag #'phantase {
    \clef "vtaille" R1*87 |
    r2 r4 <>^\markup\character Phantase la |
    sib2. sib4 |
    do'1~ |
    do'4. sib8 sib2 |
    la\trill re'~ |
    re'4. do'8 do'4. do'8 |
    do'4. do'8 sib4. sib8 |
    sib4. sib8 la4.\trill la8 |
    si4. si8 do'2 |
    re'4 re' re'2~ |
    re'4 do' do'2 |
    si4 mib' re' do' |
    si do' re'2~ |
    re'4 do' do'2~ |
    do'2( si) |
    do'2. do'4 |
    do'1~ |
    do'4 sib sib2 |
    la4\trill re' do' sib |
    la sib do'2~ |
    do'4 sib sib2~ |
    sib( la) |
    sib sib~ |
    sib4. la8 la4. la8 |
    la4. re'8 re'4. re'8 |
    re'2( do'4\trill sib8) do' |
    re'4 sib la sol |
    fad\trill sol la2~ |
    la4 sol sol2~ |
    sol( fad) |
    sol do'4. sib8 |
    la4. la8 la4 sib |
    sol4. sol8 sol4. la8 |
    fad4. fad8 sol4( fad8) sol |
    sol2( fad4.\trill) sol8 |
    sol1 |
  }

  %% Phobetor
  \tag #'(phobetor basse) {
    <<
      \tag #'basse { s1*47 \phobetorMark }
      \tag #'phobetor { \clef "vbasse" R1*47 <>^\markup\character Phobétor }
    >>
    sol4 sol sol sol |
    fa2 fa4 sol |
    mib1\trill |
    re2. r8 re' |
    re'4.(\melisma do'8 sib4. la8 |
    sol2)\melismaEnd fa4. mib8 |
    re2 do4\trill sib, |
    fa1 |
    r2 r4 r8 fa |
    sib4.(\melisma la8 sol4. fa8 |
    mib2)\melismaEnd re4. mib8 |
    do2\trill fa8[ mib] fa4 |
    sib,1 |
    r4 sib sib sib |
    la la sol4.\trill sol8 |
    fa2 fa4 fa |
    sol2 sol4\trill sol |
    la2. la4 |
    fa4 fa dod re |
    la,1 |
    re |
    r4 sol sol sol |
    fa fa fa sol |
    mib2 mib4 fa |
    re2 re4 re |
    do do do do |
    sib,2. do4 |
    re1 |
    sol,2 r |
    \tag #'phobetor {
      R1*11 |
      r2 r4 <>^\markup\character Phobétor re |
      sol2. sol4 |
      sol1~ |
      sol |
      re |
      la2. sol4 |
      fad4.\trill fad8 sol4. fad8 |
      mi2 fa~ |
      fa4. fa8 mib4 re8[ do] |
      si,2. si,4 |
      do re mib fa |
      sol2. sol4 |
      sol1 |
      sol |
      r2 sol4 fa |
      mib re do sib, |
      la,2.\trill la,4 |
      sib, do re mib |
      fa2. fa4 |
      fa1~ |
      fa2. fa4 |
      fa1 |
      sib,2 r |
      do1 |
      re4. re8 re4. re8 |
      mib2.( re8) mib |
      re2. re4 |
      re1~ |
      re |
      re |
      sol |
      fa |
      mib4. mib8 mib4. mib8 |
      re1~ |
      re2. re4 |
      sol,1 |
    }
  }
>>

<<
  \tag #'(cybele basse) {
    \cybeleMark r2 r8 sib'16 sib' sib'8 do''16 re'' |
    do''4.\trill do''8 do''8. do''16 re''8. la'16 |
    sib'8 sib' r sib'16 sib' si'8 si'16 si' do''8 re'' |
    mib''4 mib''8. do''16 sol'8.\trill sol'16 sol'8. do''16 |
    la'8\trill la' sib' sib' sib' la' |
    sib'4 r8 re''16 fa'' sib'8 sib'16 sib' |
    sol'4\trill r8 mib'' si'8. si'16 si'8. do''16 |
    do''8 do'' r mib'' mib''8. re''16 do''8. sib'16 |
    la'4\trill la'8 la'16 la' la'8. sib'16 |
    fad'8 fad' r sib' do'' re'' la'8.\trill sib'16 |
    sol'4 r8 re'' re'' mib'' fa'' re'' |
    mib''4 mib''8 mib''16 re'' do''8. sib'16 |
    la'8\trill la' r sib' do'' re'' la'8.\trill sib'16 |
    sol'4 r8 re'' re'' re''16 mib''16 fa''8 fa''16 fa'' |
    sib'8 sib' r mib'' do''8.\trill do''16 do''8. do''16 |
    la'8.\trill la'16 la' la' sib' do'' fa'4 r8 fa'' |
    re''8.\trill re''16 re''8 mi'' dod''16 mi'' mi'' fa'' dod''8. re''16 |
    re''4 r8 la' fad'4 r |
    r re''8 re''16 re'' fad'8 fad'16 fad' sol'8 sol'16 la' |
    sib'8 sib' r sib'16 re'' sol'8. fa'16 mi'8\trill mi'16 fa' |
    \custosNote re'4
  }
  \tag #'melisse {
    \clef "vbas-dessus" R1*4 R2.*2 R1*2 R2. R1 |
    r4 r8 si'^\markup\character Mélisse si' do'' re'' si' |
    do''4 do''8 do''16 sib'! la'8. sol'16 |
    fad'8 fad' r sol' la' sib' fad'8. sol'16 |
    sol'4 r r2 |
    R1*6 |
  }
>>

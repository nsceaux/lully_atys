%% ACTE 5 SCENE 5
\tag #'(cybele basse) {
  Je com -- mence à trou -- ver sa pei -- ne trop cru -- el -- le,
  u -- ne ten -- dre pi -- tié rap -- pel -- le
  l’a -- mour que mon cou -- roux croy -- ait a -- voir ban -- ni,
  ma ri -- va -- le n’est plus, A -- tys n’est plus cou -- pa -- ble.
  Il est ai -- sé d’ai -- mer un cri -- mi -- nel ai -- ma -- ble
  a -- près l’a -- voir pu -- ni.
}
Il est ai -- sé d’ai -- mer un cri -- mi -- nel ai -- ma -- ble
a -- près l’a -- voir pu -- ni.
\tag #'(cybele basse) {
  Que son dé -- ses -- poir m’é -- pou -- van -- te !
  Ses jours sont en pé -- ril, et j’en fré -- mis d’ef -- froi :
  je veux d’un soin si cher ne me fi -- er qu’à moi,
  al -- lons… mais quel spec -- table à mes yeux se pré -- sen -- te ?
  C’est A -- tys mou -- rant que je vois !
}

\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'cybele  \includeNotes "voix"
    >> \keepWithTag #'cybele \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'melisse \includeNotes "voix"
    >> \keepWithTag #'melisse \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion { s1*4 s2.*2 s1*2 s2. s1\break }
    >>
  >>
  \layout { }
  \midi { }
}
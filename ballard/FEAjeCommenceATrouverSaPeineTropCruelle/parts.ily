\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix"
          #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Cybèle
    \livretVerse#12 { Je commence à trouver sa peine trop cruelle, }
    \livretVerse#8 { Une tendre pitié rappelle }
    \livretVerse#12 { L’Amour que mon couroux croyait avoir banni, }
    \livretVerse#12 { Ma rivale n’est plus, Atys n’est plus coupable, }
    \livretVerse#12 { Qu’il est aisé d’aimer un criminel aimable }
    \livretVerse#6 { Après l’avoir puni. }
  }
  \column {
    \null
    \livretVerse#8 { Que son désespoir m’épouvante ! }
    \livretVerse#12 { Ses jours sont en péril, et j’en frémis d’effroi : }
    \livretVerse#12 { Je veux d’un soin si cher ne me fier qu’à moi, }
    \livretVerse#12 { Allons… mais quel spectable à mes yeux se présente ? }
    \livretVerse#8 { C’est Atys mourant que je vois ! }
  }
}#}))

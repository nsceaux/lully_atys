En vain j’ai res -- pec -- té la cé -- lè -- bre mé -- moi -- re
des hé -- ros des siè -- cles pas -- sés ;
c’est en vain que leurs noms si fa -- meux dans l’his -- toi -- re,
du sort des noms com -- muns ont é -- té dis -- pen -- sés :
nous voy -- ons un hé -- ros dont la bril -- lan -- te gloi -- re
les a pres -- que tous ef -- fa -- cé.
Nous voy -- ons un hé -- ros dont la bril -- lan -- te gloi -- re
les a pres -- que tous ef -- fa -- cé.

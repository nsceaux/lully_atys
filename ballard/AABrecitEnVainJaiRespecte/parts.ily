\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \column {
\livretVerse#12 { En vain j’ai respecté la célèbre mémoire }
\livretVerse#8 { Des héros des siècles passés ; }
\livretVerse#12 { C’est en vain que leurs noms si fameux dans l’histoire, }
\livretVerse#12 { Du sort des noms communs ont été dispensés : }
\livretVerse#12 { Nous voyons un héros dont la brillante gloire }
\livretVerse#8 { Les a presque tous effacés. }
} #}))

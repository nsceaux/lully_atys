% Sangaride, et Doris.
\tag #'(voix1 voix2 basse) {
  Al -- lons, al -- lons, ac -- cou -- rez tous,
  Cy -- bè -- le va des -- cen -- dre.
  Al -- lons, al -- lons, ac -- cou -- rez tous,
  Cy -- bè -- le va des -- cen -- dre.
  Al -- lons, al -- lons, ac -- cou -- rez tous,
  Cy -- bè -- le va des -- cen -- dre.
}
\tag #'(voix1 basse) {
  % Sangaride
  Que dans nos con -- certs les plus doux,
  son nom sa -- cré se fasse en -- ten -- dre.
  % Atys
  Sur l’u -- ni -- vers en -- tier son pou -- voir doit s’é -- ten -- dre.
  % Sangaride
  Les Dieux sui -- vent ses lois et crai -- gnent son cou -- roux.
}
% Atys, Sangaride, Idas, Doris
\tag #'(voix1 voix2 voix3 voix4 basse) {
  Quels hon -- neurs ! quels res -- pects ne doit- on point lui ren -- dre ?
  Quels hon -- neurs ! quels res -- pects ne doit- on point lui ren -- dre ?
  Al -- lons, al -- lons, ac -- cou -- rez tous,
  al -- lons, al -- lons, ac -- cou -- rez tous,
  Cy -- bè -- le va des -- cen -- dre.
  Al -- lons, al -- lons, ac -- cou -- rez tous,
  al -- lons, al -- lons, ac -- cou -- rez tous,
  Cy -- bè -- le va des -- cen -- dre.
}
\tag #'(voix1 basse) {
  % Sangaride
  É -- cou -- tons les oi -- seaux de ces bois d’a -- len -- tour,
  ils rem -- plis -- sent leurs chants d’u -- ne dou -- ceur nou -- vel -- le.
  On di -- rait que dans ce beau jour,
  ils ne par -- lent que de Cy -- bè -- le.
  % Atys
  Si vous les é -- cou -- tez, ils par -- le -- ront d’a -- mour.
  Un roi re -- dou -- ta -- ble,
  a -- mou -- reux, ai -- ma -- ble,
  va de -- ve -- nir votre é -- poux ;
  tout par -- le d’a -- mour pour vous.
  % Sangaride
  Il est vrai, je tri -- omphe, et j’ai -- me ma vic -- toi -- re.
  Quand l’a -- mour fait ré -- gner, est- il un plus grand bien ?
  Pour vous, A -- tys, vous n’ai -- mez rien,
  et vous en fai -- tes gloi -- re.
  % Atys
  L’a -- mour fait trop ver -- ser de pleurs ;
  sou -- vent ses dou -- ceurs sont mor -- tel -- les.
  Il ne faut re -- gar -- der les bel -- les
  que comme on voit d’ai -- ma -- bles fleurs.
  J’ai -- me les ro -- ses nou -- vel -- les,
  j’ai -- me les voir s’em -- bel -- lir,
  sans leurs é -- pi -- nes cru -- el -- les,
  j’ai -- me -- rais, j’ai -- me -- rais à les cueil -- lir,
  sans leurs é -- pi -- nes cru -- el -- les,
  j’ai -- me -- rais à les cueil -- lir.
  % Sangaride
  Quand le pé -- ril est a -- gré -- a -- ble,
  le moy -- en de s’en a -- lar -- mer ?
  Est-ce un grand mal de trop ai -- mer
  ce que l’on trouve ai -- ma -- ble ?
  Est-ce un grand mal de trop ai -- mer
  ce que l’on trouve ai -- ma -- ble ?
  Peut- on être in -- sen -- sible aux plus char -- mants ap -- pas ?
  % Atys
  Non vous ne me con -- nais -- sez pas.
  Je me dé -- fends d’ai -- mer au -- tant qu'il m’est pos -- si -- ble,
  Je me dé -- fends d’ai -- mer,
  Je me dé -- fends d’ai -- mer au -- tant qu’il m’est pos -- si -- ble ;
  si j’ai -- mais, un jour, par mal -- heur,
  je con -- nais bien mon cœur
  il se -- rait trop sen -- si -- ble,
  il se -- rait trop sen -- si -- ble.
  Si j’ai -- mais, un jour, par mal -- heur,
  je con -- nais bien mon cœur
  il se -- rait trop sen -- si -- ble,
  il se -- rait trop sen -- si -- ble.
  Mais il faut que cha -- cun s’as -- sem -- ble près de vous,
  Cy -- bè -- le pour -- rait nous sur -- pren -- dre.
}
% Atys, et Idas.
\tag #'(voix1 basse) {
  Al -- lons, al -- lons, ac -- cou -- rez tous,
  al -- lons, al -- lons, ac -- cou -- rez tous,
  Cy -- bè -- le va des -- cen -- dre.
  Al -- lons, al -- lons, ac -- cou -- rez tous,
  al -- lons, al -- lons, ac -- cou -- rez tous,
  Cy -- bè -- le va des -- cen -- dre.
}
\tag #'voix2 {
  Al -- lons, al -- lons, ac -- cou -- rez tous,
  ac -- cou -- rez tous,
  Cy -- bè -- le va des -- cen -- dre.
  Al -- lons, al -- lons, ac -- cou -- rez tous,
  ac -- cou -- rez tous,
  Cy -- bè -- le va des -- cen -- dre.
}

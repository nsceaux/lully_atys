\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Sangaride, et Doris
    \livretVerse#8 { Allons, allons, accourez tous, }
    \livretVerse#6 { Cybèle va descendre. }
    \livretPers Sangaride
    \livretVerse#8 { Que dans nos concerts les plus doux, }
    \livretVerse#8 { Son nom sacré se fasse entendre. }
    \livretPers Atys
    \livretVerse#12 { Sur l’univers entier son pouvoir doit s’étendre. }
    \livretPers Sangaride
    \livretVerse#12 { Les Dieux suivent ses lois et craignent son couroux. }
    \livretPers Atys, Sangaride, Idas, Doris
    \livretVerse#12 { Quels honneurs ! quels respects ne doit-on point lui rendre ? }
    \livretVerse#8 { Allons, allons, accourez tous, }
    \livretVerse#6 { Cybèle va descendre. }
    \livretPers Sangaride
    \livretVerse#12 { Écoutons les oiseaux de ces bois d’alentour, }
    \livretVerse#12 { Ils remplissent leurs chants d’une douceur nouvelle. }
    \livretVerse#8 { On dirait que dans ce beau jour, }
    \livretVerse#8 { Ils ne parlent que de Cybèle. }
    \livretPers Atys
    \livretVerse#12 { Si vous les écoutez, ils parleront d’amour. }
    \livretVerse#5 { Un roi redoutable, }
    \livretVerse#5 { Amoureux, aimable, }
    \livretVerse#7 { Va devenir votre époux ; }
    \livretVerse#7 { Tout parle d’amour pour vous. }
    \livretPers Sangaride
    \livretVerse#12 { Il est vrai, je triomphe, et j’aime ma victoire. }
    \livretVerse#12 { Quand l’Amour fait régner, est-il un plus grand bien ? }
  }
  \column {
    \livretVerse#8 { Pour vous, Atys, vous n’aimez rien, }
    \livretVerse#6 { Et vous en faites gloire. }
    \livretPers Atys
    \livretVerse#8 { L’amour fait trop verser de pleurs ; }
    \livretVerse#8 { Souvent ses douceurs sont mortelles. }
    \livretVerse#8 { Il ne faut regarder les belles }
    \livretVerse#8 { Que comme on voit d’aimables fleurs. }
    \livretVerse#7 { J’aime les roses nouvelles, }
    \livretVerse#7 { J’aime les voir s’embellir, }
    \livretVerse#7 { Sans leurs épines cruelles, }
    \livretVerse#7 { J’aimerais à les cueillir. }
    \livretPers Sangaride
    \livretVerse#8 { Quand le péril est agréable, }
    \livretVerse#8 { Le moyen de s’en alarmer ? }
    \livretVerse#8 { Est-ce un grand mal de trop aimer }
    \livretVerse#6 { Ce que l’on trouve aimable ? }
    \livretVerse#12 { Peut-on être insensible aux plus charmants appas ? }
    \livretPers Atys
    \livretVerse#8 { Non vous ne me connaissez pas. }
    \livretVerse#12 { Je me défends d’aimer autant qu’il m’est possible ; }
    \livretVerse#8 { Si j’aimais, un jour, par malheur, }
    \livretVerse#6 { Je connais bien mon cœur }
    \livretVerse#6 { Il serait trop sensible. }
    \livretVerse#12 { Mais il faut que chacun s’assemble près de vous, }
    \livretVerse#8 { Cybèle pourrait nous surprendre. }
    \livretPers Idas, Atys
    \livretVerse#8 { Allons, allons, accourez tous, }
    \livretVerse#6 { Cybèle va descendre. }
  }
}#}))

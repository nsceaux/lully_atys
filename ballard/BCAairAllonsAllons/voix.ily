<<
  %% Sangaride
  \tag #'(voix1 basse) {
    \sangarideMark r4 r8 si' si'4.\trill do''8 |
    re''4 mi''8[ re''] do''[ si'] la'[ sol'] |
    fad'4.\trill re''8 re''4. re''8 |
    mi''[ re''] do''[ si'] si'4( la'\trill) |
    sol'4. re''8 %{ re''4 re'' %} re''4. re''8 |
    mi''4 fa''8[ mi''] re''[ do''] si'[ la'] |
    sold'4.\trill mi''8 mi''4. mi''8 |
    fa''8[ mi''] re''[ do''] do''4( si')\trill |
    la'4. do''8 si'4.\trill do''8 |
    re''4 mi''8[ re''] do''[ si'] la'[ sol'] |
    fad'4.\trill re''8 re''4. re''8 |
    mi''[ re''] do''[ si'] si'4( la')\trill |
    sol'1. %{ sol'1 r2 %} |
  }
  %% Doris
  \tag #'voix2 {
    \dorisMark r4 r8 sol' sol'4. la'8 |
    si'4 do''8[ re''] mi''[ re''] do''[ si'] |
    la'4.\trill fad'8 sol'4 la'8[ si'] |
    do''[ si'] la'[ sol'] sol'4( fad') |
    sol'4. si'8 si'4. si'8 |
    do''4 re''8[ mi''] fa''[ mi''] re''[ do''] |
    si'4.\trill sold'8 la'4 si'8[ do''] |
    re''[ do''] si'[ la'] la'4( sold') |
    la'4. fad'8 sol'4. la'8 |
    si'4 do''8[ re''] mi''[ re''] do''[ si'] |
    la'4.\trill fad'8 sol'4 la'8[ si'] |
    do''[ si'] la'[ sol'] sol'4( fad') |
    sol'1. %{ sol'1 r2 %} |
  }
  \tag #'voix3 { \clef "vhaute-contre" R1*12 R1. }
  \tag #'voix4 { \clef "vbasse" R1*12 R1. }
>>
<<
  \tag #'(voix2 voix3 voix4) { R1.*14 r2 }
  \tag #'(voix1 basse) {
    %% Sangaride
    \tag #'voix1 <>^\markup\character Sangaride
    r2 re''4 re'' do''\trill si' |
    do''2 la'2.\trill sol'4 |
    fad'2.\trill si'4 si'4. si'8 |
    dod''2. dod''4 red''2 |
    mi'' mi''( red'') |
    mi''2.
    %% Atys
    \atysMark sold4 sold4. sold8 |
    la1 si2 |
    do'1 do'4 re' |
    mi'2 fa'2. mi'4 |
    mi'2( re'1)\trill |
    do'1
    %% Sangaride
    \sangarideMark mi''2 |\noBreak
    la'\trill la' si'4 do'' |
    si'2.\trill mi''4 dod''2\trill |
    re'' re'' dod'' %{ re''2. dod''4 %} |
    \tag#'voix1 <>^\markup\character Sangaride re''2
  }
>>
<<
  \tag #'(voix1 basse) {
    %% Sangaride
    la'4. si'8 |
    do''2 do''4. do''8 |
    do''4 do'' %{ do''4. do''8 %} re''4. la'8 |
    si'4 do'' do''( si')\trill |
    do''2 sol'4. la'8 |
    si'2 si'4. do''8 |
    la'4. la'8 la'4.\trill la'8 |
    sol'4 do'' la'2\trill |
    sol'1 |
    r4 si' si'4. si'8 |
    do''4 mi''8[ fa''] sol''[ fa''] mi''[ re''] |
    do''4. la'8 la'4. la'8 |
    re''4 fa''8[ mi''] re''[ do''] si'[ la'] |
    sold'4.\trill mi''8 mi''4. mi''8 |
    fa''8[ mi''] re''[ do''] do''4( si')\trill |
    la'4. dod''8 dod''4. dod''8 |
    re''2 r8 re'' re'' do'' |
    si'4.\trill re''8 re''4. re''8 |
    mi''4 mi''8[ re''] do''[ si'] la'[ sol'] |
    fad'4.\trill re''8 re''4. re''8 |
    mi''[ re''] do''[ si'] si'4( la')\trill |
    sol'2
  }
  \tag #'voix2 {
    fad'4.^\markup\character "Doris" sol'8 |
    la'2 la'4. la'8 |
    la'4 la' la'4. la'8 |
    sol'4 sol' sol'2 |
    mi' mi'4. fad'8 |
    sol'2 sol'4. la'8 |
    fad'4.\trill fad'8 fad'4. fad'8 |
    sol'4. sol'8 sol'4( fad') |
    sol'1 |
    r4 sol' sol'4. sol'8 |
    sol'4 do'' do'' sol' |
    la'4. fa'8 fa'4. %{ la'8 %} fa'8 |
    la'4 re''8[ do''] si'4. si'8 |
    si'4. si'8 do''4. do''8 |
    do''4 si'8[ la'] la'4( sold') |
    la'4. mi'8 mi'4. mi'8 |
    fad'4 fad' la'4. la'8 | % fad'4 fad'8[ sol'] la'[ sol'] la'[ fad'] |
    sol'4. si'8 si'4. si'8 |
    do''4 sol' la'4. la'8 |
    la'4. la'8 si'4. si'8 |
    si'4 la'8[ sol'] sol'4( fad') |
    sol'2
  }
  %% Atys
  \tag #'voix3 {
    re'4.^\markup\character "Atys" re'8 |
    mi'2 mi'4. mi'8 |
    fa'4 fa' fa'4. fa'8 |
    fa'?4 mi' re'2\trill |
    do' do'4. do'8 |
    re'2 re'4. re'8 |
    re'4. re'8 re'4. %{ do'8 %} re'8 |
    si4 mi' re'2 %{ re'4.( do'8) %} |
    si1 |
    r4 re' re'4. re'8 |
    mi'4 sol'8[ fa'] mi'[ re'] do'[ si] |
    la4. do'8 do'4. do'8 |
    fa'4. re'8 re'[ mi'] re'[ do'] |
    si4. sold8 la4. la8 |
    la4 fa' %{ mi'4.( re'8) %} mi'2 |
    dod'4. la8 la4. la8 |
    la4 la re'4. re'8 |
    re'4. sol'8 sol'4. fa'8 |
    mi'4 do' do'4. do'8 |
    re'4. fad'8 sol'4. sol'8 |
    sol'4 mi' re'2 %{ re'4.( do'8) %} |
    si2\trill
  }
  %% Idas
  \tag #'voix4 {
    <>^\markup\character Idas re4. re8 |
    la,2 la4. la8 |
    fa4. fa8 %{ fa4 fa %} re4. re8 |
    sol4 do sol,2 |
    do do4. do8 |
    sol,2 sol4. sol8 |
    re4. re8 re4. re8 | % re4 re re re
    mi4 do re2 |
    sol,1 |
    r4 sol sol4. sol8 |
    do4 do8[ si,] do[ re] mi[ do] |
    fa4. fa8 fa4. fa8 |
    re4 re8[ mi] fa[ mi] fa[ re] |
    mi4. mi8 do4. do8 |
    %{ re4. re8 %} re4 re mi2 |
    la,4. la8 la4. la8 |
    re4 re8[ mi] fad[ mi] fad[ re] |
    sol4. sol8 sol4. sol8 |
    do'4 do'8[ si] la[ sol] fad[ mi] |
    re4. re8 si,4.\trill si,8 |
    do4 do re2 |
    sol,
  }
>>
<<
  \tag #'(voix2 voix3 voix4) {
    r2 R1*31 R1. R2.*105 R1 R2. R1*2
  }
  \tag #'(voix1 basse) {
    %% Sangaride
    \tag #'voix1 <>^\markup\character Sangaride
    sol'4. sol'8 |
    re''2 la'4. si'8 |
    sol'2 do''4. do''8 |
    do''4.( si'8) si'4. do''8 |
    la'2.\trill la'8 si' |
    do''2 do''4. re''8 |
    mi''2 si'4\trill si'8 si' |
    do''4. re''8 do''4( si')\trill |
    la'2. do''8 mi'' |
    do''4.\trill do''8 do''4 do''8 si' |
    si'2 la'4.\trill si'8 |
    sol'4 sol'8 sol' sol'4. la'8 |
    fad'2\trill fad'4
    %% Atys
    \atysMark la4 |
    la4. la8 la4. si8 |
    sol2. sol4 |
    la4. si8 si4( la8.) sol16 | % la8)\trill sol
    sol2. sol4 |
    re'2. mi'8 fad' |
    sol'4 mi' r do'8 do' |
    do'2 r4 re' |
    si\trill si r8 si si dod' |
    re'2 dod'4. re'8 |
    re'2 r4 la |
    la2 la4 si |
    si2( la4.)\trill sol8 |
    sol4
    %% Sangaride
    \sangarideMark si'8 re'' si'4\trill si'8 sol' |
    do''4. do''8 re'' re'' mi'' fa'' |
    mi''4\trill mi'' r mi'8 fad'! |
    sol'4 la'8 si' do''4. mi''8 |
    do''4\trill do'' do'' si' |
    si'2\trill r8 si' si' re'' |
    sol'4 fa' fa'4. mi'8 |
    mi'2\trill do''4 do''8 do'' re''4. mi''8 |
    la'4\trill la'
    %% Atys
    \atysMark re'4 |
    re'2 mi'4 |
    do'2\trill si4 |
    la2\trill sol4 |
    fad2\trill re'4 |
    si\trill do'4. re'8 |
    mi'4 fad'4. sol'8 |
    red'4. dod'8( si4) |
    r4 mi' si |
    do'2 do'8 do' |
    do'2 re'4 |
    si2\trill la4 |
    mi' mi' fa' |
    dod'2. |
    re'4 re' dod' | % re'4 re'4. dod'8
    re'2. |
    si4.\trill la8 si4 |
    do'2 do'8 si |
    la2\trill la4 |
    re'4. do'8 si4 |
    do' si4.\trill la8 |
    la2. |
    mi'4 mi' si |
    do'4 do'4. re'8 |
    si2\trill si4 |
    r4 re'4. re'8 |
    re'4 do'4. do'8 |
    do'2. |
    si4 la4.\trill sol8 |
    sol2. |
    la4 la si |
    do' do'4. re'8 |
    si2\trill la4 |
    re'4. re'8 mi'[ re'] |
    do'[ si] la4.\trill sol8 |
    sol2 r4 |
    %% Sangaride
    \sangarideMark si'4 do''4. re''8 %{ do''4 re'' %} |
    do''2.\trill |
    la'4 %{ si' do'' %} si'4. do''8 |
    si'4.\trill la'8( sol'4) |
    r si' mi'' |
    dod''2 %{ re''8[ dod''] %} re''4 |
    re''4 re'' dod''\trill |
    re''2. |
    la'4 la' si' |
    do''2. |
    si'4 do''8[ si'] la'[ sol'] |
    fad'2.\trill |
    re''4. la'8 si'4 |
    do'' si'2 |
    si'4( la'2)\trill |
    sol'2. |
    la'4 la' si' |
    do''2. |
    si'4 do''8[ si'] la'[ sol'] |
    fad'2.\trill |
    re''4. la'8 si'4 |
    do'' si'2 |
    si'4( la'2)\trill |
    sol'2. |
    r4 si' sol' |
    la' la' fad' |
    sol'4. sol'8 la'4 | % sol'4 sol' la'
    si' do''( si'8) do'' |
    si'2\trill
    %% Atys
    \atysMark re'4~ |
    re' r la8 la |
    si la sol4(\trill fad8) sol |
    fad2.\trill |
    r4 r8 si si do' |
    re'2 fad4 |
    sol2 do'4 |
    la4.\trill la8 si do' |
    si4.\trill la8( sol4) |
    r4 r8 la la fad |
    sol2 mi4 |
    %{ la4. %} la4 r8 re'8 re' do' |
    si2\trill~ si8 do' |
    la2\trill re'4 |
    % sol4. sol8 la si | si4( la2)\trill |
    sol4. sol8 la4 | si si( la)\trill |
    sol2 si8 do' |
    re'2 fad4 |
    sol mi4. la8 |
    fad2.\trill |
    r4 si4. do'8 |
    la4\trill la si |
    sol sol4. do'8 |
    la4\trill si do' |
    si4.\trill la8( sol4) |
    r la4. re'8 |
    sol4 sol la |
    si( la2)\trill |
    sol2 mi'8. mi'16 |
    mi'2 re'4 |
    re'2 re'8 dod' |
    re'2. |
    r4 si4. do'8 |
    la4\trill la si |
    sol sol4. do'8 |
    la4\trill si do' |
    si4.\trill la8( sol4) |
    r4 la4. re'8 |
    sol4 sol la |
    si( la2)\trill |
    sol2. |
    r4 re'8. re'16 %{ re'8 re' %} sol4 sol8 sol |
    la8 sib do'8. do'16 do'8 sib16[ la] | % do'8 do' do'[ sib16] la
    %{ sib4. %} sib4 r8 re'8 mib' mib'16 mib' do'8\trill do'16 do' |
    la2\trill la4 r |
  }
>>
<<
  \tag #'(voix1 basse) {
    r4 r8 sol sib4. sol8 |
    re'4 mib'8[ re'] do'[ sib] do'[ la] |
    sib4. re'8 re'4. re'8 |
    sol'4 sol'8[ fa'] mib'[ re'] do'[ sib] |
    la4.\trill fa'8 fa'4. fa'8 |
    sol'[ fa'] mib'[ re'] re'4( do')\trill |
    sib4. fa'8 re'4.\trill sib8 |
    fa'4 sol'8[ fa'] mib'[ re'] mib'[ do'] |
    re'4. re'8 re'4. re'8 |
    mib'4 do'8[ re'] mib'[ re'] do'[ sib] |
    la4. re'8 sol'4. fa'8 |
    mib'[ re'] do'[ sib] sib4( la)\trill |
    sol1 |
  }
  \tag #'voix2 {
    \clef "vbasse" R1 |
    r4 sol^\markup\character "Idas" fad\trill re |
    sol sib8[ la] sol[ fa] mib[ re] |
    mib4 mib8[ re] do[ re] mib[ do] |
    fa4. fa8 re4. re8 |
    mib4 mib fa2 |
    sib,2 r | % sib,1
    r4 sib la\trill fa |
    sib4 sib8[ la] sol[ fa] mib[ re] |
    do4 mib8[ re] do[ sib,] la,[ sol,] |
    re4. re8 sib,4. sib,8 |
    do4 do re2 |
    sol,1 |
  }
>>
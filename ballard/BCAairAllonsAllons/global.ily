\key sol \major
\digitTime\time 2/2 \midiTempo#160 s1*12
\time 3/2 \midiTempo#320 s1. \bar "|." s1.*14
\time 2/2 \midiTempo#160 s1*8
\digitTime\time 2/2 s1*45
\time 3/2 s1.
\digitTime\time 3/4 s2.*105 \bar "|."
\time 4/4 \midiTempo#80
\key re \minor s1
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 2/2 \midiTempo#160 s1*14 \bar "|."

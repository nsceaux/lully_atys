<<
  \tag #'(flore basse) {
    \floreMark r4 sib' do'' |
    re'' do''\tr sib'8[ la'] |
    sib'4. sib'8 sib'4 | % sib'4 sib' sib' |
    la'4 sol'(\trill fa'8) sol' | % la' sol'8[\trill fa'] sol'4 |
    fad'2 r8 re'' |
    si'4. si'8 do'' re'' | % do''8. re''16 |
    mib''2 do''4 |
    la'4.\trill la'8 sib'4~ |
    sib'8 do'' re''4( do'')\trill |
    sib'2. |
    sib'4 re'' mib'' |
    do''\trill do'' re'' | % do''4. re''8 |
    si'2 si'8 do'' |
    re''[\melisma do'' re'' mib'' fa'' re''] |
    mib''4\melismaEnd mib''4. re''8 |
    mib''4( re''2)\trill |
    do''4 mib''4. mib''8 |
    mib''4 mib'' re'' | % mib''2 mib''8 re'' |
    do''2\trill la'8 la' |
    sib'[\melisma la' sib' do'' re'' mib''] |
    fa''4\melismaEnd fa''4. fa''8 |
    mib''2.\trill |
    re''4 re''4. re''8 |
    do''4\trill do'' sib' | % do''4\trill do''4. sib'8 |
    la'2.\trill |
    r4 sib'4. sib'8 |
    do''8[\melisma sib' do'' re'' mib'' do''] |
    re''[ do'' re'' mib'' fa'' re''] |
    mib''[ fa'' mib'' re'' do'' sib'] |
    la'4\trill\melismaEnd la' sib' |
    sib'4( la'2)\trill |
    sol'4 sib'4. sib'8 |
    sol'4\trill sol' do'' |
    la'2.\trill |
    r4 sib'4. sib'8 |
    do''[\melisma sib' do'' re'' mib'' do''] |
    re''[ do'' re'' mib'' fa'' re''] |
    mib''[ fa'' mib'' re'' do'' sib'] |
    la'4\trill\melismaEnd la' sib' |
    sib'( la'2)\trill |
    sol'4 r r |
    R2.*35 |
  }
  \tag #'temps {
    \tempsMark r4 sol la |
    sib fad4.\trill fad8 |
    sol4. sol8 sol4 | % sol4 sol sol |
    fa4 mib( re8) mib | % fa4 mib8[ re] mib4 |
    re2\trill r8 re |
    sol4. fa8 mib re | % mib8. re16 |
    do2\trill do4 |
    fa4. mib8 re4 |
    mib4 fa2 |
    sib,2. |
    sib,4 sib sol |
    lab lab fa |
    sol2 sol8 la |
    si[\melisma la si do' re' si] |
    do'4\melismaEnd do' fa |
    sol2. |
    do4 do'4. sib8 |
    la4\trill la sib |
    fa2 fa8 fa |
    sol[\melisma fa sol la sib sol] |
    la4\melismaEnd la sib |
    sib4( la2)\trill | % sib2( la4) |
    sib4 sib4. sib8 |
    fad4\trill fad sol |
    re2. |
    r4 sol4. sol8 |
    la[\melisma sol la sib do' la] |
    sib[ la sib do' re' sib] |
    do'[ sib do' re' mib' do'] |
    re'4\melismaEnd re' sol |
    re2. |
    sol4 sol re |
    mib mib do |
    re2. |
    r4 sol4. sol8 |
    la[\melisma sol la sib do' la] |
    sib[ la sib do' re' sib] |
    do'[ sib do' re' mib' do'] |
    re'4\melismaEnd re' sol |
    re2. |
    sol4 r r |
    R2.*35 |
  }
  \tag #'vdessus {
    \clef "vbas-dessus" R2.*40 |
    r4 re''4.^\markup\character "[Chœur des Heures]" mib''8 |
    fa''4 fa'' mib'' |
    re''2\trill r4 |
    r re''4. mi''8 |
    fa''4 fa''4. fa''8 |
    fa''2( mi''4)\trill | % fa''4( mi''2) |
    fa''4 fa''4. fa''8 |
    re''4\trill re''4. mi''8 |
    re''4( dod''2)\trill |
    re''4 la'4. la'8 |
    si'4 si'4.\trill si'8 |
    do''2 r4 |
    r do''4. do''8 |
    fa''8[\melisma sol'' fa'' mib'' re'' do''] |
    sib'4.\trill la'8[ sib' do''] |
    re''4\melismaEnd re''4. mib''8 |
    re''4( do''2)\trill |
    sib'4 re''4. re''8 |
    do''4\trill do'' sib' |
    la'2\trill r4 |
    r sib'4. sib'8 |
    do''[\melisma sib' do'' re'' mib'' do''] |
    re''[ do'' re'' mib'' fa'' re''] |
    mib''[ fa'' mib'' re'' do'' sib'] |
    la'4\trill\melismaEnd la' sib' |
    sib'4( la'2)\trill |
    sol'4 re''4. re''8 |
    mib''4 do''4.\tr sib'8 |
    la'2\trill r4 |
    r sib'4. sib'8 |
    do''8[\melisma sib' do'' re'' mib'' do''] |
    re''[ do'' re'' mib'' fa'' re''] |
    mib''[ fa'' mib'' re'' do'' sib'] |
    la'4\trill\melismaEnd la' sib' |
    sib'4( la'2)\trill |
    sol'2. |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre2" R2.*40 |
    r4 sol'4. sol'8 |
    fa'4 fa' sol' |
    fa'2 r4 |
    r fa'4. sol'8 |
    la'4 fa'4. fa'8 |
    sol'2. |
    do'4 la'4. la'8 |
    fa'4 fa'4. sol'8 |
    mi'2( fad'?8[ sol']) |
    fad'4 fad'4. fad'8 |
    sol'4 sol'4. sol'8 |
    sol'2 r4 |
    r fa'4. fa'8 |
    sib4 sib fa'8. fa'16 |
    sol'4 sol'4. sol'8 |
    fa'4 fa' sol' |
    fa'2. |
    re'4 fa'4. sol'8 |
    la'4 la' sol' |
    fad'2 r4 |
    r sol'4. sol'8 |
    sol'4 fa'4. fa'8 |
    fa'4 fa' sib'8. sib'16 |
    sol'4 la'4. la'8 |
    fad'4 fad' sol' |
    sol'2( fad'4) | % sol'4( fad'2) |
    sol'4 sol'4. sol'8 |
    sol'4 mib'4. mib'8 |
    re'2 r4 |
    r sol'4. sol'8 |
    sol'4 fa'4. fa'8 |
    fa'4 fa' sib'8. sib'16 |
    sol'4 la'4. la'8 |
    fad'4 fad' sol' |
    sol'4( fad'2) |
    sol'2. |
  }
  \tag #'vtaille {
    \clef "vtaille" R2.*40 |
    r4 sib4. sib8 |
    sib4 sib sib |
    sib2 r4 |
    r re'4. re'8 |
    do'4 do' re' |
    sib2.\trill |
    la4\trill re'4. re'8 |
    re'4 sib4. sib8 |
    la2.\trill |
    la4 re'4. re'8 |
    re'4 re'4. re'8 | % re'4 re' re' |
    mib'2 r4 |
    r la4. la8 |
    re'8[\melisma mib' re' do' sib la] |
    sol4\melismaEnd mib mib' |
    re' sib4. sib8 |
    sib2( la4)\trill | % sib4( la2) |
    sib4 sib4. sib8 |
    la4 re'4. re'8 |
    re'2 r4 |
    r4 re'4. re'8 |
    do'4 do'4. fa'8 |
    fa'4 re' re'8. fa'16 |
    mib'4 mib'4. mib'8 |
    re'4 re' re' |
    re'2( la4) |
    sib4 sib4. sib8 |
    sib4 la4. sol8 |
    fad2\trill r4 |
    r re'4. re'8 |
    do'4 do'4. fa'8 |
    fa'4 re' re'8. fa'16 | % fa'8. fa'16 |
    mib'4 mib'4. mib'8 |
    re'4 re' re' |
    re'2. | % re'2( la4) |
    si2. |
  }
  \tag #'vbasse {
    \clef "vbasse" R2.*40 |
    r4 sol4. sol8 |
    re4 re mib |
    sib,2 r4 |
    r sib4. sib8 |
    la4\trill la sib |
    sol2.\trill |
    fa4 re4. re8 |
    sib4 sib4. sol8 | % sib4 sib sol |
    la2. |
    re4 re4. re8 |
    sol4 sol4. sol8 | % sol4 sol sol |
    do2 r4 |
    r fa4. fa8 |
    re8[\melisma do re mib fa re] |
    mib[ re mib fa sol la] |
    sib4\melismaEnd sib mib |
    fa2. |
    sib,4 sib4. sib8 |
    fad4\trill fad sol |
    re2 r4 |
    r sol4. sol8 |
    la8[\melisma sol la sib do' la] |
    sib[ la sib do' re' sib] |
    do'[ sib do' re' mib' do'] |
    re'4\melismaEnd re' sol |
    re2. |
    %{ sol4 %} sol,4 sol4. sol8 |
    do4 do4.\trill do8 |
    re2 r4 |
    r sol4. sol8 |
    la[\melisma sol la sib do' la] |
    sib[ la sib do' re' sib] |
    do'[ sib do' re' mib' do'] |
    re'4\melismaEnd re' sol |
    re2. |
    sol, |
  }
>>


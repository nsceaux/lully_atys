\piecePartSpecs
#`((dessus #:score-template "score-voix")
   (haute-contre #:score-template "score-voix")
   (haute-contre-sol #:score-template "score-voix")
   (taille #:score-template "score-voix")
   (quinte #:score-template "score-voix")
   (basse #:score "score-basse")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \column {
\livretPers Le Temps et Flore
\livretVerse#12 { Les plaisirs à ses yeux ont beau se présenter, }
\livretVerse#12 { Si tôt qu’il voit Bellone, il quitte tout pour elle ; }
\livretVerse#6 { Rien ne peut l’arrêter }
\livretVerse#6 { Quand la gloire l’appelle. }
\livretDidasP\wordwrap {
  Le chœur des heures répète ces deux derniers vers.
}
} #}))

\tag #'(temps flore basse) {
  Les plai -- sirs à ses yeux ont beau se pré -- sen -- ter,
  si tôt qu’il voit Bel -- lone, il quit -- te tout pour el -- le ;
  -le ;
  \ru#4 {
    Rien ne peut l’ar -- rê -- ter
    quand la gloi -- re l’ap -- pel -- le.
  }
}
\tag #'vdessus {
  Rien ne peut l’ar -- rê -- ter
  quand la gloi -- re l’ap -- pel -- le,
  quand la gloi -- re l’ap -- pel -- le.
  Rien ne peut l’ar -- rê -- ter
  quand la gloi -- re l’ap -- pel -- le.
  Rien ne peut l’ar -- rê -- ter
  quand la gloi -- re l’ap -- pel -- le.
  Rien ne peut l’ar -- rê -- ter
  quand la gloi -- re l’ap -- pel -- le.
}
\tag #'vhaute-contre {
  Rien ne peut l’ar -- rê -- ter
  quand la gloi -- re l’ap -- pel -- le,
  quand la gloi -- re l’ap -- pel -- le.
  Rien ne peut l’ar -- rê -- ter,
  quand la gloi -- re, quand la gloi -- re, la gloi -- re l’ap -- pel -- le.
  Rien ne peut l’ar -- rê -- ter
  quand la gloi -- re l’ap -- pel -- le,
  quand la gloi -- re, la gloi -- re l’ap -- pel -- le.
  Rien ne peut l’ar -- rê -- ter
  quand la gloi -- re l’ap -- pel -- le,
  quand la gloi -- re, la gloi -- re l’ap -- pel -- le.
}
\tag #'vtaille {
  Rien ne peut l’ar -- rê -- ter
  quand la gloi -- re l’ap -- pel -- le,
  quand la gloi -- re l’ap -- pel -- le.
  Rien ne peut l’ar -- rê -- ter,
  quand la gloi -- re, la gloi -- re l’ap -- pel -- le.
  Rien ne peut l’ar -- rê -- ter
  quand la gloi -- re l’ap -- pel -- le,
  quand la gloi -- re, la gloi -- re l’ap -- pel -- le.
  Rien ne peut l’ar -- rê -- ter
  quand la gloi -- re l’ap -- pel -- le,
  quand la gloi -- re, la gloi -- re l’ap -- pel -- le.
}
\tag #'vbasse {
  Rien ne peut l’ar -- rê -- ter
  quand la gloi -- re l’ap -- pel -- le,
  quand la gloi -- re l’ap -- pel -- le.
  Rien ne peut l’ar -- rê -- ter
  quand la gloi -- re l’ap -- pel -- le.
  Rien ne peut l’ar -- rê -- ter
  quand la gloi -- re l’ap -- pel -- le.
  Rien ne peut l’ar -- rê -- ter
  quand la gloi -- re l’ap -- pel -- le.
}

\score {
  \new ChoirStaff <<
    \new Staff \with { \haraKiri \tinyStaff } \withTinyLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse \includeLyrics "paroles"
    \new Staff \with { \haraKiriFirst } <<
      \global \keepWithTag #'basse \includeNotes "basse"
    >>
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { indent = \noindent }
}
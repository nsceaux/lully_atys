\clef "basse"
<<
  \tag #'basse { R2.*40 | r4 }
  \tag #'(basse-continue tous) {
    sol,4\repeatTie sol la |
    sib fad2\trill |
    sol2 sol4 |
    fa mib2\trill |
    re re4 |
    sol4. fa8 mib re |
    do2 do4 |
    fa4. mib8 re4 |
    mib fa4 fa, |
    sib,4. do8 sib, la, |
    sib,4 sib sol |
    %lab lab fa |
    lab2 fa4 |
    sol2 sol8 la |
    si8 la si do' re' si |
    do'2 fa4 |
    sol sol,2 |
    do4 do'4. sib8 |
    la2\trill sib4 |
    fa2 fa4 |
    sol8 fa sol la sib sol |
    la2 sib4 |
    %sib2 la4 |
    sib la2\trill |
    %sib2. |
    sib2 sib4 |
    fad2\trill sol4 |
    re2 re,4 |
    sol, sol4. sol8 |
    la sol la sib do' la |
    sib la sib do' re' sib |
    do' sib do' re' mib' do' |
    re'4 re' sol |
    re re,2 |
    sol,4 sol re |
    mib2 do4 |
    re2 re,4 |
    sol, sol4. sol8 |
    la8 sol la sib do' la |
    sib la sib do' re' sib |
    do' sib do' re' mib' do' |
    re'4 re' sol |
    re2 re,4 |
    sol,4 
  }
>> sol4. sol8 |
\twoVoices #'(basse basse-continue tous) <<
  { re4 re mib | }
  { re2 mib4 | }
>>
sib,2 r4 |
r sib4. sib8 |
\twoVoices #'(basse basse-continue tous) <<
  { la4 la sib | }
  { la2\trill sib4 | }
>>
sol2.\trill |
fa4 re4. re8 |
sib4 sib4. sol8 | % sib4 sib sol |
\twoVoices #'(basse basse-continue tous) <<
  { la2. |
    re4 re4. re8 |
    sol4 sol4. sol8 |
    do2 r4 |
    r fa4. fa8 | }
  { la4 la,2 |
    re2 re4 |
    sol4 sol sol, |
    do2. |
    fa2 fa4 | }
>>
re8\trill do re mib fa re |
mib re mib fa sol la |
\twoVoices #'(basse basse-continue tous) <<
  { sib4 sib mib | }
  { sib2 mib4 | }
>>
fa fa,2 |
sib,4 sib4. sib8 |
\twoVoices #'(basse basse-continue tous) <<
  { fad4 fad sol | re2 }
  { fad2\trill sol4 | re2 }
>> <<
  \tag #'basse { r4 | r4 }
  \tag #'(basse-continue tous) {
    \tag #'tous <>^"B.C."
    re,4 | sol,4
    \tag #'tous <>^"Tous"
  }
>> sol4. sol8 |
la sol la sib do' la |
sib la sib do' re' sib |
do' sib do' re' mib' do' |
re'4 re' sol |
re4 re,2 | % re2. |
sol,4 sol4. sol8 |
do4 do4. do8 |
re2 <<
  \tag #'basse { r4 | r4 }
  \tag #'(basse-continue tous) {
    \tag #'tous <>^"B.C."
    re,4 | sol,4
    \tag #'tous <>^"Tous"
  }
>> sol4. sol8 |
la sol la sib do' la |
sib la sib do' re' sib |
do' sib do' re' mib' do' |
\twoVoices #'(basse basse-continue tous) <<
  { re'4 re' sol | }
  { re'2 sol4 | }
>>
re4 re,2 | % re2. |
sol,2. |


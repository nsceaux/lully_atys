Re -- ti -- rez- vous, ces -- sez de pré -- ve -- nir le Temps ;
ne me dé -- ro -- bez point de pré -- ci -- eux ins -- tants :
la puis -- san -- te Cy -- bè -- le
pour ho -- no -- rer A -- tys qu’elle a pri -- vé du jour,
veut que je re -- nou -- vel -- le
dans une il -- lus -- tre cour
le sou -- ve -- nir de son a -- mour.
Que l’a -- gré -- ment rus -- ti -- que
de Flore et de ses jeux,
cède à l’ap -- pa -- reil ma -- gni -- fi -- que
de la mu -- se tra -- gi -- que,
et de ses spec -- ta -- cles pom -- peux.

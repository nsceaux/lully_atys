\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPersDidas Melpomène parlant à Flore
    \livretVerse#12 { Retirez-vous, cessez de prévenir le Temps ; }
    \livretVerse#12 { Ne me dérobez point de précieux instants : }
    \livretVerse#6 { La puissante Cybèle }
    \livretVerse#12 { Pour honorer Atys qu’elle a privé du jour, }
    \livretVerse#6 { Veut que je renouvelle }
    \livretVerse#6 { Dans une illustre cour }
  }
  \column {
    \null
    \livretVerse#8 { Le souvenir de son amour. }
    \livretVerse#6 { Que l’agrément rustique }
    \livretVerse#6 { De Flore et de ses jeux, }
    \livretVerse#8 { Cède à l’appareil magnifique }
    \livretVerse#6 { De la muse tragique, }
    \livretVerse#8 { Et de ses spectacles pompeux. }
  }
} #}))

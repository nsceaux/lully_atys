\piecePartSpecs
#`((dessus #:score "score-dessus")
   (haute-contre #:score-template "score-voix")
   (haute-contre-sol #:score-template "score-voix")
   (taille #:score-template "score-voix")
   (quinte #:score-template "score-voix")
   (basse #:score "score-basse")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Atys
    \livretVerse#8 { Mais déjà de ce mont sacré }
    \livretVerse#8 { Le sommet paraît éclairé }
    \livretVerse#6 { D’une splendeur nouvelle. }
    \livretPersDidas Sangaride s’avançant vers la Montagne
    \livretVerse#12 { La Déesse descend, allons au devant d’elle. }
    \livretPers Atys et Sangaride
    \livretVerse#6 { Commençons, commençons }
    \livretVerse#12 { De célébrer ici sa fête solemnelle, }
    \livretVerse#6 { Commençons, commençons }
    \livretVerse#6 { Nos jeux et nos chansons. }
    \livretDidasP\wordwrap { Les chœurs répètent ces derniers vers. }
    \livretPers Atys et Sangaride
    \livretVerse#12 { Il est temps que chacun fasse éclater son zèle. }
    \livretVerse#8 { Venez, Reine des Dieux, venez, }
    \livretVerse#8 { Venez, favorable Cybèle. }
    \livretDidasP\wordwrap { Les chœurs répètent ces deux derniers vers. }
  }
  \column {
    \livretPers Atys
    \livretVerse#8 { Quittez votre cour immortelle, }
    \livretVerse#8 { Choisissez ces lieux fortunés }
    \livretVerse#8 { Pour votre demeure éternelle. }
    \livretPers Les Chœurs
    \livretVerse#8 { Venez, Reine des Dieux, venez, }
    \livretPers Sangaride
    \livretVerse#12 { La Terre sous vos pas va devenir plus belle }
    \livretVerse#12 { Que le séjour des Dieux que vous abandonnez. }
    \livretPers Les Chœurs
    \livretVerse#8 { Venez, favorable Cybèle. }
    \livretPers Atys et Sangaride
    \livretVerse#12 { Venez voir les autels qui vous sont destinés. }
    \livretPers Atys, Sangaride, Idas, Doris, et les chœurs
    \livretVerse#8 { Écoutez un peuple fidèle }
    \livretVerse#4 { Qui vous appelle, }
    \livretVerse#8 { Venez Reine des Dieux, venez, }
    \livretVerse#8 { Venez favorable Cybèle. }
  }
}#}))

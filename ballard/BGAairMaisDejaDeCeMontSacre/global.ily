\key do \major
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.*2
\time 4/4 s1*5
\digitTime\time 3/4 \midiTempo#160 s2.*71
\time 4/4 s1
\time 3/2 s1.
\digitTime\time 2/2 s1
\digitTime\time 3/4 s2.*100
\digitTime\time 2/2 s1*30
\digitTime\time 3/4 s2.*64 \bar "|."

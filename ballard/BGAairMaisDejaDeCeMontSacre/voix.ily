<<
  %% Dessus / Sangaride
  \tag #'(vdessus basse) {
    << { s1*2 s2.*2 s2 } \tag #'vdessus { \sangarideMark R1*2 | R2.*2 | r2 } >>
    \tag #'basse \sangarideMark r4 re''8 do'' |
    si'4 si'8 re'' sol'2 |
    r4 mi''8 re'' do''4 do''8 mi'' |
    la'4. re''8 si'4. mi''8 |
    do''8. do''16 %{ re''8. mi''16 %} re''8 mi'' mi''4( re'') |
    do'' %{ duo %} r8 sol' la'4 |
    si'4.\trill si'8 dod''4 |
    re''2. |
    do''4 do'' si' |
    la'4.( sold'8) la'4 |
    sold'2\trill si'4 |
    dod''4. dod''8 re''[ dod''] |
    re''4 re''( dod'') |
    re''4. re''8 do''4 |
    si'2\trill si'4 |
    do''4. do''8 si'4 | % sib'4
    la'4. si'8 do''4 |
    si'2\trill mi''4 |
    do''2 re''4 |
    do''4( si'4.\trill) do''8 |
    do''4. sol'8 la'4 |
    sib'4. sib'8 sib'4 | % sib'8[ do''] |
    la'2\trill re''4 |
    si'2\trill mi''4 |
    do''2 re''4 |
    do''4( si'4.\trill) do''8 |
    do''4 %{ choeur %}
    <<
      \tag #'basse { r4 r | R2.*49 | r4 }
      \tag #'vdessus { r8 do''8^\markup\character "[Chœur]" do''4 |
        si'4. si'8 dod''4 |
        re''2 re''4 |
        do''2 si'4 |
        la'4.( sold'8) la'4 |
        sold'2\trill la'4~ |
        la'8 si' sold'2\trill |
        la'2 r4 |
        R2.*2 |\allowPageTurn
        r4 r8 dod'' dod''4 |
        re''4. la'8 la'4 |
        si'2\trill si'4 |
        do''2 do''4 |
        la'2\trill si'4 |
        do'' la'2\trill |
        sol' r4 |
        R2.*2 |\allowPageTurn
        r4 r8 re'' re''4 |
        mi''4. re''8 mi''4 |
        fa''2 fa''4 |
        re''2 re''4 |
        mi'' dod''2\trill |
        re'' r4 |
        R2.*2 |\allowPageTurn
        r4 r8 la' la'4 |
        si'4.\trill si'8 si'4 |
        do''2 do''4 |
        re''2 re''4 |
        mi''2 mi''4 |
        fa'' mi''2 |
        re''\trill r4 |
        R2.*2 |\allowPageTurn
        r4 r8 si' si'4 |
        do''4. re''8 mi''4 |
        do''2 fa''4 |
        re''2\trill sol''4 |
        mi''2\trill mi''4 |
        re'' re''2\trill |
        do'' r4 |
        R2. |\allowPageTurn
        r4 r8 si' si'4 |
        do''4. re''8 mi''4 |
        do''2 fa''4 |
        re''2 sol''4 |
        mi''2\trill mi''4 |
        re'' re''2\trill |
        do''4
      } >>
    <>^\markup\character Sangaride do''8 do'' si'4 si'8 do'' |
    la'2\trill la'4 la'8 la' re''4. re''8 |
    si'2\trill si'4
    << \tag #'basse { s2. \sangarideMark } \tag #'vdessus { r4 | r4 r } >>
    r8 do'' |
    si'2.\trill |
    la'4 la' si' |
    do''2 re''4 |
    mi'' do'' re'' |
    si' si' do'' |
    do''2( si'4\trill) | % do''4( si'2)
    do''2
    <<
      \tag #'basse { r4 R2.*63 r4 r }
      \tag #'vdessus {
        mi''4-\tag #'vdessus ^\markup\character "Chœur" |
        do''2. |
        fa''4 fa'' fa'' |
        mi''2 do''4 |
        si'2\trill mi''4 |
        mi'' mi'' mi'' |
        do'' do'' re'' |
        do''( si'2\trill) |
        la' r4 |
        r4 r r8 fa'' |
        re''2. |
        do''4 do'' do'' |
        re''2 r8 re'' |
        mi''2 fa''4 |
        re'' re'' mi'' |
        dod'' dod'' re'' |
        re''2( dod''4\trill) |
        re''2 r4 |
        r4 r r8 si' |
        do''4 do'' do'' |
        re'' re'' mi'' |
        mi''( re''2)\trill |
        do'' r4 |
        R2.*4 |\allowPageTurn
        r4 r r8 re'' |
        mi''2. |
        re''4 re'' re'' |
        do''2 r8 re'' |
        si'2\trill r8 do'' |
        la'4 re'' re'' |
        re'' do'' do'' |
        do''2( si'4) |
        do''4 mi'' mi'' |
        la' si'4. la'8 |
        si'4( la'2)\trill |
        sol'2 r4 |
        r4 r r8 re'' |
        do''2 r4 |
        r4 r r8 do'' |
        do''2. |
        mi''4 mi'' do'' |
        la'2\trill re''4 |
        si'2.\trill |
        r4 r r8 mi'' |
        do''4 do'' re'' |
        si'\trill si' do'' |
        do''2( si'4\trill) |
        do''2 r4 |
        R2.*4 |\allowPageTurn
        r4 r r8 re'' |
        mi''2 r8 mi'' |
        re''2.\trill |
        do''4 do'' do'' |
        si'2\trill r8 mi'' |
        mi''2 r8 re'' |
        re''4 do'' do'' |
        re'' re'' mi'' |
        mi''( re''2\trill) |
        do''2 } >>
    << { s4 s2.*10 s2 } \tag #'vdessus { r4 | R2.*10 | r4 r } >>
    <<
      \tag #'basse { r4 R2.*3 r4 r \sangarideMark } 
      \tag #'vdessus {
        r8 mi''^\markup\character "[Chœur]" |
        re''2.\trill |
        mi''4 mi'' re'' |
        do''2\trill r8 do'' |
        si'2\trill <>^\markup\character Sangaride
      }
    >> re''4 |\noBreak
    sol'2 sol'4 |
    sol'( fa'\trill mi'8) fa' |
    mi'2\trill sol'4 |
    la'8 si' do''4. re''8 |
    si'2\trill si'4 |
    do'' re'' mi'' |
    fa''4.( mi''8) fa''4 |
    mi''4.\trill re''8 mi''[ re''] |
    do''[ si'] la'4.\trill sol'8 |
    sol'2
    <<
      \tag #'basse { r4 R2.*3 r4 r }
      \tag #'vdessus {
        r8 si'^\markup\character "[Chœur]" |
        do''4 do'' do'' |
        re'' re'' mi'' |
        mi''( re''2)\trill |
        do''2
      }
    >> <>^\markup\character [Sangaride] do''4. do''8 |
    si'2\trill si'4. si'8 |
    mi'2 mi'4. mi'8 |
    la'2 la'4 si' |
    sold'2\trill sold'4 sold' |
    la'2 fad'4 fad' |
    sol'2 si'4. si'8 |
    si'2 la'4. sol'8 |
    fad'2\trill
    <<\tag #'basse { r2 R1*21 R2.*64 }
      \tag #'vdessus {
        la'4.^\markup\character [Chœur] la'8 |
        sib'2. re''4 |
        re''2 re''4 re'' |
        re''2( do''\trill) |
        re''4. re''8 la'4. la'8 |
        si'4\trill si'8 si' si'[ do''] re''[ si'] |
        mi''2 mi''4 r |
        r2 re''4. re''8 |
        re''2. re''4 |
        re''2 do''4 do'' |
        do''2( si'\trill) |
        do''2. do''4 |
        re''2 re''4 re'' |
        do''1\trill |
        si'2\trill si'4. si'8 |
        la'2\trill re''4. re''8 |
        re''2 do''4. do''8 |
        do''4. do''8 si'4\trill si'8 do'' |
        la'4\trill la' la'4. la'8 |
        si'4 si' do'' do''8 do'' |
        do''4 do''8 do'' si'[ do''] re''[ si'] |
        mi''2 mi''4 r8 mi'' |
        do''2. |
        fa''4 fa'' fa'' |
        mi''2\trill do''4 |
        si'2\trill mi''4 |
        mi'' mi'' mi'' |
        do'' do'' re'' |
        do''( si'2)\trill |
        la'2 r4 |
        r4 r r8 fa'' |
        re''2.\trill |
        do''4 do'' do'' |
        re''2 r8 re'' |
        mi''2 fa''4 |
        re''4 re'' mi'' |
        dod'' dod'' re'' |
        re''2( dod''4\trill) |
        re''2 r4 |
        r4 r r8 si' |
        do''4 do'' do'' |
        re'' re'' mi'' |
        mi''( re''2)\trill |
        do''2 r4 |
        R2.*4 |
        r4 r r8 re'' |
        mi''2. |
        re''4 re'' re'' |
        do''2 r8 re'' |
        si'2\trill r8 do'' |
        la'4 re'' re'' |
        re'' do'' do'' |
        do''2( si'4) |
        do'' mi'' mi'' |
        la' si'4. la'8 |
        si'4( la'2\trill) |
        sol'2 r4 |
        r4 r r8 re'' |
        do''2. |
        r4 r r8 do'' |
        do''2. |
        mi''4 mi'' do'' |
        la'2 re''4 |
        si'2.\trill |
        r4 r r8 mi'' |
        do''4 do'' re'' |
        si' si' do'' |
        do''2( si'4\trill) |
        do''2 r4 |
        R2.*4 |
        r4 r r8 re'' |
        mi''2 r8 mi'' |
        re''2.\trill |
        do''4 do'' do'' |
        si'2\trill r8 mi'' |
        mi''2 r8 re'' |
        re''4 do'' do'' |
        re'' re'' mi'' |
        mi''4( re''2\trill) |
        do''2. | } >>
  }
  %% Haute-contre / Atys
  \tag #'(vhaute-contre basse) {
    \atysMark r4 r8 sol16 sol do'8 do'16 re' mi'8. fa'16 |
    re'4 r8 re'16 mi' fa'8. fa'16 fa'8 fa'16 mi' |
    mi'2 mi'8 mi'16 mi' |
    fad'8 sol' si4( la) |
    sol4 r4
    <<
      { s2 s1*4 s2.*71 | s1 | s1. | s2. }
      \tag #'vhaute-contre {
        r2 | R1*4 |
        r4 r8 mi'^\markup\character Atys mi'4 |
        re'4. re'8 mi'4 |
        fa'2. |
        mi'4 mi'4. mi'8 |
        mi'2 re'4 |
        mi'2 mi'4 |
        mi'4. mi'8 fa'[ mi'] |
        fa'4 fa'( mi') |
        re'2 r4 |
        r r8 sol' fa'4 |
        mi'2 mi'4 |
        fa'4. fa'8 mi'4 |
        re'2 sol'4 |
        mi'2 fa'4 |
        mi'( re'4.) do'8 |
        do'2 r4 |
        r4 r8 sol' sol'4 |
        do'2 fa'4 |
        re'2 sol'4 |
        mi'2 fa'4 |
        mi'( re'4.) do'8 |
        do'4 r8 %{ chœur %} mi'8 mi'4 |
        re'4. re'8 mi'4 |
        fa'2 fa'4 |
        mi'2 mi'4 |
        mi'2 re'4 |
        mi'2 mi'4 |
        fa' mi'2 |
        dod'2 r4 |
        R2.*2 |
        r4 r8 la' la'4 |
        fad'4. fad'8 fad'4 |
        sol'2 sol'4 |
        sol'2 la'4 |
        fad'2 sol'4~ |
        sol'8 la' fad'2 |
        sol' r4 |
        R2.*2 |
        r4 r8 sol' sol'4 |
        sol'4. sol'8 sol'4 |
        la'2 la'4 |
        fa'2 fa'4 |
        sol' mi'2 |
        fad' r4 |
        R2.*2 |
        r4 r8 fad' fad'4 |
        sol'4. sol'8 sol'4 |
        mi'2 la'4 |
        %{ sol'2 %} la'2 sol'4 |
        sol'2 sol'4 |
        la' sol'2 |
        sol' r4 |
        R2.*2 |
        r4 r8 re' re'4 |
        mi'4. fa'8 sol'4 |
        fa'2 fa'4 |
        sol'2 sol'4 |
        la'2 sol'4 |
        la' sol'2 |
        mi'2 r4 |
        R2. |
        r4 r8 re' re'4 |
        mi'4. fa'8 sol'4 |
        fa'2 fa'4 |
        sol'2 sol'4 |
        la'2 sol'4 |
        la' sol'2 |
        mi'4 %{%} <>^\markup\character Atys mi'8 mi' re'4 re'8 mi' |
        do'2 do'4 do'8 do' fa'4. fa'8 |
        re'2 re'4
      }
    >>
    \tag #'basse \atysMark
    r8 sol' |
    <<
      \tag #'basse { mi'2 s4 s2.*70 s2 \atysMark }
      \tag #'vhaute-contre {
        mi'2. |
        re'4 re' mi' |
        fa'2 r8 fa' |
        mi'2 fa'4 |
        sol' mi' fa' |
        re' re' mi' |
        mi'( re'2) |
        do'2 %{%} sol'4 |
        fa'2. |
        fa'4 re' re' |
        mi'2 la'4 |
        sold'2 si4 |
        do' do'8[ re'] mi'4 |
        fa'4 fa' fa' |
        mi'2. | % mi'2( re'4)
        dod'2 r4 |
        r4 r r8 la' |
        sol'2. |
        mi'4 mi' la' |
        sol'2 r8 sol' |
        sol'2 la'4 |
        fa' fa' sol' |
        %% 
        mi' mi' fa' |
        fa'( mi'2) |
        re' r4 |
        r4 r r8 sol' |
        sol'4 sol' la' |
        fa' fa' sol' |
        sol'4.( fa'8[ mi' fa']) |
        mi'2 r4 |
        R2.*4 |
        r4 r r8 sol' |
        sol'2. |
        sol'4 sol' sol' |
        sol'2 r8 fa' |
        fa'2 r8 mi' |
        mi'4 re'8[ mi'] fa'4 |
        sol'4 sol' sol' |
        fa'2. |
        mi'4 sol' sol' |
        fad' sol'4. sol'8 |
        sol'2( fad'4) |
        sol'2 r4 |
        r4 r r8 fa' |
        mi'2. |
        r4 r r8 sol' |
        la'2. |
        sol'4 sol' la' |
        fad'2 fad'4 |
        sol'2. |
        r4 r r8 sol' |
        la'4 fa' fa' |
        re' re' mi' |
        mi'( re'2) |
        do'2 r4 |
        R2.*4 |
        r4 r r8 sol' |
        sol'2 r8 la' |
        sol'2. |
        sol'4 sol' fad' |
        sol'2 r8 mi' |
        fa'2 r8 fa' |
        sol'4 sol' la' |
        fa' fa' sol' |
        sol'4.( fa'8[ mi' fa']) |
        mi'2 <>^\markup\character Atys 
      }
    >> r8 mi' |
    fa'4 re'4.\trill do'8 |
    si4 si do' |
    do'4( si2)\trill |
    la2 dod'8 dod' |
    re'2 re'4 |
    re'2 re'8 dod' |
    re'2 re'4 |
    si si do' |
    la si do' |
    do'2( si4) |
    do'2
    \tag #'vhaute-contre {
      r8 sol' |
      sol'2. |
      sol'4 sol' sol' |
      fad'2 r8 fad' |
      sol'2 r4 |
      R2.*9 |
      r4 r r8 sol' |
      sol'4 sol' fa' |
      fa' fa' sol' |
      sol'4.( fa'8[ mi' fa']) |
      mi'2 <>^\markup\character [Atys] mi'4. mi'8 |
      re'2\trill re'4. mi'8 |
      do'2 do'4. do'8 |
      do'2 do'4 re' |
      mi'2 si4 si |
      do'2 do'4. re'8 |
      si2\trill re'4. re'8 |
      re'2 do'4. si8 |
      la2 %{%} fad'4. fad'8 |
      sol'2. sol'4 |
      la'2 la'4 re' |
      sol'1 |
      fad'4. fad'8 fad'[ sol'] la'[ fad'] |
      sol'4 sol'8 sol' sol'4. sol'8 |
      sol'2 sol'4 r |
      R1 |
      r4 re' re'8[ mi'] fa'[ re'] |
      sol'4 sol'8 sol' sol'[ fa'] sol'[ mi'] |
      fa'4 fa' fa'4. fa'8 |
      mi'4 mi' mi' mi'8 fa' |
      re'4 re'8 re' re'[ mi'] fad'[ sol'] |
      la'1 |
      re'2 sol'4. sol'8 |
      sol'2 fa'4. fa'8 |
      mi'4 mi' mi' mi'8 la' |
      fad'4 fad' sol'4. sol'8 |
      sol'2 fa'4. fa'8 |
      fa'4. fa'8 mi'4 mi'8 fa' re'4 re'8 sol' sol'4. sol'8 |
      sol'2 sol'4 r8 sol' |
      fa'2. |
      fa'4 re' re' |
      mi'2 la'4 |
      sold'2 si4 |
      do' do'8[ re'] mi'4 |
      fa'4 fa' fa' |
      mi'2. | % mi'2( re'4)
      dod'2 r4 |
      r4 r r8 la' |
      sol'2. |
      mi'4 mi' la' |
      sol'2 r8 sol' |
      sol'2 la'4 |
      fa' fa' sol' |
      mi' mi' fa' |
      fa'( mi'2) |
      re' r4 |
      r4 r r8 sol' |
      sol'4 sol' la' |
      fa' fa' sol' |
      sol'4.( fa'8[ mi' fa']) |
      mi'2 r4 |
      R2.*4 |
      r4 r r8 sol' |
      sol'2. |
      sol'4 sol' sol' |
      sol'2 r8 fa' |
      fa'2 r8 mi' |
      mi'4 re'8[ mi'] fa'4 |
      sol' sol' sol' |
      fa'2. |
      mi'4 sol' sol' |
      fad' sol'4. sol'8 |
      sol'2( fad'4) |
      sol'2 r4 |
      r4 r r8 fa' |
      mi'2. |
      r4 r r8 sol' |
      la'2. |
      sol'4 sol' la' |
      fad'2 fad'4 |
      sol'2. |
      r4 r r8 sol' |
      la'4 fa' fa' |
      re' re' mi' |
      mi'( re'2) |
      do' r4 |
      R2.*4 |
      r4 r r8 sol' |
      sol'2 r8 la' |
      sol'2. |
      sol'4 sol' fad' |
      sol'2 r8 mi' |
      fa'2 r8 fa' |
      sol'4 sol' la' |
      fa' fa' sol' |
      sol'4.( fa'8[ mi' fa']) |
      mi'2. |
    }
  }
  \tag #'vtaille {
    \clef "vtaille" R1*2 R2.*2 R1*5 R2.*21 |
    r4 r8 sol sol4 |
    sol4. sol8 sol4 |
    la2 la4 |
    la2 si4 |
    do'2 re'4 |
    si2 do'4 |
    si4 si2 |
    la r4 |
    R2.*2 |
    r4 r8 mi' mi'4 |
    re'4. re'8 re'4 |
    re'2 re'4 |
    mi'2 mi'4 |
    re'2 re'4 |
    mi' re'2\trill |
    si\trill r4 |
    R2.*2 |
    r4 r8 si si4 |
    do'4. re'8 do'4 |
    do'2 do'4 |
    sib2 sib4 |
    sib la2\trill |
    la r4 |
    R2.*2 |
    r4 r8 re' re'4 |
    re'4. re'8 re'4 |
    do'2 fa'4 |
    re'2\trill re'4 |
    do'2\trill do'4 |
    do' do'2 |
    si\trill r4 |
    R2.*2 |
    r4 r8 sol sol4 |
    sol4. si8 do'4 |
    la2\trill re'4 |
    si2\trill mi'4 |
    do'2\trill do'4 |
    do' si2\trill |
    do' r4 |
    R2. |
    r4 r8 sol sol4 |
    sol4. si8 do'4 |%%90
    la2\trill re'4 |
    si2\trill mi'4 |
    do'2\trill do'4 |
    do' si2\trill |
    do'4 r r2 |
    R1. |
    R1 |
    R2.*7 |
    r4 r do'4 |
    la2.\trill |
    re'4 re' re' | % la4 la si
    do'2 mi'4 |
    mi'2 sold4 |
    la la la |
    la la la |
    la2( sold4)\trill | % la4( sold2)\trill |
    la2 r4 |
    r4 r r8 re' |
    si2.\trill |
    do'4 mi' mi' |
    re'2\trill r8 re' |
    do'2 do'4 |
    sib sib sib |
    la la la |
    % la2( sol4) | fad2\trill r4 |
    la2. | la2 r4 |
    r4 r r8 re' |
    mi'4 do' la |
    si si do' |
    do'2( si4)\trill | % do'( si2)\trill
    do'2 r4 |
    R2.*4 |
    r4 r r8 si |
    do'2. |
    re'4 re' re' |
    la2 r8 la |
    re'2 r8 do' |
    do'4 la la |
    sol sol do' |
    re'2. |
    sol4 do' do' |
    re' re'4. mi'8 |
    re'4.( do'8[ si do']) |
    si2\trill r4 |
    r4 r r8 la |
    la2 r4 |
    r4 r r8 do'8 |
    do'2. |
    do'4 do' do' |
    re'2 re'4 |
    re'2 r4 |%91
    r4 r r8 do' |
    do'4 la la |
    sol sol sol |
    sol2. | % sol2( fa4)
    mi2\trill r4 |
    R2.*4 |
    r4 r r8 si8 |
    do'2 r8 do' |
    re'2. |
    la8[ si] do'4 la |
    re'2 r8 si |
    la2\trill r8 la |
    sol4 do' la |
    si si do' |
    % si( la2)\trill | sol2 r4 |
    do'2( si4\trill) | do'2 r4 |
    R2.*10 |
    r4 r r8 do' |
    si2.\trill |
    do'4 do' re' |
    la2 r8 re' |
    re'2 r4 |
    R2.*9 |
    r4 r r8 re' |
    mi'4 mi' la |
    si si do' |
    do'2( si4)\trill | % do'( si2)\trill
    do'2 r |
    R1*7 |
    r2 re'4. re'8 |
    re'2. re'4 |
    do'2 la4 sib |
    sol1\trill |
    la4. re'8 re'4. re'8 |
    re'4 re'8 re' re'4. re'8 |
    do'2 do'4 r4 |
    r2 si4. si8 |
    la2.\trill la4 |
    si2 do'4. do'8 |
    re'1 |
    sol2 la |
    la sol4.\trill sol8 |
    sol2( fad)\trill |
    sol2 re'4. re'8 |
    re'2 la4. la8 |
    la4 la mi' mi'8 mi' |%92
    re'4 re' re'4. re'8 |
    re'2 re'4. re'8 |
    re'4 re' do' do'8 do' |
    re'4 re'8 re' re'4. re'8 |
    do'2\trill do'4 r8 do' |
    la2. |
    re'4 re' re' | % la4 la si
    do'2 mi'4 |
    mi'2 sold4 |
    la la la |
    la la la |
    la2( sold4)\trill | % la4( sold2)\trill
    la2 r4 |
    r4 r r8 re' |
    si2.\trill |
    do'4 mi' mi' |
    re'2 r8 re' |
    do'2 do'4 |
    sib sib sib |
    la la la |
    % la2( sol4) | fad2\trill r4 |
    la2. | la2 r4 |
    r4 r r8 re' |
    mi'4 do' la |
    si si do' |
    do'2( si4)\trill | % do'( si2)\trill
    do'2 r4 |
    R2.*4 |
    r4 r r8 si |
    do'2. |
    re'4 re' re' |
    la2 r8 la |
    re'2 r8 do' |
    do'4 la la |
    sol sol do' |
    re'2. |
    sol4 do' do' |
    re' re'4. mi'8 |
    re'4.( do'8[ si do']) |
    si2\trill r4 |
    r4 r r8 la |
    la2. |
    r4 r r8 do' |
    do'2. |
    do'4 do' do' |
    re'2 re'4 |%93
    re'2. |
    r4 r r8 do' |
    do'4 la la |
    sol sol sol |
    sol2. | % sol2( fa4)
    mi2\trill r4 |
    R2.*4 |
    r4 r r8 si |
    do'2 r8 do' |
    re'2. |
    la8[ si] do'4 la |
    re'2 r8 si |
    la2\trill r8 la |
    sol4 do' la |
    si si do' |
    do'2( si4)\trill | % do'( si2)\trill |
    do'2.
  }
  \tag #'vbasse {
    \clef "vbasse" R1*2 R2.*2 R1*5 R2.*21 |
    r4 r8 do do4 |
    sol4. fa8 mi4 |
    re2 re4 |
    la2 sol4 |
    fa4.( mi8) fa4 |
    mi2 do4 |
    re mi2 |
    la, r4 |
    R2.*2 |
    r4 r8 la la4 |
    re'4. re'8 do'4 |
    si2\trill sol4 |
    do'2 la4 |
    re'2 sol4 |
    do re2 |
    sol, r4 |
    R2.*2 |
    r4 r8 sol sol4 |
    do'4. sib8 do'4 |
    fa2 fa4 |
    sib2 sib4 |
    sol la2 |
    re r4 |
    R2.*2 |
    r4 r8 re re4 |
    sol4. sol8 sol4 |
    la2 la4 |
    si2\trill si4 |
    do'2 do'4 |
    fa do2 |
    sol2 r4 |
    R2.*2 |
    r4 r8 sol fa4 |
    mi4.\trill re8 do4 |
    fa2 re4 |
    sol2 mi4 |
    la2 mi4 |
    fa sol2 |
    do r4 |
    R2. |
    r4 r8 sol fa4 |
    mi4.\trill re8 do4 |
    fa2 re4 |
    sol2 mi4 |
    la2 mi4 fa sol2 |
    do4 r r2 |
    R1. |
    R1 |
    R2.*7 |
    r4 r do4 |
    fa2. |
    re4 re re |
    la2 la,4 |
    mi2 mi4 |
    do do do |
    fa fa re |
    mi2. |
    la,2 r4 |
    r4 r r8 re |
    sol2. |
    la4 la la |
    si2\trill r8 si |
    do'2 la4 |
    sib sib sol |
    la4 la re |
    la,2. |
    re2 r4 |
    r4 r r8 sol |
    mi4 mi fa |
    re\trill re do |
    sol( sol,2) |
    do2 r4 |
    R2.*4 |
    r4 r r8 sol |
    do'2. |
    sib4 sib sib |
    la2 r8 la |
    sol2 r8 sol |
    fa4 fa fa |
    mi\trill mi mi |
    re2.\trill |
    do4 do do |
    do si,4.\trill do8 |
    re2. |
    sol,2 r4 |
    r4 r r8 re |
    la2 r4 |
    r4 r r8 mi |
    fa2. |
    do'4 do' la |
    re'2 re4 |
    sol2 r4 |
    r4 r r8 mi |
    fa4 fa re |
    sol sol do |
    sol,2. |
    do2 r4 |
    R2.*4 |
    r4 r r8 sol |
    do'2 r8 do' |
    sib2. |
    la4 la la |
    sol2 r8 sol |
    fa2 r8 fa |
    mi4 mi fa |
    re\trill re do |
    sol4( sol,2) |
    do2 r4 |
    R2.*10 |
    r4 r r8 do |
    sol2. |
    do'4 do' si |
    la2\trill r8 sol | % la
    sol2 r4 |
    R2.*9 |
    r4 r r8 sol |
    mi4 mi fa |
    re\trill re do |
    sol4( sol,2) |
    do2 r |
    R1*7 |
    r2 re'4. re'8 |
    sib2. sib4 |
    fad2\trill fad4 sol |
    mib1\trill |
    re4. re8 re[ mi] fad[ re] |
    sol4 sol8 sol sol[ la] si[ sol] |
    do'2 do'4 r |
    r2 sol4. sol8 |
    fa2. fa4 |
    mi2 mi4. mi8 |
    re1\trill |
    do2. do4 |
    do2 si,4. si,8 |
    la,1\trill |
    sol,4. sol,8 sol,[ la,] si,[ do] |
    re4 re8 re re[ mi] fa[ re] |
    la4 la8 la la[ si] do'[ la] |
    re'2 re'4 r |
    r4 r8 re re[ mi] fa[ re] |
    sol2 sol4 r |
    r4 r8 sol sol[ la] si[ sol] |
    do'2 do'4 r8 do |
    fa2. |
    re4 re re |
    la2 la,4 |
    mi2 mi4 |
    do do do |
    fa fa re |
    mi2. |
    la,2 r4 |
    r4 r r8 re |
    sol2. |
    la4 la la |
    si2\trill r8 si |
    do'2 la4 |
    sib4 sib sol |
    la la re |
    la,2. |
    re2 r4 |
    r4 r r8 sol |
    mi4\trill mi fa |
    re\trill re do |
    sol( sol,2) |
    do r4 |
    R2.*4 |
    r4 r r8 sol |
    do'2. |
    sib4 sib sib |
    la2 r8 la |
    sol2 r8 sol |
    fa4 fa fa |
    mi mi mi |
    re2. |
    do4 do do |
    do4 si,4.\trill do8 |
    re2. |
    sol,2 r4 |
    r4 r r8 re |
    la2. |
    r4 r r8 mi |
    fa2. |
    do'4 do' la |
    re'2 re4 |
    sol2. |
    r4 r r8 mi |
    fa4 fa re |
    sol sol do |
    sol,2. |
    do2 r4 |
    R2.*4 |
    r4 r r8 sol |
    do'2 r8 do' |
    sib2. |
    la4 la la |
    sol2 r8 sol |
    fa2 r8 fa |
    mi4\trill mi fa |
    re\trill re do |
    sol4( sol,2) |
    do2. |
  }
>>

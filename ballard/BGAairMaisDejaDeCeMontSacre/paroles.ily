%% ACTE 1 SCENE 7
% Atys
\tag #'(vhaute-contre basse) {
  Mais dé -- jà de ce mont sa -- cré
  le som -- met pa -- raît é -- clai -- ré
  d’u -- ne splen -- deur nou -- vel -- le.
}
% Sangaride s'avançant vers la montagne.
\tag #'(vdessus basse) {
  La dé -- es -- se des -- cend,
  la dé -- es -- se des -- cend, al -- lons, al -- lons au de -- vant d’el -- le.
}
% Atys et Sangaride.
\tag #'(vdessus vhaute-contre basse) {
  Com -- men -- çons, com -- men -- çons
  de cé -- lé -- brer i -- ci sa fê -- te so -- lem -- nel -- le,
  com -- men -- çons nos jeux,
  \tag #'(vdessus basse) { com -- men -- çons, }
  com -- men -- çons nos jeux et nos chan -- sons,
  \tag #'(vdessus basse) { com -- men -- çons, }
  com -- men -- çons nos jeux, nos jeux et nos chan -- sons.
}
% Choeur
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Com -- men -- çons, com -- men -- çons nos jeux et nos chan -- sons, et nos chan -- sons.
  Com -- men -- çons, com -- men -- çons nos jeux, nos jeux et nos chan -- sons.
  Com -- men -- çons, com -- men -- çons nos jeux et nos chan -- sons.
  Com -- men -- çons, com -- men -- çons nos jeux, nos jeux et nos chan -- sons.
  Com -- men -- çons, com -- men -- çons nos jeux, nos jeux et nos chan -- sons.
  Com -- men -- çons, com -- men -- çons nos jeux, nos jeux et nos chan -- sons.
}
% Atys et Sangaride.
\tag #'(vdessus vhaute-contre basse) {
  Il est temps que cha -- cun fasse é -- cla -- ter son zè -- le.
  \tag #'(basse) { Ve -- nez, }
  Ve -- nez, rei -- ne des dieux,
  \tag #'vhaute-contre { ve -- nez, }
  ve -- nez, fa -- vo -- ra -- ble Cy -- bè -- le.
}
% Choeur
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Ve -- nez, rei -- ne des dieux,
  ve -- nez, ve -- nez, fa -- vo -- ra -- ble Cy -- bè -- le.
  Ve -- nez, rei -- ne des dieux,
  ve -- nez, ve -- nez, fa -- vo -- ra -- ble Cy -- bè -- le.
  Ve -- nez, fa -- vo -- ra -- ble Cy -- bè -- le.
  Ve -- nez, rei -- ne des dieux,
  ve -- nez, ve -- nez, fa -- vo -- ra -- ble Cy -- bè -- le,
  fa -- vo -- ra -- ble Cy -- bè -- le.
  Ve -- nez, ve -- nez, rei -- ne des dieux,
  ve -- nez, ve -- nez, fa -- vo -- ra -- ble Cy -- bè -- le.
  Ve -- nez, ve -- nez, rei -- ne des dieux,
  ve -- nez, ve -- nez, fa -- vo -- ra -- ble Cy -- bè -- le.
}
% Atys
\tag #'(vhaute-contre basse) {
  Quit -- tez vo -- tre cour im -- mor -- tel -- le,
  choi -- sis -- sez ces lieux for -- tu -- nés
  pour vo -- tre de -- meure é -- ter -- nel -- le.
}
% Choeur
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Ve -- nez, rei -- ne des dieux, ve -- nez.
}
% Sangaride
\tag #'(vdessus basse) {
  La ter -- re sous vos pas va de -- ve -- nir plus bel -- le
  que le sé -- jour des dieux que vous a -- ban -- don -- nez.
}
% Choeur
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Ve -- nez, fa -- vo -- ra -- ble Cy -- bè -- le.
}
% Atys et Sangaride
\tag #'(vdessus vhaute-contre basse) {
  Ve -- nez voir les au -- tels qui vous sont des -- ti -- nés.
  Ve -- nez voir les au -- tels qui vous sont des -- ti -- nés.
}
% Atys, Sangaride, Idas, Doris, et les choeurs.
\tag #'(vdessus) {
  É -- cou -- tez un peu -- ple fi -- dè -- le
  qui vous ap -- pel -- le, qui vous ap -- pel -- le,
  é -- cou -- tez un peu -- ple fi -- dè -- le,
  un peu -- ple fi -- dè -- le
  é -- cou -- tez, é -- cou -- tez,
  é -- cou -- tez un peu -- ple fi -- dè -- le,
  é -- cou -- tez un peu -- ple fi -- dè -- le
  qui vous ap -- pel -- le.
  Ve -- nez, rei -- ne des dieux, ve -- nez,
  ve -- nez, fa -- vo -- ra -- ble Cy -- bè -- le.
  Ve -- nez, rei -- ne des dieux, ve -- nez,
  ve -- nez, fa -- vo -- ra -- ble Cy -- bè -- le.
  Ve -- nez, fa -- vo -- ra -- ble Cy -- bè -- le.
}
\tag #'(vhaute-contre) {
  É -- cou -- tez un peu -- ple fi -- dè -- le
  qui vous ap -- pel -- le, qui vous ap -- pel -- le,
  qui vous ap -- pel -- le, qui vous ap -- pel -- le,
  é -- cou -- tez un peu -- ple fi -- dè -- le
  qui vous ap -- pel -- le,
  é -- cou -- tez, é -- cou -- tez,
  é -- cou -- tez un peu -- ple fi -- dè -- le,
  é -- cou -- tez un peu -- ple fi -- dè -- le
  qui vous ap -- pel -- le.
  Ve -- nez, rei -- ne des dieux, ve -- nez,
  ve -- nez, fa -- vo -- ra -- ble Cy -- bè -- le.
  Ve -- nez, rei -- ne des dieux, ve -- nez,
  ve -- nez, fa -- vo -- ra -- ble Cy -- bè -- le.
  Ve -- nez, fa -- vo -- ra -- ble Cy -- bè -- le.
}
\tag #'(vtaille) {
  É -- cou -- tez un peu -- ple fi -- dè -- le
  qui vous ap -- pel -- le, qui vous ap -- pel -- le,
  é -- cou -- tez un peu -- ple fi -- dè -- le,
  un peu -- ple fi -- dè -- le
  é -- cou -- tez, é -- cou -- tez,
  é -- cou -- tez un peu -- ple fi -- dè -- le,
  é -- cou -- tez un peu -- ple fi -- dè -- le
  qui vous ap -- pel -- le.
  Ve -- nez, rei -- ne des dieux, ve -- nez,
  ve -- nez, fa -- vo -- ra -- ble Cy -- bè -- le.
  Ve -- nez, rei -- ne des dieux, ve -- nez,
  ve -- nez, fa -- vo -- ra -- ble Cy -- bè -- le.
  Ve -- nez, fa -- vo -- ra -- ble Cy -- bè -- le.
}
\tag #'(vbasse) {
  É -- cou -- tez un peu -- ple fi -- dè -- le
  qui vous ap -- pel -- le, qui vous ap -- pel -- le,
  é -- cou -- tez un peu -- ple fi -- dè -- le,
  un Peu -- ple fi -- dè -- le
  qui vous ap -- pel -- le, qui vous ap -- pel -- le,
  qui vous ap -- pel -- le, qui vous ap -- pel -- le, qui vous ap -- pel -- le.
  Ve -- nez, rei -- ne des dieux, ve -- nez,
  ve -- nez, fa -- vo -- ra -- ble Cy -- bè -- le.
  Ve -- nez, rei -- ne des Dieux, ve -- nez,
  ve -- nez, fa -- vo -- ra -- ble Cy -- bè -- le.
  Ve -- nez, fa -- vo -- ra -- ble Cy -- bè -- le.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Ve -- nez, rei -- ne des dieux,
  ve -- nez, ve -- nez, fa -- vo -- ra -- ble Cy -- bè -- le,
  fa -- vo -- ra -- ble Cy -- bè -- le.
  Ve -- nez, ve -- nez, rei -- ne des dieux,
  ve -- nez, ve -- nez, fa -- vo -- ra -- ble Cy -- bè -- le.
  Ve -- nez, ve -- nez, rei -- ne des dieux,
  ve -- nez, ve -- nez, fa -- vo -- ra -- ble Cy -- bè -- le.
}

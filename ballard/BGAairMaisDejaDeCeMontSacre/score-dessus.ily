\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> { \set fontSize = -2 \keepWithTag #'basse \includeLyrics "paroles" }
    \new Staff <<
      { s1*2 s2.*2 s1*5 s2.*28
        <>^"Hautbois" s2.*43 \startHaraKiri }
      \global \keepWithTag #'hautbois \includeNotes "dessus"
    >>
    \new Staff \with { instrumentName = "Violons" } <<
      \global \keepWithTag #'violons \includeNotes "dessus"
    >>
  >>
  \layout { }
}

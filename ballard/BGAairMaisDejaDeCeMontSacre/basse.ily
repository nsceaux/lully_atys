\clef "basse"
<<
  \tag #'basse { R1*2 R2.*2 R1*5 R2.*21 }
  \tag #'(basse-continue tous) {
    \tag #'tous <>^"B.C."
    do1 |
    sol2 re | 
    la2. |
    re'8 sol re4 re, |
    sol,4 sol fad2\trill |
    sol4 sol8 fa mi4 mi8 re |
    do4 do8 re mi2\trill |
    fa8 mi re4 sol8 fa mi4 |
    la8. la16 %{ si8. do'16 %} si8 do' sol4 sol, |
    do2 do4 |
    sol4. fa8 mi4 |
    re2 re4 |
    la4 la sol |
    fa2.\trill |
    mi2 mi4 |
    la2 re4 |
    sol, la,2 |
    re4. mi8 fad4\trill |
    sol2. |
    la2 sol4 |
    fa2 do4 |
    sol2 mi4 |
    la2 fa4 |
    sol sol,2 |
    do do'4 |
    mi2. |
    fa2 re4 |
    sol2 mi4 |
    la2 fa4 |
    sol sol,2 |
  }
>> 
\twoVoices #'(basse basse-continue tous) <<
  { r4 r8 \tag #'tous <>^"Tous" do8 do4 | }
  { do2 do4 | }
>>
sol4. fa8 mi4 |
re2 re4 |
la2 sol4 |
\twoVoices #'(basse basse-continue tous) <<
  { fa4. mi8 fa4 | }
  { fa2\trill fa4 | }
>>
mi2 do4 |
re mi2 |
la,2. ~ |
la,~ |
la,~ |
<<
  \tag #'(basse tous) {
    \tag #'tous \voiceOne
    la,4. la8 la4 | re'4. re'8 do'4 |
    \tag #'tous \oneVoice
  }
  \tag #'basse-continue { la,2 la4 | re'2 do'4 | }
  \tag #'tous \new Voice { \voiceTwo la,2 la4 | re'2 do'4 | }
>>
si2\trill sol4 |
do'2 la4 |
re'2 sol4 |
do re2 |
sol,2.~ |
sol,~ |
sol,~ |
<<
  \tag #'(basse tous) {
    \tag #'tous \voiceOne
    sol,4. sol8 sol4 |
    \tag #'tous \oneVoice
  }
  \tag #'basse-continue { sol,2 sol4 | }
  \tag #'tous \new Voice { \voiceTwo sol,2 sol4 | }
>>
do'4. sib8 do'4 |
fa2 fa4 |
sib2 sib4 |
sol la2 |
\once\tieDashed re2.~ |
re~ |
re~ |
<<
  \tag #'(basse tous) {
    \tag #'tous \voiceOne
    re4. re8 re4 |
    sol4. sol8 sol4 |
    \tag #'tous \oneVoice
  }
  \tag #'basse-continue { re2 re4 | sol2 sol4 | }
  \tag #'tous \new Voice { \voiceTwo re2 re4 | sol2 sol4 | }
>>
la2 la4 |
si2\trill si4 |
do'2 do'4 |
fa do2 |
sol2.~ |
sol~ |
sol~ |
sol4. sol8 fa4 |
mi4.\trill re8 do4 |
fa2 re4 |
sol2 mi4 |
la2 mi4 |
fa sol2 |
do2.~ |
do |
sol4. sol8 fa4 |
mi4.\trill re8 do4 |
fa2 re4 |
sol2 mi4 |
la2 mi4 |
fa sol2 |
<<
  \tag #'basse { do4 r r2 | R1. | R1 | R2.*7 | r4 r }
  \tag #'(basse-continue tous) {
    do2 \tag #'tous <>^"B.C." sol4 sol8 mi |
    fa2. fa4 re2 |
    sol2. sol4 |
    do'4 do8 re mi fa |
    sol2 sol,4 |
    re4. mi8 fa re |
    la4 sol\trill fa |
    mi la fa |
    sol2 do4 |
    fa, sol,2 |
    do2 \tag #'tous <>^"Tous" \bar "" \allowPageTurn
  }
>> do4 |
fa2. |
\twoVoices #'(basse basse-continue tous) <<
  { re4 re re | }
  { re2 re4 | }
>>
la2 la,4 |
mi2 mi4 |
\twoVoices #'(basse basse-continue tous) <<
  { do4 do do | fa fa re | }
  { do2 do4 | fa2 re4 | }
>>
mi4 mi,2 |
la,2 la,4 |
\twoVoices #'(basse basse-continue tous) <<
  { re4 re4. re8 | sol2 sol4 | la4 la la | }
  { re2 re4 | sol2. | la2 la4 | }
>>
si2\trill si4 |
do'2 la4 |
\twoVoices #'(basse basse-continue tous) <<
  { sib4 sib sol | la la re | }
  { sib2 sol4 | la2 re4 | }
>>
la,2. |
\twoVoices #'(basse basse-continue tous) <<
  { re4 re4. re8 | sol2 r8 sol |
    mi4 mi fa | re re do | }
  { re2 re4 | sol2 sol4 |
    mi2\trill fa4 | re2\trill do4 | }
>>
sol4 sol,2 |
do do4 |
si,8\trill la, si, sol, la, si, |
do si, do re mi fa |
sol4 fa8 mi re do |
si,2.\trill |
do4 sol,4. sol8 |
do'2. |
sib4 sib sib |
\twoVoices #'(basse basse-continue tous) <<
  { la2 r8 la | sol2 r8 sol |
    fa4 fa fa | mi mi mi |
    re2. | do4 do do | }
  { la2 la4 | sol2. |
    fa | mi\trill |
    re2. | do2 do4 | }
>>
do4 si,4.\trill do8 |
re4 re,2 |
sol,4 sol sol |
re re8 mi fa re |
la4 la la |
mi\trill mi8 fa sol mi |
fa mi fa sol la si |
do'2 la4 |
re'2 re4 |
sol sol sol |
mi\trill mi8 fa sol mi |
fa4 fa re |
sol4 sol do |
sol,2. |
do2 do4 |
si,8\trill la, si, sol, la, si, |
do si, do re mi fa |
sol4 fa8 mi re do |
si,2.\trill |
do4 sol,4. sol8 |
do'2 do'4 |
sib2. |
\twoVoices #'(basse basse-continue tous) <<
  { la4 la la | sol2 r8 sol |
    fa2 r8 fa | mi4 mi fa |
    re re do | }
  { la2 la4 | sol2\trill sol4 |
    fa2 fa4 | mi2 fa4 |
    re2\trill do4 | }
>>
sol4 sol,2 |
do <<
  \tag #'basse { r4 | R2.*10 | }
  \tag #'(basse-continue tous) {
    \tag#'tous <>^"B.C." do4 |
    re2. |
    mi2 la,4 |
    mi,2. |
    la,4 la2 |
    sib2 sol4 |
    la la,2 |
    re2 re4 |
    sol2 mi4 |
    fa re\trill do |
    sol sol,2 |
  }
>>
\twoVoices #'(basse basse-continue tous) <<
  { r4 r \tag #'tous <>^"Tous" r8 do |
    sol2. |
    do'4 do' si |
    la2 r8 la | }
  { do2 do4 | % do r8 do
    sol2. |
    % do'4 do' si | la2 r8 la |
    do'2 si4 | la2. | }
>>
sol2 <<
  \tag #'basse { r4 | R2.*9 | }
  \tag #'(basse-continue tous) {
    \tag#'tous <>^"B.C." sol4 |
    mi2\trill mi4 |
    re2 sol4 |
    do2. |
    si,4 la,2\trill |
    sol, sol8 fa |
    mi4 re\trill do |
    si,2.\trill |
    do2 fad,4 |
    sol, re,2 |
  }
>>
\twoVoices #'(basse basse-continue tous) <<
  { r4 r \tag #'tous <>^"Tous" r8 sol |
    mi4 mi fa |
    re re do | }
  { sol,2 sol4 | mi2 fa4 | re2\trill do4 | }
>>
sol4 sol,2 |
<<
  \tag #'basse { do2 r | R1*7 | r2 }
  \tag #'(basse-continue tous) {
    do2. \tag #'tous <>^"B.C." do4 |
    sold,1\trill |
    la,~ |
    la,4 sol, fa,2\trill |
    mi, mi4 mi |
    la,2. la,4 |
    mi2 si, |
    do1\trill |
    re2 \tag #'tous <>^"Tous" \bar "" \allowPageTurn
  }
>> re'4. re'8 |
sib2. sib4 |
fad2\trill fad4 sol |
mib1\trill |
re4. re8 re mi fad re |
sol4. sol8 sol la si sol |
do'2 do8 re mi fa |
sol2 sol4. sol8 |
fa2. fa4 |
\twoVoices #'(basse basse-continue tous) <<
  { mi2 mi4. mi8 | }
  { mi2. mi4 | }
>>
re1 |
do2. do4 |
\twoVoices #'(basse basse-continue tous) <<
  { do2 si,4. si,8 | }
  { do2 si, | }
>>
la,1\trill |
\twoVoices #'(basse basse-continue tous) <<
  { sol,4. sol,8 }
  { sol,2 }
>> sol,8 la, si, do |
re2 re8 mi fa re |
\twoVoices #'(basse basse-continue tous) <<
  { la4. la8 }
  { la2 }
>> la8 si do' la |
\twoVoices #'(basse basse-continue tous) <<
  { re'2 re'4 r | r r8 re }
  { re'2 re | re2 }
>> re8 mi fa re |
\twoVoices #'(basse basse-continue tous) <<
  { sol2 sol4 r | r r8 sol }
  { sol2 sol, | sol }
>> sol8 la si sol |
\twoVoices #'(basse basse-continue tous) <<
  { do'2 do'4 r8 do | }
  { do'2 do'4 do | }
>>
fa2. |
\twoVoices #'(basse basse-continue tous) <<
  { re4 re re | }
  { re2 re4 | }
>>
la2 la,4 |
mi2 mi4 |
\twoVoices #'(basse basse-continue tous) <<
  { do4 do do | fa fa re | }
  { do2 do4 | fa2 re4 | }
>>
mi4 mi,2 |
la,2 la,4 |
\twoVoices #'(basse basse-continue tous) <<
  { re4 re4. re8 | sol2 sol4 | }
  { re2~ re8 re | sol2. | }
>>
la4 la la |
\twoVoices #'(basse basse-continue tous) <<
  { si2\trill si4 | }
  { si2\trill r8 si | }
>>
do'2 la4 |
sib sib sol |
la la re |
la,2. |
\twoVoices #'(basse basse-continue tous) <<
  { re4 re4. re8 | sol2 r8 sol | mi4 mi fa | re re do | }
  { re2 re4 | sol2 sol4 | mi2\trill fa4 | re2\trill do4 | }
>>
sol4 sol,2 |
do do4 |
si,8\trill la, si, sol, la, si, |
do si, do re mi fa |
sol4 fa8 mi re do |
si,2.\trill |
do4 sol,4. sol8 |
do'2. |
\twoVoices #'(basse basse-continue tous) <<
  { sib4 sib sib |
    la2 r8 la |
    sol2 r8 sol |
    fa4 fa fa |
    mi mi mi | }
  { sib2 sib4 |
    la2 la4 |
    sol2. |
    fa\trill |
    mi\trill | }
>>
re2.\trill |
\twoVoices #'(basse basse-continue tous) <<
  { do4 do do | }
  { do2 do4 | }
>>
do4 si,4.\trill do8 |
re4 re,2 |
sol,4\trill sol sol |
re re8 mi fa re |
la4 la la |
mi\trill mi8 fa sol mi |
fa mi fa sol la si |
do'2 la4 |
re'2 re4 |
sol4 sol sol |
mi\trill mi8 fa sol mi |
fa4 fa re |
sol sol do |
sol,2. |
do2 do4 |
si,8\trill la, si, sol, la, si, |
do si, do re mi fa |
sol4 fa8 mi re do |
si,2.\trill |
do4 sol,4. sol8 |
do'2 do'4 |
sib2.\trill |
\twoVoices #'(basse basse-continue tous) <<
  { la4 la la |
    sol2 r8 sol |
    fa2 r8 fa |
    mi4 mi fa |
    re re do | }
  { la2 la4 |
    sol2 sol4 |
    fa2\trill fa4 |
    mi2 fa4 |
    re2\trill do4 | }
>>
sol4 sol,2 |
do2. |

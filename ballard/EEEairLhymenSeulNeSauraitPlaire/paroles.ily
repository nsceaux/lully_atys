\tag #'couplet-1-1 {
  \modVersion\set stanza = "[1]"
  L’hy -- men seul ne sau -- rait plai -- re,
  il a beau flat -- ter nos vœux ;
}
\tag #'couplet-1-2 {
  l’a -- mour seul a droit de fai -- re
  les plus doux de tous les nœuds.
}
\tag #'couplet-2-1 {
  \override Lyrics . LyricText . font-shape = #'italic
  \set stanza = "[2]"
  [L’A -- mour trou -- ble tout le mon -- de,
  c’est la sour -- ce de nos pleurs ;
}
\tag #'couplet-2-2 {
  \override Lyrics . LyricText . font-shape = #'italic
  c’est un feu brû -- lant dans l’on -- de,
  c’est l’é -- cueil des plus grands cœurs :]
}
\tag #'refrain {
  Il est fier, il est re -- bel -- le,
  mais il char -- me tel qu’il est ;
  l’hy -- men vient quand on l’ap -- pel -- le,
  l’a -- mour vient quand il lui plaît.
}

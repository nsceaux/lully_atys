\baussenFigures {
  s2.*4 s4 <_+>2 s2. <6> s
  s2.*2 s4 <6>2 <_+>2\figExtOn <_+>4\figExtOff s2. <7 _+>2 <5>4 <6 5>4 <_+>2 s2.*2
  s2. <5/> s2.*2 <7>2 <5>4 <6 5>2. s
}

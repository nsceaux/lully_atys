<<
  \tag #'(vdessus basse) {
    \clef "vdessus" mi''4 mi''2 |
    do'' do''4 |
    fa'' fa''2 |
    re''\trill re''4 |
    sol'' mi''2\trill |
    fa'' mi''4 |
    fa'' re''2\trill |
    do''2. |
    re''4 re''2 |
    mi'' mi''4 |
    do'' re''2 |
    si'\trill si'4 |
    do'' do''2 |
    do'' si'4 |
    do'' la'2\trill |
    sol'2. |
    re''4 mi''2 |
    re''\trill mi''4 |
    fa'' mi''2 |
    re''\trill re''4 |
    fa'' fa''2 |
    fa'' mi''4 |
    fa'' re''2\tr |
    do''2. |
  }
  \tag #'vbas-dessus {
    \clef "vbas-dessus" do''4 do''2 |
    la'\trill la'4 |
    re'' re''2 |
    si'\trill si'4 |
    si' dod''2\trill |
    re'' do''!4 |
    re'' si'2\trill |
    do''2. |
    si'4 si'2\trill |
    do'' do''4 |
    la' si'2 |
    sold'\trill sold'4 |
    la' mi'2 |
    fad'\trill sol'4 |
    la' fad'2\trill |
    sol'2. |
    si'4 do''2 |
    si'\trill do''4 |
    re'' do''2 |
    si'\trill si'4 |
    la' la'2 |
    si'\trill do''4 |
    re'' si'2\trill |
    do''2. |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" do'4 do'2 |
    fa' fa'4 |
    re' re'2 |
    sol' sol'4 |
    mi' la'2 |
    re' la'4 |
    fa' sol'2 |
    do'2. |
    sol'4 sol'2 |
    do' do'4 |
    fa' re'2 |
    mi' mi'4 |
    la la2 |
    re' mi'4 |
    do' re'2 |
    sol2. |
    sol'4 do'2 |
    sol' do'4 |
    si do'2 |
    sol sol4 |
    re' re'2 |
    sol' la'4 |
    fa'4 sol'2 |
    do'2. |
  }
>>

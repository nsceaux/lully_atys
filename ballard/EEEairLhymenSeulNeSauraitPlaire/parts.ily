\piecePartSpecs
#`((basse #:tag-notes basse
          #:clef "alto"
          #:indent 0
          #:system-count 3
          #:score "score-basse")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretVerse#7 { L’hymen seul ne saurait plaire, }
    \livretVerse#7 { Il a beau flatter nos vœux ; }
    \livretVerse#7 { L’amour seul a droit de faire }
    \livretVerse#7 { Les plus doux de tous les nœuds. }
    \livretVerse#7 { Il est fier, il est rebelle, }
    \livretVerse#7 { Mais il charme tel qu’il est ; }
    \livretVerse#7 { L’hymen vient quand on l’appelle, }
    \livretVerse#7 { L’amour vient quand il lui plaît. }
  }
  \column {
    \livretVerse#7 { L’Amour trouble tout le monde, }
    \livretVerse#7 { C’est la source de nos pleurs ; }
    \livretVerse#7 { C’est un feu brûlant dans l’onde, }
    \livretVerse#7 { C’est l’écueil des plus grands cœurs : }
    \livretVerse#7 { Il est fier, il est rebelle, }
    \livretVerse#7 { Mais il charme tel qu’il est ; }
    \livretVerse#7 { L’hymen vient quand on l’appelle, }
    \livretVerse#7 { L’amour vient quand il lui plaît. }
  }
}#}))

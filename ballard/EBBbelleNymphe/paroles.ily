%% ACTE 4 SCENE 2
Bel -- le nym -- phe, l’hy -- men va sui -- vre mon en -- vi -- e,
l’a -- mour a -- vec moi vous con -- vi -- e
à ve -- nir vous pla -- cer sur un trône é -- cla -- tant,
j’ap -- proche a -- vec trans -- port du fa -- vo -- rable ins -- tant
d’où dé -- pend la dou -- ceur du res -- te de ma vi -- e :
mais mal -- gré les ap -- pas du bon -- heur qui m’at -- tend,
mal -- gré tous les trans -- ports de mon âme a -- mou -- reu -- se,
si je ne puis vous rendre heu -- reu -- se,
je ne se -- rai ja -- mais con -- tent.
Je fais mon bon -- heur de vous plai -- re,
j’at -- tache à vo -- tre cœur mes dé -- sirs les plus doux.

Sei -- gneur, j’o -- bé -- i -- rai, je dé -- pends de mon pè -- re,
et mon père au -- jour -- d’hui veut que je sois à vous.

Re -- gar -- dez mon a -- mour, plu -- tôt que ma cou -- ron -- ne.

Ce n’est point la gran -- deur qui me peut é -- blou -- ir.

Ne sau -- riez- vous m’ai -- mer sans que l’on vous l’or -- don -- ne.

Sei -- gneur, con -- ten -- tez- vous que je sache o -- bé -- ïr,
en l’é -- tat où je suis c’est ce que je puis di -- re…

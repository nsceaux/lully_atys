\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Célénus
    \livretVerse#12 { Belle nymphe, l’hymen va suivre mon envie, }
    \livretVerse#8 { L’Amour avec moi vous convie }
    \livretVerse#12 { À venir vous placer sur un trône éclatant, }
    \livretVerse#12 { J’approche avec transport du favorable instant }
    \livretVerse#12 { D’où dépend la douceur du reste de ma vie : }
    \livretVerse#12 { Mais malgré les appas du bonheur qui m’attend, }
    \livretVerse#12 { Malgré tous les transports de mon âme amoureuse, }
    \livretVerse#8 { Si je ne puis vous rendre heureuse, }
    \livretVerse#8 { Je ne serai jamais content. }
    \livretVerse#8 { Je fais mon bonheur de vous plaire, }
    \livretVerse#12 { J’attache à votre cœur mes désirs les plus doux. }
  }
  \column {
    \livretPers Sangaride
    \livretVerse#12 { Seigneur, j’obéirai, je dépends de mon père, }
    \livretVerse#12 { Et mon père aujourd’hui veut que je sois à vous. }
    \livretPers Célénus
    \livretVerse#12 { Regardez mon amour, plutôt que ma couronne. }
    \livretPers Sangaride
    \livretVerse#12 { Ce n’est point la grandeur qui me peut éblouir. }
    \livretPers Célénus
    \livretVerse#12 { Ne sauriez-vous m’aimer sans que l’on vous l’ordonne. }
    \livretPers Sangaride
    \livretVerse#12 { Seigneur, contentez-vous que je sache obéïr, }
    \livretVerse#12 { En l’état où je suis c’est ce que je puis dire… }
  }
}#}))

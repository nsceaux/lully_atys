\piecePartSpecs
#`((dessus)
   (haute-contre #:system-count 3)
   (haute-contre-sol #:system-count 3)
   (taille #:system-count 3)
   (quinte #:system-count 3)
   (basse #:score-template "score"
          #:system-count 3))

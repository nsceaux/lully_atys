\clef "haute-contre" do''2 sol' |
sol'2. sol'4 |
la'2. la'8 la' | % la'8. la'16
si'2 do''4. si'8 |
si'2. sol'4 |
sol'2 sol'4. sol'8 |
fa'2 do''4. re''8 |
mi''2. mi''4 |
la'2 sol'4. sol'8 |
sol'2 fad'4.( mi'16 fad') |
sol'1 |
sol'2 r8 si' si'16 la' si' dod'' | % do''
re''8 re'' re'' mi''16 re'' do''8 do'' la' sol' |
fa'4. fa'8 mi' sold' sold' sold' |
la'4. la'8 la' fad' fad' fad' |
sol'4 sol'8 sol' %{ sol' sol' %} sol'4 sol'8 sol' |
fa'2. la'4 |
la' re'' do''4. do''8 |
do''4. re''8 si'4. si'8 | % do''4. do''8 si'4. do''8 |
do''2 r8 si' si'16 la' si' dod'' | % do''
do''1 |

\clef "quinte" si4 si la |
sol mi2 |
la2 la4 |
si la si |
si mi2 |
la2. |
la4 la la |
la si2 |
la2 la4 |
la2 re'4~ |
re' la4. la8 |
la2. |
la4 la la |
la la2 |
sol2 sol4 |
sol2 la4 |
si8 do' re'4. re'8 |
re'2. |
sol4 sol sol | % do'4 do' mi'
re'4 re'2 | % la4 re'2
re'2 re'4 | % sol2 sol4
do'4 sol la | % sol2 la4
si8 do' re'4. re'8 |
re'2. |

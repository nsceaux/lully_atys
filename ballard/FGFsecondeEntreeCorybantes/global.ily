\key do \major
\digitTime \time 2/2 \midiTempo #132 s1*2
\digitTime\time 3/4 s2.*4
\digitTime \time 2/2 s1*2
\digitTime\time 3/4 s2.*3 \alternatives s2. { \digitTime\time 2/2 s1 }
s1*2
\digitTime\time 3/4 s2.*3
\digitTime \time 2/2 s1*3
\digitTime\time 3/4 s2.*2
\digitTime \time 2/2 s1*3
\digitTime\time 3/4 s2.*2
\digitTime \time 2/2 s1*4 \bar "|."

\clef "basse" sol,1 |
sol4. sol8 sol4 la |
sib2. sib4 |
si2. sol4 |
do'4 do sol fa |
mib2~ mib8 mib re\trill do |
re do sib, la, sol,2 |
re, re8 mib re do |
sib,2 sib |
% Manuscrit : la4. sib8 la4. sol8 
la4. sib8 la4 sol |
fad2.\trill fad4 |
sol2. sol4 |
la2 re |
sol, la, |
re,4 re8. mib16 re8. do16 sib,8. la,16 |
re,1 |
R1.*3 |
% Manuscrit : sol8 fad sol la sol la fad mi fad sol fad sol |
% Manuscrit : mi re mib fa mib fa re2. |
sol8 fa sol la sol la fa\trill mib fa sol fa sol |
mib\trill re mib fa mib fa re2.\trill |
re'8 do' re' mib' re' do' si\trill la si sol la si |
do' sib do' re' do' sib la\trill sol la fa sol la |
sib4 sib,8 do re mib fa4 fa,2 |
sib,4. sib8 la sol fad2.\trill |
sol la8 sol la sib la sol |
fa4 sol8 fa mi re dod2.\trill |
% Manuscrit : re8 do re mi re do sib, la, sib, do sib, do |
re8 do re mib re do sib, la, sib, do sib, do |
la,2.\trill la8 sol la sib la sol |
fad\trill mi fad re mi fad sol fa? sol la sol fa |
% Manuscrit : mi re mi do re mi fa mi fa sol fa mib |
mi\trill re mi do re mi fa mib fa sol fa mib |
re do re sib, do re mib re mib fa mib re |
do\trill sib, do la, sib, do re do re mib re do |
sib,4 do8 sib, la, sol, re4 re,2 |
sol, re4. do8 |
si,2 do4. do8 |
dod1 |
re2 sol, |
mib si,\trill |
do re4 re, |
sol,1 |

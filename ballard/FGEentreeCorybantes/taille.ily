\clef "taille" mi'8 fa' sol'4 sol' sol'2 sol'4 |
sol' mi'8 fa' sol' mi' re'4 si8 do' re' si |
mi'4 mi'8 fa' sol'4 do' re'2 |
re'4 mi'2 mi'4 mi'2 |
re'4. re'16 re' re'8 re' re'4 re'2 |
re'4 re'8 do' si4 si2. |
mi'8 re' mi' fa' sol' mi' fa'4 fa' fa' |
fa'2 fa'4 fa' mi'2 |
mi'4 dod'8 re' mi' dod' re'4 re' re' |
re' mi' mi' mi' la si |
si do' fa'8 mi' re' do' si4. do'8 |
do'4 do' do' do'2. |

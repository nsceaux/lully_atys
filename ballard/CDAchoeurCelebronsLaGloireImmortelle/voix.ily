<<
  \tag #'vdessus {
    \clef "vbas-dessus" si'8^\markup\character { Chœur des Nations } si' |
    do''2. do''4 |
    la'2\trill la'4 re'' |
    si'2\trill si' |
    si'4 si'8 si' si'4. si'8 |
    dod''2\trill re''4. re''8 |
    re''2 dod''4.\trill re''8 |
    re''2. re''8 re'' |
    re''2. re''4 |
    mi''2 mi''4 mi'' |
    do''2\trill do'' |
    re''4 re''8 re'' re''4. re''8 |
    si'2\trill do''4. do''8 |
    si'2\trill si'4( dod''8) re'' | % si'4 dod''8[ re''] |
    dod''2.\trill dod''4 |
    re''4 re'' re'' do''! |
    si'2.\trill si'4 |
    do''2 mi''4 re'' |
    do''2 do''4. do''8 |
    do''2. do''4 |
    si'2\trill si'4 do'' |
    si'2( la')\trill |
    sol'2 r |
    R1*2 |
    r2 r4 re'' |
    mi'' mi'' mi'' mi'' |
    mi''2. fad''4 |
    red''2\trill r |
    R1*2 |
    r2 red''4 red'' |
    mi''2 mi''4. mi''8 |
    fad''2. fad''4 |
    si'2 mi''4. mi''8 |
    mi''2( red''\trill) |
    mi''2 r |
    R1 |
    r2 r4 si' |
    do''4 do'' do'' re'' |
    do''2( si'4.)\trill la'8 |
    la'2 r |
    R1*2 |
    r2 dod''4 dod'' |
    re''2 re''4 re'' |
    mi''2. mi''4 |
    la'2 re''4. re''8 |
    re''2( dod''\trill) |
    re''2 r |
    R1*3 |
    r2 la'4 la' |
    si'2 si'4 si' |
    do''2. do''4 |
    si'2 dod''4 re'' |
    re''2( dod''\trill) |
    re'' r |
    R1*3 |
    r2 la'4 la' |
    si'2 si'4. si'8 |
    do''2. re''4 |
    si'2\trill si'4 do'' |
    si'2( la'\trill) |
    sol'1 |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" sol'8 sol' |
    sol'2. la'4 |
    fad'2 fad'4 fad' |
    sol'2 sol' |
    sol'4 sol'8 sol' sol'4. sol'8 |
    mi'2 fad'4. sol'8 |
    mi'2 %{ mi'4( fad'8) sol' %} mi'4. la'8 |
    fad'2. fad'8 fad' |
    sol'2. sol'4 |
    sol'2 sol'4 sol' |
    fa'2 fa' |
    fa'4 fa'8 fa' fa'4. fa'8 |
    mi'2 mi'4. mi'8 |
    fa'2 mi'4. mi'8 | % mi'4 mi'
    mi'2. mi'4 |
    fa'4 fa' fa' fa' |
    re'2. re'4 |
    mi'2 sol'4 sol' |
    la'2 mi'4. mi'8 |
    re'2. re'4 |
    re'2 re'4 mi' |
    re'1 | % re'2.( do'4)
    si2 r |
    R1*2 |
    r2 r4 sol' |
    sol' sol' sol' sol' |
    la'2. la'4 |
    fad'2 r |
    R1*2 |
    r2 fad'4 fad' |
    sol'2 %{ sol'4. sol'8 %} sol'4 sol' |
    fad'2. fad'4 |
    sol'2 sol'4. la'8 |
    sol'2( fad') |
    mi'2 r |
    R1 |
    r2 r4 mi' |
    mi' mi' mi' fa' |
    mi'2. %{ re'4 %} mi'4 |
    dod'2 r |
    R1*2 |
    r2 %{ mi'4 mi' %} mi'4. mi'8 |
    fa'2 fa'4 fa' |
    sol'2. la'4 |
    fa'2 fa'4 sol' |
    fa'2( mi') |
    re'2 r |
    R1*3 |
    r2 fad'4 fad' |
    sol'2 sol'4 sol' |
    la'2. la'4 |
    sol'2 mi'4 fad' |
    sol'1 |
    fad'2 r |
    R1*3 |
    r2 fad'4 fad' |
    sol'2 sol'4. sol'8 |
    la'2. la'4 |
    sol'2 sol'4 sol' |
    sol'2( fad') |
    sol'1 |
  }
  \tag #'vtaille {
    \clef "vtaille" re'8 re' |
    mi'2. mi'4 |
    re'2 re'4 re' |
    re'2 re' |
    mi'4 mi'8 mi' si4. si8 |
    la2\trill la4. la8 |
    si2 la4. la8 |
    la2. la8 la |
    si2. si4 |
    do'2 do'4 do' |
    la2\trill la |
    si4 si8 si si4. si8 |
    sold2\trill la4. la8 |
    la2 sold4.\trill la8 |
    la2. la4 |
    la4 la la la |
    sol2. sol4 |
    sol2 do'4. do'8 |
    do'2 la4. la8 |
    la2. la4 |
    sol2 sol4 sol |
    sol2( fad) |
    sol r |
    R1*2 |
    r2 r4 si |
    do' do' do' do' |
    do'2. do'4 |
    si2\trill r |
    R1*2 |
    r2 si4 si |
    si2 %{ si4. si8 %} si4 si |
    si2. si4 |
    si2 si4 do' |
    si1 | % si2.( la4)
    sold2\trill r |
    R1 |
    r2 r4 sold |
    la la la la |
    la2( sold4.\trill) la8 |
    la2 r |
    R1*2 |
    r2 %{ la4 la %} la4. la8 |
    la2 la4 la |
    la2. la4 |
    la2 la4 sib |
    la1 | % la2.( sol4)
    fad2 r |
    R1*3 |
    r2 re'4 re' |
    re'2 re'4 re' |
    re'2. re'4 |
    re'2 mi'4 re' |
    mi'1 |
    la2 r |
    R1*3 |
    r2 re'4 re' |
    re'2 %{ re'4. re'8 %} re'4 re' |
    re'2. re'4 |
    re'2 re'4 mi' |
    % re'2.( do'4) |
    re'1 |
    si1\trill |
  }
  \tag #'vbasse {
    \clef "vbasse" sol8 sol |
    do'2. la4 |
    re'2 re'4 re |
    sol2 sol |
    mi4 mi8 mi mi4. mi8 |
    la2 fad4. fad8 |
    sol2 la8[ sol] la4 |
    re2. re8 re |
    sol2. sol4 |
    mi2\trill mi4 mi |
    fa2 fa |
    re4 re8 re re4. re8 | % re4 re
    mi2 do4 do |
    re2 mi8[ re] mi4 |
    la,2. la4 |
    re4 re re re |
    sol2. sol4 |
    do2 do'4 si |
    la2 la4 sol |
    fad2. fad4 |
    sol2 sol4 do |
    re1 |
    sol,2 r |
    R1*2 |
    r2 r4 sol |
    do' do' do' si |
    la2. la4 |
    si2 r |
    R1*2 |
    r2 si4 la |
    sol2 fad4 mi |
    red2.\trill red4 |
    mi2 mi4 la, |
    si,1 |
    mi2 r |
    R1 |
    r2 r4 mi |
    la la la re |
    mi2. mi4 |
    la,2 r |
    R1*2 |
    r2 la4 sol |
    fa2 mi4 re |
    dod2.\trill dod4 |
    re2 re4 sol, |
    la,1 |
    re2 r |
    R1*3 |
    r2 re'4 do' |
    si2 la4 sol |
    fad2.\trill fad4 |
    sol2 sol4 fad |
    mi1\trill |
    re2 r |
    R1*3 |
    r2 re'4 do' |
    si2 la4 sol |
    fad2.\trill fad4 |
    sol2 sol4 do |
    re1 |
    sol, |
  }
>>

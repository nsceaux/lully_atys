\piecePartSpecs
#`((dessus)
   (haute-contre)
   (haute-contre-sol)
   (taille)
   (quinte)
   (basse #:score "score-basse")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \column {
  \livretPers Chœurs des Peuples et des Zéphirs
  \livretVerse#8 { Célébrons la gloire immortelle }
  \livretVerse#12 { Du sacrificateur dont Cybèle a fait choix : }
  \livretVerse#8 { Atys doit dispenser ses lois, }
  \livretVerse#8 { Honorons le choix de Cybèle. }
} #}))

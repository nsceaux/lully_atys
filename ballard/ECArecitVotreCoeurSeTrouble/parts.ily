\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix"
          #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Célénus
    \livretVerse#8 { Votre cœur se trouble, il soupire. }
    \livretPers Sangaride
    \livretVerse#8 { Expliquez en votre faveur }
    \livretVerse#12 { Tout ce que vous voyez de trouble dans mon cœur. }
    \livretPers Célénus
    \livretVerse#12 { Rien ne m’alarme plus, Atys, ma crainte est vaine, }
    \livretVerse#12 { Mon amour touche enfin le cœur de la beauté }
    \livretVerse#6 { Dont je suis enchanté : }
  }
  \column {
    \null
    \livretVerse#8 { Toi qui fus témoin de ma peine, }
    \livretVerse#12 { Cher Atys, sois témoin de ma félicité. }
    \livretVerse#12 { Peux-tu la concevoir ? non, il faut que l’on aime, }
    \livretVerse#12 { Pour juger des douceurs de mon bonheur extrême. }
    \livretVerse#8 { Mais, près de voir combler mes vœux, }
    \livretVerse#12 { Que les moments sont longs pour mon cœur amoureux ! }
    \livretVerse#12 { Vos parents tardent trop, je veux aller moi-même }
    \livretVerse#8 { Les presser de me rendre heureux. }
  }
}#}))

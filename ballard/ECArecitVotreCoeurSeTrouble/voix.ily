\celaenusMark r8 re'16 re' sol8. sol16 |
mi4\trill mi r8 la16 la |
fad4\trill fad8 
\sangarideMark la'16 la' si'8 si' do'' do''16 do'' |
la'4\trill do''8 do''16 do'' re''8 mi'' |
fa''8. mi''16 re''8\trill re'' re''8. mi''16 |
do''4 r4
\celaenusMark mi8\trill mi16 mi mi8. fa16 |
sol4. sol8 do'8. do'16 fad8.\trill sol16 |
sol4 sol r8 re16 mi fa8 fa16 sol |
la8. si16 do'8. do'16 si8\trill do' |
re'4 la8. la16 si4 la8.\trill sol16 |
sol2 r |
si8. si16 si8. si16 do'4 re'8. mi'16 |
la8\trill la re'8. re'16 si4\trill si8. si16 |
sold4.\trill la8 si do' si8.\trill la16 |
la4 r8 mi' la8. la16 la8. la16 |
fad4\trill re' r8 fad sol8 sol16 la |
si8 si r sol16 sol mi8\trill fa16 sol |
la8. la16 la8. la16 si8 do' do'[ si] |
do'2 r |
mi4. mi16 mi fa8. fa16 sol8. la16 |
re4 r16 sib sib sib do'8. re'16 |
fad4\trill la8. la16 sib4 la8.\trill sol16 |
sol2 r8 sol16 sol re8\trill re16 mi |
fa4 r8 la la8. la16 la8 si |
do'8 do' r la16 sol fa8\trill fa16 mi re8.\tr do16 |
do1 |

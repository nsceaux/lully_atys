%% ACTE 4 SCENE 3
Vo -- tre cœur se trou -- ble, il sou -- pi -- re.

Ex -- pli -- quez en vo -- tre fa -- veur
tout ce que vous voy -- ez de trou -- ble dans mon cœur.

Rien ne m’a -- lar -- me plus, A -- tys, ma crainte est vai -- ne,
mon a -- mour touche en -- fin le cœur de la beau -- té
dont je suis en -- chan -- té :
toi qui fus té -- moin de ma pei -- ne,
cher A -- tys, sois té -- moin de ma fé -- li -- ci -- té.
Peux- tu la con -- ce -- voir ? non, il faut que l’on ai -- me,
pour ju -- ger des dou -- ceurs de mon bo -- nheur ex -- trê -- me.
Mais, près de voir com -- bler mes vœux,
que les mo -- ments sont longs pour mon cœur a -- mou -- reux !
Vos pa -- rents tar -- dent trop, je veux al -- ler moi- mê -- me
les pres -- ser de me rendre heu -- reux.

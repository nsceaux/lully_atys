\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix"
          #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \column {
  \livretPers Atys
  \livretVerse#6 { On voit dans ces campagnes }
  \livretVerse#8 { Tous nos Phrygiens s’avancer. }
  \livretPers Doris
  \livretVerse#8 { Je vais prendre soin de presser }
  \livretVerse#6 { Les Nymphes nos compagnes. }
} #}))

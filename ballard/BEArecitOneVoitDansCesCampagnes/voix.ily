\atysMark r4 r8 la do'8. do'16 do'8 re' | % do'8 do' do'8. re'16
si8\trill si si16 si si si do'4 do'8 re' |
mi'4
\dorisMark sold'16 sold' sold' sold' la'4 %{ la'8. la'16 %} la'8 la' |
fad'4. re''8 si'8.\trill si'16 si'8. do''16 |
la'4\trill la' r4 |
R2.*3 |

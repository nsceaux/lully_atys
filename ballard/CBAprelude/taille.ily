\clef "taille" re'2. re'4 |
re'2 re' |
re' mi'4. mi'8 |
mi'2 re'4. re'8 |
re'2 dod' |
re'4 la' la'4. la'8 |
si'2 la' |
si'2. si'4 |
mi'2 re' |
mi'4 la' la'4. la'8 |
la'2 sol'4. sol'8 |
sol'2 mi'4. mi'8 |
re'2 re'~ |
re' re'4. %{ do'8 %} re'8 |
si4. sol'8 sol'2~ |
sol'2. re'4 |
mi'2 mi'4. mi'8 |
%{ do'2 %} re'2 re'~ |
re' re'4. do'8 |
si1 |

\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Célénus
    \livretVerse#12 { Vous m’ôtez Sangaride ? inhumaine Cybèle ; }
    \livretVerse#6 { Est-ce le prix du zèle }
    \livretVerse#12 { Que j’ai fait avec soin éclater à vos yeux ? }
    \livretVerse#12 { Préparez-vous ainsi la douceur éternelle }
    \livretVerse#8 { Dont vous devez combler ces lieux ? }
    \livretVerse#12 { Est-ce ainsi que les rois sont protegés des dieux ? }
    \livretVerse#6 { Divinité cruelle, }
    \livretVerse#8 { Descendez-vous exprès des cieux }
    \livretVerse#8 { Pour troubler un amour fidèle ? }
    \livretVerse#12 { Et pour venir m’ôter ce que j’aime le mieux ? }
    \livretPers Cybèle
    \livretVerse#12 { J’aimais Atys, l’amour a fait mon injustice ; }
    \livretVerse#8 { Il a pris soin de mon supplice ; }
    \livretVerse#8 { Et si vous êtes outragé, }
    \livretVerse#8 { Bientôt vous serez trop vengé. }
    \livretVerse#8 { Atys adore Sangaride. }
    \livretPers Célénus
    \livretVerse#8 { Atys l’adore ? ah le perfide ! }
    \livretPers Cybèle
    \livretVerse#12 { L’ingrat vous trahissait, et voulait me trahir : }
    \livretVerse#12 { Il s’est trompé lui-même en croyant m’éblouir. }
    \livretVerse#12 { Les Zéphirs l’ont laissé, seul, avec ce qu’il aime, }
  }
  \column {
    \livretVerse#6 { Dans ces aimables lieux ; }
    \livretVerse#8 { Je m’y suis cachée à leurs yeux ; }
    \livretVerse#12 { J’y viens d’être témoin de leur amour extrême. }
    \livretPers Célénus
    \livretVerse#12 { Ô Ciel ! Atys plairait aux yeux qui m’ont charmé ? }
    \livretPers Cybèle
    \livretVerse#12 { Eh pouvez-vous douter qu’Atys ne soit aimé ? }
    \livretVerse#12 { Non, non, jamais amour n’eût tant de violence, }
    \livretVerse#12 { Ils ont juré cent fois de s’aimer malgré nous, }
    \livretVerse#8 { Et de braver notre vengeance ; }
    \livretVerse#12 { Ils nous ont appelés cruels, tyrans, jaloux ; }
    \livretVerse#8 { Enfin leurs cœurs d’intelligence, }
    \livretVerse#12 { Tous deux… ah je frémis au moment que j’y pense ! }
    \livretVerse#12 { Tous deux s’abandonnaient à des transports si doux, }
    \livretVerse#12 { Que je n’ai pû garder plus longtemps le silence, }
    \livretVerse#12 { Ni retenir l’éclat de mon juste couroux. }
    \livretPers Célénus
    \livretVerse#12 { La mort est pour leur crime une peine légère. }
    \livretPers Cybèle
    \livretVerse#12 { Mon cœur à les punir est assez engagé ; }
    \livretVerse#12 { Je vous l’ai déja dit, croyez-en ma colère, }
    \livretVerse#8 { Bientôt vous serez trop vengé. }
  }
}#}))

<<
  \tag #'(voix1 basse) {
    \celaenusMark r8 re16 mi fad8\trill fad16 sol la8 la16 la la8 si16 dod' |
    re'8 re' si si16 si si8 fad |
    sol sol r mi16 fad sol8 sol16 la |
    si4 si8 si mi'4 mi'8 mi' |
    dod'8.\trill mi16 mi mi mi fad sol8 sol16 sol sol8 sol16 fad |
    fad4\trill fad8 si si si dod' re' sold4.\trill la8 |
    la4 r8 mi16 mi la8 la16 si |
    dod'8. la16 la la si dod' re' la la la fad8.\trill la16 |
    re8 re r16 re' re' do' si8.\trill si16 do'8. la16 |
    mi'4 r8 si16 si sold8\trill sold16 sold la8 si |
    dod' dod' re'4 re'16 dod' si la |
    sol4\trill sol8 fad mi4\trill mi8 fad |
    re4
    \cybeleMark r8 la' la'8. la'16 fad'4\trill |
    r8 re'' re''8. re''16 si'8.\trill si'16 si'8. si'16 |
    sold'8 sold'16 si' si'8. dod''16 re''8. re''16 re''8. mi''16 |
    dod''8\trill dod'' r16 la' la' la' re''8. re''16 re''8. re''16 |
    si'8\trill mi'' dod''\trill mi''16 fad'' re''4 re''8 dod'' |
    re''2 r |
    r8 la' fad'8.\trill fad'16 sol'8. sol'16 sol'8. la'16 |
    si'4 si'
    <<
      \tag #'basse { s2 s1 s8 \cybeleMark }
      \tag #'voix1 { r2 R1 r8 }
    >> mi''8 la'16 la' la' la' mi'8\trill mi'16 mi' la'8 la'16 la' |
    fad'\trill re'' re'' re'' fad'8. fad'16 sol'8 sol'16 sol' la'8 si'16 do'' |
    si'4\trill r8 re''16 re'' si'8\trill si'16 si' |
    do''4 la'8.\trill la'16 la'8 la'16 sol' |
    fad'8\trill fad' si' si'16 la' la'8. sold'16 |
    sold'4 r si'16 si' si' si' mi'8 fad'16 sold' |
    la'8. la'16 la' la' si' dod'' re''8. re''16 re'' re'' re'' mi'' |
    dod''8\trill dod''
    \celaenusMark r8 mi' la4 r8 la |
    mi8.\trill mi16 mi8. fad16 sol8. sol16 sol8. fad16 |
    fad4\trill
    \cybeleMark re''4 r si'16 si' si' si' |
    sold'8. sold'16 la'8 la' la' sold' |
    la'4 r8 mi'' dod''8.\trill dod''16 dod''8. mi''16 |
    la'8. la'16 la' la' si' dod'' re''8 re'' r16 la' la' la' |
    fad'8.\trill fad'16 sol'8 sol'16 si' sol'8\trill sol'16 sol' |
    mi'4\trill dod''8 dod''16 mi'' sold'8.\trill sold'16 sold'8. la'16 |
    la'4 la' r8 dod''16 dod'' dod''8 dod''16 dod'' |
    re''8. re''16 si'8.\trill mi''16 dod''8.\trill fad''16 |
    red''4 r8 si' fad'8.\trill fad'16 |
    sol'8. sol'16 la'8. si'16 mi'8\trill mi' r mi'' |
    dod''\trill fad'' r dod''16 dod'' lad'8 lad'16 lad' lad'8 si'16 dod'' |
    fad'8\trill fad' r fad'' re''8. re''16 re''8. re''16 |
    si'4\trill si'8 si'16 la' la'8. sold'16 |
    sold'8.\trill mi'16 mi' mi' fad' sold' la'8 la'16 la' si'8 dod''16 re'' |
    dod''8\trill dod''16 la' la' si' dod'' la' mi''8 mi''16 fad'' dod''8\trill dod''16 re'' |
    re''4
    \celaenusMark r8 la fad8.\trill fad16 sol8 la |
    si8 si16 si sol8\trill sol16 si mi4 mi8
    \cybeleMark mi''8 dod''8.\trill dod''16 dod''8. mi''16 la'8 la'16 la' mi'8 fad'16 sol' |
    fad'8.\trill la'16 la' la' la' si' \ficta do''?8 %{ dod''8 1ère édition %} la'16 la' re''8 re''16 re'' |
    si'8\trill si'16 mi'' dod''8\trill mi''16 fad'' re''4 re''8 dod'' |
    \custosNote re''4
  }
  \tag #'(voix2 basse) {
    <<
      \tag #'basse {
        s1 s2.*2 s1 s1 s1. s2. s1*3 s2. s1 s1*5 s1 s1 s2 \celaenusMark
      }
      \tag #'voix2 {
        \clef "vbasse-taille" R1 R2.*2 R1 R1 R1. R2. R1*3 R2. R1 R1*5 R1 R1 r2
        <>^\markup\character Célénus
      }
    >> r8 sol sol sol |
    mi4\trill mi mi' r8 dod'16 dod' |
    <<
      \tag #'basse { la16\trill la s8 }
      \tag #'voix2 { la8\trill la r4 r2 | R1 R2.*3 R1*2 }
    >>
  }
>>
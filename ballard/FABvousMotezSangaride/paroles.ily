%% ACTE 5 SCENE 1
\tag #'(voix1 basse) {
  Vous m’ô -- tez San -- ga -- ride ? i -- nhu -- mai -- ne Cy -- bè -- le ;
  est- ce le prix du zè -- le
  que j’ai fait a -- vec soin é -- cla -- ter à vos yeux ?
  Pré -- pa -- rez- vous ain -- si la dou -- ceur é -- ter -- nel -- le
  dont vous de -- vez com -- bler ces lieux ?
  Est-ce ain -- si que les rois sont pro -- te -- gés des dieux ?
  Di -- vi -- ni -- té cru -- el -- le,
  des -- cen -- dez- vous ex -- près des cieux
  pour trou -- bler un a -- mour fi -- dè -- le ?
  Et pour ve -- nir m’ô -- ter ce que j’ai -- me le mieux ?
  
  J’ai -- mais A -- tys, l’a -- mour a fait mon in -- jus -- ti -- ce ;
  il a pris soin de mon sup -- pli -- ce ;
  et si vous ê -- tes ou -- tra -- gé,
  bien -- tôt vous se -- rez trop ven -- gé.
  A -- tys a -- do -- re San -- ga -- ri -- de.
}
\tag #'(voix2 basse) {
  A -- tys l’a -- do -- re ? ah le per -- fi -- de !
}
\tag #'(voix1 basse) {
  L’in -- grat vous tra -- his -- sait, et vou -- lait me tra -- hir :
  il s’est trom -- pé lui- même en croy -- ant m’é -- blou -- ir.
  Les Zé -- phirs l’ont lais -- sé, seul, a -- vec ce qu’il ai -- me,
  dans ces ai -- ma -- bles lieux ;
  je m’y suis ca -- chée à leurs yeux ;
  j’y viens d’ê -- tre té -- moin de leur a -- mour ex -- trê -- me.
  
  Ô Ciel ! A -- tys plai -- rait aux yeux qui m’ont char -- mé ?
  
  Eh pou -- vez- vous dou -- ter qu’A -- tys ne soit ai -- mé ?
  Non, non, ja -- mais a -- mour n’eût tant de vi -- o -- len -- ce,
  ils ont ju -- ré cent fois de s’ai -- mer mal -- gré nous,
  et de bra -- ver no -- tre ven -- gean -- ce ;
  ils nous ont ap -- pe -- lés cru -- els, ty -- rans, ja -- loux ;
  en -- fin leurs cœurs d’in -- tel -- li -- gen -- ce,
  tous deux… ah je fré -- mis au mo -- ment que j’y pen -- se !
  Tous deux s’a -- ban -- don -- naient à des trans -- ports si doux,
  que je n’ai pû gar -- der plus long -- temps le si -- len -- ce,
  ni re -- te -- nir l’é -- clat de mon jus -- te cou -- roux.
  
  La mort est pour leur crime u -- ne pei -- ne lé -- gè -- re.
  
  Mon cœur à les pu -- nir est as -- sez en -- ga -- gé ;
  je vous l’ai dé -- ja dit, croy -- ez- en ma co -- lè -- re,
  bien -- tôt vous se -- rez trop ven -- gé.
}

\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse #:score-template "score-basse-continue-voix"
          #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers \line { ATYS seul. }
    \livretVerse#12 { Nous pouvons nous flatter de l’espoir le plus doux }
    \livretVerse#8 { Cybèle et l’Amour sont pour nous. }
    \livretVerse#12 { Mais du devoir trahi j’entends la voix pressante }
    \livretVerse#8 { Qui m’accuse et qui m’épouvante. }
    \livretVerse#12 { Laisse-mon cœur en paix, impuissante Vertu, }
    \livretVerse#8 { N’ai-je point assez combattu ? }
    \livretVerse#12 { Quand l’Amour malgré toi me contraint à me rendre, }
    \livretVerse#6 { Que me demandes-tu ? }
  }
  \column {
    \null
    \livretVerse#8 { Puisque tu ne peux me défendre, }
    \livretVerse#6 { Que me sert-il d’entendre }
    \livretVerse#8 { Les vains repproches que tu fais ? }
    \livretVerse#12 { Impuissante Vertu laisse mon cœur en paix. }
    \livretVerse#8 { Mais le Sommeil vient me surprendre, }
    \livretVerse#12 { Je combats vainement sa charmante douceur. }
    \livretVerse#6 { Il faut laisser suspendre }
    \livretVerse#6 { Les troubles de mon cœur. }
  }
}#}))

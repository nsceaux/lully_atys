%% ACTE 3 SCENE 3
% Atys seul.
Nous pou -- vons nous flat -- ter de l’es -- poir le plus doux
Cy -- bèle et l’a -- mour sont pour nous.
Mais du de -- voir tra -- hi j’en -- tends la voix pres -- san -- te
qui m’ac -- cuse et qui m’é -- pou -- van -- te.
Lais -- se- mon cœur en paix, im -- puis -- san -- te ver -- tu,
n’ai- je point as -- sez com -- bat -- tu ?
Quand l’a -- mour mal -- gré toi me con -- traint à me ren -- dre,
que me de -- man -- des- tu ?
Puis -- que tu ne peux me dé -- fen -- dre,
que me sert- il d’en -- ten -- dre
les vains rep -- pro -- ches que tu fais ?
Im -- puis -- san -- te ver -- tu lais -- se mon cœur en paix.
Mais le som -- meil vient me sur -- pren -- dre,
je com -- bats vai -- ne -- ment sa char -- man -- te dou -- ceur.
Il faut lais -- ser sus -- pen -- dre
les trou -- bles de mon cœur.

\tag #'(couplet-1-1 basse) {
  \set stanza = "[1]"
  La beau -- té la plus sé -- vè -- re
  prend pi -- tié d’un long tour -- ment,
}
\tag #'couplet-1-2 {
  et l’a -- mant qui per -- sé -- vè -- re
  de -- vient un heu -- reux a -- mant.
}
\tag #'couplet-2-1 {
  \override Lyrics . LyricText . font-shape = #'italic
  \set stanza = "[2]"
  [Il n’est point de ré -- sis -- tan -- ce
  dont le temps ne vienne à bout,
}
\tag #'couplet-2-2 {
  \override Lyrics . LyricText . font-shape = #'italic
  et l’ef -- fort de la cons -- tan -- ce
  à la fin doit vain -- cre tout.]
}

\tag #'(refrain basse) {
  Tout est doux, et rien ne coû -- te
  pour un cœur qu’on veut tou -- cher,
  l’on -- de se fait u -- ne rou -- te
  en s’ef -- for -- çant d’en cher -- cher,
  l’eau qui tom -- be goûte à goû -- te
  per -- ce le plus dur ro -- cher.
}
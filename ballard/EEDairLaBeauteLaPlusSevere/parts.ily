\piecePartSpecs
#`((basse #:tag-notes basse
          #:clef "alto"
          #:indent 0
          #:score "score-basse")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretVerse#7 { La beauté la plus sévère }
    \livretVerse#7 { Prend pitié d’un long tourment, }
    \livretVerse#7 { Et l’amant qui persévère }
    \livretVerse#7 { Devient un heureux amant. }
    \livretVerse#7 { Tout est doux, et rien ne coûte }
    \livretVerse#7 { Pour un cœur qu’on veut toucher, }
    \livretVerse#7 { L’onde se fait une route }
    \livretVerse#7 { En s’efforçant d’en chercher, }
    \livretVerse#7 { L’eau qui tombe goûte à goûte }
    \livretVerse#7 { Perce le plus dur rocher. }
  }
  \column {
    \livretVerse#7 { Il n’est point de résistance }
    \livretVerse#7 { Dont le temps ne vienne à bout, }
    \livretVerse#7 { Et l’effort de la constance }
    \livretVerse#7 { À la fin doit vaincre tout. }
    \livretVerse#7 { Tout est doux, et rien ne coûte }
    \livretVerse#7 { Pour un cœur qu’on veut toucher, }
    \livretVerse#7 { L’onde se fait une route }
    \livretVerse#7 { En s’efforçant d’en chercher, }
    \livretVerse#7 { L’eau qui tombe goûte à goûte }
    \livretVerse#7 { Perce le plus dur rocher. }
  }
}#}))

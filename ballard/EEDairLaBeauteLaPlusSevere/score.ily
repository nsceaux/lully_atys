\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff \withLyricsD <<
      \global \keepWithTag #'vdessus \includeNotes "voix"
    >>
    \keepWithTag #'(couplet-1-1 refrain) \includeLyrics "paroles"
    \keepWithTag #'couplet-1-2 \includeLyrics "paroles"
    \keepWithTag #'couplet-2-1 \includeLyrics "paroles"
    \keepWithTag #'couplet-2-2 \includeLyrics "paroles"
    \new Staff \withLyricsD <<
      \global \keepWithTag #'vbas-dessus \includeNotes "voix"
    >>
    \keepWithTag #'(couplet-1-1 refrain) \includeLyrics "paroles"
    \keepWithTag #'couplet-1-2 \includeLyrics "paroles"
    \keepWithTag #'couplet-2-1 \includeLyrics "paroles"
    \keepWithTag #'couplet-2-2 \includeLyrics "paroles"
    \new Staff \withLyricsD <<
      \global \keepWithTag #'vhaute-contre \includeNotes "voix"
    >>
    \keepWithTag #'(couplet-1-1 refrain) \includeLyrics "paroles"
    \keepWithTag #'couplet-1-2 \includeLyrics "paroles"
    \keepWithTag #'couplet-2-1 \includeLyrics "paroles"
    \keepWithTag #'couplet-2-2 \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}

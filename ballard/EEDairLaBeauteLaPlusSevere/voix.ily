<<
  \tag #'(vdessus basse) {
    \clef "vdessus" mi''4. mi''8 |
    re''4\trill re'' sol''4. sol''8 |
    mi''4\trill mi'' fa''4. fa''8 |
    fa''4 mi'' re''4.\trill do''8 |
    do''2 mi''4. mi''8 |
    do''4 do'' fa''8[ mi''] re''[ do''] |
    si'4\trill si' mi''4. mi''8 |
    do''4 re'' si'4. mi''8 |
    dod''2 mi''4. mi''8 |
    fa''4 fa'' mi''4. fa''8 |
    re''4\trill re'' sol''4. sol''8 |
    mi''4 fa'' mi''4.\trill re''8 |
    re''2 fa''4. fa''8 |
    fa''4 fa'' mi''4. mi''8 |
    mi''4 mi'' re''4. re''8 |
    re''4 mi''4 re''4.\trill do''8 |
    do''2
  }
  \tag #'vbas-dessus {
    \clef "vbas-dessus" do''4. do''8 |
    si'4\trill si' mi''4. mi''8 |
    do''4 do'' la'4. la'8 |
    si'4 do'' si'4.\trill do''8 |
    do''2 do''4. do''8 |
    la'4\trill la' re''8[ do''] si'[ la'] |
    sold'4\trill sold' do''4. do''8 |
    la'4 si' sold'4.\trill la'8 |
    la'2 dod''4. dod''8 |
    re''4 re'' do''!4. re''8 |
    si'4\trill si' si'4. mi''8 |
    dod''4 re'' dod''4.\trill re''8 |
    re''2 la'4. la'8 |
    si'4 si' do''4. do''8 |
    do''4 do'' la'4. la'8 |
    si'4 do'' si'4.\trill do''8 |
    do''2
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" do'4. do'8 |
    sol'4 sol' mi'4. mi'8 |
    la'4 la' fa' re' |
    sol' do' sol4. do'8 |
    do'2 do'4. do'8 |
    fa'4 fa' re'4. re'8 |
    mi'4 mi' do'4. do'8 |
    fa'4 re' mi'4. mi'8 |
    la2 la'4. la'8 |
    re'4 re' la'4. fa'8 |
    sol'4 sol' mi'4. mi'8 |
    la'4 re' la4. re'8 |
    re'2 re'4. re'8 |
    sol'4 sol' do'4. do'8 |
    fa'4 fa' fa'4. re'8 |
    sol'4 do' sol4. do'8 |
    do'2
  }
>>

\clef "dessus" sol''2 do''2. lab''4 |
fa''2 re''2. sol''4 |
mib''2 do''2. mib''4 |
re''2\trill re''2. sol''4 |
do''2 do''2. re''4 |
si'1. |
mib''2 mi''2. mi''4 |
fa''2. fa''4 sol''2 |
lab''2 mi''2. fa''4 |
fa''2 la'2. re''4 |
si'2 sol''2. si'4 |
do''2 do''( re''4) mib'' |
re''2\trill fa''( mib''4)\tr re'' |
mib''2\trill re''2. do''4 |
do''1. |

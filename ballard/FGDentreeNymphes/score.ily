\score {
  \new StaffGroup <<
    \new Staff <<
      { <>^"[Flutes]" s1.*14 s1
        \footnoteHere #'(0 . 0) \markup { [Philidor 1703] Reprise. }
      }
      \global \includeNotes "dessus" >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
  \midi { }
}

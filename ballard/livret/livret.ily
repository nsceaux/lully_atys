\notesSection "Livret"
\markuplist\abs-fontsize-lines #8 \page-columns-title \act\line { LIVRET } {
\livretAct PROLOGUE
\livretDescAtt\wordwrap-center {
  Le Théâtre représente le palais du Temps, où ce dieu paraît au
  milieu des douze heures du jour, et des douze heures de la nuit.
}
\livretPers Le Temps
\livretRef#'AABrecitEnVainJaiRespecte
\livretVerse#12 { En vain j’ai respecté la célèbre mémoire }
\livretVerse#8 { Des héros des siècles passés ; }
\livretVerse#12 { C’est en vain que leurs noms si fameux dans l’histoire, }
\livretVerse#12 { Du sort des noms communs ont été dispensés : }
\livretVerse#12 { Nous voyons un héros dont la brillante gloire }
\livretVerse#8 { Les a presque tous effacés. }

\livretPers Chœur des Heures
\livretRef#'AACchoeurSesJustesLois
\livretVerse#4 { Ses justes lois, }
\livretVerse#4 { Ses grands exploits }
\livretVerse#8 { Rendent sa mémoire éternelle : }
\livretVerse#6 { Chaque jour, chaque instant }
\livretVerse#10 { Ajoute encore à son nom éclatant }
\livretVerse#6 { Une gloire nouvelle. }

\livretRef#'AADairFlore
\livretDidasPPage\justify {
  La Déesse Flore conduite par un des Zéphirs s’avance avec une Troupe
  de Nymphes qui portent divers ornements de Fleurs.
}
\livretPers Le Temps
\livretRef#'AAErecitLaSaisonDesFrimas
\livretVerse#12 { La saison des frimas peut-elle nous offrir }
\livretVerse#8 { Les fleurs que nous voyons paraître ? }
\livretVerse#6 { Quel dieu les fait renaître }
\livretVerse#8 { Lorsque l’hiver les fait mourir ? }
\livretVerse#7 { Le froid cruel règne encore ; }
\livretVerse#7 { Tout est glacé dans les champs, }
\livretVerse#4 { D’où vient que Flore }
\livretVerse#6 { Devance le printemps ? }
\livretPers Flore
\livretVerse#12 { Quand j’attends les beaux jours, je viens toujours trop tard, }
\livretVerse#12 { Plus le printemps s’avance, et plus il m’est contraire ; }
\livretVerse#8 { Son retour presse le départ }
\livretVerse#8 { Du héros à qui je veux plaire. }
\livretVerse#12 { Pour lui faire ma cour, mes soins ont entrepris }
\livretVerse#12 { De braver désormais l’hiver le plus terrible, }
\livretVerse#12 { Dans l’ardeur de lui plaire on a bientôt appris }
\livretVerse#8 { À ne rien trouver d’impossible. }

\livretPers Le Temps et Flore
\livretRef#'AAFairChoeurLesPlaisirASesYeux
\livretVerse#12 { Les plaisirs à ses yeux ont beau se présenter, }
\livretVerse#12 { Si tôt qu’il voit Bellone, il quitte tout pour elle ; }
\livretVerse#6 { Rien ne peut l’arrêter }
\livretVerse#6 { Quand la gloire l’appelle. }
\livretDidasP\wordwrap {
  Le chœur des heures répète ces deux derniers vers.
}

\livretRef#'AAGgavotte
\livretDidasPPage\wordwrap {
  La suite de Flore commence des jeux mêlés de danses et de chants.
}

\livretPers Un Zéphir
\livretRef#'AAHairLePrintempsQuelqueFois
\livretVerse#12 { Le printemps quelque fois est moins doux qu’il ne semble, }
\livretVerse#8 { Il fait trop payer ses beaux jours ; }
\livretVerse#12 { Il vient pour écarter les jeux et les amours, }
\livretVerse#8 { Et c’est l’hiver qui les rassemble. }

\livretRef#'AAIpreludeMelpomene
\livretDidasPPage\justify {
  Melpomène qui est la muse qui préside à la tragédie, vient
  accompagnée d’une troupe de héros, elle est suivie d’Hercule,
  d’Antæe, de Castor, de Pollux, de Lincée, d’Idas, d’Étéocle,
  et de Polinice.
}
\livretPersDidas Melpomène parlant à Flore
\livretRef#'AAJrecitRetirezVous
\livretVerse#12 { Retirez-vous, cessez de prévenir le Temps ; }
\livretVerse#12 { Ne me dérobez point de précieux instants : }
\livretVerse#6 { La puissante Cybèle }
\livretVerse#12 { Pour honorer Atys qu’elle a privé du jour, }
\livretVerse#6 { Veut que je renouvelle }
\livretVerse#6 { Dans une illustre cour }
\livretVerse#8 { Le souvenir de son amour. }
\livretVerse#6 { Que l’agrément rustique }
\livretVerse#6 { De Flore et de ses jeux, }
\livretVerse#8 { Cède à l’appareil magnifique }
\livretVerse#6 { De la muse tragique, }
\livretVerse#8 { Et de ses spectacles pompeux. }

\livretRef#'AAKairMelpomene
\livretDidasPPage\justify {
  La suite de Melpomène prend la place de la suite de Flore.  Les
  héros recommencent leurs anciennes querelles.  Hercule combat et
  lutte contre Antæe, Castor et Pollux combattent contre Lyncée et
  Idas, et Étéocle combat contre son frère Polynice.
}
\livretRef#'AALcybeleVeutQueFlore
\livretDidasPPage\justify {
  Iris, par l’ordre de Cybèle, descend assis
  sur son arc, pour accorder Melpomène et Flore.
}
\livretPersDidas Iris parlant à Melpomène
\livretVerse#12 { Cybèle veut que Flore aujourd’hui vous seconde. }
\livretVerse#12 { Il faut que les plaisirs viennent de toutes parts, }
\livretVerse#12 { Dans l’empire puissant, où règne un nouveau Mars, }
\livretVerse#8 { Ils n’ont plus d’autre asile au monde. }
\livretVerse#12 { Rendez-vous, s’il se peut, dignes de ses regards ; }
\livretVerse#8 { Joignez la beauté vive et pure }
\livretVerse#6 { Dont brille la nature, }
\livretVerse#8 { Aux ornements des plus beaux arts. }
\livretDidasP\justify {
  Iris remonte au ciel sur son arc, et la suite
  de Melpomène s’accorde avec la suite de Flore.
}
\livretPers Melpomène et Flore
\livretVerse#12 { Rendons-nous, s’il se peut, dignes de ses regards ; }
\livretVerse#8 { Joignons la beauté vive et pure }
\livretVerse#6 { Dont brille la nature, }
\livretVerse#8 { Aux ornements des plus beaux arts. }

\livretPers Le Temps, et le Chœur des Heures
\livretVerse#8 { Préparez de nouvelles fêtes, }
\livretVerse#12 { Profitez du loisir du plus grand des héros ; }
\livretPers Le Temps, Melpomène et Flore
\livretVerse#8 { Préparez/Préparons de nouvelles fêtes }
\livretVerse#12 { Profitez/Profitons du loisir du plus grand des héros. }
\livretPers Tous ensemble
\livretVerse#8 { Le temps des jeux, et du repos, }
\livretVerse#12 { Lui sert à méditer de nouvelles conquêtes. }
\sep
\livretAct ACTE PREMIER
\livretDescAtt\wordwrap-center {
  Le Théâtre représente une montagne consacrée à Cybèle.
}
\livretScene SCÈNE PREMIÈRE
\livretPers Atys
\livretRef#'BAAairAllonsAllons
\livretVerse#8 { Allons, allons, accourez tous, }
\livretVerse#6 { Cybèle va descendre. }
\livretVerse#12 { Trop heureux Phrygiens, venez ici l’attendre. }
\livretVerse#8 { Mille peuples seront jaloux }
\livretVerse#6 { Des faveurs que sur nous }
\livretVerse#6 { Sa bonté va répandre. }

\livretScene SCÈNE SECONDE
\livretPers Idas, Atys
\livretRef#'BBAairAllonsAllons
\livretVerse#8 { Allons, allons, accourez tous, }
\livretVerse#6 { Cybèle va descendre. }
\livretPers Atys
\livretVerse#12 { Le soleil peint nos champs des plus vives couleurs, }
\livretVerse#6 { Il a séché les pleurs }
\livretVerse#12 { Que sur l’émail des prés a répandu l’aurore ; }
\livretVerse#12 { Et ses rayons nouveaux ont déjà fait éclore }
\livretVerse#6 { Mille nouvelles fleurs. }
\livretPers Idas
\livretVerse#8 { Vous veillez lorsque tout sommeille ; }
\livretVerse#8 { Vous nous éveillez si matin }
\livretVerse#8 { Que vous ferez croire à la fin }
\livretVerse#8 { Que c’est l’Amour qui vous éveille. }
\livretPers Atys
\livretVerse#12 { Non tu dois mieux juger du parti que je prends. }
\livretVerse#12 { Mon cœur veut fuir toujours les soins et les mystères ; }
\livretVerse#12 { J’aime l’heureuse paix des cœurs indifférents ; }
\livretVerse#8 { Si leurs plaisirs ne sont pas grands, }
\livretVerse#8 { Au moins leurs peines sont légères. }
\livretPers Idas
\livretVerse#8 { Tôt ou tard l’amour est vainqueur, }
\livretVerse#8 { En vain les plus fiers s’en défendent, }
\livretVerse#8 { On ne peut refuser son cœur }
\livretVerse#8 { À de beaux yeux qui le demandent. }
\livretVerse#12 { Atys, ne feignez plus, je sais votre secret. }
\livretVerse#8 { Ne craignez rien, je suis discret. }
\livretVerse#8 { Dans un bois solitaire, et sombre, }
\livretVerse#12 { L’indifférent Atys se croyait seul, un jour ; }
\livretVerse#12 { Sous un feuillage épais où je rêvais à l’ombre, }
\livretVerse#8 { Je l’entendis parler d’amour. }
\livretPers Atys
\livretVerse#12 { Si je parle d’amour, c’est contre son empire, }
\livretVerse#8 { J’en fais mon plus doux entretien. }
\livretPers Idas
\livretVerse#8 { Tel se vante de n’aimer rien, }
\livretVerse#8 { Dont le cœur en secret soupire. }
\livretVerse#12 { J’entendis vos regrets, et je les sais si bien }
\livretVerse#12 { Que si vous en doutez je vais vous les redire. }
\livretVerse#12 { Amants qui vous plaignez, vous êtes trop heureux : }
\livretVerse#12 { Mon cœur de tous les cœurs est le plus amoureux, }
\livretVerse#12 { Et tout près d’expirer je suis réduit à feindre ; }
\livretVerse#8 { Que c’est un tourment rigoureux }
\livretVerse#8 { De mourir d’amour sans se plaindre ! }
\livretVerse#12 { Amants qui vous plaignez, vous êtes trop heureux. }
\livretPers Atys
\livretVerse#12 { Idas, il est trop vrai, mon cœur n’est que trop tendre, }
\livretVerse#12 { L’Amour me fait sentir ses plus funestes coups. }
\livretVerse#12 { Qu’aucun autre que toi n’en puisse rien apprendre. }

\livretScene SCÈNE TROISIÈME
\livretDescAtt\wordwrap-center { Sangaride, Doris, Atys, Idas. }
\livretPers Sangaride, et Doris
\livretRef#'BCAairAllonsAllons
\livretVerse#8 { Allons, allons, accourez tous, }
\livretVerse#6 { Cybèle va descendre. }
\livretPers Sangaride
\livretVerse#8 { Que dans nos concerts les plus doux, }
\livretVerse#8 { Son nom sacré se fasse entendre. }
\livretPers Atys
\livretVerse#12 { Sur l’univers entier son pouvoir doit s’étendre. }
\livretPers Sangaride
\livretVerse#12 { Les Dieux suivent ses lois et craignent son couroux. }
\livretPers Atys, Sangaride, Idas, Doris
\livretVerse#12 { Quels honneurs ! quels respects ne doit-on point lui rendre ? }
\livretVerse#8 { Allons, allons, accourez tous, }
\livretVerse#6 { Cybèle va descendre. }
\livretPers Sangaride
\livretVerse#12 { Écoutons les oiseaux de ces bois d’alentour, }
\livretVerse#12 { Ils remplissent leurs chants d’une douceur nouvelle. }
\livretVerse#8 { On dirait que dans ce beau jour, }
\livretVerse#8 { Ils ne parlent que de Cybèle. }
\livretPers Atys
\livretVerse#12 { Si vous les écoutez, ils parleront d’amour. }
\livretVerse#5 { Un roi redoutable, }
\livretVerse#5 { Amoureux, aimable, }
\livretVerse#7 { Va devenir votre époux ; }
\livretVerse#7 { Tout parle d’amour pour vous. }
\livretPers Sangaride
\livretVerse#12 { Il est vrai, je triomphe, et j’aime ma victoire. }
\livretVerse#12 { Quand l’Amour fait régner, est-il un plus grand bien ? }
\livretVerse#8 { Pour vous, Atys, vous n’aimez rien, }
\livretVerse#6 { Et vous en faites gloire. }
\livretPers Atys
\livretVerse#8 { L’amour fait trop verser de pleurs ; }
\livretVerse#8 { Souvent ses douceurs sont mortelles. }
\livretVerse#8 { Il ne faut regarder les belles }
\livretVerse#8 { Que comme on voit d’aimables fleurs. }
\livretVerse#7 { J’aime les roses nouvelles, }
\livretVerse#7 { J’aime les voir s’embellir, }
\livretVerse#7 { Sans leurs épines cruelles, }
\livretVerse#7 { J’aimerais à les cueillir. }
\livretPers Sangaride
\livretVerse#8 { Quand le péril est agréable, }
\livretVerse#8 { Le moyen de s’en alarmer ? }
\livretVerse#8 { Est-ce un grand mal de trop aimer }
\livretVerse#6 { Ce que l’on trouve aimable ? }
\livretVerse#12 { Peut-on être insensible aux plus charmants appas ? }
\livretPers Atys
\livretVerse#8 { Non vous ne me connaissez pas. }
\livretVerse#12 { Je me défends d’aimer autant qu’il m’est possible ; }
\livretVerse#8 { Si j’aimais, un jour, par malheur, }
\livretVerse#6 { Je connais bien mon cœur }
\livretVerse#6 { Il serait trop sensible. }
\livretVerse#12 { Mais il faut que chacun s’assemble près de vous, }
\livretVerse#8 { Cybèle pourrait nous surprendre. }
\livretPers Idas, Atys
\livretVerse#8 { Allons, allons, accourez tous, }
\livretVerse#6 { Cybèle va descendre. }

\livretScene SCÈNE QUATRIÈME
\livretDescAtt\wordwrap-center { Sangaride, Doris. }
\livretPers Sangaride
\livretRef#'BDAatysEstTropHeureux
\livretVerse#6 { Atys est trop heureux. }
\livretPers Doris
\livretVerse#12 { L’amitié fut toujours égale entre vous deux, }
\livretVerse#8 { Et le sang d’assez près vous lie : }
\livretVerse#12 { Quel que soit son bonheur, lui portez-vous envie ? }
\livretVerse#12 { Vous, qu’aujourd’hui l’hymen avec de si beaux nœuds }
\livretVerse#8 { Doit unir au Roi de Phrygie ? }
\livretPers Sangaride
\livretVerse#6 { Atys, est trop heureux. }
\livretVerse#12 { Souverain de son cœur, maître de tous ses vœux, }
\livretVerse#8 { Sans crainte, sans mélancolie, }
\livretVerse#12 { Il jouit en repos des beaux jours de sa vie ; }
\livretVerse#12 { Atys ne connaît point les tourments amoureux, }
\livretVerse#6 { Atys est trop heureux. }
\livretPers Doris
\livretVerse#12 { Quel mal vous fait l’Amour ? votre chagrin m’étonne. }
\livretPers Sangaride
\livretVerse#12 { Je te fie un secret qui n’est su de personne. }
\livretVerse#8 { Je devrais aimer un amant }
\livretVerse#6 { Qui m’offre une couronne ; }
\livretVerse#6 { Mais, hélas ! vainement }
\livretVerse#6 { Le Devoir me l’ordonne, }
\livretVerse#6 { L’Amour, pour mon tourment, }
\livretVerse#6 { En ordonne autrement. }
\livretPers Doris
\livretVerse#12 { Aimeriez-vous Atys, lui dont l’indifférence }
\livretVerse#12 { Brave avec tant d’orgueil l’Amour et sa puissance ? }
\livretPers Sangaride
\livretVerse#12 { J’aime, Atys, en secret, mon crime, est sans témoins. }
\livretVerse#12 { Pour vaincre mon amour, je mets tout en usage, }
\livretVerse#12 { J’appelle ma raison, j’anime mon courage ; }
\livretVerse#8 { Mais à quoi servent tous mes soins ? }
\livretVerse#8 { Mon cœur en souffre davantage, }
\livretVerse#6 { Et n’en aime pas moins. }
\livretPers Doris
\livretVerse#8 { C’est le commun défaut des belles. }
\livretVerse#8 { L’ardeur des conquêtes nouvelles }
\livretVerse#12 { Fait négliger les cœurs qu’on a trop tôt charmés, }
\livretVerse#12 { Et les indifférents sont quelquefois aimés }
\livretVerse#8 { Aux dépents des amants fidèles. }
\livretVerse#12 { Mais vous vous exposez à des peines cruelles. }
\livretPers Sangaride
\livretVerse#12 { Toujours aux yeux d’Atys je serai sans appas ; }
\livretVerse#12 { Je le sais, j’y consens, je veux, s’il est possible, }
\livretVerse#8 { Qu’il soit encor plus insensible ; }
\livretVerse#12 { S’il me pouvait aimer, que deviendrais-je ? hélas ! }
\livretVerse#12 { C’est mon plus grand bonheur qu’Atys ne m’aime pas. }
\livretVerse#12 { Je prétends être heureuse, au moins, en apparence ; }
\livretVerse#12 { Au destin d’un grand Roi je me vais attacher. }
\livretPers Sangaride, et Doris
\livretVerse#12 { Un amour malheureux dont le devoir s’offense, }
\livretVerse#8 { Se doit condamner au silence ; }
\livretVerse#12 { Un amour malheureux qu’on nous peut reprocher, }
\livretVerse#8 { Ne saurait trop bien se cacher. }

\livretScene SCÈNE CINQUIÈME
\livretDescAtt\wordwrap-center { Atys, Sangaride, Doris. }
\livretPers Atys
\livretRef#'BEArecitOneVoitDansCesCampagnes
\livretVerse#6 { On voit dans ces campagnes }
\livretVerse#8 { Tous nos Phrygiens s’avancer. }
\livretPers Doris
\livretVerse#8 { Je vais prendre soin de presser }
\livretVerse#6 { Les Nymphes nos compagnes. }

\livretScene SCÈNE SIXIÈME
\livretDescAtt\wordwrap-center { Atys, Sangaride. }
\livretPers Atys
\livretRef#'BFArecitSangarideCeJour
\livretVerse#12 { Sangaride, ce jour est un grand jour pour vous. }
\livretPers Sangaride
\livretVerse#12 { Nous ordonnons tous deux la fête de Cybèle, }
\livretVerse#8 { L’honneur est égal entre nous. }
\livretPers Atys
\livretVerse#12 { Ce jour même, un grand Roi doit être votre époux, }
\livretVerse#12 { Je ne vous vis jamais si contente et si belle ; }
\livretVerse#8 { Que le sort du Roi sera doux ! }
\livretPers Sangaride
\livretVerse#12 { L’indifférent Atys n’en sera point jaloux. }
\livretPers Atys
\livretVerse#12 { Vivez tous deux contents, c’est ma plus chère envie ; }
\livretVerse#12 { J’ai pressé votre hymen, j’ai servi vos amours. }
\livretVerse#12 { Mais enfin ce grand jour, le plus beau de vos jours, }
\livretVerse#8 { Sera le dernier de ma vie. }
\livretPers Sangaride
\livretVerse#12 { O dieux ! }
\livretPers Atys
\livretVerse#12 { \transparent { O dieux ! } Ce n’est qu’à vous que je veux révéler }
\livretVerse#12 { Le secret désespoir où mon malheur me livre ; }
\livretVerse#12 { Je n’ai que trop su feindre, il est temps de parler ; }
\livretVerse#8 { Qui n’a plus qu’un moment à vivre, }
\livretVerse#8 { N’a plus rien à dissimuler. }
\livretPers Sangaride
\livretVerse#8 { Je frémis, ma crainte est extrême ; }
\livretVerse#12 { Atys, par quel malheur faut-il vous voir périr ? }
\livretPers Atys
\livretVerse#8 { Vous me condamnerez vous même, }
\livretVerse#8 { Et vous me laisserez mourir. }
\livretPers Sangaride
\livretVerse#12 { J’armerai, s’il se faut, tout le pouvoir suprême… }
\livretPers Atys
\livretVerse#8 { Non, rien ne peut me secourir, }
\livretVerse#12 { Je meurs d’amour pour vous, je n’en saurais guérir ; }
\livretPers Sangaride
\livretVerse#12 { Quoy ? vous ? }
\livretPers Atys
\livretVerse#12 { \transparent { Quoy ? vous ? } Il est trop vrai. }
\livretPers Sangaride
\livretVerse#12 { \transparent { Quoy ? vous ? Il est trop vrai. } Vous m’aimez ? }
\livretPers Atys
\livretVerse#12 { \transparent { Quoy ? vous ? Il est trop vrai. Vous m’aimez ? } Je vous aime. }
\livretVerse#8 { Vous me condamnerez vous même, }
\livretVerse#8 { Et vous me laisserez mourir. }
\livretVerse#8 { J’ai mérité qu’on me punisse, }
\livretVerse#8 { J’offense un rival généreux, }
\livretVerse#12 { Qui par mille bienfaits a prévenu mes vœux : }
\livretVerse#12 { Mais je l’offense en vain, vous lui rendez justice ; }
\livretVerse#8 { Ah ! que c’est un cruel supplice }
\livretVerse#12 { D’avouer qu’un rival est digne d’être heureux ! }
\livretVerse#12 { Prononcez mon arrêt, parlez sans vous contraindre. }
\livretPers Sangaride
\livretVerse#12 { Hélas ! }
\livretPers Atys
\livretVerse#12 { \transparent { Hélas ! } Vous soupirez ? je vois couler vos pleurs ? }
\livretVerse#12 { D’un malheureux amour plaignez-vous les douleurs ? }
\livretPers Sangaride
\livretVerse#8 { Atys, que vous seriez à plaindre }
\livretVerse#8 { Si vous saviez tous vos malheurs ! }
\livretPers Atys
\livretVerse#8 { Si je vous perds, et si je meurs, }
\livretVerse#8 { Que puis-je encore avoir à craindre ? }
\livretPers Sangaride
\livretVerse#12 { C’est peu de perdre en moi ce qui vous a charmé, }
\livretVerse#12 { Vous me perdez, Atys, et vous êtes aimé. }
\livretPers Atys
\livretVerse#12 { Aimé ! qu’entends-je ? ô Ciel ! quel aveu favorable ! }
\livretPers Sangaride
\livretVerse#8 { Vous en serez plus misérable. }
\livretPers Atys
\livretVerse#8 { Mon malheur en est plus affreux, }
\livretVerse#12 { Le bonheur que je perds doit redoubler ma rage ; }
\livretVerse#12 { Mais n’importe, aimez-moi, s’il se peut, d’avantage, }
\livretVerse#12 { Quand j’en devrais mourir cent fois plus malheureux. }
\livretPers Sangaride
\livretVerse#12 { Si vous cherchez la mort, il faut que je vous suive ; }
\livretVerse#12 { Vivez, c’est mon amour qui vous en fait la loi. }
\livretPers Atys
\livretVerse#6 { Eh comment ! eh pourquoi }
\livretVerse#6 { Voulez-vous que je vive, }
\livretVerse#8 { Si vous ne vivez pas pour moi ? }
\livretPers Atys et Sangaride
\livretVerse#12 { Si l’hymen unissait mon destin et le vôtre, }
\livretVerse#8 { Que ses nœuds auraient eu d’attraits ! }
\livretVerse#8 { L’Amour fit nos cœurs l’un pour l’autre, }
\livretVerse#12 { Faut-il que le devoir les sépare à jamais ? }
\livretPers Atys
\livretVerse#6 { Devoir impitoyable ! }
\livretVerse#6 { Ah quelle cruauté ! }
\livretPers Sangaride
\livretVerse#12 { On vient, feignez encor, craignez d’être écouté. }
\livretPers Atys
\livretVerse#7 { Aimons un bien plus durable }
\livretVerse#7 { Que l’éclat de la beauté : }
\livretVerse#5 { Rien n’est plus aimable }
\livretVerse#5 { Que la liberté. }

\livretScene SCÈNE SEPTIÈME
\livretDescAtt\column {
  \wordwrap-center { Atys, Sangaride, Doris, Idas. }
  \justify {
    Chœur de Phrygiens chantants. Chœur de Phrygiennes chantantes.
    Troupe de Phrygiens dansants. Troupe de Phrygiennes dansantes.
  }
  \justify\italic {
    Dix hommes phrygiens chantants conduits par Atys.
    Dix femmes phrygiennes chantantes conduites par Sangaride.
    Six Phrygiens dansants. Six Nymphes phrygiennes dansantes.
  }
}
\livretPers Atys
\livretRef#'BGAairMaisDejaDeCeMontSacre
\livretVerse#8 { Mais déjà de ce mont sacré }
\livretVerse#8 { Le sommet paraît éclairé }
\livretVerse#6 { D’une splendeur nouvelle. }
\livretPersDidas Sangaride s’avançant vers la Montagne
\livretVerse#12 { La Déesse descend, allons au devant d’elle. }
\livretPers Atys et Sangaride
\livretVerse#6 { Commençons, commençons }
\livretVerse#12 { De célébrer ici sa fête solemnelle, }
\livretVerse#6 { Commençons, commençons }
\livretVerse#6 { Nos jeux et nos chansons. }
\livretDidasP\wordwrap { Les chœurs répètent ces derniers vers. }
\livretPers Atys et Sangaride
\livretVerse#12 { Il est temps que chacun fasse éclater son zèle. }
\livretVerse#8 { Venez, Reine des Dieux, venez, }
\livretVerse#8 { Venez, favorable Cybèle. }
\livretDidasP\wordwrap { Les chœurs répètent ces deux derniers vers. }
\livretPers Atys
\livretVerse#8 { Quittez votre cour immortelle, }
\livretVerse#8 { Choisissez ces lieux fortunés }
\livretVerse#8 { Pour votre demeure éternelle. }
\livretPers Les Chœurs
\livretVerse#8 { Venez, Reine des Dieux, venez, }
\livretPers Sangaride
\livretVerse#12 { La Terre sous vos pas va devenir plus belle }
\livretVerse#12 { Que le séjour des Dieux que vous abandonnez. }
\livretPers Les Chœurs
\livretVerse#8 { Venez, favorable Cybèle. }
\livretPers Atys et Sangaride
\livretVerse#12 { Venez voir les autels qui vous sont destinés. }
\livretPers Atys, Sangaride, Idas, Doris, et les chœurs
\livretVerse#8 { Écoutez un peuple fidèle }
\livretVerse#4 { Qui vous appelle, }
\livretVerse#8 { Venez Reine des Dieux, venez, }
\livretVerse#8 { Venez favorable Cybèle. }

\livretScene SCÈNE HUITIÈME
\livretDescAtt\justify {
  La Déesse Cybèle paraît sur son char, et les Phrygiens et les
  Phrygiennes lui témoignent leur joie et leur respect.
}
\livretPersDidas Cybèle sur son char
\livretRef#'BHBairChoeurVenezTousDansMonTemple
\livretVerse#12 { Venez tous dans mon temple, et que chacun révère }
\livretVerse#12 { Le sacrificateur dont je vais faire choix : }
\livretVerse#8 { Je m’expliquerai par sa voix, }
\livretVerse#12 { Les vœux qu’il m’offrira seront sûrs de me plaire. }
\livretVerse#12 { Je reçois vos respects ; j’aime à voir les honneurs }
\livretVerse#12 { Dont vous me presentez un éclatant hommage, }
\livretVerse#6 { Mais l’hommage des cœurs }
\livretVerse#8 { Est ce que j’aime davantage. }
\livretVerse#7 { Vous devez vous animer }
\livretVerse#5 { D’une ardeur nouvelle, }
\livretVerse#7 { S’il faut honorer Cybèle, }
\livretVerse#7 { Il faut encor plus l’aimer. }
\livretDidasP\justify {
  Cybèle portée par son char volant, se va rendre dans son
  temple. Tous les Phrygiens s’empressent d’y aller, et répètent les
  quatres derniers vers que la Déesse a prononcés.
}
\livretPers Les Chœurs
\livretVerse#7 { Nous devons nous animer }
\livretVerse#5 { D’une ardeur nouvelle, }
\livretVerse#7 { S’il faut honorer Cybèle, }
\livretVerse#7 { Il faut encor plus l’aimer. }
\sep
\livretAct ACTE SECOND
\livretDescAtt\wordwrap-center {
  Le Théâtre change et représente Le temple de Cybèle.
}
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\wordwrap-center {
  Célénus roi de Phrygie. Atys, Suivants de Célénus.
}
\livretPers Célénus
\livretRef#'CABrecitNavancezPasPlusLoin
\livretVerse#12 { N’avancez pas plus loin, ne suivez point mes pas ; }
\livretVerse#8 { Sortez. Toi ne me quitte pas. }
\livretVerse#12 { Atys, il faut attendre ici que la Déesse }
\livretVerse#8 { Nomme un grand sacrificateur. }
\livretPers Atys
\livretVerse#12 { Son choix sera pour vous, seigneur ; quelle tristesse }
\livretVerse#8 { Semble avoir surpris votre cœur ? }
\livretPers Célénus
\livretVerse#12 { Les rois les plus puissants connaissent l’importance }
\livretVerse#6 { D’un si glorieux choix : }
\livretVerse#12 { Qui pourra l’obtenir étendra sa puissance }
\livretVerse#12 { Partout où de Cybèle on révère les lois. }
\livretPers Atys
\livretVerse#12 { Elle honore aujourd’hui ces lieux de sa présence, }
\livretVerse#12 { C’est pour vous préférer aux plus puissants des Rois. }
\livretPers Célénus
\livretVerse#12 { Mais quand j’ai vu tantôt la beauté qui m’enchante, }
\livretVerse#12 { N’as-tu point remarqué comme elle était tremblante ? }
\livretPers Atys
\livretVerse#12 { À nos jeux, à nos chants, j’étais trop appliqué, }
\livretVerse#12 { Hors la fête, Seigneur, je n’ai rien remarqué. }
\livretPers Célénus
\livretVerse#12 { Son trouble m’a surpris. Elle t’ouvre son âme ; }
\livretVerse#12 { N’y découvres-tu point quelque secrète flamme ? }
\livretVerse#12 { Quelque rival caché ? }
\livretPers Atys
\livretVerse#12 { \transparent { Quelque rival caché ? } Seigneur, que dites-vous ? }
\livretPers Célénus
\livretVerse#12 { Le seul nom de rival allume mon couroux. }
\livretVerse#12 { J’ai bien peur que le Ciel n’ait pu voir sans envie }
\livretVerse#6 { Le bonheur de ma vie, }
\livretVerse#12 { Et si j’étais aimé mon sort serait trop doux. }
\livretVerse#12 { Ne t’étonne point tant de voir la jalousie }
\livretVerse#6 { Dont mon âme est saisie }
\livretVerse#12 { On ne peut bien aimer sans être un peu jaloux. }
\livretPers Atys
\livretVerse#12 { Seigneur, soyez content, que rien ne vous alarme ; }
\livretVerse#12 { L’hymen va vous donner la beauté qui vous charme, }
\livretVerse#8 { Vous serez son heureux époux. }
\livretPers Célénus
\livretVerse#12 { Tu peux me rassurer, Atys, je te veux croire, }
\livretVerse#8 { C’est son cœur que je veux avoir, }
\livretVerse#8 { Dis-moi s’il est en mon pouvoir ? }
\livretPers Atys
\livretVerse#12 { Son cœur suit avec soin le devoir et la gloire, }
\livretVerse#12 { Et vous avez pour vous la gloire et le devoir. }
\livretPers Célénus
\livretVerse#12 { Ne me déguise point ce que tu peux connaître. }
\livretVerse#8 { Si j’ai ce que j’aime en ce jour }
\livretVerse#8 { L’hymen seul m’en rend-il le maître ? }
\livretVerse#12 { La gloire et le devoir auront tout fait, peut-être, }
\livretVerse#12 { Et ne laissent pour moi rien à faire à l’amour. }
\livretPers Atys
\livretVerse#12 { Vous aimez d’un amour trop délicat, trop tendre. }
\livretPers Célénus
\livretVerse#12 { L’indifférent Atys ne le saurait comprendre. }
\livretPers Atys
\livretVerse#8 { Qu’un indifférent est heureux ! }
\livretVerse#8 { Il jouit d’un destin paisible. }
\livretVerse#12 { Le ciel fait un présent bien cher, bien dangeureux, }
\livretVerse#8 { Lorsqu’il donne un cœur trop sensible. }
\livretPers Célénus
\livretVerse#8 { Quand on aime bien tendrement }
\livretVerse#12 { On ne cesse jamais de souffrir, et de craindre ; }
\livretVerse#8 { Dans le bonheur le plus charmant, }
\livretVerse#12 { On est ingénieux à se faire un tourment, }
\livretVerse#8 { Et l’on prend plaisir à se plaindre. }
\livretVerse#12 { Va songe à mon hymen, et vois si tout est prêt, }
\livretVerse#12 { Laisse-moi seul ici, la Déesse paraît. }

\livretScene SCÈNE SECONDE
\livretDescAtt\wordwrap-center {
  Cybèle, Célénus, Mélisse, troupe de prêtresses de Cybèle.
}
\livretPers Cybèle
\livretRef#'CBBrecitJeVeuxJoindreEnCesLieux
\livretVerse#12 { Je veux joindre en ces lieux la gloire et l’abondance, }
\livretVerse#12 { D’un sacrificateur je veux faire le choix, }
\livretVerse#12 { Et le Roi de Phrygie aurait la préférence }
\livretVerse#12 { Si je voulais choisir entre les plus grands rois. }
\livretVerse#12 { Le puissant dieu des flots vous donna la naissance, }
\livretVerse#12 { Un peuple renommé s’est mis sous votre loi ; }
\livretVerse#12 { Vous avez sans mon choix, d’ailleurs, trop de puissance, }
\livretVerse#12 { Je veux faire un bonheur qui ne soit dû qu’à moi. }
\livretVerse#12 { Vous estimez Atys, et c’est avec justice, }
\livretVerse#12 { Je prétends que mon choix à vos vœux soit propice, }
\livretVerse#8 { C’est Atys que je veux choisir. }
\livretPers Célénus
\livretVerse#12 { J’aime Atys, et je vois sa gloire avec plaisir. }
\livretVerse#8 { Je suis Roi, Neptune est mon père, }
\livretVerse#12 { J’épouse une beauté qui va combler mes vœux : }
\livretVerse#8 { Le souhait qui me reste à faire, }
\livretVerse#12 { C’est de voir mon ami parfaitement heureux. }
\livretPers Cybèle
\livretVerse#12 { Il m’est doux que mon choix à vos désirs réponde ; }
\livretVerse#8 { Une grande divinité }
\livretVerse#8 { Doit faire sa félicité }
\livretVerse#6 { Du bien de tout le monde. }
\livretVerse#12 { Mais surtout le bonheur d’un roi chéri des cieux }
\livretVerse#8 { Fait le plus doux plaisir des Dieux. }
\livretPers Célénus
\livretVerse#12 { Le sang approche Atys de la Nymphe que j’aime, }
\livretVerse#8 { Son mérite l’égale aux rois : }
\livretVerse#8 { Il soutiendra mieux que moi-même }
\livretVerse#6 { La majesté suprême }
\livretVerse#6 { De vos divines lois. }
\livretVerse#8 { Rien ne pourra troubler son zèle, }
\livretVerse#12 { Son cœur s’est conservé libre jusqu’à ce jour ; }
\livretVerse#8 { Il faut tout un cœur pour Cybèle, }
\livretVerse#12 { À peine tout le mien peut suffire à l’amour. }
\livretPers Cybèle
\livretVerse#12 { Portez à votre ami la première nouvelle }
\livretVerse#12 { De l’honneur éclatant où ma faveur l’appelle. }

\livretScene SCÈNE TROISIÈME
\livretDescAtt\wordwrap-center { Cybèle, Mélisse. }
\livretPers Cybèle
\livretRef#'CCArecitTuTetonnesMelisse
\livretVerse#12 { Tu t’étonnes, Mélisse, et mon choix te surprend ? }
\livretPers Mélisse
\livretVerse#12 { Atys vous doit beaucoup, et son bonheur est grand. }
\livretPers Cybèle
\livretVerse#12 { J’ai fait encor pour lui plus que tu ne peux croire. }
\livretPers Mélisse
\livretVerse#12 { Est-il pour un mortel un rang plus glorieux ? }
\livretPers Cybèle
\livretVerse#8 { Tu ne vois que sa moindre gloire ; }
\livretVerse#12 { Ce mortel dans mon cœur est au dessus des Dieux. }
\livretVerse#12 { Ce fut au jour fatal de ma dernière fête }
\livretVerse#12 { Que de l’aimable Atys je devins la conquête : }
\livretVerse#12 { Je partis à regret pour retourner aux Cieux, }
\livretVerse#12 { Tout m’y parut changé, rien n’y plut à mes yeux. }
\livretVerse#7 { Je sens un plaisir extrême }
\livretVerse#7 { À revenir dans ces lieux ; }
\livretVerse#8 { Où peut-on jamais être mieux, }
\livretVerse#8 { Qu’aux lieux où l’on voit ce qu’on aime. }
\livretPers Mélisse
\livretVerse#12 { Tous les Dieux ont aimé, Cybèle aime à son tour. }
\livretVerse#7 { Vous méprisiez trop l’Amour, }
\livretVerse#7 { Son nom vous semblait étrange, }
\livretVerse#7 { À la fin il vient un jour }
\livretVerse#5 { Où l’Amour se venge. }
\livretPers Cybèle
\livretVerse#12 { J’ai crû me faire un cœur maître de tout son sort, }
\livretVerse#12 { Un cœur toujours exempt de trouble et de tendresse. }
\livretPers Mélisse
\livretVerse#5 { Vous braviez à tort }
\livretVerse#5 { L’Amour qui vous blesse ; }
\livretVerse#5 { Le cœur le plus fort }
\livretVerse#7 { À des moments de faiblesse. }
\livretVerse#12 { Mais vous pouviez aimer, et descendre moins bas. }
\livretPers Cybèle
\livretVerse#12 { Non, trop d’égalité rend l’amour sans appas. }
\livretVerse#8 { Quel plus haut rang ai-je à prétendre ? }
\livretVerse#12 { Et de quoi mon pouvoir ne vient-il point à bout ? }
\livretVerse#8 { Lorsqu’on est au dessus de tout, }
\livretVerse#12 { On se fait pour aimer un plaisir de descendre. }
\livretVerse#12 { Je laisse aux Dieux les biens dans le Ciel preparés, }
\livretVerse#12 { Pour Atys, pour son cœur, je quitte tout sans peine, }
\livretVerse#12 { S’il m’oblige à descendre, un doux penchant m’entraîne ; }
\livretVerse#12 { Les cœurs que le destin à le plus separés, }
\livretVerse#12 { Sont ceux qu’Amour unit d’une plus forte chaîne. }
\livretVerse#12 { Fais venir le Sommeil ; que lui-même en ce jour, }
\livretVerse#8 { Prenne soin ici de conduire }
\livretVerse#8 { Les Songes qui lui font la cour ; }
\livretVerse#8 { Atys ne sait point mon amour, }
\livretVerse#12 { Par un moyen nouveau je prétends l’en instruire. }
\livretDidasP Mélisse se retire.
\livretPers Cybèle
\livretVerse#12 { Que les plus doux Zéphirs, que les peuples divers, }
\livretVerse#8 { Qui des deux bouts de l’Univers }
\livretVerse#8 { Sont venus me montrer leur zèle, }
\livretVerse#8 { Célèbrent la gloire immortelle }
\livretVerse#12 { Du sacrificateur dont Cybèle a fait choix, }
\livretVerse#8 { Atys doit dispenser mes lois, }
\livretVerse#8 { Honorez le choix de Cybèle. }

\livretScene SCÈNE QUATRIÈME
\livretDescAtt\column {
  \justify {
    Les Zéphirs paraissent dans une gloire élevée et brillante. Les
    Peuples différents qui sont venus à la fête de Cybèle entrent dans
    le temple, et tous ensemble s’efforcent d’honorer Atys, qui vient
    revêtu des habits de grand sacrificateur.
  }
  \justify\italic {
    Cinq Zéphirs dansants dans la gloire.
    Huit Zéphirs jouants du hautbois et des cromornes, dans la gloire.
    Troupe de Peuples différents chantants qui accompagnent Atys.
    Six Indiens et six Egyptiens dansants.
  }
}
\livretPers Chœurs des Peuples et des Zéphirs
\livretRef#'CDAchoeurCelebronsLaGloireImmortelle
\livretVerse#8 { Célébrons la gloire immortelle }
\livretVerse#12 { Du sacrificateur dont Cybèle a fait choix : }
\livretVerse#8 { Atys doit dispenser ses lois, }
\livretVerse#8 { Honorons le choix de Cybèle. }
\null
\livretRef#'CDDchoeurQueDevantVous
\livretVerse#10 { Que devant vous tout s’abaisse, et tout tremble ; }
\livretVerse#10 { Vivez heureux, vos jours sont notre espoir : }
\livretVerse#9 { Rien n’est si beau que de voir ensemble }
\livretVerse#10 { Un grand mérite avec un grand pouvoir. }
\livretVerse#4 { Que l’on bénisse }
\livretVerse#4 { Le Ciel propice, }
\livretVerse#4 { Qui dans vos mains }
\livretVerse#6 { Met le sort des humains. }
\livretPers Atys
\livretRef#'CDEindigneQueJeSuis
\livretVerse#12 { Indigne que je suis des honneurs qu’on m’adresse, }
\livretVerse#12 { Je dois les recevoir au nom de la Déesse ; }
\livretVerse#12 { J’ose, puisqu’il lui plaît, lui présenter vos vœux : }
\livretVerse#7 { Pour le prix de votre zèle, }
\livretVerse#7 { Que la puissante Cybèle }
\livretVerse#7 { Vous rende à jamais heureux. }
\livretPers \line { Chœurs des Peuples et des Zephirs. }
\livretRef#'CDFchoeurQueLaPuissanteCybele
\livretVerse#7 { Que la puissante Cybèle }
\livretVerse#7 { Nous rende à jamais heureux. }
\sep

\livretAct ACTE TROISIÈME
\livretDescAtt\wordwrap-center {
  Le Théâtre change et répresente le palais du sacrificateur de
  Cybèle.
}
\livretScene SCÈNE PREMIÈRE
\livretPersDidas Atys seul
\livretRef#'DAArecitQueServentLesFaveurs
\livretVerse#12 { Que servent les faveurs que nous fait la fortune }
\livretVerse#8 { Quand l’Amour nous rend malheureux ? }
\livretVerse#12 { Je perds l’unique bien qui peut combler mes vœux, }
\livretVerse#8 { Et tout autre bien m’importune. }
\livretVerse#12 { Que servent les faveurs que nous fait la fortune }
\livretVerse#8 { Quand l’Amour nous rend malheureux ? }

\livretScene SCÈNE SECONDE
\livretDescAtt\wordwrap-center { Idas, Doris, Atys. }
\livretPers Idas
\livretRef#'DBArecitPeutOnIciParlerSansFeindre
\livretVerse#8 { Peut-on ici parler sans feindre ? }
\livretPers Atys
\livretVerse#12 { Je commande en ces lieux, vous n’y devez rien craindre. }
\livretPers Doris
\livretVerse#12 { Mon frère est votre ami. }
\livretPers Idas
\livretVerse#12 { \transparent { Mon frère est votre ami. } Fiez-vous à ma sœur. }
\livretPers Atys
\livretVerse#12 { Vous devez avec moi partager mon bonheur. }
\livretPers Idas et Doris
\livretVerse#12 { Nous venons partager vos mortelles alarmes ; }
\livretVerse#8 { Sangaride les yeux en larmes }
\livretVerse#6 { Nous vient d’ouvrir son cœur. }
\livretPers Atys
\livretVerse#12 { L’heure approche où l’hymen voudra qu’elle se livre }
\livretVerse#8 { Au pouvoir d’un heureux époux. }
\livretPers Idas et Doris
\livretVerse#5 { Elle ne peut vivre }
\livretVerse#7 { Pour un autre que pour vous. }
\livretPers Atys
\livretVerse#12 { Qui peut la dégager du devoir qui la presse ? }
\livretPers Idas et Doris
\livretVerse#12 { Elle veut elle-même aux pieds de la Déesse }
\livretVerse#12 { Déclarer hautement vos secrètes amours. }
\livretPers Atys
\livretVerse#8 { Cybèle pour moi s’intéresse, }
\livretVerse#12 { J’ose tout espérer de son divin secours… }
\livretVerse#12 { Mais quoi, trahir le roi ! tromper son espérance ! }
\livretVerse#12 { De tant de biens reçus est-ce la récompense ? }
\livretPers Idas et Doris
\livretVerse#6 { Dans l’empire amoureux }
\livretVerse#8 { Le Devoir n’a point de puissance ; }
\livretVerse#4 { L’Amour dispense }
\livretVerse#8 { Les rivaux d’être généreux ; }
\livretVerse#10 { Il faut souvent pour devenir heureux }
\livretVerse#8 { Qu’il en coûte un peu d’innocence. }
\livretPers Atys
\livretVerse#12 { Je souhaite, je crains, je veux, je me repens. }
\livretPers Idas et Doris
\livretVerse#12 { Verrez-vous un rival heureux à vos dépens ? }
\livretPers Atys
\livretVerse#12 { Je ne puis me résoudre à cette violence. }
\livretPers Atys, Idas et Doris
\livretVerse#10 { En vain, un cœur, incertain de son choix, }
\livretVerse#8 { Met en balance mille fois }
\livretVerse#8 { L’Amour et la Reconnaissance, }
\livretVerse#10 { L’Amour toujours emporte la balance. }
\livretPers Atys
\livretVerse#12 { Le plus juste parti cède enfin au plus fort. }
\livretVerse#8 { Allez, prenez soin de mon sort, }
\livretVerse#12 { Que Sangaride ici se rende en diligence. }

\livretScene SCÈNE TROISIÈME
\livretPers \line { ATYS seul. }
\livretRef#'DCArecitNousPouvonsNousFlatter
\livretVerse#12 { Nous pouvons nous flatter de l’espoir le plus doux }
\livretVerse#8 { Cybèle et l’Amour sont pour nous. }
\livretVerse#12 { Mais du devoir trahi j’entends la voix pressante }
\livretVerse#8 { Qui m’accuse et qui m’épouvante. }
\livretVerse#12 { Laisse-mon cœur en paix, impuissante Vertu, }
\livretVerse#8 { N’ai-je point assez combattu ? }
\livretVerse#12 { Quand l’Amour malgré toi me contraint à me rendre, }
\livretVerse#6 { Que me demandes-tu ? }
\livretVerse#8 { Puisque tu ne peux me défendre, }
\livretVerse#6 { Que me sert-il d’entendre }
\livretVerse#8 { Les vains repproches que tu fais ? }
\livretVerse#12 { Impuissante Vertu laisse mon cœur en paix. }
\livretVerse#8 { Mais le Sommeil vient me surprendre, }
\livretVerse#12 { Je combats vainement sa charmante douceur. }
\livretVerse#6 { Il faut laisser suspendre }
\livretVerse#6 { Les troubles de mon cœur. }
\livretDidasP Atys descend.

\livretScene SCÈNE QUATRIÈME
\livretDescAtt \justify {
  Le théâtre change et représente un antre entouré de pavots et de
  ruisseaux, où le dieu du sommeil se vient rendre accompagné des
  songes agréables et funestes.
}
\livretDescAtt \column {
  \wordwrap-center {
    Atys dormant. Le Sommeil, Morphée, Phobetor, Phantase,
    Les songes heureux. Les songes funestes.
  }
  \justify\italic {
    Deux songes jouants de la viole.  Deux songes jouants du théorbe.
    Six songes jouants de la Flûte.  Douze songes funestes chantants.
    Huit songes agréables dansants.  Huit Songes funestes dansants.
  }
}
\livretPers Le Sommeil
\livretRef#'DDAdormons
\livretVerse#5 { Dormons, dormons tous ; }
\livretVerse#7 { Ah que le repos est doux ! }
\livretPers Morphée
\livretVerse#12 { Régnez, divin Sommeil, régnez sur tout le monde, }
\livretVerse#12 { Répandez vos pavots les plus assoupissants ; }
\livretVerse#8 { Calmez les soins, charmez les sens, }
\livretVerse#12 { Retenez tous les cœurs dans une paix profonde. }
\livretPers Phobétor
\livretVerse#8 { Ne vous faites point violence, }
\livretVerse#8 { Coulez, murmurez, clairs ruisseaux, }
\livretVerse#8 { Il n’est permis qu’au bruit des eaux }
\livretVerse#12 { De troubler la douceur d’un si charmant silence. }
\livretPers Le Sommeil, Morphée, Phobétor et Phantase
\livretVerse#5 { Dormons, dormons tous, }
\livretVerse#7 { Ah que le repos est doux ! }
\livretDidasP\justify {
  Les songes agréables approchent d’Atys, et par leurs chants, et par
  leurs danses, lui font connaître l’amour de Cybèle, et le bonheur
  qu’il en doit espérer.
}
\livretPers Morphée
\livretRef#'DDBecouteAtys
\livretVerse#12 { Écoute, écoute Atys la gloire qui t’appelle, }
\livretVerse#12 { Sois sensible à l’honneur d’être aimé de Cybèle, }
\livretVerse#12 { Jouis heureux Atys de ta félicité. }
\livretPers Morphée, Phobétor et Phantase
\livretVerse#8 { Mais souviens-toi que la beauté }
\livretVerse#6 { Quand elle est immortelle, }
\livretVerse#8 { Demande la fidélité }
\livretVerse#6 { D’une amour éternelle. }
\livretPers Phantase
\livretVerse#6 { Que l’amour a d’attraits }
\livretVerse#4 { Lorsqu’il commence }
\livretVerse#8 { À faire sentir sa puissance, }
\livretVerse#6 { Que l’Amour a d’attraits }
\livretVerse#4 { Lorsqu’il commence }
\livretVerse#6 { Pour ne finir jamais. }
\livretPers Morphée
\livretRef#'DDDgouteEnPaix
\livretVerse#12 { Goûte en paix chaque jour une douceur nouvelle, }
\livretVerse#12 { Partage l’heureux sort d’une divinité, }
\livretVerse#8 { Ne vante plus la liberté, }
\livretVerse#12 { Il n’en est point du prix d’une chaîne si belle : }
\livretPers Morphée, Phobétor et Phantase
\livretVerse#8 { Mais souviens-toi que la beauté }
\livretVerse#6 { Quand elle est immortelle, }
\livretVerse#8 { Demande la fidélité }
\livretVerse#6 { D’une amour éternelle. }
\livretPers Phantase
\livretVerse#6 { Trop heureux un amant }
\livretVerse#4 { Qu’Amour exempte }
\livretVerse#8 { Des peines d’une longue attente ! }
\livretVerse#6 { Trop heureux un amant }
\livretVerse#4 { Qu’Amour exempte }
\livretVerse#6 { De crainte, et de tourment ! }
\livretDidasP\justify {
  Les songes funestes approchent d’Atys, et le menacent de la
  vengeance de Cybèle s’il méprise son amour, et s’il ne l'aime pas
  avec fidélité.
}
\livretPers Un songe funeste
\livretRef#'DDFgardeToiDoffenser
\livretVerse#12 { Garde-toi d’offenser un amour glorieux, }
\livretVerse#12 { C’est pour toi que Cybèle abandonne les cieux }
\livretVerse#8 { Ne trahis point son espérance. }
\livretVerse#12 { Il n’est point pour les Dieux de mépris innocent, }
\livretVerse#12 { Ils sont jaloux des cœurs, ils aiment la vengeance, }
\livretVerse#8 { Il est dangereux qu’on offense }
\livretVerse#6 { Un amour tout-puissant. }
\livretPers Chœur de songes funestes
\livretRef#'DDHchoeurLamourQuonOutrage
\livretVerse#5 { L’amour qu’on outrage }
\livretVerse#5 { Se transforme en rage, }
\livretVerse#6 { Et ne pardonne pas }
\livretVerse#6 { Aux plus charmants appas. }
\livretVerse#7 { Si tu n’aimes point Cybèle }
\livretVerse#5 { D’une amour fidèle, }
\livretVerse#8 { Malheureux, que tu souffriras ! }
\livretVerse#4 { Tu périras : }
\livretVerse#8 { Crains une vengeance cruelle, }
\livretVerse#8 { Tremble, crains un affreux trépas. }
\livretDidasP\justify {
  Atys épouvanté par les songes funestes, se réveille en sursaut, le
  Sommeil et les songes disparaissent avec l'antre où ils étaient, et
  Atys se retrouve dans le même palais où il s’était endormi.
}

\livretScene SCÈNE CINQUIÈME
\livretDescAtt\wordwrap-center { Atys, Cybèle, et Mélisse. }
\livretPers Atys
\livretRef#'DEArecitVenezAMonSecours
\livretVerse#12 { Venez à mon secours ô dieux ! ô justes dieux ! }
\livretPers Cybèle
\livretVerse#12 { Atys, ne craignez rien, Cybèle est en ces lieux. }
\livretPers Atys
\livretVerse#12 { Pardonnez au désordre où mon cœur s’abandonne ; }
\livretVerse#12 { C’est un songe… }
\livretPers Cybèle
\livretVerse#12 { \transparent { C’est un songe… } Parlez, quel songe vous étonne ? }
\livretVerse#8 { Expliquez-moi votre embarras. }
\livretPers Atys
\livretVerse#12 { Les songes sont trompeurs, et je ne les crois pas. }
\livretVerse#6 { Les plaisirs et les peines }
\livretVerse#8 { Dont en dormant on est séduit, }
\livretVerse#6 { Sont des chimères vaines }
\livretVerse#6 { Que le réveil détruit. }
\livretPers Cybèle
\livretVerse#8 { Ne méprisez pas tant les songes }
\livretVerse#8 { L’Amour peut emprunter leur voix, }
\livretVerse#7 { S’ils font souvent des mensonges }
\livretVerse#7 { Ils disent vrai quelquefois. }
\livretVerse#12 { Ils parlaient par mon ordre, et vous les devez croire. }
\livretPers Atys
\livretVerse#12 { O Ciel ? }
\livretPers Cybèle
\livretVerse#12 { \transparent { O Ciel ? } N’en doutez point, connaissez votre gloire. }
\livretVerse#8 { Répondez avec liberté, }
\livretVerse#12 { Je vous demande un cœur qui dépend de lui-même. }
\livretPers Atys
\livretVerse#8 { Une grande divinité }
\livretVerse#12 { Doit d’assurer toujours de mon respect extrême. }
\livretPers Cybèle
\livretVerse#8 { Les dieux dans leur grandeur suprême }
\livretVerse#12 { Reçoivent tant d’honneurs qu’ils en sont rebutés, }
\livretVerse#12 { Ils se lassent souvent d’être trop respectés, }
\livretVerse#8 { Ils sont plus contents qu’on les aime. }
\livretPers Atys
\livretVerse#8 { Je sais trop ce que je vous dois }
\livretVerse#8 { Pour manquer de reconnaissance… }

\livretScene SCÈNE SIXIÈME
\livretDescAtt\wordwrap-center { Sangaride, Cybèle, Atys, Mélisse. }
\livretPersDidas Sangaride se jetant aux pieds de Cybèle
\livretRef#'DFArecitJaiRecoursAVotrePuissance
\livretVerse#8 { J’ai recours à votre puissance, }
\livretVerse#8 { Reine des Dieux, protégez-moi. }
\livretVerse#8 { L’intérêt d’Atys vous en presse… }
\livretPersDidas Atys interrompant Sangaride
\livretVerse#12 { Je parlerai pour vous, que votre crainte cesse. }
\livretPers Sangaride
\livretVerse#8 { Tous deux unis des plus beaux nœuds… }
\livretPersDidas Atys interrompant Sangaride
\livretVerse#12 { Le sang et l’amitié nous unissent tous deux. }
\livretVerse#8 { Que votre secours la délivre }
\livretVerse#8 { Des lois d’un hymen rigoureux, }
\livretVerse#8 { Ce sont les plus doux de ses vœux }
\livretVerse#12 { De pouvoir à jamais vous servir et vous suivre. }
\livretPers Cybèle
\livretVerse#7 { Les dieux sont les protecteurs }
\livretVerse#7 { De la liberté des cœurs. }
\livretVerse#12 { Allez, ne craignez point le roi ni sa colère, }
\livretVerse#6 { J’aurai soin d’apaiser }
\livretVerse#8 { Le Fleuve Sangar votre père ; }
\livretVerse#8 { Atys veut vous favoriser, }
\livretVerse#12 { Cybèle en sa faveur ne peut rien refuser. }
\livretPers Atys
\livretVerse#12 { Ah ! c’en est trop… }
\livretPers Cybèle
\livretVerse#12 { \transparent { Ah ! c’en est trop… } Non, non, il n’est pas nécessaire }
\livretVerse#8 { Que vous cachiez votre bonheur, }
\livretVerse#6 { Je ne prétends point faire }
\livretVerse#4 { Un vain mystère }
\livretVerse#8 { D’un amour qui vous fait honneur. }
\livretVerse#12 { Ce n’est point à Cybèle à craindre d’en trop dire. }
\livretVerse#12 { Il est vrai, j’aime Atys, pour lui j’ai tout quitté, }
\livretVerse#12 { Sans lui je ne veux plus de grandeur ni d’empire, }
\livretVerse#6 { Pour ma félicité }
\livretVerse#6 { Son cœur seul peut suffire. }
\livretVerse#12 { Allez, Atys lui-même ira vous garantir }
\livretVerse#8 { De la fatale violence }
\livretVerse#8 { Où vous ne pouvez consentir. }
\livretDidasP\line { Sangaride se retire. }
\livretPersDidas Cybèle parle à Atys
\livretVerse#12 { Laissez-nous, attendez mes ordres pour partir, }
\livretVerse#12 { Je prétends vous armer de ma toute puissance. }

\livretScene SCÈNE SEPTIÈME
\livretDescAtt\wordwrap-center { Cybèle, Mélisse. }
\livretPers Cybèle
\livretRef#'DGArecitQuAtysDansSesRespects
\livretVerse#12 { Qu’Atys dans ses respects mêle d’indifférence ! }
\livretVerse#8 { L’ingrat Atys ne m’aime pas ; }
\livretVerse#12 { L’Amour veut de l’amour, tout autre prix l’offense, }
\livretVerse#12 { Et souvent le respect et la reconnaissance }
\livretVerse#8 { Sont l’excuse des cœurs ingrats. }
\livretPers Mélisse
\livretVerse#7 { Ce n’est pas un si grand crime }
\livretVerse#7 { De ne s’exprimer pas bien, }
\livretVerse#8 { Un cœur qui n’aima jamais rien }
\livretVerse#8 { Sait peu comment l’amour s’exprime. }
\livretPers Cybèle
\livretVerse#12 { Sangaride est aimable, Atys peut tout charmer, }
\livretVerse#8 { Ils témoignent trop s’estimer, }
\livretVerse#12 { Et de simples parents sont moins d’intelligence : }
\livretVerse#8 { Ils se sont aimés dès l’enfance, }
\livretVerse#8 { Ils pourraient enfin trop s’aimer. }
\livretVerse#12 { Je crains une amitié que tant d’ardeur anime. }
\livretVerse#8 { Rien n’est si trompeur que l’estime : }
\livretVerse#6 { C’est un nom supposé }
\livretVerse#12 { Qu’on donne quelquefois à l’amour déguisé. }
\livretVerse#12 { Je prétends m’éclaircir leur feinte sera vaine. }
\livretPers Mélisse
\livretVerse#12 { Quels secrets par les dieux ne sont point pénétrés ? }
\livretVerse#8 { Deux cœurs à feindre préparés }
\livretVerse#6 { Ont beau cacher leur chaîne, }
\livretVerse#6 { On abuse avec peine }
\livretVerse#8 { Les Dieux par l’amour éclairés. }
\livretPers Cybèle
\livretVerse#12 { Va, Mélisse, donne ordre à l’aimable Zéphire }
\livretVerse#12 { D’accomplir promptement tout ce qu’Atys désire. }

\livretScene SCÈNE HUITIÈME
\livretPersDidas Cybèle seule
\livretRef#'DHAespoirSiCherEtSiDoux
\livretVerse#7 { Espoir si cher, et si doux, }
\livretVerse#7 { Ah ! pourquoi me trompez-vous ? }
\livretVerse#12 { Des suprêmes grandeurs vous m’avez fait descendre, }
\livretVerse#12 { Mille cœurs m’adoraient, je les néglige tous, }
\livretVerse#12 { Je n’en demande qu’un, il a peine à se rendre ; }
\livretVerse#12 { Je ne sens que chagrin, et que soupçons jaloux ; }
\livretVerse#12 { Est-ce le sort charmant que je devais attendre ? }
\livretVerse#7 { Espoir si cher, et si doux, }
\livretVerse#7 { Ah ! pourquoi me trompez-vous ? }
\livretVerse#12 { Hélas ! par tant d’attraits fallait-il me surprendre ? }
\livretVerse#12 { Heureuse, si toujours j’avais pu m’en défendre ! }
\livretVerse#12 { L’Amour qui me flattait me cachait son couroux : }
\livretVerse#12 { C’est donc pour me frapper des plus funestes coups, }
\livretVerse#12 { Que le cruel Amour m’a fait un cœur si tendre ? }
\livretVerse#7 { Espoir si cher, et si doux, }
\livretVerse#7 { Ah ! pourquoi me trompez-vous ? }
\sep
\livretAct ACTE QUATRIÈME
\livretDescAtt\wordwrap-center {
  Le théâtre change et représente le palais du Fleuve Sangar.
}
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\wordwrap-center { Sangaride, Doris, Idas. }
\livretPers Doris
\livretRef#'EAArecitQuoiVousPleurez
\livretVerse#12 { Quoi, vous pleurez ? }
\livretPers Idas
\livretVerse#12 { \transparent { Quoi, vous pleurez ? } D’où vient votre peine nouvelle ? }
\livretPers Doris
\livretVerse#12 { N’osez-vous découvrir votre amour à Cybèle ? }
\livretPers Sangaride
\livretVerse#12 { Hélas ! }
\livretPers Doris et Idas
\livretVerse#12 { \transparent { Hélas ! } Qui peut encor redoubler vos ennuis ? }
\livretPers Sangaride
\livretVerse#13 { Hélas ! j’aime… hélas ! j’aime… }
\livretPers Doris et Idas
\livretVerse#13 { \transparent { Hélas ! j’aime… hélas ! j’aime… } Achevez. }
\livretPers Sangaride
\livretVerse#13 { \transparent { Hélas ! j’aime… hélas ! j’aime… Achevez. } Je ne puis. }
\livretPers Doris et Idas
\livretVerse#12 { L’Amour n’est guère heureux lorsqu’il est trop timide. }
\livretPers Sangaride
\livretVerse#6 { Hélas ! j’aime un perfide }
\livretVerse#6 { Qui trahit mon amour ; }
\livretVerse#12 { La Déesse aime Atys, il change en moins d’un jour, }
\livretVerse#12 { Atys comblé d’honneurs n’aime plus Sangaride. }
\livretVerse#6 { Hélas ! j’aime un perfide }
\livretVerse#6 { Qui trahit mon amour. }
\livretPers Doris et Idas
\livretVerse#12 { Il nous montrait tantôt un peu d’incertitude ; }
\livretVerse#12 { Mais qui l’eut soupçonné de tant d’ingratitude ? }
\livretPers Sangaride
\livretVerse#12 { J’embarassais Atys, je l’ai vu se troubler : }
\livretVerse#8 { Je croyais devoir révéler }
\livretVerse#6 { Notre amour à Cybèle ; }
\livretVerse#6 { Mais l’ingrat, l’infidèle, }
\livretVerse#8 { M’empechait toujours de parler. }
\livretPers Doris et Idas
\livretVerse#12 { Peut-on changer si tôt quand l’amour est extrême ? }
\livretVerse#6 { Gardez-vous, gardez-vous }
\livretVerse#8 { De trop croire un transport jaloux. }
\livretPers Sangaride
\livretVerse#12 { Cybèle hautement déclare qu’elle l’aime, }
\livretVerse#12 { Et l’ingrat n’a trouvé cet honneur que trop doux ; }
\livretVerse#12 { Il change en un moment, je veux changer de même, }
\livretVerse#12 { J’accepterai sans peine un glorieux époux, }
\livretVerse#12 { Je ne veux plus aimer que la grandeur suprême. }
\livretPers Doris et Idas
\livretVerse#12 { Peut-on changer si tôt quand l’amour est extrême ? }
\livretVerse#6 { Gardez-vous, gardez-vous }
\livretVerse#8 { De trop croire un transport jaloux. }
\livretPers Sangaride
\livretVerse#8 { Trop heureux un cœur qui peut croire }
\livretVerse#8 { Un dépit qui sert à sa gloire. }
\livretVerse#12 { Revenez ma raison, revenez pour jamais, }
\livretVerse#12 { Joignez-vous au dépit pour étouffer ma flamme, }
\livretVerse#12 { Réparez, s’il se peut, les maux qu’Amour m’a faits, }
\livretVerse#8 { Venez rétablir dans mon âme }
\livretVerse#8 { Les douceurs d’une heureuse paix ; }
\livretVerse#12 { Revenez, ma raison, revenez pour jamais. }
\livretPers Idas et Doris
\livretVerse#8 { Une infidelité cruelle }
\livretVerse#8 { N’efface point tous les appas }
\livretVerse#4 { D’un infidèle, }
\livretVerse#8 { Et la raison ne revient pas }
\livretVerse#6 { Si tôt qu’on l’a rappelle. }
\livretPers Sangaride
\livretVerse#7 { Après une trahison }
\livretVerse#7 { Si la raison ne m’éclaire, }
\livretVerse#7 { Le dépit et la colère }
\livretVerse#7 { Me tiendront lieu de raison. }
\livretPers Sangaride, Doris, Idas
\livretVerse#8 { Qu’une première amour est belle ? }
\livretVerse#8 { Qu’on a peine à s’en dégager ! }
\livretVerse#8 { Que l’on doit plaindre un cœur fidèle }
\livretVerse#8 { Lorsqu’il est forcé de changer. }

\livretScene SCÈNE SECONDE
\livretDescAtt\wordwrap-center {
  Célénus, suivants de Célénus, Sangaride, Idas, et Doris.
}
\livretPers Célénus
\livretRef#'EBBbelleNymphe
\livretVerse#12 { Belle nymphe, l’hymen va suivre mon envie, }
\livretVerse#8 { L’Amour avec moi vous convie }
\livretVerse#12 { À venir vous placer sur un trône éclatant, }
\livretVerse#12 { J’approche avec transport du favorable instant }
\livretVerse#12 { D’où dépend la douceur du reste de ma vie : }
\livretVerse#12 { Mais malgré les appas du bonheur qui m’attend, }
\livretVerse#12 { Malgré tous les transports de mon âme amoureuse, }
\livretVerse#8 { Si je ne puis vous rendre heureuse, }
\livretVerse#8 { Je ne serai jamais content. }
\livretVerse#8 { Je fais mon bonheur de vous plaire, }
\livretVerse#12 { J’attache à votre cœur mes désirs les plus doux. }
\livretPers Sangaride
\livretVerse#12 { Seigneur, j’obéirai, je dépends de mon père, }
\livretVerse#12 { Et mon père aujourd’hui veut que je sois à vous. }
\livretPers Célénus
\livretVerse#12 { Regardez mon amour, plutôt que ma couronne. }
\livretPers Sangaride
\livretVerse#12 { Ce n’est point la grandeur qui me peut éblouir. }
\livretPers Célénus
\livretVerse#12 { Ne sauriez-vous m’aimer sans que l’on vous l’ordonne. }
\livretPers Sangaride
\livretVerse#12 { Seigneur, contentez-vous que je sache obéïr, }
\livretVerse#12 { En l’état où je suis c’est ce que je puis dire… }

\livretScene SCÈNE TROISIÈME
\livretDescAtt\wordwrap-center {
  Atys, Célénus, Sangaride, Doris, Idas, suivants de Célénus.
}
\livretPers Célénus
\livretRef#'ECArecitVotreCoeurSeTrouble
\livretVerse#8 { Votre cœur se trouble, il soupire. }
\livretPers Sangaride
\livretVerse#8 { Expliquez en votre faveur }
\livretVerse#12 { Tout ce que vous voyez de trouble dans mon cœur. }
\livretPers Célénus
\livretVerse#12 { Rien ne m’alarme plus, Atys, ma crainte est vaine, }
\livretVerse#12 { Mon amour touche enfin le cœur de la beauté }
\livretVerse#6 { Dont je suis enchanté : }
\livretVerse#8 { Toi qui fus témoin de ma peine, }
\livretVerse#12 { Cher Atys, sois témoin de ma félicité. }
\livretVerse#12 { Peux-tu la concevoir ? non, il faut que l’on aime, }
\livretVerse#12 { Pour juger des douceurs de mon bonheur extrême. }
\livretVerse#8 { Mais, près de voir combler mes vœux, }
\livretVerse#12 { Que les moments sont longs pour mon cœur amoureux ! }
\livretVerse#12 { Vos parents tardent trop, je veux aller moi-même }
\livretVerse#8 { Les presser de me rendre heureux. }

\livretScene SCÈNE QUATRIÈME
\livretDescAtt\wordwrap-center { Atys, Sangaride. }
\livretPers Atys
\livretRef#'EDBrecitQuilSaitPeuSonMalheur
\livretVerse#12 { Qu’il sait peu son malheur ! et qu’il est déplorable ! }
\livretVerse#12 { Son amour méritait un sort plus favorable : }
\livretVerse#12 { J’ai pitié de l’erreur dont son cœur s’est flatté. }
\livretPers Sangaride
\livretVerse#12 { Épargnez-vous le soin d’être si pitoyable, }
\livretVerse#12 { Son amour obtiendra ce qu’il a merité. }
\livretPers Atys
\livretVerse#12 { Dieux ! qu’est-ce que j’entends ! }
\livretPers Sangaride
\livretVerse#12 { \transparent { Dieux ! qu’est-ce que j’entends ! } Qu’il faut que je me venge, }
\livretVerse#12 { Que j’aime enfin le roi, qu’il sera mon époux. }
\livretPers Atys
\livretVerse#12 { Sangaride, eh d’où vient ce changement étrange ? }
\livretPers Sangaride
\livretVerse#12 { N’est-ce pas vous ingrat qui voulez que je change ? }
\livretPers Atys
\livretVerse#12 { Moi ! }
\livretPers Sangaride
\livretVerse#12 { \transparent { Moi ! } Quelle trahison ! }
\livretPers Atys
\livretVerse#12 { \transparent { Moi ! Quelle trahison ! } Quel funeste couroux ! }
\livretPers Atys et Sangaride
\livretVerse#12 { Pourquoi m’abandonner pour une amour nouvelle ? }
\livretVerse#12 { Ce n’est pas moi qui rompt une chaîne si belle. }
\livretPers Atys
\livretVerse#8 { Beauté trop cruelle, c’est vous, }
\livretPers Sangaride
\livretVerse#8 { Amant infidèle, c’est vous, }
\livretPers Atys
\livretVerse#8 { Ah ! c’est vous, beauté trop cruelle, }
\livretPers Sangaride
\livretVerse#8 { Ah ! c’est vous amant infidèle. }
\livretPers Atys et Sangaride
\livretVerse#8 { Beauté trop cruelle, c’est vous, }
\livretVerse#8 { Amant infidèle, c’est vous, }
\livretVerse#8 { Qui rompez des liens si doux. }
\livretPers Sangaride
\livretVerse#12 { Vous m’avez immolée à l’amour de Cybèle. }
\livretPers Atys
\livretVerse#12 { Il est vrai qu’à ses yeux, par un secret effroi, }
\livretVerse#12 { J’ai voulu de nos cœurs cacher l’intelligence : }
\livretVerse#12 { Mais ce n’est que pour vous que j’ai crains sa vengeance, }
\livretVerse#8 { Et je ne la crains pas pour moi. }
\livretVerse#12 { Cybèle m’aime en vain, et c’est vous que j’adore. }
\livretPers Sangaride
\livretVerse#8 { Après votre infidélité, }
\livretVerse#8 { Auriez-vous bien la cruauté }
\livretVerse#8 { De vouloir me tromper encore ? }
\livretPers Atys
\livretVerse#8 { Moi ! vous trahir ? vous le pensez ? }
\livretVerse#8 { Ingrate, que vous m’offensez ! }
\livretVerse#8 { Eh bien, il ne faut plus rien taire, }
\livretVerse#12 { Je vais de la déesse attirer la colère, }
\livretVerse#12 { M’offrir à sa fureur, puisque vous m’y forcez… }
\livretPers Sangaride
\livretVerse#12 { Ah ! demeurez, Atys, mes soupçons sont passés ; }
\livretVerse#12 { Vous m’aimez, je le crois, j’en veux être certaine. }
\livretVerse#6 { Je le souhaite assez, }
\livretVerse#6 { Pour le croire sans peine. }
\livretPers Atys
\livretVerse#6 { Je jure, }
\livretPers Sangaride
\livretVerse#6 { \transparent { Je jure, } Je promets, }
\livretPers Atys et Sangaride
\livretVerse#6 { De ne changer jamais. }
\livretPers Sangaride
\livretVerse#12 { Quel tourment de cacher une si belle flamme. }
\livretPers Atys
\livretVerse#12 { Redoublons-en l’ardeur dans le fonds de notre âme. }
\livretPers Atys et Sangaride
\livretVerse#8 { Aimons en secret, aimons-nous : }
\livretVerse#12 { Aimons plus que jamais, en dépit des jaloux. }
\livretPers Sangaride
\livretVerse#12 { Mon père vient ici, }
\livretPers Atys
\livretVerse#12 { \transparent { Mon père vient ici, } Que rien ne vous étonne ; }
\livretVerse#12 { Servons-nous du pouvoir que Cybèle me donne, }
\livretVerse#8 { Je vais préparer les Zéphirs }
\livretVerse#6 { À suivre nos désirs. }

\livretScene SCÈNE CINQUIÈME
\livretDescAtt\column {
  \wordwrap-center {
    Sangaride, Célénus, le Dieux du Fleuve Sangar, troupe de dieux de
    fleuves, de ruisseaux, et de divinités de fontaines.
  }
  \justify\italic {
    Le Fleuve Sangar.  Suite du Fleuve Sangar.  Douze grands dieux de
    fleuves chantants.  Cinq dieux de fleuves jouants de la flûte.
    Quatre divinités de fontaines, et quatre dieux de fleuves
    chantants et dansants.  Quatre divinités de Fontaines.
    Deux dieux de fleuves.  Deux dieux de fleuves dansants ensemble.
    Deux petits dieux de ruisseaux chantants et dansants.
    Quatre petits dieux de ruisseaux dansants.  Six grands dieux de
    fleuves dansants.  Deux vieux dieux de fleuves et deux vieilles
    nymphes de fontaines dansantes.
  }
}
\livretPers Le Dieu du Fleuve Sangar
\livretRef#'EEBOVousQuiPrenezPart
\livretVerse#12 { Ô vous, qui prenez part au bien de ma famille, }
\livretVerse#12 { Vous, vénérables dieux des fleuves les plus grands, }
\livretVerse#12 { Mes fidèles amis, et mes plus chers parents, }
\livretVerse#12 { Voyez quel est l’époux que je donne à ma fille : }
\livretVerse#12 { J’ai pris soin de choisir entre les plus grands rois. }
\livretPers Chœur de dieux de fleuves
\livretVerse#7 { Nous approuvons votre choix. }
\livretPers Le Dieu du Fleuve Sangar
\livretVerse#8 { Il a Neptune pour son père, }
\livretVerse#8 { Les Phrygiens suivent ses lois ; }
\livretVerse#6 { J’ai cru ne pouvoir faire }
\livretVerse#8 { Un choix plus digne de vous plaire. }
\livretPers Chœur de dieux de fleuves
\livretVerse#7 { Tous, d’une commune voix, }
\livretVerse#7 { Nous approuvons votre choix. }
\livretPers Le Dieu du Fleuve Sangar
\livretRef#'EECairQueLonChante
\livretVerse#7 { Que l’on chante, que l’on danse, }
\livretVerse#7 { Rions tous lorsqu’il le faut ; }
\livretVerse#6 { Ce n’est jamais trop tôt }
\livretVerse#6 { Que le plaisir commence. }
\livretVerse#7 { On trouve bientôt la fin }
\livretVerse#7 { Des jours de réjouissance, }
\livretVerse#8 { On a beau chasser le chagrin, }
\livretVerse#8 { Il revient plutôt qu’on ne pense. }
\livretPers Le Dieu du Fleuve Sangar, et le Chœur
\livretVerse#7 { Que l’on chante, que l’on danse, }
\livretVerse#7 { Rions tous lorsqu’il le faut ; }
\livretVerse#6 { Ce n’est jamais trop tôt }
\livretVerse#6 { Que le plaisir commence. }
\livretVerse#7 { Que l’on chante, que l’on danse, }
\livretVerse#7 { Rions tous lorsqu’il le faut. }
\livretPers\wordwrap {
  Dieux de fleuves, divinités de fontaines, et de ruisseaux, chantants
  et dansants ensemble.
}
\livretRef#'EEDairLaBeauteLaPlusSevere
\livretVerse#7 { La beauté la plus sévère }
\livretVerse#7 { Prend pitié d’un long tourment, }
\livretVerse#7 { Et l’amant qui persévère }
\livretVerse#7 { Devient un heureux amant. }
\livretVerse#7 { Tout est doux, et rien ne coûte }
\livretVerse#7 { Pour un cœur qu’on veut toucher, }
\livretVerse#7 { L’onde se fait une route }
\livretVerse#7 { En s’efforçant d’en chercher, }
\livretVerse#7 { L’eau qui tombe goûte à goûte }
\livretVerse#7 { Perce le plus dur rocher. }
\null
\livretRef#'EEEairLhymenSeulNeSauraitPlaire
\livretVerse#7 { L’hymen seul ne saurait plaire, }
\livretVerse#7 { Il a beau flatter nos vœux ; }
\livretVerse#7 { L’amour seul a droit de faire }
\livretVerse#7 { Les plus doux de tous les nœuds. }
\livretVerse#7 { Il est fier, il est rebelle, }
\livretVerse#7 { Mais il charme tel qu’il est ; }
\livretVerse#7 { L’hymen vient quand on l’appelle, }
\livretVerse#7 { L’amour vient quand il lui plaît. }
\null
\livretVerse#7 { Il n’est point de résistance }
\livretVerse#7 { Dont le temps ne vienne à bout, }
\livretVerse#7 { Et l’effort de la constance }
\livretVerse#7 { À la fin doit vaincre tout. }
\livretVerse#7 { Tout est doux, et rien ne coûte }
\livretVerse#7 { Pour un cœur qu’on veut toucher, }
\livretVerse#7 { L’onde se fait une route }
\livretVerse#7 { En s’efforçant d’en chercher, }
\livretVerse#7 { L’eau qui tombe goûte à goûte }
\livretVerse#7 { Perce le plus dur rocher. }
\null
\livretVerse#7 { L’Amour trouble tout le monde, }
\livretVerse#7 { C’est la source de nos pleurs ; }
\livretVerse#7 { C’est un feu brûlant dans l’onde, }
\livretVerse#7 { C’est l’écueil des plus grands cœurs : }
\livretVerse#7 { Il est fier, il est rebelle, }
\livretVerse#7 { Mais il charme tel qu’il est ; }
\livretVerse#7 { L’hymen vient quand on l’appelle, }
\livretVerse#7 { L’amour vient quand il lui plaît. }
\livretPersDidas Un dieu de fleuve et une divinité de fontaine, dançent et chantent ensemble
\livretRef#'EEGduneConstanceExtreme
\livretVerse#6 { D’une constance extrême, }
\livretVerse#6 { Un ruisseau suit son cours ; }
\livretVerse#6 { Il en sera de même }
\livretVerse#6 { Du choix de mes amours, }
\livretVerse#6 { Et du moment que j’aime }
\livretVerse#6 { C’est pour aimer toujours. }
\null
\livretVerse#6 { Jamais un cœur volage }
\livretVerse#6 { Ne trouve un heureux sort, }
\livretVerse#6 { Il n’a point l’avantage }
\livretVerse#6 { D’être longtemps au port, }
\livretVerse#6 { Il cherche encor l’orage }
\livretVerse#6 { Au moment qu’il en sort. }
\livretPers Chœur de dieux de fleuves et de divinités de Fontaines
\livretRef#'EEIchoeurUnGrandCalmeEstTropFacheux
\livretVerse#7 { Un grand calme est trop fâcheux, }
\livretVerse#7 { Nous aimons mieux la tourmente. }
\livretVerse#7 { Que sert un cœur qui s’exempte }
\livretVerse#7 { De tous les soins amoureux ? }
\livretVerse#7 { À quoi sert une eau dormante ? }
\livretVerse#7 { Un grand calme est trop fâcheux, }
\livretVerse#7 { Nous aimons mieux la tourmente. }

\livretScene SCÈNE SIXIÈME
\livretDescAtt\wordwrap-center {
  Atys, troupe de Zéphirs volants, Sangaride, Célénus, Le Dieu du
  Fleuve Sangar, troupe de dieux de fleuves, de ruisseaux, et de
  divinités de fontaines.
}
\livretPers Chœur de dieux de fleuves et de fontaines
\livretRef#'EFAchoeurVenezFormerDesNoeudsCharmants
\livretVerse#8 { Venez former des nœuds charmants, }
\livretVerse#12 { Atys, venez unir ces bien heureux amants. }
\livretPers Atys
\livretVerse#8 { Cet hymen déplaît à Cybèle, }
\livretVerse#8 { Elle défend de l’achever : }
\livretVerse#12 { Sangaride est un bien qu’il faut lui réserver, }
\livretVerse#8 { Et que je demande pour elle. }
\livretPers Chœur
\livretVerse#6 { Ah quelle loi cruelle ! }
\livretPers Célénus
\livretVerse#12 { Atys peut s’engager lui-même à me trahir ? }
\livretVerse#8 { Atys contre moi s’interesse ? }
\livretPers Atys
\livretVerse#8 { Seigneur, je suis à la déesse, }
\livretVerse#12 { Dès qu’elle a commandé, je ne puis qu’obéïr. }
\livretPers Le Dieu du Fleuve Sangar
\livretVerse#8 { Pourquoi faut-il qu’elle sépare }
\livretVerse#12 { Deux illustres amants pour qui l’hymen prépare }
\livretVerse#6 { Ses liens les plus doux ? }
\livretPers Chœur
\livretVerse#4 { Opposons-nous }
\livretVerse#6 { À ce dessein barbare. }
\livretPersDidas Atys élevé sur un nuage
\livretVerse#7 { Apprenez, audacieux, }
\livretVerse#7 { Qu’il n’est rien qui n’obéïsse }
\livretVerse#12 { Aux souveraines lois de la reine des dieux. }
\livretVerse#8 { Qu’on nous enlève de ces lieux ; }
\livretVerse#12 { Zéphirs, que sans tarder mon ordre s’accomplisse. }
\livretDidasP\justify {
  Les Zéphirs volent, et enlèvent Atys et Sangaride.
}
\livretPers Chœur
\livretVerse#4 { Quelle injustice ! }
\sep
\livretAct ACTE CINQUIÈME
\livretDescAtt\wordwrap-center {
  Le théâtre change et représente des jardins agréables.
}
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\wordwrap-center { Célénus, Cybèle, Mélisse. }
\livretPers Célénus
\livretRef#'FABvousMotezSangaride
\livretVerse#12 { Vous m’ôtez Sangaride ? inhumaine Cybèle ; }
\livretVerse#6 { Est-ce le prix du zèle }
\livretVerse#12 { Que j’ai fait avec soin éclater à vos yeux ? }
\livretVerse#12 { Préparez-vous ainsi la douceur éternelle }
\livretVerse#8 { Dont vous devez combler ces lieux ? }
\livretVerse#12 { Est-ce ainsi que les rois sont protegés des dieux ? }
\livretVerse#6 { Divinité cruelle, }
\livretVerse#8 { Descendez-vous exprès des cieux }
\livretVerse#8 { Pour troubler un amour fidèle ? }
\livretVerse#12 { Et pour venir m’ôter ce que j’aime le mieux ? }
\livretPers Cybèle
\livretVerse#12 { J’aimais Atys, l’amour a fait mon injustice ; }
\livretVerse#8 { Il a pris soin de mon supplice ; }
\livretVerse#8 { Et si vous êtes outragé, }
\livretVerse#8 { Bientôt vous serez trop vengé. }
\livretVerse#8 { Atys adore Sangaride. }
\livretPers Célénus
\livretVerse#8 { Atys l’adore ? ah le perfide ! }
\livretPers Cybèle
\livretVerse#12 { L’ingrat vous trahissait, et voulait me trahir : }
\livretVerse#12 { Il s’est trompé lui-même en croyant m’éblouir. }
\livretVerse#12 { Les Zéphirs l’ont laissé, seul, avec ce qu’il aime, }
\livretVerse#6 { Dans ces aimables lieux ; }
\livretVerse#8 { Je m’y suis cachée à leurs yeux ; }
\livretVerse#12 { J’y viens d’être témoin de leur amour extrême. }
\livretPers Célénus
\livretVerse#12 { Ô Ciel ! Atys plairait aux yeux qui m’ont charmé ? }
\livretPers Cybèle
\livretVerse#12 { Eh pouvez-vous douter qu’Atys ne soit aimé ? }
\livretVerse#12 { Non, non, jamais amour n’eût tant de violence, }
\livretVerse#12 { Ils ont juré cent fois de s’aimer malgré nous, }
\livretVerse#8 { Et de braver notre vengeance ; }
\livretVerse#12 { Ils nous ont appelés cruels, tyrans, jaloux ; }
\livretVerse#8 { Enfin leurs cœurs d’intelligence, }
\livretVerse#12 { Tous deux… ah je frémis au moment que j’y pense ! }
\livretVerse#12 { Tous deux s’abandonnaient à des transports si doux, }
\livretVerse#12 { Que je n’ai pû garder plus longtemps le silence, }
\livretVerse#12 { Ni retenir l’éclat de mon juste couroux. }
\livretPers Célénus
\livretVerse#12 { La mort est pour leur crime une peine légère. }
\livretPers Cybèle
\livretVerse#12 { Mon cœur à les punir est assez engagé ; }
\livretVerse#12 { Je vous l’ai déja dit, croyez-en ma colère, }
\livretVerse#8 { Bientôt vous serez trop vengé. }

\livretScene SCÈNE SECONDE
\livretDescAtt\wordwrap-center {
  Atys, Sangaride, Cybèle, Célénus, Mélisse,
  troupe de prêtresses de Cybèle.
}
\livretPers Cybèle et Célénus
\livretRef#'FBAvenezVousLivrerAuSupplice
\livretVerse#8 { Venez vous livrer au supplice. }
\livretPers Atys et Sangaride
\livretVerse#12 { Quoi la terre et le ciel contre nous sont armés ? }
\livretVerse#8 { Souffrirez-vous qu’on nous punisse ? }
\livretPers Cybèle et Célénus
\livretVerse#8 { Oubliez-vous votre injustice ? }
\livretPers Atys et Sangaride
\livretVerse#12 { Ne vous souvient-il plus de nous avoir aimés ? }
\livretPers Cybèle et Célénus
\livretVerse#12 { Vous changez mon amour en haine légitime. }
\livretPers Atys et Sangaride
\livretVerse#6 { Pouvez-vous condamner }
\livretVerse#6 { L’amour qui nous anime ? }
\livretVerse#4 { Si c’est un crime, }
\livretVerse#8 { Quel crime est plus à pardonner ? }
\livretPers Cybèle et Célénus
\livretVerse#8 { Perfide, deviez-vous me taire }
\livretVerse#12 { Que c’était vainement que je voulais vous plaire ? }
\livretPers Atys et Sangaride
\livretVerse#8 { Ne pouvant suivre vos désirs, }
\livretVerse#8 { Nous croyons ne pouvoir mieux faire }
\livretVerse#12 { Que de vous épargner de mortels déplaisirs. }
\livretPers Cybèle
\livretVerse#12 { D’un supplice cruel craignez l’horreur extrême. }
\livretPers Cibèle et Célénus
\livretVerse#8 { Craignez un funeste trépas. }
\livretPers Atys et Sangaride
\livretVerse#12 { Vengez-vous, s’il le faut, ne me pardonnez pas, }
\livretVerse#8 { Mais pardonnez à ce que j’aime. }
\livretPers Cybèle et Célénus
\livretVerse#12 { C’est peu de nous trahir, vous nous bravez, ingrats ? }
\livretPers Atys et Sangaride
\livretVerse#12 { Serez-vous sans pitié ? }
\livretPers Cybèle et Célénus
\livretVerse#12 { \transparent { Serez-vous sans pitié ? } Perdez toute espérance. }
\livretPers Atys et Sangaride
\livretVerse#11 { L’amour nous a forcé à vous faire une offense, }
\livretVerse#8 { Il demande grâce pour nous. }
\livretPers Cybèle et Célénus
\livretVerse#5 { L’Amour en couroux }
\livretVerse#5 { Demande vengeance. }
\livretPers Cybèle
\livretVerse#12 { Toi, qui portes partout et la rage et l’horreur, }
\livretVerse#12 { Cesse de tourmenter les criminelles ombres, }
\livretVerse#12 { Viens, cruelle Alecton, sors des royaumes sombres, }
\livretVerse#12 { Inspire au cœur d’Atys ta barbare fureur. }

\livretScene SCÈNE TROISIÈME
\livretDescAtt\wordwrap-center {
  Alecton, Atys, Sangaride, Cybèle, Célénus, Mélisse,
  Idas, Doris, troupe de prêtresses de Cybèle, chœur de Phrygiens.
}
\livretRef#'FCApreludePourAlecton
\livretDidasPPage\justify {
  Alecton sort des Enfers, tenant à la main un flambeau qu’elle secoue
  en volant et en passant au dessus d’Atys.
}
\livretPers Atys
\livretRef#'FCBcielQuelleVapeurMenvironne
\livretVerse#8 { Ciel ! quelle vapeur m’environne ! }
\livretVerse#12 { Tous mes sens sont troublés, je frémis, je frissonne, }
\livretVerse#12 { Je tremble, et tout à coup, une infernale ardeur }
\livretVerse#12 { Vient enflammer mon sang, et dévorer mon cœur. }
\livretVerse#12 { Dieux ! que vois-je ? le ciel s’arme contre la terre ? }
\livretVerse#12 { Quel désordre ! quel bruit ! quel éclat de tonnerre ! }
\livretVerse#12 { Quels abîmes profonds sous mes pas sont ouverts ! }
\livretVerse#12 { Que de fantômes vains sont sortis des Enfers ! }
\livretDidasP\wordwrap { Il parle à Cybèle, qu’il prend pour Sangaride. }
\livretVerse#12 { Sangaride, ah fuyez la mort que vous prépare }
\livretVerse#8 { Une divinité barbare : }
\livretVerse#12 { C’est votre seul péril qui cause ma terreur. }
\livretPers Sangaride
\livretVerse#12 { Atys reconnaissez votre funeste erreur. }
\livretPersDidas Atys prenant Sangaride pour un monstre
\livretVerse#12 { Quel monstre vient à nous ! quelle fureur le guide ! }
\livretVerse#12 { Ah respecte, cruel, l’aimable Sangaride. }
\livretPers Sangaride
\livretVerse#12 { Atys, mon cher Atys. }
\livretPers Atys
\livretVerse#12 { \transparent { Atys, mon cher Atys. } Quels hurlements affreux ! }
\livretPersDidas Célénus à Sangaride
\livretVerse#8 { Fuyez, sauvez-vous de sa rage. }
\livretPersDidas Atys tenant à la main le couteau sacré qui sert aux sacrifices.
\livretVerse#12 { Il faut combattre ; Amour, seconde mon courage. }
\livretDidasP\wordwrap {
  Atys court après Sangaride qui fuit dans un des côtés du théâtre.
}
\livretPers Célénus et le chœur
\livretVerse#8 { Arrête, arrête malheureux. }
\livretDidasP Célénus court après Atys.
\livretPersDidas Sangaride dans un des côtés du théâtre.
\livretVerse#12 { Atys ! }
\livretPers Le chœur
\livretVerse#12 { \transparent { Atys ! } Ô ciel }
\livretPers Sangaride
\livretVerse#12 { \transparent { Atys ! Ô ciel } Je meurs. }
\livretPers Le Chœur
\livretVerse#12 { \transparent { Atys ! Ô ciel Je meurs. } Atys, Atys lui-même, }
\livretVerse#6 { Fait périr ce qu’il aime ! }
\livretPersDidas Célénus revenant sur le théâtre
\livretVerse#12 { Je n’ai pu retenir ses efforts furieux, }
\livretVerse#8 { Sangaride expire à vos yeux. }
\livretPers Cybèle
\livretVerse#12 { Atys me sacrifie une indigne rivale. }
\livretVerse#12 { Partagez avec moi la douceur sans égale, }
\livretVerse#12 { Que l’on goûte en vengeant un amour outragé. }
\livretVerse#12 { Je vous l’avais promis. }
\livretPers Célénus
\livretVerse#12 { \transparent { Je vous l’avais promis. } Ô promesse fatale ! }
\livretVerse#12 { Sangaride n’est plus, et je suis trop vengé. }
\livretDidasP\wordwrap {
  Célénus se retire au côté du théâtre, où est Sangaride morte.
}

\livretScene SCÈNE QUATRIÈME
\livretDescAtt\wordwrap-center {
  Atys, Cybèle, Mélisse, Idas, chœur de Phrygiens.
}
\livretPers Atys
\livretRef#'FDAqueJeViensDimmoler
\livretVerse#12 { Que je viens d’immoler une grande victime ! }
\livretVerse#12 { Sangaride est sauvée, et c’est par ma valeur. }
\livretPersDidas Cybèle touchant Atys
\livretVerse#12 { Achève ma vengeance, Atys, connais ton crime, }
\livretVerse#12 { Et reprends ta raison pour sentir ton malheur. }
\livretPers Atys
\livretVerse#12 { Un calme heureux succède aux troubles de mon cœur. }
\livretVerse#8 { Sangaride, nymphe charmante, }
\livretVerse#12 { Qu’êtes-vous devenue ? où puis-je avoir recours ? }
\livretVerse#8 { Divinité toute puissante, }
\livretVerse#12 { Cybèle, ayez pitié de nos tendres amours, }
\livretVerse#12 { Rendez-moi Sangaride, épargnez ses beaux jours. }
\livretPersDidas Cybèle montrant à Atys Sangride morte
\livretVerse#13 { Tu la peux voir, regarde. }
\livretPers Atys
\livretVerse#13 { \transparent { Tu la peux voir, regarde. } Ah quelle barbarie ! }
\livretVerse#8 { Sangaride a perdu la vie ! }
\livretVerse#12 { Ah quelle main cruelle ! ah quel cœur inhumain !… }
\livretPers Cybèle
\livretVerse#12 { Les coups dont elle meurt sont de ta propre main. }
\livretPers Atys
\livretVerse#12 { Moi, j’aurais immolé la beauté qui m’enchante ? }
\livretVerse#6 { Ô Ciel ! ma main sanglante }
\livretVerse#12 { Est de ce crime horrible un témoin trop certain ! }
\livretPers Le Chœur
\livretVerse#6 { Atys, Atys lui-même, }
\livretVerse#6 { Fait périr ce qu’il aime. }
\livretPers Atys
\livretVerse#12 { Quoi, Sangaride est morte ? Atys est son bourreau ! }
\livretVerse#12 { Quelle vengeance ô dieux ! quel supplice nouveau ! }
\livretVerse#8 { Quelles horreurs sont comparables }
\livretVerse#6 { Aux horreurs que je sens ? }
\livretVerse#8 { Dieux cruels, dieux impitoyables, }
\livretVerse#6 { N’êtes-vous tout-puissants }
\livretVerse#8 { Que pour faire des misérables ? }
\livretPers Cybèle
\livretVerse#8 { Atys, je vous ai trop aimé : }
\livretVerse#12 { Cet amour par vous-même en couroux transformé }
\livretVerse#8 { Fait voir encor sa violence : }
\livretVerse#12 { Jugez, ingrat, jugez en ce funeste jour, }
\livretVerse#8 { De la grandeur de mon amour }
\livretVerse#8 { Par la grandeur de ma vengeance. }
\livretPers Atys
\livretVerse#12 { Barbare ! quel amour qui prend soin d’inventer }
\livretVerse#12 { Les plus horribles maux que la rage peut faire ! }
\livretVerse#8 { Bien heureux qui peut éviter }
\livretVerse#6 { Le malheur de vous plaire. }
\livretVerse#12 { Ô dieux ! injustes dieux ! que n’êtes-vous mortels ? }
\livretVerse#12 { Faut-il que pour vous seuls vous gardiez la vengeance ? }
\livretVerse#12 { C’est trop, c’est trop souffrir leur cruelle puissance, }
\livretVerse#12 { Chassons-les d’ici bas, renversons leurs autels. }
\livretVerse#12 { Quoi, Sangaride est morte ? Atys, Atys lui-même }
\livretVerse#6 { Fait périr ce qu’il aime ? }
\livretPers Le Chœur
\livretVerse#6 { Atys, Atys lui-même }
\livretVerse#6 { Fait périr ce qu’il aime. }
\livretPersDidas Cybèle ordonnant d’emporter le corps de Sangaride morte
\livretVerse#12 { Ôtez ce triste objet. }
\livretPers Atys
\livretVerse#12 { \transparent { Ôtez ce triste objet. } Ah ne m’arrachez pas }
\livretVerse#8 { Ce qui reste de tant d’appas ! }
\livretVerse#8 { En fussiez-vous jalouse encore, }
\livretVerse#6 { Il faut que je l’adore }
\livretVerse#8 { Jusque dans l’horreur du trépas. }

\livretScene SCÈNE CINQUIÈME
\livretDescAtt\wordwrap-center { Cybèle, Mélisse. }
\livretPers Cybèle
\livretRef#'FEAjeCommenceATrouverSaPeineTropCruelle
\livretVerse#12 { Je commence à trouver sa peine trop cruelle, }
\livretVerse#8 { Une tendre pitié rappelle }
\livretVerse#12 { L’Amour que mon couroux croyait avoir banni, }
\livretVerse#12 { Ma rivale n’est plus, Atys n’est plus coupable, }
\livretVerse#12 { Qu’il est aisé d’aimer un criminel aimable }
\livretVerse#6 { Après l’avoir puni. }
\livretVerse#8 { Que son désespoir m’épouvante ! }
\livretVerse#12 { Ses jours sont en péril, et j’en frémis d’effroi : }
\livretVerse#12 { Je veux d’un soin si cher ne me fier qu’à moi, }
\livretVerse#12 { Allons… mais quel spectable à mes yeux se présente ? }
\livretVerse#8 { C’est Atys mourant que je vois ! }

\livretScene SCÈNE SIXIÈME
\livretDescAtt\wordwrap-center {
  Atys, Idas, Cybèle, Mélisse, prêtresses de Cybèle.
}
\livretPersDidas Idas soûtenant Atys
\livretRef#'FFAilSestPerceLeSein
\livretVerse#12 { Il s’est percé le sein, et mes soins pour sa vie }
\livretVerse#8 { N’ont pu prévenir sa fureur. }
\livretPers Cybèle
\livretVerse#6 { Ah c’est ma barbarie, }
\livretVerse#8 { C’est moi, qui lui perce le cœur. }
\livretPers Atys
\livretVerse#6 { Je meurs, l’amour me guide }
\livretVerse#6 { Dans la nuit du trépas ; }
\livretVerse#8 { Je vais où sera Sangaride, }
\livretVerse#12 { Inhumaine, je vais, où vous ne serez pas. }
\livretPers Cybèle
\livretVerse#12 { Atys, il est trop vrai, ma rigueur est extrême, }
\livretVerse#8 { Plaignez-vous, je veux tout souffrir. }
\livretVerse#12 { Pourquoi suis-je immortelle en vous voyant périr ? }
\livretPers Atys et Cybèle
\livretVerse#6 { Il est doux de mourir }
\livretVerse#6 { Avec ce que l’on aime. }
\livretPers Cybèle
\livretVerse#12 { Que mon amour funeste armé contre moi-même, }
\livretVerse#12 { Ne peut-il vous venger de toutes mes rigueurs. }
\livretPers Atys
\livretVerse#12 { Je suis assez vengé, vous m’aimez, et je meurs. }
\livretPers Cybèle
\livretVerse#8 { Malgré le destin implacable }
\livretVerse#12 { Qui rend de ton trépas l’arrêt irrévocable, }
\livretVerse#12 { Atys, sois à jamais l’objet de mes amours : }
\livretVerse#12 { Reprends un sort nouveau, deviens un arbre aimable }
\livretVerse#8 { Que Cybèle aimera toujours. }

\livretRef#'FGAritournelle
\livretDidasPPage\wordwrap {
  Atys prend la forme de l’arbre aimé de la déesse Cybèle, que l’on appelle pin.
}
\livretPers Cybèle
\livretRef#'FGBvenezFurieuxCorybantes
\livretVerse#8 { Venez furieux Corybantes, }
\livretVerse#12 { Venez joindre à mes cris vos clameurs éclatantes ; }
\livretVerse#12 { Venez nymphes des eaux, venez dieux des forêts, }
\livretVerse#8 { Par vos plaintes les plus touchantes }
\livretVerse#8 { Seconder mes tristes regrets. }

\livretScene SCÈNE SEPTIÈME ET DERNIÈRE
\livretDescAtt\column {
  \wordwrap-center {
    Cybèle, troupe de nymphes des eaux, troupe de divinités des bois,
    troupe de Corybantes.
  }
  \justify {
    Quatre nymphes chantantes.
    Huit dieux des bois chantants.
    Quatorze corybantes chantantes.
    Huit corybantes dansantes.
    Trois dieux des bois, dansants.
    Trois nymphes dansantes.
  }
}
\livretPers Cybèle
\livretRef#'FGCairChoeurAtysAimableAtys
\livretVerse#12 { Atys, l’aimable Atys, malgré tous ses attraits, }
\livretVerse#8 { Descend dans la nuit éternelle ; }
\livretVerse#7 { Mais malgré la mort cruelle, }
\livretVerse#5 { L’amour de Cybèle }
\livretVerse#5 { Ne mourra jamais. }
\livretVerse#8 { Sous une nouvelle figure, }
\livretVerse#12 { Atys est ranimé par mon pouvoir divin ; }
\livretVerse#8 { Célébrez son nouveau destin, }
\livretVerse#8 { Pleurez sa funeste aventure. }
\livretPers Chœur des nymphes des eaux et des divinités des bois
\livretVerse#8 { Célébrons son nouveau destin, }
\livretVerse#8 { Pleurons sa funeste aventure. }
\livretPers Cybèle
\livretVerse#6 { Que cet arbre sacré }
\livretVerse#4 { Soit révéré }
\livretVerse#6 { De toute la nature. }
\livretVerse#12 { Qu’il s’élève au-dessus des arbres les plus beaux : }
\livretVerse#12 { Qu’il soit voisin des cieux, qu’il règne sur les eaux ; }
\livretVerse#12 { Qu’il ne puisse brûler que d’une flamme pure. }
\livretVerse#6 { Que cet Arbre sacré }
\livretVerse#4 { Soit révéré }
\livretVerse#6 { De toute la nature. }
\livretDidasP\wordwrap { Le chœur répète ces trois derniers vers. }
\livretPers Cybèle
\livretVerse#8 { Que ses rameaux soient toujours verts : }
\livretVerse#8 { Que les plus rigoureux hivers }
\livretVerse#8 { Ne leur fassent jamais d’injure. }
\livretVerse#6 { Que cet arbre sacré }
\livretVerse#4 { Soit révéré }
\livretVerse#6 { De toute la nature. }
\livretDidasP\wordwrap { Le chœur répète ces trois derniers vers. }
\livretPers Cybèle et le chœur des divinités des bois et des eaux
\livretVerse#4 { Quelle douleur ! }
\livretPers Cybèle et le chœur des corybantes
\livretVerse#4 { Ah ! quelle rage ! }
\livretPers Cybèle et les chœurs
\livretVerse#4 { Ah ! quel malheur ! }
\livretPers Cybèle
\livretVerse#8 { Atys au printemps de son âge, }
\livretVerse#6 { Périt comme une fleur }
\livretVerse#5 { Qu’un soudain orage }
\livretVerse#5 { Renverse et ravage. }
\livretPers Cybèle et le chœur des divinités des bois et des eaux
\livretVerse#4 { Quelle douleur ! }
\livretPers Cybèle et le chœur des corybantes
\livretVerse#4 { Ah ! quelle rage ! }
\livretPers Cybèle et les chœurs
\livretVerse#4 { Ah ! quel malheur ! }
\livretRef#'FGDentreeNymphes
\livretDidasPPage\justify {
  Les divinités des bois et des eaux, avec les Corybantes, honorent le
  nouvel arbre, et le consacrent à Cybèle. Les regrets des divinités
  des bois et des eaux, et les cris des Corybantes, sont secondés et
  terminés par des tremblements de terre, par des éclairs, et par des
  éclats de tonnerre.
}
\livretPers Cybèle et le chœur des divinités des bois et des eaux
\livretRef#'FGGchoeurQueLeMalheurDAtys
\livretVerse#12 { Que le malheur d’Atys afflige tout le monde. }
\livretPers Cybèle et le chœur des Corybantes
\livretVerse#6 { Que tout sente, ici bas, }
\livretVerse#8 { L’horreur d’un si cruel trépas. }
\livretPers Cybèle et le chœur des divinités des bois et des eaux
\livretVerse#12 { Pénétrons tous les cœurs d’une douleur profonde : }
\livretVerse#12 { Que les bois, que les eaux, perdent tous leurs appas. }
\livretPers Cybèle et le chœur des Corybantes
\livretVerse#8 { Que le tonnerre nous réponde : }
\livretVerse#12 { Que la terre frémisse, et tremble sous nos pas. }
\livretPers Cybèle et le chœur des divinités des bois et des eaux
\livretVerse#12 { Que le malheur d’Atys afflige tout le monde. }
\livretPers Tous ensemble
\livretVerse#6 { Que tout sente, ici bas, }
\livretVerse#8 { L’horreur d’un si cruel trépas. }
\livretFinAct FIN.
}

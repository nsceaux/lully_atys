\livretAct ACTE PREMIER
\livretDescAtt\wordwrap-center {
  Le Théâtre représente une montagne consacrée à Cybèle.
}
\livretScene SCÈNE PREMIÈRE
\livretPers Atys
\livretRef#'BAAairAllonsAllons
%# Allons, allons, accourez tous,
%# Cybèle va descendre.
%# Trop heureux Phrygi=ens, venez ici l'attendre.
%# Mille peuples seront jaloux
%# Des faveurs que sur nous
%# Sa bonté va répandre.

\livretScene SCÈNE SECONDE
\livretPers Idas, Atys
\livretRef#'BBAairAllonsAllons
%# Allons, allons, accourez tous,
%# Cybèle va descendre.
\livretPers Atys
%# Le soleil peint nos champs des plus vives couleurs,
%# Il a séché les pleurs
%# Que sur l'émail des prés a répandu l'aurore;
%# Et ses ray=ons nouveaux ont déjà fait éclore
%# Mille nouvelles fleurs.
\livretPers Idas
%# Vous veillez lorsque tout sommeille;
%# Vous nous éveillez si matin
%# Que vous ferez croire à la fin
%# Que c'est l'Amour qui vous éveille.
\livretPers Atys
%# Non tu dois mieux juger du parti que je prends.
%# Mon cœur veut fuir toujours les soins et les mystères;
%# J'aime l'*heureuse paix des cœurs indifférents;
%# Si leurs plaisirs ne sont pas grands,
%# Au moins leurs peines sont légères.
\livretPers Idas
%# Tôt ou tard l'amour est vainqueur,
%#8 En vain les plus fiers s'en défendent,
%# On ne peut refuser son cœur
%#8 À de beaux yeux qui le demandent.
%# Atys, ne feignez plus, je sais votre secret.
%# Ne craignez rien, je suis discret.
%# Dans un bois solitaire, et sombre,
%# L'indifférent Atys se croy=ait seul, un jour;
%# Sous un feuillage épais où je rêvais à l'ombre,
%# Je l'entendis parler d'amour.
\livretPers Atys
%# Si je parle d'amour, c'est contre son empire,
%# J'en fais mon plus doux entretien.
\livretPers Idas
%# Tel se vante de n'aimer rien,
%# Dont le cœur en secret soupire.
%# J'entendis vos regrets, et je les sais si bien
%# Que si vous en doutez je vais vous les redire.
%# Amants qui vous plaignez, vous êtes trop heureux:
%# Mon cœur de tous les cœurs est le plus amoureux,
%# Et tout près d'expirer je suis réduit à feindre;
%# Que c'est un tourment rigoureux
%# De mourir d'amour sans se plaindre!
%# Amants qui vous plaignez, vous êtes trop heureux.
\livretPers Atys
%# Idas, il est trop vrai, mon cœur n'est que trop tendre,
%# L'Amour me fait sentir ses plus funestes coups.
%# Qu'aucun autre que toi n'en puisse rien apprendre.

\livretScene SCÈNE TROISIÈME
\livretDescAtt\wordwrap-center { Sangaride, Doris, Atys, Idas. }
\livretPers Sangaride, et Doris
\livretRef#'BCAairAllonsAllons
%# Allons, allons, accourez tous,
%# Cybèle va descendre.
\livretPers Sangaride
%# Que dans nos concerts les plus doux,
%# Son nom sacré se fasse entendre.
\livretPers Atys
%# Sur l'univers entier son pouvoir doit s'étendre.
\livretPers Sangaride
%# Les Dieux suivent ses lois et craignent son couroux.
\livretPers Atys, Sangaride, Idas, Doris
%# Quels honneurs! quels respects ne doit-on point lui rendre?
%# Allons, allons, accourez tous,
%# Cybèle va descendre.
\livretPers Sangaride
%# Écoutons les oiseaux de ces bois d'alentour,
%# Ils remplissent leurs chants d'une douceur nouvelle.
%# On dirait que dans ce beau jour,
%# Ils ne parlent que de Cybèle.
\livretPers Atys
%# Si vous les écoutez, ils parleront d'amour.
%# Un roi redoutable,
%# Amoureux, aimable,
%# Va devenir votre époux;
%# Tout parle d'amour pour vous.
\livretPers Sangaride
%# Il est vrai, je tri=omphe, et j'aime ma victoire.
%# Quand l'Amour fait régner, est-il un plus grand bien?
%# Pour vous, Atys, vous n'aimez rien,
%# Et vous en faites gloire.
\livretPers Atys
%# L'amour fait trop verser de pleurs;
%# Souvent ses douceurs sont mortelles.
%# Il ne faut regarder les belles
%# Que comme on voit d'aimables fleurs.
%# J'aime les roses nouvelles,
%# J'aime les voir s'embellir,
%# Sans leurs épines cru=elles,
%# J'aimerais à les cueillir.
\livretPers Sangaride
%# Quand le péril est agré=able,
%# Le moy=en de s'en alarmer?
%# Est-ce un grand mal de trop aimer
%# Ce que l'on trouve aimable?
%# Peut-on être insensible aux plus charmants appas?
\livretPers Atys
%# Non vous ne me connaissez pas.
%# Je me défends d'aimer autant qu'il m'est possible;
%# Si j'aimais, un jour, par malheur,
%# Je connais bien mon cœur
%# Il serait trop sensible.
%# Mais il faut que chacun s'assemble près de vous,
%# Cybèle pourrait nous surprendre.
\livretPers Idas, Atys
%# Allons, allons, accourez tous,
%# Cybèle va descendre.

\livretScene SCÈNE QUATRIÈME
\livretDescAtt\wordwrap-center { Sangaride, Doris. }
\livretPers Sangaride
\livretRef#'BDAatysEstTropHeureux
%# Atys est trop heureux.
\livretPers Doris
%# L'amitié fut toujours égale entre vous deux,
%# Et le sang d'assez près vous lie:
%# Quel que soit son bonheur, lui portez-vous envie?
%# Vous, qu'aujourd'*hui l'*hymen avec de si beaux nœuds
%# Doit unir au Roi de Phrygie?
\livretPers Sangaride
%# Atys, est trop heureux.
%# Souverain de son cœur, maître de tous ses vœux,
%# Sans crainte, sans mélancolie,
%# Il jou=it en repos des beaux jours de sa vie;
%# Atys ne connaît point les tourments amoureux,
%# Atys est trop heureux.
\livretPers Doris
%# Quel mal vous fait l'Amour? votre chagrin m'étonne.
\livretPers Sangaride
%# Je te fie =un secret qui n'est su de personne.
%# Je devrais aimer un amant
%# Qui m'offre une couronne;
%# Mais, *hélas! vainement
%# Le Devoir me l'ordonne,
%# L'Amour, pour mon tourment,
%# En ordonne autrement.
\livretPers Doris
%# Aimeriez-vous Atys, lui dont l'indifférence
%# Brave avec tant d'orgueil l'Amour et sa puissance?
\livretPers Sangaride
%# J'aime, Atys, en secret, mon crime, est sans témoins.
%# Pour vaincre mon amour, je mets tout en usage,
%# J'appelle ma raison, j'anime mon courage;
%# Mais à quoi servent tous mes soins?
%# Mon cœur en souffre davantage,
%# Et n'en aime pas moins.
\livretPers Doris
%# C'est le commun défaut des belles.
%# L'ardeur des conquêtes nouvelles
%# Fait négliger les cœurs qu'on a trop tôt charmés,
%# Et les indifférents sont quelquefois aimés
%# Aux dépents des amants fidèles.
%# Mais vous vous exposez à des peines cru=elles.
\livretPers Sangaride
%# Toujours aux yeux d'Atys je serai sans appas;
%# Je le sais, j'y consens, je veux, s'il est possible,
%# Qu'il soit encor plus insensible;
%# S'il me pouvait aimer, que deviendrais-je? *hélas!
%# C'est mon plus grand bonheur qu'Atys ne m'aime pas.
%# Je prétends être *heureuse, au moins, en apparence;
%# Au destin d'un grand Roi je me vais attacher.
\livretPers Sangaride, et Doris
%# Un amour malheureux dont le devoir s'offense,
%# Se doit condamner au silence;
%# Un amour malheureux qu'on nous peut reprocher,
%# Ne saurait trop bien se cacher.

\livretScene SCÈNE CINQUIÈME
\livretDescAtt\wordwrap-center { Atys, Sangaride, Doris. }
\livretPers Atys
\livretRef#'BEArecitOneVoitDansCesCampagnes
%# On voit dans ces campagnes
%# Tous nos Phrygi=ens s'avancer.
\livretPers Doris
%# Je vais prendre soin de presser
%# Les Nymphes nos compagnes.

\livretScene SCÈNE SIXIÈME
\livretDescAtt\wordwrap-center { Atys, Sangaride. }
\livretPers Atys
\livretRef#'BFArecitSangarideCeJour
%# Sangaride, ce jour est un grand jour pour vous.
\livretPers Sangaride
%# Nous ordonnons tous deux la fête de Cybèle,
%# L'*honneur est égal entre nous.
\livretPers Atys
%# Ce jour même, un grand Roi doit être votre époux,
%# Je ne vous vis jamais si contente et si belle;
%# Que le sort du Roi sera doux!
\livretPers Sangaride
%# L'indifférent Atys n'en sera point jaloux.
\livretPers Atys
%# Vivez tous deux contents, c'est ma plus chère envie;
%# J'ai pressé votre *hymen, j'ai servi vos amours.
%# Mais enfin ce grand jour, le plus beau de vos jours,
%# Sera le dernier de ma vie.
\livretPers Sangaride
%#- O dieux!
\livretPers Atys
%#= Ce n'est qu'à vous que je veux révéler
%# Le secret désespoir où mon malheur me livre;
%# Je n'ai que trop su feindre, il est temps de parler;
%# Qui n'a plus qu'un moment à vivre,
%# N'a plus rien à dissimuler.
\livretPers Sangaride
%# Je frémis, ma crainte est extrême;
%# Atys, par quel malheur faut-il vous voir périr?
\livretPers Atys
%# Vous me condamnerez vous même,
%# Et vous me laisserez mourir.
\livretPers Sangaride
%# J'armerai, s'il se faut, tout le pouvoir suprême…
\livretPers Atys
%# Non, rien ne peut me secourir,
%# Je meurs d'amour pour vous, je n'en saurais guérir;
\livretPers Sangaride
%#- Quoy? vous?
\livretPers Atys
%#- Il est trop vrai.
\livretPers Sangaride
%#- Vous m'aimez?
\livretPers Atys
%#= Je vous aime.
%# Vous me condamnerez vous même,
%# Et vous me laisserez mourir.
%# J'ai mérité qu'on me punisse,
%# J'offense un rival généreux,
%# Qui par mille bienfaits a prévenu mes vœux:
%# Mais je l'offense en vain, vous lui rendez justice;
%# Ah! que c'est un cru=el supplice
%# D'avou=er qu'un rival est digne d'être *heureux!
%# Prononcez mon arrêt, parlez sans vous contraindre.
\livretPers Sangaride
%#- Hélas!
\livretPers Atys
%#= Vous soupirez? je vois couler vos pleurs?
%# D'un malheureux amour plaignez-vous les douleurs?
\livretPers Sangaride
%# Atys, que vous seriez à plaindre
%# Si vous saviez tous vos malheurs!
\livretPers Atys
%# Si je vous perds, et si je meurs,
%# Que puis-je encore avoir à craindre?
\livretPers Sangaride
%# C'est peu de perdre en moi ce qui vous a charmé,
%# Vous me perdez, Atys, et vous êtes aimé.
\livretPers Atys
%# Aimé! qu'entends-je? ô Ciel! quel aveu favorable!
\livretPers Sangaride
%# Vous en serez plus misérable.
\livretPers Atys
%# Mon malheur en est plus affreux,
%# Le bonheur que je perds doit redoubler ma rage;
%# Mais n'importe, aimez-moi, s'il se peut, d'avantage,
%# Quand j'en devrais mourir cent fois plus malheureux.
\livretPers Sangaride
%# Si vous cherchez la mort, il faut que je vous suive;
%# Vivez, c'est mon amour qui vous en fait la loi.
\livretPers Atys
%# Eh comment! eh pourquoi
%# Voulez-vous que je vive,
%# Si vous ne vivez pas pour moi?
\livretPers Atys et Sangaride
%# Si l'*hymen unissait mon destin et le vôtre,
%# Que ses nœuds auraient eu d'attraits!
%# L'Amour fit nos cœurs l'un pour l'autre,
%# Faut-il que le devoir les sépare à jamais?
\livretPers Atys
%# Devoir impitoy=able!
%# Ah quelle cru=auté!
\livretPers Sangaride
%# On vient, feignez encor, craignez d'être écouté.
\livretPers Atys
%# Aimons un bien plus durable
%# Que l'éclat de la beauté:
%# Rien n'est plus aimable
%# Que la liberté.

\livretScene SCÈNE SEPTIÈME
\livretDescAtt\column {
  \wordwrap-center { Atys, Sangaride, Doris, Idas. }
  \justify {
    Chœur de Phrygiens chantants. Chœur de Phrygiennes chantantes.
    Troupe de Phrygiens dansants. Troupe de Phrygiennes dansantes.
  }
  \justify\italic {
    Dix hommes phrygiens chantants conduits par Atys.
    Dix femmes phrygiennes chantantes conduites par Sangaride.
    Six Phrygiens dansants. Six Nymphes phrygiennes dansantes.
  }
}
\livretPers Atys
\livretRef#'BGAairMaisDejaDeCeMontSacre
%# Mais déjà de ce mont sacré
%# Le sommet paraît éclairé
%# D'une splendeur nouvelle.
\livretPersDidas Sangaride s’avançant vers la Montagne
%# La Dé=esse descend, allons au devant d'elle.
\livretPers Atys et Sangaride
%# Commençons, commençons
%# De célébrer ici sa fête solemnelle,
%# Commençons, commençons
%# Nos jeux et nos chansons.
\livretDidasP\wordwrap { Les chœurs répètent ces derniers vers. }
\livretPers Atys et Sangaride
%# Il est temps que chacun fasse éclater son zèle.
%# Venez, Reine des Dieux, venez,
%# Venez, favorable Cybèle.
\livretDidasP\wordwrap { Les chœurs répètent ces deux derniers vers. }
\livretPers Atys
%# Quittez votre cour immortelle,
%# Choisissez ces lieux fortunés
%# Pour votre demeure éternelle.
\livretPers Les Chœurs
%# Venez, Reine des Dieux, venez,
\livretPers Sangaride
%# La Terre sous vos pas va devenir plus belle
%# Que le séjour des Dieux que vous abandonnez.
\livretPers Les Chœurs
%# Venez, favorable Cybèle.
\livretPers Atys et Sangaride
%# Venez voir les autels qui vous sont destinés.
\livretPers Atys, Sangaride, Idas, Doris, et les chœurs
%# Écoutez un peuple fidèle
%# Qui vous appelle,
%# Venez Reine des Dieux, venez,
%# Venez favorable Cybèle.

\livretScene SCÈNE HUITIÈME
\livretDescAtt\justify {
  La Déesse Cybèle paraît sur son char, et les Phrygiens et les
  Phrygiennes lui témoignent leur joie et leur respect.
}
\livretPersDidas Cybèle sur son char
\livretRef#'BHBairChoeurVenezTousDansMonTemple
%# Venez tous dans mon temple, et que chacun révère
%# Le sacrificateur dont je vais faire choix:
%# Je m'expliquerai par sa voix,
%# Les vœux qu'il m'offrira seront sûrs de me plaire.
%# Je reçois vos respects; j'aime à voir les honneurs
%# Dont vous me presentez un éclatant hommage,
%# Mais l'*hommage des cœurs
%# Est ce que j'aime davantage.
%# Vous devez vous animer
%# D'une ardeur nouvelle,
%# S'il faut honorer Cybèle,
%# Il faut encor plus l'aimer.
\livretDidasP\justify {
  Cybèle portée par son char volant, se va rendre dans son
  temple. Tous les Phrygiens s’empressent d’y aller, et répètent les
  quatres derniers vers que la Déesse a prononcés.
}
\livretPers Les Chœurs
%# Nous devons nous animer
%# D'une ardeur nouvelle,
%# S'il faut honorer Cybèle,
%# Il faut encor plus l'aimer.
\sep

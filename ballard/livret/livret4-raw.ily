\livretAct ACTE QUATRIÈME
\livretDescAtt\wordwrap-center {
  Le théâtre change et représente le palais du Fleuve Sangar.
}
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\wordwrap-center { Sangaride, Doris, Idas. }
\livretPers Doris
\livretRef#'EAArecitQuoiVousPleurez
%#- Quoi, vous pleurez?
\livretPers Idas
%#= D'où vient votre peine nouvelle?
\livretPers Doris
%# N'osez-vous découvrir votre amour à Cybèle?
\livretPers Sangaride
%#- Hélas!
\livretPers Doris et Idas
%#= Qui peut encor redoubler vos ennuis?
\livretPers Sangaride
%#- Hélas! j'aime… *hélas! j'aime…
\livretPers Doris et Idas
%#- Achevez.
\livretPers Sangaride
%#= Je ne puis.
\livretPers Doris et Idas
%# L'Amour n'est guère *heureux lorsqu'il est trop timide.
\livretPers Sangaride
%# Hélas! j'aime un perfide
%# Qui trahit mon amour;
%# La Dé=esse aime Atys, il change en moins d'un jour,
%# Atys comblé d'*honneurs n'aime plus Sangaride.
%# Hélas! j'aime un perfide
%# Qui trahit mon amour.
\livretPers Doris et Idas
%# Il nous montrait tantôt un peu d'incertitude;
%# Mais qui l'eut soupçonné de tant d'ingratitude?
\livretPers Sangaride
%# J'embarassais Atys, je l'ai vu se troubler:
%# Je croy=ais devoir révéler
%# Notre amour à Cybèle;
%# Mais l'ingrat, l'infidèle,
%# M'empechait toujours de parler.
\livretPers Doris et Idas
%# Peut-on changer si tôt quand l'amour est extrême?
%# Gardez-vous, gardez-vous
%# De trop croire un transport jaloux.
\livretPers Sangaride
%# Cybèle hautement déclare qu'elle l'aime,
%# Et l'ingrat n'a trouvé cet *honneur que trop doux;
%# Il change en un moment, je veux changer de même,
%# J'accepterai sans peine un glori=eux époux,
%# Je ne veux plus aimer que la grandeur suprême.
\livretPers Doris et Idas
%# Peut-on changer si tôt quand l'amour est extrême?
%# Gardez-vous, gardez-vous
%# De trop croire un transport jaloux.
\livretPers Sangaride
%# Trop *heureux un cœur qui peut croire
%# Un dépit qui sert à sa gloire.
%# Revenez ma raison, revenez pour jamais,
%# Joignez-vous au dépit pour étouffer ma flamme,
%# Réparez, s'il se peut, les maux qu'Amour m'a faits,
%# Venez rétablir dans mon âme
%# Les douceurs d'une *heureuse paix;
%# Revenez, ma raison, revenez pour jamais.
\livretPers Idas et Doris
%# Une infidelité cru=elle
%# N'efface point tous les appas
%# D'un infidèle,
%# Et la raison ne revient pas
%# Si tôt qu'on l'a rappelle.
\livretPers Sangaride
%# Après une trahison
%# Si la raison ne m'éclaire,
%# Le dépit et la colère
%# Me tiendront lieu de raison.
\livretPers Sangaride, Doris, Idas
%# Qu'une première amour est belle?
%# Qu'on a peine à s'en dégager!
%# Que l'on doit plaindre un cœur fidèle
%# Lorsqu'il est forcé de changer.

\livretScene SCÈNE SECONDE
\livretDescAtt\wordwrap-center { 
  Célénus, suivants de Célénus, Sangaride, Idas, et Doris.
}
\livretPers Célénus
\livretRef#'EBBbelleNymphe
%# Belle nymphe, l'*hymen va suivre mon envie,
%# L'Amour avec moi vous convie
%# À venir vous placer sur un trône éclatant,
%# J'approche avec transport du favorable instant
%# D'où dépend la douceur du reste de ma vie:
%# Mais malgré les appas du bon*heur qui m'attend,
%# Malgré tous les transports de mon âme amoureuse,
%# Si je ne puis vous rendre *heureuse,
%# Je ne serai jamais content.
%# Je fais mon bonheur de vous plaire,
%# J'attache à votre cœur mes désirs les plus doux.
\livretPers Sangaride
%# Seigneur, j'obé=irai, je dépends de mon père,
%# Et mon père aujourd'hui veut que je sois à vous.
\livretPers Célénus
%# Regardez mon amour, plutôt que ma couronne.
\livretPers Sangaride
%# Ce n'est point la grandeur qui me peut éblou=ir.
\livretPers Célénus
%# Ne sauriez-vous m'aimer sans que l'on vous l'ordonne.
\livretPers Sangaride
%# Seigneur, contentez-vous que je sache obé=ïr,
%# En l'état où je suis c'est ce que je puis dire…

\livretScene SCÈNE TROISIÈME
\livretDescAtt\wordwrap-center {
  Atys, Célénus, Sangaride, Doris, Idas, suivants de Célénus.
}
\livretPers Célénus
\livretRef#'ECArecitVotreCoeurSeTrouble
%# Votre cœur se trouble, il soupire.
\livretPers Sangaride
%# Expliquez en votre faveur
%# Tout ce que vous voy=ez de trouble dans mon cœur.
\livretPers Célénus
%# Rien ne m'alarme plus, Atys, ma crainte est vaine,
%# Mon amour touche enfin le cœur de la beauté
%# Dont je suis enchanté:
%# Toi qui fus témoin de ma peine,
%# Cher Atys, sois témoin de ma félicité.
%# Peux-tu la concevoir? non, il faut que l'on aime,
%# Pour juger des douceurs de mon bon*heur extrême.
%# Mais, près de voir combler mes vœux,
%# Que les moments sont longs pour mon cœur amoureux!
%# Vos parents tardent trop, je veux aller moi-même
%# Les presser de me rendre *heureux.

\livretScene SCÈNE QUATRIÈME
\livretDescAtt\wordwrap-center { Atys, Sangaride. }
\livretPers Atys
\livretRef#'EDBrecitQuilSaitPeuSonMalheur
%# Qu'il sait peu son malheur! et qu'il est déplorable!
%# Son amour méritait un sort plus favorable:
%# J'ai pitié de l'erreur dont son cœur s'est flatté.
\livretPers Sangaride
%# Épargnez-vous le soin d'être si pitoy=able,
%# Son amour obtiendra ce qu'il a merité.
\livretPers Atys
%#- Dieux! qu'est-ce que j'entends!
\livretPers Sangaride
%#= Qu'il faut que je me venge,
%# Que j'aime enfin le roi, qu'il sera mon époux.
\livretPers Atys
%# Sangaride, eh d'où vient ce changement étrange?
\livretPers Sangaride
%# N'est-ce pas vous ingrat qui voulez que je change?
\livretPers Atys
%#- Moi!
\livretPers Sangaride
%#- Quelle trahison!
\livretPers Atys
%#= Quel funeste couroux!
\livretPers Atys et Sangaride
%# Pourquoi m'abandonner pour une amour nouvelle?
%# Ce n'est pas moi qui rompt une chaîne si belle.
\livretPers Atys
%# Beauté trop cru=elle, c'est vous,
\livretPers Sangaride
%# Amant infidèle, c'est vous,
\livretPers Atys
%# Ah! c'est vous, beauté trop cru=elle,
\livretPers Sangaride
%# Ah! c'est vous amant infidèle.
\livretPers Atys et Sangaride
%# Beauté trop cru=elle, c'est vous,
%# Amant infidèle, c'est vous,
%# Qui rompez des li=ens si doux.
\livretPers Sangaride
%# Vous m'avez immolée =à l'amour de Cybèle.
\livretPers Atys
%# Il est vrai qu'à ses yeux, par un secret effroi,
%# J'ai voulu de nos cœurs cacher l'intelligence:
%# Mais ce n'est que pour vous que j'ai crains sa vengeance,
%# Et je ne la crains pas pour moi.
%# Cybèle m'aime en vain, et c'est vous que j'adore.
\livretPers Sangaride
%# Après votre infidélité,
%# Auriez-vous bien la cru=auté
%# De vouloir me tromper encore?
\livretPers Atys
%# Moi! vous trahir? vous le pensez?
%# Ingrate, que vous m'offensez!
%# Eh bien, il ne faut plus rien taire,
%# Je vais de la dé=esse attirer la colère,
%# M'offrir à sa fureur, puisque vous m'y forcez…
\livretPers Sangaride
%# Ah! demeurez, Atys, mes soupçons sont passés;
%# Vous m'aimez, je le crois, j'en veux être certaine.
%# Je le souhaite assez,
%# Pour le croire sans peine.
\livretPers Atys
%#- Je jure,
\livretPers Sangaride
%#= Je promets,
\livretPers Atys et Sangaride
%# De ne changer jamais.
\livretPers Sangaride
%# Quel tourment de cacher une si belle flamme.
\livretPers Atys
%# Redoublons-en l'ardeur dans le fonds de notre âme.
\livretPers Atys et Sangaride
%# Aimons en secret, aimons-nous:
%# Aimons plus que jamais, en dépit des jaloux.
\livretPers Sangaride
%#- Mon père vient ici,
\livretPers Atys
%#= Que rien ne vous étonne;
%# Servons-nous du pouvoir que Cybèle me donne,
%# Je vais préparer les Zéphirs
%# À suivre nos désirs.

\livretScene SCÈNE CINQUIÈME
\livretDescAtt\column {
  \wordwrap-center {
    Sangaride, Célénus, le Dieux du Fleuve Sangar, troupe de dieux de
    fleuves, de ruisseaux, et de divinités de fontaines.
  }
  \justify\italic {
    Le Fleuve Sangar.  Suite du Fleuve Sangar.  Douze grands dieux de
    fleuves chantants.  Cinq dieux de fleuves jouants de la flûte.
    Quatre divinités de fontaines, et quatre dieux de fleuves
    chantants et dansants.  Quatre divinités de Fontaines.
    Deux dieux de fleuves.  Deux dieux de fleuves dansants ensemble.
    Deux petits dieux de ruisseaux chantants et dansants.
    Quatre petits dieux de ruisseaux dansants.  Six grands dieux de
    fleuves dansants.  Deux vieux dieux de fleuves et deux vieilles
    nymphes de fontaines dansantes.
  }
}
\livretPers Le Dieu du Fleuve Sangar
\livretRef#'EEBOVousQuiPrenezPart
%# Ô vous, qui prenez part au bien de ma famille,
%# Vous, vénérables dieux des fleuves les plus grands,
%# Mes fidèles amis, et mes plus chers parents,
%# Voy=ez quel est l'époux que je donne à ma fille:
%# J'ai pris soin de choisir entre les plus grands rois.
\livretPers Chœur de dieux de fleuves
%# Nous approuvons votre choix.
\livretPers Le Dieu du Fleuve Sangar
%# Il a Neptune pour son père,
%# Les Phrygi=ens suivent ses lois;
%# J'ai cru ne pouvoir faire
%# Un choix plus digne de vous plaire.
\livretPers Chœur de dieux de fleuves
%# Tous, d'une commune voix,
%# Nous approuvons votre choix.
\livretPers Le Dieu du Fleuve Sangar
\livretRef#'EECairQueLonChante
%# Que l'on chante, que l'on danse,
%# Ri=ons tous lorsqu'il le faut;
%# Ce n'est jamais trop tôt
%# Que le plaisir commence.
%# On trouve bientôt la fin
%# Des jours de réjou=issance,
%# On a beau chasser le chagrin,
%# Il revient plutôt qu'on ne pense.
\livretPers Le Dieu du Fleuve Sangar, et le Chœur
%# Que l'on chante, que l'on danse,
%# Ri=ons tous lorsqu'il le faut;
%# Ce n'est jamais trop tôt
%# Que le plaisir commence.
%# Que l'on chante, que l'on danse,
%# Ri=ons tous lorsqu'il le faut.
\livretPers\wordwrap {
  Dieux de fleuves, divinités de fontaines, et de ruisseaux, chantants
  et dansants ensemble.
}
\livretRef#'EEDairLaBeauteLaPlusSevere
%# La beauté la plus sévère
%# Prend pitié d'un long tourment,
%# Et l'amant qui persévère
%# Devient un *heureux amant.
%# Tout est doux, et rien ne coûte
%# Pour un cœur qu'on veut toucher,
%# L'onde se fait une route
%# En s'efforçant d'en chercher,
%# L'eau qui tombe goûte à goûte
%# Perce le plus dur rocher.
\null
\livretRef#'EEEairLhymenSeulNeSauraitPlaire
%# L'*hymen seul ne saurait plaire,
%# Il a beau flatter nos vœux;
%# L'amour seul a droit de faire
%# Les plus doux de tous les nœuds.
%# Il est fier, il est rebelle,
%# Mais il charme tel qu'il est;
%# L'*hymen vient quand on l'appelle,
%# L'amour vient quand il lui plaît.
\null
%# Il n'est point de résistance
%# Dont le temps ne vienne à bout,
%# Et l'effort de la constance
%# À la fin doit vaincre tout.
%# Tout est doux, et rien ne coûte
%# Pour un cœur qu'on veut toucher,
%# L'onde se fait une route
%# En s'efforçant d'en chercher,
%# L'eau qui tombe goûte à goûte
%# Perce le plus dur rocher.
\null
%# L'Amour trouble tout le monde,
%# C'est la source de nos pleurs;
%# C'est un feu brûlant dans l'onde,
%# C'est l'écueil des plus grands cœurs:
%# Il est fier, il est rebelle,
%# Mais il charme tel qu'il est;
%# L'*hymen vient quand on l'appelle,
%# L'amour vient quand il lui plaît.
\livretPersDidas Un dieu de fleuve et une divinité de fontaine, dançent et chantent ensemble
\livretRef#'EEGduneConstanceExtreme
%# D'une constance extrême,
%# Un ruisseau suit son cours;
%# Il en sera de même
%# Du choix de mes amours,
%# Et du moment que j'aime
%# C'est pour aimer toujours.
\null
%# Jamais un cœur volage
%# Ne trouve un *heureux sort,
%# Il n'a point l'avantage
%# D'être longtemps au port,
%# Il cherche encor l'orage
%# Au moment qu'il en sort.
\livretPers Chœur de dieux de fleuves et de divinités de Fontaines
\livretRef#'EEIchoeurUnGrandCalmeEstTropFacheux
%# Un grand calme est trop fâcheux,
%# Nous aimons mieux la tourmente.
%# Que sert un cœur qui s'exempte
%# De tous les soins amoureux?
%# À quoi sert une eau dormante?
%# Un grand calme est trop fâcheux,
%# Nous aimons mieux la tourmente.

\livretScene SCÈNE SIXIÈME
\livretDescAtt\wordwrap-center {
  Atys, troupe de Zéphirs volants, Sangaride, Célénus, Le Dieu du
  Fleuve Sangar, troupe de dieux de fleuves, de ruisseaux, et de
  divinités de fontaines.
}
\livretPers Chœur de dieux de fleuves et de fontaines
\livretRef#'EFAchoeurVenezFormerDesNoeudsCharmants
%# Venez former des nœuds charmants,
%# Atys, venez unir ces bien heureux amants.
\livretPers Atys
%# Cet *hymen déplaît à Cybèle,
%# Elle défend de l'achever:
%# Sangaride est un bien qu'il faut lui réserver,
%# Et que je demande pour elle.
\livretPers Chœur
%# Ah quelle loi cru=elle!
\livretPers Célénus
%# Atys peut s'engager lui-même à me trahir?
%# Atys contre moi s'interesse?
\livretPers Atys
%# Seigneur, je suis à la dé=esse,
%# Dès qu'elle a commandé, je ne puis qu'obé=ïr.
\livretPers Le Dieu du Fleuve Sangar
%# Pourquoi faut-il qu'elle sépare
%# Deux illustres amants pour qui l'*hymen prépare
%# Ses li=ens les plus doux?
\livretPers Chœur
%# Opposons-nous
%# À ce dessein barbare.
\livretPersDidas Atys élevé sur un nuage
%# Apprenez, audaci=eux,
%# Qu'il n'est rien qui n'obé=ïsse
%# Aux souveraines lois de la reine des dieux.
%# Qu'on nous enlève de ces lieux;
%# Zéphirs, que sans tarder mon ordre s'accomplisse.
\livretDidasP\justify {
  Les Zéphirs volent, et enlèvent Atys et Sangaride.
}
\livretPers Chœur
%# Quelle injustice!
\sep

\livretAct ACTE SECOND
\livretDescAtt\wordwrap-center {
  Le Théâtre change et représente Le temple de Cybèle.
}
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\wordwrap-center {
  Célénus roi de Phrygie. Atys, Suivants de Célénus.
}
\livretPers Célénus
\livretRef#'CABrecitNavancezPasPlusLoin
%# N'avancez pas plus loin, ne suivez point mes pas;
%# Sortez. Toi ne me quitte pas.
%# Atys, il faut attendre ici que la Dé=esse
%# Nomme un grand sacrificateur.
\livretPers Atys
%# Son choix sera pour vous, seigneur; quelle tristesse
%# Semble avoir surpris votre cœur?
\livretPers Célénus
%# Les rois les plus puissants connaissent l'importance
%# D'un si glori=eux choix:
%# Qui pourra l'obtenir étendra sa puissance
%# Partout où de Cybèle on révère les lois.
\livretPers Atys
%# Elle *honore aujourd'*hui ces lieux de sa présence,
%# C'est pour vous préférer aux plus puissants des Rois.
\livretPers Célénus
%# Mais quand j'ai vu tantôt la beauté qui m'enchante,
%# N'as-tu point remarqué comme elle était tremblante?
\livretPers Atys
%# À nos jeux, à nos chants, j'étais trop appliqué,
%# Hors la fête, Seigneur, je n'ai rien remarqué.
\livretPers Célénus
%# Son trouble m'a surpris. Elle t'ouvre son âme;
%# N'y découvres-tu point quelque secrète flamme?
%#- Quelque rival caché?
\livretPers Atys
%#= Seigneur, que dites-vous?
\livretPers Célénus
%# Le seul nom de rival allume mon couroux.
%# J'ai bien peur que le Ciel n'ait pu voir sans envie
%# Le bonheur de ma vie,
%# Et si j'étais aimé mon sort serait trop doux.
%# Ne t'étonne point tant de voir la jalousie
%# Dont mon âme est saisie
%# On ne peut bien aimer sans être un peu jaloux.
\livretPers Atys
%# Seigneur, soy=ez content, que rien ne vous alarme;
%# L'*hymen va vous donner la beauté qui vous charme,
%# Vous serez son heureux époux.
\livretPers Célénus
%# Tu peux me rassurer, Atys, je te veux croire,
%# C'est son cœur que je veux avoir,
%# Dis-moi s'il est en mon pouvoir?
\livretPers Atys
%# Son cœur suit avec soin le devoir et la gloire,
%# Et vous avez pour vous la gloire et le devoir.
\livretPers Célénus
%# Ne me déguise point ce que tu peux connaître.
%# Si j'ai ce que j'aime en ce jour
%# L'*hymen seul m'en rend-il le maître?
%# La gloire et le devoir auront tout fait, peut-être,
%# Et ne laissent pour moi rien à faire à l'amour.
\livretPers Atys
%# Vous aimez d'un amour trop délicat, trop tendre.
\livretPers Célénus
%# L'indifférent Atys ne le saurait comprendre.
\livretPers Atys
%# Qu'un indifférent est *heureux!
%# Il jou=it d'un destin paisible.
%# Le ciel fait un présent bien cher, bien dangeureux,
%# Lorsqu'il donne un cœur trop sensible.
\livretPers Célénus
%# Quand on aime bien tendrement
%# On ne cesse jamais de souffrir, et de craindre;
%# Dans le bonheur le plus charmant,
%# On est ingéni=eux à se faire un tourment,
%# Et l'on prend plaisir à se plaindre.
%# Va songe à mon hymen, et vois si tout est prêt,
%# Laisse-moi seul ici, la Dé=esse paraît.

\livretScene SCÈNE SECONDE
\livretDescAtt\wordwrap-center {
  Cybèle, Célénus, Mélisse, troupe de prêtresses de Cybèle.
}
\livretPers Cybèle
\livretRef#'CBBrecitJeVeuxJoindreEnCesLieux
%# Je veux joindre en ces lieux la gloire et l'abondance,
%# D'un sacrificateur je veux faire le choix,
%# Et le Roi de Phrygie =aurait la préférence
%# Si je voulais choisir entre les plus grands rois.
%# Le puissant dieu des flots vous donna la naissance,
%# Un peuple renommé s'est mis sous votre loi;
%# Vous avez sans mon choix, d'ailleurs, trop de puissance,
%# Je veux faire un bonheur qui ne soit dû qu'à moi.
%# Vous estimez Atys, et c'est avec justice,
%# Je prétends que mon choix à vos vœux soit propice,
%# C'est Atys que je veux choisir.
\livretPers Célénus
%# J'aime Atys, et je vois sa gloire avec plaisir.
%# Je suis Roi, Neptune est mon père,
%# J'épouse une beauté qui va combler mes vœux:
%# Le souhait qui me reste à faire,
%# C'est de voir mon ami parfaitement *heureux.
\livretPers Cybèle
%# Il m'est doux que mon choix à vos désirs réponde;
%# Une grande divinité
%# Doit faire sa félicité
%# Du bien de tout le monde.
%# Mais surtout le bonheur d'un roi chéri des cieux
%# Fait le plus doux plaisir des Dieux.
\livretPers Célénus
%# Le sang approche Atys de la Nymphe que j'aime,
%# Son mérite l'égale aux rois:
%# Il soutiendra mieux que moi-même
%# La majesté suprême
%# De vos divines lois.
%# Rien ne pourra troubler son zèle,
%# Son cœur s'est conservé libre jusqu'à ce jour;
%# Il faut tout un cœur pour Cybèle,
%# À peine tout le mien peut suffire à l'amour.
\livretPers Cybèle
%# Portez à votre ami la première nouvelle
%# De l'*honneur éclatant où ma faveur l'appelle.

\livretScene SCÈNE TROISIÈME
\livretDescAtt\wordwrap-center { Cybèle, Mélisse. }
\livretPers Cybèle
\livretRef#'CCArecitTuTetonnesMelisse
%# Tu t’étonnes, Mélisse, et mon choix te surprend?
\livretPers Mélisse
%# Atys vous doit beaucoup, et son bonheur est grand.
\livretPers Cybèle
%# J'ai fait encor pour lui plus que tu ne peux croire.
\livretPers Mélisse
%# Est-il pour un mortel un rang plus glori=eux?
\livretPers Cybèle
%# Tu ne vois que sa moindre gloire;
%# Ce mortel dans mon cœur est au dessus des Dieux.
%# Ce fut au jour fatal de ma dernière fête
%# Que de l'aimable Atys je devins la conquête:
%# Je partis à regret pour retourner aux Cieux,
%# Tout m'y parut changé, rien n'y plut à mes yeux.
%# Je sens un plaisir extrême
%# À revenir dans ces lieux;
%# Où peut-on jamais être mieux,
%# Qu'aux lieux où l'on voit ce qu'on aime.
\livretPers Mélisse
%# Tous les Dieux ont aimé, Cybèle aime à son tour.
%# Vous méprisiez trop l'Amour,
%# Son nom vous semblait étrange,
%# À la fin il vient un jour
%# Où l'Amour se venge.
\livretPers Cybèle
%# J'ai crû me faire un cœur maître de tout son sort,
%# Un cœur toujours exempt de trouble et de tendresse.
\livretPers Mélisse
%# Vous braviez à tort
%# L'Amour qui vous blesse;
%# Le cœur le plus fort
%# À des moments de faiblesse.
%# Mais vous pouviez aimer, et descendre moins bas.
\livretPers Cybèle
%# Non, trop d'égalité rend l'amour sans appas.
%# Quel plus haut rang ai-je à prétendre?
%# Et de quoi mon pouvoir ne vient-il point à bout?
%# Lorsqu'on est au dessus de tout,
%# On se fait pour aimer un plaisir de descendre.
%# Je laisse aux Dieux les biens dans le Ciel preparés,
%# Pour Atys, pour son cœur, je quitte tout sans peine,
%# S'il m'oblige à descendre, un doux penchant m'entraîne;
%# Les cœurs que le destin à le plus separés,
%# Sont ceux qu'Amour unit d'une plus forte chaîne.
%# Fais venir le Sommeil; que lui-même en ce jour,
%# Prenne soin ici de conduire
%# Les Songes qui lui font la cour;
%# Atys ne sait point mon amour,
%# Par un moy=en nouveau je prétends l'en instruire.
\livretDidasP Mélisse se retire.
\livretPers Cybèle
%# Que les plus doux Zéphirs, que les peuples divers,
%# Qui des deux bouts de l'Univers
%# Sont venus me montrer leur zèle,
%# Célèbrent la gloire immortelle
%# Du sacrificateur dont Cybèle a fait choix,
%# Atys doit dispenser mes lois,
%# Honorez le choix de Cybèle.

\livretScene SCÈNE QUATRIÈME
\livretDescAtt\column {
  \justify {
    Les Zéphirs paraissent dans une gloire élevée et brillante. Les
    Peuples différents qui sont venus à la fête de Cybèle entrent dans
    le temple, et tous ensemble s’efforcent d’honorer Atys, qui vient
    revêtu des habits de grand sacrificateur.
  }
  \justify\italic {
    Cinq Zéphirs dansants dans la gloire.
    Huit Zéphirs jouants du hautbois et des cromornes, dans la gloire.
    Troupe de Peuples différents chantants qui accompagnent Atys.
    Six Indiens et six Egyptiens dansants.
  }
}
\livretPers Chœurs des Peuples et des Zéphirs
\livretRef#'CDAchoeurCelebronsLaGloireImmortelle
%# Célébrons la gloire immortelle
%# Du sacrificateur dont Cybèle a fait choix:
%# Atys doit dispenser ses lois,
%# Honorons le choix de Cybèle.
\null
\livretRef#'CDDchoeurQueDevantVous
%# Que devant vous tout s'abaisse, et tout tremble;
%# Vivez *heureux, vos jours sont notre espoir:
%# Rien n'est si beau que de voir ensemble
%# Un grand mérite avec un grand pouvoir.
%# Que l'on bénisse
%# Le Ciel propice,
%# Qui dans vos mains
%# Met le sort des humains.
\livretPers Atys
\livretRef#'CDEindigneQueJeSuis
%# Indigne que je suis des *honneurs qu'on m'adresse,
%# Je dois les recevoir au nom de la Dé=esse;
%# J'ose, puisqu'il lui plaît, lui présenter vos vœux:
%# Pour le prix de votre zèle,
%# Que la puissante Cybèle
%# Vous rende à jamais *heureux.
\livretPers \line { Chœurs des Peuples et des Zephirs. }
\livretRef#'CDFchoeurQueLaPuissanteCybele
%# Que la puissante Cybèle
%# Nous rende à jamais heureux.
\sep


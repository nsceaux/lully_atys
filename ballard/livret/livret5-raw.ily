\livretAct ACTE CINQUIÈME
\livretDescAtt\wordwrap-center {
  Le théâtre change et représente des jardins agréables.
}
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\wordwrap-center { Célénus, Cybèle, Mélisse. }
\livretPers Célénus
\livretRef#'FABvousMotezSangaride
%# Vous m'ôtez Sangaride? in*humaine Cybèle;
%# Est-ce le prix du zèle
%# Que j'ai fait avec soin éclater à vos yeux?
%# Préparez-vous ainsi la douceur éternelle
%# Dont vous devez combler ces lieux?
%# Est-ce ainsi que les rois sont protegés des dieux?
%# Divinité cru=elle,
%# Descendez-vous exprès des cieux
%# Pour troubler un amour fidèle?
%# Et pour venir m'ôter ce que j'aime le mieux?
\livretPers Cybèle
%# J'aimais Atys, l'amour a fait mon injustice;
%# Il a pris soin de mon supplice;
%# Et si vous êtes outragé,
%# Bientôt vous serez trop vengé.
%# Atys adore Sangaride.
\livretPers Célénus
%# Atys l'adore? ah le perfide!
\livretPers Cybèle
%# L'ingrat vous trahissait, et voulait me trahir:
%# Il s'est trompé lui-même en croy=ant m'éblou=ir.
%# Les Zéphirs l'ont laissé, seul, avec ce qu'il aime,
%# Dans ces aimables lieux;
%# Je m'y suis cachée =à leurs yeux;
%# J'y viens d'être témoin de leur amour extrême.
\livretPers Célénus
%# Ô Ciel! Atys plairait aux yeux qui m'ont charmé?
\livretPers Cybèle
%# Eh pouvez-vous douter qu'Atys ne soit aimé?
%# Non, non, jamais amour n'eût tant de vi=olence,
%# Ils ont juré cent fois de s'aimer malgré nous,
%# Et de braver notre vengeance;
%# Ils nous ont appelés cru=els, tyrans, jaloux;
%# Enfin leurs cœurs d'intelligence,
%# Tous deux… ah je frémis au moment que j'y pense!
%# Tous deux s'abandonnaient à des transports si doux,
%# Que je n'ai pû garder plus longtemps le silence,
%# Ni retenir l'éclat de mon juste couroux.
\livretPers Célénus
%# La mort est pour leur crime une peine légère.
\livretPers Cybèle
%# Mon cœur à les punir est assez engagé;
%# Je vous l'ai déja dit, croy=ez-en ma colère,
%# Bientôt vous serez trop vengé.

\livretScene SCÈNE SECONDE
\livretDescAtt\wordwrap-center {
  Atys, Sangaride, Cybèle, Célénus, Mélisse,
  troupe de prêtresses de Cybèle.
}
\livretPers Cybèle et Célénus
\livretRef#'FBAvenezVousLivrerAuSupplice
%# Venez vous livrer au supplice.
\livretPers Atys et Sangaride
%# Quoi la terre et le ciel contre nous sont armés?
%# Souffrirez-vous qu'on nous punisse?
\livretPers Cybèle et Célénus
%# Oubli=ez-vous votre injustice?
\livretPers Atys et Sangaride
%# Ne vous souvient-il plus de nous avoir aimés?
\livretPers Cybèle et Célénus
%# Vous changez mon amour en haine légitime.
\livretPers Atys et Sangaride
%# Pouvez-vous condamner
%# L'amour qui nous anime?
%# Si c'est un crime,
%# Quel crime est plus à pardonner?
\livretPers Cybèle et Célénus
%# Perfide, deviez-vous me taire
%# Que c'était vainement que je voulais vous plaire?
\livretPers Atys et Sangaride
%# Ne pouvant suivre vos désirs,
%# Nous croy=ons ne pouvoir mieux faire
%# Que de vous épargner de mortels déplaisirs.
\livretPers Cybèle
%# D'un supplice cru=el craignez l'*horreur extrême.
\livretPers Cibèle et Célénus
%# Craignez un funeste trépas.
\livretPers Atys et Sangaride
%# Vengez-vous, s'il le faut, ne me pardonnez pas,
%# Mais pardonnez à ce que j'aime.
\livretPers Cybèle et Célénus
%# C'est peu de nous trahir, vous nous bravez, ingrats?
\livretPers Atys et Sangaride
%#- Serez-vous sans pitié?
\livretPers Cybèle et Célénus
%#= Perdez toute espérance.
\livretPers Atys et Sangaride
%# L'amour nous a forcé =à vous faire une offense,
%# Il demande grâce pour nous.
\livretPers Cybèle et Célénus
%# L'Amour en couroux
%# Demande vengeance.
\livretPers Cybèle
%# Toi, qui portes partout et la rage et l'*horreur,
%# Cesse de tourmenter les criminelles ombres,
%# Viens, cru=elle Alecton, sors des roy=aumes sombres,
%# Inspire au cœur d'Atys ta barbare fureur.

\livretScene SCÈNE TROISIÈME
\livretDescAtt\wordwrap-center {
  Alecton, Atys, Sangaride, Cybèle, Célénus, Mélisse,
  Idas, Doris, troupe de prêtresses de Cybèle, chœur de Phrygiens.
}
\livretRef#'FCApreludePourAlecton
\livretDidasPPage\justify {
  Alecton sort des Enfers, tenant à la main un flambeau qu’elle secoue
  en volant et en passant au dessus d’Atys.
}
\livretPers Atys
\livretRef#'FCBcielQuelleVapeurMenvironne
%# Ciel! quelle vapeur m'environne!
%# Tous mes sens sont troublés, je frémis, je frissonne,
%# Je tremble, et tout à coup, une infernale ardeur
%# Vient enflammer mon sang, et dévorer mon cœur.
%# Dieux! que vois-je? le ciel s'arme contre la terre?
%# Quel désordre! quel bruit! quel éclat de tonnerre!
%# Quels abîmes profonds sous mes pas sont ouverts!
%# Que de fantômes vains sont sortis des Enfers!
\livretDidasP\wordwrap { Il parle à Cybèle, qu’il prend pour Sangaride. }
%# Sangaride, ah fuy=ez la mort que vous prépare
%# Une divinité barbare:
%# C'est votre seul péril qui cause ma terreur.
\livretPers Sangaride
%# Atys reconnaissez votre funeste erreur.
\livretPersDidas Atys prenant Sangaride pour un monstre
%# Quel monstre vient à nous! quelle fureur le guide!
%# Ah respecte, cru=el, l'aimable Sangaride.
\livretPers Sangaride
%#- Atys, mon cher Atys.
\livretPers Atys
%#= Quels *hurlements affreux!
\livretPersDidas Célénus à Sangaride
%# Fuy=ez, sauvez-vous de sa rage.
\livretPersDidas Atys tenant à la main le couteau sacré qui sert aux sacrifices.
%# Il faut combattre; Amour, seconde mon courage.
\livretDidasP\wordwrap {
  Atys court après Sangaride qui fuit dans un des côtés du théâtre.
}
\livretPers Célénus et le chœur
%# Arrête, arrête mal*heureux.
\livretDidasP Célénus court après Atys.
\livretPersDidas Sangaride dans un des côtés du théâtre.
%#- Atys!
\livretPers Le chœur
%#- Ô ciel
\livretPers Sangaride
%#- Je meurs.
\livretPers Le Chœur
%#= Atys, Atys lui-même,
%# Fait périr ce qu'il aime!
\livretPersDidas Célénus revenant sur le théâtre
%# Je n'ai pu retenir ses efforts furi=eux,
%# Sangaride expire à vos yeux.
\livretPers Cybèle
%# Atys me sacrifie =une indigne rivale.
%# Partagez avec moi la douceur sans égale,
%# Que l'on goûte en vengeant un amour outragé.
%#- Je vous l'avais promis.
\livretPers Célénus
%#= Ô promesse fatale!
%# Sangaride n'est plus, et je suis trop vengé.
\livretDidasP\wordwrap {
  Célénus se retire au côté du théâtre, où est Sangaride morte.
}

\livretScene SCÈNE QUATRIÈME
\livretDescAtt\wordwrap-center {
  Atys, Cybèle, Mélisse, Idas, chœur de Phrygiens.
}
\livretPers Atys
\livretRef#'FDAqueJeViensDimmoler
%# Que je viens d'immoler une grande victime!
%# Sangaride est sauvée, =et c'est par ma valeur.
\livretPersDidas Cybèle touchant Atys
%# Achève ma vengeance, Atys, connais ton crime,
%# Et reprends ta raison pour sentir ton mal*heur.
\livretPers Atys
%# Un calme *heureux succède aux troubles de mon cœur.
%# Sangaride, nymphe charmante,
%# Qu'êtes-vous devenue? =où puis-je avoir recours?
%# Divinité toute puissante,
%# Cybèle, ay=ez pitié de nos tendres amours,
%# Rendez-moi Sangaride, épargnez ses beaux jours.
\livretPersDidas Cybèle montrant à Atys Sangride morte
%#- Tu la peux voir, regarde.
\livretPers Atys
%#= Ah quelle barbarie!
%# Sangaride a perdu la vie!
%# Ah quelle main cru=elle! ah quel cœur inhumain!…
\livretPers Cybèle
%# Les coups dont elle meurt sont de ta propre main.
\livretPers Atys
%# Moi, j'aurais immolé la beauté qui m'enchante?
%# Ô Ciel! ma main sanglante
%# Est de ce crime *horrible un témoin trop certain!
\livretPers Le Chœur
%# Atys, Atys lui-même,
%# Fait périr ce qu'il aime.
\livretPers Atys
%# Quoi, Sangaride est morte? Atys est son bourreau!
%# Quelle vengeance ô dieux! quel supplice nouveau!
%# Quelles *horreurs sont comparables
%# Aux horreurs que je sens?
%# Dieux cru=els, dieux impitoy=ables,
%# N'êtes-vous tout-puissants
%# Que pour faire des misérables?
\livretPers Cybèle
%# Atys, je vous ai trop aimé:
%# Cet amour par vous-même en couroux transformé
%# Fait voir encor sa vi=olence:
%# Jugez, ingrat, jugez en ce funeste jour,
%# De la grandeur de mon amour
%# Par la grandeur de ma vengeance.
\livretPers Atys
%# Barbare! quel amour qui prend soin d'inventer
%# Les plus *horribles maux que la rage peut faire!
%# Bien *heureux qui peut éviter
%# Le malheur de vous plaire.
%# Ô dieux! injustes dieux! que n'êtes-vous mortels?
%# Faut-il que pour vous seuls vous gardiez la vengeance?
%# C'est trop, c'est trop souffrir leur cru=elle puissance,
%# Chassons-les d'ici bas, renversons leurs autels.
%# Quoi, Sangaride est morte? Atys, Atys lui-même
%# Fait périr ce qu'il aime?
\livretPers Le Chœur
%# Atys, Atys lui-même
%# Fait périr ce qu'il aime.
\livretPersDidas Cybèle ordonnant d’emporter le corps de Sangaride morte
%#- Ôtez ce triste objet.
\livretPers Atys
%#= Ah ne m'arrachez pas
%# Ce qui reste de tant d'appas!
%# En fussiez-vous jalouse encore,
%# Il faut que je l'adore
%# Jusque dans l'*horreur du trépas.

\livretScene SCÈNE CINQUIÈME
\livretDescAtt\wordwrap-center { Cybèle, Mélisse. }
\livretPers Cybèle
\livretRef#'FEAjeCommenceATrouverSaPeineTropCruelle
%# Je commence à trouver sa peine trop cru=elle,
%# Une tendre pitié rappelle
%# L'Amour que mon couroux croy=ait avoir banni,
%# Ma rivale n'est plus, Atys n'est plus coupable,
%# Qu'il est aisé d'aimer un criminel aimable
%# Après l'avoir puni.
%# Que son désespoir m'épouvante!
%# Ses jours sont en péril, et j'en frémis d'effroi:
%# Je veux d'un soin si cher ne me fi=er qu'à moi,
%# Allons… mais quel spectable à mes yeux se présente?
%# C'est Atys mourant que je vois!

\livretScene SCÈNE SIXIÈME
\livretDescAtt\wordwrap-center {
  Atys, Idas, Cybèle, Mélisse, prêtresses de Cybèle.
}
\livretPersDidas Idas soûtenant Atys
\livretRef#'FFAilSestPerceLeSein
%# Il s'est percé le sein, et mes soins pour sa vie
%# N'ont pu prévenir sa fureur.
\livretPers Cybèle
%# Ah c'est ma barbarie,
%# C'est moi, qui lui perce le cœur.
\livretPers Atys
%# Je meurs, l'amour me guide
%# Dans la nuit du trépas;
%# Je vais où sera Sangaride,
%# Inhumaine, je vais, où vous ne serez pas.
\livretPers Cybèle
%# Atys, il est trop vrai, ma rigueur est extrême,
%# Plaignez-vous, je veux tout souffrir.
%# Pourquoi suis-je immortelle en vous voy=ant périr?
\livretPers Atys et Cybèle
%# Il est doux de mourir
%# Avec ce que l'on aime.
\livretPers Cybèle
%# Que mon amour funeste armé contre moi-même,
%# Ne peut-il vous venger de toutes mes rigueurs.
\livretPers Atys
%# Je suis assez vengé, vous m'aimez, et je meurs.
\livretPers Cybèle
%# Malgré le destin implacable
%# Qui rend de ton trépas l'arrêt irrévocable,
%# Atys, sois à jamais l'objet de mes amours:
%# Reprends un sort nouveau, deviens un arbre aimable
%# Que Cybèle aimera toujours.

\livretRef#'FGAritournelle
\livretDidasPPage\wordwrap {
  Atys prend la forme de l’arbre aimé de la déesse Cybèle, que l’on appelle pin.
}
\livretPers Cybèle
\livretRef#'FGBvenezFurieuxCorybantes
%# Venez furi=eux Corybantes,
%# Venez joindre à mes cris vos clameurs éclatantes;
%# Venez nymphes des eaux, venez dieux des forêts,
%# Par vos plaintes les plus touchantes
%# Seconder mes tristes regrets.

\livretScene SCÈNE SEPTIÈME ET DERNIÈRE
\livretDescAtt\column {
  \wordwrap-center {
    Cybèle, troupe de nymphes des eaux, troupe de divinités des bois,
    troupe de Corybantes.
  }
  \justify {
    Quatre nymphes chantantes.
    Huit dieux des bois chantants.
    Quatorze corybantes chantantes.
    Huit corybantes dansantes.
    Trois dieux des bois, dansants.
    Trois nymphes dansantes.
  }
}
\livretPers Cybèle
\livretRef#'FGCairChoeurAtysAimableAtys
%# Atys, l'aimable Atys, malgré tous ses attraits,
%# Descend dans la nuit éternelle;
%# Mais malgré la mort cru=elle,
%# L'amour de Cybèle
%# Ne mourra jamais.
%# Sous une nouvelle figure,
%# Atys est ranimé par mon pouvoir divin;
%# Célébrez son nouveau destin,
%# Pleurez sa funeste aventure.
\livretPers Chœur des nymphes des eaux et des divinités des bois
%# Célébrons son nouveau destin,
%# Pleurons sa funeste aventure.
\livretPers Cybèle
%# Que cet arbre sacré
%# Soit révéré
%# De toute la nature.
%# Qu'il s'élève au-dessus des arbres les plus beaux:
%# Qu'il soit voisin des cieux, qu'il règne sur les eaux;
%# Qu'il ne puisse brûler que d'une flamme pure.
%# Que cet Arbre sacré
%# Soit révéré
%# De toute la nature.
\livretDidasP\wordwrap { Le chœur répète ces trois derniers vers. }
\livretPers Cybèle
%# Que ses rameaux soient toujours verts:
%# Que les plus rigoureux *hivers
%# Ne leur fassent jamais d'injure.
%# Que cet arbre sacré
%# Soit révéré
%# De toute la nature.
\livretDidasP\wordwrap { Le chœur répète ces trois derniers vers. }
\livretPers Cybèle et le chœur des divinités des bois et des eaux
%# Quelle douleur!
\livretPers Cybèle et le chœur des corybantes
%# Ah! quelle rage!
\livretPers Cybèle et les chœurs
%# Ah! quel malheur!
\livretPers Cybèle
%# Atys au printemps de son âge,
%# Périt comme une fleur
%# Qu'un soudain orage
%# Renverse et ravage.
\livretPers Cybèle et le chœur des divinités des bois et des eaux
%# Quelle douleur!
\livretPers Cybèle et le chœur des corybantes
%# Ah! quelle rage!
\livretPers Cybèle et les chœurs
%# Ah! quel malheur!
\livretRef#'FGDentreeNymphes
\livretDidasPPage\justify {
  Les divinités des bois et des eaux, avec les Corybantes, honorent le
  nouvel arbre, et le consacrent à Cybèle. Les regrets des divinités
  des bois et des eaux, et les cris des Corybantes, sont secondés et
  terminés par des tremblements de terre, par des éclairs, et par des
  éclats de tonnerre.
}
\livretPers Cybèle et le chœur des divinités des bois et des eaux
\livretRef#'FGGchoeurQueLeMalheurDAtys
%# Que le mal*heur d'Atys afflige tout le monde.
\livretPers Cybèle et le chœur des Corybantes
%# Que tout sente, ici bas,
%# L'*horreur d'un si cru=el trépas.
\livretPers Cybèle et le chœur des divinités des bois et des eaux
%# Pénétrons tous les cœurs d'une douleur profonde:
%# Que les bois, que les eaux, perdent tous leurs appas.
\livretPers Cybèle et le chœur des Corybantes
%# Que le tonnerre nous réponde:
%# Que la terre frémisse, et tremble sous nos pas.
\livretPers Cybèle et le chœur des divinités des bois et des eaux
%# Que le malheur d'Atys afflige tout le monde.
\livretPers Tous ensemble
%# Que tout sente, ici bas,
%# L'*horreur d'un si cru=el trépas.
\livretFinAct FIN.

\livretAct PROLOGUE
\livretDescAtt\wordwrap-center {
  Le Théâtre représente le palais du Temps, où ce dieu paraît au
  milieu des douze heures du jour, et des douze heures de la nuit.
}
\livretPers Le Temps
\livretRef#'AABrecitEnVainJaiRespecte
%# En vain j'ai respecté la célèbre mémoire
%# Des héros des siècles passés;
%# C'est en vain que leurs noms si fameux dans l'*histoire,
%# Du sort des noms communs ont été dispensés:
%# Nous voy=ons un héros dont la brillante gloire
%# Les a presque tous effacés.

\livretPers Chœur des Heures
\livretRef#'AACchoeurSesJustesLois
%# Ses justes lois,
%# Ses grands exploits
%# Rendent sa mémoire éternelle:
%# Chaque jour, chaque instant
%# Ajoute encore à son nom éclatant
%# Une gloire nouvelle.

\livretRef#'AADairFlore
\livretDidasPPage\justify {
  La Déesse Flore conduite par un des Zéphirs s’avance avec une Troupe
  de Nymphes qui portent divers ornements de Fleurs.
}
\livretPers Le Temps
\livretRef#'AAErecitLaSaisonDesFrimas
%# La saison des frimas peut-elle nous offrir
%# Les fleurs que nous voy=ons paraître?
%# Quel dieu les fait renaître
%# Lorsque l'*hiver les fait mourir?
%# Le froid cru=el règne encore;
%# Tout est glacé dans les champs,
%# D'où vient que Flore
%# Devance le printemps?
\livretPers Flore
%# Quand j'attends les beaux jours, je viens toujours trop tard,
%# Plus le printemps s'avance, et plus il m'est contraire;
%# Son retour presse le départ
%# Du héros à qui je veux plaire.
%# Pour lui faire ma cour, mes soins ont entrepris
%# De braver désormais l'*hiver le plus terrible,
%# Dans l'ardeur de lui plaire on a bientôt appris
%# À ne rien trouver d'impossible.

\livretPers Le Temps et Flore
\livretRef#'AAFairChoeurLesPlaisirASesYeux
%# Les plaisirs à ses yeux ont beau se présenter,
%# Si tôt qu'il voit Bellone, il quitte tout pour elle;
%# Rien ne peut l'arrêter
%# Quand la gloire l'appelle.
\livretDidasP\wordwrap {
  Le chœur des heures répète ces deux derniers vers.
}

\livretRef#'AAGgavotte
\livretDidasPPage\wordwrap {
  La suite de Flore commence des jeux mêlés de danses et de chants.
}

\livretPers Un Zéphir
\livretRef#'AAHairLePrintempsQuelqueFois
%# Le printemps quelque fois est moins doux qu'il ne semble,
%# Il fait trop pay=er ses beaux jours;
%# Il vient pour écarter les jeux et les amours,
%# Et c'est l'*hiver qui les rassemble.

\livretRef#'AAIpreludeMelpomene
\livretDidasPPage\justify {
  Melpomène qui est la muse qui préside à la tragédie, vient
  accompagnée d’une troupe de héros, elle est suivie d’Hercule,
  d’Antæe, de Castor, de Pollux, de Lincée, d’Idas, d’Étéocle,
  et de Polinice.
}
\livretPersDidas Melpomène parlant à Flore
\livretRef#'AAJrecitRetirezVous
%# Retirez-vous, cessez de prévenir le Temps;
%# Ne me dérobez point de préci=eux instants:
%# La puissante Cybèle
%# Pour honorer Atys qu'elle a privé du jour,
%# Veut que je renouvelle
%# Dans une illustre cour
%# Le souvenir de son amour.
%# Que l'agrément rustique
%# De Flore et de ses jeux,
%# Cède à l'appareil magnifique
%# De la muse tragique,
%# Et de ses spectacles pompeux.

\livretRef#'AAKairMelpomene
\livretDidasPPage\justify {
  La suite de Melpomène prend la place de la suite de Flore.  Les
  héros recommencent leurs anciennes querelles.  Hercule combat et
  lutte contre Antæe, Castor et Pollux combattent contre Lyncée et
  Idas, et Étéocle combat contre son frère Polynice.
}
\livretRef#'AALcybeleVeutQueFlore
\livretDidasPPage\justify {
  Iris, par l’ordre de Cybèle, descend assis
  sur son arc, pour accorder Melpomène et Flore.
}
\livretPersDidas Iris parlant à Melpomène
%# Cybèle veut que Flore aujourd'hui vous seconde.
%# Il faut que les plaisirs viennent de toutes parts,
%# Dans l'empire puissant, où règne un nouveau Mars,
%# Ils n'ont plus d'autre asile au monde.
%# Rendez-vous, s'il se peut, dignes de ses regards;
%# Joignez la beauté vive et pure
%# Dont brille la nature,
%# Aux ornements des plus beaux arts.
\livretDidasP\justify {
  Iris remonte au ciel sur son arc, et la suite
  de Melpomène s’accorde avec la suite de Flore.
}
\livretPers Melpomène et Flore
%# Rendons-nous, s'il se peut, dignes de ses regards;
%# Joignons la beauté vive et pure
%# Dont brille la nature,
%# Aux ornements des plus beaux arts.

\livretPers Le Temps, et le Chœur des Heures
%# Préparez de nouvelles fêtes,
%# Profitez du loisir du plus grand des héros;
\livretPers Le Temps, Melpomène et Flore
%#8 Préparez/Préparons de nouvelles fêtes
%#12 Profitez/Profitons du loisir du plus grand des héros.
\livretPers Tous ensemble
%# Le temps des jeux, et du repos,
%# Lui sert à méditer de nouvelles conquêtes.
\sep

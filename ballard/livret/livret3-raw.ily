\livretAct ACTE TROISIÈME
\livretDescAtt\wordwrap-center {
  Le Théâtre change et répresente le palais du sacrificateur de
  Cybèle.
}
\livretScene SCÈNE PREMIÈRE
\livretPersDidas Atys seul
\livretRef#'DAArecitQueServentLesFaveurs
%# Que servent les faveurs que nous fait la fortune
%# Quand l'Amour nous rend mal*heureux?
%# Je perds l'unique bien qui peut combler mes vœux,
%# Et tout autre bien m'importune.
%# Que servent les faveurs que nous fait la fortune
%# Quand l'Amour nous rend mal*heureux?

\livretScene SCÈNE SECONDE
\livretDescAtt\wordwrap-center { Idas, Doris, Atys. }
\livretPers Idas
\livretRef#'DBArecitPeutOnIciParlerSansFeindre
%# Peut-on ici parler sans feindre? 
\livretPers Atys
%# Je commande en ces lieux, vous n'y devez rien craindre.
\livretPers Doris
%#- Mon frère est votre ami.
\livretPers Idas
%#= Fi=ez-vous à ma sœur.
\livretPers Atys
%# Vous devez avec moi partager mon bonheur.
\livretPers Idas et Doris
%# Nous venons partager vos mortelles alarmes;
%# Sangaride les yeux en larmes
%# Nous vient d'ouvrir son cœur.
\livretPers Atys
%# L'*heure approche où l'*hymen voudra qu'elle se livre
%# Au pouvoir d'un heureux époux.
\livretPers Idas et Doris
%# Elle ne peut vivre
%# Pour un autre que pour vous.
\livretPers Atys
%# Qui peut la dégager du devoir qui la presse?
\livretPers Idas et Doris
%# Elle veut elle-même aux pieds de la Dé=esse
%# Déclarer hautement vos secrètes amours.
\livretPers Atys
%# Cybèle pour moi s'intéresse,
%# J'ose tout espérer de son divin secours…
%# Mais quoi, trahir le roi! tromper son espérance!
%# De tant de biens reçus est-ce la récompense?
\livretPers Idas et Doris
%# Dans l'empire amoureux
%# Le Devoir n'a point de puissance;
%# L'Amour dispense
%# Les rivaux d'être généreux;
%# Il faut souvent pour devenir *heureux
%# Qu'il en coûte un peu d'innocence.
\livretPers Atys
%# Je souhaite, je crains, je veux, je me repens.
\livretPers Idas et Doris
%# Verrez-vous un rival *heureux à vos dépens?
\livretPers Atys
%# Je ne puis me résoudre à cette vi=olence.
\livretPers Atys, Idas et Doris
%# En vain, un cœur, incertain de son choix,
%# Met en balance mille fois
%# L'Amour et la Reconnaissance,
%# L'Amour toujours emporte la balance.
\livretPers Atys
%# Le plus juste parti cède enfin au plus fort.
%# Allez, prenez soin de mon sort,
%# Que Sangaride ici se rende en diligence.

\livretScene SCÈNE TROISIÈME
\livretPers \line { ATYS seul. }
\livretRef#'DCArecitNousPouvonsNousFlatter
%# Nous pouvons nous flatter de l'espoir le plus doux
%# Cybèle et l'Amour sont pour nous.
%# Mais du devoir trahi j'entends la voix pressante
%# Qui m'accuse et qui m'épouvante.
%# Laisse-mon cœur en paix, impuissante Vertu,
%# N'ai-je point assez combattu?
%# Quand l'Amour malgré toi me contraint à me rendre,
%# Que me demandes-tu?
%# Puisque tu ne peux me défendre,
%# Que me sert-il d'entendre
%# Les vains repproches que tu fais?
%# Impuissante Vertu laisse mon cœur en paix.
%# Mais le Sommeil vient me surprendre,
%# Je combats vainement sa charmante douceur.
%# Il faut laisser suspendre
%# Les troubles de mon cœur.
\livretDidasP Atys descend.

\livretScene SCÈNE QUATRIÈME
\livretDescAtt \justify {
  Le théâtre change et représente un antre entouré de pavots et de
  ruisseaux, où le dieu du sommeil se vient rendre accompagné des
  songes agréables et funestes.
}
\livretDescAtt \column {
  \wordwrap-center {
    Atys dormant. Le Sommeil, Morphée, Phobetor, Phantase,
    Les songes heureux. Les songes funestes.
  }
  \justify\italic {
    Deux songes jouants de la viole.  Deux songes jouants du théorbe.
    Six songes jouants de la Flûte.  Douze songes funestes chantants.
    Huit songes agréables dansants.  Huit Songes funestes dansants.
  }
}
\livretPers Le Sommeil
\livretRef#'DDAdormons
%# Dormons, dormons tous;
%# Ah que le repos est doux!
\livretPers Morphée
%# Régnez, divin Sommeil, régnez sur tout le monde,
%# Répandez vos pavots les plus assoupissants;
%# Calmez les soins, charmez les sens,
%# Retenez tous les cœurs dans une paix profonde.
\livretPers Phobétor
%# Ne vous faites point vi=olence,
%# Coulez, murmurez, clairs ruisseaux,
%# Il n'est permis qu'au bruit des eaux
%# De troubler la douceur d'un si charmant silence.
\livretPers Le Sommeil, Morphée, Phobétor et Phantase
%# Dormons, dormons tous,
%# Ah que le repos est doux!
\livretDidasP\justify {
  Les songes agréables approchent d’Atys, et par leurs chants, et par
  leurs danses, lui font connaître l’amour de Cybèle, et le bonheur
  qu’il en doit espérer.
}
\livretPers Morphée
\livretRef#'DDBecouteAtys
%# Écoute, écoute Atys la gloire qui t'appelle,
%# Sois sensible à l'*honneur d'être aimé de Cybèle,
%# Jou=is *heureux Atys de ta félicité.
\livretPers Morphée, Phobétor et Phantase
%# Mais souviens-toi que la beauté
%# Quand elle est immortelle,
%# Demande la fidélité
%# D'une amour éternelle.
\livretPers Phantase
%# Que l'amour a d'attraits
%# Lorsqu'il commence
%# À faire sentir sa puissance,
%# Que l'Amour a d'attraits
%# Lorsqu'il commence
%# Pour ne finir jamais.
\livretPers Morphée
\livretRef#'DDDgouteEnPaix
%# Goûte en paix chaque jour une douceur nouvelle,
%# Partage l'*heureux sort d'une divinité,
%# Ne vante plus la liberté,
%# Il n'en est point du prix d'une chaîne si belle:
\livretPers Morphée, Phobétor et Phantase
%# Mais souviens-toi que la beauté
%# Quand elle est immortelle,
%# Demande la fidélité
%# D'une amour éternelle.
\livretPers Phantase
%# Trop *heureux un amant
%# Qu'Amour exempte
%# Des peines d'une longue attente!
%# Trop heureux un amant
%# Qu'Amour exempte
%# De crainte, et de tourment!
\livretDidasP\justify {
  Les songes funestes approchent d’Atys, et le menacent de la
  vengeance de Cybèle s’il méprise son amour, et s’il ne l'aime pas
  avec fidélité.
}
\livretPers Un songe funeste
\livretRef#'DDFgardeToiDoffenser
%# Garde-toi d'offenser un amour glori=eux,
%# C'est pour toi que Cybèle abandonne les cieux
%# Ne trahis point son espérance.
%# Il n'est point pour les Dieux de mépris innocent,
%# Ils sont jaloux des cœurs, ils aiment la vengeance,
%# Il est dangereux qu'on offense
%# Un amour tout-puissant.
\livretPers Chœur de songes funestes
\livretRef#'DDHchoeurLamourQuonOutrage
%# L'amour qu'on outrage
%# Se transforme en rage,
%# Et ne pardonne pas
%# Aux plus charmants appas.
%# Si tu n'aimes point Cybèle
%# D'une amour fidèle,
%# Mal*heureux, que tu souffriras!
%# Tu périras:
%# Crains une vengeance cru=elle,
%# Tremble, crains un affreux trépas.
\livretDidasP\justify {
  Atys épouvanté par les songes funestes, se réveille en sursaut, le
  Sommeil et les songes disparaissent avec l'antre où ils étaient, et
  Atys se retrouve dans le même palais où il s’était endormi.
}

\livretScene SCÈNE CINQUIÈME
\livretDescAtt\wordwrap-center { Atys, Cybèle, et Mélisse. }
\livretPers Atys
\livretRef#'DEArecitVenezAMonSecours
%# Venez à mon secours ô dieux! ô justes dieux!
\livretPers Cybèle
%# Atys, ne craignez rien, Cybèle est en ces lieux.
\livretPers Atys
%# Pardonnez au désordre où mon cœur s'abandonne;
%#- C'est un songe…
\livretPers Cybèle
%#= Parlez, quel songe vous étonne?
%# Expliquez-moi votre embarras.
\livretPers Atys
%# Les songes sont trompeurs, et je ne les crois pas.
%# Les plaisirs et les peines
%# Dont en dormant on est séduit,
%# Sont des chimères vaines
%# Que le réveil détruit.
\livretPers Cybèle
%# Ne méprisez pas tant les songes
%# L'Amour peut emprunter leur voix,
%# S'ils font souvent des mensonges
%# Ils disent vrai quelquefois.
%# Ils parlaient par mon ordre, et vous les devez croire.
\livretPers Atys
%#- O Ciel?
\livretPers Cybèle
%#= N'en doutez point, connaissez votre gloire.
%# Répondez avec liberté,
%# Je vous demande un cœur qui dépend de lui-même.
\livretPers Atys
%# Une grande divinité
%# Doit d'assurer toujours de mon respect extrême.
\livretPers Cybèle
%# Les dieux dans leur grandeur suprême
%# Reçoivent tant d'*honneurs qu'ils en sont rebutés,
%# Ils se lassent souvent d'être trop respectés,
%# Ils sont plus contents qu'on les aime.
\livretPers Atys
%# Je sais trop ce que je vous dois
%# Pour manquer de reconnaissance…

\livretScene SCÈNE SIXIÈME
\livretDescAtt\wordwrap-center { Sangaride, Cybèle, Atys, Mélisse. }
\livretPersDidas Sangaride se jetant aux pieds de Cybèle
\livretRef#'DFArecitJaiRecoursAVotrePuissance
%# J'ai recours à votre puissance,
%# Reine des Dieux, protégez-moi.
%# L'intérêt d'Atys vous en presse…
\livretPersDidas Atys interrompant Sangaride
%# Je parlerai pour vous, que votre crainte cesse.
\livretPers Sangaride
%# Tous deux unis des plus beaux nœuds…
\livretPersDidas Atys interrompant Sangaride
%# Le sang et l'amitié nous unissent tous deux.
%# Que votre secours la délivre
%# Des lois d'un *hymen rigoureux,
%# Ce sont les plus doux de ses vœux
%# De pouvoir à jamais vous servir et vous suivre.
\livretPers Cybèle
%# Les dieux sont les protecteurs
%# De la liberté des cœurs.
%# Allez, ne craignez point le roi ni sa colère,
%# J'aurai soin d'apaiser
%# Le Fleuve Sangar votre père;
%# Atys veut vous favoriser,
%# Cybèle en sa faveur ne peut rien refuser.
\livretPers Atys
%#- Ah! c'en est trop…
\livretPers Cybèle
%#= Non, non, il n'est pas nécessaire
%# Que vous cachiez votre bonheur,
%# Je ne prétends point faire
%# Un vain mystère
%# D'un amour qui vous fait *honneur.
%# Ce n'est point à Cybèle à craindre d'en trop dire.
%# Il est vrai, j'aime Atys, pour lui j'ai tout quitté,
%# Sans lui je ne veux plus de grandeur ni d'empire,
%# Pour ma félicité
%# Son cœur seul peut suffire.
%# Allez, Atys lui-même ira vous garantir
%# De la fatale vi=olence
%# Où vous ne pouvez consentir.
\livretDidasP\line { Sangaride se retire. }
\livretPersDidas Cybèle parle à Atys
%# Laissez-nous, attendez mes ordres pour partir,
%# Je prétends vous armer de ma toute puissance.

\livretScene SCÈNE SEPTIÈME
\livretDescAtt\wordwrap-center { Cybèle, Mélisse. }
\livretPers Cybèle
\livretRef#'DGArecitQuAtysDansSesRespects
%# Qu'Atys dans ses respects mêle d'indifférence!
%# L'ingrat Atys ne m'aime pas;
%# L'Amour veut de l'amour, tout autre prix l'offense,
%# Et souvent le respect et la reconnaissance
%# Sont l'excuse des cœurs ingrats.
\livretPers Mélisse
%# Ce n'est pas un si grand crime
%# De ne s'exprimer pas bien,
%# Un cœur qui n'aima jamais rien
%# Sait peu comment l'amour s'exprime.
\livretPers Cybèle
%# Sangaride est aimable, Atys peut tout charmer,
%# Ils témoignent trop s'estimer,
%# Et de simples parents sont moins d'intelligence:
%# Ils se sont aimés dès l'enfance,
%# Ils pourraient enfin trop s'aimer.
%# Je crains une amitié que tant d'ardeur anime.
%# Rien n'est si trompeur que l'estime:
%# C'est un nom supposé
%# Qu'on donne quelquefois à l'amour déguisé.
%# Je prétends m'éclaircir leur feinte sera vaine.
\livretPers Mélisse
%# Quels secrets par les dieux ne sont point pénétrés?
%# Deux cœurs à feindre préparés
%# Ont beau cacher leur chaîne,
%# On abuse avec peine
%# Les Dieux par l'amour éclairés.
\livretPers Cybèle
%# Va, Mélisse, donne ordre à l'aimable Zéphire
%# D'accomplir promptement tout ce qu'Atys désire.

\livretScene SCÈNE HUITIÈME
\livretPersDidas Cybèle seule
\livretRef#'DHAespoirSiCherEtSiDoux
%# Espoir si cher, et si doux,
%# Ah! pourquoi me trompez-vous?
%# Des suprêmes grandeurs vous m'avez fait descendre,
%# Mille cœurs m'adoraient, je les néglige tous,
%# Je n'en demande qu'un, il a peine à se rendre;
%# Je ne sens que chagrin, et que soupçons jaloux;
%# Est-ce le sort charmant que je devais attendre?
%# Espoir si cher, et si doux,
%# Ah! pourquoi me trompez-vous?
%# Hélas! par tant d'attraits fallait-il me surprendre?
%# Heureuse, si toujours j'avais pu m'en défendre!
%# L'Amour qui me flattait me cachait son couroux:
%# C'est donc pour me frapper des plus funestes coups,
%# Que le cru=el Amour m'a fait un cœur si tendre?
%# Espoir si cher, et si doux,
%# Ah! pourquoi me trompez-vous?
\sep

\clef "haute-contre" sol'2. do''4 |
re''2 mi'' |
do''2. do''4 |
si'2 si'4. si'8 |
dod''2 re''4. re''8 |
re''2 do''4. do''8 |
do''2 sib'8 do'' re''4 |
mib''2 re''4. re''8 |
si'4. sol'8 sol'2 |
sol' si'4. si'8 |
do''2 do''4. do''8 |
do''2. do''4 |
do''2 la' |
sol'4. si'8 si'4. dod''8 |
re''2 la'4 si' |
do''4. re''8 mi''4 re''8 do'' |
si'4 do''8 re'' si'4. do''8 |
do''1 |

\clef "dessus" re''4. re''8 si'4.\trill re''8 |
sol'4. sol'8 la'4. si'8 |
do''4. do''8 do''4. re''8 |
si'4.\trill re''8 mi''4. fad''8 |
sol''4. sol''8 sol''4. la''8 |
fad''4.\trill mi''8 mi''4.\tr re''8 |
re''1 |
re'' |
fad''8(\trill mi''16 fad'') sol''8. fad''16 mi''8. mi''16 mi''8. fad''16 |
red''8. dod''16 si'8. si''16 si''8. la''16 sol''8.\trill fad''16 |
sol''8. fad''16 mi''8. mi''16 la''8. la''16 la''8. mi''16 |
fa''8. fa''16 fa''8. mi''16 re''8. dod''16 re''8. mi''16 |
%{ do''8. %}dod''8.\trill si'16 la'8. mi''16 mi''8. fad''16 sol''8. la''16 |
fad''4\trill si''8. si''16 mi''8. la''16 fad''8.\trill sol''16 |
%sol''1 |
sol''1 |

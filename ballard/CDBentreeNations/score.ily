\score {
  \new StaffGroup <<
    \new Staff <<
      { s1*14 \footnoteHere #'(0 . 0) \markup {
          [Manuscript] Reprise à la fin de l’entrée.
        }
      }
      \global \includeNotes "dessus"
    >>
    \new Staff << \global \includeNotes "haute-contre" >>
    \new Staff << \global \includeNotes "taille" >>
    \new Staff << \global \includeNotes "quinte" >>
    \new Staff << \global \includeNotes "basse" >>
  >>
  \layout { }
  \midi { }
}

% Atys
\tag #'(voix1 basse) {
  Je sais trop ce que je vous dois
  pour man -- quer de re -- con -- nais -- san -- ce…
}
%% ACTE 3 SCENE 6
% Sangaride se jettant aux pieds de Cybele.
\tag #'(voix1 basse) {
  J’ai re -- cours à vo -- tre puis -- san -- ce,
  rei -- ne des dieux, pro -- té -- gez- moi.
  L’in -- té -- rêt d’A -- tys vous en pres -- se…
}
% Atys interrompant Sangaride.
\tag #'(voix2 basse) {
  Je par -- le -- rai pour vous, que vo -- tre crain -- te ces -- se.
}
% Sangaride
\tag #'(voix1 basse) {
  Tous deux u -- nis des plus beaux nœuds…
}
% Atys interrompant Sangaride.
\tag #'(voix2 basse) {
  Le sang et l’a -- mi -- tié nous u -- nis -- sent tous deux.
  Que vo -- tre se -- cours la dé -- li -- vre
  des lois d’un hy -- men ri -- gou -- reux,
  ce sont les plus doux de ses vœux
  de pou -- voir à ja -- mais vous ser -- vir et vous sui -- vre.
}
% Cybele
\tag #'(voix1 basse) {
  Les dieux sont les pro -- tec -- teurs
  de la li -- ber -- té des cœurs.
}
% Sangaride et Atys
\tag #'(voix1 voix2 basse) {
  Les dieux sont les pro -- tec -- teurs
  de la li -- ber -- té des cœurs.
}
% Cybele
\tag #'(voix1 basse) {
  Al -- lez, ne crai -- gnez point le roi ni sa co -- lè -- re,
  j’au -- rai soin d’a -- pai -- ser
  le Fleu -- ve San -- gar vo -- tre pè -- re ;
  A -- tys veut vous fa -- vo -- ri -- ser,
  Cy -- bèle en sa fa -- veur ne peut rien re -- fu -- ser.
}
% Atys
\tag #'(voix1 basse) {
  Ah ! c’en est trop…
}
% Cybele
\tag #'(voix1 basse) {
  Non, non, il n’est pas né -- ces -- sai -- re
  que vous ca -- chiez vo -- tre bon -- heur,
  je ne pré -- tends point fai -- re
  un vain mys -- tè -- re
  d’un a -- mour qui vous fait hon -- neur.
  Ce n’est point à Cy -- bèle à crain -- dre d’en trop di -- re.
  Il est vrai, j’aime A -- tys, pour lui j’ai tout quit -- té,
  sans lui je ne veux plus de gran -- deur ni d’em -- pi -- re,
  pour ma fé -- li -- ci -- té
  son cœur seul peut suf -- fi -- re.
  Al -- lez, A -- tys lui- même i -- ra vous ga -- ran -- tir
  de la fa -- ta -- le vi -- o -- len -- ce
  où vous ne pou -- vez con -- sen -- tir.
  % Sangaride se retire.
  % Cybele parle à Atys.
  Lais -- sez- nous, at -- ten -- dez mes or -- dres pour par -- tir,
  je pré -- tends vous ar -- mer de ma tou -- te puis -- san -- ce.
}

\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix"
          #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Atys
    \livretVerse#8 { Je sais trop ce que je vous dois }
    \livretVerse#8 { Pour manquer de reconnaissance… }
    \livretPersDidas Sangaride se jetant aux pieds de Cybèle
    \livretVerse#8 { J’ai recours à votre puissance, }
    \livretVerse#8 { Reine des Dieux, protégez-moi. }
    \livretVerse#8 { L’intérêt d’Atys vous en presse… }
    \livretPersDidas Atys interrompant Sangaride
    \livretVerse#12 { Je parlerai pour vous, que votre crainte cesse. }
    \livretPers Sangaride
    \livretVerse#8 { Tous deux unis des plus beaux nœuds… }
    \livretPersDidas Atys interrompant Sangaride
    \livretVerse#12 { Le sang et l’amitié nous unissent tous deux. }
    \livretVerse#8 { Que votre secours la délivre }
    \livretVerse#8 { Des lois d’un hymen rigoureux, }
    \livretVerse#8 { Ce sont les plus doux de ses vœux }
    \livretVerse#12 { De pouvoir à jamais vous servir et vous suivre. }
    \livretPers Cybèle
    \livretVerse#7 { Les dieux sont les protecteurs }
    \livretVerse#7 { De la liberté des cœurs. }
    \livretVerse#12 { Allez, ne craignez point le roi ni sa colère, }
    \livretVerse#6 { J’aurai soin d’apaiser }
    \livretVerse#8 { Le Fleuve Sangar votre père ; }
  }
  \column {
    \null
    \livretVerse#8 { Atys veut vous favoriser, }
    \livretVerse#12 { Cybèle en sa faveur ne peut rien refuser. }
    \livretPers Atys
    \livretVerse#12 { Ah ! c’en est trop… }
    \livretPers Cybèle
    \livretVerse#12 { \transparent { Ah ! c’en est trop… } Non, non, il n’est pas nécessaire }
    \livretVerse#8 { Que vous cachiez votre bonheur, }
    \livretVerse#6 { Je ne prétends point faire }
    \livretVerse#4 { Un vain mystère }
    \livretVerse#8 { D’un amour qui vous fait honneur. }
    \livretVerse#12 { Ce n’est point à Cybèle à craindre d’en trop dire. }
    \livretVerse#12 { Il est vrai, j’aime Atys, pour lui j’ai tout quitté, }
    \livretVerse#12 { Sans lui je ne veux plus de grandeur ni d’empire, }
    \livretVerse#6 { Pour ma félicité }
    \livretVerse#6 { Son cœur seul peut suffire. }
    \livretVerse#12 { Allez, Atys lui-même ira vous garantir }
    \livretVerse#8 { De la fatale violence }
    \livretVerse#8 { Où vous ne pouvez consentir. }
    \livretDidasP\line { Sangaride se retire. }
    \livretPersDidas Cybèle parle à Atys
    \livretVerse#12 { Laissez-nous, attendez mes ordres pour partir, }
    \livretVerse#12 { Je prétends vous armer de ma toute puissance. }
  }
}#}))

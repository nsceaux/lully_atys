<<
  \tag #'(voix1 basse) {
    \atysMark r4 r8 re'16 re' sib8.\trill sib16 sib8 sib16 la |
    la4\trill r8 la16 la re'8. re'16 re'8 re'16 sol |
    la4 la
    \tag #'basse \sangarideMark
    \tag #'voix1 \sangarideMarkText\markup { [se jetant aux pieds de Cybèle] }
    r4 r8 la'16 la' |
    re''8. re''16 re''8 re''16 mi'' fa''4 fa'' |
    mi''\trill mi''8 fa'' re''8.\trill re''16 re''8. mi''16 |
    dod''4\trill r r la'16 la' la' la' |
    fad'4\trill fad'8 sol' sol'4
    <<
      \tag #'basse { sol'8 s8 | s1 | s8 \sangarideMark }
      \tag #'voix1 { sol'4 R1 r8 <>^\markup\character Sangaride }
    >> do''8 do''8. do''16 do''8. do''16 do''8([ si'16)] do'' |
    <<
      \tag #'basse { si'8\trill s2.. | s1*5 | s2. }
      \tag #'voix1 { si'4\trill r r2 | R1*5 | r2 r4 }
    >> \cybeleMark la'4 |
    re''1 |
    sib'4. sib'8 sib'4 la' |
    sol'2\trill do''4. do''8 |
    sib'4. la'8 sol'4.\trill fa'8 |
    fa'2
    \sangarideMark r4 la' |
    fa'1 |
    sib'4. sib'8 sib'4. do''8 |
    la'2\trill la'4 si'! |
    dod'' re'' dod''4. re''8 |
    re''4
    \cybeleMark r8 la' fa'4\trill r16 fa' sol' la' |
    sib'8. sib'16 la'8.\trill la'16 la'8. si'?16 |
    do''8 do'' r la'16 la' re''8 re''16 la' |
    sib'8 sib' sol'\trill sol'16 sol' do''8 do''16 do'' |
    la'8\trill la' r do'' do''8. la'16 la' la' si' do'' |
    si'4\trill r8 si' mi''8. mi''16 mi''8. mi''16 |
    dod''4 dod''8 dod'' re''4 re''8 dod'' |
    re''4
    \atysMark fa'2 re'8 re' |
    la4\trill
    \cybeleMark r8 fa''\trill re''8 sib'16 sib' sib'8 do''16 re'' |
    sol'8\trill sol' r16 sol' sol' la' sib'4 sib'8 sib'16 la' |
    la'4\trill r16 fa'' fa'' mi'' re''8.\trill do''16 |
    si'4\trill si' do'' re''8 mi'' |
    sold' sold' r8 mi''16 fa'' re''8\trill re''16 do'' si'8.\trill la'16 |
    la'4 r4 r8 fa'16 sol' la'8\trill la'16 si' |
    do''8. do''16 re''8. re''16 mi''8. fa''16 |
    mi''8\trill mi'' r do''16 do'' sol'8\trill sol'16 la' |
    sib'4 r8 re'' sib'\trill la' sol'8. fa'16 |
    mi'4\trill r8 la' fa'8. sol'16 la'8. si'!16 |
    do''4 do''8 re'' mi''4\trill mi''8 do'' |
    fa''4 fa''8 r la' la'16 la' si'8. do''16 |
    si'4\trill mi''8. fa''16 re''4 re''8 dod'' |
    re''4 re'' r4 r8 la' |
    fad'8. la'16 la'8. la'16 sib'8. sib'16 sib' sib' do'' re'' |
    sol'4\trill r16 sol' sol' sol' la'8.\trill la'16 la'8 sib' |
    do''8 do'' do''16 do'' do'' re'' sol'8\trill sol'16 la' |
    fa'4 r r \tag #'voix1 <>^\markup\italic\right-align\line { [Sangaride se retire] }
    r8 \tag #'voix1 <>^\markup\italic { [à Atys] } do''16 do'' |
    la'2\trill la'8. la'16 la'8. la'16 |
    fa'8.\trill fa'16 fa'8. mi'16 mi'8\trill la'16 si' |
    dod''8 dod''16 dod'' re''8 re''16 re'' re''8\trill re''16 dod'' |
    re''2 re'' |
    R1*3 |
  }
  \tag #'(voix2 basse) {
    <<
      \tag #'basse { s1*6 s2.. \atysMark }
      \tag #'voix2 {
        \clef "vhaute-contre" R1*6 r2 r4 r8
        <>^\markup\character-text Atys [interrompant Sangaride] }
    >> re'8 |
    re'8. re'16 sib8\trill sib sol8.\trill sol16 sol la sib do' |
    <<
      \tag #'basse { la16\trill la16 s2.. | s8 \atysMark }
      \tag #'voix2 {
        la8\trill la r4 r2 |
        r8 <>^\markup\character-text Atys [interrompant Sangaride]
      }
    >> sol'8 re'16\trill re' re' mi' fa'8 fa'16 mi' re'8\trill re'16 mi' |
    do'4 r8 mi' mi' mi'16 mi' fa'8 fa'16 fa' |
    re'8\trill re' r re' la8 la16 si do'8 do'16 si |
    si4\trill r8 si do' do'16 do' dod'8 dod'16 dod' |
    re'4 re'8. re'16 mi'4 mi'8. mi'16 |
    fa'4 re'8. do'16 si4\trill si8. do'16 |
    la2 la4 
    \tag #'voix2 {
      r4 |
      R1*4 |
      r2 <>^\markup\character Atys r4 fa'4 |
      re'1 |
      re'4. re'8 re'4 mi' |
      fa'2 fa'4 sol' |
      mi' fa' mi'4.\trill re'8 |
      re'4 r r2 |
      R2.*3 R1*6
    }
  }
>>

<<
  %% Cybèle
  \tag #'(cybele) {
    
  }

  %% Atys
  \tag #'(atys) {
  
    <<
      { s4 | s1*10 | s2.*3 | s1*3 | s4 }
      \tag #'atys {
        r4 |
        R1*4 |
        r2 r4 
        \revertNoHaraKiri R2.*3 | R1*3 | r4
      }
    >>
    \tag #'basse 
    \tag #'atys {
      r4 r2 | R1 | R2. | R1 | R1*2 | R2.*2 |
      R1*8 | R2. | R1*2 | R2.*2 | R1*4 |
    }
  }
>>

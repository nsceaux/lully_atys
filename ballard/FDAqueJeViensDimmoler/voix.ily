<<
  \tag #'(recit basse) {
    \atysMark r8 sol'16 sol' mi'8\trill mi'16 mi' do'8 do'16 do' do'8 re'16 mi' |
    fa'8 fa' r la16 si do'8 do'16 re' |
    mi'8. mi'16 mi'8.\trill mi'16 mi'8 fad' |
    sol'8
    \tag #'basse \cybeleMark
    \tag #'recit \cybeleMarkText\markup { [touchant Atys] }
    re''8 si'16\trill si' si' re'' sol'8. sol'16 do'' do'' do'' mi'' |
    la'8\trill la' r do''16 do'' fa''8 fa''16 fa'' |
    re''4\trill re''8 sol'' do''4 do''8 si' |
    do''4
    \atysMark r8 sol sol8. sol16 |
    sol8. sol16 do'8. do'16 fad8. fad16 fad8. sol16 |
    sol2 r |
    r4 r8 re'16 re' si8\trill si mi' mi'16 si |
    do'8 do' r4 fa'8. fa'16 fa'8 fa'16 fa' |
    re'8\trill re' r sol' sol'8. sol'16 sol'8[ fa'16]\trill mi' |
    fa'4 r16 re' re' re' la8.\trill la16 la8. si16 |
    do'4 do'8. mi'16 do'8.\trill do'16 re'8. mi'16 |
    fa'8 re'16 re' si8\trill si16 si sold8 si16 si mi'8 mi'16 mi' |
    dod'8 dod'16 dod' re'4 re'8 dod' |
    re'4
    \tag #'basse \cybeleMark
    \tag #'recit \cybeleMarkText\markup { [montrant à Atys Sangaride morte] }
    r16 la' la' la' fad'4 r8 re'' |
    si'\trill si' r4
    \atysMark sol'4 re'16 re' re' re' |
    sol8\trill sol r8 sol16 sol do'8 do'16 do' re'8. mi'16 |
    la8 la fa'4 fa'8. fa'16 fa'8. la'16 |
    re'8 re' sol'8. sol'16 mi'8\trill mi'16 mi' |
    dod'4
    \cybeleMark r8 mi'' la'4 la'8 la'16 la' |
    fad'4 la'8 la'16 la' la'8. si'16 |
    sol'4
    \atysMark re'4 r8 re' si\trill si16 si |
    do'4 do'8 do' sol4 la8 sib |
    la\trill la r la' fa'8. fa'16 do'8. re'16 |
    mi'8 mi'16 do' do' re' mi' fa' sol'8 sol'16 sol' do'8 do'16 re' |
    si2\trill r |
    <<
      \tag #'basse { s1*7 \atysMark }
      \tag #'recit { R1*7 <>^\markup\character Atys }
    >>
    r4 mib' do'8. do'16 do'8. sol16 |
    lab8 lab r4 r8 fa' re'16\trill re' re' re' |
    si4 sol8 sol16 sol re'8. re'16 |
    sol'4 mib'8. sol'16 mib'8\trill mib'16 mib' |
    do'4\trill fa'8 fa'16 fa' re'8.\trill re'16 re'8. fa'16 |
    sib8 sib sib8. sib16 fa8\trill fa16 sib |
    sol2\trill sol'4. sol'8 |
    do'4 lab' fa'8 re'16 re' |
    si8 si mib'8. mib'16 mib'8 fa'16 sol' |
    lab'8 re'16 fa' si8. si16 si8 si16 do' |
    do'4 do'8
    \cybeleMark mib''8 si'8 si'16 si' si'8 si'16 re'' |
    sol'8 mib'16 fa' sol'8\trill sol'16 la' sib'8 do''16 re'' mib''8 mib''16 fa'' |
    re''8.\trill sib'16 do''8. re''16 mib''4 mib''8 mib''16 re'' |
    mib''4 mib'' r8 mib'' sib'8. sib'16 |
    sol'4\trill r8 sol'' do''16 do'' do'' do'' do''8. sol'16 |
    lab'4 r16 do'' do'' do'' fa''4 re''8 re''16 re'' |
    si'4 re''8 re''16 sol'' mib''8. re''16 re''8. mib''16 |
    do''4\trill do''8
    \atysMark mib'8 sol\trill sol r mib'16 mib' |
    do'8\trill do'16 do' sol8\trill sol16 la sib8. sib16 sib sib do' re' |
    mib'8 mib'16 mib' la8\trill la16 sib sib8 sib r re'16 re' |
    mi'8. mi'16 mi'8 fad'16 sol' fad'8\trill re'16 re' la8\trill la16 sib |
    sol8\trill sol r re' si8. re'16 sol'8. sol'16 |
    sol'4 r16 mib' mib' mib' mib'8. sol'16 |
    do'4\trill r8 fa' re'8.\trill re'16 re'8. fa'16 |
    sib8 sib16 sib sib8 do'16 re' mib'8 mib' r sol' |
    mib'8. mib'16 sib8. do'16 re'8 re'16 mi' mi'8\trill mi'16 fa' |
    fa'8 fa' r fa'16 fa' sib8 sib16 sib |
    sol4\trill mib'8. sol'16 si4 si8. re'16 |
    sol4 r sol' do'16 do' do' do' |
    la8\trill la r fa' re'8.\trill re'16 re'8. re'16 |
    si8 si r mib'16 sol' re'4\trill re'8. mib'16 |
    do'2 do'4
    <<
      \tag #'basse { s4 s1*5 s4 }
      \tag #'recit { r4 | R1*5 | r4 }
    >>
    \cybeleMark r8 do''8 la'8.\trill la'16 la'8. la'16 |
    fad'4 r4
    \atysMark re'4 re'16 re' re' re' |
    si8 si16 si do'8 do'16 do' do'8 re' |
    mib'4 sol'8 sol'16 sol' do'8. do'16 do'8. do'16 |
    la8\trill la16 fa' fa' fa' mib' re' mib'8 mib' |
    lab'8. fa'16 re'8.\trill re'16 si8\trill si16 do' |
    do'2 r4 |
  }
  %% Chœur (dessus)
  \tag #'(vdessus basse) {
    <<
      \tag #'basse {
        s1 s2.*2 s1 s2. s1 s2. s1 s1 s1*6 s2. s1*4 s2. s1 s2. s1 s1
        s1*2 s1 \choeurMark
      }
      \tag #'vdessus {
        \clef "vbas-dessus" R1 R2.*2 R1 R2. R1 R2. R1 R1 R1*6 R2. R1*4
        R2. R1 R2. R1 R1 R1*2 R1 <>^\markup\character Chœur
      }
    >>
    r2 r4 mib''4 |
    re''2\trill r4 mib'' |
    do''2. do''4 |
    si'2\trill si' |
    r do''4. do''8 |
    do''2 si'4.\trill si'8 |
    do''2 do''
    <<
      \tag #'basse {
        s1*2 s2.*2 s1 s2. s1 s2.*3 s1*12 s2. s1*3 s2. s1*4
        s2. \choeurMark
      }
      \tag #'vdessus {
        R1*2 | R2.*2 | R1 | R2. | R1 | R2.*3 | R1*12 | R2. | R1*3 |
        R2. | R1*4 | r2 r4 <>^\markup\character Chœur
      }
    >> mib''4 |
    re''2\trill r4 mib'' |
    do''2. do''4 |
    si'2\trill si' |
    r do''4. do''8 |
    do''2 si'4.\trill si'8 |
    do''8 do''
    \tag #'vdessus { r4 r2 R1 R2. R1 R2.*3 }
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre2" R1 R2.*2 R1 R2. R1 R2. R1 R1 R1*6 R2. R1*4
    R2. R1 R2. R1 R1 R1*2 R1 |
    r2 r4 sol' |
    sol'2 r4 sol' |
    sol'2. fa'4 |
    sol'2 sol' |
    r sol'4. sol'8 |
    re'2 re'4. re'8 |
    mib'2 mib' |
    R1*2 R2.*2 R1 R2. R1 R2.*3 R1*12 R2. R1*3 R2. R1*4 |
    r2 r4 sol' |
    sol'2 r4 sol' |
    sol'2. fa'4 |
    sol'2 sol' |
    r sol'4. sol'8 |
    re'2 re'4. re'8 |
    mib' mib' r4 r2 |
    R1 R2. R1 R2.*3
  }
  \tag #'vtaille {
    \clef "vtaille" R1 R2.*2 R1 R2. R1 R2. R1 R1 R1*6 R2. R1*4 R2. R1 R2.
    R1 R1 R1*2 R1 |
    r2 r4 do'4 |
    re'2 r4 do' |
    do'2. fa'4 |
    re'2\trill re' |
    r do'4. sol8 |
    lab2 sol4. sol8 |
    sol2 sol |
    R1*2 R2.*2 R1 R2. R1 R2.*3 R1*12 R2. R1*3 R2. R1*4 |
    r2 r4 do' |
    re'2 r4 do' |
    do'2. fa'4 |
    re'2\trill re' |
    r do'4. sol8 |
    lab2 sol4. sol8 |
    sol sol r4 r2 |
    R1 R2. R1 R2.*3
  }
  \tag #'vbasse {
    \clef "vbasse" R1 R2.*2 R1 R2. R1 R2. R1 R1 R1*6 R2. R1*4 R2. R1
    R2. R1 R1 R1*2 R1 |
    r2 r4 do' |
    si2\trill r4 do' |
    lab2. lab4 |
    sol2 sol |
    r mib4. mib8 |
    fa2 sol4. sol8 |
    do2 do |
    R1*2 R2.*2 R1 R2. R1 R2.*3 R1*12 R2. R1*3 R2. R1*4 |
    r2 r4 do' |
    si2 r4 do' |
    lab2. lab4 |
    sol2 sol |
    r mib4. mib8 |
    fa2 sol4. sol8 |
    do8 do r4 r2 |
    R1 R2. R1 R2.*3
  }
>>
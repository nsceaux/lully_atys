\score {
  \new ChoirStaff \with { \haraKiriFirst }<<
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
        >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'recit \includeNotes "voix"
    >> \keepWithTag #'recit \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion {
        s1 s2.*2 s1 s2. s1 s2. s1 s1 s1*6 s2. s1*4 s2. s1 s2. s1 s1
        s1*2 s1\break s1*7\break
      }
    >>
  >>
  \layout { }
  \midi { }
}

%% ACTE 5 SCENE 4
\tag #'(recit basse) {
  Que je viens d’im -- mo -- ler u -- ne gran -- de vic -- ti -- me !
  San -- ga -- ride est sau -- vée, et c’est par ma va -- leur.
  
  A -- chè -- ve ma ven -- geance, A -- tys, con -- nais ton cri -- me,
  et re -- prends ta rai -- son pour sen -- tir ton mal -- heur.
  
  Un calme heu -- reux suc -- cède aux trou -- bles de mon cœur.
  San -- ga -- ri -- de, nym -- phe char -- man -- te,
  qu’ê -- tes- vous de -- ve -- nu -- e ? où puis-je a -- voir re -- cours ?
  Di -- vi -- ni -- té tou -- te puis -- san -- te,
  Cy -- bèle, ay -- ez pi -- tié de nos ten -- dres a -- mours,
  ren -- dez- moi San -- ga -- ride, é -- par -- gnez ses beaux jours.
  
  Tu la peux voir, re -- gar -- de.
  
  Ah quel -- le bar -- ba -- ri -- e !
  San -- ga -- ride a per -- du la vi -- e !
  Ah quel -- le main cru -- el -- le ! ah quel cœur in -- hu -- main !…
  
  Les coups dont el -- le meurt sont de ta pro -- pre main.
  
  Moi, j’au -- rais im -- mo -- lé la beau -- té qui m’en -- chan -- te ?
  Ô Ciel ! ma main san -- glan -- te
  est de ce crime hor -- rible un té -- moin trop cer -- tain !
}
% Le Choeur
\tag #'(choeur basse) {
  A -- tys, A -- tys lui- mê -- me,
  fait pé -- rir ce qu’il ai -- me.
}
\tag #'(recit basse) {
  Quoi, San -- ga -- ride est mor -- te ? A -- tys est son bour -- reau !
  Quel -- le ven -- geance ô dieux ! quel sup -- pli -- ce nou -- veau !
  Quel -- les hor -- reurs sont com -- pa -- ra -- bles
  aux hor -- reurs que je sens ?
  Dieux cru -- els, dieux im -- pi -- toy -- a -- bles,
  n’ê -- tes- vous tout- puis -- sants
  que pour fai -- re des mi -- sé -- ra -- bles ?
  
  A -- tys, je vous ai trop ai -- mé :
  cet a -- mour par vous- même en cou -- roux trans -- for -- mé
  fait voir en -- cor sa vi -- o -- len -- ce :
  ju -- gez, in -- grat, ju -- gez en ce fu -- nes -- te jour,
  de la gran -- deur de mon a -- mour
  par la gran -- deur de ma ven -- gean -- ce.
  
  Bar -- ba -- re ! quel a -- mour qui prend soin d’in -- ven -- ter
  les plus hor -- ri -- bles maux que la ra -- ge peut fai -- re !
  Bien heu -- reux qui peut é -- vi -- ter
  le mal -- heur de vous plai -- re.
  Ô dieux ! in -- jus -- tes dieux ! que n’ê -- tes- vous mor -- tels ?
  Faut- il que pour vous seuls vous gar -- diez la ven -- gean -- ce ?
  C’est trop, c’est trop souf -- frir leur cru -- el -- le puis -- san -- ce,
  chas -- sons- les d’i -- ci bas, ren -- ver -- sons leurs au -- tels.
  Quoi, San -- ga -- ride est mor -- te ? A -- tys, A -- tys lui- mê -- me
  fait pé -- rir ce qu’il ai -- me ?
}
% Le Choeur
\tag #'(choeur basse) {
  A -- tys, A -- tys lui- mê -- me,
  fait pé -- rir ce qu’il ai -- me.
}
\tag #'(recit basse) {
  Ô -- tez ce triste ob -- jet.
  
  Ah ne m’ar -- ra -- chez pas
  ce qui res -- te de tant d’ap -- pas !
  En fus -- siez- vous ja -- louse en -- co -- re,
  il faut que je l’a -- do -- re
  jus -- que dans l’hor -- reur du tré -- pas.
}

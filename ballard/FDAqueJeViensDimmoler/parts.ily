\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Atys
    \livretVerse#12 { Que je viens d’immoler une grande victime ! }
    \livretVerse#12 { Sangaride est sauvée, et c’est par ma valeur. }
    \livretPersDidas Cybèle touchant Atys
    \livretVerse#12 { Achève ma vengeance, Atys, connais ton crime, }
    \livretVerse#12 { Et reprends ta raison pour sentir ton malheur. }
    \livretPers Atys
    \livretVerse#12 { Un calme heureux succède aux troubles de mon cœur. }
    \livretVerse#8 { Sangaride, nymphe charmante, }
    \livretVerse#12 { Qu’êtes-vous devenue ? où puis-je avoir recours ? }
    \livretVerse#8 { Divinité toute puissante, }
    \livretVerse#12 { Cybèle, ayez pitié de nos tendres amours, }
    \livretVerse#12 { Rendez-moi Sangaride, épargnez ses beaux jours. }
    \livretPersDidas Cybèle montrant à Atys Sangride morte
    \livretVerse#13 { Tu la peux voir, regarde. }
    \livretPers Atys
    \livretVerse#13 { \transparent { Tu la peux voir, regarde. } Ah quelle barbarie ! }
    \livretVerse#8 { Sangaride a perdu la vie ! }
    \livretVerse#12 { Ah quelle main cruelle ! ah quel cœur inhumain !… }
    \livretPers Cybèle
    \livretVerse#12 { Les coups dont elle meurt sont de ta propre main. }
    \livretPers Atys
    \livretVerse#12 { Moi, j’aurais immolé la beauté qui m’enchante ? }
    \livretVerse#6 { Ô Ciel ! ma main sanglante }
    \livretVerse#12 { Est de ce crime horrible un témoin trop certain ! }
    \livretPers Le Chœur
    \livretVerse#6 { Atys, Atys lui-même, }
    \livretVerse#6 { Fait périr ce qu’il aime. }
    \livretPers Atys
    \livretVerse#12 { Quoi, Sangaride est morte ? Atys est son bourreau ! }
    \livretVerse#12 { Quelle vengeance ô dieux ! quel supplice nouveau ! }
    \livretVerse#8 { Quelles horreurs sont comparables }
    \livretVerse#6 { Aux horreurs que je sens ? }
  }
  \column {
    \null
    \livretVerse#8 { Dieux cruels, dieux impitoyables, }
    \livretVerse#6 { N’êtes-vous tout-puissants }
    \livretVerse#8 { Que pour faire des misérables ? }
    \livretPers Cybèle
    \livretVerse#8 { Atys, je vous ai trop aimé : }
    \livretVerse#12 { Cet amour par vous-même en couroux transformé }
    \livretVerse#8 { Fait voir encor sa violence : }
    \livretVerse#12 { Jugez, ingrat, jugez en ce funeste jour, }
    \livretVerse#8 { De la grandeur de mon amour }
    \livretVerse#8 { Par la grandeur de ma vengeance. }
    \livretPers Atys
    \livretVerse#12 { Barbare ! quel amour qui prend soin d’inventer }
    \livretVerse#12 { Les plus horribles maux que la rage peut faire ! }
    \livretVerse#8 { Bien heureux qui peut éviter }
    \livretVerse#6 { Le malheur de vous plaire. }
    \livretVerse#12 { Ô dieux ! injustes dieux ! que n’êtes-vous mortels ? }
    \livretVerse#12 { Faut-il que pour vous seuls vous gardiez la vengeance ? }
    \livretVerse#12 { C’est trop, c’est trop souffrir leur cruelle puissance, }
    \livretVerse#12 { Chassons-les d’ici bas, renversons leurs autels. }
    \livretVerse#12 { Quoi, Sangaride est morte ? Atys, Atys lui-même }
    \livretVerse#6 { Fait périr ce qu’il aime ? }
    \livretPers Le Chœur
    \livretVerse#6 { Atys, Atys lui-même }
    \livretVerse#6 { Fait périr ce qu’il aime. }
    \livretPersDidas Cybèle ordonnant d’emporter le corps de Sangaride morte
    \livretVerse#12 { Ôtez ce triste objet. }
    \livretPers Atys
    \livretVerse#12 { \transparent { Ôtez ce triste objet. } Ah ne m’arrachez pas }
    \livretVerse#8 { Ce qui reste de tant d’appas ! }
    \livretVerse#8 { En fussiez-vous jalouse encore, }
    \livretVerse#6 { Il faut que je l’adore }
    \livretVerse#8 { Jusque dans l’horreur du trépas. }
  }
}#}))

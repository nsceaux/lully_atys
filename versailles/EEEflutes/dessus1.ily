\clef "dessus" mi''4 mi''2 |
do'' do''4 fa'' fa''2 |
re'' re''4 sol'' mi''2 |
fa'' mi''4 fa'' re''2 |
do''2. re''4 re''2 |
mi'' mi''4 do'' re''2 |
si' si'4 do'' do''2 |
do'' si'4 do'' la'2 |
sol'2. re''4 mi''2 |
re'' mi''4 fa'' mi''2 |
re'' re''4 fa'' fa''2 |
fa'' mi''4 fa'' re''2 |
do''2.

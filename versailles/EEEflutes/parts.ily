\piecePartSpecs
#`((flutes-hautbois)
   (basse-continue #:clef "alto"
                   #:system-count 3
                   #:instrument , #{ \markup\center-column { BFl Bc } #})
   (basse-tous #:clef "alto"
               #:score-template "score"
               #:instrument , #{ \markup\center-column { BFl Bc } #}))

\clef "dessus" do''4 do''2 |
la' la'4 re'' re''2 |
si' si'4 si' dod''2 |
re'' do''!4 re'' si'2 |
do''2. si'4 si'2 |
do'' do''4 la' si'2 |
sold' sold'4 la' mi'2 |
fad' sol'4 la' fad'2 |
sol'2. si'4 do''2 |
si' do''4 re'' do''2 |
si' si'4 la' la'2 |
si' do''4 re'' si'2 |
do''2.

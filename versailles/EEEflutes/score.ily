\score {
  <<
    \new GrandStaff \with {
      instrumentName = "Flûtes"
      shortInstrumentName = "Fl"
    } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with {
        instrumentName = \markup\center-column { "[Basse de flûte" "et] Bc" }
        shortInstrumentName = \markup\center-column { [BFl] Bc }
      } <<
        \global \keepWithTag #'basse \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout {
    indent = 24\mm
    system-count = 2
  }
  \midi { }
}
\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {\livretPers Celænus
    \livretVerse#12 { N’avancez pas plus loin, ne suivez point mes pas ; }
    \livretVerse#8 { Sortez. Toy ne me quitte pas. }
    \livretVerse#12 { Atys, il faut attendre icy que la Déesse }
    \livretVerse#8 { Nomme un grand Sacrificateur. }
    \livretPers Atys
    \livretVerse#12 { Son choix sera pour vous, Seigneur ; quelle tristesse }
    \livretVerse#8 { Semble avoir surpris vostre cœur ? }
    \livretPers Celænus
    \livretVerse#12 { Les Roys les plus puissants connoissent l’importance }
    \livretVerse#6 { D’un si glorieux choix : }
    \livretVerse#12 { Qui pourra l’obtenir estendra sa puissance }
    \livretVerse#12 { Par tout où de Cybele on revere les loix. }
    \livretPers Atys
    \livretVerse#13 { Elle honore aujourd’huy ces lieux de sa presence, }
    \livretVerse#12 { C’est pour vous preferer aux plus puissans des Roys. }
    \livretPers Celænus
    \livretVerse#12 { Mais quand j’ay veu tantost la Beauté qui m’enchante, }
    \livretVerse#12 { N’as-tu point remarqué comme elle estoit tremblante ? }
    \livretPers Atys
    \livretVerse#12 { A nos jeux, à nos chants, j’estois trop appliqué, }
    \livretVerse#12 { Hors la feste, Seigneur, je n’ay rien remarqué. }
    \livretPers Celænus
    \livretVerse#12 { Son trouble m’a surpris. Elle t’ouvre son ame ; }
    \livretVerse#12 { N’y découvres-tu point quelque secrette flâme ? }
    \livretVerse#12 { Quelque Rival caché ? }
    \livretPers Atys
    \livretVerse#12 { \transparent { Quelque Rival caché ? } Seigneur, que dites-vous ? }
    \livretPers Celænus
    \livretVerse#12 { Le seul nom de rival allume mon couroux. }
    \livretVerse#12 { J’ay bien peur que le Ciel n’ait pû voir sans envie }
    \livretVerse#6 { Le bonheur de ma vie, }
    \livretVerse#12 { Et si j’estois aimé mon sort seroit trop doux. }
    \livretVerse#12 { Ne t’estonnes point tant de voir la jalousie }
    \livretVerse#6 { Dont mon ame est saisie }
    \livretVerse#12 { On ne peut bien aimer sans estre un peu jaloux. }
  }
  \column {
    \livretPers Atys
    \livretVerse#12 { Seigneur, soyez content, que rien ne vous allarme ; }
    \livretVerse#13 { L’Hymen va vous donner la Beauté qui nous charme, }
    \livretVerse#8 { Vous serez son heureux espoux. }
    \livretPers Celænus
    \livretVerse#12 { Tu peux me rassurer, Atys, je te veux croire, }
    \livretVerse#8 { C’est son cœur que je veux avoir, }
    \livretVerse#8 { Dy-moy s’il est en mon pouvoir ? }
    \livretPers Atys
    \livretVerse#12 { Son cœur suit avec soin le Devoir et la Gloire, }
    \livretVerse#12 { Et vous avez pour vous le Gloire et le Devoir. }
    \livretPers Celænus
    \livretVerse#12 { Ne me déguise point ce que tu peux connaistre. }
    \livretVerse#8 { Si j’ay ce que j’aime en ce jour }
    \livretVerse#9 { L’Hymen seul m’en rend-t’il le maistre ? }
    \livretVerse#12 { La Gloire et le Devoir auront tout fait, peut-estre, }
    \livretVerse#12 { Et ne laissent pour moy rien à faire à l’Amour. }
    \livretPers Atys
    \livretVerse#12 { Vous aimez d’un amour trop delicat, trop tendre. }
    \livretPers Celænus
    \livretVerse#12 { L’indifferent Atys ne le sçauroit comprendre. }
    \livretPers Atys
    \livretVerse#8 { Qu’un Indifferent est heureux ! }
    \livretVerse#8 { Il joüit d’un destin paisible. }
    \livretVerse#12 { Le Ciel fait un present bien cher, bien dangeureux, }
    \livretVerse#8 { Lorsqu’il donne un cœur trop sensible. }
    \livretPers Celænus
    \livretVerse#8 { Quand on aime bien tendrement }
    \livretVerse#12 { On ne cesse jamais de souffrir, et de craindre ; }
    \livretVerse#8 { Dans le bonheur le plus charmant, }
    \livretVerse#12 { On est ingénieux à se faire un tourment, }
    \livretVerse#8 { Et l’on prend plaisir à se plaindre. }
    \livretVerse#12 { Va songe à mon hymen, et voy si tout est prest, }
    \livretVerse#12 { Laisse-moy seul icy, la Déesse paraist. }
  }
}#}))

<<
  %% Atys
  \tag #'(atys basse) {
    << { s1*7 s2. } \tag #'atys { \atysClef R1*7 | r2 r4 } >>
    \tag #'basse \atysMark r8 do' |
    dod'8. dod'16 dod'8. re'16 re'4 r8 re' |
    sib4 sib8 sib16 la la4 la |
    la8. la16 si8 do' re'4 re'8. mi'16 |
    dod'4
    << { s2. s1*2 s2. s1*3 s4 } \tag #'atys { r4 r2 | R1*2 | R2. | R1*3 | r4 } >>
    \tag #'basse \atysMark dod'8 dod' re'4 re'8 re' |
    si4 r8 sol do'4 sib8 sib16 la |
    la8 la r la la la sib8. do'16 |
    re'8. re'16 mi'8 fa'
    \footnoteHere#'(0 . 0) \markup { F-V : \raise#1 \score {
        \withLyrics {
          \tinyQuote \clef "vhaute-contre" \partial 2
          fa'4( fa'8.) mi'16 | fa'4
        } \lyrics { -sants des roys. }
        \layout { \quoteLayout }
      }
    }
    fa'4( mi'8.) fa'16 |
    fa'4
    << { s2. s1*2 s4 } \tag #'atys { r4 r2 | R1*2 | r4 } >>
    \tag #'basse \atysMark r8 mi'16 si do'4. do'16 do' |
    la4. la16 si do'4. do'16 re' |
    si4. la16 sol do'8 do' r do' |
    la4 r8 la16 si do'4 do'8 si |
    do'4
    << { s2. s1 s2.*3 s8 } \tag #'atys { r4 r2 | R1 | R2.*3 | r8 } >>
    \tag #'basse \atysMark re'8 la4 r8 la re' la |
    << \tag #'atys sib4 \tag #'basse { sib8 s } >>
    << { s2.*2 s1*3 s2.*5 s4 } \tag #'atys { r4 r2 | R2. | R1*3 | R2.*5 | r4 } >>
    \tag #'basse \atysMark r8 fa' la8. la16 la8 si |
    do'4 r8 re' sol8. sol16 sol8. do'16 |
    la8 la16 fa fa sol la si do'8 do'16 do' re'8 mi'16 fa' |
    mi'4 mi' r do'8. do'16 |
    do'4 sib8[ la16] sib sib4( la8.) sol16 |
    sol4
    << { s2. s1*3 s4 } \tag #'atys { r4 r2 | R1*3 | r4 } >>
    \tag #'basse \atysMark r8 dod' dod'8. dod'16 dod'8. dod'16 |
    re'4 re'8. re'16 mi'4 mi'8. mi'16 |
    fa'4 fa' re'8 re'16 do' si8. la16 |
    sold4. si8 mi' re' re'8[ dod'?16] re' |
    << \tag #'atys dod'4 \tag #'basse { dod'8 s } >>
    << { s2. s1*3 s2.*2 s1 s2 } \tag #'atys { r4 r2 | R1*3 | R2.*2 | R1 | r2 } >>
    \tag #'basse \atysMark r8 fa'16 fa' re'8 re'16 re' |
    la4 la8 la16 la sib4 r8 re'8 |
    la8 << \tag #'basse { la16 s s2. s1 \atysMark } \tag #'atys { la8 r4 r2 | R1 } >>
    fa'4. fa'8 mi'4. fa'8 |
    re'2 re'4. mi'8 |
    dod'2 re'4. la8 |
    sib2 sib4. do'8 |
    la4. la8 la4( sol) |
    la1 |
    fa'4. fa'8 mi'4. fa'8 |
    re'2 re'4. mi'8 |
    dod'2 re'4. la8 |
    sib2 sib4. do'8 |
    la4. la8 la4( sol) |
    la2. mi'4 |
    fa'2 re'4 re'8 do' |
    si2. sol'4 |
    do'4. re'8 re'4. do'8 |
    do'2 mi'4. mi'8 |
    mi'4 fa' re'2~ |
    re' do'4( si8) do' |
    do'2( si) |
    la2. la4 |
    re'2 re'4 re'8 la |
    sib2. sib4 |
    sol4. sol8 do'4. sib8 |
    la1 |
    fa'4. mi'8 re'4. do'8 |
    si2 dod'4 re' |
    re'2( dod') |
    re'2
    \tag #'atys { r2 | R1*32 }
  }
  %% Celaenus
  \tag #'(celaenus basse) {
    \tag #'basse \celaenusMark
    \tag #'celaenus \celaenusClef
    r2 r16 fa fa fa sol8. la16 |
    sol2 mi8 mi16 mi la8. mi16 |
    fa2 r4 r8 la |
    sib4 r mi8 mi16 mi mi8. fa16 |
    re4 r r r8 re' |
    la4. la8 la8. la16 la8. sib16 |
    do'8 do' sib8. la16 sol8 sol r sol16 sol |
    do'8 do'16 sib sib8. la16 la4
    << { s4 s1*3 s4 } \tag #'celaenus { r4 | R1*3 | r4 } >>
    \tag #'basse \celaenusMark r8 mi la8. la16 la8. la16 |
    re'4 r8 re' la8. la16 la8 la |
    re4 re la8. la16 la8 si16 do' |
    si4 r8 sol16 sol sol8 la16 si |
    do'4 do'8 do' re'4 re'8[ do'16] re' |
    mi'4 mi'8 mi fad8. fad16 fad8. fad16 |
    sold4 sold8. sold16 la4 la8. sold16 |
    la4 << { s2. s1*3 s4 } \tag #'celaenus { r4 r2 | R1*3 | r4 } >>
    \tag #'basse \celaenusMark la r8 la16 la la8. sib16 |
    sol8 sol16 sol sol8 fa16 mi fa4 fa |
    r8 la16 la re'8 re'16 re' si si si si do'8. do'16 |
    sold8 sold
    << { s2. s1*3 s4 } \tag #'celaenus { r4 r2 | R1*3 | r4 } >>
    \tag #'basse \celaenusMark r8 sol mi mi mi sol |
    do4 r8 do'16 do' fa4 fa8. fa16 |
    re8 re r re'16 re' sib8 sib16 sib |
    sol8 sol sol8. fa16 fa8. mi16 |
    mi8 mi la la16 la la8 mi |
    << \tag #'celaenus fa4 \tag #'basse { fa8 s } >>
    << { s2. s8 } \tag #'celaenus { r4 r2 | r8 } >>
    \tag #'basse \celaenusMark re16 mi fa8 fa16 sol la8. la16 re' re' re' mi' |
    dod'4 r8 la16 la la8 si16 dod' |
    re'8 re'16 re' sib8 sib16 sib sol sol do' do' mi8 mi16 fa |
    fa4 fa do'8 do'16 do' la8. la16 |
    fad4. sib8 sol8. sol16 sol8 fad |
    sol4 r8 sib16 re' sib8 sib16 sib |
    sol8. sol16 re8. re16 re8. mi16 |
    fa8 fa fa8. la16 fa8 fa16 mi |
    mi8 mi r16 la la la re'8. re'16 |
    si8 mi' dod' re' re'8. dod'16 |
    re'4
    << { s2. s1*4 s4 } \tag #'celaenus { r4 r2 | R1*4 | r4 } >>
    \tag #'basse \celaenusMark r8 re' sib8. sib16 sib8. sib16 |
    sol4. do'8 do'4 do'8 sib16 la |
    sib8 sib r sib16 la sol8 sol16 fa mi8. re16 |
    dod8 r16 la16 mi8. mi16 fa8. fa16 fa8 sol |
    la4
    << { s2. s1*3 s8 } \tag #'celaenus { r4 r2 | R1*3 | r8 } >>
    \tag #'basse \celaenusMark r16 la la la la la mi8. mi16 mi fad sol la |
    fad8 fad r la sib sib16 la sol8 sol16 fa? |
    mi4 r8 do'16 do' sol8 sol16 sol la8. sib16 |
    la8 la r la re'8. re'16 re'8. re'16 |
    si8. si16 si8. do'16 re'8. mi'16 |
    dod'8 dod' r la16 la re'8 re'16 la |
    sol4 sol8 fa mi4 mi8. fa16 |
    re4 r4 << { s2 s1 s8. } \tag #'celaenus { r2 | R1 | r8 r16 } >>
    \tag #'basse \celaenusMark fa16 fa sol la si do'8 la16 la re'8 re'16 sol |
    la2 la |
    << { s1*27 s2 } \tag #'celaenus { R1*27 | r2 } >>
    \tag #'basse \celaenusMark fa4 re |
    la2 la4 la |
    \once\slurDashed la2( sol4)( fa8) sol |
    la2 dod'4. dod'8 |
    re'2 re'4 la |
    sib2 sib4. sib8 |
    sib2 la4. sib8 |
    la2( sol) |
    fa2 r |
    r fa4 re |
    fa2 la4 la8 si |
    do'4. do'8 re'4( do'8) re' |
    mi'2. mi'4 |
    re'4. re'8 re'4. do'8 |
    si2 si4. do'8 |
    la2 la4( sold8) la |
    sold1 |
    do'4 do' re' mi' |
    sold2 la4( sold8) la |
    la2( sold) |
    la2. la4 |
    mi4. mi8 mi4( re8) mi |
    fa2 la4. la8 |
    re'2 re'4. mi'8 |
    dod'1 |
    re'4. do'8 sib4 la |
    sib2 sol4. fa8 |
    fa2( mi) |
    re2 la4 fa16 fa fa sol |
    mi4. mi8 fa8. fa16 fa8. sol16 |
    la4 la8. la16 la4 si8. do'16 |
    si4 r8 sol16 fa mi4 mi8 fa |
    re1 |
  }
>>

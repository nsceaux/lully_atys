\score {
  <<
    \new ChoirStaff \with { \forceGroupBracket \haraKiri } <<
      \new Staff \with { \atysInstr } \withLyrics <<
        \global \keepWithTag #'atys \includeNotes "voix"
      >> \keepWithTag #'atys \includeLyrics "paroles"
      \new Staff \with { \celaenusInstr } \withLyrics <<
        \global \keepWithTag #'celaenus \includeNotes "voix"
      >> \keepWithTag #'celaenus \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
        \modVersion {
          s1*14 s2. s1*16 s2.*3 s1*2 s2. s1*3 s2.*5 s1*17 s2.*2 s1*5\break
          %% Atys : Qu'un indifférent est heureux
          s1*28\break
          %% Celænus : Quand on aime bien tendrement
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}

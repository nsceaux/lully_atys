%% ACTE 2 SCENE 1
%Celaenus
\tag #'(celaenus basse) {
  N’a -- van -- cez pas plus loin, ne sui -- vez point mes pas ;
  Sor -- tez. Toy ne me quit -- te pas.
  A -- tys, il faut at -- tendre i -- cy que la dé -- es -- se
  Nomme un grand sa -- cri -- fi -- ca -- teur.
}
%Atys
\tag #'(atys basse) {
  Son choix se -- ra pour vous, sei -- gneur ; quel -- le tris -- tes -- se
  Semble a -- voir sur -- pris vos -- tre cœur ?
}
%Celaenus
\tag #'(celaenus basse) {
  Les roys les plus puis -- sants con -- nois -- sent l’im -- por -- tan -- ce
  D’un si glo -- ri -- eux choix :
  Qui pour -- ra l’ob -- te -- nir es -- ten -- dra sa puis -- san -- ce
  Par tout où de Cy -- bele on re -- ve -- re les loix.
}
%Atys
\tag #'(atys basse) {
  Elle ho -- nore au -- jour -- d’huy ces lieux de sa pre -- sen -- ce,
  C’est pour vous pre -- fe -- rer aux plus puis -- sants des roys.
}
%Celaenus
\tag #'(celaenus basse) {
  Mais quand j’ay veu tan -- tost la beau -- té qui m’en -- chan -- te,
  N’as- tu point re -- mar -- qué comme elle es -- toit trem -- blan -- te ?
}
%Atys
\tag #'(atys basse) {
  A nos jeux, à nos chants, j’es -- tois trop ap -- pli -- qué,
  Hors la fes -- te, sei -- gneur, je n’ay rien re -- mar -- qué.
}
%Celaenus
\tag #'(celaenus basse) {
  Son trou -- ble m’a sur -- pris. El -- le t’ou -- vre son a -- me ;
  N’y dé -- cou -- vres- tu point quel -- que se -- cret -- te flâ -- me ?
  Quel -- que ri -- val ca -- ché ?
}
%Atys
\tag #'(atys basse) {
  Sei -- gneur, que di -- tes- vous ?
}
%Celaenus
\tag #'(celaenus basse) {
  Le seul nom de ri -- val al -- lu -- me mon cou -- roux.
  J’ay bien peur que le ciel n’ait pû voir sans en -- vi -- e
  Le bon -- heur de ma vi -- e,
  Et si j’es -- tois ai -- mé mon sort se -- roit trop doux.
  Ne t’es -- ton -- nes point tant de voir la ja -- lou -- si -- e
  Dont mon ame est sai -- si -- e,
  On ne peut bien ai -- mer sans estre un peu ja -- loux.
}
%Atys
\tag #'(atys basse) {
  Sei -- gneur, soy -- ez con -- tent, que rien ne vous al -- lar -- me ;
  L’Hy -- men va vous don -- ner la beau -- té qui vous char -- me,
  Vous se -- rez son heu -- reux es -- poux.
}
%Celaenus
\tag #'(celaenus basse) {
  Tu peux me ras -- su -- rer, A -- tys, je te veux croi -- re,
  C’est son cœur que je veux a -- voir,
  Dy- moy s’il est en mon pou -- voir ?
}
%Atys
\tag #'(atys basse) {
  Son cœur su -- it avec soin le de -- voir et la gloi -- re,
  Et vous a -- vez pour vous la gloire et le de -- voir.
}
%Celaenus
\tag #'(celaenus basse) {
  Ne me dé -- gui -- se point ce que tu peux con -- nais -- tre.
  Si j’ay ce que j’aime en ce jour,
  L’hy -- men seul m’en rend- t’il le mais -- tre ?
  La gloire et le de -- voir au -- ront tout fait, peut- es -- tre,
  Et ne lais -- sent pour moy rien à faire à l’a -- mour.
}
%Atys
\tag #'(atys basse) {
  Vous ai -- mez d’un a -- mour trop de -- li -- cat, trop ten -- dre.
}
%Celaenus
\tag #'(celaenus basse) {
  L’in -- dif -- fe -- rent A -- tys ne le sçau -- roit com -- pren -- dre.
}
%Atys
\tag #'(atys basse) {
  Qu’un in -- dif -- fe -- rent est heu -- reux !
  Il joü -- it d’un des -- tin pai -- si -- ble.
  Qu’un in -- dif -- fe -- rent est heu -- reux !
  Il joü -- it d’un des -- tin pai -- si -- ble.
  Le ciel fait un pre -- sent bien cher, bien dan -- ge -- reux,
  Lors -- qu’il donne un cœur trop sen -- si -- ble.
  Le ciel fait un pre -- sent bien cher, bien dan -- ge -- reux,
  Lors -- qu’il donne un cœur trop sen -- si -- ble.
}
%Celaenus
\tag #'(celaenus basse) {
  Quand on ai -- me bien ten -- dre -- ment
  On ne ces -- se ja -- mais de souf -- frir, et de crain -- dre ;
  Quand on -dre ;
  Dans le bon -- heur le plus char -- mant,
  On est in -- gé -- ni -- eux à se faire un tour -- ment,
  Et l’on prend plai -- sir à se plain -- dre.
  On est in -- gé -- ni -- eux à se faire un tour -- ment,
  Et l’on prend plai -- sir à se plain -- dre.
  Va songe à mon hy -- men, et voy si tout est prest,
  Lais -- se- moy seul i -- cy, la de -- es -- se pa -- raist.
}

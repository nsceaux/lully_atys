\clef "dessus" r4 sib' do'' |
re'' do'' sib' |
la'2. |
sib'4 do'' re'' |
mib'' do'' fa'' |
re''2 re''8 mib'' |
fa''4 fa'' re'' |
mib'' mib'' re'' |
re'' do''4. re''8 |
re''2. |
sib'4 si'4. si'8 |
do''4 re'' mib'' |
re''4. mib''8 fa'' re'' |
mib''4 do''4. mib''8 |
re''4 re'' mib'' |
fa''4. fa''8 fa''4 |
sol'' do''2 |
sib'4 sib''4. la''8 |
sol''4 sol''4. re''8 |
mib''4 mi''4. mi''8 |
fa''4 fad''4. fad''8 |
sol''4. sol''8 la''4 |
sib''4 fad''4. sol''8 |
sol''4 sib''4. la''8 |
sol''4 sol''4. re''8 |
mib''4 mi''4. mi''8 |
fa''4 fad''4. fad''8 |
sol''4. sol''8 la''4 |
sib''4 fad''4. sol''8 |
sol''2. |

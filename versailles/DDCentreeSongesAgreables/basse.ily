\clef "basse" sol,4_\markup\center-align "[Bc]" sol^"[Tous]" la |
sib la sol |
re'2 re4 |
sol la sib |
mib fa fa, |
sib,2 sib4 |
la2 si4 |
do' do sol8 fa |
mib2. |
re |
sol,4 sol fa |
mib re do |
si,2. |
do |
sol4 fa mib |
re2. |
mib4 fa fa, |
sib,2. |
si, |
do4 do' sib |
la re' do' |
sib4. sib8 la4 |
sol re2 |
sol4 sol, la, |
sib, si,2 |
do4 do' sib |
la re' do' |
sib4. sib8 la4 |
sol re2 |
sol,2. |

\score {
  <<
    \new GrandStaff \with {
      instrumentName = "Violons"
      shortInstrumentName = "Violons"
      \haraKiri
    } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new ChoirStaff \with { \forceGroupBracket \haraKiriFirst } <<
      \new Staff \with { \atysInstr } \withLyrics <<
        \global \includeNotes "voix"
      >> \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout { }
  \midi { }
}

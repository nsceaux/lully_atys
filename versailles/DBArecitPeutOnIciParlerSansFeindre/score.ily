\score {
  <<
    \new ChoirStaff \with { \forceGroupBracket \haraKiri } <<
      \new Staff \with { \dorisInstr } \withLyrics <<
        \global \keepWithTag #'doris \includeNotes "voix"
      >> \keepWithTag #'doris \includeLyrics "paroles"
      \new Staff \with { \atysInstr } \withLyrics <<
        \global \keepWithTag #'atys \includeNotes "voix"
      >> \keepWithTag #'atys \includeLyrics "paroles"
      \new Staff \with { \idasInstr } \withLyrics <<
        \global \keepWithTag #'idas \includeNotes "voix"
      >> \keepWithTag #'idas \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
        \modVersion {
          s1*2 s2. s1 s2.*2 s1*6 s2.*2 s1*6 s2.*2 s1*2 s2. s1*4 s2.*2\break
          %% Dans l'empire amoureux
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}

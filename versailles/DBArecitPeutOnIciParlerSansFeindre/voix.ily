<<
  %% Doris
  \tag #'(doris basse) {
    <<
      \tag #'basse { s1*2 s2. s4. \dorisMark }
      \tag #'doris { \dorisClef R1*2 | R2. | r4 r8 }
    >> re''8 sib' sib' sib' sib' |
    sol'4
    << { s2 s2. s1 s4 } \tag #'doris { r2 | R2. | R1 | r4 } >>
    \tag #'basse \dorisMark sol'8 sol' sol'4 sol'8 sol' |
    la'4 la'8 sib' do''4 do''8 re'' |
    sib'4 sib' r sib'8 sib' |
    la'4 la'8 la' re''4( dod''8) re'' |
    dod''4 dod''8 dod'' re'' re'' re'' dod'' |
    re''4
    << { s2 s2. s1 s2 } \tag #'doris { r2 | R2. | R1 | r2 } >>
    \tag #'basse \dorisMark sib'8. sib'16 sib'8. sib'16 |
    sol'8 sol' r sol'16 sol' la'8. la'16 la'8. la'16 |
    fad'4
    << { s2. s1 s4 s8 } \tag #'doris { r4 r2 | R1 | r4 r8 } >>
    \tag #'basse \dorisMark la'16 la' la'4 la'8 la' |
    sib'8. sib'16 fa'8. fa'16 sol'8 lab' |
    sol'4 sol'8 sol'16 la' sib'8 sib'16 do'' |
    re''4 sib'8 sib' sib'4 sib'8 la' |
    sib'4
    << { s2.*2 s1*4 s2.*2 } \tag #'doris { r4 r2 | R2. | R1*4 | R2.*2 | } >>
    \tag #'basse \dorisMark r4 sol' sol' |
    re'' re'' la' |
    sib'2 sib'8. sib'16 |
    do''2 do''8. do''16 |
    re''2 fa''4 |
    sib' do'' re'' |
    re''( do''2) |
    sib'2 sib'4 |
    la'2 re''4 |
    si' si' si'8. dod''?16 |
    dod''2 re''8[ dod''] |
    re''4 re''4. dod''8 |
    re''2. |
    sib'4 si'4. si'8 |
    do''4. do''8 re''4 |
    mib'' re''4. do''8 |
    do''2 sol'8 la' |
    sib'2 la'4 |
    sol' sol'4. la'8 |
    fad'2 re'4 |
    re'' re''4. re''8 |
    mib''4 re'' do'' |
    re'' sib'4.( la'16) sib' |
    la'2 sib'8 sib' |
    sib'2 la'4 |
    fad' sol' la' |
    sib'( la'2) |
    sol' sib'8 sib' |
    sib'2 la'4 |
    fad' sol' la' |
    sib'( la'2) |
    sol'4
    << { s2. s1 s4 } \tag #'doris { r4 r2 | R1 | r4 } >>
    \tag #'basse \dorisMark r8 mib''16 mib'' sol'8 sol'16 la' sib'8. do''16 |
    re''8. re''16 sol'8. la'16 fad'4
    << { s4 s2. s2 } \tag #'doris { r4 | R2. | r2 } >>
    \tag #'basse \dorisMark r4 sib' |
    la'2. re''4 |
    sol'2 sol'4 do'' |
    la'2 la'8[ sol'] la'4 |
    sib'4. sib'8 fa'4. sol'8 |
    la'4. la'8 la'4 si' |
    do''2. la'4 |
    sib'4. sib'8 sib'4. sib'8 |
    la'4 sib' sol'4.( la'8) |
    fad'2. la'4 |
    si'4 si' do'' re'' |
    mib'' re'' do''( si'8) do'' |
    si'2 si'4. re''8 |
    mib''4. re''8 do''4. sib'8 |
    la'4 la' sib'8[ la'] sib'4 |
    sib'2( la') |
    sib'2 r |
    r2 r4 r8 la' |
    fad'2. r8 la' |
    si'4 si' do'' re'' |
    mib''4. re''8 do''4. sib'8 |
    sib'2( la') |
    sol'2 r |
    \tag #'doris { R1*4 | R2. | }
  }
  %% Atys
  \tag #'(atys basse) {
    <<
      \tag #'basse { s1. \atysMark }
      \tag #'atys { \atysClef R1 | r2 }
    >> r8 sib16 do' re'8 re'16 mi' |
    fa'4 r16 do' do' re' mib'8. fa'16 |
    re'4 << \tag #'atys re'4 \tag #'basse { re'8 s } >>
    << { s2 s2. s4 } \tag #'atys { r2 | R2. | r4 } >>
    \tag #'basse \atysMark r8 re'16 do' si8 si16 do' |
    do'4 do'8 do' re'4 re'8 re' |
    mib'4
    << { s2. s1*4 s4 } \tag #'atys { r4 r2 | R1*4 | r4 } >>
    \tag #'basse \atysMark r8 sib16 sib sol8 sol16 sol |
    do'8. do'16 sol4 sol8 la16 sib |
    la8 la r do'16 re' mib'8 mib'16 re' do'8. sib16 |
    sib4 r
    << { s2 s1 s4 } \tag #'atys { r2 | R1 | r4 } >>
    \tag #'basse \atysMark r8 re' re'8. re'16 re'8 mib'?8 |
    fa'4 r8 fa'16 fa' mib'4 mib'8 re' |
    do'4 << \tag #'atys do'4 \tag #'basse { do'8 s } >>
    << { s2 s2.*2 s1 s4 } \tag #'atys { r2 | R2.*2 | R1 | r4 } >>
    \tag #'basse \atysMark r8 fa' re' re'16 re' sib8 sib16 la |
    la8 la r fa'16 fa' re'8 re'16 re' |
    si8. re'16 re'8 mib'8 mib'4( re'8.) do'16 |
    do'4 r8 sol' mib'8. mib'16 mib'8. sol'16 |
    do'4. do'8 sol8. sol16 sol8 la |
    sib8 sib r sib sib8. sib16 do'8. re'16 |
    la4 sib8 sib16 sib sib8 do' |
    re'2 re'4 |
    << { s2.*31 s4 } \tag #'atys { R2.*31 | r4 } >>
    \tag #'basse \atysMark r8 re'16 re' sib8 sib r mib' |
    do'4 r8 fa' fa'4 r16 re' re' re' |
    sol4
    << { s2.*2 } \tag #'atys { r4 r2 | r2 r4 } >>
    \tag #'basse \atysMark r8 re'16 re' |
    si8 do'16 re' mib'8. mib'16 mib' mib' mib' re' |
    re'4 re'
    << { s2 s1*22 } \tag #'atys {
        r4 re' |
        fa'2. fa'4 |
        sib2 sib4 mib' |
        do'2 do'4 fa' |
        re'4. re'8 re'4. re'8 |
        do'4. do'8 do'4. re'8 |
        mib'2. mib'4 |
        re'4. re'8 re'4. re'8 |
        re'4 re' re'( do') |
        re'2. re'4 |
        re' re' mib' fa' |
        sol'4. sol'8 sol'4 fad' |
        sol'2 sol'4. sol'8 |
        sol'4. fa'8 mib'4. re'8 |
        do'4. do'8 re'4. mib'8 |
        re'2( do') |
        sib2 r |
        r r4 r8 do' |
        la2. r8 re' |
        re'4 re' mib' fa' |
        sol' sol' fad' sol' |
        sol'2( fad') |
        sol'2 r | } >>
    \tag #'basse \atysMark r8 sib16 sib si8 si16 do' re'8 re'16 mib' fa'8 mib'16 re' |
    mib'4 r8 do' sol sol16 la sib8 sib16 la |
    la2 sib8 sib16 sib do'8 re' |
    mib'4 r8 do' la8. la16 la8. sib16 |
    sol2 sol4 |
  }
  %% Idas
  \tag #'(idas basse) {
    \tag #'basse \idasMark
    \tag #'idas \idasClef
    r4 sib8 sib16 sib sol8. sol16 sol8. sol16 |
    re4 re 
    << { s2 s2. s1 s4 } \tag #'idas { r2 | R2. | R1 | r4 } >>
    \tag #'basse \idasMark r8 do'16 do' la8 la16 la |
    fad4
    \tag #'idas {
      r2 |
      R1 |
      r4 do'8 do' do'4 do'8 do' |
      fad4 fad8 fad fad4 fad8 fad |
      sol4 sol r sol8 sol |
      re'4 re'8 do' sib4( la8) sib |
      la4 la8 la sib sol la la, |
      re4 r2 |
      R2. |
      R1 |
      r2 re'8. re'16 re'8. re'16 |
      sib8 sib r sib16 sib do'8. do'16 do'8. do'16 |
      la4 r r2 |
      R1 |
      r4 r8 fa16 fa mib4 mib8 mib |
      re8. re16 re8. re16 re8. re16 |
      mib4 mib8 mib16 fa sol8 sol16 la |
      sib4 re8 mib fa4 fa8 fa |
      sib,4 r r2 |
      R2. |
      R1*4 |
      R2.*3 |
      r4 re re |
      sol sol4. sol8 |
      la2 la8. la16 |
      sib2 re4 |
      sol la sib |
      fa2. |
      sib,2 sib,4 |
      fa2 re4 |
      sol sol mi8 mi |
      la2 sib4 |
      sol la la, |
      re2. |
      sol4 sol fa |
      mib4 mib re4 |
      do sol4. sol,8 |
      do2 do8 do |
      sol2 fa4 |
      mib4 mib( re8) mib |
      re2 re4 |
      si4 si4. si8 |
      do'4 sib! la |
      fad sol sol, |
      re2 sol8 sol |
      do'2 do'4 |
      re' mib' do' |
      re'( re2) |
      sol sol8 sol |
      do2 do4 |
      re mib do |
      re( re,2) |
      sol,4 r r2 |
      R1 |
      r4 r8 do16 re mib8 mib16 fa sol8. la16 |
      sib8. sib16 sib8. do'16 re'4 r |
      R2. |
      r2 r4 sol4 |
      re2. re4 |
      mib2 mib4 do |
      fa2 fa4 fa4 |
      sib,4. sib8 sib4. sib8 |
      fa4. fa8 mib4 re |
      do2. do4 |
      sol4. sol8 sol4. sol8 |
      fa4 sol mib2 |
      re2. re4 |
      sol fa mib re |
      do sib, lab,( sol,8) lab, |
      sol,2 sol,4 sol |
      do4. do8 do4. do8 |
      fa4. fa8 re4 sib, |
      fa2 fa4 fa |
      sib la sol fa |
      mib re mib do |
      re2 re4. re8 |
      sol4 fa mib re |
      do sib, la, sol, |
      re1 | % re2( re,)
      sol,2 r |
      R1*4 |
      R2. |
    }
  }
>>
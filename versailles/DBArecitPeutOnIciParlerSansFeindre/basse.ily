\clef "basse" sol,1~ |
sol,2 sol |
la2. |
sib4 sib, sol,2 |
do2. |
re2 sol8 fa |
mib2 si, |
do2 do' |
fad1 |
sol1 |
re'4. do'8 sib2 |
la4. la8 sib8 sol la[ la,] |
re4 sol, mib |
mi!2. |
fa2 la,8 sib, fa,4 |
sib,1 | \allowPageTurn
mib?2 do |
re sol |
re do4. sib,8 |
fa4. fa8 mib2 |
re2. |
mib4. mib16 fa sol8 sol16 la |
sib4 re8 mib fa4 fa, |
sib,1 | \allowPageTurn
fa2 fad4 |
sol4. do8 sol,2 |
do1~ |
do |
sol,2 sol |
fad4 sol8 fa? mib4 |
re2 re,4 |
sol, sol2 |
fad4 re re |
sol2 sol4 |
la2 la4 |
sib2 re4 |
sol la sib |
fa fa,2 |
sib,2 sib,4 |
fa2 re4 |
sol2 mi4 |
la2 sib4 |
sol la la, |
re2 re,4 |
sol,4 sol fa |
mib mib re |
do sol4. sol,8 |
do2 do4 |
sol2 fa4 |
mib mib( re8) mib |
re2 re'8 do' |
si2. |
do'4 sib! la |
fad sol sol, |
re2 sol4 |
do4 do2 |
re4 mib do |
re re,2 |
sol, sol,4 |
do2 do4 |
re mib do |
re re,2 |
sol,1 |\allowPageTurn
la,2 si, |
do4. do16 re mib8. fa16 sol8. la16 |
sib4 sib8 do' re'4 re |
sol do2 |
sol,2. sol4 |
re2. re4 |
mib2 mib4 do |
fa2. fa,4 |
sib,4. sib8 sib2 |
fa mib4 re |
do2. do4 |
sol2. sol4 |
fa sol mib2 |
re2. re4 |
sol fa mib re |
do sib, lab,2 |
sol,2. sol4 |
do2. do4 |
fa4. fa8 re4 sib, |
fa2. fa4 |
sib la sol fa |
mib re mib do |
re2. re4 |
sol fa mib re |
do sib, la, sol, |
re2 re, |
sol,2 sol4 fa |
mib re8. do16 si,2 |
do2. sol,4 |
re2 sib, |
do re4 re, |
sol,2 sol8 la |
\once\set Staff.whichBar = "|"
\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Idas
    \livretVerse#8 { Peut-on icy parler sans feindre ? }
    \livretPers Atys
    \livretVerse#12 { Je commande en ces lieux, vous n’y devez rien craindre. }
    \livretPers Doris
    \livretVerse#12 { Mon frere est votre amy. }
    \livretPers Idas
    \livretVerse#12 { \transparent { Mon frere est votre amy. } Fiez-vous à ma sœur. }
    \livretPers Atys
    \livretVerse#12 { Vous devez avec moy partager mon bon-heur. }
    \livretPers\line { Idas, & Doris }
    \livretVerse#12 { Nous venons partager vos mortelles allarmes ; }
    \livretVerse#8 { Sangaride les yeux en larmes }
    \livretVerse#6 { Nous vient d’ouvrir son cœur. }
    \livretPers Atys
    \livretVerse#13 { L’heure aproche où l’Hymen voudra qu’elle se livre }
    \livretVerse#8 { Au pouvoir d’un heureux espoux. }
    \livretPers\line { Idas, & Doris }
    \livretVerse#5 { Elle ne peut vivre }
    \livretVerse#7 { Pour un autre que pour vous. }
    \livretPers Atys
    \livretVerse#12 { Qui peut la dégager du devoir qui la presse ? }
    \livretPers\line { Idas, & Doris }
    \livretVerse#12 { Elle veut elle mesme aux pieds de la Deesse }
    \livretVerse#12 { Declarer hautement vos secretes amours. }
  }
  \column {
    \livretPers Atys
    \livretVerse#8 { Cybele pour moy s’interesse, }
    \livretVerse#12 { J’ose tout esperer de son divin secours… }
    \livretVerse#12 { Mais quoy, trahir le Roy ! tromper son esperance ! }
    \livretVerse#12 { De tant de biens receus est-ce la recompense ? }
    \livretPers\line { Idas, & Doris }
    \livretVerse#6 { Dans l’Empire amoureux }
    \livretVerse#8 { Le Devoir n’a point de puissance ; }
    \livretVerse#4 { L’Amour dispence }
    \livretVerse#8 { Les Rivaux d’estre genereux ; }
    \livretVerse#10 { Il faut souvent pour devenir heureux }
    \livretVerse#8 { Qu’il en couste un peu d’innocence. }
    \livretPers Atys
    \livretVerse#12 { Je souhaite, je crains, je veux, je me repens. }
    \livretPers\line { Idas, & Doris }
    \livretVerse#12 { Verrez-vous un rival heureux à vos dépens ? }
    \livretPers Atys
    \livretVerse#12 { Je ne puis me resoudre à cette violence. }
    \livretPers\line { Atys, Idas, & Doris }
    \livretVerse#10 { En vain, un cœur, incertain de son choix, }
    \livretVerse#8 { Met en balance mille fois }
    \livretVerse#8 { L’Amour et la Reconnoissance, }
    \livretVerse#10 { L’Amour toûjours emporte la balance. }
    \livretPers Atys
    \livretVerse#12 { Le plus juste party cede enfin au plus fort. }
    \livretVerse#8 { Allez, prenez soin de mon sort, }
    \livretVerse#12 { Que Sangaride icy se rende en diligence. }
  }
}#}))

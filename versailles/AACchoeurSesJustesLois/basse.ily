\clef "basse" <<
  \tag #'basse {
    R2.*2 |
    sol4 sol8 la sib sol |
    do'2. |
    R2.*2 |
    fa4 fa8 sol la fa |
    sib2. |
    sol4. sol8[ sol8. sol16] |
  }
  \tag #'basse-continue {
    re'4 re'4. re'8 |
    re'2. |
    sol4 sol sol |
    do'2. |
    fa4 fa4. fa8 |
    fa2. |
    fa2 fa4 |
    sib2. |
    sol2 sol4 |
  }
>>
fa4 fa sol |
mib2. |
re |
<<
  \tag #'basse {
    R2.*2 |
    sol4 sol8 la sib sol |
    do'2. |
    R2.*2 |
    fa4 fa8 sol la fa |
    sib2. |
    sol4. sol8[ sol8. sol16] |
    la4 la re4 |
    la,2. |
    re4 r r |
    r sol sol |
    do r r |
    r fa fa |
  }
  \tag #'basse-continue {
    re'4 re'4. re'8 |
    re'2. |
    sol2 sol4 |
    do'2. |
    fa'4 fa'4. fa'8 |
    fa'2. |
    fa2 fa4 |
    sib2. |
    sol2 sol4 |
    la2 re4 |
    la,2. |
    re4 re'4. re'8 |
    sol4 sol4. sol8 |
    do4 do'4. do'8 |
    fa4 fa fa |
  }
>>
sib,2 sib,4 |
fa2 re4 |
sol4 sol la |
<<
  \tag #'basse {
    sib4 sib sol |
    re'2 re8 re |
    sol4. sol8 mib4 |
  }
  \tag #'basse-continue {
    sib2 sol4 |
    re'2 re8 re |
    sol2 mib4 |
  }
>>
fa4 fa,2 |
sib,4 <<
  \tag #'basse {
    r4 r |
    r4 fa4 fa |
    do4 r4 r |
    r4
  }
  \tag #'basse-continue {
    sib4. sib8 |
    fa4 fa4. fa8 |
    do4 do'4. do'8 |
    sol4
  }
>> sol4 sol |
re2 re4 |
sol2 sol4 |
la4 la la |
sib sib sol |
re'2 <<
  \tag #'basse { re8 re | mib4 mib do | }
  \tag #'basse-continue { re4 | mib2 do4 | }
>>
re2. |
sol,2. |

\tag #'(vdessus2 vhaute-contre2 vtaille2)         { Ses jus -- tes loix, }
\tag #'(vdessus vhaute-contre vtaille vbasse)     { Ses grands ex -- ploits, }
\tag #'(vdessus2 vhaute-contre2 vtaille2)         { Ses jus -- tes loix, }
\tag #'(vdessus vhaute-contre vtaille vbasse)     { Ses grands ex -- ploits, }
Ren -- dent sa me -- moire é -- ter -- nel -- le,
\tag #'(vdessus2 vhaute-contre2 vtaille2)         { Ses jus -- tes loix, }
\tag #'(vdessus vhaute-contre vtaille vbasse)     { Ses grands ex -- ploits, }
\tag #'(vdessus2 vhaute-contre2 vtaille2) { Ses jus -- tes loix, }
\tag #'(vdessus vhaute-contre vtaille vbasse)     { Ses grands ex -- ploits, }
Ren -- dent sa me -- moire é -- ter -- nel -- le :
\tag #'(vdessus2 vhaute-contre2 vtaille2 vbasse2) { Cha -- que jour, }
\tag #'(vdessus vhaute-contre vtaille vbasse)     { chaque ins -- tant }
\tag #'(vdessus2 vhaute-contre2 vtaille2 vbasse2) { Cha -- que jour, }
\tag #'(vdessus vhaute-contre vtaille vbasse)     { chaque ins -- tant }
Ad -- jouste en -- cor à son nom es -- \forceHyphen clat -- tant
U -- ne gloi -- re nou -- vel -- le.
\tag #'(vdessus2 vhaute-contre2 vtaille2 vbasse2) { Cha -- que jour, }
\tag #'(vdessus vhaute-contre vtaille vbasse)     { chaque ins -- tant }
\tag #'(vdessus2 vhaute-contre2 vtaille2 vbasse2) { Cha -- que jour, }
\tag #'(vdessus vhaute-contre vtaille vbasse)     { chaque ins -- tant }
Ad -- jouste en -- cor à son nom es -- clat -- tant
U -- ne gloi -- re nou -- vel -- le.

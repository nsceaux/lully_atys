\piecePartSpecs
#`((dessus #:system-count 4)
   (haute-contre)
   (taille)
   (quinte)
   (basse #:score "score-basse")
   (basse-continue)
   (basse-tous #:score "score-basse")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \column {
\livretPers Chœur des Heures
\livretVerse#4 { Ses justes loix, }
\livretVerse#4 { Ses grands exploits }
\livretVerse#8 { Rendent sa memoire éternelle : }
\livretVerse#6 { Chaque jour, chaque instant }
\livretVerse#10 { Adjouste encor à son nom esclattant }
\livretVerse#6 { Une gloire nouvelle. }
}#}))

\tag #'(sangar basse) {
  Que l’on chan -- te, que l’on dan -- se,
  ri -- ons tous lors -- qu’il le faut,
  que l’on chan -- te, que l’on dan -- se,
  ri -- ons tous lors -- qu’il le faut.

  Ce n’est ja -- mais trop tôt
  que le plai -- sir com -- men -- ce.
  Ce n’est ja -- mais trop tôt
  que le plai -- sir com -- men -- ce.
  On trou -- ve bien -- tôt la fin
  des jours de ré -- jou -- is -- san -- ce,
  on trou -- ve bien -- tôt la fin
  des jours de ré -- jou -- is -- san -- ce ;
  on a beau chas -- ser le cha -- grin,
  il re -- vient plu -- tôt qu’on ne pen -- se.
  On a beau chas -- ser le cha -- grin,
  il re -- vient plu -- tôt qu’on ne pen -- se.

  Que l’on chan -- te, que l’on dan -- se,
  ri -- ons tous lors -- qu’il le faut,
  que l’on chan -- te, que l’on dan -- se,
  ri -- ons tous lors -- qu’il le faut.
}
\tag #'(choeur basse) {
  Que l’on chan -- te, que l’on dan -- se,
  ri -- ons tous lors -- qu’il le faut,
  que l’on chan -- te, que l’on dan -- se,
  ri -- ons tous lors -- qu’il le faut.

  Ce n’est ja -- mais trop tôt
  que le plai -- sir com -- men -- ce.
  Ce n’est ja -- mais trop tôt
  que le plai -- sir com -- men -- ce.

  Que l’on chan -- te, que l’on dan -- se,
  ri -- ons tous lors -- qu’il le faut,
  que l’on chan -- te, que l’on dan -- se,
  ri -- ons tous lors -- qu’il le faut.
}
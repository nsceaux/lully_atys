\score {
  <<
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \hcchantInstr } \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \includeLyrics "paroles"
      \new Staff \with { \tchantInstr } \withLyrics <<
        \global \keepWithTag #'vtaille1 \includeNotes "voix"
      >> \includeLyrics "paroles"
      \new Staff \with { \tchantInstr } \withLyrics <<
        \global \keepWithTag #'vtaille2 \includeNotes "voix"
      >> \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\center-column { BT. Sangar }
        shortInstrumentName = \markup\center-column { BT. Sangar }
      } \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout { }
  \midi { }
}
<<
  \tag #'(vhaute-contre basse) {
    \clef "vhaute-contre"
    <>^\markup "Douze grands dieux de fleuves" mi'4 mi'2 |
    re' re'4 sol' sol'2 |
    mi' mi'4 la' la'2 |
    fad'2. sol'4 sol' fad' |
    sol'2. re'4 mi'2 |
    fa' fa'4 fa' fa'2 |
    mi' mi'4 sol' sol'2 |
    fa'2. re'4 mi' fa' |
    mi'2. mi'4 mi' mi' |
    re'2 mi'4 do'2. |
    fa'4 fa' fa' mi'2 mi'4 |
    mi'2 mi'4 mi' mi' mi' |
    fa'2 fa'4 re'2. |
    sol'4 fa' mi' re'2 sol'4 |
    mi'2 mi'4 mi' mi'2 |
    re'2 re'4 sol' sol'2 |
    mi'2 mi'4 la' la'2 |
    fad'2. sol'4 sol' fad' |
    sol'2. re'4 mi'2 |
    fa'2 fa'4 fa' fa'2 |
    mi'2 mi'4 sol' sol'2 |
    fa'2. re'4 mi' fa' |
    mi'2.
  }
  \tag #'vtaille1 {
    \clef "vtaille" do'4 do'2 |
    si2 si4 mi' mi'2 |
    do' do'4 mi' mi'2 |
    re'2. si4 do' la |
    si2. si4 dod'2 |
    re'2 re'4 re' re'2 |
    do'2 do'4 mi' mi'2 |
    do'2. do'4 do' si |
    do'2. do'4 do' do' |
    si2 do'4 la2. |
    re'4 re' re' si2 mi'4 |
    dod'2 dod'4 dod' dod' dod' |
    re'2 re'4 si2. |
    do'4 si do' do'2 si4 |
    do'2 do'4 do' do'2 |
    si2 si4 mi' mi'2 |
    do' do'4 mi' mi'2 |
    re'2. si4 do' la |
    si2. si4 dod'2 |
    re'2 re'4 re' re'2 |
    do'2 do'4 mi' mi'2 |
    do'2. do'4 do' si |
    do'2.
  }
  \tag #'vtaille2 {
    \clef "vtaille" sol4 sol2 |
    sol2 sol4 si si2 |
    la2 la4 do' do'2 |
    la2. mi'4 mi' re' |
    re'2. sol4 sol2 |
    la la4 la si2 |
    do' do'4 do' do'2 |
    la2. sol4 sol sol |
    sol2. sol4 sol sol |
    sol2 sol4 fa2. |
    la4 la la la2 sold4 |
    la2 la4 la la la |
    la2 la4 sol2. |
    sol4 sol sol sol2 sol4 |
    sol2 sol4 sol sol2 |
    sol sol4 si si2 |
    la la4 do' do'2 |
    la2. mi'4 mi' re' |
    re'2. sol4 sol2 |
    la2 la4 la si2 |
    do' do'4 do' do'2 |
    la2. sol4 sol sol |
    sol2.
  }
  \tag #'vbasse {
    \clef "vbasse" do4 do2 |
    sol2 sol4 mi mi2 |
    la la4 la, la,2 |
    re2. mi4 do re |
    sol,2. sol4 sol2 |
    re re4 re re2 |
    la2 la4 mi mi2 |
    fa2. sol4 sol sol, |
    do2. do4 do do |
    sol2 mi4 fa2. |
    re4 re re mi2 mi4 |
    la,2 la,4 la la la |
    re2 re4 sol2. |
    mi4 re do sol2 sol4 |
    do2 do4 do do2 |
    sol2 sol4 mi mi2 |
    la2 la4 la, la,2 |
    re2. mi4 do re |
    sol,2. sol4 sol2 |
    re2 re4 re re2 |
    la2 la4 mi mi2 |
    fa2. sol4 sol sol, |
    do2.
  }
>>

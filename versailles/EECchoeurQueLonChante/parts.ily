\piecePartSpecs
#`((basse-continue)
   (basse-viole)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers\line { Le Dieu du Fleuve Sangar, & le Chœur }
    \livretVerse#7 { Que l’on chante, que l’on dance, }
    \livretVerse#7 { Rions tous lors qu’il le faut ; }
    \livretVerse#6 { Ce n’est jamais trop tost }
  }
  \column {
    \null
    \livretVerse#6 { Que le plaisir commence. }
    \livretVerse#7 { Que l’on chante, que l’on dance, }
    \livretVerse#7 { Rions tous lors qu’il le faut. }
  }
}#}))

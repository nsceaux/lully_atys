\clef "basse" do2 do' |
si4 sol8 la si4 sol |
do'4 do8 re mi4 do |
fa fa8 mi re mi fa re |
la4 la,8 si, do4 la, |
re2 sol, |
re,4 re8 mi fa4 re |
sol8 la si sol do'4 do |
sol sol,8 la, si,4 sol, |
do4 do8 re mi4 do |
re4 re8 mi fa4 re |
mi4 mi8 fad sold4 mi |
la4 re mi mi, |
la, la,8 si, dod2 |
re4 re'8 do' si2 |
do'4 si8 la sold2 |
la4 la8 sol fad2 |
sol4. fa!8 mi re do re |
mi4. fa8 sol4 sol, |
do4. re8*1/2 mi fa sol la si do'4 |


\clef "quinte" sol' sol'8 fa' mi'4. mi'8 |
re'2 re'4. re'8 |
%{ Ballard : re'4 %} do'4 sol sol sol |
la2 la |
la la4. la8 |
la2 sol4. sol8 |
la2 la4 re' |
re'2 re' |
re'1 |
do'2 do'8 re' mi'4 |
re' la re' si |
si2 si4 si |
la2 mi'4 mi' |
mi'2. mi'8 mi' |
re'4 re'2 re'4 |
do'4 do' si2 |
la2. la8 la |
si4 sol do'2 |
do'4. do'8 sol'4. sol'8 |
sol'1 |

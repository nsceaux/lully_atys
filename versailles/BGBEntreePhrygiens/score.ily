\score {
  \new StaffGroup <<
    \new Staff \with { \dvInstr } << \global \includeNotes "dessus" >>
    \new Staff \with { \hcvInstr } << \global \includeNotes "haute-contre" >>
    \new Staff \with { \tvInstr } << \global \includeNotes "taille" >>
    \new Staff \with { \qvInstr } << \global \includeNotes "quinte" >>
    \new Staff \with { \bvInstr } << \global \includeNotes "basse" >>
  >>
  \layout { }
}

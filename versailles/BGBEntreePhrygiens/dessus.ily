\clef "dessus" do''4 do''8 re'' mi''4 do'' |
sol''4. sol''8 sol''4. fa''8 |
mi''4. re''8 do''4. si'8 |
la'2 fa''4. fa''8 |
mi''2 la''4. la''8 |
fad''4. mi''16 fad'' sol''4. sol''8 |
sol''2 fa''!4. fa''8 |
fa''2 mi''8[ re''16 mi''] mi''8.[ re''16] |
re''1 |
mi''4. fa''8 sol''4. mi''8 |
fa''4. sol''8 la''4. si''8 |
sold''4 mi'' si'4. si'8 |
do''4. re''8 si'4. la'8 |
la'2. mi''8 mi'' |
fa''4 re''2 sol''8 fa'' |
mi''4 mi''2 mi''8 si' |
do''4 la'2 re''8 do'' |
si'4. si'8 do'' re'' mi'' fa'' |
sol''4 la'' re''4. do''8 |
do''1
\clef "taille" mi' mi'8 fa' sol'4 sol' |
sol'2 sol'4. sol'8 |
sol'4. sol'8 sol'2 |
do'4. do'8 re'4. re'8 |
mi'2 mi'4. mi'8 |
re'2 re'4. re'8 |
re'4 la'2 la'4 |
sol'2 sol'4. sol'8 |
sol'1 |
sol'4 sol'8 fa' mi'4 sol' |
fa'2 fa'4. fa'8 |
mi'2 mi'4 mi' |
mi'4 fa' mi' mi'8 re' |
%{ do'4 do'8 %} dod'4 dod'8 re' mi'4. la'8 |
la'4 fa' sol' re' |
mi'2 mi' |
mi'4 do' re'4. re'8 |
re'4. re'8 re'4 sol'8 fa' |
mi'4 re' re'4. re'8 |
mi'1 |

\piecePartSpecs
#`((dessus)
   (haute-contre)
   (taille)
   (quinte)
   (basse #:instrument ,#{\markup\center-column { BVn et Bc } #})
   (basse-continue #:instrument ,#{\markup\center-column { BVn et Bc } #})
   (basse-tous #:instrument ,#{\markup\center-column { BVn et Bc } #}
               #:score-template "score-basse-continue"))

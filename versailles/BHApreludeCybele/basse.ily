\clef "basse" la,2 la4. la8 |
mi2~ mi8 mi re do |
re2~ re8 re do si, |
do2~ do8 do si, la, |
si,2 mi4 mi, |
la, la re2 |
sol4 do sol,2 |
do~ do8 do re mi |
fa2 fad4. fad8 |
sold4. sold8 la4 la, |
mi2 do |
dod2. dod4 |
re2. re4 |
mi4 la, mi,2 |
la,2 la |
dod2. dod4 |
re2. re4 |
mi4 la, mi,2 |
la,1 |

\clef "dessus" la''4. la''8 mi''4. fad''8 |
sol''2. la''8 mi'' |
fa''4. fa''8 fa''4. sol''8 |
mi''4. mi''8 mi''4. fa''8 |
re''4. re''8 re''4 do''8 si' |
do''4 la' fa''4. fa''8 |
fa''4 mi''8 fa'' re''4. do''8 |
do''2 mi''4. mi''8 |
mi''4. fa''8 re''4. re''8 |
re''4. mi''8 do''4. %{ do''8 %} si'8 |
si'2 mi''4. mi''8 |
la''4. sol''8 sol''4 fa''8 mi'' |
fa''2 si'4. si'8 |
si'4 do''8 re'' si'4. la'8 |
la'2 mi''4. mi''8 |
la''4. sol''8 sol''4 fa''8 mi'' |
fa''2 si'4. si'8 |
si'4 do''8 re'' si'4. la'8 |
la'1 |

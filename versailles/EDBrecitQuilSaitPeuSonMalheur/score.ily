\score {
  <<
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \sangarideInstr } \withLyrics <<
        \global \keepWithTag #'sangaride \includeNotes "voix"
      >> \keepWithTag #'sangaride \includeLyrics "paroles"
      \new Staff \with { \atysInstr } \withLyrics <<
        \global \keepWithTag #'atys \includeNotes "voix"
      >> \keepWithTag #'atys \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
        \modVersion {
          s1 s2. s1*5 s2.*3 s1*3 s2. s1 s2.*3 s1*3 s2. s1 s1*4 s2. s1*2 s1*7
          s1 s2. s1 s2. s1 s2.*4 s1*4 s2. s1*7 s2. s1 s2.*2 s1*7 s2.*3 s1*24\break
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
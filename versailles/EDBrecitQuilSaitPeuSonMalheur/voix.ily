<<
  %% Sangaride
  \tag #'(sangaride basse) {
    << { s1 s2. s1*4 s4 } \tag #'sangaride { \sangarideClef R1 | R2. | R1*4 | r4 } >>
    \tag #'basse \sangarideMark r8 \ficta si'8 si'8. si'16 si'8. si'16 |
    do''4 sol'8 sol'16 sol' la'8. sib'16 |
    la'8 la' r do''16 do'' do''8 re''16 mib'' |
    fa''4 sib'8. sib'16 sib'8 sib'16 la' |
    sib'4
    << { s2. s4 } \tag #'sangaride { r4 r2 | r4 } >>
    \tag #'basse \sangarideMark r8 sib' mib''8. mib''16 mib''8 mib'' |
    do''8 do'' r do'' lab'8. lab'16 lab'8. do''16 |
    fa'4 r8 fa'16 fa' sib'8 sib'16 sib' |
    sol'4
    << { s2.*2 s4 } \tag #'sangaride { r4 r2 | R2. | r4 } >>
    \tag #'basse \sangarideMark mib''8 mib''16 re'' do''8. sib'16 |
    la'4 la'8. la'16 re''8 re''16 re'' |
    si'8 si'
    << { s4 } \tag #'sangaride { r4 } >>
    \tag #'basse \sangarideMark si'!8 si' si'8. re''16 |
    sol'4
    << { s2. s4 } \tag #'sangaride { r4 r2 | r4 } >>
    \tag #'basse \sangarideMark r8 mib'' mib''8. re''16 re''8. re''16 |
    sol'4 do''8 do''16 sib' la'8. sol'16 |
    fad'4 fad'8 re'' re''4 re'' |
    si'4. si'8 do''4 do''8 sol' |
    lab'4 lab'8. sol'16 sol'8 sol'16 do'' do''8 do'' |
    la'8. la'16 la'8 la' sib'4. sib'8 |
    sol'4 do''8 do'' fa'4 fa'8. sib'16 |
    sol'8 sol'
    << { s2 s2. } \tag #'sangaride { r2 | r2 r4 } >>
    \tag #'basse \sangarideMark r8 fa'' |
    re''4 re''8. re''16 la'4 la'8. re''16 |
    si'4
    << { s2. s4 } \tag #'sangaride { r4 r2 | r4 } >>
    \tag #'basse \sangarideMark fa''8. re''16 mib''8. do''16 la'8 sib'16 do'' |
    fad'8 fad' re''8. re''16 si'4 r8 si' |
    do''4 do''8. do''16 la'4 la'8 re'' |
    sol'4 sol'8 do'' la' la'16 la' la'8[ sol'16] la' |
    sib'8 sib' << \tag #'sangaride sol'4 \tag #'basse { sol'8 s } >>
    << s4 \tag #'sangaride r4 >>
    \tag #'basse \sangarideMark re''8. re''16 |
    si'4 do''8. mib''16 si'4 do''8[ si'16] do'' |
    do''2( si'4.) do''8 |
    do''4 r8 mib''16 mib'' sol'8 sol'16 la' |
    sib'4 sib'8 sib' sib'4 sib'8 la' |
    la'4 la'8
    << { s4. s1 s2.*4 s1*2 s8 } \tag #'sangaride { r8 r4 | R1 | R2.*4 | R1*2 | r8 } >>
    \tag #'basse \sangarideMark sib'8 sib'4 si'8 si'16 si' si'8 do'' |
    re''8. re''16 re''8. re''16 mib''4 mib''8 fa''16 sol'' |
    do''8 do''16 do'' do''8 re''16 mib'' fa''8. fa''16 |
    re''4 re''
    << { s2 s1*5 s4 } \tag #'sangaride { r2 | R1*5 | r4 } >>
    \tag #'basse \sangarideMark sol''4 r8 re''16 re'' si'8 re'' |
    sol'4 r8 sol'16 sol' do''8 do''16 do'' |
    la'4 fa''8. fa''16 sib'4 r8 sib'16 sib' |
    sol'4 r8 sib'16 sib' mib''8 mib''16 mib'' |
    do''8 do'' r16 re'' re'' re'' re''8[ mib''16] fa'' |
    si'4 re''8. mib''16 do''4 do''8. si'16 |
    do''4 << \tag #'sangaride do''4 \tag #'basse { do''8 s } >>
    << { s4 } \tag #'sangaride { r4 } >>
    \tag #'basse \sangarideMark sol'8. sol'16 |
    la'4
    << { s2 } \tag #'sangaride { r4 r } >>
    \tag #'basse \sangarideMark fa'8. fa'16 |
    sol'4 r8 do'' re'' mib'' re''8. mib''16 |
    mib''4
    << { s2 } \tag #'sangaride { r4 r } >>
    \tag #'basse \sangarideMark re''8. re''16 |
    sol'4 r8 do'' do'' sib' la'8. sol'16 |
    sol'4 si'8. si'16 do''4 do''8. sol'16 |
    lab'4 lab'8 lab'16 lab' sib'8. do''16 |
    fa'4 << \tag #'sangaride fa'4 \tag #'basse { fa'8 s } >>
    << { s4 s2. s1*24 }
      \tag #'sangaride { r4 | R2. | R1*2 |
        r2 r4 do'' |
        do''2 sib'4. sib'8 |
        sib'2 lab'4( sol'8) lab' |
        sol'2. do''4 |
        la'2 sib'4. sib'8 |
        sib'2 la'4. la'8 |
        sib'2. sib'4 |
        sol'2 do''4 do''8 do'' |
        la'2. re''8 re'' |
        sib'2 la'4. re''8 |
        si'2. si'4 |
        do''2 do''4. do''8 |
        re''2 do''4( si'8) do'' |
        si'2. mib''4 |
        mib''2 re''4 re''8 re'' |
        si'2 do''4. do''8 |
        do''2 si'4. do''8 |
        do''2. mib''4 |
        mib''2 re''4 re''8 re'' |
        si'2 do''4. do''8 |
        do''2 si'4. do''8 |
        do''1 |
      } >>
    \tag #'basse \sangarideMark r4 r8 do'' sol'8. sol'16 sol'8. sol'16 |
    mi'4
    << { s2. s1*2 s2.*3 } \tag #'sangaride { r4 r2 | R1*2 | R2.*3 } >>
  }

  %% Atys
  \tag #'(atys basse) {
    \tag #'basse \atysMark
    \tag #'atys \atysClef
    r2 r8 mib'16 mib' do'8 do'16 do' |
    lab4. lab16 do' lab8 lab16 lab16 |
    sol4 sol r8 sol16 sol do'8 do'16 re' |
    mib'4 mib' sol8. sol16 sol8. la16 |
    sib4 sib r8 re'16 re' si8 si16 do' |
    re'4. re'16 re' mib'4 mib'8. fa'16 |
    re'4
    << { s2. s2.*3 s4 } \tag #'atys { r4 r2 | R2.*3 | r4 } >>
    \tag #'basse \atysMark fa'4 sib8. sib16 sib8. sib16 |
    sol4
    << { s2. s1 s2. s4 } \tag #'atys { r4 r2 | R1 | R2. | r4 } >>
    \tag #'basse \atysMark mib'8. sol'16 si4 si8 do' |
    re'4 re'8 re'16 re' si8. re'16 |
    sol8 sol
    << { s2 s2. s4 } \tag #'atys { r2 | R2. | r4 } >>
    \tag #'basse \atysMark sol'4
    << { s2. } \tag #'atys { r2 | r4 } >>
    \tag #'basse \atysMark mib'8. sol16 lab4 lab8. sib16 |
    sol4
    << { s2.*2 s1*5 s4 } \tag #'atys {
        r8 sol' fa'8. fa'16 fa'8. sol'16 |
        mib'4 mib'8 mib'16 re' do'8. sib16 |
        la4 la r2 |
        r8 sol' sol'8. sol'16 mi'4. mi'8 |
        fa'4 fa'8 fa' fa'4 fa'8 mi' |
        fa'8 fa'16 fa' fa'8 fa' re'4. re'8 |
        mib'4 mib'8 mib' mib'4 mib'8 re' |
        mib' mib'
      } >>
    \tag #'basse \atysMark r8 sol' mib' mib'16 sol' |
    do'4 do'8. do'16 la4 
    << { s4 s1 s4 } \tag #'atys { r4 | R1 | r4 } >>
    \tag #'basse \atysMark sol'8. si16 do'8. do'16 fa'8 fa'16 fa' |
    re'8 re'
    << { s2. s1*3 s4. }
      \tag #'atys {
        r4 r2 |
        r2 r4 r8 re' |
        mib'4 mib'8. mib'16 do'4 do'8 fa' |
        sib4 sib8 mib' do' do'16 re' mib'8[ re'16] mib' |
        re'4 r8
      }
    >>
    \tag #'basse \atysMark mib'8 do'4
    << { s4 s1*2 s2. s1 s4. } \tag #'atys {
        fa'8. fa'16 |
        re'4 mib'8. sol'16 re'4 mib'8[ re'16] mib' |
        mib'2( re'4.) do'8 |
        do'4 r2 |
        R1 |
        r4 r8
      } >>
    \tag #'basse \atysMark fa'16 fa' do'8 do'16 re' |
    mib'8. mib'16 mib' mib' mib' sol' do'8 do'16 do' do'8 re'16 mib' |
    fa'8. re'16 la4 la8 si16 do' |
    si8 si sol'8. re'16 re'8 re'16 re' |
    mib'8 mib'16 sol' mib'8 mib'16 mib' do'8 do' |
    fa' fa'16 fa' fa'8 sol' do'8. re'16 |
    sib4 r8 fa' re' re' re' re' |
    mib'4 sib8. do'16 lab4 sol8 lab |
    << \tag #'atys { sol4 sol } \tag #'basse { sol8 s4. } >>
    << { s2 s1 s2. s2 } \tag #'atys { r2 | R1 | R2. | r2 } >>
    \tag #'basse \atysMark si4 si8 si |
    do'4 r sol' sol'8 sol' |
    do'4 r8 fa' sib8. sib16 sib sib sib sib |
    sol4 r8 sol' mib'16 mib' mib' mib' sib8 do' |
    re'8 re'16 sib re' re' re' mib' fa'8 fa'16 fa' fa'8 sol'16 re' |
    mib'8 mib'16 sol' mib' re' do' sib la8 re'16 re' re'8 re'16 re' |
    si4
    << { s2.*2 s1 s2.*2 s1*1 s4. } \tag #'atys { r4 r2 | R2. | R1 | R2.*2 | R1 | r4 r8 } >>
    \tag #'basse \atysMark sol'8 mib' mib'
    << { s4 s4. } \tag #'atys { r4 | r4 r8 } >>
    \tag #'basse \atysMark fa'8 re' re'
    << { s4 s1 s4 } \tag #'atys {
        r4 |
        r4 r8 mib' fa' sol' fa'8. mib'16 |
        mib'4
      } >>
    \tag #'basse \atysMark r8 sol' re' re'
    << { s4 s1*2 s2. s4. } \tag #'atys {
        fa'8. sol'16 |
        \ficta mi'4 r8 mi' fad' sol' fad'8. sol'16 |
        sol'4 r r2 |
        R2. |
        r4 r8
      } >>
    \tag #'basse \atysMark re'16 re' re'8 re'16 re' |
    mib'4 mib'8. sol16 lab8 lab16 sib |
    sol2 sol4 sol' |
    sol'2 fa'4. fa'8 |
    fa'2 mib'4( re'8) mib' |
    re'2. sol'4 |
    mi'4 mi' fa'2~ |
    fa' mib'4. mib'8 |
    mib'2 re'4. mib'8 |
    do'2 do'4 fa' |
    re'2. re'4 |
    mib'2 mi'4 mi'8 mi' |
    fad'2. fad'8 fad' |
    sol'2 fad'4. sol'8 |
    sol'2. sol'4 |
    sol'2 fa'4. fa'8 |
    fa'2 mib'4( re'8) mib' |
    re'2. sol'4 |
    lab'2 fa'4 fa'8 fa' |
    re'2 mib'4. \ficta lab'8 |
    re'2 re'4. mib'8 |
    do'2. sol'4 |
    lab'2 fa'4 fa'8 fa' |
    re'2 mib'4. \ficta lab'8 |
    re'2 re'4. mib'8 |
    do'1 |
    << { s1 s4 } \tag #'atys { R1 | r4 } >>
    \tag #'basse \atysMark r8 do' mi'8. mi'16 mi'8. fa'16 |
    re'4 re' r8 sol16 sol sol8 la16 si |
    do'4 do'8 do' do'4 re'8 mi' |
    la8 la do'16 do' re' mi' fa'8 fa'16 fa' |
    re'8 sol' do' do' do' si |
    do'2. |
  }
>>
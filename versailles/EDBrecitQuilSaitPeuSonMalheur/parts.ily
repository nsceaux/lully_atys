\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix"
                   #:indent 0)
   (basse-viole #:score-template "score-basse-viole-voix"
                #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Atys
    \livretVerse#12 { Qu’il sçait peu son malheur ! et qu’il est déplorable ! }
    \livretVerse#12 { Son amour meritoit un sort plus favorable : }
    \livretVerse#12 { J’ay pitié de l’erreur dont son cœur s’est flatté. }
    \livretPers Sangaride
    \livretVerse#12 { Espargnez-vous le soin d’estre si pitoyable, }
    \livretVerse#12 { Son amour obtiendra ce qu’il a merité. }
    \livretPers Atys
    \livretVerse#12 { Dieux ! qu’est-ce que j’entends ! }
    \livretPers Sangaride
    \livretVerse#12 { \transparent { Dieux ! qu’est-ce que j’entends ! } Qu’il faut que je me vange, }
    \livretVerse#12 { Que j’aime enfin le Roy, qu’il sera mon espoux. }
    \livretPers Atys
    \livretVerse#12 { Sangaride, eh d’où vient ce changement estrange ? }
    \livretPers Sangaride
    \livretVerse#12 { N’est-ce pas vous ingrat qui voulez que je change ? }
    \livretPers Atys
    \livretVerse#12 { Moy ! }
    \livretPers Sangaride
    \livretVerse#12 { \transparent { Moy ! } Quelle trahison ! }
    \livretPers Atys
    \livretVerse#12 { \transparent { Moy ! Quelle trahison ! } Quel funeste couroux ! }
    \livretPers\line { Atys & Sangaride }
    \livretVerse#12 { Pourquoy m’abandonner pour une amour nouvelle ? }
    \livretVerse#12 { Ce n’est pas moy qui rompt une chaisne si belle. }
    \livretPers Atys
    \livretVerse#8 { Beauté trop cruelle, c’est vous, }
    \livretPers Sangaride
    \livretVerse#8 { Amant infidelle, c’est vous, }
    \livretPers Atys
    \livretVerse#8 { Ah ! c’est vous, Beauté trop cruelle, }
    \livretPers Sangaride
    \livretVerse#8 { Ah ! c’est vous Amant infidelle. }
    \livretPers \line { Atys & Sangaride }
    \livretVerse#8 { Beauté trop cruelle, c’est vous, }
    \livretVerse#8 { Amant infidelle, c’est vous, }
    \livretVerse#8 { Qui rompez des liens si doux. }
    \livretPers Sangaride
    \livretVerse#11 { Vous m’avez immolée à l’amour de Cybele. }
  }
  \column {
    \livretPers Atys
    \livretVerse#12 { Il est vray qu’à ses yeux, par un secret effroy, }
    \livretVerse#12 { J’ay voulu de nos cœurs cacher l’intelligence : }
    \livretVerse#12 { Mais ce n’est que pour vous que j’ay crain sa vengeance, }
    \livretVerse#8 { Et je ne la crains pas pour moy. }
    \livretVerse#12 { Cybele m’ayme en vain, et c’est vous que j’adore. }
    \livretPers Sangaride
    \livretVerse#8 { Aprés vostre infidelité, }
    \livretVerse#8 { Auriez-vous bien la cruauté }
    \livretVerse#8 { De vouloir me tromper encore ? }
    \livretPers Atys
    \livretVerse#8 { Moy ! vous trahir ? vous le pensez ? }
    \livretVerse#8 { Ingrate, que vous m’offencez ! }
    \livretVerse#8 { Hé bien, il ne faut plus rien taire, }
    \livretVerse#12 { Je vais de la Déesse attirer la colere, }
    \livretVerse#12 { M’offrir à sa fureur, puisque vous m’y forcez… }
    \livretPers Sangaride
    \livretVerse#12 { Ah ! demeurez, Atys, mes soupçons sont passez ; }
    \livretVerse#12 { Vous m’aimez, je le croy, j’en veux estre certaine. }
    \livretVerse#6 { Je le souhaite assez, }
    \livretVerse#6 { Pour le croire sans peine. }
    \livretPers Atys
    \livretVerse#6 { Je jure, }
    \livretPers Sangaride
    \livretVerse#6 { \transparent { Je jure, } Je promets, }
    \livretPers\line { Atys & Sangaride }
    \livretVerse#6 { De ne changer jamais. }
    \livretPers Sangaride
    \livretVerse#12 { Quel tourment de cacher une si belle flame. }
    \livretPers Atys
    \livretVerse#12 { Redoublons-en l’ardeur dans le fonds de nostre ame. }
    \livretPers\line { Atys & Sangaride }
    \livretVerse#8 { Aimons en secret, aimons-nous : }
    \livretVerse#12 { Aimons plusque jamais, en dépit des Jaloux. }
    \livretPers Sangaride
    \livretVerse#12 { Mon père vient icy, }
    \livretPers Atys
    \livretVerse#12 { \transparent { Mon père vient icy, } Que rien ne vous estonne ; }
    \livretVerse#12 { Servons-nous du pouvoir que Cybele me donne, }
    \livretVerse#8 { Je vais preparer les Zephirs }
    \livretVerse#6 { A suivre nos desirs. }
  }
}#}))

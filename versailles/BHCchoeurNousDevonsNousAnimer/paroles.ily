\tag #'vdessus {
  % Les Choeurs
  Nous de -- vons nous a -- ni -- mer
  D’une ar -- deur nou -- vel -- le,
  Nous de -- vons nous a -- ni -- mer
  D’une ar -- deur nou -- vel -- le,
  S’il faut ho -- no -- rer Cy -- be -- le,
  Il faut en -- cor plus l’ai -- mer.
  S’il faut ho -- no -- rer Cy -- be -- le,
  Il faut en -- cor plus l’ai -- mer,
  Il faut en -- cor plus l’ai -- mer,
  Il faut en -- cor plus l’ai -- mer,
  Il faut en -- cor plus l’ai -- mer.
}
\tag #'vhaute-contre {
  % Les Choeurs
  Nous de -- vons nous a -- ni -- mer
  D’une ar -- deur nou -- vel -- le,
  Nous de -- vons nous a -- ni -- mer
  D’une ar -- deur nou -- vel -- le,
  S’il faut ho -- no -- rer Cy -- be -- le,
  Il faut en -- cor plus l’ai -- mer.
  S’il faut ho -- no -- rer Cy -- be -- le,
  Il faut en -- cor plus l’ai -- mer,
  Il faut en -- cor plus l’ai -- mer,
  Il faut en -- cor plus l’ai -- mer,
  Il faut en -- cor plus l’ai -- mer.
}
\tag #'vtaille {
  % Les Choeurs
  Nous de -- vons nous a -- ni -- mer
  D’une ar -- deur nou -- vel -- le,
  Nous de -- vons nous a -- ni -- mer
  D’une ar -- deur nou -- vel -- le,
  S’il faut ho -- no -- rer Cy -- be -- le,
  Il faut en -- cor plus l’ai -- mer.
  S’il faut ho -- no -- rer Cy -- be -- le,
  Il faut en -- cor plus l’ai -- mer,
  Il faut en -- cor plus l’ai -- mer,
  Il faut en -- cor plus l’ai -- mer,
  Il faut en -- cor plus l’ai -- mer.
}
\tag #'vbasse {
  % Les Choeurs
  Nous de -- vons nous a -- ni -- mer
  D’une ar -- deur nou -- vel -- le,
  Nous de -- vons nous a -- ni -- mer
  D’une ar -- deur nou -- vel -- le,
  S’il faut ho -- no -- rer Cy -- be -- le,
  Il faut en -- cor plus l’ai -- mer.
  S’il faut ho -- no -- rer Cy -- be -- le,
  Il faut en -- cor plus l’ai -- mer,
  Il faut en -- cor plus l’ai -- mer,
  Il faut en -- cor plus l’ai -- mer,
  Il faut en -- cor plus l’ai -- mer.
}

\clef "vdessus" <>^\markup "Chœur des Phrygiens et Phrygiennes" do''4. do''8 |
re''4 re'' re''4. re''8 |
mi''2 re''4 si' |
do''4. re''8 do''4( si') |
do''2 mi''4. mi''8 |
re''4. re''8 re''4. mi''8 |
fa''2 fa''4. sol''8 |
mi''4 mi'' mi''( re'') |
mi''2 r |
R1*2 |
r2 r4 mi'' |
mi''2 mi''4. mi''8 |
fa''2. fa''4 |
re''2 re''4 mi'' |
re''2 re''4 do'' |
do''2( si'4.) do''8 |
do''2 r |
R1 |
r2 r4 re'' |
mi''2 mi''4. mi''8 |
mi''2. fad''4 |
red''2 red''4 si' |
do''2 la'4. sol'8 |
fad'2( sold'4.) la'8 |
sold'2. mi''4 |
mi''2 re''4. do''8 |
do''2( si'4.) la'8 |
la'2 r |
R1*2 |
r2 r4 do'' |
do''2 si'4. la'8 |
la'2( sold'4.) la'8 |
la'2 r |
R1*2 |
r2 r4 mi'' |
mi''2 re''4. do''8 |
do''2( si'4.) la'8 |
la'1 |


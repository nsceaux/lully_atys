\piecePartSpecs
#`((dessus)
   (haute-contre)
   (taille)
   (quinte)
   (basse)
   (basse-continue #:instrument ,#{\markup\center-column { BVn et Bc }#})
   (basse-tous #:score-template "score"
               #:instrument ,#{\markup\center-column { BVn et Bc }#})
   (basse-viole #:score-template "score-basse-viole"
                #:instrument ,#{\markup\center-column { BVn et Bc }#})
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \column {
  \livretPers Les Chœurs
  \livretVerse#7 { Nous devons nous animer }
  \livretVerse#5 { D’une ardeur nouvelle, }
  \livretVerse#7 { S’il faut honorer Cybèle, }
  \livretVerse#7 { Il faut encor plus l’aimer. }
}#}))

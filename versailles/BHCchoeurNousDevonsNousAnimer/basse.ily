\clef "basse" la4 la |
si2. si4 |
do'2 sol |
la4 fa sol2 |
do2. do4 |
sol2 fa4 mi |
re2. re4 |
la4 sol fa2 |
mi mi4 re |
do4 si, do la, |
fa mi fa re |
mi re mi mi, |
la,2 la |
re2. re4 |
sol2. mi4 |
fa2. fa4 |
sol2 sol, |
do4 si, do re |
mi re mi do |
sol la si sol |
do'2 do'4 si |
la2. la4 |
si2 si4 sol |
la2. la4 |
si2 si, |
mi2. do4 |
re2 re4 re |
mi2 mi, |
la,4 si, do re |
mi fad sold mi |
la sol! fa! re |
mi re do la, |
re2 re4. re8 |
mi2 mi, |
la,4 si, do re |
mi fad sold mi |
la sol! fa! re |
mi re do la, |
re2 re4 re |
mi2. mi,4 |
la,1 |

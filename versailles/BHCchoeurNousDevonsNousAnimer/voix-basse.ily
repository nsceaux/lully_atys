\clef "vbasse" la4. la8 |
si4 si si4. si8 |
do'2 sol4 sol |
la fa sol2 |
do do4. do8 |
sol4. sol8 fa4.\trill mi8 |
re2 re4 re |
la sol fa2 |
mi r |
R1*2 |
r2 r4 mi |
la2 la4. la8 |
re2. re4 |
sol2 sol4 mi |
fa2 fa4 fa |
sol2. sol4 |
do2 r |
R1 |
r2 r4 sol |
do'2 do'4. si8 |
la2. la4 |
si2 si4 sol |
la2 la4 la |
si2. si4 |
mi2. do4 |
re2 re4 re |
mi2. mi4 |
la,2 r |
R1*2 |
r2 r4 la |
re2 re4 re |
mi2. mi4 |
la,2 r |
R1*2 |
r2 r4 la |
re2 re4 re |
mi2. mi4 |
la,1 |

\score {
  <<
    \new ChoirStaff <<
      \new Staff \with { \dchantInstr } \withLyrics <<
        \global \includeNotes "voix-dessus"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \with { \hcchantInstr } \withLyrics <<
        \global \includeNotes "voix-haute-contre"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \with { \tchantInstr } \withLyrics <<
        \global \includeNotes "voix-taille"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \with { \dchantInstr } \withLyrics <<
        \global \includeNotes "voix-basse"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new StaffGroup <<
      \new Staff \with { \dvInstr } <<
        \footnoteHere#'(0 . 0) \markup {
          Le dernier chœur manque dans F-V ; nous suivons la source de Valenciennes.
        }
        \global \includeNotes "dessus"
      >>
      \new Staff \with { \hcvInstr } << \global \includeNotes "haute-contre" >>
      \new Staff \with { \tvInstr } << \global \includeNotes "taille" >>
      \new Staff \with { \qvInstr } << \global \includeNotes "quinte" >>
      \new Staff \with { \bvInstr } << \global \keepWithTag #'basse \includeNotes "basse" >>
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \keepWithTag #'basse-continue \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout { }
  \midi { }
}
\clef "dessus" R1 |
<<
  \tag #'dessus1 {
    r4 r8 re'' re''4. re''8 |
    sol''4 sol''8 fad'' mi''4 fad''8 sol'' |
    fad''4. fad''8 sol''4. sol''8 |
    sol''4 fad''8 sol'' mi''4. re''8 |
    re''4. fad''8 fad''4. sold''8 |
    la''4. mi''8 mi''4. fad''8 |
    sol''4. sol''8 sol''4. la''8 |
    si''4 la''8 sol'' fad''4. sol''8 |
    sol''2 r2 |
  }
  \tag #'dessus2 {
    \footnoteHere#'(0 . 0) \markup {
      F-V : \raise#1 \score {
        { \tinyQuote r2 r4 r8 sol' |
          sol'4. sol'8 do''4 do''8 si' |
          la'4 si'8 do'' si'4. mi''8 |
          mi''4 re''8 mi'' dod''4. re''8 |
          re''4. la'8 la'4. si'8 |
          do''4. la'8 la'4. la'8 |
          si'4. si'8 si'4. do''8 |
          re''4 do''8 si' la'4. sol'8 |
          sol'2 r2 | }
        \layout { \quoteLayout }
      }
    }
    r4 r8 si' si'4. si'8 |
    mi''4 mi''8 re'' dod''4. dod''8 |
    re''4 re''8 do''! si'4. si'8 |
    mi''4 re''8 mi'' dod''4. si'16 dod'' |
    re''4. la'8 la'4. si'8 |
    do''4. do''8 do''4. re''8 |
    si'4. si'8 si'4. do''8 |
    re''4 do''8 si' la'4.\trill sol'8 |
    sol'2 r |
  }
>>

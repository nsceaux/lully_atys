\score {
  <<
    \new GrandStaff \with {
      instrumentName = "Violons"
      shortInstrumentName = "Violons"
    } <<
      \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus" >>
      \new Staff << \global \keepWithTag #'dessus2 \includeNotes "dessus" >>
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \irisInstr \haraKiriFirst } \withLyrics <<
        \global \includeNotes "voix"
      >> \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with {
        instrumentName = "Bc"
        shortInstrumentName = "Bc"
      } <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout { system-count = 2 }
  \midi { }
}

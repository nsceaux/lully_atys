\clef "basse" sol,1~ |
sol,4. sol8 sol4. sol8 |
mi4. mi8 la4. la8 |
re4. re8 mi4 mi8 re |
dod4 re la,2 |
re4. re'8 do'4. si8 |
la4 la, do la, |
mi4. mi8 re4 do |
si, do re re, |
sol,1 |

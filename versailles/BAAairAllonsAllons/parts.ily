\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix"
                   #:indent 0)
   (basse-viole #:score-template "score-basse-viole-voix"
                #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Atys
    \livretVerse#8 { Allons, allons, accourez tous, }
    \livretVerse#6 { Cybele va descendre. }
    \livretVerse#12 { Trop heureux Phrygiens, venez icy l’attendre. }
  }
  \column {
    \null
    \livretVerse#8 { Mille Peuples seront jaloux }
    \livretVerse#6 { Des faveurs que sur nous }
    \livretVerse#6 { Sa bonté va répandre. }
  }
}#}))



\clef "basse" sol,1~ |
sol,4 sol fad re |
sol sib8 la sol fa mib re |
mib4 mib8 re do re mib do |
fa2 re |
mib2 fa4 fa, |
sib,1~ |
sib,4 sib la fa |
sib sib8 la sol fa mib re |
do4 mib8 re do sib, la, sol, |
re2 sib, |
do re4 re, |
sol,1~ |
sol,4 sol2 fad4 |
sol2. sol4 |
re2 mib |
fa mib4. re8 |
do2. do4 |
sol2 fad |
sol sol, |
re re, |

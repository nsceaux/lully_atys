\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix"
                   #:indent 0)
   (basse-viole #:score-template "score-basse-viole-voix"
                #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers "Cybele, & Celænus"
    \livretVerse#8 { Venez vous livrer au suplice. }
    \livretPers\line { Atys, & Sangaride }
    \livretVerse#12 { Quoy la Terre et le Ciel contre nous sont armez ? }
    \livretVerse#8 { Souffrirez-vous qu’on nous punisse ? }
    \livretPers "Cybele, & Celænus"
    \livretVerse#7 { Oubliez-vous vostre injustice ? }
    \livretPers\line { Atys & Sangaride }
    \livretVerse#12 { Ne vous souvient-il plus de nous avoir aimez ? }
    \livretPers "Cybele & Celænus"
    \livretVerse#12 { Vous changez mon amour en haine legitime. }
    \livretPers\line { Atys & Sangaride }
    \livretVerse#6 { Pouvez-vous condamner }
    \livretVerse#6 { L’Amour qui nous anime ? }
    \livretVerse#4 { Si c’est un crime, }
    \livretVerse#8 { Quel crime est plus à pardonner ? }
    \livretPers "Cybele & Celænus"
    \livretVerse#8 { Perfide, deviez-vous me taire }
    \livretVerse#12 { Que c’estoit vainement que je voulois vous plaire ? }
    \livretPers\line { Atys & Sangaride }
    \livretVerse#8 { Ne pouvant suivre vos desirs, }
    \livretVerse#8 { Nous croyons ne pouvoir mieux faire }
    \livretVerse#12 { Que de vous épargner de mortels déplaisirs. }
  }
  \column {
    \livretPers Cybele
    \livretVerse#12 { D’un suplice cruel craignez l’horreur extresme. }
    \livretPers  "Cybele & Celænus"
    \livretVerse#8 { Craignez un funeste trépas. }
    \livretPers\line { Atys & Sangaride }
    \livretVerse#12 { Vangez-vous, s’il le faut, ne me pardonnez pas, }
    \livretVerse#8 { Mais pardonnez à ce que j’aime. }
    \livretPers "Cybele & Celænus"
    \livretVerse#12 { C’est peu de nous trahir, vous nous bravez, Ingrats ? }
    \livretPers\line { Atys & Sangaride }
    \livretVerse#12 { Serez-vous sans pitié ? }
    \livretPers "Cybele & Celænus"
    \livretVerse#12 { \transparent { Serez-vous sans pitié ? } Perdez toute esperance. }
    \livretPers\line { Atys & Sangaride }
    \livretVerse#12 { L’Amour nous a forcez à vous faire une offence, }
    \livretVerse#8 { Il demande grace pour nous. }
    \livretPers "Cybele & Celænus"
    \livretVerse#5 { L’Amour en couroux }
    \livretVerse#5 { Demande vengeance. }
    \livretPers Cybele
    \livretVerse#12 { Toy, qui portes par tout et la rage et l’horreur, }
    \livretVerse#12 { Cesse de tourmenter les criminelles Ombres, }
    \livretVerse#12 { Vien, cruelle Alecton, sors des Royaumes sombres, }
    \livretVerse#12 { Inspire au cœur d’Atys ta barbare fureur. }
  }
}#}))

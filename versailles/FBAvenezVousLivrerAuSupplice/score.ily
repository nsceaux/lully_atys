\score {
  <<
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \sangarideInstr \haraKiri } \withLyrics <<
        { \noHaraKiri s2.*4 s1*7 s1 s2.*5 s1 s2. s1*2 s1 s1*5 s2. s1*7
          s2. s1 s1 \revertNoHaraKiri }
        \global \keepWithTag #'sangaride \includeNotes "voix"
      >> \keepWithTag #'sangaride \includeLyrics "paroles"
      \new Staff \with { \atysInstr \haraKiri } \withLyrics <<
        { \noHaraKiri s2.*4 s1*7 s1 s2.*5 s1 s2. s1*2 s1 s1*5 s2. s1*7
          s2. s1 s1 \revertNoHaraKiri }
        \global \keepWithTag #'atys \includeNotes "voix"
      >> \keepWithTag #'atys \includeLyrics "paroles"
      \new Staff \with { \cybeleInstr } \withLyrics <<
        \global \keepWithTag #'cybele \includeNotes "voix"
      >> \keepWithTag #'cybele \includeLyrics "paroles"
      \new Staff \with { \celaenusInstr \haraKiri } \withLyrics <<
        { \noHaraKiri s2.*4 s1*7 s1 s2.*5 s1 s2. s1*2 s1 s1*5 s2. s1*7
          s2. s1 s1 s2.*15 \revertNoHaraKiri }
        \global \keepWithTag #'celaenus \includeNotes "voix"
      >> \keepWithTag #'celaenus \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
        \modVersion { 
          s2.*4 s1*7 s1 s2.*5 s1 s2. s1*2 s1 s1*5 s2. s1*7 s2. s1 s1
          s2.*15 s1\break
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}

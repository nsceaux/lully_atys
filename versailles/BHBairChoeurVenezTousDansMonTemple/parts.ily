\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Cybèle
    \livretVerse#12 { Venez tous dans mon temple, et que chacun révère }
    \livretVerse#12 { Le sacrificateur dont je vais faire choix : }
    \livretVerse#8 { Je m’expliquerai par sa voix, }
    \livretVerse#12 { Les vœux qu’il m’offrira seront sûrs de me plaire. }
    \livretVerse#12 { Je reçois vos respects ; j’aime à voir les honneurs }
    \livretVerse#12 { Dont vous me presentez un éclatant hommage, }
  }
  \column {
    \livretVerse#6 { Mais l’hommage des cœurs }
    \livretVerse#8 { Est ce que j’aime davantage. }
    \livretVerse#7 { Vous devez vous animer }
    \livretVerse#5 { D’une ardeur nouvelle, }
    \livretVerse#7 { S’il faut honorer Cybèle, }
    \livretVerse#7 { Il faut encor plus l’aimer. }
  }
}#}))

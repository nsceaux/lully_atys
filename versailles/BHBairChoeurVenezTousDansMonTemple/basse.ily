\clef "basse" la,1 |
la,2 la,8 sol, |
fa,2 re, |
mi, mi4 la, |
re1 |
sol8 fa mi re16 do sol4 sol, |
do4 do'2 si4 |
do'2 si4 la |
sol2 fa4 mi |
fa2 re |
mi4 do8 si,16 la, mi4 mi, |
la,1~ |
la,2 la4 sold |
la4 sol fa2 |
mi sold, |
la,4 re, mi,2 |
la,4 si, do la, |
fa mi fa re |
mi re mi mi, |
la, si, do la, |
si,2 sol, |
la, si, |
mi4 re do la, |
fa mi fa re |
mi re mi mi, |
la, si, do la, |
re mi fad re |
sol la sol fa! |
mi mi re do |
sol fa sol sol, |
do si, do re |
mi re mi do |
sol fa sol mi |
fa mi fa re |
mi2. do4 |
si,2. la,4 |
mi4 re mi mi, |
la, si, do re |
mi fad sold mi |
la sol! fa! re |
mi re do la, |
re mi fa re |
mi re mi mi, |
la, si, do re |
mi fad sold mi |
la sol! fa! re |
mi re do la, |
re mi fa re |
mi re mi mi, |
la,2

\cybeleClef r8 mi''16 mi'' do''8 do''16 mi'' la'8 la'16 la' mi'8 fa'16 sol' |
mi'8 mi' r mi' mi'16 mi' fa' sol' |
la'4 la'8 la' re''4 re''8 re'' |
si'4 r mi''16 mi'' mi'' si' do''8 do''16 do'' |
la'4 r8 re'' la'8. la'16 si'8. do''16 |
si'8 si'16 si' do''8 re''16 mi'' mi''4( re'') |
do'' r8 sol'16 la' fa'4. fa'16 mi' |
mi'4. do''16 do'' sol'4 sol'8 la' |
si'8. sol'16 sol' la' si' dod''? re'' re'' re'' re'' mi''8. mi''16 |
la'4 la' re''8. do''16 si'8 si'16 si' |
sold'4 do''8 re''16 mi'' si'8. si'16 si'8. do''16 |
la'2 la' |
r la'4 si' |
do''4. re''8 re''4. mi''8 |
mi''2 si'4. si'8 |
do''4. re''8 do''4( si') |
la'2 r |
R1 |
r2 mi''4. si'8 |
do''4. si'8 la'4. sol'8 |
fad'2 si'4. si'8 |
la'4. sol'8 fad'2 |
mi' r |
R1 |
r2 r4 si' |
do''2 do''4. si'8 |
la'2. re''4 |
si'2 si'4 sol' |
do''2 re''4 mi'' |
mi''2( re''4.) do''8 |
do''2 r |
r r4 do'' |
si'2 si'4. do''8 |
la'2. si'4 |
sold'2 sold'4 mi'' |
re''2 re''4 do'' |
do''2( si'4.) la'8 |
la'2 r |
R1*2 |
r2 r4 do'' |
si'2 si'4 la' |
la'2( sold'4.) la'8 |
la'2 r |
R1*2 |
r2 r4 mi'' |
fa''2 re''4. do''8 |
do''2( si'4.) la'8 |
la'2


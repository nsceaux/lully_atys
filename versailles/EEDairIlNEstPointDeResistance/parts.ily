\piecePartSpecs
#`((basse-continue #:clef "alto"
                   #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers "Dieux de Fleuves, Divinitez de Fontaines, & de Ruisseaux"
    \livretVerse#7 { Il n’est point de resistance }
    \livretVerse#7 { Dont le temps ne vienne à bout, }
    \livretVerse#7 { Et l’effort de la constance }
    \livretVerse#7 { A la fin doit vaincre tout. }
    \livretVerse#7 { Tout est doux, et rien ne coûte }
  }
  \column {
    \null
    \livretVerse#7 { Pour un cœur qu’on veut toucher, }
    \livretVerse#7 { L’onde se fait une route }
    \livretVerse#7 { En s’efforçant d’en chercher, }
    \livretVerse#7 { L’eau qui tombe goute à goute }
    \livretVerse#7 { Perce le plus dur Rocher. }
  }
}#}))

Il n’est point de re -- sis -- tan -- ce
Dont le temps ne vienne à bout,
Et l’ef -- fort de la cons -- tan -- ce
A la fin doit vain -- cre tout.

Tout est doux, et rien ne coû -- te
Pour un cœur qu’on veut tou -- cher,
L’on -- de se fait u -- ne rou -- te
En s’ef -- for -- çant d’en cher -- cher,
L’eau qui tom -- be goute à gou -- te
Per -- ce le plus dur ro -- cher.

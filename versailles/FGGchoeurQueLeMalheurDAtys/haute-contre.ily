\clef "haute-contre" R1*2 |
r2 r4 do''8 do'' |
sib'4 sib'8 sib' sib'4. sib'8 |
la'8 la' la'8 la' la'8. la'16 |
fad'4 r r2 |
R1 |
r2 r4 si'8 si' |
do''4 do''8 do'' do''4. re''8 |
si'8. si'16 do''8. do''16 si'8. do''16 |
do''2 r |
R1 |
R1.*2 |
R1 |
R1. |
r2 r8 si' si' si' |
do''4. do''8 do''4. re''8 |
do''2 do''4. sib'8 |
la'2 la'4 la'8 la' |
re''2 re''4 re'' |
do''2. do''4 |
do''2 do''4. do''8 |
do''4. do''8 do''4. re''8 |
si'2 r8 re''8 re'' re''  |
re''4 re'' re''4. re''8 |
re''4 re'' r8 la' la' la' |
si'4. si'8 si'4. si'8 |
sol'2 sol'4 sol'8 sol' |
do''2 do''4 do'' |
la'2. la'4 |
re''4. do''8 si'4. la'8 |
sold'4. sold'8 sold'4. sold'8 |
la'2 r8 dod'' dod'' dod'' |
re''4. re''8 re''4. re''8 |
si'2 si'4 si'8 si' |
do''2 do''4 do'' |
la'2. re''4 |
si'2 do''4. do''8 |
sol'4. sol'8 sol'4. sol'8 |
sol'2 r |
R1 |
r2 r4 do''8 do'' |
sib'4 sib'8 sib' sib'4. sib'8 |
la'8. la'16 la'8 la' la'8. la'16 |
fad'4 r r2 |
R1 |
r2 r4 si'8 si' |
do''4 do''8 do'' do''4. re''8 |
si' si' do''8. re''16 si'8. do''16 |
do''2 r |
R1 |
r2 r4 si'8 si' |
do''4 do''8 do'' do''4. re''8 |
si'8 si' do''8. re''16 si'8. do''16 |
do''2. |

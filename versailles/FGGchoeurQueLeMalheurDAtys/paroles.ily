\tag #'(cybele basse nymphes1) {
  Que le mal -- heur d’A -- tys af -- fli -- ge tout le mon -- de.
}
\tag #'(cybele corybantes1 corybantes2 corybantes3 corybantes4 basse) {
  Que tout sente, i -- cy bas,
  L’hor -- reur d’un si cru -- el tré -- pas.
}
\tag #'(cybele nymphes1 basse) {
  Que le mal -- heur d’A -- tys af -- fli -- ge tout le mon -- de.
}
\tag #'(cybele corybantes1 corybantes2 corybantes3 corybantes4 basse) {
  Que tout sente, i -- cy bas,
  L’hor -- reur d’un si cru -- el tré -- pas.
}
\tag #'(cybele nymphes1 nymphes2 basse) {
  Pe -- ne -- trons tous les cœurs d’u -- ne dou -- leur pro -- fon -- de :
  Que les bois, que les eaux, per -- dent tous leurs ap -- pas.
}
\tag #'(cybele corybantes1 basse) {
  Que le ton -- ner -- re nous res -- pon -- de :
  Que la ter -- re fre -- misse, et trem -- ble sous nos pas.
  Que le ton -- ner -- re nous res -- pon -- de,
  Que le ton -- ner -- re nous res -- pon -- de :
  Que la ter -- re fre -- misse, et trem -- ble sous nos pas.
  Que le ton -- ner -- re nous res -- pon -- de :
  Que la ter -- re fre -- mis -- se,
  Que la ter -- re fre -- misse, et trem -- ble sous nos pas.
}
\tag #'corybantes2 {
  Que le ton -- ner -- re nous res -- pon -- de, nous res -- pon -- de :
  Que la ter -- re fre -- misse, et trem -- ble, et trem -- ble sous nos pas.
  Que le ton -- ner -- re nous res -- pon -- de,
  nous res -- pon -- de, nous res -- pon -- de :
  Que la ter -- re fre -- misse, et tremble, et trem -- ble sous nos pas.
  Que le ton -- ner -- re nous res -- pon -- de :
  Que la ter -- re fre -- misse, et tremble, et trem -- ble sous nos pas.
}
\tag #'corybantes3 {
  Que le ton -- ner -- re nous res -- pon -- de, nous res -- pon -- de :
  Que la ter -- re fré -- misse, et trem -- ble sous nos pas.
  Que le ton -- ner -- re nous res -- pon -- de,
  nous res -- pon -- de, nous res -- pon -- de :
  Que la ter -- re fré -- misse, et trem -- ble sous nos pas.
  Que le ton -- ner -- re nous res -- pon -- de :
  Que la ter -- re fré -- mis -- se,
  Que la ter -- re fré -- misse, et trem -- ble sous nos pas.
}
\tag #'corybantes4 {
  Que le ton -- ner -- re nous res -- pon -- de :
  Que la ter -- re fré -- misse, et trem -- ble sous nos pas.
  Que le ton -- ner -- re nous res -- pon -- de :
  Que la ter -- re fré -- misse, et trem -- ble sous nos pas.
  Que le ton -- ner -- re nous res -- pon -- de :
  Que la ter -- re fré -- misse, et trem -- ble sous nos pas.
}

\tag #'(cybele nymphes1 basse) {
  Que le mal -- heur d’A -- tys af -- fli -- ge tout le mon -- de.
}
\tag #'(cybele nymphes1 dieux basse corybantes1 corybantes2 corybantes3 corybantes4) {
  Que tout sente, i -- cy bas,
  L’hor -- reur d’un si cru -- el tré -- pas.
}
\tag #'(cybele nymphes1 basse) {
  Que le mal -- heur d’A -- tys af -- fli -- ge tout le mon -- de.
}
\tag #'(cybele nymphes1 dieux corybantes1 corybantes2 corybantes3 corybantes4 basse) {
  Que tout sente, i -- cy bas,
  L’hor -- reur d’un si cru -- el tré -- pas.
}
\tag #'(cybele nymphes1 dieux basse) {
  Que le mal -- heur d’A -- tys af -- fli -- ge tout le mon -- de.
}
\tag #'(cybele nymphes1 dieux corybantes1 corybantes2 corybantes3 corybantes4 basse) {
  Que tout sente, i -- cy bas,
  L’hor -- reur d’un si cru -- el tré -- pas.
}

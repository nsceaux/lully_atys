\piecePartSpecs
#`((dessus)
   (haute-contre)
   (taille)
   (quinte)
   (basse #:score "score-basse")
   (basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (basse-tous #:score "score-basse")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers\wordwrap { Cybele, & le Chœur des Divinitez des Bois, & des Eaux }
    \livretVerse#12 { Que le malheur d’Atys afflige tout le monde. }
    \livretPers\line { Cybele, & le Chœur des Corybantes }
    \livretVerse#6 { Que tout sente, icy bas, }
    \livretVerse#8 { L’horreur d’un si cruel trépas. }
    \livretPers\line { Cybele, & le Chœur des Divinitez des Bois, & des Eaux }
    \livretVerse#12 { Penetrons tous les Cœurs d’une douleur profonde : }
    \livretVerse#12 { Que les Bois, que les Eaux, perdent tous leurs appas. }
  }
  \column {
    \livretPers\line { Cybele, & le Chœur des Corybantes }
    \livretVerse#8 { Que le Tonnerre nous responde : }
    \livretVerse#12 { Que la Terre fremisse, et tremble sous nos pas. }
    \livretPers\line { Cybele, & le Chœur des Divinitez des Bois, & des Eaux }
    \livretVerse#12 { Que le malheur d’Atys afflige tout le monde. }
    \livretPers\line { Tous ensemble. }
    \livretVerse#6 { Que tout sente, icy bas, }
    \livretVerse#8 { L’horreur d’un si cruel trépas. }
  }
}#}))

\score {
  <<
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \cybeleInstr } \withLyrics <<
        \global \keepWithTag #'cybele \includeNotes "voix"
      >> \keepWithTag #'cybele \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \dchantInstr } \withLyrics <<
        \global \keepWithTag #'nymphes1 \includeNotes "voix"
      >> \keepWithTag #'nymphes1 \includeLyrics "paroles"
      \new Staff \with { \dchantInstr \haraKiriFirst } \withLyrics <<
        \global \keepWithTag #'nymphes2 \includeNotes "voix"
      >> \keepWithTag #'nymphes2 \includeLyrics "paroles"
      \new Staff \with { \hcchantInstr } \withLyrics <<
        \global \keepWithTag #'dieux1 \includeNotes "voix"
      >> \keepWithTag #'dieux \includeLyrics "paroles"
      \new Staff \with { \tchantInstr } \withLyrics <<
        \global \keepWithTag #'dieux2 \includeNotes "voix"
      >> \keepWithTag #'dieux \includeLyrics "paroles"
      \new Staff \with { \btchantInstr } \withLyrics <<
        \global \keepWithTag #'dieux3 \includeNotes "voix"
      >> \keepWithTag #'dieux \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \with { \dchantInstr } \withLyrics <<
        \global \keepWithTag #'corybantes1 \includeNotes "voix"
      >> \keepWithTag #'corybantes1 \includeLyrics "paroles"
      \new Staff \with { \hcchantInstr } \withLyrics <<
        \global \keepWithTag #'corybantes2 \includeNotes "voix"
      >> \keepWithTag #'corybantes2 \includeLyrics "paroles"
      \new Staff \with { \tchantInstr } \withLyrics <<
        \global \keepWithTag #'corybantes3 \includeNotes "voix"
      >> \keepWithTag #'corybantes3 \includeLyrics "paroles"
      \new Staff \with { \btchantInstr } \withLyrics <<
        \global \keepWithTag #'corybantes4 \includeNotes "voix"
      >> \keepWithTag #'corybantes4 \includeLyrics "paroles"
    >>
    \new StaffGroup <<
      \new Staff \with { \dvInstr } << \global \includeNotes "dessus" >>
      \new Staff \with { \hcvInstr } << \global \includeNotes "haute-contre" >>
      \new Staff \with { \tvInstr } << \global \includeNotes "taille" >>
      \new Staff \with { \qvInstr } << \global \includeNotes "quinte" >>
      \new Staff \with { \bvInstr } << \global \keepWithTag #'basse \includeNotes "basse" >>
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff <<
        \global \keepWithTag #'basse-continue \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout { }
  \midi { }
}

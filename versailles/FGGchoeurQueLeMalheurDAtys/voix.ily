<<
  \tag #'(cybele basse) {
    \cybeleClef
    r2 sol'8 sol'16 sol' sol'8. sol'16 |
    do''4 r8 sol' sib'8. sib'16 sib'8. la'16 |
    la'4 la' %{%} r4 fa''8 fa'' |
    re''4 re''8 re'' re''4. mi''8 |
    dod'' dod'' re''8. mi''16 dod''8. re''16 |
    re''4 r %{%} fad'8 fad'16 fad' fad'8. fad'16 |
    sol'4. sol'8 do''8. do''16 do''8. si'16 |
    si'4 si' %{%} r4 re''8 re'' |
    mi''4 mi''8 mi'' fa''4. fa''8 |
    re''8. re''16 mi''8. fa''16 re''8. do''16 |
    do''2 mi''4. mi''8 |
    mi''2 re''4. re''8 |
    re''2 do''4 do''8 do'' do''4 si' |
    la'2 la' re''4. re''8 |
    re''2 do''4. do''8 |
    la'2 re''4 re''8 do'' do''4( si'8) do'' |
    si'2 r8 re'' re'' re'' |
    mi''4. mi''8 fa''4. fa''8 |
    sol''[ la'' sol'' fa'' mi'' re'' do'' sib'?8]( |
    la'2) la'4 do''8 do'' |
    fa''2 fa''4 fa'' |
    mi''2. mi''4 |
    la'8[ sol' la' si' do'' re'' do'' re'']( |
    mi''4.) mi''8 mi''4. fa''8 |
    re''2 r8 si' si' do'' |
    la'4 la' si'4. do''8 |
    re''4 re'' r8 re'' re'' re'' |
    re''4. re''8 re''4. re''8 |
    si'2 si'4 si'8 si' |
    mi''2 mi''4 mi'' |
    do''2. do''4 |
    fa''8[ sol'' fa'' mi'' re'' mi'' re'' do'']( |
    si'4.) si'8 si'4. mi''8 |
    dod''2 r8 la' la' la' |
    la'4. la'8 la'4. re''8 |
    si'2 si'4 re''8 re'' |
    mi''2 mi''4 mi'' |
    do''2 do''4 fa''8 fa'' |
    re''4 re''8 re'' mi''4 mi'' |
    re'' re'' do''4. re''8 |
    si'2 si'8 si'16 si' si'8. si'16 |
    do''4 r8 sol' sib'8. sib'16 sib'8. do''16 |
    la'4 la' r4 fa''8 fa'' |
    re''4 re''8 re'' re''4. mi''8 |
    dod''8 dod'' re''8. mi''16 dod''8. re''16 |
    re''4 r fad'8 fad'16 fad' fad'8. fad'16 |
    sol'4. sol'8 do''8. do''16 do''8. si'16 |
    si'4 si' r4 re''8 re'' |
    mi''4 mi''8 mi'' fa''4. fa''8 |
    re''8 re'' mi''8. fa''16 re''8. do''16 |
    do''2 mi''8 mi''16 mi'' do''8. do''16 |
    la'4. do''8 fa''8. fa''16 fa''8. fa''16 |
    re''4 re'' r re''8 re'' |
    mi''4 mi''8 mi'' fa''4. fa''8 |
    re''8 re'' mi''8. fa''16 re''8. do''16 |
    do''2. |
  }
  %% Nymphes
  \tag #'nymphes1 {
    \clef "vbas-dessus" <>^\markup { Nymphes et divinités des bois }
    r2 sol'8 sol'16 sol' sol'8. sol'16 |
    do''4 r8 sol' sib'8. sib'16 sib'8. la'16 |
    la'4 la' r2 |
    R1 R2. |
    r2 fad'8 fad'16 fad' fad'8. fad'16 |
    sol'4. sol'8 do''8. do''16 do''8. si'16 |
    si'4 si' r2 |
    R1 R2. |
    r2 mi''4. mi''8 |
    mi''2 re''4. re''8 |
    re''2 do''4 do''8 do'' do''4 si' |
    la'2 la' re''4. re''8 |
    re''2 do''4. do''8 |
    la'2 re''4 re''8 do'' do''4( si'8) do'' |
    si'2 r2 |
    R1*23 |
    r2 si'8 si'16 si' si'8. si'16 |
    do''4 r8 sol' sib'8. sib'16 sib'8. do''16 |
    la'4 la' r4 fa''8 fa'' |
    re''4 re''8 re'' re''4. mi''8 |
    dod''8 dod'' re''8. mi''16 dod''8. re''16 |
    re''4 r fad'8 fad'16 fad' fad'8. fad'16 |
    sol'4. sol'8 do''8. do''16 do''8. si'16 |
    si'4 si' r4 re''8 re'' |
    mi''4 mi''8 mi'' fa''4. fa''8 |
    re''8 re'' mi''8. fa''16 re''8. do''16 |
    do''2 mi''8 mi''16 mi'' do''8. do''16 |
    la'4. do''8 fa''8. fa''16 fa''8. fa''16 |
    re''4 re'' r re''8 re'' |
    mi''4 mi''8 mi'' fa''4. fa''8 |
    re''8 re'' mi''8. fa''16 re''8. do''16 |
    do''2. |
  }
  \tag #'nymphes2 {
    \clef "vbas-dessus" R1*4 R2. R1*4 R2. |
    r2 do''4. do''8 |
    la'2 la'4. si'8 |
    sold'2 la'4 la'8 la' la'4 sol'? |
    fad'2 fad' sib'4. sib'8 |
    sol'2 sol'4. la'8 |
    fad'2 fad'4 fad'8 fad' fad'4. fad'8 |
    sol'2 r2 |
    R1*23
  }
  %% Corybantes
  \tag #'corybantes1 {
    \clef "vbas-dessus" <>^\markup { Corybantes }
    R1*2 |
    r2 r4 fa''8 fa'' |
    re''4 re''8 re'' re''4. mi''8 |
    dod'' dod'' re''8. mi''16 dod''8. re''16 |
    re''4 r r2 |
    R1 |
    r2 r4 re''8 re'' |
    mi''4 mi''8 mi'' fa''4. fa''8 |
    re''8. re''16 mi''8. fa''16 re''8. do''16 |
    do''2 r |
    R1 R1.*2 R1 R1. |
    r2 r8 re'' re'' re'' |
    mi''4. mi''8 fa''4. fa''8 |
    sol''[ la'' sol'' fa'' mi'' re'' do'' sib'?8]( |
    la'2) la'4 do''8 do'' |
    fa''2 fa''4 fa'' |
    mi''2. mi''4 |
    la'8[ sol' la' si' do'' re'' do'' re'']( |
    mi''4.) mi''8 mi''4. fa''8 |
    re''2 r8 si' si' do'' |
    la'4 la' si'4. do''8 |
    re''4 re'' r8 re'' re'' re'' |
    re''4. re''8 re''4. re''8 |
    si'2 si'4 si'8 si' |
    mi''2 mi''4 mi'' |
    do''2. do''4 |
    fa''8[ sol'' fa'' mi'' re'' mi'' re'' do'']( |
    si'4.) si'8 si'4. mi''8 |
    dod''2 r8 la' la' la' |
    la'4. la'8 la'4. re''8 |
    si'2 si'4 re''8 re'' |
    mi''2 mi''4 mi'' |
    do''2 do''4 fa''8 fa'' |
    re''4 re''8 re'' mi''4 mi'' |
    re'' re'' do''4. re''8 |
    si'2 r |
    R1 |
    r2 r4 fa''8 fa'' |
    re''4 re''8 re'' re''4. mi''8 |
    dod''8 dod'' re''8. mi''16 dod''8. re''16 |
    re''4 r4 r2 |
    R1 |
    r2 r4 re''8 re'' |
    mi''4 mi''8 mi'' fa''4. fa''8 |
    re''8 re'' mi''8. fa''16 re''8. do''16 |
    do''2 r2 |
    R1 |
    r2 r4 re''8 re'' |
    mi''4 mi''8 mi'' fa''4. fa''8 |
    re''8 re'' mi''8. fa''16 re''8. do''16 |
    do''2. |
  }
  \tag #'corybantes2 {
    \clef "vhaute-contre" R1*2 |
    r2 r4 la'8 la' |
    fa'4 fa'8 fa' sol'4. sol'8 |
    mi'8 mi' fa' fa' mi'8. la'16 |
    fad'4 r4 r2 |
    R1 |
    r2 r4 sol'8 sol' |
    sol'4 sol'8 sol' la'4. la'8 |
    sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 |
    mi'2 r2 |
    R1 |
    R1.*2 |
    R1 |
    R1. |
    r2 r8 sol' sol' sol' |
    sol'4 sol' la'4. la'8 |
    sol'4. sol'8 sol'4. sol'8 |
    fa'2 fa'4 la'8 la' |
    la'2 la'4 la' |
    la'2. mi'4 |
    fa'2 fa'4 la' |
    sol'4. sol'8 sol'4. sol'8 |
    sol'2 r8 sol' sol' sol' |
    fad'4. fad'8 sol'4. sol'8 |
    la'4 la' fad'4. fad'8 |
    sol'4. sol'8 sol'4. sol'8 |
    sol'2 sol'4 sol'8 sol' |
    sol'2 sol'4 sol' |
    fa'2. fa'4 |
    fa'2. fa'4 |
    mi'4. mi'8 mi'4. mi'8 |
    mi'2 r8 mi' mi' mi' |
    fad'4. fad'8 fad'4. fad'8 |
    sol'2 sol'4 sol'8 sol' |
    sol'2 sol'4 sol' |
    fa'2. la'4 |
    sol'2. sol'4 |
    sol' sol' sol'4. sol'8 |
    sol'2 r2 |
    R1 |
    r2 r4 la'8 la' |
    fa'4 fa'8 fa' sol'4. sol'8 |
    mi'8 mi' fa' fa' mi'8. la'16 |
    fad'4 r4 r2 |
    R1 |
    r2 r4 sol'8 sol' |
    sol'4 sol'8 sol' la'4. la'8 |
    sol'8 sol' sol'8. sol'16 sol'8. sol'16 |
    mi'2 r2 |
    R1 |
    r2 r4 sol'8 sol' |
    sol'4 sol'8 sol' la'4. la'8 |
    sol' sol' sol'8. sol'16 sol'8. sol'16 |
    mi'2. |
  }
  \tag #'corybantes3 {
    \clef "vtaille" R1*2 |
    r2 r4 do'8 do' |
    sib4 sib8 sib sib4. sib8 |
    la la la la la8. la16 |
    la4 r4 r2 |
    R1 |
    r2 r4 si8 si |
    do'4 do'8 do' do'4. re'8 |
    si8. si16 do'8. do'16 si8. do'16 |
    do'2 r2 |
    R1 |
    R1.*2 |
    R1 |
    R1. |
    r2 r8 si si si |
    do'4. do'8 do'4. re'8 |
    do'4. do'8 do'4. do'8 |
    do'2 do'4 la8 la |
    re'2 re'4 re' |
    do'2. do'4 |
    do'1~ |
    do'4. do'8 do'4. re'8 |
    si2 r8 re' re' re' |
    re'4 re' re'4. re'8 |
    re'4 la la4. la8 |
    si4 si re'4. re'8 |
    mi'2 mi'4 sol8 sol |
    do'2 do'4 do' |
    la2. la4 |
    re'8[ mi' re' do' si do' si la]( |
    sold4.) sold8 sold4. sold8 |
    la2 r8 dod' dod' dod' |
    re'4. re'8 re'4. re'8 |
    re'2 re'4 si8 si |
    do'2 do'4 do' |
    la2 la4 re'8 re' |
    si4 si8 si do'4 do' |
    re' re' mi'4. mi'8 |
    re'2 r2 |
    R1 |
    r2 r4 do'8 do' |
    sib4 sib8 sib sib4. sib8 |
    la8 la la la la8. la16 |
    la4 r4 r2 |
    R1 |
    r2 r4 si8 si |
    do'4 do'8 do' do'4. re'8 |
    si si do'8. re'16 si8. do'16 |
    do'2 r2 |
    R1 |
    r2 r4 si8 si |
    do'4 do'8 do' do'4. re'8 |
    si8 si do'8. do'16 si8. do'16 |
    do'2. |
  }
  \tag #'corybantes4 {
    \clef "vbasse" R1*2 |
    r2 r4 fa8 fa |
    sib4 sib8 sib sol4. sol8 |
    la la fa re la,8. re16 |
    re4 r4 r2 |
    R1 |
    r2 r4 sol8 sol |
    do'4 do'8 do' fa4. re8 |
    sol8. sol16 mi8. do16 sol,8. do16 |
    do2 r2 |
    R1 |
    R1.*2 |
    R1 |
    R1. |
    r2 r8 sol8 sol sol |
    do'4\melisma re'8[ do'] sib[ la sol fa]( |
    mi4.)\melismaEnd mi8 mi4. mi8 |
    fa2 fa4 fa8 fa |
    re2 re4 re |
    la2. la4 |
    fa8[ mi fa sol la si la si]( |
    do'4.) do'8 do'4. do'8 |
    sol2 r8 sol sol sol |
    re'4\melisma mi'8[ re' do' si la sol] |
    fad[ sol fad mi re mi re do]( |
    si,4.)\melismaEnd si,8 si,4. si,8 |
    mi2 mi4 mi8 mi |
    do2 do4 do |
    fa2. fa4 |
    re8[ do re mi fa mi fa re]( |
    mi4.) mi8 mi4. mi8 |
    la,2 r8 la la la |
    re'4. re'8 re'4. re'8 |
    sol2 sol4 sol8 sol |
    mi2 mi4 mi |
    fa2. re4 |
    sol4\melisma la8[ sol fa mi re do]( |
    si,4)\melismaEnd si, do8[ si,] do4 |
    sol,2 r2 |
    R1 |
    r2 r4 fa8 fa |
    sib4 sib8 sib sol4. sol8 |
    la8 la fa re la,8. re16 |
    re4 r4 r2 |
    R1 |
    r2 r4 sol8 sol |
    do'4 do'8 do' fa4. re8 |
    sol sol mi do sol,8. do16 |
    do2 r2 |
    R1 |
    r2 r4 sol8 sol |
    do'4 do'8 do' fa4. re8 |
    sol8 sol mi do sol,8. do16 |
    do2. |
  }
  %% Dieux des bois
  \tag #'dieux1 {
    R1*4 R2. R1*4 R2. R1*2 R1.*2 R1 R1. R1*24 R1*2 |
    r2 r4 la'8 la' |
    fa'4 fa'8 fa' sol'4. sol'8 |
    mi'8 mi' fa' fa' mi'8. la'16 |
    fad'4 r r2 |
    R1 |
    r2 r4 sol'8 sol' |
    sol'4 sol'8 sol' la'4. la'8 |
    sol'8 sol' sol'8. sol'16 sol'8. sol'16 |
    mi'2 sol'8 sol'16 sol' sol'8. sol'16 |
    fa'4. la'8 la'8. la'16 la'8. la'16 |
    sol'4 sol' r sol'8 sol' |
    sol'4 sol'8 sol' la'4. la'8 |
    sol' sol' sol'8. sol'16 sol'8. sol'16 |
    mi'2. |
  }
  \tag #'dieux2 {
    \clef "vtaille"
    R1*4 R2. R1*4 R2. R1*2 R1.*2 R1 R1. R1*24 R1*2 |
    r2 r4 do'8 do' |
    sib4 sib8 sib sib4. sib8 |
    la8 la la la la8. la16 |
    la4 r r2 |
    R1 |
    r2 r4 si8 si |
    do'4 do'8 do' do'4. re'8 |
    si si do'8. re'16 si8. do'16 |
    do'2 do'8 do'16 do' do'8. do'16 |
    do'4. la8 re'8. re'16 re'8. re'16 |
    si4 si r si8 si |
    do'4 do'8 do' do'4. re'8 |
    si8 si do'8. do'16 si8. do'16 |
    do'2. |
  }
  \tag #'dieux3 {
    \clef "vbasse"
    R1*4 R2. R1*4 R2. R1*2 R1.*2 R1 R1. R1*24 R1*2 |
    r2 r4 fa8 fa |
    sib4 sib8 sib sol4. sol8 |
    la8 la fa re la,8. re16 |
    re4 r r2 |
    R1 |
    r2 r4 sol8 sol |
    do'4 do'8 do' fa4. re8 |
    sol sol mi do sol,8. do16 |
    do2 do'8 do'16 do' mi8. mi16 |
    fa4. fa8 re8. re16 re8. re16 |
    sol4 sol r sol8 sol |
    do'4 do'8 do' fa4. re8 |
    sol8 sol mi do sol,8. do16 |
    do2. |
  }
>>

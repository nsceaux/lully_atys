\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Cybele
    \livretVerse#12 { Je veux joindre en ces lieux la gloire et l’abondance, }
    \livretVerse#12 { D’un sacrificateur je veux faire le choix, }
    \livretVerse#12 { Et le Roy de Phrygie auroit la preference }
    \livretVerse#12 { Si je voulois choisir entre les plus grands Roys. }
    \livretVerse#12 { Le puissant Dieux des flots vous donna la naissance, }
    \livretVerse#12 { Un Peuple renommé s’est mis sous vostre loy ; }
    \livretVerse#12 { Vous avez sans mon choix, d’ailleurs, trop de puissance, }
    \livretVerse#12 { Je veux faire un bonheur qui ne soit dû qu’à moy. }
    \livretVerse#12 { Vous estimez Atys, et c’est avec justice, }
    \livretVerse#12 { Je pretens que mon choix à vos vœux soit propice, }
    \livretVerse#8 { C’est Atys que je veux choisir. }
    \livretPers Celænus
    \livretVerse#12 { J’aime Atys, et je voy sa gloire avec plaisir. }
    \livretVerse#8 { Je suis Roy, Neptune est mon pere, }
    \livretVerse#12 { J’espouse une Beauté qui va combler mes vœux : }
    \livretVerse#8 { Le souhait qui me reste à faire, }
    \livretVerse#12 { C’est de voir mon Amy parfaitement heureux. }
    \livretPers Cybele
    \livretVerse#12 { Il m’est doux que mon choix à vos désirs réponde ; }
  }
  \column {
    \livretVerse#8 { Une grande Divinité }
    \livretVerse#8 { Doit faire sa felicité }
    \livretVerse#6 { Du bien de tout le monde. }
    \livretVerse#12 { Mais sur tout le bonheur d’un Roy chery des Cieux }
    \livretVerse#8 { Fait le plus doux plaisir des Dieux. }
    \livretPers Celænus
    \livretVerse#12 { Le sang aproche Atys de la Nymphe que j’aime, }
    \livretVerse#8 { Son merite l’égale aux Roys : }
    \livretVerse#8 { Il soûtiendra mieux que moy-mesme }
    \livretVerse#6 { La majesté supresme }
    \livretVerse#6 { De vos divines loix. }
    \livretVerse#8 { Rien ne pourra troubler son zele, }
    \livretVerse#12 { Son cœur s’est conservé libre jusqu’à ce jour ; }
    \livretVerse#8 { Il faut tout un cœur pour Cybele, }
    \livretVerse#12 { A peine tout le mien peut suffire à l’Amour. }
    \livretPers Cybele
    \livretVerse#12 { Portez à votre Amy la premiere nouvelle }
    \livretVerse#12 { De l’honneur éclatant où ma faveur l’appelle. }
  }
}#}))

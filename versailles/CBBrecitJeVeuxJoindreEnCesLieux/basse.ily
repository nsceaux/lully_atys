\clef "basse" sol,2 sol |
fad mi |
re1 |
si,2 do |
do'4 si la2 |
sol4 sold2 |
la4 si8[ sol] la la, |
re2. do8 si, |
la,2 \once\tieDashed mi~ |
mi8 re16 do si,4 do2 |
re sol4 |
mi si,2 |
do2. |
re4. sol,8 re,4 |
sol,1 |
sol2 red |
mi1 |\allowPageTurn
si,2 do |
dod2. |
re2 mi~ |
mi4 do re re, |
sol,1 \allowPageTurn |
do2 sold, |
la, mi4 |
la, la fad2 |
sol4 sol,2 |
la,1 |
re,4 re do |
si,2 la,4 |
sol,2 sol |
mi re |
do fa~ |
fa fad |
sol mi |
re4 do sol sol, |
do2 do' |
si la |
sol fad4 mi |
re re'8 do' si4 la8 sol |
fad4. mi8 fa4 re |
mi do re re, |
sol,8 sol la si do'2 |
si la |
sol fad4 mi |
re re'8 do' si4 la8 sol |
fad4. mi8 fa4 re |
mi do re re, |
sol,2 sol |
fad mi |
re la, |
mi1 | \allowPageTurn
si,2 mi4. re8 |
do4 sold,4. la,8 mi,4 |
la,1 |
si,2 mi8 re do si, |
la, sol, fad,2 |
sol,1 |
do |
re4 fad, sol, re, |
sol,2 sol |
mi2. |
fad2 sol4 |
do re re, |
sol,2. fad,4 |
mi,2 mi4 re |
do1 |
si,2 la, |
si,1 |
mi, |

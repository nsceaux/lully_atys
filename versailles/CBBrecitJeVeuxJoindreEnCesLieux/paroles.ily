%% ACTE 2 SCENE 2
% Cybele
\tag #'(cybele basse) {
  Je veux joindre en ces lieux la gloire et l’a -- bon -- dan -- ce,
  D’un sa -- cri -- fi -- ca -- teur je veux fai -- re le choix,
  Et le roy de Phry -- gie au -- roit la pre -- fe -- ren -- ce
  Si je vou -- lois choi -- sir en -- tre les plus grands roys.
  Le puis -- sant dieu des flots vous don -- na la nais -- san -- ce,
  Un peu -- ple re -- nom -- mé s’est mis sous vos -- tre loy ;
  Vous a -- vez sans mes soins, d’ail -- leurs, trop de puis -- san -- ce,
  Je veux faire un bon -- heur qui ne soit dû qu’à moy.
  Vous es -- ti -- mez A -- tys, et c’est a -- vec jus -- ti -- ce,
  Je pre -- tens que mon choix à vos vœux soit pro -- pi -- ce,
  C’est A -- tys que je veux choi -- sir.
}
% Celaenus
\tag #'(celaenus basse) {
  J’aime A -- tys, et je voy sa gloire a -- vec plai -- sir.
  Je suis roy, Nep -- tune est mon pe -- re,
  J’es -- pouse u -- ne beau -- té qui va com -- bler mes vœux :
  Le sou -- hait qui me reste à fai -- re,
  C’est de voir mon a -- my par -- fai -- te -- ment heu -- reux.
}
% Cybele
\tag #'(cybele basse) {
  Il m’est doux que mon choix à vos dé -- sirs ré -- pon -- de ;
  U -- ne gran -- de di -- vi -- ni -- té
  Doit fai -- re sa fe -- li -- ci -- té
  Du bien de tout le mon -- de.
  Mais sur tout le bon -- heur d’un roy che -- ry des cieux
  Fait le plus doux plai -- sir des dieux.
  Mais sur tout le bon -- heur d’un roy che -- ry des cieux
  Fait le plus doux plai -- sir des dieux.
}
% Celaenus
\tag #'(celaenus basse) {
  Le sang a -- proche A -- tys de la nym -- phe que j’ai -- me,
  Son me -- ri -- te l’é -- gale aux roys :
  Il soû -- tien -- dra mieux que moy- mes -- me
  La ma -- jes -- té su -- pres -- me
  De vos di -- vi -- nes loix.
  Rien ne pour -- ra trou -- bler son ze -- le,
  Son cœur s’est con -- ser -- vé li -- bre jus -- qu’à ce jour ;
  Il faut tout un cœur pour Cy -- be -- le,
  A pei -- ne tout le mien peut suf -- fire à l’a -- mour.
}
% Cybele
\tag #'(cybele basse) {
  Por -- tez à vostre a -- my la pre -- mie -- re nou -- vel -- le
  De l’hon -- neur é -- cla -- tant où ma fa -- veur l’ap -- pel -- le.
}

\notesSection "Acteurs de la tragédie"
\markuplist\abs-fontsize-lines#8 \with-line-width-ratio #0.8
\override-lines #'(column-padding . 20)
\page-columns-title \act\line { ACTEURS DE LA TRAGEDIE } {
  \character-ambitus\wordwrap {
    \smallCaps Atys, \smaller\italic {
      parent de Sangaride, & favori de Celænus roy de Phrygie.
    }
  } "vhaute-contre" { mi la' }
  \character-ambitus\wordwrap {
    \smallCaps Idas, \smaller\italic {
      amy d’Atys, & frère de la Nymphe Doris.
    }
  } "vbasse" { sol, mi' }
  \character-ambitus\wordwrap {
    \smallCaps Sangaride, \smaller\italic {
      nymphe, fille du Fleuve Sangar.
    }
  } "vbas-dessus" { re' sol'' }
  \character-ambitus\wordwrap {
    \smallCaps Doris, \smaller\italic {
      nymphe, amie de Sangaride, & sœur d’Idas.
    }
  } "vbas-dessus" { re' fa'' }
  \choir-ambitus\column {
    \wordwrap { Chœur de Phrygiens & de Phrygiennes }
    \wordwrap {
      Chœur & troupe de peuples differents qui viennent à la feste de Cybele.
    }
    \wordwrap {
      Troupe de dieux de fleuves, & de ruisseaux, & de nymphes
      de fontaines, qui chantent & qui dancent.
    }
    \wordwrap {
      Troupe de divinitez des bois & des eaux.
    }
    \wordwrap {
      Troupe de Corybantes.
    }
  }
  #'("vdessus" "vhaute-contre" "vtaille" "vbasse") {
    { mi' la'' }
    { la sib' }
    { mi fa' }
    { sol, mi' }
  }
  \wordwrap {
    Troupe de Phrygiens & de Phrygiennes qui dancent à la feste de Cybele.
  } \vspace#1
  \character-ambitus\smallCaps La Déesse Cybele "vbas-dessus" { re' sol'' }
  \character-ambitus\wordwrap {
    \smallCaps Melisse, \smaller\italic {
      confidente & prestresse de Cybele.
    }
  } "vbas-dessus" { mi' sol'' }
  \wordwrap {
    Troupe de prestresses de Cybele.
  } \vspace#1
  \character-ambitus\wordwrap {
    \smallCaps Celænus, \smaller\italic {
      roi de Phrygie, fils de Neptune, & amant de Sangaride.
    }
  } "vbasse-taille" { la, mi' }
  \wordwrap {
    Troupe de suivants de Celænus.
  } \vspace#1
  \wordwrap {
    Troupe de Zéphirs chantants, dançants, & volants.
  } \vspace#1
  \character-ambitus\smallCaps Le Dieu du Sommeil "vhaute-contre" { la sol' }
  \character-ambitus\smallCaps Morphée "vhaute-contre" { sol la' }
  \character-ambitus\smallCaps Phantase "vtaille" { fad fa' }
  \character-ambitus\smallCaps Phobetor "vbasse" { sol, re' }
  \wordwrap {
    Troupe de songes agreables.
  } \vspace#1
  \choir-ambitus\wordwrap { Troupe des songes funestes. }
  #'("vhaute-contre" "vtaille" "vtaille" "vbasse") {
    { do' la' }
    { la fa' }
    { fa do' }
    { mi, re' }
  }
  \character-ambitus\wordwrap {
    \smallCaps { Le Dieu du Fleuve Sangar, }
    \smaller\italic { pere de Sangaride. }
  } "vbasse" { sol, re' }
  \smallCaps Alecton
}

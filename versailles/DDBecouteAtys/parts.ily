\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix"
                   #:indent 0)
   (basse-viole #:score-template "score-basse-viole-voix"
                   #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Morphée
    \livretVerse#12 { Escoute, escoute Atys la gloire qui t’appelle, }
    \livretVerse#12 { Sois sensible à l’honneur d’estre aymé de Cybelle, }
    \livretVerse#12 { Joüis heureux Atys de ta felicité. }
    \livretPers\line { Morphée, Phobetor & Phantase }
    \livretVerse#8 { Mais souvien-toy que la Beauté }
    \livretVerse#6 { Quand elle est immortelle, }
    \livretVerse#8 { Demande la fidelité }
    \livretVerse#6 { D’une amour éternelle. }
  }
  \column {
    \null
    \livretPers Phantase
    \livretVerse#6 { Que l’Amour a d’attraits }
    \livretVerse#4 { Lors qu’il commence }
    \livretVerse#8 { A faire sentir sa puissance, }
    \livretVerse#6 { Que l’Amour a d’attraits }
    \livretVerse#4 { Lors qu’il commence }
    \livretVerse#6 { Pour ne finir jamais. }
  }
}#}))

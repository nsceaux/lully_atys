\clef "basse" sol,1~ |
sol, |
fad, |
sol,2 sol |
la sib |
fa4 mib re do8 sib, |
la,4 sol, la, fa, |
sib, sib la sol |
fad mi fad! re |
sol fad sol sol, |
re mi fa re |
sol la sib sol |
la2. la4 |
sol fa mi la |
re2 re'4 do' |
sib2 sib4 sib |
fa4 fa mib4 re |
do r sol2 |
mib2 mib4 mib |
sib2. sib4 |
la4 sol fad2 sol2 |
re2. re4 |
mib2. do4 |
re2 re, |
sol,2. |
sol2 la4 |
sib fad sol |
re2 mi4 |
fad2. |
sol |
la |
sib4 la sol |
fa2 sib4 |
sol2 la4 |
sib fad sol |
re2 do4 |
si,2. |
do |
re2 sol,4 |
re,2. |
%sol, |

<<
  %% Morphée
  \tag #'(morphee basse) {
    \tag #'basse \morpheeMark
    \tag #'morphee \morpheeClef
    r2 r4 re' |
    mib'2. mib'4 |
    re'2. re'4 |
    sib2. sib4 |
    fa'4 fa' fa' fa' |
    fa'2 fa' |
    r do'4 do' |
    re'2 re'4 re' |
    re'2 do'4. re'8 |
    sib2 sib4. do'8 |
    la2 la4. fa'8 |
    fa'4. mi'8 mi'4. mi'8 |
    dod'2. dod'4 |
    dod'? re' re' dod' |
    re'1 |
    fa'2 fa'4 re' |
    do'4. do'8 do'4 re' |
    mib'2 re'4. re'8 |
    sol'2 sol'4 sol' |
    fa'2 fa'4. fa'8 |
    fad'4 sol' la'4 la' re'4. mi'8 |
    fad'2 fad'4 fad' |
    sol'2 sol'4 sol' |
    sol'2( fad') |
    sol'2. |
    \tag #'morphee { R2.*15 }
  }
  %% Phantase
  \tag #'(phantase basse) {
    <<
      \tag #'basse { s1*15 s1*5 s1. s1*3 s2. \phantaseMark }
      \tag #'phantase {
        \phantaseClef R1*15 |
        re'2 re'4 sib |
        la4. la8 la4 si |
        do'2 sib4. sib8 |
        sib2 sib4 mib' |
        re'2 re'4. re'8 |
        do'4 do' do'4. re'8 sib4( la8) sib8 |
        la2 re'4 re' |
        sib2 sib4 do' |
        sib2( la) |
        sol2. |
      }
    >>
    r4 sib do' |
    re' la sib |
    fad2. |
    la4 sib4. do'8 |
    sib4  sol mib' |
    do' re' mib' |
    re' do' sib |
    fa'2 re'4 |
    r sib do' |
    re' la sib |
    fad2. |
    re'4 mib'4. fa'8 |
    mib'4. re'8( do'4) |
    la4 la sib |
    sib( la4.) sol8 |
    %sol2. |
  }
  %% Phobetor
  \tag #'phobetor {
    \phobetorClef R1*15 |
    sib2 sib4 sib |
    fa4. fa8 mib4 re |
    do2 sol4. sol8 |
    mib2 mib4 mib |
    sib2 sib4. sib8 |
    la4 sol fad4. fad8 sol4( fad8) sol8 |
    re2 re4 re |
    mib2 mib4 do |
    re1 |
    sol,2. |
    R2.*15 |
  }
>>

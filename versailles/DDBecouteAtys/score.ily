\score {
  <<
    \new ChoirStaff \with { \forceGroupBracket \haraKiriFirst } <<
      \new Staff \with { \morpheeInstr } \withLyrics <<
        \modVersion { s1*15\break s1*5 s1. s1*3 s2.\break }
        \global \keepWithTag #'morphee \includeNotes "voix"
      >> \keepWithTag #'morphee \includeLyrics "paroles"
      \new Staff \with { \phantaseInstr } \withLyrics <<
        \global \keepWithTag #'phantase \includeNotes "voix"
      >> \keepWithTag #'phantase \includeLyrics "paroles"
      \new Staff \with { \phobetorInstr } \withLyrics <<
        \global \keepWithTag #'phobetor \includeNotes "voix"
      >> \keepWithTag #'phobetor \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout { }
  \midi { }
}

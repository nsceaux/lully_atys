s1*2 <6>2 <5-> <_->1 <6> s1*2 s2 <6> s1 <_->2 <_-> s1 <_-> s1*3
%% Trio
s1 <6> <_->2 <_-> s1*2 <6>1 <_->2 <_+>1 s <4>2 <3> s2.
%% Phantase
<_->2. s2 <_->4 <_+>2. s <_-> <6> s2.*2 <_->2 <6>4 s2 <_->4 <_+>2. s4 <\markup\fontsize#-5 \concat { \bracket \number 5 \hspace#0.2 \figure-flat } >2 <_->2.

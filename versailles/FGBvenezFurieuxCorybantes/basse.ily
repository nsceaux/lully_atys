\clef "basse" do1 |
fa,4 fa mi |
re2 dod |
re1 |
la2 fad |
sol mi |
fa1 |
sol4 fa4 sol sol, |
do1 |
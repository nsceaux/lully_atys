\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Cybele
    \livretVerse#8 { Venez furieux Corybantes, }
    \livretVerse#12 { Venez joindre à mes cris vos clameurs esclatantes ; }
  }
  \column {
    \livretVerse#12 { Venez Nymphes des Eaux, venez Dieux des Forests, }
    \livretVerse#8 { Par vos plaintes les plus touchantes }
    \livretVerse#8 { Secondez mes tristes regrets. }
  }
} #}))

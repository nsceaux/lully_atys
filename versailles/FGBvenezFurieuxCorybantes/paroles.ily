%% ACTE 5 SCENE 7
%% Cybele
Ve -- nez fu -- ri -- eux co -- ry -- ban -- tes,
Ve -- nez joindre à mes cris vos cla -- meurs es -- cla -- tan -- tes ;
Ve -- nez nym -- phes des eaux, ve -- nez dieux des fo -- rests,
Par vos plain -- tes les plus tou -- chan -- tes
Se -- con -- dez mes tris -- tes re -- grets.

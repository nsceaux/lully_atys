\clef "taille" re'4 re' re' |
re' mi'2 |
re' re'4 |
re' re' re' |
re' mi'2 |
re'2. |
la'8 sol' fad'4 mi' |
re'4 si2 |
mi' mi'4 |
mi'4. mi'8 re'4 |
re' dod'4. re'8 |
re'2. |
mi'4 mi' mi' |
mi' la2 |
si si4 |
do'2 do'4 |
re' re'4. do'8 |
si2. |
mi'8 re' mi' fa' mi'4 |
re'4 re'2 |
re'2 si4 |
do'2 do'4 |
re' re'4. do'8 |
si2. |

\clef "basse" sib,2. sib,4 sib, sib, |
mib2. mib4 mib mib |
do2. do4 do do |
re2 sib,4 do re2 |
sol,2 sol1 |
mi1. |
fa2. fa4 re2 |
mib2 mib4 do fa fa, |
sib,1. |
sib2 sib sib |
fa2 fa fa4 re |
mib1. |
sib1 sib,2 |
fa1. |
do1. |
sol,1. |
sol2 sol sol |
do1. |
fa1. |
do'1. |
fa1 fa4 do |
fa1. |
do2 do2 do |
sol1. |
la |
sib |
mib2. do4 fa fa, |
sib,2 r r |

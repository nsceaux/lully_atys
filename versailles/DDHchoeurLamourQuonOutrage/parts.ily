\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers\line { Chœur de Songes funestes }
    \livretVerse#5 { L’amour qu’on outrage }
    \livretVerse#5 { Se transforme en rage, }
    \livretVerse#6 { Et ne pardonne pas }
    \livretVerse#6 { Aux plus charmants appas. }
    \livretVerse#7 { Si tu n’aymes point Cybele }
  }
  \column {
    \null
    \livretVerse#5 { D’une amour fidelle, }
    \livretVerse#8 { Malheureux, que tu souffriras ! }
    \livretVerse#4 { Tu periras : }
    \livretVerse#8 { Crains une vengeance cruelle, }
    \livretVerse#8 { Tremble, crains un affreux trépas. }
  }
} #}))

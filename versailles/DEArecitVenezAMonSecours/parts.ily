\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Atys
    \livretVerse#12 { Venez à mon secours ô Dieux ! ô justes Dieux ! }
    \livretPers Cybele
    \livretVerse#12 { Atys, ne craignez rien, Cybele, est en ces lieux. }
    \livretPers Atys
    \livretVerse#12 { Pardonnez au desordre où mon cœur s’abandonne ; }
    \livretVerse#12 { C’est un songe… }
    \livretPers Cybele
    \livretVerse#12 { \transparent { C’est un songe… } Parlez, quel songe vous estonne ? }
    \livretVerse#8 { Expliquez moy vostre embaras. }
    \livretPers Atys
    \livretVerse#12 { Les songes sont trompeurs, et je ne les croy pas. }
    \livretVerse#6 { Les plaisirs et les peines }
    \livretVerse#8 { Dont en dormant on est séduit, }
    \livretVerse#6 { Sont des chimeres vaines }
    \livretVerse#6 { Que le resveil détruit. }
    \livretPers Cybele
    \livretVerse#8 { Ne mesprisez pas tant les songes }
    \livretVerse#10 { L’Amour peut souvent emprunter leur voix, }
  }
  \column {
    \null
    \livretVerse#7 { S’ils font souvent des mensonges }
    \livretVerse#7 { Ils disent vray quelquefois. }
    \livretVerse#12 { Ils parloient par mon ordre, et vous les devez croire. }
    \livretPers Atys
    \livretVerse#12 { O Ciel ? }
    \livretPers Cybele
    \livretVerse#12 { \transparent { O Ciel ? } N’en doutez point, connoissez vostre gloire. }
    \livretVerse#8 { Respondez avec liberté, }
    \livretVerse#12 { Je vous demande un cœur qui despend de luy-mesme. }
    \livretPers Atys
    \livretVerse#8 { Une grande Divinité }
    \livretVerse#12 { Doit d’assûrer toûjours de mon respect extresme. }
    \livretPers Cybele
    \livretVerse#8 { Les Dieux dans leur grandeur supresme }
    \livretVerse#12 { Reçoivent tant d’honneurs qu’ils en sont rebutez, }
    \livretVerse#12 { Ils se lassent souvent d’estre trop respectez, }
    \livretVerse#8 { Ils sont plus contents qu’on les ayme. }
  }
}#}))

\clef "basse" sib2 sib,4 sib,8 do re4 sib, |
fa4. sol8 la4 sib si2 |
do'4. sib!8 la4 sib sol2 |
fa2. r8 fa la,2 |
sib,2. r8 sib sol2 |
do' la re'~ |
re'4. re8 mi2 fa |
sib,1 la,2~ |
la,4. sib,8 do2 do, |
fa,2 fa4 fa8 sol \sugNotes { la4 fa } |
fa,1. |
fa4. fa8 do4 do8 re mib4 mib8 fa |
sol4. fa8 mib4. re8 do2 |
re4. re16 re re4 re re re |
sol4. sol16 sol sol4 sol sol sol |
do4. do16 do do4 do do do |
fa4. fa16 fa fa4 fa fa fa |
sib,4. sib,16 sib, sib,4 sib, sib, sib, |
mib1. |
fa4 fa8 mib re4 re8 do sib,4 sib,8 la, |
sol,2 mib4 mib8 re do2 |
fa4 fa8 mib re2 sib4 sib8 la |
sol4 re mib2 fa4 fa, |
sib,2. sib,8 do re4 re8 mib |
sib,1. |

\clef "dessus" sib'4 sib'8 do'' re''4 sib' fa''4. sib''8 |
la''4. sol''8 fa''4 re'' sol''8 fa'' mib'' re'' |
mib''4. re''8 do''4 re'' sib'4. do''8 |
la'2. r8 do'' fa''4. fa''8 |
re''2. r8 re'' sol''4. sol''8 |
mi''8 mi'' fa'' sol'' la''4. la''8 fa''4. fa''8 |
sib''2. r8 la'' la''2~ |
la''4. sol''8 sol''2. r8 la'' |
fa''2. r8 sol'' sol''4. fa''8 |
fa''1. |
fa'' |
do''4 do''8 re'' mib''4 mib''8 fa'' sol''4 fa''8 mib'' |
re''4. re''8 sol''4. la''8 la''4. sol''8 |
fad''4. re''16 re'' re''4 re'' la' re'' |
si'4. sol''16 sol'' sol''4 sol'' re'' sol'' |
mi''4. do''16 do'' do''4 do'' sol' do'' |
la'4. fa''16 fa'' fa''4 fa'' do'' fa'' |
re''4. sib''16 sib'' sib''4 sib'' fa'' sib'' |
sol'' sol''8 fa'' mib''4 mib''8 re'' do''4 do''8 sib' |
la'2 fa''4 fa''8 mib'' re''4 re''8 do'' |
sib'4 sib'8 la' sol'2 mib''4 mib''8 re'' |
do''2 fa''4 fa''8 mib'' re''2 |
sib''4 sib''8 fa'' sol'' fa'' mib'' re'' do''4. sib'8 |
sib'1. |
sib'1. |

<<
  \tag #'(sangaride basse) {
    <<
      \tag #'basse { s1 s2. s4 \sangarideMark }
      \tag #'sangaride { \sangarideClef R1 R2. r4 }
    >> r16 sold' sold' sold' la'8. si'16 |
    do''8. do''16 do''8. do''16 do''8. si'16 |
    si'4 si'8 sol' do''8 do''16 do'' do''8 do''16 si' |
    do''8
    <<
      \tag #'basse { s2.. s2. s1*2 s4 \sangarideMark }
      \tag #'sangaride { r8 r4 r2 R2. R1*2 r4 }
    >> r16 mi' mi' mi' la'8. la'16 |
    fad'8 la'16 la' si'4 la'8. sol'16 |
    sol'4
    <<
      \tag #'basse { s2. s2.*5 s1*2 s4 \sangarideMarkShort }
      \tag #'sangaride { r4 r2 R2.*5 R1*2 r4 }
    >> r8 mi'' la'4 <<
      \tag #'basse { s4 s1*5 s4 \sangarideMark }
      \tag #'sangaride { r4 R1*5 r4 }
    >> r8 sol'16 sol' mi'8. do''16 sol'8 sol'16 la' |
    si'8 si' r re'' si'8. si'16 mi''8. si'16 |
    do''8. do''16 do''8. re''16 re''8. mi''16 |
    mi''4 <<
      \tag #'basse { s2 s2.*2 s4 \sangarideMark }
      \tag #'sangaride { r4 r R2.*2 r4 }
    >> r8 mi'16 mi' la'8 la'16 la' |
    fad'8. la'16 la' la' re'' re'' si'8 si' |
    <<
      \tag #'basse { s1*3 s4 \sangarideMarkShort }
      \tag #'sangaride { R1*3 r4 }
    >> si'4 r mi'' |
    <<
      \tag #'basse { s2. \sangarideMark }
      \tag #'sangaride { r2 r4 }
    >> r8 la'16 sol' |
    fad'4 <<
      \tag #'basse { s2. s2.*2 s1*4 s2. s1 s2.*2 s1*2 s4. \sangarideMarkShort }
      \tag #'sangaride { r4 r2 R2.*2 R1*4 R2. R1 R2.*2 R1*2 r4 r8 }
    >> mi''8 fa''4 <<
      \tag #'basse { s4 s1*3 s4 \sangarideMark }
      \tag #'sangaride { r4 R1*3 r4 }
    >> r8 re'' sol'16 sol' sol' sol' sol'8[ fa'16] sol' |
    fad'8 fad' r16 la' la' si' sol'8. sol'16 sol'8. fad'16 |
    sol'4 <<
      \tag #'basse { s2. s1 s4. \sangarideMark }
      \tag #'sangaride { r4 r2 R1 r4 r8 }
    >> si'8 mi''8. mi''16 mi''8 si' |
    do''8. do''16 do''8. si'16 la'8. sol'16 |
    fad'4 r16 si' si' si' mi''8. mi''16 |
    dod''4 r8 dod''16 red'' mi''4 mi''8 red'' |
    mi''4 <<
      \tag #'basse { s2. s2.*2 s4 \sangarideMark }
      \tag #'sangaride { r4 r2 R2.*2 r4 }
    >> r16 sol'16 sol' sol' sol'4 sol'8 fa'16 mi' |
    mi'4 mi'8 <<
      \tag #'basse { s8 s2 s2.*3 s1*3 s4 \sangarideMark }
      \tag #'sangaride { r8 r2 R2.*3 R1*3 r4 }
    >> r8 do'' do''8. do''16 dod''8. dod''16 |
    re''4. fa''8 dod''8. dod''16 dod''8. re''16 |
    re''8 re'' r re'' si'4 sol'8 sol'16 sol' |
    do''8. do''16 re''8 mi'' mi''4( re''8.) do''16 |
    do''4 \tag #'sangaride { r4 r2 R1 R2. R1 r4 r }
  }
  \tag #'(atys basse) {
    \atysClef r2 r8 mi'16 mi' do'8 do'16 do' |
    la4 la8 la16 la re'8 la |
    si4 <<
      \tag #'basse { s2 s2. s1 s8 \atysMark }
      \tag #'atys { r4 r R2. R1 r8  }
    >> mi'16 sol' mi'8 mi'16 mi' do'8. do'16 sol16 sol sol do' |
    la4 r16 do' do' do' dod'8. dod'16 |
    re'8 re'16 re' re'8 re'16 dod' re'4 re' |
    r8 la16 la la8 si do'4 do'8. re'16 |
    si4 <<
      \tag #'basse { s2 s2. s4 \atysMark }
      \tag #'atys { r4 r R2. r4 }
    >> r8 re' si8. si16 si8. si16 |
    do'4 do'8 do'16 do' re'8 mi' |
    la8 la r do'16 do' fa'8 fa'16 fa' |
    re'4 r8 re'16 re' re'8 do'16 si |
    do'4 r8 la16 la mi'8 mi'16 mi' |
    fa'4 r8 re'16 re' si8 si16 si |
    sold4 r8 mi' mi'4 re'8. re'16 |
    re'4 do'8[ si16] do' do'4( si) |
    la4 <<
      \tag #'basse { s2 \atysMark }
      \tag #'atys { r4 r }
    >> r16 do' do' do' |
    dod'8 dod'16 dod' dod'8 dod'16 re' re'8 re'16 re' mi'8 mi'16 mi' |
    la8. la16 la la la si do'8 do' r8 mi' |
    do'8. do'16 do'8. do'16 la8 la16 la re'8 re'16 re' |
    si4 r8 sol'16 sol' do'8 do'16 do' do'8. do'16 |
    la8 la r fa'16 fa' re'8. re'16 re'8 re'16 mi' |
    do'4 <<
      \tag #'basse { s2. s1 s2. s4 \atysMark }
      \tag #'atys { r4 r2 R1 R2. r4 }
    >> r8 si si16 si si si |
    do'8. do'16 la4 la |
    fa'4 fa'16 mi' re' do' si8. do'16 |
    la4 <<
      \tag #'basse { s2 s2. \atysMark }
      \tag #'atys { r4 r R2. }
    >> sol'4 r8 mi'16 mi' dod'8 re' re' dod' |
    re'4 r8 re' si8. si16 si8[ la16] si |
    do'8. do'16 do'8 si si4( la8)[ sold16] la |
    sold4 <<
      \tag #'basse { s2. \atysMark }
      \tag #'atys { r4 r2 }
    >> r8 si si8. si16 do'4 <<
      \tag #'basse { s4 | s \atysMark }
      \tag #'atys { r4 r }
    >> r8 si16 si sold4 sold8 si |
    si16 si si si do'8. do'16 la8 la |
    fa'4 fa'16 mi' re' do' si8. do'16 |
    la4 r mi'8 mi'16 mi' fa' fa' fa' fa' |
    dod'8 dod' r dod' re' re'16 re' la8 si16 do' |
    si8. sol16 sol sol la si do'8. do'16 re' mi' fa' sol' |
    mi'4 do' r8 sol16 sol sol8 la |
    si8. si16 si8. dod'16 dod'8. re'16 |
    re'8 re' fa' r16 re' la8 la16 la si8. do'16 |
    si8 si r si16 dod' re'8 re'16 mi' |
    fa'8. fa'16 fa'8 mi' re'8. do'16 |
    do'4 r8 sol16 sol sold4 sold8 la |
    si4. mi'8 si8. si16 mi'8. si16 |
    do'4 do'8 <<
      \tag #'basse { s4. \atysMark }
      \tag #'atys { r8 r4 }
    >> re'8 re'16 re' |
    si4 r8 si si8. la16 la8[ sol16] la |
    sold4 r si8 si16 si do'8. do'16 |
    la4 la8. si16 do'4 do'8[ si16] do' |
    si4 <<
      \tag #'basse { s2. s1 s4 \atysMark }
      \tag #'atys { r4 r2 R1 r4 }
    >> si8 si16 mi' dod'4 dod'8 red'16 mi' |
    red'8 r16 si16 si8 si si8. si16 si8 la |
    si4 si8 <<
      \tag #'basse { s8 s2 s2.*2 s1 s4 \atysMark }
      \tag #'atys { r8 r2 R2.*2 R1 r4 }
    >> r8 si sold4 r8 mi' |
    dod' dod' r4 r8 mi' |
    fa'4 r8 la16 la re'8 re'16 re' |
    si8 si <<
      \tag #'basse { s2. s4. \atysMark }
      \tag #'atys { r4 r2 r4 r8 }
    >> do'16 do' do'8. si16 si8 do'16 re' |
    sold4 r8 mi'16 mi' dod'8 dod'16 dod' |
    re'4 re'8 re'16 re' mi'8. mi'16 |
    fa'8 fa' r re'16 re' la8 la16 si |
    do'4 do'8 mi' do'4 do'8 si |
    si4 si do'8 do'16 do' re'8. mi'16 |
    fa'4 r8 re' si8. si16 si8. do'16 |
    la4
    <<
      \tag #'basse { s2. s1*3 s4 \atysMark }
      \tag #'atys { r4 r2 R1*3 r4 }
    >> mi'8. mi'16 la4 re'8. si16 |
    sold4 r8 sold16 sold la4. si16 do' |
    si4 si r8 sold |
    sold8. sold16 la8. si16 la4( sold8.) la16 |
    la2
  }
>>
<<
  \tag #'(sangaride basse) {
    \tag #'basse \sangarideMark do''8. do''16 |
    do''2 si'8. si'16 |
    sold'4 la'4. mi'8 |
    fad'4 sol'!4. sol'8 |
    sol'2( fad'4) |
    sol'2 si'8. si'16 |
    si'2 si'4 |
    do''8[ re''] si'4. do''8 |
    do''2 r8 mi'' |
    dod''4 re''4. re''8 |
    re''4 dod''4. dod''8 |
    re''4 re'' r8 la' |
    si'4. si'8 do''8. si'16 |
    la'4 si'4. si'8 |
    sold'4 la'4.( sold'16) la' |
    sold'2 sold'4 |
    la'2 la'8 la' |
    fad'4 fad'4. fad'8 |
    sol'4 sol' si' |
    do''4. la'8 la'8. sol'16 |
    fad'2 fad'8 fad' |
    sol'4 fad'( sol'8) la' |
    sold'2 mi''4 |
    re''4. re''8 do'' si' |
    do''2 do''4 |
    si'4. si'8 si' si' |
    mi'4 la'4. sol'8 |
    fad'4 sold'4. sold'8 |
    la'8[ si'] sold'4. la'8 |
    la'2 mi''4 |
    re''4. re''8 do'' si' |
    do''2 do''4 |
    si'4. si'8 si' si' |
    mi'4 la'4. sol'8 |
    fad'4 sold'4. sold'8 |
    la'[ si'] sold'4. la'8 |
    la'4
  }
  %% Atys
  \tag #'atys {
    mi'8. mi'16 |
    mi'2 re'8. re'16 |
    re'4 do'4. do'8 |
    do'4 si4. do'8 |
    la4 la re'8. do'16 |
    si2 re'8. re'16 |
    re'2 re'4 |
    mi'8[ fa'] re'4. do'8 |
    do'2 r8 sol' |
    sol'4 fa'( mi'8.) fa'16 |
    mi'4 mi'( re'8) mi' |
    fa'4 re' r8 fa' |
    fa'4. mi'8 mi'8. mi'16 |
    mi'4 re'4. re'8 |
    re'4 do'4.( si16) do' |
    si2 si4 |
    do'2 do'8 do' |
    la4 la4. re'8 |
    si4 si sol' |
    sol'4. fad'8 fad'8. mi'16 |
    red'2 red'8 red' |
    mi'4 red'4. mi'8 |
    mi'2 r4 |
    R2. |
    r4 r mi'4 |
    re'4. re'8 re' re' |
    re'4 do'4. do'8 |
    do'8[ re'] si4. si8 |
    do'[ re'] si4. la8 |
    la2 r4 |
    R2. |
    r4 r mi'4 |
    re'4. re'8 re' re' |
    re'4 do'4. do'8 |
    do'4 si4. si8 |
    do'[ re'] si4. la8 |
    la4
  }
>>
<<
  \tag #'(sangaride basse) {
    <<
      \tag #'basse { s2. s1 s4 \sangarideMark }
      \tag #'sangaride { r4 r2 R1 r4 }
    >> r8 re'' si'4 r16 si'16 si' si' |
    mi'4 r8 mi' la'4 la'8 la'16 si' |
    sold'2 r4 |
    \tag #'sangaride { R2.*26 R1 }
  }
  \tag #'(atys basse) {
    \tag #'basse \atysMark
    r8 mi' do'4 do'8 do'16 do' |
    la4 la fa'4 fa'16 fa' fa' fa' |
    re'4 <<
      \tag #'basse { s2. s1 s2. \atysMark }
      \tag #'atys { r4 r2 R1 R2. }
    >> r8 mi' do'4 la |
    re' re' si |
    do'2 la4 |
    r mi' fa' |
    sol'2 mi'4 |
    fa' re'2 |
    do'2. |
    \footnoteHere#'(0 . 0) \markup { Ballard ne propose pas la reprise des premières mesures du petit air. }
    r8 mi' do'4 la |
    re' re' si |
    do'2 la4 |
    r mi' fa' |
    sol'2 mi'4 |
    fa' re'2 | 
    do'2 do'4 |
    si si do' |
    la la fa'8[ mi'] |
    re'[ do'] si4. la8 |
    sold2 mi'4 |
    dod' re' mi' |
    fa' re' fa'8[ mi'] |
    re'[ do'] si4. la8 |
    la2 mi'4 |
    dod' re' mi' |
    fa' re' fa'8[ mi'] |
    re'[ do'] si4. la8 |
    la2. |
    R1 |
  }
>>

\clef "basse" la,1 |
fa2. |
mi4. re8 do si, |
la, sol, fad,2 |
sol,4 sol mi8 fa re4 |
do1 |
fa2 mi4 |
re la8 la, re2 |
re4 do8 si, la,2 |
mi2 dod4 |
re sol, re, |
sol,2 sol |
mi2. |
fa2 re4 |
sol2 sold4 |
la2 dod4 |
re2. |
mi4 do re2 |
mi4 la, mi,2 |
la, fa |
mi re4 dod |
re2 la~ |\allowPageTurn
la fad |
sol mi |
fa sol4 sol, |
do2 do'4 si8 la |
sol2 sold |
la fa4 |
mi2 sold,4 |
la, la2 |
re2 mi4 |
la,2. |
re4 fad sol |
sol2 la8 re la,4 |
re2 sold, |
la,4 la8 sol fa2 |
mi1 | \allowPageTurn
mi,2 la, |
si, mi |
sold,4 la, fa |
re2 mi4 |
la,1 |\allowPageTurn
la2 fad |
sol4. fa8 mi4 re |
do2. si,8 la, |
sol,2 mi,4 |
re,2 re |
sol fa8 mi |
re4 sol8 do sol,4 |
do2 si,4. la,8 |
sold,1 | \allowPageTurn
la,2 re |
mi red |
mi2. la,4 |
re2 fad, |
sol,4 sol8 fa mi2 |
re4 re mi8 do re4 |
sol,2 la, |
si, do |
si,4 si sold2 |
la2. |
si2 sold4 |
la2 sol8 la si si, |
mi1 |
la,2 la4 |
re2. |
sol2 si, |
do re |
mi2 la4 |
fa2 dod4 |
re2. |
la,1 |\allowPageTurn
mi2 do |
re mi4 mi, |
la,2 la4 sol |
fa2 mi |
re sol |
mi4 re8 do sol4 sol, |
do2 fa |
mi4. re8 do4 si,8 la, |
mi2. |
re4 do8 la, mi4 mi, |
la,2 \bar "" \allowPageTurn la4 |
re2. |
mi4 la,2 re4 sol,2 |
re,2. |
sol,2 sol4 |
fa2. |
mi8 re sol4 sol, |
do4 do'4. si8 |
la4 re2 |
la,2. |
re2 re4 |
sol2 do4 |
fa2 re4 |
mi la,2 |
mi, mi4 |
la,2 la,4 |
re2 re,4 |
sol,2 sol4 |
la2. |
si |
la4 si si, |
mi2 mi4 |
fad sold2 |
la la,4~ |
la, sold,4. la,16 si, |
do2 la,4 |
re2. |
do8 la, mi4 mi, |
la,2 la4 |
fad sold2 |
la2 la,4~ |
la,4 sold,4. la,16 si, |
do2 la,4 |
re2. |
do8 la, mi4 mi, |
la,1 |
fa2 re |
sol sold |
la fa |
mi mi,4 |
la,2 la4~ |
la sold2 |
la4 la,4. si,8 |
do2 re4 |
mi si, do |
fa, sol,2 |
do4. re8 do si, |
la,2 la4~ |
la4 sold2 |
la4 la,4. si,8
do2 re4 |
mi4 si, do |
fa, sol,2 |
do do4 |
sol sol mi |
fa2 re8 mi |
fa2 re4 |
mi4. fad8 sold4 |
la8 sol fa4 mi |
re2 sold,4 |
la, mi,2 |
la,2. |
la8 sol fa4 mi |
re2 sold,4 |
la, mi,2 |
la,2 si,4 |
do1 |


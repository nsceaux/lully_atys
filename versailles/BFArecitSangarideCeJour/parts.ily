\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix"
                   #:indent 0)
   (basse-viole #:score-template "score-basse-viole-voix"
                #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Atys
    \livretVerse#12 { Sangaride, ce jour est un grand jour pour vous. }
    \livretPers Sangaride
    \livretVerse#12 { Nous ordonnons tous deux la feste de Cybele, }
    \livretVerse#9 { L’honneur est égal entre nous. }
    \livretPers Atys
    \livretVerse#12 { Ce jour mesme, un grand Roy doit estre vostre espoux, }
    \livretVerse#12 { Je ne vous vis jamais si contente et si belle ; }
    \livretVerse#8 { Que le sort du Roy sera doux ! }
    \livretPers Sangaride
    \livretVerse#12 { L’indifferent Atys n’en sera point jaloux. }
    \livretPers Atys
    \livretVerse#12 { Vivez tous deux contens, c’est ma plus chere envie ; }
    \livretVerse#13 { J’ay pressé vostre hymen, j’ay servy vos amours. }
    \livretVerse#12 { Mais enfin ce grand jour, le plus beau de vos jours, }
    \livretVerse#8 { Sera le dernier de ma vie. }
    \livretPers Sangaride
    \livretVerse#12 { O dieux ! }
    \livretPers Atys
    \livretVerse#12 { \transparent { O dieux ! } Ce n’est qu’à vous que je veux reveler }
    \livretVerse#12 { Le secret desespoir où mon malheur me livre ; }
    \livretVerse#12 { Je n’ay que trop sceu feindre, il est temps de parler ; }
    \livretVerse#8 { Qui n’a plus qu’un moment à vivre, }
    \livretVerse#8 { N’a plus rien à dissimuler. }
    \livretPers Sangaride
    \livretVerse#8 { Je fremis, ma crainte est extresme ; }
    \livretVerse#12 { Atys, par quel malheur faut-il vous voir perir ? }
    \livretPers Atys
    \livretVerse#8 { Vous me condamnerez vous mesme, }
    \livretVerse#8 { Et vous me laisserez mourir. }
    \livretPers Sangaride
    \livretVerse#12 { J’armeray, s’il se faut, tout le pouvoir supresme… }
    \livretPers Atys
    \livretVerse#8 { Non, rien ne peut me secourir, }
    \livretVerse#12 { Je meurs d’amour pour vous, je n’en sçaurois guerir ; }
    \livretPers Sangaride
    \livretVerse#12 { Quoy ? vous ? }
    \livretPers Atys
    \livretVerse#12 { \transparent { Quoy ? vous ? } Il est trop vray. }
    \livretPers Sangaride
    \livretVerse#12 { \transparent { Quoy ? vous ? Il est trop vray. } Vous m’aimez ? }
    \livretPers Atys
    \livretVerse#12 { \transparent { Quoy ? vous ? Il est trop vray. Vous m’aimez ? } Je vous aime. }
    \livretVerse#8 { Vous me condamnerez vous mesme, }
    \livretVerse#8 { Et vous me laisserez mourir. }
    \livretVerse#8 { J’ay merité qu’on me punisse, }
    \livretVerse#8 { J’offence un Rival genereux, }
    \livretVerse#12 { Qui par mille bien-faits a prevenu mes vœux : }
    \livretVerse#12 { Mais je l’offence en vain, vous luy rendez justice ; }
  }
  \column {
    \livretVerse#8 { Ah ! que c’est un cruel suplice }
    \livretVerse#12 { D’avoüer qu’un Rival est digne d’estre heureux ! }
    \livretVerse#12 { Prononcez mon arrest, parlez sans vous contraindre. }
    \livretPers Sangaride
    \livretVerse#12 { Helas ! }
    \livretPers Atys
    \livretVerse#12 { \transparent { Helas ! } Vous soûpirez ? je voy couler vos pleurs ? }
    \livretVerse#12 { D’un malheureux amour plaignez-vous les douleurs ? }
    \livretPers Sangaride
    \livretVerse#8 { Atys, que vous seriez à plaindre }
    \livretVerse#8 { Si vous sçaviez tous vos malheurs ! }
    \livretPers Atys
    \livretVerse#8 { Si je vous pers, et si je meurs, }
    \livretVerse#8 { Que puis-je encore avoir à craindre ? }
    \livretPers Sangaride
    \livretVerse#12 { C’est peu de perdre en moy ce qui vous a charmé, }
    \livretVerse#12 { Vous me perdez, Atys, et vous estes aimé. }
    \livretPers Atys
    \livretVerse#12 { Aimé ! qu’entens-je ? ô Ciel ! quel aveu favorable ! }
    \livretPers Sangaride
    \livretVerse#8 { Vous en serez plus miserable. }
    \livretPers Atys
    \livretVerse#8 { Mon malheur en est plus affreux, }
    \livretVerse#12 { Le bonheur que je pers doit redoubler ma rage ; }
    \livretVerse#12 { Mais n’importe, aimez-moy, s’il se peut, d’avantage, }
    \livretVerse#12 { Quand j’en devrois mourir cent fois plus malheureux. }
    \livretPers Sangaride
    \livretVerse#12 { Si vous cherchez la mort, il faut que je vous suive ; }
    \livretVerse#12 { Vivez, c’est mon amour qui vous en fait la loy. }
    \livretPers Atys
    \livretVerse#6 { Hé comment ! hé pourquoy }
    \livretVerse#6 { Voulez-vous que je vive, }
    \livretVerse#8 { Si vous ne vivez pas pour moy ? }
    \livretPers\line { Atys & Sangaride }
    \livretVerse#12 { Si l’Hymen unissoit mon destin et le vostre, }
    \livretVerse#8 { Que ses nœuds auroient eû d’attraits ! }
    \livretVerse#8 { L’Amour fit nos cœurs l’un pour l’autre, }
    \livretVerse#12 { Faut-il que le devoir les separe à jamais ? }
    \livretPers Atys
    \livretVerse#6 { Devoir impitoyable ! }
    \livretVerse#6 { Ah quelle cruauté ! }
    \livretPers Sangaride
    \livretVerse#12 { On vient, feignez encor, craignez d’estre écouté. }
    \livretPers Atys
    \livretVerse#7 { Aimons un bien plus durable }
    \livretVerse#7 { Que l’éclat de la Beauté : }
    \livretVerse#5 { Rien n’est plus aimable }
    \livretVerse#5 { Que la liberté. }
  }
}#}))

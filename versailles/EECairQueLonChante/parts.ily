\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse-continue)
   (basse-viole)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers\line { Le Dieu du Fleuve Sangar }
    \livretVerse#7 { Que l’on chante, que l’on dance, }
    \livretVerse#7 { Rions tous lors qu’il le faut ; }
    \livretVerse#6 { Ce n’est jamais trop tost }
    \livretVerse#6 { Que le plaisir commence. }
  }
  \column {
    \null
    \livretVerse#7 { On trouve bien-tost la fin }
    \livretVerse#7 { Des jours de réjoüissance, }
    \livretVerse#8 { On a beau chasser le chagrin, }
    \livretVerse#8 { Il revient plustost qu’on ne pense. }
  }
}#}))

\clef "basse" do4 do2 |
sol2 sol4 mi mi2 |
la la4 fa fa2 |
do'2. do'4 si do' |
sol2. sol4 sol2 |
re re4 re re2 |
la2 la4 mi4 mi2 |
fa2. re4 re sol |
do2. do4 do do |
sol2 mi4 fa2. |
re4 re re mi2 mi4 |
la,2 la,4 la4 la la |
re2 re4 sol2. |
mi4 re do sol2 sol,4 |
do2 do4 do'2 do'4 |
la2 la4 sib2 sib4 |
sol2 sol4 la2 la4 |
fa mi re la2 la4 |
la,2 la4 re' re' re' |
si2 si4 do'2 do'4 |
la2 la4 sib la sib |
fa2 fa4 fa, fa re |
mib2 mib4 do do do |
re re sib,4 mib2 do4 |
re do re sol,2 sol,4 |
sol2 sol4 mi2 mi4 |
fa2 fa4 re2 re4 |
sol2 mi4 la fa sol |
do2 do4 do do2 |
sol2 sol4 mi mi2 |
la la4 fa fa2 |
do'2. do'4 si do' |
sol2. sol4 sol2 |
re2 re4 re re2 |
la2 la4 mi mi2 |
fa2. re4 re sol |
do2.

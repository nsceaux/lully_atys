\clef "dessus" sol''4 mi''2 |
re'' re''4 sol'' sol''2 |
mi'' mi''4 la'' la''2 |
sol''2. do''4 re'' mi'' |
re''2. re''4 mi''2 |
fa'' fa''4 fa'' fa''2 |
mi'' mi''4 mi'' sol''2 |
do''2. fa''4 fa'' re'' |
mi''2. sol''4 fa'' mi'' |
re''2 mi''4 do''2. |
fa''4 fa''8 mi'' re'' do'' si'2 mi''4 |
dod''2 dod''4 mi'' mi'' mi'' |
fa''2 fa''4 re''2. |
sol''4 fa'' mi'' re''2 sol''4 |
mi''2 mi''4 r4 r sol''4 |
la'' la'' la'' fa''2 fa''4 |
sib''8 la'' sol''4. fa''8 mi''2 mi''4 |
re'' dod'' re'' dod''2 dod''4 |
r4 r mi''4 fa''4. sol''8 la''4 |
sol''2 sol''4 sol''2 sol''4 |
la'' fa'' do'' sib' do'' re'' |
do''2 do''4 r fa'' sib'' |
sol''2 sol''4 sol'' sol'' la'' |
fad'' fad'' sib'' sol''2 sol''4 |
sol''4. sol''8 fad''4 sol''2 sol''4 |
r re'' re'' mi''4. fa''8 sol''4 |
do'' do'' do'' fa'' fa'' fa'' |
re''2 sol''4 mi'' fa'' re'' |
mi''2 mi''4 sol'' mi''2 |
re'' re''4 sol'' sol''2 |
mi'' mi''4 la'' la''2 |
sol''2. do''4 re'' mi'' |
re''2. re''4 mi''2 |
fa'' fa''4 fa'' fa''2 |
mi'' mi''4 mi'' sol''2 |
do''2. fa''4 fa'' re'' |
mi''2.

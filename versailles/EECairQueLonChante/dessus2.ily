\clef "dessus" mi''4 do''2 |
si' si'4 si' mi''2 |
do'' do''4 do'' fa''2 |
mi''2. mi''4 re'' do'' |
si'2. si'4 dod''2 |
re'' la'4 la' si'2 |
do'' do''4 do'' mi''2 |
la'2. la'4 re'' si' |
do''2. mi''4 re'' do'' |
si'2 do''4 la'2. |
la''4 la'' la'' sold''2 sold''4 |
la''2 mi''4 dod'' dod'' dod'' |
re''2 la'4 si'2. |
do''4 si' do'' do''2 si'4 |
do''2 do''4 r4 r mi''4 |
fa'' fa'' fa'' re''2 re''4 |
sol''8 fa'' mi''4. re''8 dod''2 dod''4 |
la''4 sol'' fa'' mi''2 mi''4 |
r4 r dod''4 re''4. mi''8 fa''4 |
fa''2 re''4 mi''2 mi''4 |
fa''2 la''4 re'' do'' sib' |
la'2 la'4 r la' re'' |
sib'2 sib'4 mib''4. re''8 do'' sib' |
la'4 la' re'' sib'2 do''4 |
la'2 re''4 si'!2 si'4 |
r si' si' do''4. re''8 mi''4 |
la'4 la' la' re'' la' re'' |
si'2 mi''4 do'' re'' si' |
do''2 do''4 mi'' do''2 |
si' si'4 si' mi''2 |
do'' do''4 do'' fa''2 |
mi''2. mi''4 re'' do'' |
si'2. si'4 dod''2 |
re''2 re''4 la' si'2 |
do'' do''4 do'' mi''2 |
la'2. la'4 re'' si' |
do''2.


\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Celænus
    \livretVerse#12 { Vous m’ostez Sangaride ? inhumaine Cybelle ; }
    \livretVerse#6 { Est-ce le prix du zele }
    \livretVerse#12 { Que j’ay fait avec soin éclater à vos yeux ? }
    \livretVerse#12 { Preparez-vous ainsi la douceur eternelle }
    \livretVerse#8 { Dont vous devez combler ces lieux ? }
    \livretVerse#12 { Est-ce ainsi que les Roys sont protegez des Dieux ? }
    \livretVerse#6 { Divinité cruelle, }
    \livretVerse#8 { Descendez-vous exprés des Cieux }
    \livretVerse#8 { Pour troubler un amour fidelle ? }
    \livretVerse#12 { Et pour venir m’oster ce que j’aime le mieux ? }
    \livretPers Cybele
    \livretVerse#12 { J’aimois Atys, l’Amour a fait mon injustice ; }
    \livretVerse#8 { Il a pris soin de mon suplice ; }
    \livretVerse#8 { Et si vous estes outragé, }
    \livretVerse#8 { Bien-tost vous serez top vangé. }
    \livretVerse#8 { Atys adore Sangaride. }
    \livretPers Celænus
    \livretVerse#8 { Atys l’adore ? ah le perfide ! }
    \livretPers Cybele
    \livretVerse#12 { L’Ingrat vous trahissoit, et vouloit me trahir : }
    \livretVerse#12 { Il s’est trompé luy mesme en croyant m’ébloüir. }
    \livretVerse#12 { Les Zephirs l’ont laissé, seul, avec ce qu’il aime, }
  }
  \column {
    \livretVerse#6 { Dans ces aimables lieux ; }
    \livretVerse#8 { Je m’y suis cachée à leurs yeux ; }
    \livretVerse#12 { J’y viens d’estre témoin de leur amour extresme. }
    \livretPers Celænus
    \livretVerse#12 { O Ciel ! Atys plairoit aux yeux qui m’ont charmé ? }
    \livretPers Cybele
    \livretVerse#12 { Eh pouvez-vous douter qu’Atys ne soit aimé ? }
    \livretVerse#12 { Non, non, jamais amour n’eût tant de violence, }
    \livretVerse#12 { Ils ont juré cent fois de s’aimer malgré nous, }
    \livretVerse#8 { Et de braver nostre vengeance ; }
    \livretVerse#12 { Ils nous ont appelez cruels, tyrans, jaloux ; }
    \livretVerse#8 { Enfin leurs cœurs d’intelligence, }
    \livretVerse#12 { Tous deux… ah je frémis au moment que j’y pense ! }
    \livretVerse#12 { Tous deux s’abandonnoient à des transports si doux, }
    \livretVerse#12 { Que je n’ay pû garder plus long-temps le silence, }
    \livretVerse#12 { Ny retenir l’éclat de mon juste couroux. }
    \livretPers Celænus
    \livretVerse#12 { La mort est pour leur crime une peine legere. }
    \livretPers Cybele
    \livretVerse#12 { Mon cœur à les punir est assez engagé ; }
    \livretVerse#12 { Je vous l’ay déja dit, croyez-en ma colere, }
    \livretVerse#8 { Bient-tost vous serez trop vangé. }
  }
}#}))

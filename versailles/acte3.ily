\newBookPart #'()
\act "Acte Troisième"
\sceneDescription\markup\wordwrap-center {
  [Le théâtre change et représente le palais du sacrificateur de Cybèle.]
}
\scene "Scène Premiere" "SCÈNE 1 : Atys"
\sceneDescription\markup { \smallCaps Atys seul. }
%% 3-1
\pieceToc\markup\wordwrap {
  Air : \italic { Que servent les faveurs que nous fait la fortune }
}
\includeScore "DAArecitQueServentLesFaveurs"

\scene "Scène II" "SCÈNE 2 : Idas, doris, Atys"
\sceneDescription\markup\smallCaps { Atys, Doris, Idas. }
%% 3-2
\pieceToc\markup {
  Récit : \italic { Peut-on ici parler sans feindre }
}
\includeScore "DBArecitPeutOnIciParlerSansFeindre"

\scene "Scène III" "SCÈNE 3 : Atys"
\sceneDescription\markup { \smallCaps Atys seul. }
%% 3-3
\pieceToc\markup\wordwrap {
  Récit : \italic { Nous pouvons nous flater de l'espoir le plus doux }
}
\includeScore "DCArecitNousPouvonsNousFlatter"
\newBookPart#'(full-rehearsal haute-contre haute-contre-sol taille quinte)

\scene "Scène IV" \markup\wordwrap {
  SCÈNE 4 : Atys, le Sommeil, Morphée, Phobétor, Phantase, les songes
  agréables, les songes funestes.
}
\sceneDescription\markup\column {
  \wordwrap-center\smallCaps { Le Sommeil. }
  \justify {
    [Le théâtre change et représente un antre entouré de pavots et de
    ruisseaux, où le dieu du sommeil se vient rendre accompagné des
    songes agréables et funestes.]
  }
  \null
  \wordwrap-center {
    \smallCaps [Atys dormant.
    \smallCaps { Le Sommeil, Morphée, Phobétor, Phantase, }
    les songes heureux. Les songes funestes.]
  }
  \null
  \smaller\italic\justify {
    [Deux songes jouants de la viole.
    Deux songes jouants du théorbe.
    Six songes jouants de la flûte.
    Douze songes funestes chantants.
    Huit songes agréables dansants.
    Huit songes funestes dansants.]
  }
}
%% 3-4
\pieceToc\markup\wordwrap {
  Prélude, air : \italic { Dormons, dormons tous }
}
\includeScore "DDApreludeSommeil"
\includeScore "DDAdormons"

\sceneDescription\markup\justify {
  [Les songes agréables approchent d’Atys, et par leurs chants, et par
  leurs danses, lui font connaître l’amour de Cybèle, et le bonheur
  qu’il en doit espérer.]
}
%% 3-5
\pieceToc\markup\wordwrap {
  Air : \italic { Écoute, écoute Atys la gloire qui t’appelle }
}
\includeScore "DDBecouteAtys"
\newBookPart#'(full-rehearsal)
%% 3-6
\pieceToc "Entrée des songes agréables"
\includeScore "DDCentreeSongesAgreables"
\newBookPart#'(full-rehearsal)
%% 3-7
\pieceToc\markup\wordwrap {
  Air : \italic { Goûte en paix chaque jour une douceur nouvelle }
}
\includeScore "DDDgouteEnPaix"
%% 3-8
\pieceToc "Les songes agréables"
\includeScore "DDEairSongesAgreables"
\sceneDescription\markup\justify {
  [Les songes funestes approchent d’Atys, et le menacent de la
  vengeance de Cybèle s’il méprise son amour, et s’il ne l’aime pas
  avec fidélité.]
}
%% 3-9
\pieceToc\markup\wordwrap {
  Air : \italic { Garde-toi d’offenser un amour glorieux }
}
\includeScore "DDFgardeToiDoffenser"
%% 3-10
\pieceToc "Entrée des songes funestes"
\includeScore "DDGentreeSongesFunestes"
%% 3-11
\pieceToc\markup\wordwrap { Chœur : \italic { L’amour qu’on outrage } }
\includeScore "DDHchoeurLamourQuonOutrage"
%% 3-12
\pieceToc "[Deuxième entrée des songes funestes]"
\includeScore "DDIsongesFunestes"
\sceneDescriptionBottom\markup\justify {
  [Atys épouvanté par les songes funestes, se réveille en sursaut,
  le Sommeil et les songes disparaissent avec l’antre où ils étaient,
  et Atys se retrouve dans le même palais où il s’étais endormi.]
}
\newBookPart#'(full-rehearsal dessus)

\scene "Scène V" "SCÈNE 5 : Atys, Cybèle, Mélisse"
\sceneDescription\markup\smallCaps { Cybèle, Atys, [et Mélisse.] }
%% 3-13
\pieceToc\markup\wordwrap {
  Récit : \italic { Venez à mon secours ô dieux ! ô justes dieux ! }
}
\includeScore "DEArecitVenezAMonSecours"
\newBookPart#'(full-rehearsal)

\scene "Scène VI" "SCÈNE 6 : Atys, Sangaride, Cybèle, Mélisse"
\sceneDescription\markup\smallCaps { Cybèle, Atys, Sangaride, [et Mélisse.] }
%% 3-14
\pieceToc\markup\wordwrap {
  Récit : \italic { J’ai recours à votre puissance }
}
\includeScore "DFArecitJaiRecoursAVotrePuissance"
\newBookPart#'(full-rehearsal)

\scene "Scène VII" "SCÈNE 7 : Cybèle, Mélisse"
\sceneDescription\markup\smallCaps { Cybèle, Mélisse. }
%% 3-15
\pieceToc\markup\wordwrap {
  Récit, air : \italic { Qu’Atys dans ses respects mêle d’indifférence }
}
\includeScore "DGArecitQuAtysDansSesRespects"

\scene "Scène VIII" "SCÈNE 8 : Cybèle"
\sceneDescription\markup { \smallCaps Cybèle [seule.] }
%% 3-16
\pieceToc\markup\wordwrap {
  Air : \italic { Espoir si cher, et si doux }
}
\includeScore "DHAespoirSiCherEtSiDoux"
\actEnd "FIN DU TROISIÈME ACTE"

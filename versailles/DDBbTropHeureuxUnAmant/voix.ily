\phantaseClef r4 sib do' |
re' la sib |
fad2. |
la4 sib4. do'8 |
sib4  sol mib' |
do' re' mib' |
re' do' sib |
fa'2 re'4 |
r sib do' |
re' la sib |
fad2. |
re'4 mib'4. fa'8 |
mib'4. re'8( do'4) |
la4 la sib |
sib( la4.) sol8 |
%sol2. |

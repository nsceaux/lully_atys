\clef "basse" sol2 la4 |
sib fad sol |
re2 mi4 |
fad2. |
sol |
la |
sib4 la sol |
fa2 sib4 |
sol2 la4 |
sib fad sol |
re2 do4 |
si,2. |
do |
re2 sol,4 |
re,2. |
%sol, |

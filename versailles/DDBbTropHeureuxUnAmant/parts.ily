\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Phantase
    \livretVerse#6 { Trop heureux un Amant }
    \livretVerse#4 { Qu’amour exemte }
    \livretVerse#8 { Des peines d’une longue attente ! }
  }
  \column {
    \null
    \livretVerse#6 { Trop heureux un Amant }
    \livretVerse#4 { Qu’amour exemte }
    \livretVerse#6 { De crainte, et de tourment ! }
  }
}#}))

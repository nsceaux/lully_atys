\score {
  <<
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \dchantInstr } \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \includeLyrics "paroles"
      \new Staff \with { \dchantInstr } \withLyrics <<
        \global \keepWithTag #'vbas-dessus \includeNotes "voix"
      >> \includeLyrics "paroles"
      \new Staff \with { \hcchantInstr } \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \keepWithTag #'basse \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout { }
  \midi { }
}

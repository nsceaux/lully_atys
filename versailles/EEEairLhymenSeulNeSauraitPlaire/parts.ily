\piecePartSpecs
#`((basse-continue #:clef "alto"
                   #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers "Dieux de Fleuves, Divinitez de Fontaines, & de Ruisseaux"
    \livretVerse#7 { L’Hymen seul ne sçauroit plaire, }
    \livretVerse#7 { Il a beau flatter nos vœux ; }
    \livretVerse#7 { L’Amour seul a droit de faire }
    \livretVerse#7 { Les plus doux de tous les nœuds. }
  }
  \column {
    \null
    \livretVerse#7 { Il est fier, il est rebelle, }
    \livretVerse#7 { Mais il charme tel qu’il est ; }
    \livretVerse#7 { L’Hymen vient quand on l’appelle, }
    \livretVerse#7 { L’Amour vient quand il luy plaist. }
  }
}#}))

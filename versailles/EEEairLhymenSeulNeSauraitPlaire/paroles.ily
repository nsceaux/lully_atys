L’Hy -- men seul ne sçau -- roit plai -- re,
Il a beau flat -- ter nos vœux ;
L’A -- mour seul a droit de fai -- re
Les plus doux de tous les nœuds.

Il est fier, il est re -- bel -- le,
Mais il char -- me tel qu’il est ;
L’Hy -- men vient quand on l’ap -- pel -- le,
L’A -- mour vient quand il luy plaist.

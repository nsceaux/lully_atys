\clef "quinte" sol4 sol si la |
sol si la re' |
do'2 re' |
sol4 re' %{ do'4. %} si4. la8 |
sol2 la4. la8 |
re'4 re' la4. la8 |
la1 |
la |
re'4 re' do' do' |
si2 si4. si8 |
si2 la4. la8 |
la4 fa fa sol |
mi mi'8. mi'16 mi'4. mi'8 |
re'4 re' mi'8 la la la |
sol1 |
sol1 |

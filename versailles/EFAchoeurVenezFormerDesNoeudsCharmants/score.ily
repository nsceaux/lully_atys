\score {
  <<
    \new ChoirStaff \with { \forceGroupBracket \haraKiriFirst } <<
      \new Staff \with { \atysInstr } \withLyrics <<
        \global \keepWithTag #'atys \includeNotes "voix"
      >> \keepWithTag #'atys \includeLyrics "paroles"
      \new Staff \with { \celaenusInstr } \withLyrics <<
        \global \keepWithTag #'celaenus \includeNotes "voix"
      >> \keepWithTag #'celaenus \includeLyrics "paroles"
      \new Staff \with { \sangarInstr } \withLyrics <<
        \global \keepWithTag #'sangar \includeNotes "voix"
      >> \keepWithTag #'sangar \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket \haraKiriFirst } <<
      \new Staff \with { \dchantInstr } \withLyrics <<
        <>^\markup\raise#1 { Divinitez de fontaines et dieux de ruisseaux }
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \with { \hcchantInstr } \withLyrics <<
        <>^\markup\raise#1 { Un dieu de fleuves }
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \with { \tchantInstr } \withLyrics <<
        <>^\markup\raise#1 { Un dieu de fleuves }
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket \haraKiriFirst } <<
      \new Staff \with { \hcchantInstr } \withLyrics <<
        <>^\markup\raise#1 { Grands dieux de fleuves }
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \with { \tchantInstr } \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \with { \btchantInstr } \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
        \modVersion {
          %{ Chœur %} s1*12
          %{ Atys %} s1*2 s2.*2 s1*2\noBreak s1 s2. s1*2 s2. s1*2 s2.*2 s1
          %{ Chœur %} s1*7\break
          %{ Atys %} s1*7 s2.\break
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}

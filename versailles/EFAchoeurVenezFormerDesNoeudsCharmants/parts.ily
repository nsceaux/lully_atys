\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers\line { Chœur de Dieux de Fleuves, & de Fontaines. }
    \livretVerse#8 { Venez former des nœuds charmants, }
    \livretVerse#12 { Atys, venez unir ces bien-heureux Amants. }
    \livretPers Atys
    \livretVerse#8 { Cét Hymen desplaist à Cybele, }
    \livretVerse#8 { Elle deffend de l’achever : }
    \livretVerse#12 { Sangaride est un bien qu’il faut luy reserver, }
    \livretVerse#8 { Et que je demande pour elle. }
    \livretPers Chœur
    \livretVerse#6 { Ah quelle loy cruelle ! }
    \livretPers Celænus
    \livretVerse#12 { Atys peut s’engager luy-mesme à me trahir ? }
    \livretVerse#8 { Atys contre moy s’interesse ? }
    \livretPers Atys
    \livretVerse#8 { Seigneur, je suis à la Déesse, }
  }
  \column {
    \null
    \livretVerse#12 { Dés qu’elle a commandé, je ne puis qu’obeïr. }
    \livretPers\line { Le Dieu du Fleuve Sangar }
    \livretVerse#8 { Pourquoy faut-il qu’elle separe }
    \livretVerse#12 { Deux illustres Amants pour qui l’Hymen prepare }
    \livretVerse#6 { Ses liens les plus doux ? }
    \livretPers Chœur
    \livretVerse#4 { Opposons-nous }
    \livretVerse#6 { A ce dessein barbare. }
    \livretPersDidas Atys "élevé sur un nuage"
    \livretVerse#7 { Aprenez, audacieux, }
    \livretVerse#7 { Qu’il n’est rien qui n’obeïsse }
    \livretVerse#12 { Aux souveraines loix de la Reyne des Dieux. }
    \livretVerse#8 { Qu’on nous enleve de ces lieux ; }
    \livretVerse#12 { Zephirs, que sans tarder mon ordre s’accomplisse. }
  }
}#}))

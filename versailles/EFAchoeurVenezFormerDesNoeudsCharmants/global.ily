\key do \major
\time 2/2 \midiTempo #160 s1*11
\time 4/4 \midiTempo #80 s1*3
\digitTime\time 3/4 s2.*2
\time 2/2 \midiTempo #160 s1
\time 4/4 \midiTempo #80 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\digitTime\time 3/4 s2.
\time 2/2 \midiTempo #160 s1
\time 4/4 \midiTempo #80 s1
\digitTime\time 3/4 s2.*2
\time 4/4 s1*7
\time 2/2 \midiTempo #160 s1
\time 4/4 \midiTempo #80 s1*7
\digitTime\time 3/4 \midiTempo #160 s2.\bar "||" s2.*6 \bar "|."

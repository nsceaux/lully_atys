<<
  %% Atys
  \tag #'(atys basse) {
    << \tag #'basse { s1*11 s4 \atysMark } \tag #'atys { \atysClef R1*11 | r4 } >>
    r8 mi'16 sol' do'8. do'16 sol8 sol16 do' |
    la8 la r16 do' do' do' dod'8. dod'16 dod'8. re'16 |
    re'4 r8 fa'16 fa' la4 la8 si |
    do' mi' do'4 do'8 do'16 si |
    si4 re'16 re' re' mi' la8 la16 si |
    sol2 sol |
    << { s1*2 s2. s1 s4. } \tag #'atys { R1*2 | R2. | R1 | r4 r8 } >>
    \tag #'basse \atysMark re'8 re'8. si16 mi' mi' mi' si |
    do'8 do' r do'16 do' do'8 re'16 mi' |
    fa'4 re'8. do'16 si4 si8. do'16 |
    la4
    << { s2. s2.*2 s1*8 } \tag #'atys { r4 r2 | R2.*2 | R1*8 | } >>
    \tag #'basse \atysMark r4 r8 sol16
    -\tag #'atys ^\markup\italic "élevé sur un nuage"
    sol do'8. do'16 do'8. re'16 |
    mi'4 r8 mi'16 sol' do'8. do'16 do'8. do'16 |
    la4 la do'8 do'16 do' re'8. mi'16 |
    fa'4 fa'8 fa' mi'4 mi'8 fad' |
    sol'4 si8 si16 si mi'8. mi'16 mi'8. mi'16 |
    do'4 r8 fa' re'8. sol'16 sol'8. sol'16 |
    do'4 r8 fa' re'8. re'16 mi'8. fa'16 |
    mi'2 mi'4 |
    \tag #'atys { R2.*6 }
  }
  %% Celænus
  \tag #'(celaenus basse) {
    << { s1*14 s2.*2 s1*2 s4. }
      \tag #'celaenus { \celaenusClef R1*14 | R2.*2 | R1*2 | r4 r8 } >>
    \tag #'basse \celaenusMark do' la8. la16 la8 do' |
    fa fa fa fa fa mi |
    mi4 r8 mi' do' do'16 mi' la8 si16 do' |
    si8 si \tag #'celaenus { r4 r2 | R2. | R1*2 | R2.*2 | R1*15 | R2.*7 }
  }
  %% Sangar
  \tag #'(sangar basse) {
    << { s1*14 s2.*2 s1*3 s2. s1*2 s2. s1 s4 }
      \tag #'sangar { \sangarMark R1*14 | R2.*2 | R1*3 | R2. | R1*2 | R2. | R1 | r4 } >>
    \tag #'basse \sangarMark
    r16 la la la re'4 fad8 fad16 la |
    re4 re8 la16 la sib8 sib16 sib |
    sol8 sol sol sol sol la |
    fad4 fad8 re16 re sol4 sol8 fad |
    sol2
    \tag #'sangar { r2 | R1*13 | R2.*7 }
  }
  %% Chœur (dessus)
  \tag #'(vdessus basse) {
    \clef "vbas-dessus" r2 r4 mi'' |
    re''4. re''8 mi''4. mi''8 |
    re''4 mi'' do''( si'8) do'' |
    si'2. mi''4 |
    do''2. do''4 |
    re''4. re''8 si'4. si'8 |
    do''4. do''8 si'4. mi''8 |
    dod''2. mi''4 |
    fa''2. fa''4 |
    re''4. re''8 mi''4. mi''8 |
    fa''4. mi''8 re''4. do''8 |
    do''4
    << { s2. s1*2 s2.*2 s1 } \tag #'vdessus { r4 r2 | R1*2 | R2.*2 | R1 | } >>
    \tag #'basse \choeurMark mi''2 mi''8. mi''16 mi''8. mi''16 |
    fa''4 << \tag #'vdessus fa''4 \tag #'basse { fa''8 s } >>
    << { s2 s2. s1*2 s2. s1*2 s2.*2 s1 s2 }
      \tag #'vdessus { r2 | R2. | R1*2 | R2. | R1*2 | R2.*2 | R1 | r2 } >>
    \tag #'basse \choeurMark r8 re'' re'' re'' |
    mi''4 r r8 mi'' mi'' mi'' |
    do'' do'' do'' re'' si'4. mi''8 |
    dod''4 dod'' r8 dod'' dod'' dod'' |
    re''4 r r8 re'' re'' do''8 |
    si' si' si' do'' la'4. re''8 |
    si'2 si' |
    << { s1*7 s2. } \tag #'vdessus { R1*7 | R2. | } >>
    \tag #'vdessus <>^\markup\italic\line {
      Les Zephirs volents, & enlevent Atys & Sangaride.
    }
    \tag #'basse \choeurMark mi''4 mi'' mi'' | \tag #'vdessus \noBreak
    do''2 do''4 |
    fa'' fa'' fa'' |
    re''2 re''4 |
    re'' re'' sol'' |
    mi''2 mi''4 |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" r2 r4 sol' |
    sol'4. sol'8 sol'4. sol'8 |
    sol'4 sol' sol' fad' |
    sol'2. sol'4 |
    fa'2. fa'4 |
    fa'4. fa'8 mi'4. mi'8 |
    mi'4 fa' mi'4. mi'8 |
    mi'2. la'4 |
    la'2. la'4 |
    sol'4. sol'8 sol'4. sol'8 |
    fa'4 sol' sol' re' |
    mi' r r2 |
    R1*2 R2.*2 R1 |
    sol'2 sol'8. sol'16 sol'8. sol'16 |
    la'4 la' r2 |
    R2. R1*2 R2. R1*2 R2.*2 R1 |
    r2 r8 sol' sol' sol' |
    sol'4 r r8 sol' sol' sol' |
    fa' fa' fa' fa' mi'4. mi'8 |
    mi'4 mi' r8 la' la' la' |
    fad'4 r r8 fad' fad' fad' |
    sol' sol' sol' sol' sol'4. fad'8 |
    sol'2 sol' |
    R1*7 R2. |
    sol'4 sol' sol' |
    fa'2 fa'4 |
    la' la' la' |
    sol'2 sol'4 |
    sol' sol' sol' |
    sol'2 sol'4 |
  }
  \tag #'vtaille {
    \clef "vtaille" r2 r4 do' |
    si4. si8 do'4. do'8 |
    re'4 do' do'4. do'8 |
    re'2. do'4 |
    la2. la4 |
    si4. si8 sold4. sold8 |
    la4. la8 la4 sold |
    la2. dod'4 |
    re'2. re'4 |
    si4. si8 do'4. do'8 |
    si4 do' do' si |
    do' r r2 |
    R1*2 R2.*2 R1 |
    do'2 do'8. do'16 do'8. do'16 |
    do'4 do' r2 |
    R2. R1*2 R2. R1*2 R2.*2 R1 |
    r2 r8 si si si |
    do'4 r r8 do' do' do' |
    la la la la sold4. sold8 |
    la4 la r8 mi' mi' mi' |
    re'4 r r8 la la la |
    sol mi' mi' mi' re'4. re'8 |
    re'2 re' |
    R1*7 R2. |
    do'4 do' do' |
    la2 la4 |
    re' re' re' |
    si2 si4 |
    si si si |
    do'2 do'4 |
  }
  \tag #'vbasse {
    \clef "basse" r2 r4 do |
    sol4. sol8 do'4. si8 |
    si4 do' la4. sol8 |
    sol2. mi4 |
    fa2. fa4 |
    re4. re8 mi4. mi8 |
    do4 re mi4. mi8 |
    la,2. la4 |
    re2. re4 |
    sol4. fa8 mi4. mi8 |
    re4 do sol,4. sol,8 |
    do4 r r2 |
    R1*2 R2.*2 R1 |
    do'2 do8. do16 do8. do16 |
    fa4 fa r2 |
    R2. R1*2 R2. R1*2 R2.*2 R1 |
    r2 r8 sol sol sol |
    do'4 r r8 do do do |
    fa fa fa re mi4. mi8 |
    la,4 la, r8 la la la |
    re'4 r r8 re re re |
    mi mi mi do re4. re8 |
    sol,2 sol, |
    R1*7 R2. |
    do4 do do |
    fa2 fa4 |
    re re re |
    sol2 sol4 |
    sol sol sol |
    do'2 do'4 |
  }
>>
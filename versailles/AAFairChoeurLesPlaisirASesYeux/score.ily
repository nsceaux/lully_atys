\score {
  <<
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \floreInstr } \withLyrics <<
        \global \keepWithTag #'flore \includeNotes "voix"
      >> \keepWithTag #'flore \includeLyrics "paroles"
      \new Staff \with { \tempsInstr } \withLyrics <<
        \global \keepWithTag #'temps \includeNotes "voix"
      >> \keepWithTag #'temps \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \keepWithTag #'basse-continue \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout { }
  \midi { }
}
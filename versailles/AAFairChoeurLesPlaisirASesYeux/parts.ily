\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \column {
  \livretPers Le Temps & Flore
  \livretVerse#12 { Les Plaisirs à ses yeux ont beau se presenter, }
  \livretVerse#12 { Si-tost qu’il voit Bellone, il quitte tout pour elle ; }
  \livretVerse#6 { Rien ne peut l’arrester }
  \livretVerse#6 { Quand la Gloire l’appelle. }
} #}))

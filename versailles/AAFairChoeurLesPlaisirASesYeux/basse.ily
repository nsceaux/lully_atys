\clef "basse" sol,4 sol la |
sib fad2 |
sol2 sol4 |
fa mib2 |
re re4 |
sol4. fa8 mib re |
do2 do4 |
fa4. mib8 re4 |
mib fa4 fa, |
sib,4. do8 sib, la, |
sib,4 sib sol |
lab lab fa |
sol2 sol8 la |
si8 la si do' re' si |
do'4 do' fa |
sol2. |
do4 do'4. sib8 |
la2 sib4 |
fa2 fa4 |
sol8 fa sol la sib sol |
la2 sib4 |
sib2 la4 |
sib2. |
fad2 sol4 |
re2 r4 |
r sol4. sol8 |
la sol la sib do' la |
sib la sib do' re' sib |
do' sib do' re' mib' do' |
re'4 re' sol |
re2. |
sol,4 sol re |
mib2 do4 |
re2 r4 |
sol sol4. sol8 |
la8 sol la sib do' la |
sib la sib do' re' sib |
do' sib do' re' mib' do' |
re'4 re' sol |
re2 re,4 |
sol,4

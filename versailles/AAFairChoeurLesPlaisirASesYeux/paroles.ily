Les Plai -- sirs à ses yeux ont beau se pre -- sen -- ter,
Si -- tost qu'il voit Bel -- lone, il quit -- te tout pour el -- le ;
-le.
\ru#3 {
  Rien ne peut l'ar -- res -- ter
  Quand la gloi -- re l'ap -- pel -- le,
}
Rien ne peut l'ar -- res -- ter
Quand la gloi -- re l'ap -- pel -- le.

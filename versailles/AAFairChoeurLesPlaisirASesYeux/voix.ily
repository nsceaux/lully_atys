<<
  \tag #'(flore basse) {
    \floreClef r4 sib' do'' |
    re'' do'' sib'8[ la'] |
    sib'4 sib' sib' |
    la' sol'8[ fa'] sol'4 |
    fad'2 r8 re'' |
    si'4. si'8 do''8. re''16 |
    mib''2 do''4 |
    la'4. la'8 sib'4~ |
    sib'8 do'' re''4( do'') |
    sib'2. |
    sib'4 re'' mib'' |
    do'' do''4. re''8 |
    si'2 si'8 do'' |
    re''[\melisma do'' re'' mib'' fa'' re''] |
    mib''4\melismaEnd mib''4. re''8 |
    mib''4( re''2)\trill |
    do''4 mib''4. mib''8 |
    mib''2 mib''8 re'' |
    do''2 la'8 la' |
    sib'[\melisma la' sib' do'' re'' mib''] |
    fa''4\melismaEnd fa''4. fa''8 |
    mib''2. |
    re''4 re''4. re''8 |
    do''4 do''4. sib'8 |
    la'2. |
    r4 sib'4. sib'8 |
    do''8[\melisma sib' do'' re'' mib'' do''] |
    re''[ do'' re'' mib'' fa'' re''] |
    mib''[ fa'' mib'' re'' do'' sib'] |
    la'4\melismaEnd la' sib' |
    sib'4( la'2) |
    sol'4 sib'4. sib'8 |
    sol'4 sol' do'' |
    la'2. |
    r4 sib'4. sib'8 |
    do''[\melisma sib' do'' re'' mib'' do''] |
    re''[ do'' re'' mib'' fa'' re''] |
    mib''[ fa'' mib'' re'' do'' sib'] |
    la'4\melismaEnd la' sib' |
    sib'( la'2) |
    sol'4
  }
  \tag #'temps {
    \tempsClef r4 sol la |
    sib fad4. fad8 |
    sol4 sol sol |
    fa4 mib8[ re] mib4 |
    re2 r8 re |
    sol4. fa8 mib8. re16 |
    do2 do4 |
    fa4. mib8 re4 |
    mib4 fa2 |
    sib,2. |
    sib,4 sib sol |
    lab lab fa |
    sol2 sol8 la |
    si[\melisma la si do' re' si] |
    do'4\melismaEnd do' fa |
    sol2. |
    do4 do'4. sib8 |
    la4 la sib |
    fa2 fa8 fa |
    sol[\melisma fa sol la sib sol] |
    la4\melismaEnd la sib |
    sib2( la4) |
    sib4 sib4. sib8 |
    fad4 fad sol |
    re2. |
    r4 sol4. sol8 |
    la[\melisma sol la sib do' la] |
    sib[ la sib do' re' sib] |
    do'[ sib do' re' mib' do'] |
    re'4\melismaEnd re' sol |
    re2. |
    sol4 sol re |
    mib mib do |
    re2. |
    r4 sol4. sol8 |
    la[\melisma sol la sib do' la] |
    sib[ la sib do' re' sib] |
    do'[ sib do' re' mib' do'] |
    re'4\melismaEnd re' sol |
    re2. |
    sol4
  }
>>


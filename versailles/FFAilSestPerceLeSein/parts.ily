\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix"
                   #:indent 0)
   (basse-viole #:score-template "score-basse-viole-voix"
                #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPersDidas Idas \line { soûtenant Atys. }
    \livretVerse#12 { Il s’est percé le sein, et mes soins pour sa vie }
    \livretVerse#8 { N’ont pû prevenir sa fureur. }
    \livretPers Cybele
    \livretVerse#6 { Ah c’est ma barbarie, }
    \livretVerse#8 { C’est moy, qui luy perce le cœur. }
    \livretPers Atys
    \livretVerse#6 { Je meurs, l’Amour me guide }
    \livretVerse#6 { Dans la nuit du Trépas ; }
    \livretVerse#8 { Je vais où sera Sangaride, }
    \livretVerse#12 { Inhumaine, je vais, où vous ne serez pas. }
    \livretPers Cybele
    \livretVerse#12 { Atys, il est trop vray, ma rigueur est extresme, }
    \livretVerse#8 { Plaignez-vous, je veux tout souffrir. }
    \livretVerse#12 { Pourquoy suis-je immortelle en vous voyant perir ? }
  }
  \column {
    \livretPers\line { Atys, & Cybele }
    \livretVerse#6 { Il est doux de mourir }
    \livretVerse#6 { Avec ce que l’on aime. }
    \livretPers Cybele
    \livretVerse#12 { Que mon amour funeste armé contre moy-mesme, }
    \livretVerse#12 { Ne peut-il vous venger de toutes mes rigueurs. }
    \livretPers Atys
    \livretVerse#12 { Je suis assez vengé, vous m’aimez, et je meurs. }
    \livretPers Cybele
    \livretVerse#8 { Malgré le Destin implacable }
    \livretVerse#12 { Qui rend de ton trépas l’arrest irrevocable, }
    \livretVerse#12 { Atys, sois à jamais l’objet de mes amours : }
    \livretVerse#12 { Reprens un sort nouveau, deviens un Arbre aimable }
    \livretVerse#8 { Que Cybele aimera toûjours. }
  }
}#}))

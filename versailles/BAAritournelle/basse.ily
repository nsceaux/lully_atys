\clef "basse" sol2. sol4 |
fad1 |
sol2 fa4. mib8 |
re2. re4 |
mib8 fa mib re do2 |
fa8 sol fa mib re4 sib, |
mib2 fa4 fa, |
sib, sib8 la sol fa mib re |
do4 mib8 re do sib, la, sol, |
re2 sol, |
re,4 re8 do sib,4. sib,8 |
do2 re4 re, |
sol,2. sol4 |
sol,1 |
\clef "dessus" R1*2 |
r4 r8 sib' sib'4. do''8 |
re''4 re''8 mib'' re'' do'' sib' la' |
sol'2 do''8 re'' do'' sib' |
la'4. re''8 re''4 re''8 do''16 re'' |
mib''8 re'' do'' sib' la'4. sib'8 |
sib'4 re'' re''4. re''8 |
mib''4 do''8 re'' mib'' re'' do'' sib' |
la'4 re''8 do'' sib' la' sib' do'' |
la'4. la'8 sib'4 do''8 re'' |
do'' sib' la' sol' fad'4. sol'8 |
sol'2 r |
sol'1 |

\clef "basse" do2 do'4. do'8 |
si2 do'4 do |
fa2 fad |
sol mi4. mi8 |
la2 re |
la,2. la,4 |
re2 sol, |
do re4 re, |
sol,4. sol8 sol4. la8 |
si2 sol |
do'4. do8 do4. re8 |
mi2 do |
fa2. fa4 |
sol4. sol8 fa4. mi8 |
re4. mi8 fa4 sol |
la4 sol8 fa mi4 fa |
sol do sol,2 |
\footnoteHere#'(0 . 0) \markup\wordwrap {
  F-V : La note finale des basses est omise, ce qui pourrait suggérer un tuilage avec le récit suivant.
}
do1 |

\clef "dessus" do''4. do''8 mi''4. fa''8 |
sol''2. r8 sol'' |
la''4. la''8 re''4. re''8 |
re''2 sol''4. sol''8 |
sol''4. fa''16 mi'' fa''4. sol''8 |
mi''4. mi''8 la''4. sol''8 |
fad''4 re'' sib''2~ |
sib''4. la''8 la''4. sol''8 |
sol''4. si'8 si'4. do''8 |
re''4. mi''8 fa'' mi'' fa'' sol'' |
mi''4. mi''8 mi''4. fa''8 |
sol''4 do'' do'''4 sol'' |
la''8 si'' la'' sol'' fa'' mi'' re'' do'' |
si'4. sol''8 re''4. mi''8 |
fa''4. sol''8 la''4 sol''8 fa'' |
mi''4. fa''8 sol''4 fa''8 mi'' |
re''4 mi''8 fa'' re''4. do''8 |
do''1 |

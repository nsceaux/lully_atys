\key sol \major
\beginMark\markup\line\italic\smaller { Les hautbois et les violons jouent cet air alternativement }
\digitTime\time 3/4 \midiTempo #180 s2.*6 s2.*18 \bar "|."

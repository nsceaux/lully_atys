\clef "basse" sol4 sol re |
sol do2 |
re do4 |
si, fad,2 |
sol,4 do,2 |
re,2. |
re4 re la, |
re sol,2 |
la, la4 |
dod4. dod8 re4 |
sol, la,2 |
re2. |
la,4 la, la, |
la, la,2 |
mi2 mi4 |
do4. si,8 la,4 |
sol, re re, |
sol,2 sol4 |
mi2 mi4 |
fad fad2 |
sol sol,4 |
do4. si,8 la,4 |
sol, re re, |
sol,2. |

\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { Dessus de Hautbois }
    } << \global \includeNotes "dessus" >>
    \new Staff \with {
      instrumentName = \markup\center-column { Haute-contre de Hautbois }
    } << \global \includeNotes "haute-contre" \clef "soprano" >>
    \new Staff \with {
      instrumentName = \markup\center-column { Taille de Hautbois }
    } << \global \includeNotes "taille" \clef "mezzosoprano" >>
    \new Staff \with {
      instrumentName = \markup\center-column { Basse de Cromorne }
    } << \global \includeNotes "basse" >>
  >>
  \layout {
    indent = 30\mm
    system-count = 3
  }
}

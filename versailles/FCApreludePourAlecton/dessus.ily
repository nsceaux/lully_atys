\clef "dessus" fa'16 mi' fa' sol' la' sol' la' fa' do''
si'16 do'' re'' mi'' re'' mi'' do'' |
fa'' sol'' fa'' mi'' re'' mi'' fa'' re'' sol''8. la''16 la''8. sol''16 |
sol''4 r16 fa'' sol'' mi'' la''4. la''8 |
re''16 re'' mi'' fa'' sol'' la'' sol'' fa'' mi'' fa'' sol'' fa'' mi'' re'' do'' sib' |
la'8. la'16 sib'8. sib'16 do''4. do''16 do'' |
re''16 do'' re'' mib'' re'' do'' sib' la' sib' la' sol' la' sib' do'' re'' mi'' |
fa''4. fa''16 fa'' mi'' re'' do'' re'' mi'' fa'' sol'' la'' |
sib''4. sib''8 la''8. sol''16 sol''8. fa''16 |
fa''1 |

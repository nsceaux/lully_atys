\clef "basse" fa2 mi |
re do4 fa, |
do16 si,16 do re mi re mi do fa mi fa sol la sol la fa |
sib4 sol do' do |
fa8 sol16 fa mi re do sib, la,2 |
sib,4 fad, sol, sol |
re16 do re mi fa sol la sib do'4 do |
sol,16 fa, sol, la, sib, do re mi fa8 sib, do do, |
fa,1 |

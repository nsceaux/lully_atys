\header {
  copyrightYear = "2010"
  composer = "Jean-Baptiste Lully"
  poet = "Philippe Quinault"
  date = "1676"
  editions = "Version : manuscrit de Versailles"
}

%% LilyPond options:
%%  urtext      if true, then print urtext score
%%  part        if a symbol, then print the separate part score
%%  ballard     if true, use Ballard edition source
%%  versailles  if true, use Versailles manuscript source

#(ly:set-option 'original-layout (eqv? (ly:get-option 'urtext) #t))
#(ly:set-option 'use-ancient-clef (eqv? (ly:get-option 'urtext) #t))
#(ly:set-option 'show-ancient-clef (not (symbol? (ly:get-option 'part))))
#(ly:set-option 'print-footnotes #t)
#(ly:set-option 'use-rehearsal-numbers #t)
#(ly:set-option 'apply-vertical-tweaks
                (and (not (eqv? #t (ly:get-option 'urtext)))
                     (not (symbol? (ly:get-option 'part)))))
#(ly:set-option 'print-footnotes
                (not (symbol? (ly:get-option 'part))))
%% Staff size
#(set-global-staff-size
  (cond ((memq (ly:get-option 'part) '(basse-continue basse-tous basse-viole)) 16)
        ((symbol? (ly:get-option 'part)) 18)
        (else 16)))

%% Line/page breaking algorithm
%%  optimal   for lead sheets
%%  page-turn for instruments and vocal parts
\paper {
  #(define page-breaking (if (eqv? (ly:get-option 'part) #f)
                             ly:optimal-breaking
                             ly:page-turn-breaking))
}

\language "italiano"
\include "nenuvar-lib.ily"
\include "nenuvar-garamond.ily"

\opusPartSpecs
#`((flutes-hautbois "Flûtes, Hautbois" ()
                    (#:score "score-vents"))
   (dessus "Dessus de violons" ()
           (#:notes "dessus"))
   (haute-contre "Hautes-contre de violons" ()
                 (;;
                  #:notes "haute-contre"
                  #:clef ,(if (eqv? #t (ly:set-option 'haute-contre-treble)) "treble" "alto")))
   (taille "Tailles de violons" ()
           (#:notes "taille" #:clef "alto"))
   (quinte "Quintes de violons" ()
           (#:notes "quinte" #:clef "alto"))
   (basse "Basses de violons" ()
          (;;
           #:notes "basse"
           #:instrument "BVn"
           #:clef "basse"
           #:tag-notes basse))
   (basse-continue "Basse continue" ((basse "BVn"))
                   (;;
                    #:notes "basse"
                    #:instrument "Bc"
                    #:clef "basse"
                    #:tag-notes basse-continue
                    #:score-template "score-basse-continue"))
   (basse-viole "Basses de viole" ((basse-continue #f) (basse "BVn"))
                (;;
                 #:notes "basse"
                 #:clef "basse"
                 #:instrument "Bc"
                 #:tag-notes basse-continue
                 #:score-template "score-basse-viole"))
   (basse-tous "Basses de violons, Basse continue" ((basse "BVn") (basse-continue "Bc"))
               (;;
                #:score "score-basse"
                #:notes "basse"
                #:clef "basse"
                #:tag-notes basse-continue)))

\opusTitle "Atys"

\header {
  maintainer = \markup {
    Nicolas Sceaux, 
    \with-url #"https://cmbv.fr" CMBV
    \tagline-vspacer
  }
  tagline = \markup\sans\abs-fontsize #8 \override #'(baseline-skip . 0) {
    \right-column\bold {
      \with-url #"https://editions-nicolas-sceaux.fr" {
        \concat { Éditions \tagline-vspacer }
        \concat { Nicolas \tagline-vspacer }
        \concat { Sceaux \tagline-vspacer }
      }
    }
    \abs-fontsize #9 \with-color #(x11-color 'grey40) \raise #-0.7 \musicglyph #"clefs.petrucci.f"
    \column {
      \line { \tagline-vspacer \copyright }
      \line { \tagline-vspacer Supervision scientifique : Thomas Leconte, Nathalie Berton-Blivet et Fabien Guilloux. }
      \line {
        \tagline-vspacer
        Sheet music from
        \with-url #"https://editions-nicolas-sceaux.fr"
        https://editions-nicolas-sceaux.fr
        typeset using \with-url #"http://lilypond.org" LilyPond
        on \concat { \today . }
      }
    }
  }
}

\layout {
  indentconducteur = 15\mm
  indentpart = 10\mm
  indentconducteurinstr = 10\mm
  largeindent = \smallindent
  
  indent = #(if (symbol? (ly:get-option 'part))
                indentpart
                indentconducteur)
  short-indent = #(if (symbol? (ly:get-option 'part))
                      0
                      indentconducteur)
}

%% Répéter les tirets dans les paroles après un saut de ligne
\layout {
  \context {
    \Lyrics
    \override LyricHyphen.after-line-breaking = ##t
    %\override LyricHyphen.minimum-distance = #0.6 % default 0.1
  }
}

%%%
#(define (make-instrument-variables name full-name short-name)
     ;; \instrumentInstr
     (ly:parser-define! (string->symbol (format #f "~(~a~)Instr" name))
       #{
\with {
  instrumentName = $full-name
  shortInstrumentName = $short-name
} #})
     ;; \instrumentInstrSug
     (ly:parser-define! (string->symbol (format #f "~(~a~)InstrSug" name))
       #{
\with {
  instrumentName = $(format #f "[~a]" short-name)
  shortInstrumentName = $(format #f "[~a]" short-name)
} #}))

#(make-instrument-variables "fl" "Fl" "Fl")
#(make-instrument-variables "hb" "Hb" "Hb")
#(make-instrument-variables "dv" "DVn" "DVn")
#(make-instrument-variables "hcv" "HcVn" "HcVn")
#(make-instrument-variables "tv" "TVn" "TVn")
#(make-instrument-variables "qv" "QVn" "QVn")
#(make-instrument-variables "bv" "BVn" "BVn")
#(make-instrument-variables "bc" "Bc" "Bc")
#(make-instrument-variables "bvbc" #{\markup\center-column { BVn Bc }#} #{\markup\center-column { BVn Bc }#})
#(make-instrument-variables "flbc" #{\markup\center-column { [BFl] Bc }#} #{\markup\center-column { [BFl] Bc }#})
% Chœur
#(make-instrument-variables "dchant" "D" "D")
#(make-instrument-variables "hcchant" "HC" "HC")
#(make-instrument-variables "tchant" "T" "T")
#(make-instrument-variables "htchant" "HT" "HT")
#(make-instrument-variables "btchant" "BT" "BT")
#(make-instrument-variables "bchant" "B" "B")

#(define (make-character-variables name full-name short-name clef)
     ;; \characterInstr
     (ly:parser-define! (string->symbol (format #f "~aInstr" name))
       #{
\with {
  instrumentName = \markup $full-name
  shortInstrumentName = \markup $full-name
} #})
     ;; \characterClef
     (ly:parser-define! (string->symbol (format #f "~aClef" name))
       (define-music-function () ()
       #{ \ffclef $clef #}))
     ;; \characterName
     (ly:parser-define! (string->symbol (format #f "~aName" name))
       (define-music-function () ()
       #{ <>^\markup\character $full-name #}))
     ;; \characterMark
     (ly:parser-define! (string->symbol (format #f "~aMark" name))
       (define-music-function () ()
       #{
  \ffclef $clef <>^\markup\character $full-name
  \unless #(symbol? (*part*)) \set Staff.shortInstrumentName = \markup $full-name
          #}))
     ;; \characterMarkText
     (ly:parser-define! (string->symbol (format #f "~aMarkText" name))
       (define-music-function (text) (string?)
         #{
  \ffclef $clef <>^\markup\character-text $full-name $text
  \unless #(symbol? (*part*)) \set Staff.shortInstrumentName = \markup $full-name #}))
     ;; \characterMarkShort
     (ly:parser-define! (string->symbol (format #f "~aMarkShort" name))
       (define-music-function () ()
       #{
  \ffclef $clef <>^\markup\character $short-name
  \unless #(symbol? (*part*)) \set Staff.shortInstrumentName = \markup $full-name
          #}))
     ;; \characterMarkTextShort
     (ly:parser-define! (string->symbol (format #f "~aMarkTextShort" name))
       (define-music-function (text) (string?)
         #{
  \ffclef $clef <>^\markup\character-text $short-name $text
  \unless #(symbol? (*part*)) \set Staff.shortInstrumentName = \markup $full-name #})))
%{
%% Définition des personnages
%}
#(make-character-variables "temps" "Le Temps" "Le Temps" "vbasse-taille")
#(make-character-variables "flore" "Flore" "Flore" "vbas-dessus")
#(make-character-variables "zephir" "Un Zephir" "Un Zephir" "vhaute-contre")
#(make-character-variables "melpomene" "Melpomene" "Melpomene" "vbas-dessus")
#(make-character-variables "iris" "Iris" "Iris" "vbas-dessus")

#(make-character-variables "atys" "Atys" "Atys" "vhaute-contre")
#(make-character-variables "idas" "Idas" "Idas" "vbasse")
#(make-character-variables "sangaride" "Sangaride" "Sanga." "vbas-dessus")
#(make-character-variables "doris" "Doris" "Doris" "vbas-dessus")
#(make-character-variables "cybele" "Cybele" "Cyb" "vbas-dessus")
#(make-character-variables "celaenus" "Celænus" "Celænus" "vbasse-taille")
#(make-character-variables "melisse" "Melisse" "Melisse" "vbas-dessus")
#(make-character-variables "sommeil" "Le Sommeil" "Le Sommeil" "vhaute-contre")
#(make-character-variables "morphee" "Morphée" "Morphée" "vhaute-contre")
#(make-character-variables "phantase" "Phantase" "Phantase" "vtaille")
#(make-character-variables "phobetor" "Phobetor" "Phobetor" "vbasse")
#(make-character-variables "songeFuneste" "Un Songe funeste" "Un Songe funeste" "vbasse")
#(make-character-variables "sangar" "Sangar" "Sangar" "vbasse")

#(make-character-variables "choeur" "Chœur" "Chœur" "vdessus")

%%% Footnotes
\layout {
  \context {
    \Voice
    \override Script.avoid-slur = #'outside
    %% no line from footnotes to grobs
    \override FootnoteItem.annotation-line = ##f

    \consists Balloon_engraver
    \override BalloonText.annotation-balloon = ##f
  }
  \context {
    \CueVoice
    \override Script.avoid-slur = #'outside
    %% no line from footnotes to grobs
    \override FootnoteItem.annotation-line = ##f
  }
}

footnoteHereFull =
#(define-music-function (parser this-location offset note)
     (number-pair? markup?)
   (if (not (symbol? (ly:get-option 'part)))
       (let ((foot-mus (make-music
                        'FootnoteEvent
                        'X-offset (car offset)
                        'Y-offset (cdr offset)
                        'automatically-numbered #t
                        'text (make-null-markup)
                        'footnote-text note)))
         ;(set! location #f)
         #{ <>-\tweak footnote-music #foot-mus ^\markup\transparent\box "1" #})
       (make-music 'Music 'void #t)))

startBracket =
#(define-music-function (parser location text music) (markup? ly:music?)
   (if (symbol? (*part*))
       music
       #{ \once\override HorizontalBracketText.text = \markup\whiteout\fontsize#-1 $text $music \startGroup #}))

stopBracket = \unless #(symbol? (*part*)) \stopGroup

forceGroupBracket = \override SystemStartBracket.collapse-height = #4

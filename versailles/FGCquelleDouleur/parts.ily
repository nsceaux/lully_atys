\piecePartSpecs
#`((dessus #:score-template "score-voix"
           #:indent 0)
   (haute-contre #:score-template "score-voix"
                 #:indent 0)
   (taille #:score-template "score-voix"
           #:indent 0)
   (quinte #:score-template "score-voix"
           #:indent 0)
   (basse #:score "score-basse")
   (basse-continue #:score-template "score-basse-continue-voix"
                   #:instrument #f
                   #:indent 0)
   (basse-viole #:score-template "score-basse-viole-voix"
                #:indent 0)
   (basse-tous)
   (silence #:on-the-fly-markup , #{ \markup\null #}))

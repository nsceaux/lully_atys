\clef "haute-contre" R1*3 |
r2 la'4 la'8 la' |
sib'4 sib' r2 |
r sib'4 sib'8 sib' |
lab'4 lab' do'' do''8 do'' |
sib'4 sib' r2 |
R1 |
r2 re''4 re''8 re'' |
si'4 si' do''2 |
r si'4 si'8 do'' |
do''2 r2 | \allowPageTurn
R1*9 | \allowPageTurn
r2 la'4 la'8 la' |
sib'4 sib' r2 |
r sib'4 sib'8 sib' |
lab'4 lab' do'' do''8 do'' |
sib'4 sib' r2 |
R1 |
r2 re''4 re''8 re'' |
si'4 si' do''2 |
r si'4 si'8 do'' |
do''1 |

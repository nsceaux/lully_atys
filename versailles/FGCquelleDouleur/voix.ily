<<
  %% Cybele
  \tag #'(cybele basse) {
    <<
      \tag #'basse { s2 \cybeleMark }
      \tag #'cybele { \cybeleClef r2 }
    >> mib''4 mib''8 mib'' |
    si'2 <<
      \tag #'basse { s1 \cybeleMark }
      \tag #'cybele { r2 | r }
    >> sol''4 do''8 do'' |
    la'4 la' <<
      \tag #'basse { s1 \cybeleMark }
      \tag #'cybele { r2 | r }
    >> sib'4 sib'8 sib' |
    sol'2 <<
      \tag #'basse { s1*2 \cybeleMark }
      \tag #'cybele { r2 | R1 | r2 }
    >> re''2~ |
    re'' la'4. sib'8 |
    fad'2 <<
      \tag #'basse { s1*3 \cybeleMark }
      \tag #'cybele { r2 | R1*2 | r2 }
    >> r4 mib'' |
    re''2 re''4. mib''8 |
    do''2 do''4. re''8 |
    si'2 si'4 r8 re'' |
    sol'2 do''4 do''8 do'' |
    la'4 la'8 la' sib'4 do'' |
    re'' re''8 fa'' sib'4 fa'8 sib' |
    sol'4 sol' mib'' mib''8 mib'' |
    si'2 <<
      \tag #'basse { s1 \cybeleMark }
      \tag #'cybele { r2 | r }
    >> sol''4 do''8 do'' |
    la'4 la' <<
      \tag #'basse { s1 \cybeleMark }
      \tag #'cybele { r2 | r }
    >> sib'4 sib'8 sib' |
    sol'2 <<
      \tag #'basse { s1*2 \cybeleMark }
      \tag #'cybele { r2 | R1 | r2 }
    >> re''2~ |
    re'' la'4. sib'8 |
    fad'2
    \tag #'cybele { r2 | R1*3 }
  }
  %% Chœur
  \tag #'(vdessus basse) {
    \clef "vbas-dessus" \tag #'vdessus <>^\markup { Nymphes et divinités des bois } do''4 do'' <<
      \tag #'basse { s2 s \choeurMark }
      \tag #'vdessus { r2 | r }
    >> si'4 si'8 si' |
    do''2 <<
      \tag #'basse { s2 s1*17 s2 \choeurMark }
      \tag #'vdessus {
        r2 |
        R1*7 |
        r2 mib''2 |
        r re''4 re''8 mib'' |
        do''2 r |
        R1*7 |
        r2
      }
    >> si'4 si'8 si' |
    do''2 \tag #'vdessus {
      r2 |
      R1*7 |
      r2 mib''2 |
      r re''4 re''8 mib'' |
      do''1 |
    }
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" mib'4 mib' r2 |
    r sol'4 sol'8 sol' |
    sol'2 r2 |
    R1*7 |
    r2 sol'2 |
    r sol'4 sol'8 sol' |
    mib'2 r2 |
    R1*7 |
    r2 sol'4 sol'8 sol' |
    sol'2 r2 |
    R1*7 |
    r2 sol'2 |
    r sol'4 sol'8 sol' |
    mib'1 |
  }
  \tag #'vtaille {
    \clef "vtaille" sol4 sol r2 |
    r re'4 re'8 re' |
    do'2 r2 |
    R1*7 |
    r2 do'2 |
    r si4 si8 do' |
    do'2 r2 |
    R1*7 |
    r2 re'4 re'8 re' |
    do'2 r2 |
    R1*7 |
    r2 do'2 |
    r si4 si8 do' |
    do'1 |
  }
  \tag #'vbasse {
    \clef "vbasse" do4 do r2 |
    r sol4 sol8 sol |
    mi2 r2 |
    R1*7 |
    r2 do'2 |
    r sol4 sol8 sol |
    do2 r2 |
    R1*7 |
    r2 sol4 sol8 sol |
    mi2 r2 |
    R1*7 |
    r2 do'2 |
    r sol4 sol8 sol |
    do1 |
  }
  %% Chœur corybantes
  \tag #'(vdessus2 basse) {
    <<
      \tag #'basse { s1*3 | s2 \choeurMark }
      \tag #'vdessus2 { \clef "vbas-dessus" <>^\markup { Corybantes } R1*3 | r2 }
    >> do''4 do''8 fa'' |
    re''4 re''
    <<
      \tag #'basse { s1 \choeurMark }
      \tag #'vdessus2 { r2 | r }
    >> mib''4 mib''8 mib'' |
    do''4 do'' fa'' fa''8 fa'' |
    re''4 re''
    <<
      \tag #'basse { s1*2 \choeurMark }
      \tag #'vdessus2 { r2 | R1 | r2 }
    >> la'4 la'8 re'' |
    si'4 si' mib''2 |
    r re''4 re''8 mib'' |
    do''2
    <<
      \tag #'basse { s2 s1*9 s2 \choeurMark }
      \tag #'vdessus2 { r2 | R1*9 | r2 }
    >> do''4 do''8 fa'' |
    re''4 re''
    <<
      \tag #'basse { s1 \choeurMark }
      \tag #'vdessus2 { r2 | r }
    >> mib''4 mib''8 mib'' |
    do''4 do'' fa'' fa''8 fa'' |
    re''4 re''
    <<
      \tag #'basse { s1*2 \choeurMark }
      \tag #'vdessus2 { r2 | R1 | r2 }
    >> la'4 la'8 re'' |
    si'4 si' mib''2 |
    r re''4 re''8 mib'' |
    do''1 |
  }
  \tag #'vhaute-contre2 {
    \clef "vhaute-contre" R1*3 |
    r2 fa'4 fa'8 fa' |
    fa'4 fa' r2 |
    r sol'4 sol'8 sol' |
    mib'4 mib' lab' lab'8 lab' |
    fa'4 fa' r2 |
    R1 |
    r2 fad'4 fad'8 fad' |
    sol'4 sol' sol'2 |
    r sol'4 sol'8 sol' |
    mib'2 r2 |
    R1*9 |
    r2 fa'4 fa'8 fa' |
    fa'4 fa' r2 |
    r sol'4 sol'8 sol' |
    mib'4 mib' lab' lab'8 lab' |
    fa'4 fa' r2 |
    R1 |
    r2 fad'4 fad'8 fad' |
    sol'4 sol' sol'2 |
    r sol'4 sol'8 sol' |
    mib'1 |
  }
  \tag #'vtaille2 {
    \clef "vtaille" R1*3 |
    r2 la4 la8 la |
    sib4 sib r2 |
    r sib4 sib8 sib |
    lab4 lab do' do'8 do' |
    sib4 sib r2 |
    R1 |
    r2 re'4 re'8 re' |
    re'4 re' do'2 |
    r si4 si8 do' |
    do'2 r2 |
    R1*9 |
    r2 la4 la8 la |
    sib4 sib r2 |
    r sib4 sib8 sib |
    lab4 lab do' do'8 do' |
    sib4 sib r2 |
    R1 |
    r2 re'4 re'8 re' |
    re'4 re' do'2 |
    r si4 si8 do' |
    do'1 |
  }
  \tag #'vbasse2 {
    \clef "vbasse" R1*3 |
    r2 fa4 fa8 fa |
    sib4 sib r2 |
    r mib4 mib8 mib |
    lab4 lab fa fa8 fa |
    sib4 sib r2 |
    R1 |
    r2 re'4 re'8 re' |
    sol4 sol do'2 |
    r sol4 sol8 sol |
    do2 r2 |
    R1*9 |
    r2 fa4 fa8 fa |
    sib4 sib r2 |
    r mib4 mib8 mib |
    lab4 lab fa fa8 fa |
    sib4 sib r2 |
    R1 |
    r2 re'4 re'8 re' |
    sol4 sol do'2 |
    r sol4 sol8 sol |
    do1 |
  }
>>
\score {
  <<
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \cybeleInstr } \withLyrics <<
        \global \keepWithTag #'cybele \includeNotes "voix"
      >> \keepWithTag #'cybele \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket  } <<
      \new Staff \with { \dchantInstr } \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \with { \hcchantInstr } \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \with { \tchantInstr } \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \with { \btchantInstr } \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \with { \dchantInstr } \withLyrics <<
        \global \keepWithTag #'vdessus2 \includeNotes "voix"
      >> \keepWithTag #'choeur2 \includeLyrics "paroles"
      \new Staff \with { \hcchantInstr } \withLyrics <<
        \global \keepWithTag #'vhaute-contre2 \includeNotes "voix"
      >> \keepWithTag #'choeur2 \includeLyrics "paroles"
      \new Staff \with { \tchantInstr } \withLyrics <<
        \global \keepWithTag #'vtaille2 \includeNotes "voix"
      >> \keepWithTag #'choeur2 \includeLyrics "paroles"
      \new Staff \with { \btchantInstr } \withLyrics <<
        \global \keepWithTag #'vbasse2 \includeNotes "voix"
      >> \keepWithTag #'choeur2 \includeLyrics "paroles"
    >>
    \new StaffGroup <<
      \new Staff \with { \dvInstr }<<
        \global \includeNotes "dessus"
      >>
      \new Staff \with { \hcvInstr } <<
        \global \includeNotes "haute-contre"
      >>
      \new Staff \with { \tvInstr } <<
        \global \includeNotes "taille"
      >>
      \new Staff \with { \qvInstr } <<
        \global \includeNotes "quinte"
      >>
      \new Staff \with { \bvInstr } <<
        \global \keepWithTag #'basse \includeNotes "basse"
      >>
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \keepWithTag #'basse-continue \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout { }
  \midi { }
}

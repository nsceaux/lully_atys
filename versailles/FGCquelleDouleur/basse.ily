\clef "basse"
<<
  \tag #'(basse-continue tous) {
    do1 |
    sol2. sol4 |
    mi1 |
    fa1 |
    sib2 sib, |
    mib1 |
    lab2 fa |
    sib sib, |
    do1 |
    re2 re' |
    sol do' |
    sol sol, |
    do do'2 |
    si sib |
    la lab |
    sol1 |\allowPageTurn
    mi |
    fa4. mib8 re4 do |
    sib,1 |
    mib2 do |
    sol1 |
    mi1 |
    fa1 |
    sib2 sib, |
    mib1 |
    lab2 fa |
    sib sib, |
    do1 |
    re1 |
    sol2 do' |
    sol sol, |
    do1 | 
  }
  \tag #'basse {
    R1*3 |
    r2 fa4 fa8 fa |
    sib4 sib r2 |
    r mib4 mib8 mib |
    lab4 lab fa fa8 fa |
    sib4 sib r2 |
    R1 |
    r2 re'4 re'8 re' |
    sol4 sol do'2 |
    r sol4 sol8 sol |
    do2 r2 | \allowPageTurn
    R1*9 | \allowPageTurn
    r2 fa4 fa8 fa |
    sib4 sib r2 |
    r mib4 mib8 mib |
    lab4 lab fa fa8 fa |
    sib4 sib r2 |
    R1 |
    r2 re'4 re'8 re' |
    sol4 sol do'2 |
    r sol4 sol8 sol |
    do1 | 
  }
>>

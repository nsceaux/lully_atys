\clef "taille" R1*3 |
r2 fa'4 fa'8 fa' |
fa'4 fa' r2 |
r sol'4 sol'8 sol' |
mib'4 mib' lab' lab'8 lab' |
fa'4 fa' r2 |
R1 |
r2 la'4 la'8 la' |
sol'4 sol' sol'2 |
r sol'4 sol'8 sol' |
mib'2 r2 | \allowPageTurn
R1*9 | \allowPageTurn
r2 fa'4 fa'8 fa' |
fa'4 fa' r2 |
r sol'4 sol'8 sol' |
mib'4 mib' lab' lab'8 lab' |
fa'4 fa' r2 |
R1 |
r2 la'4 la'8 la' |
sol'4 sol' sol'2 |
r sol'4 sol'8 sol' |
mib'1 |

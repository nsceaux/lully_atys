% Choeur
\tag #'(choeur basse) {
  - tu -- re.
}
% Cybele
\tag #'(cybele basse) {
  Quel -- le dou -- leur !
}
% Choeur
\tag #'(choeur basse) {
  Quel -- le dou -- leur !
}
% Cybele
\tag #'(cybele basse) {
  Ah ! quel -- le ra -- ge !
}
% Choeur
\tag #'(choeur2 basse) {
  Ah ! quel -- le ra -- ge !
}
% Cybele
\tag #'(cybele basse) {
  Quel -- le dou -- leur !
}
% Choeur
\tag #'(choeur2 basse) {
  Ah ! quel -- le ra -- ge !
  Ah ! quel -- le ra -- ge !
}
% Cybele
\tag #'(cybele basse) {
  Ah ! __ quel mal -- heur !
}
% Choeur
\tag #'(choeur2 basse) {
  Ah ! quel -- le ra -- ge !
}
\tag #'(choeur choeur2 basse) {
  Ah ! Ah ! quel mal -- heur !
}
% Cybele
\tag #'(cybele basse) {
  A -- tys au prin -- temps de son â -- ge
  Pe -- rit comme u -- ne fleur
  Qu'un sou -- dain o -- ra -- ge
  Ren -- verse et ra -- va -- ge.
  Quel -- le dou -- leur !
}
% Choeur
\tag #'(choeur basse) {
  Quel -- le dou -- leur !
}
% Cybele
\tag #'(cybele basse) {
  Ah ! quel -- le ra -- ge !
}
% Choeur
\tag #'(choeur2 basse) {
  Ah ! quel -- le ra -- ge !
}
% Cybele
\tag #'(cybele basse) {
  Quel -- le dou -- leur !
}
% Choeur
\tag #'(choeur2 basse) {
  Ah ! quel -- le ra -- ge !
  Ah ! quel -- le ra -- ge !
}
% Cybele
\tag #'(cybele basse) {
  Ah ! __ quel mal -- heur !
}
% Choeur
\tag #'(choeur2 basse) {
  Ah ! quel -- le ra -- ge !
}
\tag #'(choeur choeur2 basse) {
  Ah ! Ah ! quel mal -- heur !
}

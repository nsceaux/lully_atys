\clef "dessus" R1*3 |
r2 do''4 do''8 fa'' |
re''4 re'' r2 |
r mib''4 mib''8 mib'' |
do''4 do'' fa'' fa''8 fa''8 |
re''4 re'' r2 |
R1 |
r2 fad''4 fad''8 fad'' |
sol''4 re'' mib''2 |
r re''4 re''8 mib'' |
do''2 r2 |
R1*9 |
r2 do''4 do''8 fa'' |
re''4 re'' r2 |
r mib''4 mib''8 mib'' |
do''4 do'' fa'' fa''8 fa'' |
re''4 re'' r2 |
R1 |
r2 fad''4 fad''8 fad'' |
sol''4 re'' mib''2 |
r re''4 re''8 mib'' |
do''1 |

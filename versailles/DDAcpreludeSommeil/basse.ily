\clef "basse" \tag #'tous <>^"Tous" sol,1 |
sol |
re4( mib) re( do) |
sib,( la,) sib,( sol,) |
fad,1 |
sol,2 <<
  \tag #'basse { r2 | R1*3 | }
  \tag #'(basse-continue tous) {
    \tag #'tous <>^"B.C."
    sol2~ | sol1 | la | la, |
  }
>>
\twoVoices #'(basse basse-continue tous) <<
  { \tag #'tous <>^"Basses" la4( sib) la( sol) | fad1 | }
  { \tag #'tous <>_"B.C." la1 | re | }
>> \tag #'tous <>^"Tous"
sol4( lab?) sol( fa) |
mib( re) do( sib,) |
lab,( sol,) lab,( fa,) |
\twoVoices #'(basse basse-continue tous) <<
  { sol,4 r r2 | }
  { sol,1 }
>>
<<
  \tag #'basse { R1*5 | }
  \tag #'(basse-continue tous) {
    \tag #'tous <>^"B.C."
    sol1 |
    sol4( lab) sol( fa) |
    \slurDashed mib( re) mib( do) | \slurSolid
    lab( sol) lab( fa) |
    sol2 sol, |
    \tag #'tous <>^"Tous"
  }
>>
do1~ |
do |
re |
re,~ |
re, |
sol,2 <<
  \tag #'basse { r2 | R1*3 | }
  \tag #'(basse-continue tous) {
    \tag #'tous <>^"B.C."
    sol2~ | sol1 | re~ | re~ |
  }
>>
<<
  \tag #'(basse tous) \new Voice {
    \tag #'tous { \voiceOne <>^"Basses" }
    re'4( mib') re'( do') |
    sib( lab) sol( fa) |
  }
  \tag #'(basse-continue tous) {
    \tag #'tous { <>_"B.C." \voiceTwo }
    re1 |
    sol4( lab) sol( fa) |
    \tag #'tous { <>^"Tous" \oneVoice }
  }
>>
mib4( re) mib( do) |
fa( sol) fa( mib) re( do) re( sib,) |
\twoVoices #'(basse basse-continue tous) <<
  { fa4 r r2 | }
  { fa1 }
>>
<<
  \tag #'basse { R1*3 | r2 }
  \tag #'(basse-continue tous) {
    \tag #'tous <>^"B.C."
    fa,1~ | fa,2 fa~ | fa1~ | fa2
    \tag #'tous <>^"Tous"
  }
>> fa,2 |
sib, si, |
do dod |
re1 |
mib4( re) mib( do) |
re( do) re( re,) |
sol,2 <<
  \tag #'basse { r2 | R1*2 | }
  \tag #'(basse-continue tous) {
    \tag #'tous <>^"B.C."
    \once\slurDashed sol4( la) |
    sib( do') re'( sib) |
    mib'( re') mib'( do') |
    \tag #'tous <>^"Tous"
  }
>>
re'4( mib') re'( do') |
\once\slurDashed sib( la) sol fa8 fa, |
mib4( re) mib( do) |
\twoVoices #'(basse basse-continue tous) <<
  { re4 r r2 | R1 | }
  { re1~ | re | }
>> \tag #'tous <>^"Tous"
re4( mib) re( do) |
\slurDashed sib,( la,) sib,( sol,) |
mib( re) mib( do) | \slurSolid
re( do) re( re,) |
\twoVoices #'(basse basse-continue tous) <<
  { sol,2 r | }
  { \tag #'tous <>_"B.C."
    \slurDashed sol,1 | }
>>

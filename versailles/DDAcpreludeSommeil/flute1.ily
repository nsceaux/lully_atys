\clef "dessus" R1*5 |
r2 \once\slurDashed sib''4( la'') |
sol''( la'') sol''( fa'') |
\once\slurDashed mi''( fa'') sol'' la''8 sol'' |
fa''2. sol''8 la'' |
mi''4. mi''8 mi''4. re''8 |
re''1 |
R1*3 |
r4 sol'' \once\slurDashed fa''( mib'') |
re'' sol'' fa''( mib'') |
re''1 |
\once\slurDashed \afterGrace mib''( { \once\override Staff.Parentheses.font-size = 1 \parenthesize fa''16) } |
\afterGrace mib''1( fa''16) |
mib''4. re''8 re''4. do''8 |
do''1 |
R1*3 |
\slurDashed re''4( mib'') re''( do'') |
sib'( la') sib'( do'') |
re''( do'') re''( mi'') |
fad''( sol'') la''2~ | \slurSolid
la''4 sib''8 la'' sol''2~ |
sol''4. la''8 fad''4. mi''16 fad'' |
sol''1 |
R1*3 |
\slurDashed r4 fa'' mib''( re'') |
do''( fa'') mib''( re'') |
do''( re'') mib'' fa''8 mib'' | \slurSolid
re''2. mib''8 re'' |
do''4. do''8 do''4. sib'8 |
sib'2 r |
R1*4 |
r2 sib' |
\afterGrace sib'1( do''16) |
\afterGrace sib'1( do''16) |
la'4. la'8 la'4. sol'8 |
sol'1 |
R1 |
\slurDashed r4 sib'' la''( sol'') |
fad''( sib'') la''( sol'') | \slurSolid
fad''2 r4 re''4 |
\afterGrace sol''1( la''16) |
\afterGrace sol''1( la''16) |
sol''2 fad''4. mi''16 fad'' |
sol''2 r |

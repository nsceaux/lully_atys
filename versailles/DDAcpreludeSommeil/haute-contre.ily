\clef "haute-contre" sib'4( do'') sib'( la') |
sol'( fad') \once\slurDashed sol'( la') |
fad'1 |
sol' |
\afterGrace re'1( mib'?16) |
re'2 r |
R1*4 |
la'1 |
\afterGrace sol'1( lab'16) |
\afterGrace sol'1( lab'16) |
sol'2 fa'4 lab' |
sol' r r2 |
R1*5 |
do''4( re'') do''( sib') |
la'( sib') la'( sol') |
fad'?( sol') la'2~ |
la'4 sol'8 fad'? sol'2~ |
sol'4. la'8 fad'4. sol'8 |
sol'2 r |
R1*4 |
\once\tieDashed sol'2~ sol'4. sol'8 |
\afterGrace sol'1( la'16) |
\afterGrace fa'1( sol'16) |
fa'2. fa'4 |
fa'4 r r2 |
R1*4 |
\once\slurDashed sib'4( do'') re''2 |
sol' la' |
la'4( sib') do''( re'') |
sib'2 do'' |
\slurDashed la'4( sib') do''( re'') | \slurSolid
sib'2 r |
R1*3 |
sol'1 |
sol'2 la' |
fad'4 r r2 |
R1 |
r2 r4 la' |
\afterGrace sib'1( do''16) |
sib'2. sib'4 |
la'4. la'8 la'4. sol'8 |
sol'2 r |

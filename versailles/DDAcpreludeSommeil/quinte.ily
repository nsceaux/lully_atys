\clef "quinte" \slurDashed sol4( fad4) sol( la) |
sib( do') re'2 | \slurSolid
re' la |
sib4 do' re'2 |
re' la |
sib r |
R1*4 |
la2 re' |
sol re' |
do'4( re') mib'2 |
mib' re'4 re' |
re' r r2 |
R1*5 |
sol1 |
la |
la4 sol fad re |
re' do' sib sol |
re'2 la |
sib r |
R1*4 |
\slurDashed sib4( do') sib( la) | \slurSolid
sol2 do' |
do'1 |
sib2 sib |
la4 r r2 |
R1*4 |
re'2 re' |
\slurDashed do'4( sib) la2 |
la4( sol) fad2 | \slurSolid
sol2. sol4 |
re'4. la8 la4. sol8 |
sol2 r |
R1*3 |
\afterGrace sib1( do'16) |
sib2 la |
la4 r r2 |
R1 |
r2 r4 re' |
\once\slurDashed re'4( do') sib4. la8 |
sol2. sol4 |
re'2. re'4 |
re'2 r |

\clef "dessus" R1*5 |
r2 \slurDashed sol''4( fa'') |
mi''( fa'') mi''( re'') |
dod''( re'') \tieDashed mi''2~ |
mi''4 re''8 dod'' re''2~ |
re''4. mi''8 dod''4. re''8 | \tieSolid
re''1 |
R1*3 |
r4 mib'' re''( do'') |
si' mib'' re''( do'') |
si'1 |
\afterGrace do''1( { \once\override Staff.Parentheses.font-size = 1 \parenthesize re''16) } | \slurSolid
\afterGrace do''1( re''16) |
do''2 si'4. la'16 si' |
do''1 |
R1*4 |
\slurDashed sol'4( fad') sol'( la') |
sib'( la') sib'( do'') |
la'( sib') do'' re''8 do'' |
sib'2. do''8 re'' |
la'4. la'8 la'4. sol'8 |
sol'1 |
R1*3 |
r4 re'' do''( sib') |
la'( re'') do''( sib') |
la'( sib') do''2~ |
do''4 sib'8 la' sib'2~ |
sib' la'4. sib'8 |
sib'2 r | \slurSolid
R1*4 |
r2 sol' |
\afterGrace sol'1( la'16) |
\afterGrace sol'1( la'16) |
sol'2 fad'4. mi'16 fad' |
sol'1 |
R1 |
r4 \slurDashed re'' do''( sib') |
la'( re'') do''( sib') | \slurSolid
la'2 r4 re''4 |
\afterGrace sol''1( la''16) |
\afterGrace sol''1( la''16) |
sol''2 fad''4. mi''16 fad'' |
sol''2 r |

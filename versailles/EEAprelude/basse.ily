\clef "basse" do2 do |
sol sol |
do'8 sib do' re' do' sib la sol |
fa2 fad |
sol8 la sol fa mi re do si, |
la,2 la |
si si |
do' dod' |
re'8 mi' re' do' si la si sol |
re do re mi re do si, la, |
sol,2 sol4 fa8 mi |
re4. re8 la4 fa |
sol8 la sol fa mi fa mi re |
do re do sib, la, sib, la, sol, |
fa,4 fa re2 |
sol8 la sol fa mi re mi do |
sol4 mi la fa |
sol mi la8 fa sol sol, |
do1 |

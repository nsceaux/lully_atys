\clef "quinte" sol2 do'4 do'8 mi' |
re' mi' re' do' si4. si8 |
\once\tieDashed do'2~ do'4. do'8 |
do'2 re' |
\once\tieDashed re'~ re'4. re'8 |
do'2 do'4. do'8 |
re'2 si8 do' re'4 |
mi'2 mi'4. mi'8 |
re'2 re' |
re' la4. la8 |
si4 si8 la sol4. sol8 |
la4 fa' mi' re' |
re' re' do' sol'8 fa' |
mi' fa' mi' re' do' re' do' sib |
la2 re'4 re' |
re'4. re'8 mi'4. mi'8 |
re'4 si4 do' la |
si si8. si16 do'4 sol8 sol |
sol1 |

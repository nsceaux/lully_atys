\clef "dessus" sol''8 la'' sol'' fa'' mi'' re'' mi'' do'' |
re'' do'' re'' mi'' fa'' mi'' fa'' sol'' |
mi''2~ mi''4. mi''8 |
fa'' sol'' fa'' mi'' re'' mi'' re'' do'' |
si'2~ si'4. si'8 |
do''8 re'' mi'' do'' fa'' sol'' fa'' mi'' |
re'' mi'' fa'' re'' sol'' la'' sol'' fa'' |
mi'' fa'' sol'' mi'' la'' sib''8 la'' sol'' |
fad''2 sol''4. sol''8 |
sol''4. la''8 fad''4. mi''16 fad'' |
sol''4 sol'8 la' si' do'' re'' mi'' |
fa'' mi'' fa'' re'' mi'' fa'' fa'' mi'' |
re''4 si'8 si' do'' re'' do'' re'' |
mi''4 mi''8 mi'' fa'' sol'' fa'' sol'' |
la'' sib'' la'' sol'' fa'' mi'' re'' do'' |
si'4. si'8 do''4 do''8 si'16 do'' |
re''8 sol'' sol'' fa'' mi'' fa'' fa'' mi'' |
re'' mi'' mi'' re'' do'' re'' re''8. do''16 |
do''1 |

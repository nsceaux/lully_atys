\clef "dessus" do''8\fortSug sol'' mi''8. do''16 sol''8 sol'16 la' si'8 sol' |
do'' la'16 si' do''8 la' re''8. mi''16 mi''8. re''16 |
re''2. |
fa''4\doux fa''4. fa''8 |
mi''4 mi''4. mi''8 |
mi''4 re''4. mi''8 |
mi''2 r8 si'16\fort si' si'8 mi'' |
dod''8 mi''16 mi'' mi''8 mi'' la'' la''16 la'' la''8 la'' |
fad''2 r8 re''\doux |
re''4 la'4. la'8 |
sib'4 la'4. re''8 |
si'2. |
si'2 r8 sol''16\fort sol'' sol''8 sol'' |
mi''8 do''16 re'' mi'' fa'' sol'' mi'' fa''8 fa''16 sol'' la'' sol'' fa'' mi'' |
re''8 mi''16 fa'' sol'' fa'' mi'' re'' dod''8[ re''16 mi''] mi''8. re''16 |
re''2 fad''8\doux fad'' |
fad''2 sol''8 sol'' |
sol''4 fad''4. mi''16 fad'' |
sol''2 r8 sol'16\fort la' si' do'' re'' si' |
mi''8 sol''16 la'' sol'' fa'' mi'' re'' do''8 do''16 do'' do''8 do'' |
fa''8 fa''16 fa'' fa''8 fa'' re'' sol'' re'' sol'' |
\ru#2 {
  mi''4. do''8\doux sol' la' |
  sib'4 sib'4. do''8 |
  la'2 r8 do''\fort do'' do'' |
  fa'' la''16 sol'' fa'' mi'' re'' do'' si' sol' la' si' do'' re'' mi'' fa'' |
  sol''4 sol''8 fa''16 sol'' la''16 sol'' fa'' mi'' re''8. do''16 |
}
do''1
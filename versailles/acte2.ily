\newBookPart #'()
\act "Acte Second"
\sceneDescription \markup \wordwrap-center {
  [Le théâtre change et représente le temple de Cybèle.]
}
\scene "Scène Premiere" "SCÈNE 1 : Célénus, Atys"
\sceneDescription\markup\wordwrap-center {
  \smallCaps Célénus [roi de Phrigie,]
  \smallCaps Atys, [suivants de Célénus.]
}
%% 2-1
\pieceToc "Ritournelle"
\includeScore "CAAritournelle"
%% 2-2
\pieceToc\markup\wordwrap {
  Récit : \italic { N’avancez pas plus loin, ne suivez point mes pas }
}
\includeScore "CABrecitNavancezPasPlusLoin"
\newBookPart#'(full-rehearsal)

\scene "Scène II" "SCÈNE 2 : Cybèle, Célénus"
\sceneDescription\markup\center-column {
  \line\smallCaps { Cybèle, Célénus, }
  \line { \smallCaps [Mélisse, troupe de prêtresses de Cybèle.] }
}
%% 2-3
\pieceToc "Prélude"
\includeScore "CBAprelude"
\newBookPart#'(haute-contre haute-contre-sol taille quinte)
%% 2-4
\pieceToc\markup\wordwrap {
  Récit : \italic { Je veux joindre en ces lieux la gloire et l’abondance }
}
\includeScore "CBBrecitJeVeuxJoindreEnCesLieux"
\newBookPart#'(full-rehearsal)

\scene "Scène III" "SCÈNE 3 : Cybèle, Mélisse"
\sceneDescription\markup\smallCaps { Cybèle, Mélisse. }
%% 2-5
\pieceToc\markup\wordwrap {
  Récit : \italic { Tu t’étonnes, Mélisse, et mon choix te surprend ? }
}
\includeScore "CCArecitTuTetonnesMelisse"
\newBookPart#'(full-rehearsal haute-contre haute-contre-sol taille quinte)

\scene "Scène IV" \markup \wordwrap {
  SCÈNE 4 : Cybèle, Atys, troupes de peuples et de Zéphirs
}
\sceneDescription\markup\column {
  \justify {
    [Les Zéphirs paraissent dans une gloire élevée et brillante.  Les
    peuples différents qui sont venus à la fête de Cybèle entrent dans
    le temple, et tous ensemble s’efforcent d’honorer Atys, qui vient
    revêtu des habits de grand sacrificateur.]
  }
  \smaller\italic\justify {
    [Cinq Zéphirs dansants dans la gloire.  Huit Zéphirs jouants du
    hautbois et des cormornes, dans la gloire.  Troupe de peuples
    différents chantants qui accompagnent Atys.  Six Indiens et six
    Egyptiens dansants.]
  }
}
%% 2-6
\pieceToc\markup\wordwrap {
  Chœur : \italic { Célébrons la gloire immortelle }
}
\includeScore "CDAchoeurCelebronsLaGloireImmortelle"
\newBookPart#'(full-rehearsal)
%% 2-7
\pieceToc "Entrée des Nations"
\includeScore "CDBentreeNations"
%% 2-8
\pieceToc "Entrée des Zéphirs"
\includeScore "CDCentreeZephirs"
\newBookPart#'(full-rehearsal)
%% 2-9
\pieceToc \markup {
  Chœur : \italic { Que devant vous tout s’abaisse, et tout tremble }
}
\includeScore "CDDchoeurQueDevantVous"
\newBookPart#'(full-rehearsal)
%% 2-10
\pieceToc \markup {
  Récit : \italic { Indigne que je suis des honneurs qu’on m’adresse }
}
\includeScore "CDEindigneQueJeSuis"
%% 2-11
\pieceToc \markup {
  Chœur : \italic { Que la puissante Cybèle }
}
\includeScore "CDFchoeurQueLaPuissanteCybele"
%% 2-12
\pieceToc \markup {
  Chœur : \italic { Que devant vous tout s’abaisse, et tout tremble }
}
\reIncludeScore "CDDchoeurQueDevantVous" "CDGchoeur"
\newBookPart#'(full-rehearsal)
%% 2-13
\pieceToc "Entr’acte"
\reIncludeScore "CDCentreeZephirs" "CDHzephirs"
\actEnd "FIN DU SECOND ACTE"

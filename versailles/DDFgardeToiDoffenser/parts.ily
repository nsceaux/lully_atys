\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Un songe funeste
    \livretVerse#12 { Garde-toy d’offencer un amour glorieux, }
    \livretVerse#12 { C’est pour toy que Cybele abandonne les Cieux }
    \livretVerse#8 { Ne trahis point son esperance. }
  }
  \column {
    \livretVerse#12 { Il n’est point pour les Dieux de mespris innocent, }
    \livretVerse#12 { Ils sont jaloux des Cœurs, ils ayment la vengeance, }
    \livretVerse#8 { Il est dangereux qu’on offence }
    \livretVerse#6 { Un amour tout-puissant. }
  }
} #}))

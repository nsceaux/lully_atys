Gar -- de- toy d'of -- fen -- cer un a -- mour glo -- ri -- eux,
C'est pour toy que Cy -- bele a -- ban -- don -- ne les cieux
Ne tra -- his point son es -- pe -- ran -- ce.
Il n'est point pour les dieux de mes -- pris in -- no -- cent,
Ils sont ja -- loux des cœurs, ils ay -- ment la ven -- gean -- ce,
ils ay -- ment la ven -- gean -- ce,
Il est dan -- ge -- reux qu'on of -- fen -- ce
Un a -- mour tout- puis -- sant.

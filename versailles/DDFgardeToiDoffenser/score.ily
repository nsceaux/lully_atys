\score {
  <<
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with {
        instrumentName = \markup\center-column { Un Songe funeste }
        shortInstrumentName = \markup\center-column { Un Songe funeste }
      } \withLyrics <<
        \global \includeNotes "voix"
      >> \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout { }
  \midi { }
}
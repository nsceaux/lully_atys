\version "2.24.0"
\include "common.ily"
\paper {
  %% plus d'espace entre titre et musique
  markup-system-spacing.padding = #2.5 % default: 0.5
  system-system-spacing.padding = #2.5 % default: 1
  %% de la place entre dernier system et copyright
  last-bottom-spacing.padding = #3
}
\layout {
  \context {
    \Lyrics
    %% de la place sous les paroles (basse continue)
    \override VerticalAxisGroup.nonstaff-unrelatedstaff-spacing.padding = #3
  }
}
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header { title = \markup\smallCaps Atys }
  %% Title page
  \markup\null\pageBreak
  %% Table of contents
  \markuplist\abs-fontsize-lines #7
  \override-lines #'(use-rehearsal-numbers . #t)
  \override-lines #'(column-number . 2)
  \override-lines #'(rehearsal-number-gauge . "4-16b")
  \table-of-contents
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Prologue
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\actn "Prologue"
%% 0-1
\pieceToc "Ouverture"
\includeScore "AAAouverture"
%% 0-2
\pieceToc\markup\wordwrap {
  \italic { En vain j’ay respecté la celebre memoire }
}
\includeScore "AABrecitEnVainJaiRespecte"
%% 0-3
\pieceToc\markup\wordwrap { \italic { Ses justes loix } }
\includeScore "AACchoeurSesJustesLois"
%% 0-4
\pieceToc "Premier air pour la suite de Flore"
\includeScore "AADairFlore"
%% 0-5
\pieceToc\markup\wordwrap {
  \italic { La Saison des frimas peut-elle nous offrir }
}
\includeScore "AAErecitLaSaisonDesFrimas"
%% 0-6
\pieceToc\markup\wordwrap {
  \italic { Les Plaisirs à ses yeux ont beau se presenter }
}
\includeScore "AAFairChoeurLesPlaisirASesYeux"
%% 0-7
\pieceToc\markup\wordwrap {
  \italic { Rien ne peut l’arrester }
}
\includeScore "AAFbChoeurRienNePeutLArreter"
%% 0-8
\pieceToc "Air pour la suite de Flore"
\includeScore "AAGgavotte"
%% 0-9
\pieceToc\markup\wordwrap {
  \italic { Le printemps quelquefois est moins doux qu’il ne semble }
}
\includeScore "AAHairLePrintempsQuelqueFois"
%% 0-9b
\pieceTocNb "0-9b" "Air pour la suite de Flore"
\reIncludeScore "AAGgavotte" "AAHbGavotte"
%% 0-10
\pieceToc "Prelude pour Melpomene"
\includeScore "AAIpreludeMelpomene"
%% 0-11
\pieceToc\markup {
  \italic { Retirez-vous, cessez de prevenir le Temps }
}
\includeScore "AAJrecitRetirezVous"
%% 0-12
\pieceToc "Air pour la suite de Melpomene"
\includeScore "AAKairMelpomene"
\partPageTurn#'(basse-viole)
%% 0-13
\pieceToc "Ritournelle"
\includeScore "AALaRitournelle"
%% 0-14
\pieceToc\markup\wordwrap {
  \italic { Cybele veut que Flore aujourd’huy vous seconde }
}
\includeScore "AALbCybeleVeutQueFlore"
AALcPreparonsDeNouvellesFetesFirstBar = 36
\includeScore "AALcPreparonsDeNouvellesFetes"
%% 0-15
\pieceToc "Menuet"
\includeScore "AAMmenuet"
\newBookPart#'(basse)
AALcPreparonsDeNouvellesFetesFirstBar = 1
globalAALcPreparonsDeNouvellesFetes = ##f
%% 0-15b
\pieceTocNb "0-15b" \markup\italic { Preparons de nouvelles festes }
\reIncludeScore "AALcPreparonsDeNouvellesFetes"  "AALNpreparonsDeNouvellesFetes"
\actEnd "FIN DU PROLOGUE"
%% 0-16
\pieceToc "Ouverture"
\reIncludeScore "AAAouverture" "AANouverture"
\newBookPart#'()

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Acte I
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\act "Acte Premier"
\scene "Scene Premiere" "SCENE 1 : Atys"
%% 1-1
\pieceToc "Ritournelle"
\includeScore "BAAritournelle"
\partNoPageBreak#'(dessus)
%% 1-2
\pieceToc\markup\wordwrap\italic { Allons, allons, accourez tous }
\includeScore "BAAairAllonsAllons"
\scene "Scene II" "SCENE 2 : Atys, Idas"
%% 1-3
\pieceToc\markup\wordwrap\italic { Allons, allons, accourez tous }
\includeScore "BBAairAllonsAllons"
\newBookPart#'(dessus)
\scene "Scene III" "SCENE 3 : Sangaride, Doris, Atys, Idas"
%% 1-4
\pieceToc\markup\wordwrap\italic { Allons, allons, accourez tous }
\includeScore "BCAairAllonsAllons"
\scene "Scene IV" "SCENE 4 : Sangaride, Doris"
%% 1-5
\pieceToc\markup\wordwrap\italic { Atys est trop heureux }
\includeScore "BDAatysEstTropHeureux"
\newBookPart#'(dessus)
\scene "Scene V" "SCENE 5 : Sangaride, Doris, Atys"
%% 1-6
\pieceToc\markup\wordwrap\italic { On voit dans ces campagnes }
\includeScore "BEArecitOneVoitDansCesCampagnes"
\scene "Scene VI" "SCENE 6 : Sangaride, Atys"
%% 1-7
\pieceToc\markup\wordwrap\italic { Sangaride ce jour est un grand jour pour vous }
\includeScore "BFArecitSangarideCeJour"
\newBookPart#'(dessus haute-contre taille quinte basse)
\scene "Scene VII" "SCENE 7 : Sangaride, Atys, chœur de Phrygiens"
%% 1-8
\pieceToc\markup\wordwrap\italic { Mais déja de ce mont sacré }
\includeScore "BGAairMaisDejaDeCeMontSacre" \partNoPageTurn#'(basse-continue basse-tous basse-viole)
\includeScore "BGAbChoeurCommencons"
\newBookPart#'(haute-contre taille quinte basse)
%% 1-9
\pieceToc\markup\wordwrap { Premier air pour les Phrygiens [et Phrygiennes] }
\includeScore "BGBEntreePhrygiens"
%% 1-10
\pieceToc\markup\wordwrap { Second air pour les Phrygiens [et Phrygiennes] }
\includeScore "BGCSecondAirPhrygiens"
\scene "Scene VIII" "SCENE 8 : Cybele, chœur de Phrygiens"
%% 1-11
\pieceToc "Prelude [pour Cybele]"
\includeScore "BHApreludeCybele"
%% 1-12
\pieceToc\markup\wordwrap\italic { Venez tous dans mon temple, et que chacun revere }
\includeScore "BHBairChoeurVenezTousDansMonTemple"
%% 1-13
\pieceToc\markup\wordwrap\italic { Nous devons nous animer }
\includeScore "BHCchoeurNousDevonsNousAnimer"
%% 1-13b
\pieceTocNb "1-13b" \markup\wordwrap { Entracte }
\reIncludeScore "BGCSecondAirPhrygiens" "BHDentracte"
\actEnd "FIN DU PREMIER ACTE"
\tocBreak
\newBookPart#'()

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Acte II
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\act "Acte Second"
\scene "Scene Premiere" "SCENE 1 : Celænus, Atys"
%% 2-1
\pieceToc "Ritournelle"
\includeScore "CAAritournelle"
%% 2-2
\pieceToc\markup\wordwrap\italic {
  N’avancez pas plus loin, ne suivez point mes pas
}
\includeScore "CABrecitNavancezPasPlusLoin"
\scene "Scene II" "SCENE 2 : Cybele, Celænus"
%% 2-3
\pieceToc "Prelude"
\includeScore "CBAprelude"
\newBookPart#'(haute-contre taille quinte basse)
%% 2-4
\pieceToc\markup\wordwrap\italic {
  Je veux joindre en ces lieux la gloire & l’abondance
}
\includeScore "CBBrecitJeVeuxJoindreEnCesLieux"
\scene "Scene III" "SCENE 3 : Cybele, Melisse"
%% 2-5
\pieceToc\markup\wordwrap\italic {
  Tu t’estonnes, Melisse, & mon choix te surprend ?
}
\includeScore "CCArecitTuTetonnesMelisse"
\newBookPart#'(haute-contre taille quinte basse)
\scene "Scene IV" \markup\wordwrap {
  SCENE 4 : Cybele, Atys, troupes de pleuples et de Zephirs
}
%% 2-6
\pieceToc\markup\wordwrap\italic { Celebrons la gloire immortelle }
\includeScore "CDAchoeurCelebronsLaGloireImmortelle"
%% 2-7
\pieceToc "[Premier air]"
\includeScore "CDBentreeNations"
%% 2-8
\pieceToc "[Deuxième air – L’Écho]"
\includeScore "CDCentreeZephirs"
%% 2-9
\pieceToc\markup\wordwrap\italic {
  Que devant vous tout s'abaisse, & tout tremble
}
\includeScore "CDDchoeurQueDevantVous"
%% 2-10
\pieceToc\markup\wordwrap\italic {
  Indigne que je suis des honneurs qu’on m’adresse
}
\includeScore "CDEindigneQueJeSuis"
%% 2-11
\pieceToc\markup\wordwrap\italic { Que la puissante Cybele }
\includeScore "CDFchoeurQueLaPuissanteCybele"
%% 2-12
\pieceToc\markup\wordwrap\italic {
  Que devant vous tout s'abaisse, & tout tremble
}
\reIncludeScore "CDDchoeurQueDevantVous" "CDGchoeurQueDevantVous"
%% 2-13
\pieceToc "[L’Écho]"
\reIncludeScore "CDCentreeZephirs" "CDHentreeZephirs"
\actEnd "FIN DU SECOND ACTE"
\newBookPart#'()

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Acte III
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\act "Acte Troisiesme"
\scene "Scene Premiere" "SCENE 1 : Atys"
%% 3-1
\pieceToc\markup Ritournelle
\includeScore "DAAritournelle"
%% 3-2
\pieceToc\markup\wordwrap\italic {
  Que servent les faveurs que nous fait la Fortune
}
\includeScore "DAArecitQueServentLesFaveurs"
\scene "Scene II" "SCENE 2 : Idas, Doris, Atys"
%% 3-3
\pieceToc\markup\wordwrap\italic {
  Peut-on icy parler sans feindre
}
\includeScore "DBArecitPeutOnIciParlerSansFeindre"
\scene "Scene III" "SCENE 3 : Atys"
%% 3-4
\pieceToc "[Prelude]"
\includeScore "DCAprelude"
%% 3-5
\pieceToc\markup\wordwrap\italic {
  Nous pouvons nous flater de l’espoir le plus doux
}
\includeScore "DCBrecitNousPouvonsNousFlatter"
\scene "Scene IV" "SCENE 4 : Sommeil"
%% 3-6
\pieceToc\markup\wordwrap { Prelude du Sommeil }
\includeScore "DDApreludeSommeil" \partNoPageTurn#'(basse-continue basse-tous basse-viole basse)
%% 3-7
\pieceToc\markup\wordwrap\italic { Dormons, dormons tous }
\includeScore "DDAdormons"
%% 3-8
\pieceToc\markup\wordwrap { Prelude du Sommeil }
\includeScore "DDAcpreludeSommeil"
%% 3-9
\pieceToc\markup\wordwrap\italic { Escoute, escoute Atys la gloire qui t’appelle }
\includeScore "DDBecouteAtys"
%% 3-10
\pieceToc "Les Songes agreables"
\includeScore "DDCentreeSongesAgreables"
%% 3-10b
\pieceTocNb "3-10b" \markup\wordwrap\italic { Trop heureux un amant }
\includeScore "DDBbTropHeureuxUnAmant"
%% 3-10c
\pieceTocNb "3-10c" "Les Songes agreables"
\includeScore "DDCbairSongesAgreables"
%% 3-11
\pieceToc\markup\wordwrap\italic { Gouste en paix chaque jour une douceur nouvelle }
\includeScore "DDDgouteEnPaix"
%% 3-12
\pieceToc "Les Songes agreables"
\includeScore "DDEairSongesAgreables"
%% 3-13
\pieceToc\markup\wordwrap\italic { Garde-toy d’offencer un amour glorieux }
\includeScore "DDFgardeToiDoffenser"
%% 3-14
\pieceToc "[Entrée des Songes Funestes]"
\includeScore "DDGentreeSongesFunestes"
%% 3-15
\pieceToc\markup\wordwrap\italic { L’amour qu’on outrage }
\includeScore "DDHchoeurLamourQuonOutrage"
%% 3-16
\pieceToc "[Deuxième entrée des] Songes funestes"
\includeScore "DDIsongesFunestes"
\scene "Scene V" "SCENE 5 : Atys, Cybele, Melisse"
%% 3-17
\pieceToc\markup\wordwrap\italic { Venez à mon secours ô Dieux ! ô justes Dieux ! }
\includeScore "DEArecitVenezAMonSecours"
\scene "Scene VI" "SCENE 6 : Sangaride, Cybele, Atys, Melisse"
%% 3-18
\pieceToc\markup\wordwrap\italic { Je sçays trop ce que je vous doy }
\includeScore "DFArecitJaiRecoursAVotrePuissance"
\scene "Scene VII" "SCENE 7 : Cybele, Melisse"
%% 3-19
\pieceToc\markup\wordwrap\italic { Qu’Atys dans ses respects mesle d’indifference }
\includeScore "DGArecitQuAtysDansSesRespects"
\newBookPart#'(dessus)
\scene "Scene VIII" "SCENE 8 : Cybele"
%% 3-20
\pieceToc\markup\italic { Espoir si cher, & si doux }
\includeScore "DHAespoirSiCherEtSiDoux"
%% 3-20b
\pieceTocNb "3-20b" \markup\wordwrap { Entracte }
\reIncludeScore "CDBentreeNations" "DHBentracte"
\actEnd "FIN DU TROISIESME ACTE"
\tocBreak
\newBookPart#'()

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Acte IV
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\act "Acte Quatriesme"
\scene "Scene Premiere" "SCENE 1 : Sangaride, Doris, Idas"
%% 4-1
\pieceToc\markup\wordwrap\italic { Quoy, vous pleurez ? }
\includeScore "EAArecitQuoiVousPleurez"
\scene "Scene II" "SCENE 2 : Sangaride, Celænus"
%% 4-2
\pieceToc "Prelude"
\includeScore "EBAprelude"
\newBookPart#'(haute-contre taille quinte basse)
%% 4-3
\pieceToc\markup\wordwrap\italic {
  Belle nymphe, l’hymen va suivre mon envie
}
\includeScore "EBBbelleNymphe"
\scene "Scene III" "SCENE 3 : Atys, Sangaride, Celænus"
%% 4-4
\pieceToc\markup\wordwrap\italic {
  Vostre cœur se trouble, il soûpire
}
\includeScore "ECArecitVotreCoeurSeTrouble"
\scene "Scene IV" "SCENE 4 : Sangaride, Atys"
%% 4-5
\pieceToc "Ritournelle"
\includeScore "EDAritournelle"
\newBookPart#'(haute-contre taille quinte basse)
%% 4-6
\pieceToc\markup\italic { Qu’il sçait peu son malheur }
\includeScore "EDBrecitQuilSaitPeuSonMalheur"
\scene "Scene V" \markup\wordwrap { SCENE 5 : Le Fleuve Sangar, les Fleuves }
%% 4-7
\pieceToc "Prelude pour les fleuves"
\includeScore "EEAprelude"
\newBookPart#'(haute-contre taille quinte basse)
%% 4-8
\pieceToc\markup\wordwrap\italic { O vous, qui prenez part au bien de ma famille }
\includeScore "EEBOVousQuiPrenezPart"
%% 4-9
\pieceToc\markup\wordwrap\italic { Que l’on chante, que l’on dance }
\includeScore "EECairQueLonChante"
%% 4-10
\pieceToc\markup\wordwrap\italic { Que l’on chante, que l’on dance }
\includeScore "EECchoeurQueLonChante"
%% 4-11
\pieceToc\markup\wordwrap\italic { La beauté la plus severe }
\includeScore "EEDflutes"
\includeScore "EEDairLaBeauteLaPlusSevere"
%% 4-12
\pieceToc\markup\wordwrap\italic { L’Hymen seul ne sçauroit plaire }
\includeScore "EEEflutes"
\includeScore "EEEairLhymenSeulNeSauraitPlaire"
%% 4-11b
\pieceTocNb "4-11b" \markup\wordwrap\italic { Il n’est point de resistance }
\reIncludeScore "EEDflutes" "EEDflutesBis"
\includeScore "EEDairIlNEstPointDeResistance"
%% 4-12b
\pieceTocNb "4-12b" \markup\wordwrap\italic { L’Amour trouble tout le monde }
\reIncludeScore "EEEflutes" "EEEflutesBis"
\includeScore "EEEairLAmourTroubleToutLeMonde"
\newBookPart#'(haute-contre taille quinte basse)
%% 4-13
\pieceToc "Menuet"
\includeScore "EEFmenuet"
%% 4-14
\pieceToc\markup\wordwrap\italic { D’une constance extresme }
\includeScore "EEGduneConstanceExtreme"
%% 4-13b
\pieceTocNb "4-13b" "Menuet"
\includeScore "EEFmenuetBis"
%% 4-14b
\pieceTocNb "4-14b" \markup\wordwrap\italic { Jamais un cœur volage }
\includeScore "EEGjamaisUnCoeurVolage"
%% 4-15
\pieceToc "Gavotte"
\includeScore "EEHgavotte"
%% 4-16
\pieceToc\markup\wordwrap\italic { Un grand calme est trop fascheux }
\includeScore "EEIchoeurUnGrandCalmeEstTropFacheux"
%% 4-15b
\pieceTocNb "4-15b" "Gavotte"
\reIncludeScore "EEHgavotte" "EEHgavotteBis"
%% 4-16b
\pieceTocNb "4-16b" \markup\wordwrap\italic { Un grand calme est trop fascheux }
\reIncludeScore "EEIchoeurUnGrandCalmeEstTropFacheux" "EEIchoeurUnGrandCalmeEstTropFacheuxBis"
\scene "Scene VI" \markup \wordwrap { SCENE 6 : Atys, Celænus, Sangar, troupe de dieux des fleuves }
%% 4-17
\pieceToc\markup\wordwrap\italic { Venez formez des nœuds charmants }
\includeScore "EFAchoeurVenezFormerDesNoeudsCharmants"
%% 4-18
\pieceToc "Entr’acte"
\includeScore "EFBentracte"
\actEnd "FIN DU QUATRIESME ACTE"
\tocBreak
\newBookPart#'()

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Acte V
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\act "Acte Cinquiesme"
\scene "Scene Premiere" "SCENE 1 : Celænus, Cybele"
%% 5-1
\pieceToc "Ritournelle"
\includeScore "FAAritournelle"
%% 5-2
\pieceToc\markup\wordwrap\italic { Vous m’ostez Sangaride, inhumaine Cybelle }
\includeScore "FABvousMotezSangaride"
\partNoPageTurn#'(basse-continue basse-tous basse-viole)
\scene "Scene II" "SCENE 2 : Celænus, Cybele, Sangaride, Atys"
%% 5-3
\pieceToc\markup\wordwrap\italic { Venez vous livrer au suplice }
\includeScore "FBAvenezVousLivrerAuSupplice"
\newBookPart#'(haute-contre taille quinte basse)
\scene "Scene III" "SCENE 3 : Atys, Sangaride, Cybele, Celænus, chœur"
%% 5-4
\pieceToc "Prelude pour Alecton"
\includeScore "FCApreludePourAlecton"
%% 5-5
\pieceToc\markup\wordwrap\italic { Ciel ! quelle vapeur m’environne }
\includeScore "FCBcielQuelleVapeurMenvironne"
\newBookPart#'(haute-contre taille quinte basse)
\scene "Scene IV" \markup\wordwrap { SCENE 4 : Cybele, Atys }
%% 5-6
\pieceToc\markup\wordwrap\italic { Que je viens d’immoler une grande victime }
\includeScore "FDAqueJeViensDimmoler"
\scene "Scene V" "SCENE 5 : Cybele, Melisse"
%% 5-7
\pieceToc\markup\wordwrap\italic { Je commence à trouver sa peine trop cruelle }
\includeScore "FEAjeCommenceATrouverSaPeineTropCruelle"
\partNoPageTurn#'(basse-continue basse-tous basse-viole)
\scene "Scene VI" "SCENE 6 : Cybele, Atys, Idas, Melisse"
%% 5-8
\pieceToc\markup\wordwrap\italic { Il s’est percé le sein }
\includeScore "FFAilSestPerceLeSein"
\scene "Scene VII" "SCENE 7 : Cybele, chœur"
%% 5-9
\pieceToc "Ritournelle"
\includeScore "FGAritournelle"
%% 5-10
\pieceToc\markup\wordwrap\italic { Venez furieux Corybantes }
\includeScore "FGBvenezFurieuxCorybantes"
%% 5-10b
\pieceTocNb "5-10b" "Ritournelle"
\reIncludeScore "FGAritournelle" "FGCritournelle"
%% 5-11
\pieceToc\markup\wordwrap\italic { Atys, l’aimable Atys, malgré tous ses attraits }
\includeScore "FGCairChoeurAtysAimableAtys"
\includeScore "FGCquelleDouleur"
%% 5-12
\pieceToc "Entrée des nymphes"
\includeScore "FGDentreeNymphes"
%% 5-13
\pieceToc "[Première entrée des] corybantes"
\includeScore "FGEentreeCorybantes"
%% 5-14
\pieceToc "[Seconde entrée des corybantes]"
\includeScore "FGFsecondeEntreeCorybantes"
%% 5-15
\pieceToc\markup\wordwrap\italic { Que le malheur d’Atys afflige tout le monde }
\includeScore "FGGchoeurQueLeMalheurDAtys"
\actEnd "FIN DU CINQUIESME ET DERNIER ACTE"
\tocFiller#10

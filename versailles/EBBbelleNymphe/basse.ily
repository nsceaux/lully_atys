\clef "basse" do1~ |
do2 si, |
do la,4 mi, |
fa, fa2 |
mi4 fa4 sol do |
sol,1 |
re,2. |
la, |
re4 mib8 do re re, |
sol,2. mi,4 |
fa,2 sol, |
sol mi |
fa fad |
sol4. fa!8 mi2 |
re4 do sol,2 |
do si, |
la, la4. sol8 |
fa1 |
mi |\allowPageTurn
la,2. |
re2 re8 do |
si,4 fad,2 |
sol,4 sol dod2 |
re sib, |
la,2. la8 sol |
fa4. re8 la4 la, |
re2 do4 |
si,2. |
do2 re |
mi fa8 re mi mi, |
la,2.~ |
la,4 fad,2 |


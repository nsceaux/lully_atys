<<
  %% Sangaride
  \tag #'(sangaride basse) {
    <<
      \tag #'basse { s1*3 | s2. | s1*2 | s2.*3 | s1*9 | s4 \sangarideMark }
      \tag #'sangaride { \sangarideClef R1*3 | R2. | R1*2 | R2.*3 | R1*9 | r4 }
    >> r8 mi'' si'8. si'16 mi''8. si'16 |
    do''4 r8 do''16 si' la'8 la'16 sol' |
    fa'8 fa' r la'16 la' la'8 si'16 do'' |
    re''4 la'8 la'16 la' si'8 do'' |
    si'4
    << { s2. | s1 | s2 } \tag #'sangaride { r4 r2 | R1 | r2 } >>
    \tag #'basse \sangarideMark r8 la'16 la' la'8 si'16 do'' |
    re''4 re''8. re''16 re''4 re''8. dod''16 |
    re''4
    << { s2 | s2. | s4. } \tag #'sangaride { r2 | R2. | r4 r8 } >>
    \tag #'basse \sangarideMark do''8 do''8. do''16 si'8. la'16 |
    sold'4 sold'8. sold'16 la'4 la'8. sold'16 |
    la'4 r8 mi'16 mi' la'8 la'16 si' |
    do''4 do''8 do''16 do'' do''8. si'16 |
  }
  %% Celaenus
  \tag #'(celaenus basse) {
    \tag #'basse \celaenusMark
    \tag #'celaenus \celaenusClef r4 do'8. do'16 sol8 sol r la |
    fa4 r8 mi re re mi8. fa16 |
    mi8 mi r mi fa fa16 fa sol8 la16 sib |
    la8 la r fa16 sol la8 la16 si |
    do'4 do'8 re' si4 si8 do' |
    re'4 r8 sol re8. re16 re8. mi16 |
    fa4 r16 fa fa sol la8. si16 |
    do'4 r8 do'16 si la8 la16 sol |
    fad8. fad16 sol8. sol16 sol8. fad16 |
    sol4 sol sol8 r16 sol16 do'8 do'16 do' |
    la8 la16 la fa8 fa16 mi re4 r8 re' |
    si8. sol16 la8. si16 do'8 sol16 la sib8 sib16 la |
    la8 la r16 la la si! do'8. do'16 do'8. re'16 |
    si4 si r8 sol sol8. la16 |
    fa4. mi8 re4. do8 |
    do4 r8 sol sold8 sold16 sold sold8 la16 si |
    do'8 do' r do' do'8. do'16 re'8. mi'16 |
    la4 la8. la16 si4 si8. si16 |
    sold4
    << { s2. | s2.*3 | s4 } \tag #'celaenus { r4 r2 | R2.*3 | r4 } >>
    \tag #'basse \celaenusMark r8 sol16 sol sol4. fa16 mi |
    fa4 r8 la re'8. re'16 re'8. mi'16 |
    dod'4 dod'
    << { s2 | s1 | s4 } \tag #'celaenus { r2 | R1 | r4 } >>
    \tag #'basse \celaenusMark fad8 fad16 fad fad8. fad16 |
    sol4 fa8. fa16 fa8 fa16 mi |
    mi4 << \tag #'celaenus mi4 \tag #'basse { mi8 s } >>
    \tag #'celaenus { r2 | R1 | R2.*2 }
  }
>>
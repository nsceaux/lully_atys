\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Celænus
    \livretVerse#12 { Belle Nymphe, l’Hymen va suivre mon envie, }
    \livretVerse#8 { L’Amour avec moy vous convie }
    \livretVerse#12 { A venir vous placer sur un Thrône éclatant, }
    \livretVerse#12 { J’aproche avec transport du favorable instant }
    \livretVerse#12 { D’où despend la douceur du reste de ma vie : }
    \livretVerse#12 { Malgré tous les transports de mon ame amoureuse, }
    \livretVerse#8 { Si je ne puis vous rendre heureuse, }
    \livretVerse#8 { Je ne seray jamais content. }
    \livretVerse#8 { Je fais mon bonheur de vous plaire, }
    \livretVerse#12 { J’attache à vostre cœur mes desirs les plus doux. }
  }
  \column {
    \livretPers Sangaride
    \livretVerse#12 { Seigneur, j’obeïray, je despens de mon Pere, }
    \livretVerse#12 { Et mon Pere aujourd’huy veut que je sois à vous. }
    \livretPers Celænus
    \livretVerse#12 { Regardez mon amour, plustost que ma Couronne. }
    \livretPers Sangaride
    \livretVerse#12 { Ce n’est point la grandeur qui me peut esbloüir. }
    \livretPers Celænus
    \livretVerse#12 { Ne sçauriez-vous m’aimer sans que l’on vous l’ordonne. }
    \livretPers Sangaride
    \livretVerse#12 { Seigneur, contentez-vous que je sçache obeïr, }
    \livretVerse#12 { En l’estat où je suis c’est ce que je puis dire… }
  }
}#}))

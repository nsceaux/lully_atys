\clef "haute-contre" re''2 re''4. re''8 |
do''2. do''8 do'' |
re''2. re''4 |
re''2. re''4 |
re''2 do'' |
do'' fa'4 sol' |
la'4. la'8 re'2 |
sib'4. sib'8 sib'4. do''8 |
re''4. re''8 re''4. re''8 |
sib'8. la'16 sol'4 sol'4. sol'8 |
la'2 do''4. do''8 |
reb''2 reb''4. reb''8 |
do''2 do''4. do''8 |
do''4. sol'8 do''4. sib'8 |
la'1 |
la'4. la'8 la'4. si'8 |
do''2 mib''4 re'' |
do'' mib' fa'2 |
sol'2. sol'4 |
sol'2 re''4. re''8 |
do''4. re''8 si'4. do''8 |
do''2 do''4. do''8 |
sib'4 fa'2 fa'4 |
mib'2 sol'4. sol'8 |
fa'2 sib'4. sib'8 |
sol'4 sol' fa'4. fa'8 |
re'16[ mib' fa' sol'] la'8 sib'16 do'' re''4. re''8 |
sib'2. sib'4 |
sib'1 |
sib'4. sib'8 la'4. sib'8 |
sib'1 |

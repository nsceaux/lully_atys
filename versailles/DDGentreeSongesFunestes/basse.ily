\clef "basse" sib,2 sib4. sib8 |
sib2 la |
sib2. sol4 |
re'2 re4 r16 re mib fa |
si,2 do4 r16 do re mib |
la,2 sib,4 r16 sib, do re |
fad,2 sol, |
sol4. sol8 fa4. mib8 |
re2. sib,4 |
mib2 mi |
fa2. fa,4 |
sib,2 sib4. sib8 |
sib2 lab4 fa |
do'2 do |
fa1 |
fa4. fa8 mib4 re |
do2 do'4 sib |
lab1 |
sol4 lab8 sol fa mib re do |
si,1 |
do4 fa, sol,2 |
do4 do' la r16 fa sol la |
sib2 re4. re8 |
mib4. mib8 do4 r16 do re mib |
fa4. fa8 re4 r16 re mib fa |
sol4 mib fa fa, |
sib,2. sib,8 do |
re1 |
mib |
re4 mib fa fa, |
sib,1 |

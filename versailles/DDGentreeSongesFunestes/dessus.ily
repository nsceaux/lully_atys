\clef "dessus" sib''2~ sib''8 sib''16 la'' sol'' fa'' mib'' re'' |
mib''4. do''8 fa''4. fa''8 |
fa''4. re''8 sol''4. sol''8 |
sol''4. fa''8 fa''2~ |
fa''4 r16 fa'' mib'' re'' mib''2~ |
mib''4 r16 mib'' re'' do'' re''2~ |
re''4 r16 do'' re'' la' sib'4 sol' |
re''4. re''8 re''4. mib''8 |
fa''4. fa''8 fa''4. mib''16 fa'' |
sol''4. sol''8 do''4. do''8 |
do''2 lab''4. lab''8 |
lab''4. sol''8 sol''4. lab''8 |
mi''2 fa''4. fa''8 |
fa''4. sol''8 mi''4. fa''8 |
fa''1 |
do''4. do''8 do''4. re''8 |
mib''4. do''8 sol''4. sol''8 |
sol''4 lab''8 sol'' fa'' mib'' re'' do'' |
si'4. si'8 si'4. do''8 |
re''4. mib''8 fa'' re'' sol'' re'' |
mib''4. re''8 re''4. do''8 |
do''4 r16 do'' re'' mib'' fa''4. fa''8 |
re''4. re''8 sib'4. re''8 |
sol'4 r16 sol' la' sib' do''4. do''8 |
la'4 r16 la' sib' do'' re''4. re''8 |
sib'4. do''8 la'4. sib'8 |
<>^\markup\center-align\line\tiny {
  Ms.Colmar : \score {
    \new RhythmicStaff \with { \override StaffSymbol.line-count = #0 }
    { \teenyQuote sib'8[ do''16 re''] mib''[ fa'' sol''16 la''] }
    \layout { \quoteLayout }
  }
}
sib'16[ do'' re'' mib''] fa''8 sol''16 la'' sib''4. sib''8 |
sib''4. fa''8 lab''4. sib''8 |
sol''2~ <>^\markup\center-align\line\tiny {
  Ms.Colmar : \score {
    \new RhythmicStaff \with { \override StaffSymbol.line-count = #0 }
    { \teenyQuote sol''8[ fa''16 mib'' re'' do'' sib'] }
    \layout { \quoteLayout }
  }
} sol''8 fa''16 mib'' re''8[ do''16 sib'] |
fa''4. do''8 do''4. sib'8 |
sib'1 |

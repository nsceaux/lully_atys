<<
  \tag #'(vdessus basse) {
    \clef "vdessus" la'4. si'8 do''4 |
    re'' si'2 |
    do'' do''4 |
    si'4. si'8 do''4 |
    la' si'2 |
    sold'2. |
    la'4. la'8 re''4 |
    si' mi''2 |
    dod'' dod''4 |
    re''4. re''8 re''4 |
    re'' dod''2 |
    re''2. |
    do''!4. re''8 do''4 |
    si' la'2 |
    sold'2 sold'4 |
    do''4. si'8 la'4 |
    si' sold'2 |
    la'2. |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" do'4. re'8 mi'4 |
    fa' re'2 |
    mi' mi'4 |
    re'4. re'8 mi'4 |
    do' re'2 |
    si2. |
    do'4. do'8 fa'4 |
    re' sol'2 |
    mi' mi'4 |
    fa'4. sol'8 la'4 |
    sol'8[ fa'] mi'2 |
    re'2. |
    mi'4. fa'8 mi'4 |
    re' do'2 |
    si si4 |
    mi'4. re'8 do'4 |
    re' si2 |
    la2. |
  }
>>

Ja -- mais un cœur vo -- la -- ge
Ne trouve un heu -- reux sort,
Il n'a point l'a -- van -- ta -- ge
D'es -- tre long -- temps au port,
Il cherche en -- cor l'o -- ra -- ge
Au mo -- ment qu'il en sort.

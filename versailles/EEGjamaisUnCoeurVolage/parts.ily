\piecePartSpecs
#`((basse-continue #:indent 0
                   #:system-count 3)
   (basse-viole #:indent 0
                #:system-count 3)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers "Un Dieu de Fleuve & une Divinité de Fontaine"
    \livretVerse#6 { Jamais un cœur volage }
    \livretVerse#6 { Ne trouve un heureux sort, }
    \livretVerse#6 { Il n’a point l’avantage }
  }
  \column {
    \null
    \livretVerse#6 { D’estre long-temps au port, }
    \livretVerse#6 { Il cherche encor l’orage }
    \livretVerse#6 { Au moment qu’il en sort. }
  }
}#}))

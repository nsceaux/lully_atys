\notesSection "Acteurs du prologue"
\markuplist\abs-fontsize-lines #8 \with-line-width-ratio #0.8
\override-lines #'(column-padding . 20)
\page-columns-title \act\line { ACTEURS DU PROLOGUE } {
  \character-ambitus\smallCaps Le Temps "vbasse-taille" { sol, mib' }
  \character-ambitus\smallCaps La Deesse Flore "vbas-dessus" { mi' fa'' }
  \character-ambitus\smallCaps Un Zephir "vhaute-contre" { sol sol' }
  \character-ambitus\wordwrap { \smallCaps Melpomene, \smaller\italic { muse tragique } }
  "vbas-dessus" { mi' fa'' }
  \character-ambitus\smallCaps La Déesse Iris "vbas-dessus" { fa' mi'' }

  \choir-ambitus\column {
    \wordwrap { Les douze Heures du jour. }
    \null
    \wordwrap { Les douze Heures de la nuit. }
    \null
    \wordwrap { Troupe de Nymphes chantantes de la suite de Flore. }
    \null
    \wordwrap { Heros chantants de la suite de Melpomene. }
  }
  #'("vdessus" "vhaute-contre" "vtaille" "vbasse") {
    { sol' sol'' }
    { sib sib' }
    { mi fa' }
    { sol, mib' }
  }
  \column {
    Suivans de Flore dançans
    \null
    Nymphes dançantes
    \null
    \wordwrap {
      Heros combattants et dansants de la suite de Melpomene.
      Hercule, Antæe, Castor, Pollux, Lyncée, Idas, Eteocle, Polinice.
    }
  }
}

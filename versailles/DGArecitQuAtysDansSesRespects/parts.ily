\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix"
                   #:indent 0)
   (basse-viole #:score-template "score-basse-viole-voix"
                #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Cybele
    \livretVerse#12 { Qu’Atys dans ses respects mesle d’indifference ! }
    \livretVerse#8 { L’Ingrat Atys ne m’ayme pas ; }
    \livretVerse#12 { L’Amour veut de l’amour, tout autre prix l’offence, }
    \livretVerse#12 { Et souvent le respect et la reconnoissance }
    \livretVerse#8 { Sont l’excuse des cœurs ingrats. }
    \livretPers Melisse
    \livretVerse#7 { Ce n’est pas un si grand crime }
    \livretVerse#7 { De ne s’exprimer pas bien, }
    \livretVerse#8 { Un cœur qui n’ayma jamais rien }
    \livretVerse#8 { Sçait peu comment l’amour s’exprime. }
    \livretPers Cybele
    \livretVerse#12 { Sangaride est aymable, Atys peut tout charmer, }
    \livretVerse#8 { Ils tesmoignent trop s’estimer, }
    \livretVerse#12 { Et de simples parents sont moins d’intelligence : }
    \livretVerse#8 { Ils se sont aymez dès l’enfance, }
  }
  \column {
    \null
    \livretVerse#8 { Ils pourroient enfin trop s’aymer. }
    \livretVerse#12 { Je crains une amitié que tant d’ardeur anime. }
    \livretVerse#8 { Rien n’est si trompeur que l’estime : }
    \livretVerse#6 { C’est un nom supposé }
    \livretVerse#12 { Qu’on donne quelquefois à l’amour desguisé. }
    \livretVerse#12 { Je prétens m’esclaircir leur feinte sera vaine. }
    \livretPers Melisse
    \livretVerse#12 { Quels secrets par les Dieux ne sont point penetrez ? }
    \livretVerse#8 { Deux cœurs à feindre preparez }
    \livretVerse#6 { Ont beau cacher leur chaîne, }
    \livretVerse#6 { On abuse avec peine }
    \livretVerse#8 { Les Dieux par l’Amour esclairez. }
    \livretPers Cybele
    \livretVerse#12 { Va, Melisse, donne ordre à l’aymable Zephire }
    \livretVerse#12 { D’accomplir promptement tout ce qu’Atys desire. }
  }
}#}))

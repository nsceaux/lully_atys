\score {
  <<
    \new ChoirStaff \with { \forceGroupBracket \haraKiriFirst } <<
      \new Staff \with { \cybeleInstr } \withLyrics <<
        \global \keepWithTag #'cybele \includeNotes "voix"
      >> \keepWithTag #'cybele \includeLyrics "paroles"
      \new Staff \with { \melisseInstr } \withLyrics <<
        \global \keepWithTag #'melisse \includeNotes "voix"
      >> \keepWithTag #'melisse \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
        \modVersion { s1*11 s1. s1*4 s1. s1*3 s2\break }
      >>
    >>
  >>
  \layout { }
  \midi { }
}

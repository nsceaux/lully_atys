<<
  %% Cybèle
  \tag #'(cybele basse) {
    \tag #'basse \cybeleMark
    \tag #'cybele \cybeleClef
    r4 r8 mi'' do''8. do''16 do''8. do''16 |
    la'2 fa'8. fa'16 fa'8 fa'16 mi' |
    mi'2 mi'4 r |
    r4 la' do''4. re''8 |
    mi''4. si'8 si'4. do''8 |
    la'2. re''4 |
    re''2 dod''4 dod''8 re'' |
    re''4. la'8 la'4. la'8 |
    si'4 do'' do''( si') |
    do''2 mi'4. fad'8 |
    sol'2 sold'4 la' |
    si'2 do''4 do''8 do'' do''4 si' |
    la'4 la' la' si' |
    do'' do''8 re'' re''4. mi''8 |
    mi''2 sold'4. sold'8 |
    la'2 la'4 sol' |
    fad'2 re''4 re''8 re'' do''4 si' |
    do''4 do'' re'' mi'' |
    fa'' re''2 r8 do'' |
    do''2( si'4.) la'8 |
    la'2 
    << \tag #'cybele { r2 | R2*25 | r4 } { s2 | s2*25 | s4 } >>
    \tag #'basse \cybeleMark r8 do''16 mi'' do''8 do''16 do'' |
    la'8. la'16 re''8. re''16 si'8. si'16 |
    sold'4 mi''16 mi'' mi'' si' do''8 do''16 do'' |
    la'8 la'16 la' la'8 si'16 do'' re''8 re'' la'16 la' si' do'' |
    si'8 si' re''16 re'' si' do'' la'8 la'16 sol' |
    fad'?8 fad' si'16 si' dod'' red'' mi''4 mi''8 red'' |
    mi''4 r8 mi'' dod''16 dod'' dod'' dod'' |
    re''8. re''16 re''8. re''16 re''8. mi''16 |
    fa''8 fa'' la'16 la' la' si' do''8 re''16 mi'' |
    la'8 la' r re''16 re'' si'8 do''16 re'' |
    sold'8. si'16 do''8. do''16 re''8 mi'' |
    fa'' re''16 do'' si'4 si'8. do''16 |
    la'2 r4 |
    r8 mi'16 mi' mi'8 mi'16 fa' sol'8. sol'16 do'' do'' do'' do'' |
    << \tag #'basse { si'16 si' s8 } \tag #'cybele { si'8 si' } >>
    << \tag #'cybele { r4 r2 | R1*20 | r2 } { s2. s1*20 s2 } >>
    \tag #'basse \cybeleMark do''4. do''8 |
    mi' mi' r fad' sol' sol'16 sol' sol'8 fa'?16 mi' |
    fa'4 fa' r8 re''16 do'' si'8 do''16 re'' |
    sold'4 do''8 re''16 mi'' si'8. do''16 |
    la'4 la' r2 |
    R1 |
  }

  %% Mélisse
  \tag #'(melisse basse) {
    << { s1*11 s1. s1*4 s1. s1*3 s2 } \tag #'melisse { \melisseClef R1*11 R1. R1*4 R1. R1*3 r2 } >>
    \tag #'basse \melisseMark  do''4 re'' |
    mi'' si' do'' re'' |
    do'' la' do'' si' |
    la' sol' fad'4. mi'8 |
    mi'2 |
    mi'2. si'4 |
    do'' do''8 si' la'4 si'8 do'' |
    si'4. si'8 do''4 re'' |
    mi''4. fa''8 re''4. sol''8 |
    mi''4 do'' r mi'' |
    re'' re''8 do'' si'4 si'8 la' |
    sold'2 r4 mi'' |
    fa'' re'' si' sold' |
    la' si' si'2 |
    la'4
    << { s2 s2.*2 s1 s2. s1 s2.*7 s1 s8 }
      \tag #'melisse { r2 R2.*2 R1 R2. R1 R2.*7 R1 r8 } >>
    \tag #'basse \melisseMark sol'16 sol' do''8 do''16 re'' mi''8 mi''16 fa'' re''8 re''16 mi'' |
    do''2 r |
    r4 mi'' mi'' si' |
    do''4. do''8 si'4. la'8 |
    sold'2. la'4 |
    si' do'' re'' si' |
    do''8[ re'' do''] si'( la'2) |
    r4 mi' mi'4. fad'8 |
    sol'4 sol' la'( sol'8) la' |
    si'2. do''4 |
    re'' mi'' fa'' re'' |
    mi''8[ fa'' mi''] re''( do''4) sol'8 la' |
    si'2 si'4 do'' |
    la'2 la'4 fa'' |
    re''2 re''4 mi'' |
    fa''2 mi''4. re''8 |
    re''2 r4 la'8 si' |
    do''2 do''4 re'' |
    si'2 si'4 mi'' |
    fa''2 si'4. si'8 |
    do''2 si'4. la'8 |
    la'4 r
    \tag #'melisse { r2 R1*2 R2. R1 }
  }
>>

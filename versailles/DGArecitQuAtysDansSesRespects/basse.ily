\clef "basse" la,1~ |
\once\tieDashed la,~ |
la, |
la |
sold2 sol |
fad fa |
mi la4 la, |
re4. mi8 fad2 |
sol4 do sol,2 |
do1 |
si,2 mi4 la, |
sold,2 la,2. la,4 |
re4. re'8 do'4. si8 |
la4 sol fa2 |
mi2. re4 |
dod1 |
re2 si, mi |
la,4 la8 sol fa4 mi |
re4. mi8 fa4 re |
mi2 mi, |
la, la,4 si, |
do4 re mi mi, |
la,2 la4 sol |
fad mi si si, |
mi8 re do si, |
mi4 fad? sold? mi |
la2 re'4 re |
sol fa mi re |
do fa, sol,2 |
do4. re8 mi4 do |
fa4. mi8 re2 |
mi re4 do |
re2 mi |
fa4 re mi mi, |
la,2. | \allowPageTurn
fa2 re4 |
mi2 la,4 |
re4. do8 si,4 fad, |
sol, sol la |
si2 sol8 la si si, |
mi2 la8 sol |
fa2 fa8 mi |
re2 la,4 |
fa2 re4 |
mi la8 sol fa mi |
re4 mi mi, |
la,2 si,4 |
do2 si,4 la, |
sol,8 sol mi8. re16 do8 fa, sol,4 |
do2 do'8 re' do' si |
la2 sold |
la sol!4 fa |
mi2. do4 |
si, la, sold,2 |
la, la4 la8 si |
do'1 |
si2 la |
sol2. mi4 |
si, do fa, sol, |
do4. re8 mi4. fa8 |
sol2. mi4 |
fa2. re4 |
sib2. sol4 |
la2 la, |
re4 re8 mi fa4 re |
la4 la,8 si, do4 la, |
mi2. do4 |
re1 |
do4 re mi mi, |
la,1~ |
la,2 mi4 dod |
re1 |
mi4 do8 si,16 la, mi8 mi, |
la,2~ la,8 la, sol, fad,?8 |
mi,1 |


\key la \minor
\time 4/4 \midiTempo #80 s1*2
\time 2/2 \midiTempo #160 s1*9
\time 3/2 s1.
\time 2/2 s1*4
\time 3/2 s1.
\time 4/4 \midiTempo #80 s1*3
\digitTime \time 2/2 \midiTempo #160 s2 \bar ".|:" s2 s1*3 \alternatives { \measure 2/4 s2 } { \measure 2/2 s1 } s1*8
\digitTime\time 3/4 \midiTempo #80 s2.*3
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.*7
\time 4/4 s1*2
\time 2/2 \midiTempo #160 s1*20
\time 4/4 \midiTempo #80 s1*3
\digitTime\time 3/4 s2.
\time 4/4 s1*2 \bar "|."

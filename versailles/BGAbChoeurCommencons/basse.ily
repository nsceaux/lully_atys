\clef "basse"
\twoVoices #'(basse basse-continue tous) <<
  { r4 r8 \tag #'tous <>^"Tous" do8 do4 | }
  { do2 do4 | }
>>
sol4. fa8 mi4 |
re2 re4 |
la2 sol4 |
fa2. |
mi2 do4~ |
do8 re mi4 mi, |
la,2. ~ |
la,~ |
la,~ |
<<
  \tag #'(basse tous) {
    \tag #'tous \voiceOne
    la,2 la4 | re'4. re'8 do'4 |
    \tag #'tous \oneVoice
  }
  \tag #'basse-continue { la,2 la4 | re'2 do'4 | }
  \tag #'tous \new Voice { \voiceTwo la,2 la4 | re'2 do'4 | }
>>
si2 sol4 |
do'2 la4 |
re'2 sol4 |
do re2 |
sol,2.~ |
sol,~ |
sol,~ |
sol,2 sol4 |
do'2 do4 |
fa2 fa4 |
sib2 sib4~ |
sib8 sol la2 |
re2.~ |
\once\tieDashed re~ |
\once\tieDashed re~ |
re2 re4 |
sol2 sol4 |
la2 la4 |
si2 si4 |
do'2 do'4~ |
do'8 fa do2 |
sol2.~ |
sol~ |
\once\tieDashed sol~ |
sol2 fa4 |
mi4. re8 do4 |
fa2 re4 |
sol2 mi4 |
la2 mi4~ |
mi8 fa sol2 |
do2.~ |
do |
sol2 fa4 |
mi4. re8 do4 |
fa2 re4 |
sol2 mi4 |
la2 mi4 |
fa sol2 |
<<
  \tag #'basse { do4 r r2 | R1. | R1 | R2.*7 | r4 r }
  \tag #'(basse-continue tous) {
    do4. \tag #'tous <>^"B.C." do8 sol4 sol8 mi |
    fa2. fa4 re2 |
    sol2. sol4 |
    do'4 do8 re mi fa |
    sol2 sol,4 |
    re4. mi8 fa re |
    la4 sol fa |
    mi la fa |
    sol2 do4 |
    fa, sol,2 |
    do2 \tag #'tous <>^"Tous" %\allowPageTurn
  }
>> do4 |
fa2. |
re2 re4 |
la2 la,4 |
mi2 mi4 |
do2 do4 |
fa2 re4 |
mi4 mi,2 |
la,2 la,4 |
re2 re4 |
sol2. |
la2 la4 |
si2 si4 |
do'2 la4 |
sib2 sol4 |
la2 re4 |
la,2. |
re2 re4 |
sol2 sol4 |
mi2 fa4 |
re re do4 |
sol4 sol,2 |
do do4 |
si,8 la, si, sol, la, si, |
do si, do re mi fa |
sol4 fa8 mi re do |
si,2. |
do4 sol,4. sol8 |
do'2. |
sib2 sib4 |
la2. |
sol2 sol4 |
fa2. |
mi2 mi4 |
re2. |
do2 do4 |
do4 si,4. do8 |
re2 re,4 |
sol,4 sol sol |
re re8 mi fa re |
la4 la la |
mi mi8 fa sol mi |
fa mi fa sol la si |
do'2 la4 |
re'2 re4 |
sol2. |
mi4. fa8 sol mi |
fa2 re4 |
sol2 do4 |
sol,2. |
do2 do4 |
si,8 la, si, sol, la, si, |
do si, do re mi fa |
sol4 fa8 mi re do |
si,2. |
do4 sol,4. sol8 |
do'2. |
sib2 sib4 |
la2. |
sol2 sol4 |
fa2 fa4 |
mi2 fa4 |
re2 do4 |
sol4 sol,2 |
do <<
  \tag #'basse { r4 | \allowPageTurn R2.*10 | \allowPageTurn }
  \tag #'(basse-continue tous) {
    \tag#'tous <>^"B.C." do4 |
    re2. |
    mi2 la,4 |
    mi,2. |
    la,4 la2 |
    sib2 sol4 |
    la la,2 |
    re2 re4 |
    sol2 mi4 |
    fa re do |
    sol sol,2 |
  }
>>
\twoVoices #'(basse basse-continue tous) <<
  { r4 r \tag #'tous <>^"Tous" r8 do | }
  { do2 do4 | }
>>
sol2. |
do'2 si4 | la2. |
sol2 <<
  \tag #'basse { r4 | \allowPageTurn R2.*9 \allowPageTurn | r4 r }
  \tag #'(basse-continue tous) {
    \tag#'tous <>^"B.C." sol4 |
    mi2 mi4 |
    re2 sol4 |
    do2. |
    si,4 la,2 |
    sol, sol8 fa |
    mi4 re do |
    si,2. |
    do2 fad,4 |
    sol, re,2 |
    sol,2
  }
>> \tag #'tous <>^"Tous" sol4 |
mi2 fa4 |
re2 do4 |
sol4 sol,2 |
<<
  \tag #'basse { do2 r | \allowPageTurn R1*7 \allowPageTurn | r2 }
  \tag #'(basse-continue tous) {
    << { s2 \tag #'tous <>^"B.C." } do1 >> |
    sold,1 |
    la,~ |
    la,4 sol, fa,2 |
    mi, mi4 mi |
    la,2. la,4 |
    mi2 si, |
    do1 |
    re2 \tag #'tous <>^"Tous" \allowPageTurn
  }
>> re'4 re' |
sib2. sib4 |
fad2. sol4 |
mib1 |
re4. re8 re mi fad re |
sol4 sol8 sol sol la si sol |
do'2 do8 re mi fa |
sol2 sol4 sol |
fa2. fa4 |
mi2. mi4 |
re1 |
do2. do4 |
si,1 |
la,1 |
sol,2~ sol,8 la, si, do |
re2 re8 mi fa re |
la4 la8 la la8 si do' la |
re'2 sol |
re2 re8 mi fa re |
sol2 do |
sol,4. sol8 sol8 la si sol |
do'2 do |
fa2. |
re2 re4 |
la2 la,4 |
mi2 mi4 |
do2 do4 |
fa2 re4 |
mi4 mi,2 |
la,2 la,4 |
re2 re4 |
sol2. |
la2 la4 |
si2 si4 |
do'2 la4 |
sib2 sol4 |
la2 re4 |
la,2. |
re2 re4 |
sol2 sol4 |
mi2 fa4 |
re re do4 |
sol4 sol,2 |
do do4 |
si,8 la, si, sol, la, si, |
do si, do re mi fa |
sol4 fa8 mi re do |
si,2. |
do4 sol,4. sol8 |
do'2. |
sib2 sib4 |
la2. |
sol2 sol4 |
fa2. |
mi2 mi4 |
re2. |
do2 do4 |
do4 si,4. do8 |
re2 re,4 |
sol,4 sol sol |
re re8 mi fa re |
la4 la la |
mi mi8 fa sol mi |
fa mi fa sol la si |
do'2 la4 |
re'2 re4 |
sol2. |
mi4. fa8 sol mi |
fa2 re4 |
sol2 do4 |
sol,2. |
do2 do4 |
si,8 la, si, sol, la, si, |
do si, do re mi fa |
sol4 fa8 mi re do |
si,2. |
do4 sol,4. sol8 |
do'2. |
sib2 sib4 |
la2. |
sol2 sol4 |
fa2 fa4 |
mi2 fa4 |
re2 do4 |
sol4 sol,2 |
do2. |

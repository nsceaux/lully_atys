\score {
  \new StaffGroupNoBar <<
    \new Staff \with { \tinyStaff \haraKiriFirst } \withTinyLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse \includeLyrics "paroles"
    \new StaffGroupNoBracket <<
      \new Staff \with { \bvInstr \haraKiriFirst } <<
        \global \keepWithTag #'basse \includeNotes "basse"
        { s2.*50 s1\allowPageTurn s1. s1 s2.*7\allowPageTurn
          s2.*65 \allowPageTurn s2.*10\allowPageTurn s2.*5 \allowPageTurn
          s2.*9\allowPageTurn s2.*4 s1\allowPageTurn }
      >>
      \new Staff \with { \bcInstr } <<
        \global \keepWithTag #'basse-continue \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout { short-indent = \indent }
}
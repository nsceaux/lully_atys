% Atys et Sangaride.
\tag #'(sangaride atys) {
  -sons.
}
% Choeur
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Com -- men -- çons, com -- men -- çons nos jeux et nos chan -- sons, et nos chan -- sons.
  Com -- men -- çons, com -- men -- çons Nos jeux, nos jeux et nos chan -- sons.
  Com -- men -- çons, com -- men -- çons Nos jeux et nos chan -- sons.
  Com -- men -- çons, com -- men -- çons Nos jeux, nos jeux et nos chan -- sons.
  Com -- men -- çons, com -- men -- çons Nos jeux, nos jeux et nos chan -- sons.
  Com -- men -- çons, com -- men -- çons Nos jeux, nos jeux et nos chan -- sons.
}
% Atys et Sangaride.
\tag #'(sangaride basse) {
  Il est temps que cha -- cun fasse é -- cla -- ter son ze -- le.
  \tag #'basse { Ve -- nez, } Ve -- nez, rei -- ne des dieux,
  Ve -- nez, fa -- vo -- ra -- ble Cy -- be -- le.
}
\tag #'(atys) {
  Il est temps que cha -- cun fasse é -- cla -- ter son ze -- le.
  Ve -- nez, rei -- ne des dieux,
  Ve -- nez, Ve -- nez, fa -- vo -- ra -- ble Cy -- be -- le.
}
% Choeur
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Ve -- nez, rei -- ne des dieux,
  Ve -- nez, Ve -- nez, fa -- vo -- ra -- ble Cy -- be -- le.
  Ve -- nez, rei -- ne des dieux,
  Ve -- nez, Ve -- nez, fa -- vo -- ra -- ble Cy -- be -- le.
  Ve -- nez, fa -- vo -- ra -- ble Cy -- be -- le.
  Ve -- nez, rei -- ne des dieux, ve -- nez,
  Ve -- nez, fa -- vo -- ra -- ble Cy -- be -- le,
  Fa -- vo -- ra -- ble Cy -- be -- le.
  Ve -- nez, Ve -- nez, rei -- ne des dieux,
  Ve -- nez, Ve -- nez, fa -- vo -- ra -- ble Cy -- be -- le.
  Ve -- nez, Ve -- nez, rei -- ne des dieux, ve -- nez,
  Ve -- nez, fa -- vo -- ra -- ble Cy -- be -- le.
}
% Atys
\tag #'(atys basse) {
  Quit -- tez vos -- tre cour im -- mor -- tel -- le,
  Choi -- sis -- sez ces lieux for -- tu -- nez
  Pour vos -- tre de -- meure é -- ter -- nel -- le.
}
% Choeur
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Ve -- nez, rei -- ne des dieux, ve -- nez.
}
% Sangaride
\tag #'(sangaride basse) {
  La ter -- re sous vos pas va de -- ve -- nir plus bel -- le
  Que le se -- jour des dieux que vous a -- ban -- don -- nez.
}
% Choeur
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Ve -- nez, fa -- vo -- ra -- ble Cy -- be -- le.
}
% Atys et Sangaride
\tag #'(sangaride atys basse) {
  Ve -- nez voir les au -- tels qui vous sont des -- ti -- nez.
  Ve -- nez voir les au -- tels qui vous sont des -- ti -- nez.
}
%%%%
% Atys, Sangaride, Idas, Doris, et les choeurs.
\tag #'(vdessus sangaride doris) {
  E -- cou -- tez un peu -- ple fi -- del -- le
  Qui vous ap -- pel -- le,
  Qui vous ap -- pel -- le,
  E -- cou -- tez un peu -- ple fi -- del -- le,
  un peu -- ple fi -- del -- le
  E -- cou -- tez, e -- cou -- tez,
  E -- cou -- tez un peu -- ple fi -- del -- le,
  E -- cou -- tez un peu -- ple fi -- del -- le
  Qui vous ap -- pel -- le.
  Ve -- nez, rei -- ne des dieux, ve -- nez,
  Ve -- nez, fa -- vo -- ra -- ble Cy -- be -- le.
  Ve -- nez, rei -- ne des dieux, ve -- nez,
  Ve -- nez, fa -- vo -- ra -- ble Cy -- be -- le.
  Ve -- nez, fa -- vo -- ra -- ble Cy -- be -- le.
}
\tag #'(vhaute-contre) {
  E -- cou -- tez un peu -- ple fi -- del -- le
  Qui vous ap -- pel -- le,
  Qui vous ap -- pel -- le,
  Qui vous ap -- pel -- le,
  Qui vous ap -- pel -- le,
  Qui vous ap -- pel -- le,
  Qui vous ap -- pel -- le,
  Qui vous ap -- pel -- le,
  E -- cou -- tez, e -- cou -- tez,
  E -- cou -- tez un peu -- ple fi -- del -- le,
  E -- cou -- tez un peu -- ple fi -- del -- le
  Qui vous ap -- pel -- le.
  Ve -- nez, rei -- ne des dieux, Ve -- nez,
  Ve -- nez, fa -- vo -- ra -- ble Cy -- be -- le.
  Ve -- nez, rei -- ne des dieux, Ve -- nez,
  Ve -- nez, fa -- vo -- ra -- ble Cy -- be -- le.
  Ve -- nez, fa -- vo -- ra -- ble Cy -- be -- le.
}
\tag #'(vtaille atys) {
  E -- cou -- tez un peu -- ple fi -- del -- le
  Qui vous ap -- pel -- le,
  Qui vous ap -- pel -- le,
  E -- cou -- tez un peu -- ple fi -- del -- le,
  Un peu -- ple fi -- del -- le
  E -- cou -- tez, e -- cou -- tez,
  E -- cou -- tez un peu -- ple fi -- del -- le,
  E -- cou -- tez un peu -- ple fi -- del -- le
  Qui vous ap -- pel -- le.
  Ve -- nez, rei -- ne des dieux, Ve -- nez,
  Ve -- nez, fa -- vo -- ra -- ble Cy -- be -- le.
  Ve -- nez, rei -- ne des dieux, Ve -- nez,
  Ve -- nez, fa -- vo -- ra -- ble Cy -- be -- le.
  Ve -- nez, fa -- vo -- ra -- ble Cy -- be -- le.
}
\tag #'(vbasse idas) {
  E -- cou -- tez un peu -- ple fi -- del -- le
  Qui vous ap -- pel -- le, qui vous ap -- pel -- le,
  E -- cou -- tez un peu -- ple fi -- del -- le,
  Un peu -- ple fi -- del -- le
  Qui vous ap -- pel -- le,
  Qui vous ap -- pel -- le,
  Qui vous ap -- pel -- le,
  Qui vous ap -- pel -- le,
  Qui vous ap -- pel -- le.
  Ve -- nez, rei -- ne des dieux, Ve -- nez,
  Ve -- nez, fa -- vo -- ra -- ble Cy -- be -- le.
  Ve -- nez, rei -- ne des dieux, Ve -- nez,
  Ve -- nez, fa -- vo -- ra -- ble Cy -- be -- le.
  Ve -- nez, fa -- vo -- ra -- ble Cy -- be -- le.
}
\tag #'(vdessus sangaride doris vhaute-contre atys vtaille idas vbasse) {
  Ve -- nez, rei -- ne des dieux, ve -- nez,
  Ve -- nez, fa -- vo -- ra -- ble Cy -- be -- le,
  fa -- vo -- ra -- ble Cy -- be -- le.
  Ve -- nez, ve -- nez, rei -- ne des dieux, ve -- nez,
  Ve -- nez, fa -- vo -- ra -- ble Cy -- be -- le.
  Ve -- nez, ve -- nez, rei -- ne des dieux, ve -- nez,
  Ve -- nez, fa -- vo -- ra -- ble Cy -- be -- le.
}

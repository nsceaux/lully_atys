\clef "dessus"
<<
  \tag #'violons {
    r4 r8 mi'' mi''4 |
    re''4. re''8 mi''4 |
    fa''2 fa''4 |
    mi''2 mi''4 |
    la''4. sold''8 la''4 |
    sold''2 la''4~ |
    la''8 si'' sold''2 |
    la''2 r4 |
    R2.*2
    r4 r8 la'' la''4 |
    fad''4. fad''8 fad''4 |
    sol''2 sol''4 |
    mi''2 la''4 |
    fad''2 sol''4~ |
    sol''8 la'' fad''2 |
    sol''2 r4 |
    R2.*2 |
    r4 r8 re''8 re''4 |
    mi''4. re''8 mi''4 |
    fa''2 fa''4 |
    re''2 re''4 |
    mi'' dod''2 |
    re''2 r4 |
    R2.*2 |
    r4 r8 fad'' fad''4 |
    sol''4. sol''8 sol''4 |
    sol''2 fa''4 |
    fa''2 fa''4 |
    mi''2 mi''4 |
    fa'' mi''2 |
    re''2 r4 |
    R2.*2 |
    r4 r8 re''8 re''4 |
    mi''4. fa''8 sol''4 |
    la''2 fa''4 |
    re''2 sol''4 |
    mi''2 mi''4 |
    re'' re''2 |
    do''2 r4 |
    R2. |
    r4 r8 re''8 re''4 |
    mi''4. fa''8 sol''4 |
    la''2 fa''4 |
    re''2 sol''4 |
    mi''2 mi''4 |
    re'' re''2 |
    do''4 r4 r2 | \allowPageTurn
  }
  \tag #'(hautbois hautbois1 hautbois2) {
    R2.*7 |
    r4 r8 s4. |
    s2.*3 |
    s2 r4 |
    R2.*4 |
    r4 r8 s4. |
    s2.*2 |
    s2 r4 |
    R2.*4 |
    r4 r8 s4. |
    s2.*3 |
    s2 r4 |
    R2.*4 |
    r4 r8 s4. |
    s2.*2 |
    s2 r4 |
    R2.*5 |
    r4 s8 s4. |
    s2. |
    s2 r4 |
    R2.*5 |
    R1 |
  }
  \tag #'(hautbois1 hautbois) \new Voice {
    \tag #'hautbois \voiceOne
    s2.*7 |
    s4 s8 mi''8 mi''4 |
    fa''4 sol''8 la'' sol'' fa'' |
    mi''4. mi''8 fa''4~ |
    fa''8 sol'' mi''4. re''8 |
    re''2 s4 |
    s2.*4 |
    s4 s8 re''8 mi''4 |
    fa''4 sol''8 la'' sol'' fa'' |
    mi''4 fa''8 sol'' fa'' mi'' |
    re''2 s4 |
    s2.*4 |
    s4 s8 mi''8 fad''4 |
    sol'' la''8 sib'' la'' sol'' |
    fad''4. fad''8 sol'' fad'' |
    sol'' la'' fad''4. sol''8 |
    sol''2 s4 |
    s2.*4 |
    s4 s8 re''8 mi''4 |
    fa'' sol''8 la'' sol'' fa'' |
    mi''4 fa''8 sol'' fa'' mi'' |
    re''2 s4 |
    s2.*5 |
    s4 mi''8 re'' mi'' fa'' |
    sol''4 la''8 sol'' fa'' mi'' |
    re''2 s4 |
    s2.*5 |
    s1 |
  }
  \tag #'(hautbois2 hautbois) \new Voice {
    \tag #'hautbois \voiceTwo
    s2.*7 |
    s4 s8 dod''8 dod''4 |
    re''4 mi''8 fa'' mi'' re'' |
    dod''4. dod''8 re''4~ |
    re''8 mi'' dod''4. re''8 |
    re''2 s4 |
    s2.*4 |
    s4 s8 si'8 do''4 |
    re''4 mi''8 fa'' mi'' re'' |
    do''4 re''8 mi'' re'' do'' |
    si'2 s4 |
    s2.*4 |
    s4 s8 la'8 la'4 |
    sib'4 do''8 re'' do'' sib' |
    la'4. la'8 sib' la' |
    sib' do'' la'4. sol'8 |
    sol'2 s4 |
    s2.*4 |
    s4 s8 si'8 do''4 |
    re''4 mi''8 fa'' mi'' re'' |
    do''4 re''8 mi'' re'' do'' |
    si'2 s4 |
    s2.*5 |
    s4 do''8 si' do'' re'' |
    mi''4 fa''8 mi'' re'' do'' |
    si'2 s4 |
    s2.*5 |
    s1 |
  }
>>
R1. |
R1 |
<<
  \tag #'violons {
    R2.*7 | \allowPageTurn
    r4 r mi''4 |
    do''2. |
    fa''4 fa'' fa'' |
    mi''2 la''4 |
    sold''2 sold''4 |
    la'' mi'' mi'' |
    do'' do'' re'' |
    do'' si'2 |
    la'4 mi''8 fa'' sol'' mi'' |
    fa''4 fa''4. mi''8 |
    re''4 sol''8 la'' sol'' fa'' |
    mi''4 do'' do'' |
    re''2 r8 re'' |
    mi''2 fa''4 |
    re'' re'' mi'' |
    dod'' dod'' re'' |
    re''4( dod''2) |
    re''4 re''8 mi'' re'' do'' |
    si'2 r8 si' |
    do''4 do'' do'' |
    re'' re'' mi'' |
    mi''4 re''2 |
    do''8 si' do'' re'' mi'' fa'' |
    sol''2. |
    sol''4 fa''8 mi'' re'' do'' |
    si'2 do''8 si'16 do'' |
    re''8 do'' re'' mi'' fa'' sol'' |
    mi'' fa'' re''4. do''8 |
    do''2. |
    re''4 sol'' sol'' |
    sol''2 r8 fa'' |
    fa''2 r8 mi'' |
    mi''4 re'' re'' |
    re'' do'' do'' |
    do''2( si'4) |
    do''4 mi'' mi'' |
    fad'' sol''4. sol''8 |
    sol''4( fad''2) |
    sol''4 re''8 do'' re'' mi'' |
    fa''4 fa''4. fa''8 |
    mi''4 mi''8 re'' mi'' fa'' |
    sol''4 sol''4. sol''8 |
    do''4. si'8 do'' re'' |
    mi''4 mi'' la'' |
    fad''2 fad''4 |
    sol'' re''4. re''8 |
    sol''2 r8 sol'' |
    do''4 fa''4. fa''8 |
    fa''4 fa'' mi'' |
    mi''( re''2) |
    do''8 si' do'' re'' mi'' fa'' |
    sol''2.~ |
    sol''4 fa''8 mi'' re'' do'' |
    si'2 do''8 si'16 do'' |
    re''8 do'' re'' mi'' fa'' sol'' |
    mi'' fa'' re''4. do''8 |
    do''2 r8 fa'' |
    sol''2. |
    sol''4 fa'' fa'' |
    fa''2 r8 mi'' |
    mi''2 r8 re'' |
    re''4 do'' do'' |
    re'' re'' mi'' |
    mi''( re''2) |
    do''2 r4 | \allowPageTurn
    R2.*10 |\allowPageTurn
    r4 r r8 mi'' |
    re''2. |
    mi''4 mi'' re'' |
    do''2 r8 do'' |
    si'2 r4 | \allowPageTurn
    R2.*9 | \allowPageTurn
    r4 r r8 si' |
    do''4 do'' do'' |
    re'' re'' mi'' |
    mi''( re''2) |
    do''2 r | \allowPageTurn
    R1*7 | \allowPageTurn
    r2 fad''4. fad''8 |
    sol''2. sol''4 |
    la''2 re''4 re'' |
    re''2( do'') |
    re''4. fad''8 fad''( sol'') la'' fad'' |
    sol''4. sol''8 sol''4. fa''!8 |
    mi''2 mi''4. mi''8 |
    re''2 re''4. re''8 |
    re''2. re''4 |
    re''2 do''4. do''8 |
    do''2 si' |
    do''2. re''4 |
    re''2 re''4. mi''8 |
    do''1 |
    si'2 sol''4. sol''8 |
    sol''2 fa''4. fa''8 |
    mi''2. mi''4 |
    fad''2 sol''4. sol''8 |
    sol''2 fa''4. fa''8 |
    fa''2 mi''4. fa''8 |
    re''4. re''8 sol''4. fa''8 |
    mi''2 mi''4 r8 mi'' |
    do''2. |
    fa''4 fa'' fa'' |
    mi''2 la''4 |
    sold''2 sold''4 |
    la'' mi'' mi'' |
    do'' do'' re'' |
    do''( si'2) |
    la'4 mi''8 fa'' sol'' mi'' |
    fa''4 fa''4. mi''8 |
    re''4 sol''8 la'' sol'' fa'' |
    mi''4 do'' do'' |
    re''2 r8 re'' |
    mi''2 fa''4 |
    re''4 re'' mi'' |
    dod'' dod'' re'' |
    re'' dod''2 |
    re''4 re''8 mi'' re'' do'' |
    si'2 r8 si' |
    do''4 do'' do'' |
    re'' re'' mi'' |
    mi''( re''2) |
    do''8 si' do'' re'' mi'' fa'' |
    sol''2. |
    sol''4 fa''8 mi'' re'' do'' |
    si'2 do''8 si'16 do'' |
    re''8 do'' re'' mi'' fa'' sol'' |
    mi''8 fa'' re''4. do''8 |
    do''2. |
    re''4 sol'' sol'' |
    sol''2 r8 fa'' |
    fa''2 r8 mi'' |
    mi''4 re'' re'' |
    re'' do'' do'' |
    do''2 si'4 |
    do'' mi'' mi'' |
    fad''4 sol''4. sol''8 |
    sol''4( fad''2) |
    sol''4 re''8 do'' re'' mi'' |
    fa''4 fa''4. fa''8 |
    mi''4 mi''8 re'' mi'' fa'' |
    sol''4 sol''4. sol''8 |
    do''4. si'8 do'' re'' |
    mi''4 mi'' la'' |
    fad''2 fad''4 |
    sol''4 re''4. re''8 |
    sol''2 r8 sol'' |
    do''4 fa''4. fa''8 |
    fa''4 fa'' mi'' |
    mi''( re''2) |
    do''8 si' do'' re'' mi'' fa'' |
    sol''2.~ |
    sol''4 fa''8 mi'' re'' do'' |
    si'2 do''8 si'16 do'' |
    re''8 do'' re'' mi'' fa'' sol'' |
    mi'' fa'' re''4. do''8 |
    do''2 r8 fa'' |
    sol''2. |
    sol''4 fa'' fa'' |
    fa''2 r8 mi'' |
    mi''2 r8 re'' |
    re''4 do'' do'' |
    re'' re'' mi'' |
    mi''4( re''2) |
    do''2. |
  }
  \tag #'(hautbois1 hautbois2 hautbois) { R2.*100 R1*30 R2.*64 }
>>


\score {
  \new GrandStaff \with { instrumentName = "Hb" } <<
    \new Staff << \global \keepWithTag #'hautbois1 \includeNotes "dessus" >>
    \new Staff \with { \haraKiri } << \global \keepWithTag #'hautbois2 \includeNotes "dessus" >>
  >>
  \layout { indent = \largeindent }
}

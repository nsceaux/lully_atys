\score {
  <<
    \new GrandStaff \with { \hbInstr \haraKiriFirst } <<
      \new Staff <<
        { s2.*50 \startHaraKiri }
        \global \keepWithTag #'hautbois1 \includeNotes "dessus"
      >>
      \new Staff <<
        { s2.*50 \startHaraKiri }
        \global \keepWithTag #'hautbois2 \includeNotes "dessus"
      >>
    >>
    \new ChoirStaff <<
      \new Staff \with { \sangarideInstr } \withLyrics <<
        \global \keepWithTag #'sangaride \includeNotes "voix"
      >> \keepWithTag #'sangaride \includeLyrics "paroles"
      \new Staff \with { \dorisInstr \haraKiriFirst } \withLyrics <<
        \global \keepWithTag #'doris \includeNotes "voix"
      >> \keepWithTag #'doris \includeLyrics "paroles"
      \new Staff \with { \atysInstr } \withLyrics <<
        \global \keepWithTag #'atys \includeNotes "voix"
      >> \keepWithTag #'atys \includeLyrics "paroles"
      \new Staff \with { \idasInstr \haraKiriFirst } \withLyrics <<
        \global \keepWithTag #'idas \includeNotes "voix"
      >> \keepWithTag #'idas \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \with { \dchantInstr } \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \with { \hcchantInstr } \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \with { \tchantInstr } \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \with { \btchantInstr } \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new StaffGroup <<
      \new Staff \with { \dvInstr } <<
        { s2.*50 s1 s1. s1 s2.*22
          \footnoteHere#'(0 . 0) \markup { La fin du chœur manque dans F-V. Nous suivons la source de Valenciennes. } }
        \global \keepWithTag #'violons \includeNotes "dessus"
      >>
      \new Staff \with { \hcvInstr } << \global \includeNotes "haute-contre" >>
      \new Staff \with { \tvInstr } << \global \includeNotes "taille" >>
      \new Staff \with { \qvInstr } << \global \includeNotes "quinte" >>
      \new Staff \with { \bvInstr } << \global \keepWithTag #'basse \includeNotes "basse" >>
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \keepWithTag #'basse-continue \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout { }
  \midi { }
}

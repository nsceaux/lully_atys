\set Score.currentBarNumber = 32
\key do \major
\once\omit Staff.TimeSignature \digitTime\time 3/4 \midiTempo#160 s2.*50
\time 4/4 s1
\time 3/2 s1.
\digitTime\time 2/2 s1
\digitTime\time 3/4 s2.*100
\digitTime\time 2/2 s1*30
\digitTime\time 3/4 s2.*64 \bar "|."

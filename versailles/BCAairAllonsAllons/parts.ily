\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers\line { Sangaride, & Doris }
    \livretVerse#8 { Allons, allons, accourez tous, }
    \livretVerse#6 { Cybele va descendre. }
    \livretPers Sangaride
    \livretVerse#8 { Que dans nos concerts les plus doux, }
    \livretVerse#8 { Son nom sacré se fasse entendre. }
    \livretPers Atys
    \livretVerse#12 { Sur l’Univers entier son pouvoir doit s’étendre. }
    \livretPers Sangaride
    \livretVerse#12 { Les Dieux suivent ses loix et craignent son couroux. }
    \livretPers\line { Atys, Sangaride, Idas, Doris }
    \livretVerse#12 { Quels honneurs ! quels respects ne doit-on point luy rendre ? }
    \livretVerse#8 { Allons, allons, accourez tous, }
    \livretVerse#6 { Cybele va descendre. }
    \livretPers Sangaride
    \livretVerse#12 { Escoutons les oyseaux de ces bois d’alentour, }
    \livretVerse#12 { Ils remplissent leurs chants d’une douceur nouvelle. }
    \livretVerse#8 { On diroit que dans ce beau jour, }
    \livretVerse#8 { Ils ne parlent que de Cybele. }
    \livretPers Atys
    \livretVerse#12 { Si vous les écoutez, ils parleront d’amour. }
    \livretVerse#5 { Un Roy redoutable, }
    \livretVerse#5 { Amoureux, aimable, }
    \livretVerse#7 { Va devenir vostre espoux ; }
    \livretVerse#7 { Tout parle d’amour pour vous. }
    \livretPers Sangaride
    \livretVerse#12 { Il est vray, je triomphe, et j’aime ma victoire. }
    \livretVerse#12 { Quand l’Amour fait regner, est-il un plus grand bien ? }
  }
  \column {
    \livretVerse#8 { Pour vous, Atys, vous n’aimez rien, }
    \livretVerse#6 { Et vous en faites gloire. }
    \livretPers Atys
    \livretVerse#8 { L’Amour fait trop verser de pleurs ; }
    \livretVerse#8 { Souvent ses douceurs sont mortelles. }
    \livretVerse#8 { Il ne faut regarder les Belles }
    \livretVerse#8 { Que comme on voit d’aimables fleurs. }
    \livretVerse#7 { J’aime les Roses nouvelles, }
    \livretVerse#7 { J’aime les voir s’embellir, }
    \livretVerse#7 { Sans leurs épines cruelles, }
    \livretVerse#7 { J’aimerois à les cüeillir. }
    \livretPers Sangaride
    \livretVerse#8 { Quand le peril est agreable, }
    \livretVerse#8 { Le moyen de s’en allarmer ? }
    \livretVerse#8 { Est-ce un grand mal de trop aimer }
    \livretVerse#6 { Ce que l’on trouve aimable ? }
    \livretVerse#12 { Peut-on estre insensible aux plus charmans appas ? }
    \livretPers Atys
    \livretVerse#8 { Non vous ne me connoissez pas. }
    \livretVerse#12 { Je me deffens d’aimer autant qu’il m’est possible ; }
    \livretVerse#8 { Si j’aimois, un jour, par malheur, }
    \livretVerse#6 { Je connoy bien mon cœur }
    \livretVerse#6 { Il seroit trop sensible. }
    \livretVerse#12 { Mais il faut que chacun s’assemble prés de vous, }
    \livretVerse#8 { Cybele pourroit nous surprendre. }
    \livretPers\line { Idas, Atys }
    \livretVerse#8 { Allons, allons, accourez tous, }
    \livretVerse#6 { Cybele va descendre. }
  }
}#}))

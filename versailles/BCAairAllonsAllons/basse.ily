\clef "basse" sol,1 |
sol2 do |
re si,4 sol, |
do2 re4 re, |
sol,2 sol |
do re |
mi do4 la, |
re2 mi4 mi, |
la,2 mi4 re8 do |
si,2 do |
re4. re8 si,4 sol, |
do2 re4 re, |
sol,1. |
sol,1 sol2 |
la1. |
si1 sold2 |
la1 si2 |
do'4 la si2 si, |
mi1 mi4 re |
do1 si,2 |
la,1 la4 si |
do'2 fa1 |
sol2 sol,1 |
do1. |
re |
sol2. mi4 la2 |
re2 la,1 |
re2 re4. re8 |
la,2 la4. la8 |
fa2 re |
sol4 do sol,2 |
do2 do4. do8 |
sol,2 sol4. sol8 |
re2. re4 |
mi4 do re2 |
sol,1~ |\allowPageTurn
sol,4 sol sol4. sol8 |
do4 do8 si, do re mi do |
fa2 fa4. fa8 |
re2. re4 |
mi2 do |
re2 mi4 mi, |
la,2. la4 |
re4 re8 mi fad mi fad re |
sol4. sol8 sol4. sol8 |
do'4 do'8 si la sol fad mi |
re2 si, |
do re4 re, |
sol,2 sol |
fad fa |
mi1 |
re2 sol, |
re,4 re do4. si,8 |
la,2 la,4. si,8 |
do2 sold, |
la,4 re, mi,2 |
la, la |
fad2. fad4 |
sol2 fad |
mi la |
re2. re4 |
do1 |
si,2. si,4 |
la, sol, re re, |
sol,2 sol |
fad1 |
mi |
fad |
sol |
fad2 la4 la, |
re2. re4 |
do2. si,8 do |
re2 re, |
sol,2 sol |
mi si, |
do1 |
si,2 la,4. sol,8 |
fad,1 |
sol, |
sol, |
do1. |\allowPageTurn
re2 re4 |
sol2 do'8 si |
la2 sol4 |
fad2 mi4 |
re4. mi8 fad4 |
sol8 fad mi4 re |
do8 si, la,2 |
si, si8 la |
sold2. |
la2 sol4 |
fad2 re4 |
sol sol, re |
dod2 re4 |
la,2 la4 |
sib sol la |
re4. re8 mi fad |
sol2 fa4 |
mi4. re8 do4 |
fa4. sol8 fa mi |
re2 mi8 mi, |
la,4 mi,4 mi8 re |
do4. si,8 la,4 |
sold,2. |
la,2 fad,4 |
sol,2 sol4 |
si,4. do8 re si, |
do4. re8 mi do |
re4. mi8 fad re |
sol do re4 re, |
sol,4. la,8 si, do |
re4 do si, |
la, fad,2 |
sol, re8 do |
si,2 do4~ |
do re re, |
sol,2. |
sol2 sol4 |
do4. re8 mi do |
re4 re,2 |
sol, sol8 fad |
mi2 mi4 |
la2 sib4 |
sol la la, |
re2 re8 mi |
fa2 fa4 |
mi2 fad!4 |
sol4 sol,2 |
re4. mi8 re do |
si,4. la,8 sol,4 |
fad, sol,2 |
re,2. |
sol,2. | \allowPageTurn
sol2. |
fad |
mi2 do4 |
re re,2 |
sol,4 sol2 |
fad2. |
mi |
re4 re,2 |
sol,4 sol2 |
fad2. |
mi2 do4~ |
do re re, |
sol, sol2 |
fad2. |
mi |
re4 re,2 |
sol,4 sol2 |
fad re4 |
mi2 do4 |
do4 re re, |
sol,4 sol2 |
fad2. |
mi |
re4 re,2 |
sol,4 sol2 |
fad2. |
mi2 do4 |
re re,2 |
sol,4 sol2 |
fad2 re4 |
mi2 do4 |
re re,2 |
sol,4 sol2 |
fad2. |
mi2. |
re4 re,2 |
sol,4 sol2 |
fad2. |
mi2 do4 |
re re,2 |
sol,4 sol2 |
fad2. |
mi2 do4 |
re re,2 |
sol,2. | \allowPageTurn
sol,1 |
fad,2. |
sol,4 sol do2 |
re re, |
sol,1~ |
sol,4 sol fad re |
sol sib8 la sol fa! mib re |
mib4 mib8 re do re mib do |
fa2 re |
mib fa4 fa, |
sib,1~ |
sib,4 sib la fa |
sib sib8 la sol fa mib re |
do4 mib8 re do sib, la, sol, |
re2 sib, |
do re4 re, |
sol,2. fa,8 mi, |
\once\set Staff.whichBar = "|"
re,1 |

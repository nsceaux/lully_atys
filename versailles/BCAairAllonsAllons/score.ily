\score {
  <<
    \new ChoirStaff \with { \forceGroupBracket \haraKiriFirst } <<
      \new Staff \with { \sangarideInstr } \withLyrics <<
        \global \keepWithTag #'sangaride \includeNotes "voix"
      >> \keepWithTag #'sangaride \includeLyrics "paroles"
      \new Staff \with { \dorisInstr } \withLyrics <<
        \global \keepWithTag #'doris \includeNotes "voix"
      >> \keepWithTag #'doris \includeLyrics "paroles"
      \new Staff \with { \atysInstr } \withLyrics <<
        \global \keepWithTag #'atys \includeNotes "voix"
      >> \keepWithTag #'atys \includeLyrics "paroles"
      \new Staff \with { \idasInstr } \withLyrics <<
        \global \keepWithTag #'idas \includeNotes "voix"
      >> \keepWithTag #'idas \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
        \modVersion {
          s1*12 s1.\break s1.*14\break
          s1*8 s1*14\break
          s1*31 s1. s2.*36\break
          s2.*16\break
          % peut-on être insensible
          s2.*45\break
          s1 s2. s1*2\break
          % allons allons
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}

\version "2.24.0"
#(ly:set-option 'versailles #t)
\include "common.ily"

\paper {
  %% de la place sous la basse pour pouvoir noter les chiffrages
  score-markup-spacing.padding = #4
  system-system-spacing.padding = #4
  last-bottom-spacing.padding = #4
}
\layout {
  \context {
    \Lyrics
    %% de la place sous les paroles
    \override VerticalAxisGroup.nonstaff-unrelatedstaff-spacing.padding = #3
  }
}

\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header {
    title = \markup\smallCaps Atys
  }
  %% Title page
  \markup\null\pageBreak
  %% Table of contents
  \markuplist\abs-fontsize-lines #7
  \override-lines #'(use-rehearsal-numbers . #t)
  \override-lines #'(column-number . 2)
  \override-lines #'(rehearsal-number-gauge . "4-16b")
  \table-of-contents \pageBreak
  %% Livret
  \include "livret/livret.ily" \pageBreak
  %% Personnages
  \include "personnages-prologue.ily" \pageBreak
  \include "personnages.ily"
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Prologue
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bookpart {
  \paper { page-count = 3 systems-per-page = 3}
  \actn "Prologue"
  %% 0-1
  \pieceToc "Ouverture"
  \includeScore "AAAouverture"
}
\bookpart {
  \sceneDescription\markup\justify {
    Le theatre represente le palais du Temps, où ce dieu paroist au
    milieu des douze Heures du jour, & des douze Heures de la nuit.
  }
  %% 0-2
  \pieceToc\markup\wordwrap {
    \italic { En vain j’ay respecté la celebre memoire }
  }
  \includeScore "AABrecitEnVainJaiRespecte"
  %% 0-3
  \pieceToc\markup\wordwrap { \italic { Ses justes loix } }
  \includeScore "AACchoeurSesJustesLois"
}
\bookpart {
  %% 0-4
  \sceneDescription\markup\justify {
    La déesse Flore conduite par un des Zephirs s’avance avec une troupe
    de nymphes qui portent divers ornements de fleurs.
  }
  \pieceToc "Premier air pour la suite de Flore"
  \includeScore "AADairFlore"
}
\bookpart {
  %% 0-5
  \pieceToc\markup\wordwrap {
    \italic { La Saison des frimas peut-elle nous offrir }
  }
  \includeScore "AAErecitLaSaisonDesFrimas"
  %% 0-6
  \pieceToc\markup\wordwrap {
    \italic { Les Plaisirs à ses yeux ont beau se presenter }
  }
  \includeScore "AAFairChoeurLesPlaisirASesYeux"
}
\bookpart {
  %% 0-7
  \pieceToc\markup\wordwrap {
    \italic { Rien ne peut l’arrester }
  }
  \includeScore "AAFbChoeurRienNePeutLArreter"
}
\bookpart {
  \paper { page-count = 3 }
  \sceneDescription\markup\wordwrap-center {
    La suite de Flore commence des jeux "meslez de dances & de chants."
  }
  %% 0-8
  \pieceToc "Air pour la suite de Flore"
  \includeScore "AAGgavotte"
  %% 0-9
  \pieceToc\markup\wordwrap {
    \italic { Le printemps quelquefois est moins doux qu’il ne semble }
  }
  \includeScore "AAHairLePrintempsQuelqueFois"
  \sceneDescriptionBottom\markup\wordwrap-center {
    Les violons recommencent l’air précédent et le jouent
    alternativement avec les hautbois.
  }
  %% 0-9b
  \pieceTocNb "0-9b" "Air pour la suite de Flore"
  \reIncludeScore "AAGgavotte" "AAHbGavotte"
}
\bookpart {
  \sceneDescription\markup\justify {
    \smallCaps Melpomene qui est la muse qui preside à la tragedie, vient
    accompagnée d’une troupe de heros, elle est suivie d’Hercule,
    d’Antæ, de Castor, de Pollux, de Lincée, d’Idas, d’Eteocle, & de
    Polinice.
  }
  %% 0-10
  \pieceToc "Prelude pour Melpomene"
  \includeScore "AAIpreludeMelpomene"
  %% 0-11
  \pieceToc\markup {
    \italic { Retirez-vous, cessez de prevenir le Temps }
  }
  \includeScore "AAJrecitRetirezVous"
  \sceneDescription\markup\justify {
    La suite de Melpomene prend la place de la suite de Flore.
    Les heros recommencent leurs anciennes querelles.
    Hercule combat & lutte contre Antæe, Castor & Pollux combattent
    contre Lyncée & Idas, & Eteocle combat contre son frere Polynice.
  }
  %% 0-12
  \pieceToc "Air pour la suite de Melpomene"
  \includeScore "AAKairMelpomene"
}
\bookpart {
  \sceneDescription\markup\wordwrap-center {
    \smallCaps Iris, par l’ordre de Cybele, descend assise sur son arc,
    "pour accorder Melpomene & Flore."
  }
  %% 0-13
  \pieceToc "Ritournelle"
  \includeScore "AALaRitournelle"
  %% 0-14
  \pieceToc\markup\wordwrap {
    \italic { Cybele veut que Flore aujourd’huy vous seconde }
  }
  \includeScore "AALbCybeleVeutQueFlore"
}
AALcPreparonsDeNouvellesFetesFirstBar = 36
\bookpart {
  \includeScore "AALcPreparonsDeNouvellesFetes"
}
\bookpart {
  %% 0-15
  \pieceToc "Menuet"
  \includeScore "AAMmenuet"
  \sceneDescription\markup\wordwrap-center {
    Après que les violons ont joué de menuet, les voix et les
    instruments redisent \italic { “Préparons de nouvelles festes” }
    ainsi qu’il est cy-devant, puis les violons rejouent l’ouverture.
  }
}
AALcPreparonsDeNouvellesFetesFirstBar = 1
globalAALcPreparonsDeNouvellesFetes = ##f
\bookpart {
  %% 0-15b
  \pieceTocNb "0-15b" \markup\italic { Preparons de nouvelles festes }
  \reIncludeScore "AALcPreparonsDeNouvellesFetes"  "AALNpreparonsDeNouvellesFetes"
  \actEnd "FIN DU PROLOGUE"
}
\bookpart {
  \paper { page-count = 3 systems-per-page = 3}
  %% 0-16
  \pieceToc "Ouverture"
  \reIncludeScore "AAAouverture" "AANouverture"
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Acte I
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bookpart {
  \act "Acte Premier"
  \sceneDescription\markup\column {
    \wordwrap-center { La scene est en Phrygie. }
    \wordwrap-center { Le theatre represente une montagne consacrée à Cybele. }
  }
  \scene "Scene Premiere" "SCENE 1 : Atys"
  \sceneDescription\markup\smallCaps Atys.
  %% 1-1
  \pieceToc "Ritournelle"
  \includeScore "BAAritournelle"
}
\bookpart {
  %% 1-2
  \pieceToc\markup\wordwrap\italic { Allons, allons, accourez tous }
  \includeScore "BAAairAllonsAllons"
  %
  \scene "Scene II" "SCENE 2 : Atys, Idas"
  \sceneDescription\markup\smallCaps { Atys, Idas. }
  %% 1-3
  \pieceToc\markup\wordwrap\italic { Allons, allons, accourez tous }
  \includeScore "BBAairAllonsAllons"
  \paper { page-count = 6 }
}
\bookpart {
  \scene "Scene III" "SCENE 3 : Sangaride, Doris, Atys, Idas"
  \sceneDescription\markup\smallCaps { Sangaride, Doris, Atys, Idas. }
  %% 1-4
  \pieceToc\markup\wordwrap\italic { Allons, allons, accourez tous }
  \includeScore "BCAairAllonsAllons"
}
\bookpart {
  \scene "Scene IV" "SCENE 4 : Sangaride, Doris"
  \sceneDescription\markup\smallCaps { Sangaride, Doris. }
  %% 1-5
  \pieceToc\markup\wordwrap\italic { Atys est trop heureux }
  \includeScore "BDAatysEstTropHeureux"
}
\bookpart {
  \scene "Scene V" "SCENE 5 : Sangaride, Doris, Atys"
  \sceneDescription\markup\smallCaps { Sangaride, Doris, Atys. }
  %% 1-6
  \pieceToc\markup\wordwrap\italic { On voit dans ces campagnes }
  \includeScore "BEArecitOneVoitDansCesCampagnes"
  %
  \scene "Scene VI" "SCENE 6 : Sangaride, Atys"
  \sceneDescription\markup\smallCaps { Sangaride, Atys. }
  %% 1-7
  \pieceToc\markup\wordwrap\italic { Sangaride ce jour est un grand jour pour vous }
  \includeScore "BFArecitSangarideCeJour"
}
\bookpart {
  \scene "Scene VII" "SCENE 7 : Sangaride, Atys, chœur de Phrygiens"
  \markup\fontsize#2 \fill-line {
    \center-column {
      \line\smallCaps { Sangaride, Atys, Doris, Idas, les Phrygiens. }
      \line { Chœur de Phrygiens chantans. Chœur de Phrygiennes chantantes. }
      \line { Troupe de Phrygiens dançans. Troupe de Phrygiennes dançantes. }
      \fontsize#-2 \italic\wordwrap-center {
        "Dix hommes phrygiens chantans conduits par Atys."
        "Dix femmes phrygiennes chantantes conduites par Sangaride."
        "Six phrygiens dançans."
        "Six nimphes phrygiennes dançantes."
      }
    }
  }
  %% 1-8
  \pieceToc\markup\wordwrap\italic { Mais déja de ce mont sacré }
  \includeScore "BGAairMaisDejaDeCeMontSacre"
  \paper { page-count = 1 }
}
\bookpart {
  \paper { systems-per-page = 1 }
  \includeScore "BGAbChoeurCommencons"
}
\bookpart {
  %% 1-9
  \pieceToc\markup\wordwrap { Premier air pour les Phrygiens [et Phrygiennes] }
  \includeScore "BGBEntreePhrygiens"
}
\bookpart {
  %% 1-10
  \pieceToc\markup\wordwrap { Second air pour les Phrygiens [et Phrygiennes] }
  \includeScore "BGCSecondAirPhrygiens"
}
\bookpart {
  \scene "Scene VIII" "SCENE 8 : Cybele, chœur de Phrygiens"
  \sceneDescription\markup\column {
    \wordwrap-center { \smallCaps Cybele sur son char. }
    \wordwrap-center {
      La Déesse Cybele paroist sur son char, & les Phrygiens
      & les Phrygiennes luy témoignent leur joye & leur respect.
    }
  }
  %% 1-11
  \pieceToc "Prelude [pour Cybele]"
  \includeScore "BHApreludeCybele"
}
\bookpart {
  %% 1-12
  \pieceToc\markup\wordwrap\italic { Venez tous dans mon temple, et que chacun revere }
  \includeScore "BHBairChoeurVenezTousDansMonTemple"
  \sceneDescription\markup\wordwrap-center\italic\smaller {
    Cybèle portée par son char volant, se va rendre dans son temple.
    Tous les Phrygiens s’empressent d’y aller, & repetent les quatres
    derniers vers que la déesse a prononcez.
  }
  %% 1-13
  \pieceToc\markup\wordwrap\italic { Nous devons nous animer }
  \includeScore "BHCchoeurNousDevonsNousAnimer"
  \paper { min-systems-per-page = 2 }
}
\bookpart {
  %% 1-13b
  \pieceTocNb "1-13b" \markup\wordwrap { Entracte }
  \markup\fontsize#1 { Les violons jouent pour entracte le deuxième air des Phrygiens. }
  \reIncludeScore "BGCSecondAirPhrygiens" "BHDentracte"
  \actEnd "FIN DU PREMIER ACTE"
  \tocBreak
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Acte II
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bookpart {
  \act "Acte Second"
  \sceneDescription\markup\wordwrap-center {
    Le theatre change & represente le temple de Cybele.
  }
  \scene "Scene Premiere" "SCENE 1 : Celænus, Atys"
  \sceneDescription\markup\wordwrap-center {
    \smallCaps Celænus, roy de Phrigie,
    \smallCaps Atys, suivans de Celænus.
  }
  %% 2-1
  \pieceToc "Ritournelle"
  \includeScore "CAAritournelle"
}
\bookpart {
  %% 2-2
  \pieceToc\markup\wordwrap\italic {
    N’avancez pas plus loin, ne suivez point mes pas
  }
  \includeScore "CABrecitNavancezPasPlusLoin"
}
\bookpart {
  \scene "Scene II" "SCENE 2 : Cybele, Celænus"
  \sceneDescription\markup\center-column {
    \line\smallCaps { Cybele, Celænus, Melisse, }
    \line { troupe de prestresses de Cybele. }
  }
  %% 2-3
  \pieceToc "Prelude"
  \includeScore "CBAprelude"
}
\bookpart {
  %% 2-4
  \pieceToc\markup\wordwrap\italic {
    Je veux joindre en ces lieux la gloire & l’abondance
  }
  \includeScore "CBBrecitJeVeuxJoindreEnCesLieux"
}
\bookpart {
  \scene "Scene III" "SCENE 3 : Cybele, Melisse"
  \sceneDescription\markup\smallCaps { Cybele, Melisse. }
  %% 2-5
  \pieceToc\markup\wordwrap\italic {
    Tu t’estonnes, Melisse, & mon choix te surprend ?
  }
  \includeScore "CCArecitTuTetonnesMelisse"
}
\bookpart {
  \scene "Scene IV" \markup\wordwrap {
    SCENE 4 : Cybele, Atys, troupes de pleuples et de Zephirs
  }
  \markup\fontsize#2 \fill-line {
    \center-column {
      \line { Les Zephirs paroissent dans une gloire élevée & brillante. }
      \wordwrap-center {
        Les peuples differens qui sont venus à la feste de Cybele
        entrent dans le temple, & tous ensemble s’efforcent d’honorer Atys,
        qui vient revestu des habits de grand Sacrificateur.
      }
      \fontsize#-1 \italic\wordwrap-center {
        \line { Cinq Zephirs dançans dans la gloire. }
        \line { Huit Zephirs joüants du haut-bois & des cormornes, dans la gloire. }
        \line { Troupe de peuples differens chantans qui accompagnent Atys. }
        \line { Six Indiens & six Egiptiens dançans. }
      }
      \vspace#1
    }
  }
  %% 2-6
  \pieceToc\markup\wordwrap\italic { Celebrons la gloire immortelle }
  \includeScore "CDAchoeurCelebronsLaGloireImmortelle"
}
\bookpart {
  %% 2-7
  \pieceTitleToc
  \markup\wordwrap {
    [Premier \auto-footnote air] 
    \fontsize#-2 \line { Ballard 1689 : Entrée des Nations }
  } "[Premier air]"
  \includeScore "CDBentreeNations"
}
\bookpart {
  %% 2-8
  \pieceTitleToc
  \markup\wordwrap {
    Deuxième air – \auto-footnote L’Écho 
    \fontsize#-2 \line { Ballard 1689 : Entrée des Zéphirs }
  } "[Deuxième air – L’Écho]"
  \includeScore "CDCentreeZephirs"
  %% 2-9
  \pieceToc\markup\wordwrap\italic {
    Que devant vous tout s'abaisse, & tout tremble
  }
  \includeScore "CDDchoeurQueDevantVous"
}
\bookpart {
  \paper { page-count = 1 }
  %% 2-10
  \pieceToc\markup\wordwrap\italic {
    Indigne que je suis des honneurs qu’on m’adresse
  }
  \includeScore "CDEindigneQueJeSuis"
  %% 2-11
  \pieceToc\markup\wordwrap\italic { Que la puissante Cybele }
  \includeScore "CDFchoeurQueLaPuissanteCybele"
}
\bookpart {
  \paper { min-systems-per-page = 2 }
  %% 2-12
  \pieceToc\markup\wordwrap\italic {
    Que devant vous tout s'abaisse, & tout tremble
  }
  \reIncludeScore "CDDchoeurQueDevantVous" "CDGchoeurQueDevantVous"
  %% 2-13
  \pieceToc "[L’Écho]"
  \reIncludeScore "CDCentreeZephirs" "CDHentreeZephirs"
  \actEnd "FIN DU SECOND ACTE"
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Acte III
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bookpart {
  \act "Acte Troisiesme"
  \sceneDescription\markup\wordwrap-center {
    Le Theatre change & represente
    le Palais "du Sacrificateur de Cybele."
  }
  \scene "Scene Premiere" "SCENE 1 : Atys"
  \sceneDescription\markup\wordwrap-center { \smallCaps Atys seul. }
  %% 3-1
  \pieceToc\markup Ritournelle
  \includeScore "DAAritournelle"
  %% 3-2
  \pieceToc\markup\wordwrap\italic {
    Que servent les faveurs que nous fait la Fortune
  }
  \includeScore "DAArecitQueServentLesFaveurs"
}
\bookpart {
  \scene "Scene II" "SCENE 2 : Idas, Doris, Atys"
  \sceneDescription\markup\wordwrap-center\smallCaps { Idas, Doris, Atys. }
  %% 3-3
  \pieceToc\markup\wordwrap\italic {
    Peut-on icy parler sans feindre
  }
  \includeScore "DBArecitPeutOnIciParlerSansFeindre"
}
\bookpart {
  \scene "Scene III" "SCENE 3 : Atys"
  \sceneDescription\markup\wordwrap-center { \smallCaps Atys seul. }
  %% 3-4
  \pieceToc "[Prelude]"
  \includeScore "DCAprelude"
  %% 3-5
  \pieceToc\markup\wordwrap\italic {
    Nous pouvons nous flater de l’espoir le plus doux
  }
  \includeScore "DCBrecitNousPouvonsNousFlatter"
}
\bookpart {
  \scene "Scene IV" "SCENE 4 : Sommeil"
  \markup\fontsize#2 \fill-line {
    \center-column {
      \line { Le Theatre change & represente un Antre entouré de Pavots & de Ruisseaux, }
      \line { où le Dieux du Sommeil se vient rendre accompagné des Songes agreables & funestes. }
      \line {
        \smallCaps Atys dormant.
        \smallCaps { Le Sommeil, Morphée, Phobetor, Phantase, }
      }
      \line { "Les Songes heureux." "Les Songes funestes." }
      \fontsize#-2 \italic\wordwrap-center {
        \line {
          "Deux Songes joüants de la Violle."
          "Deux Songes joüants du Theorbe."
          "Six Songes joüants de la Flutte."
        }
        \line {
          "Douze Songes funestes chantants."
          "Seize Songes agreables & funestes dançans."
          "Huit Songes funestes dançants."
        }
      }
    }
  }
  %% 3-6
  \pieceToc\markup\wordwrap { Prelude du Sommeil }
  \includeScore "DDApreludeSommeil"
}
\bookpart {
  %% 3-7
  \pieceToc\markup\wordwrap\italic { Dormons, dormons tous }
  \includeScore "DDAdormons"
}
%% On supprime les notes de bas page pour la reprise du prélude
\layout { \context { \Score \remove Footnote_engraver } }
\bookpart {
  \sceneDescription\markup\justify {
    Les Songes agreables aprochent d’Atys, & par leurs chants,
    & par leurs dances, luy font connoistre l’amour de Cybele,
    & le bonheur qu’il en doit esperer.
  }
  %% 3-8
  \pieceToc\markup\wordwrap { Prelude du Sommeil }
  \includeScore "DDAcpreludeSommeil"
}
\layout { \context { \Score \consists Footnote_engraver } }
\bookpart {
  %% 3-9
  \pieceToc\markup\wordwrap\italic { Escoute, escoute Atys la gloire qui t’appelle }
  \includeScore "DDBecouteAtys"
  %% 3-10
  \pieceTocFootnote "Les Songes agreables " \markup {
    Ms. Colmar : \italic {
      “Air pour les Songes agréables qui se dit alternativement avec
      l’air de Phantase ‘Que l’Amour a d’attraits’”
    }
  }
  \includeScore "DDCentreeSongesAgreables"
  %% 3-10b
  \pieceTocNb "3-10b" \markup\wordwrap\italic { Trop heureux un amant }
  \includeScore "DDBbTropHeureuxUnAmant"
  %% 3-10c
  \pieceTocFootnoteNb "3-10c" "Les Songes agreables " \markup {
    Ms. Colmar : \italic { “Les violons reprennent l’air précédant” }
  }
  \includeScore "DDCbairSongesAgreables"
  %% 3-11
  \pieceToc\markup\wordwrap\italic { Gouste en paix chaque jour une douceur nouvelle }
  \includeScore "DDDgouteEnPaix"
  %% 3-12
  \pieceToc "Les Songes agreables"
  \includeScore "DDEairSongesAgreables"
}
\bookpart {
  \sceneDescription\markup\justify {
    Les songes funestes approchent d’Atys, & le menacent de la
    vengeance de Cybele s’il mesprise son amour, & s’il ne l’ayme pas
    avec fidelité.
  }
  %% 3-13
  \pieceToc\markup\wordwrap\italic { Garde-toy d’offencer un amour glorieux }
  \includeScore "DDFgardeToiDoffenser"
  %% 3-14
  \pieceToc "[Entrée des Songes Funestes]"
  \includeScore "DDGentreeSongesFunestes"
}
\bookpart {
  \paper { min-systems-per-page = 3 }
  %% 3-15
  \pieceToc\markup\wordwrap\italic { L’amour qu’on outrage }
  \includeScore "DDHchoeurLamourQuonOutrage"
  %% 3-16
  \pieceToc "[Deuxième entrée des] Songes funestes"
  \includeScore "DDIsongesFunestes"
  \sceneDescriptionBottom\markup\justify {
    Atys espouvanté par les Songes funestes, se resveille en sursaut,
    le Sommeil & les Songes disparoissent avec l’Antre où ils estoient,
    & Atys se retrouve dans le mesme Palais où il s’estois endormy.
  }
}
\bookpart {
  \scene "Scene V" "SCENE 5 : Atys, Cybele, Melisse"
  \sceneDescription\markup \smallCaps { Cybele, Atys, & Melisse. }
  %% 3-17
  \pieceToc\markup\wordwrap\italic { Venez à mon secours ô Dieux ! ô justes Dieux ! }
  \includeScore "DEArecitVenezAMonSecours"
}
\bookpart {
  \scene "Scene VI" "SCENE 6 : Sangaride, Cybele, Atys, Melisse"
  \sceneDescription \markup \smallCaps { Sangaride, Cybele, Atys, Melisse. }
  %% 3-18
  \pieceToc\markup\wordwrap\italic { Je sçays trop ce que je vous doy }
  \includeScore "DFArecitJaiRecoursAVotrePuissance"
}
\bookpart {
  \scene "Scene VII" "SCENE 7 : Cybele, Melisse"
  \sceneDescription\markup\smallCaps { Cybele, Melisse. }
  %% 3-19
  \pieceToc\markup\wordwrap\italic { Qu’Atys dans ses respects mesle d’indifference }
  \includeScore "DGArecitQuAtysDansSesRespects"
}
\bookpart {
  \scene "Scene VIII" "SCENE 8 : Cybele"
  \sceneDescription\markup { \smallCaps Cybele seule. }
  %% 3-20
  \pieceToc\markup\italic { Espoir si cher, & si doux }
  \includeScore "DHAespoirSiCherEtSiDoux"
}
\bookpart {
  %% 3-20b
  \pieceTocNb "3-20b" \markup\wordwrap { Entracte }
  \reIncludeScore "CDBentreeNations" "DHBentracte"
  \actEnd "FIN DU TROISIESME ACTE"
  \tocBreak
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Acte IV
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bookpart {
  \act "Acte Quatriesme"
  \sceneDescription\markup\wordwrap-center {
    Le theatre change & represente le palais du Fleuve Sangar.
  }
  \scene "Scene Premiere" "SCENE 1 : Sangaride, Doris, Idas"
  \sceneDescription\markup\smallCaps { Sangaride, Doris, Idas. }
  %% 4-1
  \pieceToc\markup\wordwrap\italic { Quoy, vous pleurez ? }
  \includeScore "EAArecitQuoiVousPleurez"
}
\bookpart {
  \scene "Scene II" "SCENE 2 : Sangaride, Celænus"
  \sceneDescription\markup\wordwrap-center {
    \smallCaps Celænus, suivans de Celænus,
    \smallCaps { Sangaride, Idas, & Doris }
  }
  %% 4-2
  \pieceToc "Prelude"
  \includeScore "EBAprelude"
}
\bookpart {
  %% 4-3
  \pieceToc\markup\wordwrap\italic {
    Belle nymphe, l’hymen va suivre mon envie
  }
  \includeScore "EBBbelleNymphe"
  \scene "Scene III" "SCENE 3 : Atys, Sangaride, Celænus"
  \sceneDescription\markup\wordwrap-center {
    \smallCaps { Celænus, Sangaride, Atys, Doris, Idas, }
    "Suivans de Celænus."
  }
  %% 4-4
  \pieceToc\markup\wordwrap\italic {
    Vostre cœur se trouble, il soûpire
  }
  \includeScore "ECArecitVotreCoeurSeTrouble"
}
\bookpart {
  \scene "Scene IV" "SCENE 4 : Sangaride, Atys"
  \sceneDescription\markup\smallCaps { Sangaride, Atys. }
  %% 4-5
  \pieceToc "Ritournelle"
  \includeScore "EDAritournelle"
  %% 4-6
  \pieceToc\markup\italic { Qu’il sçait peu son malheur }
  \includeScore "EDBrecitQuilSaitPeuSonMalheur"
}
\bookpart {
  \scene "Scene V" \markup \wordwrap {
    SCENE 5 : Le Fleuve Sangar, les Fleuves
  }
  \sceneDescription\markup\column {
    \wordwrap-center {
      \smallCaps Sangaride,
      \smallCaps Celænus,
      le dieu du fleuve \smallCaps Sangar,
      troupe de dieux de fleuves, de ruisseaux,
      et de divinitez de fontaines.
    }
    \smaller\italic\wordwrap-center {
      "Douze grands dieux de fleuves chantans."
      "Cinq dieux de fleuves joüans de la flutte."
      "Quatre divinitez de fontaines, & quatres dieux de fleuves chantants & dançants."
      "Deux petits dieux de ruisseaux chantants & dançants."
      "Quatre petits dieux de ruisseaux dançants."
      "Six grands dieux de fleuves dançants."
      "Deux vieux dieux de fleuves & deux vieilles fontaines dançantes."
    }
  }
  %% 4-7
  \pieceToc "Prelude pour les fleuves"
  \includeScore "EEAprelude"
  %% 4-8
  \pieceToc\markup\wordwrap\italic { O vous, qui prenez part au bien de ma famille }
  \includeScore "EEBOVousQuiPrenezPart"
  %% 4-9
  \pieceToc\markup\wordwrap\italic { Que l’on chante, que l’on dance }
  \includeScore "EECairQueLonChante"
  %% 4-10
  \pieceToc\markup\wordwrap\italic { Que l’on chante, que l’on dance }
  \includeScore "EECchoeurQueLonChante"
}
\bookpart {
  %% 4-11
  \pieceToc\markup\wordwrap\italic { La beauté la plus severe }
  \includeScore "EEDflutes"
  \includeScore "EEDairLaBeauteLaPlusSevere"
  %% 4-12
  \pieceToc\markup\wordwrap\italic { L’Hymen seul ne sçauroit plaire }
  \includeScore "EEEflutes"
  \includeScore "EEEairLhymenSeulNeSauraitPlaire"
  %% 4-11b
  \pieceTocNb "4-11b" \markup\wordwrap\italic { Il n’est point de resistance }
  \reIncludeScore "EEDflutes" "EEDflutesBis"
  \includeScore "EEDairIlNEstPointDeResistance"
  %% 4-12b
  \pieceTocNb "4-12b" \markup\wordwrap\italic { L’Amour trouble tout le monde }
  \reIncludeScore "EEEflutes" "EEEflutesBis"
  \includeScore "EEEairLAmourTroubleToutLeMonde"
}
\bookpart {
  %% 4-13
  \pieceToc "Menuet"
  \includeScore "EEFmenuet"
  %% 4-14
  \pieceToc\markup\wordwrap\italic { D’une constance extresme }
  \includeScore "EEGduneConstanceExtreme"
  %% 4-13b
  \pieceTocNb "4-13b" "Menuet"
  \includeScore "EEFmenuetBis"
  %% 4-14b
  \pieceTocNb "4-14b" \markup\wordwrap\italic { Jamais un cœur volage }
  \includeScore "EEGjamaisUnCoeurVolage"
}
\bookpart {
  %% 4-15
  \pieceToc "Gavotte"
  \includeScore "EEHgavotte"
  %% 4-16
  \pieceToc\markup\wordwrap\italic {
    Un grand calme est trop fascheux
  }
  \includeScore "EEIchoeurUnGrandCalmeEstTropFacheux"
}
\bookpart {
  %% 4-15b
  \pieceTocNb "4-15b" "Gavotte"
  \reIncludeScore "EEHgavotte" "EEHgavotteBis"
  %% 4-16b
  \pieceTocNb "4-16b" \markup\wordwrap\italic {
    Un grand calme est trop fascheux
  }
  \reIncludeScore "EEIchoeurUnGrandCalmeEstTropFacheux" "EEIchoeurUnGrandCalmeEstTropFacheuxBis"
}
\bookpart {
  \scene "Scene VI" \markup \wordwrap {
    SCENE 6 : Atys, Celænus, Sangar, troupe de dieux des fleuves
  }
  \sceneDescription\markup\wordwrap-center {
    \smallCaps Atys, troupe de Zephirs volants,
    \smallCaps Sangaride, \smallCaps Celænus,
    le dieu du fleuve \smallCaps Sangar,
    troupe de dieux de fleuves, de ruisseaux, et de divinitez de fontaines.
  }
  %% 4-17
  \pieceToc\markup\wordwrap\italic { Venez formez des nœuds charmants }
  \includeScore "EFAchoeurVenezFormerDesNoeudsCharmants"
}
\bookpart {
  %% 4-18
  \pieceToc "Entr’acte"
  \includeScore "EFBentracte"
  \actEnd "FIN DU QUATRIESME ACTE"
  \tocBreak
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Acte V
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bookpart {
  \act "Acte Cinquiesme"
  \sceneDescription\markup\wordwrap-center {
    Le theatre change & represente des jardins agreables.
  }
  \scene "Scene Premiere" "SCENE 1 : Celænus, Cybele"
  \sceneDescription\markup\smallCaps { Celænus, Cybele, Melisse. }
  %% 5-1
  \pieceToc "Ritournelle"
  \includeScore "FAAritournelle"
}
\bookpart {
  %% 5-2
  \pieceToc\markup\wordwrap\italic { Vous m’ostez Sangaride, inhumaine Cybelle }
  \includeScore "FABvousMotezSangaride"
}
\bookpart {
  \scene "Scene II" "SCENE 2 : Celænus, Cybele, Sangaride, Atys"
  \sceneDescription\markup\wordwrap-center {
    \smallCaps { Atys, Sangaride, Cybele, Celænus, Melisse, }
    \line { troupe de prestresses de Cybele. }
  }
  %% 5-3
  \pieceToc\markup\wordwrap\italic { Venez vous livrer au suplice }
  \includeScore "FBAvenezVousLivrerAuSupplice"
}
\bookpart {
  \scene "Scene III" "SCENE 3 : Atys, Sangaride, Cybele, Celænus, chœur"
  \sceneDescription\markup\wordwrap-center {
    \smallCaps { Alecton, Atys, Sangaride, Cybele, Celænus, Melisse, Idas, Doris, }
    "troupe de prestresses de Cybele,"
    "chœur de Phrygiens."
  }
  %% 5-4
  \pieceToc "Prelude pour Alecton"
  \markup\justify\italic {
    Alecton sort des Enfers, tenant à la main un flambeau
    qu’elle secouë en volant & en passant au dessus d’Atys.
  }
  \includeScore "FCApreludePourAlecton"
}
\bookpart {
  %% 5-5
  \pieceToc\markup\wordwrap\italic { Ciel ! quelle vapeur m’environne }
  \includeScore "FCBcielQuelleVapeurMenvironne"
}
\bookpart {
  \scene "Scene IV" \markup\wordwrap { SCENE 4 : Cybele, Atys }
  \sceneDescription\markup\wordwrap-center {
    \smallCaps { Atys, Cybele, Melisse, Idas, }
    "troupe de prestresses de Cybele,"
    "chœur de Phrygiens."
  }
  %% 5-6
  \pieceToc\markup\wordwrap\italic { Que je viens d’immoler une grande victime }
  \includeScore "FDAqueJeViensDimmoler"
}
\bookpart {
  \scene "Scene V" "SCENE 5 : Cybele, Melisse"
  \sceneDescription\markup\smallCaps { Cybele, Melisse. }
  %% 5-7
  \pieceToc\markup\wordwrap\italic { Je commence à trouver sa peine trop cruelle }
  \includeScore "FEAjeCommenceATrouverSaPeineTropCruelle"
}
\bookpart {
  \scene "Scene VI" "SCENE 6 : Cybele, Atys, Idas, Melisse"
  \sceneDescription\markup\wordwrap-center {
    \smallCaps { Atys, Idas, Cybele, Melisse, } "Prestresses de Cybele."
  }
  %% 5-8
  \pieceToc\markup\wordwrap\italic { Il s’est percé le sein }
  \includeScore "FFAilSestPerceLeSein"
}
\bookpart {
  \scene "Scene VII" "SCENE 7 : Cybele, chœur"
  \sceneDescription\markup\column {
    \wordwrap-center {
      \smallCaps Cybele,
      "troupe de nymphes des eaux,"
      "troupe de divinitez des bois,"
      "troupe de corybantes."
    }
    \smaller\italic\wordwrap-center {
      "Quatre nymphes chantantes."
      "Huit dieux des bois chantants."
      "Quatorze corybantes chantantes."
      "Quatre Pages."
      "Huit corybantes dançantes."
      "Trois dieux des bois, dançants."
      "Trois nymphes dançantes."
    }
  }
  %% 5-9
  \pieceToc "Ritournelle"
  \includeScore "FGAritournelle"
  %% 5-10
  \pieceToc\markup\wordwrap\italic { Venez furieux Corybantes }
  \includeScore "FGBvenezFurieuxCorybantes"
  %% 5-10b
  \pieceTocNb "5-10b" "Ritournelle"
  \reIncludeScore "FGAritournelle" "FGCritournelle"
}
\bookpart {
  %% 5-11
  \pieceToc\markup\wordwrap\italic { Atys, l’aimable Atys, malgré tous ses attraits }
  \includeScore "FGCairChoeurAtysAimableAtys"
}
\bookpart {
  \includeScore "FGCquelleDouleur"
}
\bookpart {
  \sceneDescription\markup\smaller\justify {
    Les divinitez des bois & des eaux, avec les corybantes, honorent
    le nouvel arbre, & le consacrent à Cybele.  Les regrets des
    divinitez des bois & des eaux, & les cris des corybantes, sont
    secondez & terminez par des tremblemens des terre, par des
    esclairs, & par des esclats de tonnerre.
  }
  %% 5-12
  \pieceToc "Entrée des nymphes"
  \includeScore "FGDentreeNymphes"
  %% 5-13
  \pieceToc "[Première entrée des] corybantes"
  \includeScore "FGEentreeCorybantes"
  %% 5-14
  \pieceToc "[Seconde entrée des corybantes]"
  \includeScore "FGFsecondeEntreeCorybantes"
}
\bookpart {
  %% 5-15
  \pieceToc\markup\wordwrap\italic { Que le malheur d’Atys afflige tout le monde }
  \includeScore "FGGchoeurQueLeMalheurDAtys"
  \actEnd "FIN DU CINQUIESME ET DERNIER ACTE"
}
\tocFiller#10
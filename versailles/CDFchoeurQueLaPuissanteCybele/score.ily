\score {
  <<
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \dchantInstr } \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \includeLyrics "paroles"
      \new Staff \with { \hcchantInstr } \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \includeLyrics "paroles"
      \new Staff \with { \tchantInstr } \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \includeLyrics "paroles"
      \new Staff \with { \btchantInstr } \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout { }
  \midi { }
}

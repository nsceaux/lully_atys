\clef "basse"
mib2 mib4 mib |
sib2 sib |
fa fa4 re |
sol2 sol4 sol |
la2 la, |
re2 re4 do |
sib,2 sib,4 sib, |
fa2 fa4. fa8 |
do2 do4 do |
sol2 sol4 do |
re2 re, |
sol,1 |

\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix"
                   #:indent 0)
   (basse-viole #:score-template "score-basse-viole-voix"
                #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \column {
   \livretPers \line { Chœurs des Peuples et des Zephirs }
   \livretVerse#7 { Que la puissante Cybele }
   \livretVerse#7 { Nous rende à jamais heureux. }
} #}))

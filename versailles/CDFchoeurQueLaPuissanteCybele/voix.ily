<<
  \tag #'(vdessus basse) {
    \clef "vbas-dessus" r4^\markup\smallCaps { Chœur des peuples & des Zephirs }
    mib'' mib'' mib'' |
    re''2 re''4. re''8 |
    do''2 do''4 fa'' |
    mi''2 re''4. re''8 |
    re''2( dod''4.) re''8 |
    re''2 r |
    r4 re'' re'' re'' |
    do''2 do''4 re'' |
    mib''2 mib''4 do'' |
    sib'2 sib'4 do'' |
    sib'2( la'4.) sol'8 |
    sol'1 |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" r4 sol' sol' sol' |
    fa'2 fa'4 fa' |
    fa'2 fa'4 la' |
    sol'2 sol'4. sol'8 |
    mi'2. la'4 |
    fad'2 r |
    r4 fa'! fa' fa' |
    fa'2 fa'4 fa' |
    sol'2 sol'4. sol'8 |
    sol'2 sol'4. sol'8 |
    sol'2( fad'4.) sol'8 |
    sol'1 |
  }
  \tag #'vtaille {
    \clef "vtaille" r4 sib sib sib |
    sib2 sib4 sib |
    la2 la4 la |
    sib2 sib4 sib |
    la2. la4 |
    la2 r |
    r4 sib sib sib |
    la2 la4 si |
    do'2 do'4 do' |
    re'2 re'4 mib' |
    re'2. re'4 |
    re'1 |
  }
  \tag #'vbasse {
    \clef "vbasse" r4 mib mib mib |
    sib2 sib4 sib |
    fa2 fa4 re |
    sol2 sol4 sol |
    la2. la,4 |
    re2 r |
    r4 sib, sib, sib, |
    fa2 fa4 fa |
    do2 do4 do |
    sol2 sol4 do |
    re2. re4 |
    sol,1 |
  }
>>

\clef "dessus" R1 |
r4 la' re''4. re''8 |
re''4. do''8 do''4. do''8 |
do''4 sib'8 la' sib'4. do''8 |
la'4. si'8 \once\slurDashed do''4( si') |
la'2 la''4. si''8 |
sold''2 la''4. la''8 |
la''4. sol''8 sol''4. sol''8 |
sol''2 fa''4. fa''8 |
fa''2 mi''4. mi''8 |
fa''4. sol''8 mi''4. fa''8 |
fa''4 do'' sib' la' |
sol'4. sol'8 sol'4 la' |
sib'4. sib'8 la'4. sol'8 |
fa'4. fa'8 fa'4 sol' |
la' sib'8 la' sol'4. fa'8 |
mi'4. mi''8 mi''4. fa''8 |
sol''2. sol''4 |
sol''4 fa''8 mi'' fa''4. sol''8 |
mi''2 fa''~ |
fa''4. mi''8 mi''4. re''8 |
re''1 |

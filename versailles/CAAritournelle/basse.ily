\clef "basse" re2 re'4 dod' |
re'2. re4 |
la4. la8 la4. sol8 |
fad2 sol4 sol, |
re4 re la sol |
fa mi fa2 |
mi la, |
mi4 si mi' re' |
dod'4 la re' do'? |
si sol do' sib |
la fa do' do |
fa2. fa4 |
do'4. do'8 sib4 la |
sol2 la |
sib4. sib8 la4 sol |
fa2 sol |
la2 sol4. fa8 |
mi4. fa8 mi4 re |
dod2 re |
la, sol, |
la,1 |
re, |

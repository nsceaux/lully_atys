\clef "dessus" r2 re''4 la'' |
fa''2 fa''4. fa''8 |
mi''4. mi''8 mi''4. mi''8 |
re''2 re''4. mi''8 |
fa''2 mi''4. mi''8 |
mi''2 re''4. re''8 |
re''4. do''16 si' do''4. re''8 |
si'4. si'8 si'4. la'8 |
la'2 la''4. la''8 |
sol''2 sol''4. sol''8 |
la''4. sib''8 sol''4. fa''8 |
fa''4 la'' sol'' fa'' |
mi''4. mi''8 mi''4 fad'' |
sol''4. sol''8 fa''4. mi''8 |
re''4. re''8 re''4. mi''8 |
fa''4 sol''8 fa'' mi''4. re''8 |
dod''4. dod''8 dod''4. re''8 |
mi''2. mi''4 |
mi''2 la'4 si' |
dod''2 re''~ |
re''4. mi''8 dod''4. re''8 |
re''1 |

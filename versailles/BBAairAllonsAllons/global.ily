\key re \minor
\tag #'complet {
  \digitTime\time 2/2 \midiTempo#160 s1*12
  \digitTime\time 3/4 \midiTempo#160 s2.*26 \bar "||"
}
\time 2/2 \measure 1/2 s2
\measure 2/2 s1*17 \bar "||"
\tag #'complet {
  \time 4/4 \midiTempo#80 s1*2
  \digitTime\time 3/4 \midiTempo#160 s2.*59
  \time 4/4 \midiTempo#80 s1*3
  \digitTime\time 2/2 \midiTempo#160 s1*15
  \time 4/4 \midiTempo#80 s1*13
  \digitTime\time 2/2 \midiTempo#160 s1*29
  \time 3/2 s1.
  \digitTime\time 2/2 s1*4 \bar "|."
}

<<
  %% Atys
  \tag #'(atys basse) {
    \tag #'basse \atysMark
    \tag #'atys \atysClef r4 r8 sol sib4. sol8 |
    re'4 mib'8[ re'] do'[ sib] do'[ la] |
    sib4. re'8 re'4. re'8 |
    sol'4 sol'8[ fa'] mib'[ re'] do'[ sib] |
    la4. fa'8 fa'4. fa'8 |
    sol'[ fa'] mib'[ re'] re'4( do') |
    sib4. fa'8 re'4. sib8 |
    fa'4 sol'8[ fa'] mib'[ re'] mib'[ do'] |
    re'4. re'8 re'4. re'8 |
    mib'4 do'8[ re'] mib'[ re'] do'[ sib] |
    la4. re'8 sol'4. fa'8 |
    mib'[ re'] do'[ sib] sib4( la) |
    sol2. |
  }
  %% Idas
  \tag #'idas {
    \idasClef R1 |
    r4 sol fad re |
    sol sib8[ la] sol[ fa] mib[ re] |
    mib4 mib8[ re] do[ re] mib[ do] |
    fa4. fa8 re4. re8 |
    mib4 mib fa2 |
    sib,1 |
    r4 sib la fa |
    sib sib8[ la] sol[ fa] mib[ re] |
    do4 mib8[ re] do[ sib,] la,[ sol,] |
    re4. re8 sib,4. sib,8 |
    do4 do re2 |
    sol,2. |
  }
>>
<<
  \tag #'idas { R2.*25 }
  \tag #'(atys basse) {
    r4 sib4. sol8 |
    re'4 do' sib8[ la] |
    sib2 do'8 re' |
    mib'2 re'8 mib' |
    re'4.\trill re'8 mib'4 |
    fa' sib4( la8) sib |
    la4. do'8 re'4 |
    mib' fa' mib'8[ re'] |
    mib'4. mib'8 fa' sol' |
    do' re' re'4( do') |
    sib2. |
    sib4 sib8 do' re' mi' |
    fa'4. do'8 re'4 |
    mib' re'4 sol'
    fad'4. mi'8( re'4) |
    do'4. re'8 sib4 |
    sib4( la4.) sol8 |
    sol2. |
    sib4 sib8 do' re' mi' |
    fa'4. do'8 re'4 |
    mib' re'4 sol' |
    fad'4. mi'8( re'4) |
    do'4. re'8 sib4 |
    sib( la4.) sol8 |
    sol2.
  }
>>
<<
  \tag #'atys { r2 R1*17 }
  \tag #'(idas basse part-dessus) {
    \tag #'(basse part-dessus) \idasMark sol4 sol |
    re1 |
    mib4. mib8 re4. mib8 |
    do1 |
    sib, |
    sib4. sib8 sib4. la8 |
    sol2 sol4 do' |
    fa4. fa8 fa4. fa8 |
    sol4 sol2 la8 sib |
    la4 la fa4. fa8 |
    dod4 dod re8[ dod] re4  |
    la,1 |
    re4. re'8 re'4. do'8 |
    sib4 sib2 la8 sol |
    fad4. sol8 sol4 re |
    mib4 mib mib4 do |
    re1 |
    sol,4\fermata r4 r2 |
  }
>>
<<
  \tag #'(atys basse) {
    \tag #'basse \atysMark
    r4 re'4 sib8 sib sib re' |
    sol4 sol8 sol do'4 do'8 sol |
    la2. |
    r2 re'4 |
    sib2. |
    la4 sib sol |
    re'2 mib'4 |
    fa'4. fa'8 mib'4 ~ |
    mib'8 re' re'4( do') |
    sib2. |
    re'2 re'8 mib' |
    do'2( si8) do' |
    si2 si4 |
    do'2. |
    sib4 sib( la8) sib |
    la2. |
    re'4 re' mi' |
    fa'2. |
    fa'4 fa' mi' |
    fa'2. |
    mi'4 mi'4. fa'8 |
    dod'4 la re'~ |
    re'8 mi' mi'2\trill |
    re'2. |
    fa'4 fa' re' |
    mib'2. |
    re'4 sol'8[ fad'] sol'4 |
    fad'2. |
    re'4 sol'4 re' |
    mib'4 do' la~ |
    la8 sib sib4( la) |
    sol4
  }
  \tag #'idas { R1*2 R2.*29 r4 }
>>
<<
  \tag #'(idas basse) {
    \tag #'basse \idasMark
    \ru#2 {
      re4 sol |
      fad2 fad4 |
      sol la sib |
      la2 re'4 |
      sib sol do' |
      la2 sib8[ la] |
      sib4 sib( la) |
      sib4
    }
    re4 mi |
    fa4 re sol |
    fad2 sol4 |
    la2. |
    la4. sib8 do'4 |
    fad2. |
    sol4 la sib |
    sib( la2) |
    sol4 re4 mi |
    fa4 re sol |
    fad2 sol4 |
    la2. |
    la4. sib8 do'4 |
    fad2. |
    sol4 la sib |
    sib( la2) |
    sol4. sib8 sol8 sol re8. mi16 |
    fa4. fa8 fa4 mib8 mib16 re |
    re4 r16 sib do' re' sol8. sol16 la8. sib16 |
    fad2 r |
    r sol4 sol |
    sol2 fa4. fa8 |
    fa4 mib8[ re] mib2 |
    re1 |
    re'4 re'8 do' sib4. la8 |
    sol2 sol4 la |
    sib2 sib4 la |
    sib1 |
    sib,4 sib,8 do re4. mi8 |
    fa2 fa4 sol8 la |
    sib2. mi4 |
    fa2 fa |
    r4 fad fad fad |
    sol do re( do8) re |
    sol,4
  }
  \tag #'atys { r4 r | R2.*29 R1*18 r4 }
>>
<<
  \tag #'(atys basse) {
    \tag #'basse \atysMark
    sib8. re'16 sib4 sib8 sib |
    sol4. mib'8 do'4 do'8 re'16 mib' |
    re'4 re' r re' |
    do' do'8 re' sib4 la8 sib |
    la2
  }
  \tag #'idas { r4 r2 R1*3 r2 }
>>
<<
  \tag #'(idas basse) {
    \tag#'basse \idasMark
    r4 re8 re |
    sol4 sol r8 sol sol8 fa |
    mi4 do8. do16 fa4 fa8. fa16 |
    fa4 mi fa fa |
    r do'8 do' do'4 re'8 la |
    sib4. la8 sol fa mi re |
    dod2 la8. la16 la8 la16 sib |
    sol4. fa8 mi8. mi16 mi8. fa16 |
    re4 re r2 |
    r r4 sib |
    sib4. la8 la4( sib8) do' |
    fad2. sib4 |
    sol( fad8) sol sol4 fad |
    sol2. sib4 |
    re4. re8 re4 mib |
    fa2 fa4. sol8 |
    mib2. re8 mib |
    re2 sib4. la8 |
    sol2 fa4( mi8) fa |
    mi4. do'8 sol4. sol8 |
    la4. sib8 la4( sol) |
    fa2. la4 |
    sib2 sol4. sol8 |
    sol2 fa4( mi8) fa |
    mi2 la4. la8 |
    la4. sol8 sol2~ |
    sol fa4( mi8) fa |
    fa2( mi) |
    re2. sib4 |
    sib4. la8 la4( sib8) do' |
    fad2. sib4 |
    sol4( fad8) sol sol4 fad |
    sol2
  }
  \tag #'atys { r2 R1*31 r2 }
>>
<<
  \tag #'(atys basse) {
    \tag #'basse \atysMark
    r4 r8 re' |
    sib4. mib'8 mib'4. mib'8 |
    do'2. fa'4 |
    fa'2 fa'4 mib'8 re' |
    mib'2 mib'4. do'8 |
    sol4. sol8 sol4 la |
    sib4. sib8 sib4. do'8 do'4. re'8 |
    re'2. sol'8 fa' |
    mib'4 mib'8 re' do'4. sib8 |
    la4. la8 la4. sib8 |
    sol2 sol |
  }
  \tag #'idas { r2 R1*5 R1. R1*4 }
>>

\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse-continue #:score-template "score-basse-continue-voix"
                   #:indent 0)
   (basse-viole #:score-template "score-basse-viole-voix"
                #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Idas, Atys
    \livretVerse#8 { Allons, allons, accourez tous, }
    \livretVerse#6 { Cybele va descendre. }
    \livretPers Atys
    \livretVerse#12 { Le Soleil peint nos champs des plus vives couleurs, }
    \livretVerse#6 { Il a seché les pleurs }
    \livretVerse#12 { Que sur l’émail des prez a répandu l’Aurore ; }
    \livretVerse#12 { Et ses rayons nouveaux ont déja fait éclorre }
    \livretVerse#6 { Mille nouvelles fleurs. }
    \livretPers Idas
    \livretVerse#8 { Vous veillez lorsque tout sommeille ; }
    \livretVerse#8 { Vous nous éveillez si matin }
    \livretVerse#8 { Que vous ferez croire à la fin }
    \livretVerse#8 { Que c’est l’Amour qui vous éveille. }
    \livretPers Atys
    \livretVerse#12 { Non tu dois mieux juger du party que je prens. }
    \livretVerse#12 { Mon cœur veut fuir toûjours les soins et les misteres ; }
    \livretVerse#12 { J’ayme l’heureuse paix des cœurs indifferents ; }
    \livretVerse#8 { Si leurs plaisirs ne sont pas grands, }
    \livretVerse#8 { Au moins leurs peines sont legeres. }
    \livretPers Idas
    \livretVerse#8 { Tost ou tard l’Amour est vainqueur, }
    \livretVerse#8 { En vain les plus fiers s’en deffendent, }
    \livretVerse#8 { On ne peut refuser son cœur }
    \livretVerse#8 { A de beaux yeux qui le demandent. }
  }
  \column {
    \livretVerse#12 { Atys, ne feignez plus, je sçais votre secret. }
    \livretVerse#8 { Ne craignez rien, je suis discret. }
    \livretVerse#8 { Dans un bois solitaire, et sombre, }
    \livretVerse#12 { L’indifferent Atys se croyoit seul, un jour ; }
    \livretVerse#12 { Sous un feüillage épais où je resvois à l’ombre, }
    \livretVerse#8 { Je l’entendis parler d’amour. }
    \livretPers Atys
    \livretVerse#12 { Si je parle d’amour, c’est contre son empire, }
    \livretVerse#8 { J’en fais mon plus doux entretien. }
    \livretPers Idas
    \livretVerse#8 { Tel se vante de n’aimer rien, }
    \livretVerse#8 { Dont le cœur en secret soûpire. }
    \livretVerse#12 { J’entendis vos regrets, et je les sçais si bien }
    \livretVerse#12 { Que si vous en doutez je vais vous les redire. }
    \livretVerse#12 { Amans qui vous plaignez, vous estes trop heureux : }
    \livretVerse#12 { Mon cœur de tous les cœurs est le plus amoureux, }
    \livretVerse#12 { Et tout prés d’expirer je suis reduit à feindre ; }
    \livretVerse#8 { Que c’est un tourment rigoureux }
    \livretVerse#8 { De mourir d’amour sans se plaindre ! }
    \livretVerse#12 { Amans qui vous plaignez, vous estes trop heureux. }
    \livretPers Atys
    \livretVerse#12 { Idas, il est trop vray, mon cœur n’est que trop tendre, }
    \livretVerse#12 { L’Amour me fait sentir ses plus funestes coups. }
    \livretVerse#12 { Qu’aucun autre que toy n’en puisse rien apprendre. }
  }
} #}))

\clef "basse" sol,1~ |
sol,4 sol fad re |
sol sib8 la sol fa mib re |
mib4 mib8 re do re mib do |
fa2 re |
mib fa4 fa, |
sib,1~ |
sib,4 sib la fa |
sib sib8 la sol fa mib re |
do4 mib8 re do sib, la, sol, |
re4. re8 sib,4. sib,8 |
do4 do re2 |
sol,2. | \allowPageTurn
sol |
fad |
sol |
la |
sib |
la4 sol2 |
fa2. |
mib4 re sol |
do4. do8 re mib |
fa8 sib, fa,2 |
sib,2. |
sib2 sib4 |
la2 si4 |
do' sol8 fa mib4 |
re4. do8 sib,4 |
fad,2 sol,4 |
do, re,2 |
sol,2. |
sol4 sol8 la sib4 |
la2 si4 |
do' sol8 fa mib4 |
re4. do8 sib,4 |
fad,2 sol,4 |
do, re,2 |
sol,2. |
%%
sol2 |
re1 |
mib2 re4. mib8 |
do1 |
sib, |\allowPageTurn
sib2 sib4 la4 |
sol2 sol4 do' |
fa2. fa4 |
sol4 sol2 la8 sib |
la2 fa |
dod re |
la,1 |
re4. re'8 re'4. do'8 |
sib4 sib2 la8 sol |
fad2 sol4 re |
mib2 mib4 do |
re1 |
sol,1\fermata | \allowPageTurn
%%
sol,1 |
mib |
re2 mi4 |
fad2. |
sol |
fad4 sol2 |
fa mib4 |
re2 do4 |
sib,4 fa4. mib8 |
re4. do8 sib,4 |
sib2 sib8 sol |
lab2 fa4 |
sol2 fa4 |
mi2 fad4 |
sol sol,2 |
re4. mib8 re do |
sib,4 sib2 |
la sib4 |
sol do'4. sib8 |
la4. sol8 fa4 |
sol2. |
la2 sib4 |
sol la4. sol8 |
fa sol fa mi re do |
si,2. |
do4. re8 mib do |
sol fa mib2 |
re2 do4 |
si,2. |
do2 re4 |
sol, re,2 |
sol,4. %{%}
la,8 sib, sol, |
re2 do4 |
sib, la, sol, |
re4. mi!8 fad4 |
sol2 mi4 |
fa2 re4 |
mib fa fa, |
sib,4. la,8 sib, sol, |
re2 do4 |
sib, la, sol, |
re4. mi!8 fad4 |
sol2 mi4 |
fa2 re4 |
mib fa fa, |
sib,4 sib,2 |
la,4 sib, sol, |
re2 sol,4 |
re, re mi |
fa2 mib4 |
re la, re |
sib, la, sol, |
re re,2 |
sol,8 la, sib,2 |
la,4 sib, sol, |
re2 sol,4 |
re, re mi |
fa2 mib4 |
re la, re |
sib, la, sol, |
re re,2 |
sol,1 |
re2 la, |
sib, mib |
re2. do4 |
sib,1 |
la, |
si,2 do |
sol,1 |
fad,2 sol, |
mib1 |
re4 mib fa fa, |
sib,1 |
sib, | \allowPageTurn
la, |
sol, |
fa,2 fa4. mib8 |
re1 |
mib4 do re re, |
sol,1 | \allowPageTurn
mib2 fa4 fa, |
sib,2 sib |
fad sol4 sol, |
re1 |
si, |
do4. sib,!8 la,4 sib, |
sol,2 fa, |
fa, fad, |
sol,1 |
la,2 fa, |
sol, la, |
re, re8 do sib, la, |
sol,1 |
do |
re2. sib,4 |
mib4 do re re, |
sol,1 |
sol,2 fa,4 mib, |
re,2 re4 mib |
do1 |
sib,2. sib,4 |
si,1 |
do4. re8 mi2 |
fa4 sib, do2 |
fa,1 |
sol, |
la,2 re, |
la, fa, |
sib,1 |
la,2. sol,4 |
la,1 |
re4. do8 sib,4 sol, |
do1 |
re2. sib,4 |
mib4 do re re, |
sol,1 |
sol2. sol4 |
la2. la4 |
si1 |
do'2 do |
do sib,4 la, |
sol, fa, mib,1 |
re,4 re sib,2 |
do1 |
re2 re, |
sol,1 |

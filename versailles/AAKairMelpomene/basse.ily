\clef "basse" do2 do' |
si1 |
la |
sol2 do |
sol, sol |
mi1 |
fa2. fa4 |
dod2. dod4 |
re2 si, |
la, re4 re, |
sol,1 |
sol,2 sol,8 sol sol sol |
re re re re la la la mi |
fa4 re mi8 mi mi mi |
la la la la, re re re re |
sol sol sol sol, do do do do |
fa2 fa4. mi8 |
re4. re8 mi4. mi8 |
fa2 sol4 sol, |
do2 sol,8 sol sol sol |
do1 |

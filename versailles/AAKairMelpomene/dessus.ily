\clef "dessus" mi''2~ mi''8 do'' sol'' sol'' |
sol''2 r8 sol' sol' sol' |
do''2 do''8. do''16 fa''8. fa''16 |
fa''4. mi''8 mi''4. re''8 |
re''2 r8 re'' si'8. re''16 |
sol'2 r8 sol''16 fa'' mi'' re'' do'' si' |
la'4. la'8 la''4. la''8 |
la''2 la''8 sol'' la'' mi'' |
fad''2 sol''4. si'8 |
do''4. do''8 do''4. re''8 |
si'1 |
si'2 r8 re'' re''16 do'' re'' mi'' |
fa''8 fa'' fa'' sol''16 fa'' mi''8 mi'' do'' mi'' |
la' la' re''16 do'' si' la' sold'8 si' mi'' mi''16 re'' |
dod''8 la'' mi'' la'' fad'' la' re'' re''16 do'' |
si'8 sol'' re'' sol'' mi'' sol' do'' do''16 si' |
la'2 la'8. la'16 si' do'' re'' mi'' |
fa''4 fa''8 mi''16 fa'' sol''4 sol''8. fa''32 sol'' |
la''4. re''8 re''4. do''8 |
do''2 r8 re'' re''16 do'' re'' mi'' |
do''1 |

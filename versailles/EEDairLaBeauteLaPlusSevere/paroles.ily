La beau -- té la plus se -- ve -- re
Prend pi -- tié d’un long tour -- ment,
Et l’a -- mant qui per -- se -- ve -- re
De -- vient un heu -- reux a -- mant.

Tout est doux, et rien ne coû -- te
Pour un cœur qu’on veut tou -- cher,
L’on -- de se fait u -- ne rou -- te
En s’ef -- for -- çant d’en cher -- cher,
L’eau qui tom -- be goute à gou -- te
Per -- ce le plus dur ro -- cher.


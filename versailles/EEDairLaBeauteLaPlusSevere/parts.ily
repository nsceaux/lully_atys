\piecePartSpecs
#`((basse-continue #:clef "alto"
                   #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers "Dieux de Fleuves, Divinitez de Fontaines, & de Ruisseaux"
    \livretVerse#7 { La Beauté la plus severe }
    \livretVerse#7 { Prend pitié d’un long tourment, }
    \livretVerse#7 { Et l’Amant qui persevere }
    \livretVerse#7 { Devient un heureux Amant. }
    \livretVerse#7 { Tout est doux, et rien ne coûte }
  }
  \column {
    \null
    \livretVerse#7 { Pour un cœur qu’on veut toucher, }
    \livretVerse#7 { L’onde se fait une route }
    \livretVerse#7 { En s’efforçant d’en chercher, }
    \livretVerse#7 { L’eau qui tombe goute à goute }
    \livretVerse#7 { Perce le plus dur Rocher. }
  }
}#}))

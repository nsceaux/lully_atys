\clef "basse" do1~ |
do2 si, |
do2. do8 si, |
la,1 |
sol, |
re,4 re2 |
la,1 |
mi2 dod |
re2. do4 |
si,2 fad, |
sol,2 sol |
mi2 mi4 fa4 |
do1~ |
do |
la,1 |
sib,2 mib |
re1 |
sib,8 sol, re4 re, |
sol,1 |
do'4 do2 |
sol2. |
mi2 mi4 fa |
do2.

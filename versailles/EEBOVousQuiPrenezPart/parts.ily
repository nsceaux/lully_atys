\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers\line { Le Dieu du Fleuve Sangar }
    \livretVerse#12 { O vous, qui prenez part au bien de ma famille, }
    \livretVerse#12 { Vous, venerables Dieux des Fleuves les plus grands, }
    \livretVerse#12 { Mes fidelles Amis, et mes plus chers Parents, }
    \livretVerse#12 { Voyez quel est l’Espoux que je donne à ma fille : }
    \livretVerse#12 { J’ay pris soin de choisir entre les plus grands Roys. }
    \livretPers\line { Chœur de Dieux de Fleuves }
    \livretVerse#7 { Nous aprouvons vostre choix. }
  }
  \column {
    \livretPers\line { Le Dieu du Fleuve Sangar }
    \livretVerse#8 { Il a Neptune pour son Pere, }
    \livretVerse#8 { Les Phrygiens suivent ses Loix ; }
    \livretVerse#6 { J’ay crû ne pouvoir faire }
    \livretVerse#8 { Un choix plus digne de vous plaire. }
    \livretPers\line { Chœur de Dieux de Fleuves }
    \livretVerse#7 { Tous, d’une commune voix, }
    \livretVerse#7 { Nous aprouvons vostre choix. }
  }
}#}))

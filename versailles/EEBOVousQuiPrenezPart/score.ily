\score {
  <<
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \sangarInstr } \withLyrics <<
        \global \keepWithTag #'sangar \includeNotes "voix"
      >> \keepWithTag #'sangar \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \with { \hcchantInstr } \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \with { \tchantInstr } \withLyrics <<
        \global \keepWithTag #'vtaille1 \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \with { \tchantInstr } \withLyrics <<
        \global \keepWithTag #'vtaille2 \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \with { \btchantInstr } \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
        \modVersion { s1*5 s2. s1*7\break }
      >>
    >>
  >>
  \layout { }
  \midi { }
}

<<
  \tag #'(sangar basse) {
    \tag #'basse \sangarMark
    \tag #'sangar \sangarClef r4 do' sol8. sol16 sol8. la16 |
    fa4. fa8 fa fa fa mi |
    mi4 mi sol8 sol16 sol la8. si16 |
    do'4. la8 fad8. fad16 fad8. sol16 |
    sol2 r8 re16 re re8 re16 mi |
    fa8. fa16 fa8. fa16 fa8. mi16 |
    mi4 r8 do' la8. la16 mi8. fad16 |
    sol4 sol8. sol16 sol4 sol8 fad |
    fad4 fad r8 re16 re re8 mi16 fad! |
    sol2 la8 la16 la si8 do' |
    si2
    <<
      \tag #'sangar { r2 | R1*2 | }
      \tag #'basse { s2 | s1*2 \sangarMark }
    >>
    r8 do' do' do' mi8. mi16 mi8. fa16 |
    fa8 fa la la16 la fa4 fa8 fa16 fa |
    re4 r8 sib sol8. sol16 sol8. la16 |
    fad4 fad r re |
    sol8. sol16 sol8. sol16 sol8. fad16 |
    sol2 sol |
    \tag #'sangar { R2.*2 | R1 | r2*3/2 }
  }
  \tag #'(vhaute-contre basse) {
    <<
      \tag #'basse { s1*5 s2. s1*4 s2 \ffclef "vhaute-contre" }
      \tag #'vhaute-contre { \clef "vhaute-contre" R1*5 R2. R1*4 r2 }
    >> r8^\markup "Douze grand dieux de fleuves chantants" sol' sol' sol' |
    sol'2 sol'4 fa' |
    mi'1 |
    <<
      \tag #'basse { s1*4 s2. s1 \ffclef "vhaute-contre" }
      \tag #'vhaute-contre { R1*4 R2. R1 }
    >>
    sol'4 mi'8 mi'16 mi' mi'8. fa'16 |
    re'4 r8 sol' sol' sol' |
    sol'2 sol'4 fa' |
    mi'2.
  }
  \tag #'vtaille1 {
    \clef "vtaille" R1*5 R2. R1*4 |
    r2 r8 re'8 re' re' |
    mi'2 mi'4 do' |
    do'1 |
    R1*4 R2. R1 |
    mi'4 do'8 do'16 do' do'8. do'16 |
    si4 r8 re' re' re' |
    mi'2 do'4 do' |
    do'2.
  }
  \tag #'vtaille2 {
    \clef "vtaille" R1*5 R2. R1*4 |
    r2 r8 si si si |
    do'2 do'4 la |
    sol1 |
    R1*4 R2. R1 |
    do'4 sol8 sol16 sol sol8. sol16 |
    sol4 r8 si si si |
    do'2 do'4 la |
    sol2.
  }
  \tag #'vbasse {
    \clef "vbasse" R1*5 R2. R1*4 |
    r2 r8 sol sol sol |
    mi2 mi4 fa |
    do1 |
    R1*4 R2. R1 |
    do'4 do8 do16 do do8. do16 |
    sol4 r8 sol sol sol |
    mi2 mi4 fa |
    do2.
  }
>>

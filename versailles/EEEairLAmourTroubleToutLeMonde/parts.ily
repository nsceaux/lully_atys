\piecePartSpecs
#`((basse-continue #:clef "alto"
                   #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers "Dieux de Fleuves, Divinitez de Fontaines, & de Ruisseaux"
    \livretVerse#7 { L’Amour trouble tout le Monde, }
    \livretVerse#7 { C’est la source de nos pleurs ; }
    \livretVerse#7 { C’est un feu brûlant dans l’onde, }
    \livretVerse#7 { C’est l’écüeil des plus grands cœurs : }
  }
  \column {
    \null
    \livretVerse#7 { Il est fier, il est rebelle, }
    \livretVerse#7 { Mais il charme tel qu’il est ; }
    \livretVerse#7 { L’Hymen vient quand on l’appelle, }
    \livretVerse#7 { L’Amour vient quand il luy plaist. }
  }
}#}))

L’A -- mour trou -- ble tout le mon -- de,
C’est la sour -- ce de nos pleurs ;
C’est un feu brû -- lant dans l’on -- de,
C’est l’é -- cüeil des plus grands cœurs :

Il est fier, il est re -- bel -- le,
Mais il char -- me tel qu’il est ;
L’Hy -- men vient quand on l’ap -- pel -- le,
L’A -- mour vient quand il luy plaist.

<<
  \tag #'(vdessus basse) {
    <>^\markup {
      \concat { 1 \super re } divinité de fontaine et
      \concat { 1 \super er } petit dieu de ruisseau
    }
    \clef "vdessus" \ru#2 {
      mi''4 mi''2 |
      do'' do''4 fa'' fa''2 |
      re'' re''4 sol'' mi''2 |
      fa'' mi''4 fa'' re''2 |
      do''2.
    } re''4 re''2 |
    mi'' mi''4 do'' re''2 |
    si' si'4 do'' do''2 |
    do'' si'4 do'' la'2 |
    sol'2. re''4 mi''2 |
    re'' mi''4 fa'' mi''2 |
    re'' re''4 fa'' fa''2 |
    fa'' mi''4 fa'' re''2 |
    do''2.
  }
  \tag #'vbas-dessus {
    <>^\markup {
      \concat { 2 \super e } divinité de fontaine et
      \concat { 2 \super e } petit dieu de ruisseau
    }
    \clef "vbas-dessus" \ru#2 {
      do''4 do''2 |
      la' la'4 re'' re''2 |
      si' si'4 si' dod''2 |
      re'' do''!4 re'' si'2 |
      do''2.
    } si'4 si'2 |
    do'' do''4 la' si'2 |
    sold' sold'4 la' mi'2 |
    fad' sol'4 la' fad'2 |
    sol'2. si'4 do''2 |
    si' do''4 re'' do''2 |
    si' si'4 la' la'2 |
    si' do''4 re'' si'2 |
    do''2.
  }
  \tag #'vhaute-contre {
    <>^\markup { Dieux de fleuvres }
    \clef "vhaute-contre" \ru#2 {
      do'4 do'2 |
      fa' fa'4 re' re'2 |
      sol' sol'4 mi' la'2 |
      re' la'4 fa' sol'2 |
      do'2.
    } sol'4 sol'2 |
    do' do'4 fa' re'2 |
    mi' mi'4 la la2 |
    re' mi'4 do' re'2 |
    sol2. sol'4 do'2 |
    sol' do'4 si do'2 |
    sol sol4 re' re'2 |
    sol' la'4 fa'4 sol'2 |
    do'2.
  }
>>

\piecePartSpecs
#`((flutes-hautbois #:score-template "score"
                    #:notes "dessus"
                    #:system-count 3)
   (basse-continue)
   (basse-viole #:system-count 2))

\score {
  \new StaffGroup <<
    \new Staff \with { \flInstr } << \global \includeNotes "dessus" >>
    \new Staff \with { \bcInstr } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
  \midi { }
}

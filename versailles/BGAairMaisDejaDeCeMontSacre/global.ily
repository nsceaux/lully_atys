\key do \major
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.*2
\time 4/4 s1*5 \measure 2/4 s2 \bar "||"
\digitTime\time 3/4 \midiTempo#160 s2.*21
\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff \haraKiriFirst } \withTinyLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse \includeLyrics "paroles"
    \new Staff \with { \bcInstr } <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { short-indent = \indent }
}

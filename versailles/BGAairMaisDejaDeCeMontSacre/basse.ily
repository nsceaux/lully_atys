\clef "basse" do1 |
sol2 re | 
la2. |
re'8 sol re4 re, |
sol,4 sol fad2 |
sol4 sol8 fa mi4 mi8 re |
do4 do8 re mi2 |
fa8 mi re4 sol8 fa mi4 |
la8. la16 si8. do'16 sol4 sol, |
do2 |
do2 do4 |
sol4. fa8 mi4 |
re2 re4 |
la4 la sol |
fa2. |
mi2 mi4 |
la2 re4 |
sol, la,2 |
re4. mi8 fad4 |
sol2. |
la2 sol4 |
fa2 do4 |
sol2 mi4 |
la2 fa4 |
sol sol,2 |
do do'4 |
mi2. |
fa2 re4 |
sol2 mi4 |
la2 fa4 |
sol sol,2 |

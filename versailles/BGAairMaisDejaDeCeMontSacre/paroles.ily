%% ACTE 1 SCENE 7
% Atys
\tag #'(atys basse) {
  Mais dé -- ja de ce mont sa -- cré
  Le som -- met pa -- roist é -- clai -- ré
  D’u -- ne splen -- deur nou -- vel -- le.
}
% Sangaride s’avançant vers la montagne.
\tag #'(sangaride basse) {
  La dé -- es -- se des -- cend,
  La dé -- es -- se des -- cend, al -- lons, al -- lons au de -- vant d’el -- le.
}
\tag #'(sangaride basse) {
  Com -- men -- çons, com -- men -- çons
  De ce -- le -- brer i -- cy sa fes -- te so -- lem -- nel -- le,
  Com -- men -- çons nos jeux,
  Com -- men -- çons, com -- men -- çons Nos jeux et nos chan -- sons.
  Com -- men -- çons, com -- men -- çons Nos jeux, nos jeux et nos chan- % -- sons.
}
\tag #'atys {
  Com -- men -- çons, com -- men -- çons
  De ce -- le -- brer i -- cy sa fes -- te so -- len -- nel -- le,
  Com -- men -- çons nos jeux,
  Com -- men -- çons nos jeux et nos chan -- sons.
  Com -- men -- çons nos jeux, nos jeux et nos chan- % -- sons.
}
\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-tous)
   (basse #:score "score-basse")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Atys
    \livretVerse#8 { Mais déja de ce Mont sacré }
    \livretVerse#8 { Le sommet paroist éclairé }
    \livretVerse#6 { D’une splendeur nouvelle. }
    \livretPers Sangaride
    \livretVerse#12 { Le Déesse descend, allons au devant d’elle. }
  }
  \column {
    \livretPers\line { Atys & Sangaride }
    \livretVerse#6 { Commençons, commençons }
    \livretVerse#12 { De celebrer icy sa feste solemnelle, }
    \livretVerse#6 { Commençons, commençons }
    \livretVerse#6 { Nos Jeux et nos chansons. }
  }
}#}))

<<
  %% Dessus / Sangaride
  \tag #'(sangaride basse) {
    <<
      \tag #'basse { s1*2 s2.*2 s2 \sangarideMark }
      \tag #'sangaride { \sangarideClef R1*2 | R2.*2 | r2 }
    >> r4 re''8 do'' |
    si'4 si'8 re'' sol'2 |
    r4 mi''8 re'' do''4 do''8 mi'' |
    la'4. re''8 si'4. mi''8 |
    do''8. do''16 re''8. mi''16 mi''4( re'') |
    do''2 |
  }
  %% Haute-contre / Atys
  \tag #'(atys basse) {
    \tag #'basse \atysMark
    \tag #'atys \atysClef
    r4 r8 sol16 sol do'8 do'16 re' mi'8. fa'16 |
    re'4 r8 re'16 mi' fa'8. fa'16 fa'8 fa'16 mi' |
    mi'2 mi'8 mi'16 mi' |
    fad'8 sol' si4( la) |
    sol2 \tag #'atys { r2 R1*4 r2 }
  }
>>
<<
  \tag #'(sangaride basse) {
    r4 r8 sol' la'4 |
    si'4. si'8 dod''4 |
    re''2. |
    do''4 do'' si' |
    la'4.( sol'8) la'4 |
    sold'2 si'4 |
    dod''4. dod''8 re''[ dod''] |
    re''4 re''( dod'') |
    re''4. re''8 do''4 |
    si'2 si'4 |
    do''4. do''8 sib'4 |
    la'4. si'!8 do''4 |
    si'2 mi''4 |
    do''2 re''4 |
    do''4( si'4.) do''8 |
    do''4. sol'8 la'4 |
    sib'4. sib'8 sib'8[ do''] |
    la'2 re''4 |
    si'2 mi''4 |
    do''2 re''4 |
    do''4( si'4.) do''8 |
  }
  \tag #'atys {
    r4 r8 mi' mi'4 |
    re'4. re'8 mi'4 |
    fa'2. |
    mi'4 mi'4. mi'8 |
    mi'2 re'4 |
    mi'2 mi'4 |
    mi'4. mi'8 fa'[ mi'] |
    fa'4 fa'( mi') |
    re'2 r4 |
    r r8 sol' fa'4 |
    mi'2 mi'4 |
    fa'4. fa'8 mi'4 |
    re'2 sol'4 |
    mi'2 fa'4 |
    mi'( re'4.) do'8 |
    do'2 r4 |
    r4 r8 sol' sol'4 |
    do'2 fa'4 |
    re'2 sol'4 |
    mi'2 fa'4 |
    mi'( re'4.) do'8 |
  }
>>

\score {
  <<
    \new ChoirStaff \with { \forceGroupBracket \haraKiriFirst } <<
      \new Staff \with { \sangarideInstr } \withLyrics <<
        \global \keepWithTag #'sangaride \includeNotes "voix"
      >> \keepWithTag #'sangaride \includeLyrics "paroles"
      \new Staff \with { \atysInstr } \withLyrics <<
        \global \keepWithTag #'atys \includeNotes "voix"
      >> \keepWithTag #'atys \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \keepWithTag #'basse-continue \includeNotes "basse"
        \includeFigures "chiffres"
        \modVersion { s1*2 s2.*2 s1*5 s2\break }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
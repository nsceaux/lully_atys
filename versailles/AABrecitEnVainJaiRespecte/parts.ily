\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \column {
\livretPers Le Temps
\livretVerse#12 { En vain j’ay respecté la celebre memoire }
\livretVerse#8 { Des heros des siecles passez ; }
\livretVerse#12 { C’est en vain que leurs noms si fameux dans l’histoire, }
\livretVerse#12 { Du sort des noms communs ont esté dispensez : }
\livretVerse#12 { Nous voyons un heros dont la brillante gloire }
\livretVerse#8 { Les a presque tous effacez. }
} #}))

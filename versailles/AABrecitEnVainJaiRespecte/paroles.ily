En vain j'ay res -- pec -- té la ce -- le -- bre me -- moi -- re
Des he -- ros des sie -- cles pas -- sez ;
C'est en vain que leurs noms si fa -- meux dans l'his -- toi -- re,
Du sort des noms com -- muns ont es -- té dis -- pen -- sez ;
Nous voy -- ons un he -- ros dont la bril -- lan -- te gloi -- re
Les a pres -- que tous ef -- fa -- cez,
Nous voy -- ons un he -- ros dont la bril -- lan -- te gloi -- re
Les a pres -- que tous ef -- fa -- cez.
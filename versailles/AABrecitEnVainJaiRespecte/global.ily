\key re \minor
\time 4/4 \midiTempo #80
s1*3
\digitTime\time 3/4
s2.
\time 4/4 s1*2
\digitTime\time 3/4
s2.*2
\time 4/4 s1*5
\digitTime\time 3/4
s2. \bar "|."

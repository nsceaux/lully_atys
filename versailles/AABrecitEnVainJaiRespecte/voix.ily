\tempsClef r4 r8 sib sol8. sol16 sol8. sol16 |
re4 re8. re16 re4 re8 mib |
fa fa fa8. sol16 mib8. re16 do8 do16 re |
sib,4 r8 re16 mi fa8 sol16 la |
sib8 sib16 sib sib8 sib16 la la8 la r fa |
la8. la16 la8. sib16 sol8 sol16 sol sol8 sol16 la |
fad4 r8 re16 re la8 la16 sib |
do'4 la8 la16 la re'8. la16 |
sib4 sib sol8. sol16 sol8. fa16 |
mi4 mi8. fa16 re4 r8 la16 la |
sib4 sib8. sib16 sol4. do'8 |
do'8. si16 si8. do'16 do'4 do' |
sol8 sol mib do re4 re8 re8 |
sol,2. |

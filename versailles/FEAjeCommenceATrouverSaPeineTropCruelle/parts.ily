\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix"
                   #:indent 0)
   (basse-viole #:score-template "score-basse-viole-voix"
                #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Cybele
    \livretVerse#12 { Je commence à trouver sa peine trop cruelle, }
    \livretVerse#8 { Une tendre pitié rapelle }
    \livretVerse#12 { L’Amour que mon couroux croyoit avoir banny, }
    \livretVerse#12 { Ma Rivale n’est plus, Atys n’est plus coupable, }
    \livretVerse#12 { Qu’il est aisé d’aimer un criminel aimable }
    \livretVerse#6 { Aprés l’avoir puny. }
  }
  \column {
    \null
    \livretVerse#8 { Que son desespoir m’espouvante ! }
    \livretVerse#12 { Ses jours sont en peril, et j’en fremis d’effroy : }
    \livretVerse#12 { Je veux d’un soin si cher ne me fier qu’à moy, }
    \livretVerse#12 { Allons… mais quel spectable à mes yeux se presente ? }
    \livretVerse#8 { C’est Atys mourant que je voy ! }
  }
}#}))

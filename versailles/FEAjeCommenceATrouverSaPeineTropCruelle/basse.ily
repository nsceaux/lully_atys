\clef "basse" sol,2 sol~ |
sol fad |
sol fa4 mib8 re |
do1 |
fa4 re8 mib do4 |
sib,2. |
mib2 re |
do1~ |
do2. |
re4. sib,8 la, sol, re re, |
sol,2 sol |
do2. |
re4. sib,8 la, sol, re re, |
sol,2 sol4 re |
mib2 mi |
fa1 | \allowPageTurn
sib2 la8 sol la la, |
re1~ |
re2 re8 do sib, la, |
sol,2. la,4 |

\score {
  <<
    \new ChoirStaff \with { \forceGroupBracket \haraKiriFirst } <<
      \new Staff \with { \cybeleInstr } \withLyrics <<
        \global \keepWithTag #'cybele  \includeNotes "voix"
      >> \keepWithTag #'cybele \includeLyrics "paroles"
      \new Staff \with { \melisseInstr } \withLyrics <<
        \global \keepWithTag #'melisse \includeNotes "voix"
      >> \keepWithTag #'melisse \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
        \modVersion { s1*4 s2.*2 s1*2 s2. s1\break }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
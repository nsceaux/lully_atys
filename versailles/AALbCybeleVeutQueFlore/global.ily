\key sol \major
\once\omit Staff.TimeSignature
\time 2/2 \midiTempo#80 s1*2
\digitTime\time 3/4 \midiTempo#80 s2.*4
\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#160 s2.*27
\time 2/2 \midiTempo#160 s2 \bar "||"
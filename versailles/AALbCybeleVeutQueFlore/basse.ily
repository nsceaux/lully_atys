\clef "basse" %sol,1 |
sol,4 sol mi fad |
sol2 mi |
fad4 sol sol, |
re4. do16 si, la,4 |
sold,4 la,4. sol,8 |
fad,2 fad,8 sol, |
mi,2 la, |
re, re8 mi |
fad2. |
sol2 mi4 |
fa4. sol8 fa mi |
re4. mi8 fa re |
mi2 re4 |
do2 si,4 |
la,4. si,8 do la, |
mi2 si,4 |
do4. si,8 la, sol, |
re4 re' do' |
si la sol |
fad2 sol4 |
do re re, |
sol,2 %{%} sol4 |
do2 do4 |
fa4. sol8 fa mi |
re4. mi8 fa re |
mi2 mi,4 |
la, la8 si do' la |
mi'4 re' do' |
si4. la8 sol fa |
mi re do2 |
re4 re' do' |
si la sol |
fad2 sol4 |
do re re, |
sol,2

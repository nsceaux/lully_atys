\score {
  <<
    \new ChoirStaff \with { \forceGroupBracket \haraKiriFirst } <<
      \new Staff \with { \irisInstr } \withLyrics <<
        \global \keepWithTag #'iris \includeNotes "voix"
      >> \keepWithTag #'iris \includeLyrics "paroles"
      \new Staff \with { \floreInstr } \withLyrics <<
        \global \keepWithTag #'flore \includeNotes "voix"
      >> \keepWithTag #'flore \includeLyrics "paroles"
      \new Staff \with { \melpomeneInstr } \withLyrics <<
        \global \keepWithTag #'melpomene \includeNotes "voix"
      >> \keepWithTag #'melpomene \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \keepWithTag #'basse-continue \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout { }
  \midi { }
}

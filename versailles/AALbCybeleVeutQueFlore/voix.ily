<<
  %% Iris
  \tag #'(iris basse) {
    \clef "vbas-dessus" 
    %r2 r4 \tag #'iris <>^\markup\italic "parlant à Melpomène" r8 sol' |
    re''8 re'' si'8. si'16 do''8 do''16 do'' do''8 do''16 si' |
    si'8 si' r sol' do''8. do''16 do''8 do'' |
    do''4 si'8 si'16 si' si'8. do''16 |
    la'4 r8 la'16 si' do''8 do''16 re'' |
    mi''8. mi''16 dod''8. dod''16 dod''8. re''16 |
    re''4. la'8 la'8. si'16 |
    sol'4. sol'8 sol'4( fad'8) sol' |
    fad'2 fad'4 |
    r4 r re''8. do''16 |
    si'4 si'4. do''8 |
    la'2. |
    re''4 re''8 do'' si'8 la' |
    sold'2 r8 sold' |
    la'4 la' si' |
    do'' do''( si'8) do'' |
    si'4 si' r8 re'' |
    mi''4. re''8 do'' si' |
    la'2 la'4 |
    si' do'' re'' |
    do''2 si'4 |
    si'( la'4.) sol'8 |
    sol'2
    \tag #'(iris) {
      <>^\markup\override #'(line-width . 50) \italic\wordwrap {
        Iris remonte au ciel sur son arc, et la suite de
        Melpomène s’accorde avec la suite de Flore.
      }
      r4 |
      R2.*12 r2
    }
  }
  %% Flore
  \tag #'(flore basse) {
    <<
      \tag #'basse { s1*2 s2.*4 s1 s2.*14 s2 \floreMark }
      \tag #'flore { \clef "vbas-dessus" R1*2 | R2.*4 | R1 | R2.*14 | r4 r }
    >> re''8.  re''16 |\noBreak
    mi''4 mi''4. mi''8 |
    do''2. |
    fa''4 fa''8 mi'' re'' do'' |
    si'2 r8 si' |
    do''4 do''4. do''8 |
    si'4 si' do'' |
    re'' re''4. re''8 |
    mi''4. re''8 do''8. si'16 |
    la'2 la'4 |
    si'4 do'' re'' |
    do''2 si'4 |
    si'( la'4.) sol'8 |
    sol'2
  }
  %% Melpomène
  \tag #'(melpomene) {
    \clef "vbas-dessus" R1*2 | R2.*4 | R1 | R2.*14 |
    r4 r si'8. si'16 |
    do''4 do''4. do''8 |
    la'2. |
    re''4 re''8 do'' si'8 la' |
    sold'2 r8 sold' |
    la'4 mi'4. fad'8 |
    sol'4 sol'4 la' |
    si'4 si'4. si'8 |
    do''4. si'8 la'8. sol'16 |
    fad'2 fad'4 |
    sol' la' si' |
    la'2 sol'4 |
    sol'( fad'4.) sol'8 |
    sol'2
  }
>>

\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-tous #:score "score-basse")
   (basse #:score "score-basse")
   (basse-viole #:score-template "score-basse-viole-voix" #:indent 0)
   (flutes-hautbois #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPersDidas Iris parlant à Melpomene
    \livretVerse#12 { Cybele veut que Flore aujourd’huy vous seconde. }
    \livretVerse#12 { Il faut que les Plaisirs viennent de toutes parts, }
    \livretVerse#12 { Dans l’empire puissant, où regne un nouveau Mars, }
    \livretVerse#8 { Ils n’ont plus d’autre asile au monde. }
    \livretVerse#12 { Rendez-vous, s’il se peut, dignes de ses regards ; }
    \livretVerse#8 { Joignez la beauté vive & pure }
    \livretVerse#6 { Dont brille la nature, }
    \livretVerse#8 { Aux ornements des plus beaux arts. }
    \livretPers Melpomene & Flore
    \livretVerse#12 { Rendons-nous, s’il se peut, dignes de ses regards ; }
    \livretVerse#8 { Joignons la beauté vive & pure }
  }
  \column {
    \livretVerse#6 { Dont brille la nature, }
    \livretVerse#8 { Aux ornements des plus beaux arts. }
    \livretPers Le Temps, & le Chœur des Heures
    \livretVerse#8 { Preparez de nouvelles festes, }
    \livretVerse#12 { Profitez du loisir du plus grand des heros ; }
    \livretPers Le Temps, Melpomene & Flore
    \livretVerse#8 { Preparez/Preparons de nouvelles festes }
    \livretVerse#12 { Profitez/Profitons du loisir du plus grand des heros. }
    \livretPers Tous ensemble
    \livretVerse#8 { Le temps des Jeux, et du repos, }
    \livretVerse#12 { Luy sert à mediter de nouvelles conquestes. }
  }
}#})
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPersDidas Iris parlant à Melpomene
    \livretVerse#12 { Cybele veut que Flore aujourd’huy vous seconde. }
    \livretVerse#12 { Il faut que les Plaisirs viennent de toutes parts, }
    \livretVerse#12 { Dans l’empire puissant, où regne un nouveau Mars, }
    \livretVerse#8 { Ils n’ont plus d’autre asile au monde. }
    \livretVerse#12 { Rendez-vous, s’il se peut, dignes de ses regards ; }
    \livretVerse#8 { Joignez la beauté vive & pure }
    \livretVerse#6 { Dont brille la nature, }
    \livretVerse#8 { Aux ornements des plus beaux arts. }
  }
  \column {
    \livretPers Melpomene & Flore
    \livretVerse#12 { Rendons-nous, s’il se peut, dignes de ses regards ; }
    \livretVerse#8 { Joignons la beauté vive & pure }
    \livretVerse#6 { Dont brille la nature, }
    \livretVerse#8 { Aux ornements des plus beaux arts. }
  }
}#}))

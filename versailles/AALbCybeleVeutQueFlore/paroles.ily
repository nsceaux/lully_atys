%% Iris
\tag #'(iris basse) {
  - be -- le veut que Flore au -- jour -- d'huy vous se -- con -- de.
  Il faut que les Plai -- sirs vien -- nent de tou -- tes parts,
  Dans l'em -- pi -- re puis -- sant, où regne un nou -- veau Mars,
  Ils n'ont plus d'autre a -- sile au mon -- de.
  Ren -- dez- vous, s'il se peut, di -- gnes de ses re -- gards ;
  Joi -- gnez la beau -- té vive et pu -- re
  Dont bril -- le la na -- tu -- re,
  Aux or -- ne -- ments des plus beaux arts.
}
\tag #'(melpomene flore basse) {
  Ren -- dons- nous, s'il se peut, di -- gnes de ses re -- gards ;
  Joi -- gnons la beau -- té vive et pu -- re
  Dont bril -- le la na -- tu -- re,
  Aux or -- ne -- ments des plus beaux arts.
}


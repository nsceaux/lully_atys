\score {
  <<
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \atysInstr } \withLyrics <<
        \global \keepWithTag#'atys \includeNotes "voix"
      >> \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout { }
  \midi { }
}
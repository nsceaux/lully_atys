\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \column {
   \livretPers Atys
   \livretVerse#12 { Indigne que je suis des honneurs qu’on m’adresse, }
   \livretVerse#12 { Je dois les recevoir au nom de la Déesse ; }
   \livretVerse#12 { J’ose, puis qu’il luy plaist, luy presenter vos vœux : }
   \livretVerse#7 { Pour le prix de vostre zele, }
   \livretVerse#7 { Que la puissante Cybele }
   \livretVerse#7 { Vous rende à jamais heureux. }
} #}))

\clef "basse" sol,1 |
sol2 mi |
fa1 |
mib2 re |
do1 |
sol4. fa8 mib4 |
re2 fad, |
sol,4 sol2 |
si,2 do |
re re, |
sol, sol4 fa |

\clef "dessus" sol''2. sol''4 |
fa''2. fa''4 |
fa''4. mi''8 mi''4. fa''8 |
re''4. re''8 re''4. mi''8 |
do''4. do''8 do''4. re''8 |
si'4. si'8 si'4. mi''8 |
dod''2 re''4. re''8 |
re''2 do''!4. do''8 |
do''2 sib'4. sib'8 |
sib'4. la'8 la'4 si'8 do'' |
si'4. re''8 re''4. mi''8 |
fa''4. fa''8 fa''4. sol''8 |
mi''4. fa''8 sol''2 |
do''4. do''8 do''4. re''8 |
si'2 sol''4. sol''8 |
sol''2 fa''4. fa''8 |
fa''2 mi''4. mi''8 |
mi''4. re''8 re''4. do''8 |
do''2 sol''4. sol''8 sol''2 fa''4. fa''8 |
fa''2 mi''4. mi''8 |
mi''4. re''8 re''4. do''8 |
do''1 |

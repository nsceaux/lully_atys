\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \flInstr } << \global \includeNotes "flute1" >>
      \new Staff \with { \flInstr } << \global \includeNotes "flute2" >>
      \new Staff \with { \bcInstr } <<
        \global \keepWithTag #'basse-continue \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
    \new StaffGroup <<
      \new Staff \with { \dvInstr } << \global \includeNotes "dessus" >>
      \new Staff \with { \hcvInstr } << \global \includeNotes "haute-contre" >>
      \new Staff \with { \tvInstr } << \global \includeNotes "taille" >>
      \new Staff \with { \qvInstr } << \global \includeNotes "quinte" >>
      \new Staff \with { \bvInstr } <<
        \footnoteHere#'(0 . 0) \markup {
          F-V : la partie de BVn n’existe pas. Ici la partie provient de Ballard 1689.
        }
        \global \keepWithTag #'basse \includeNotes "basse"
      >>
    >>
  >>
  \layout { }
  \midi { }
}

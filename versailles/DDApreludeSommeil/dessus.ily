\clef "dessus" re''4( mib'') re''( do'') |
sib'( la') sib'( do'') |
la'1 |
sol' |
la'4( sib') do''( re'') |
sib' sol' r2 |
R1*4 |
re''4( mib'') re''( do'') |
si'1 |
\afterGrace do''1( re''16) |
\afterGrace do''1( re''16) |
si'4 r r2 |
R1*4 |
sol''4( \ficta lab'') sol''( fa'') |
mib''( fa'') mib''( re'') |
do''( re'') do''( sib') |
la'( sib') do'' re''8 do'' |
sib'2. do''8 re'' |
la'4. la'8 la'4. sol'8 |
sol'2 r |
R1*4 |
\once\tieDashed sib'2~ sib'4. sib'8 |
\afterGrace sib'1( do''16) |
la'1 |
sib'2 sib'4. la'16 sib' |
do''4 r r2 |
R1*3 |
\slurDashed fa''4( sol'') fa''( mib''?) | \slurSolid
re''4( mi'') fa''( sol'') |
mi''( fa'') \slurDashed sol''( la'') |
fa''( sol'') la''( sib'') |
sol''1~ |
sol''2 fad''4. mi''16 fad'' | \slurSolid
sol''2 r |
R1*3 |
re''4( do'') sib'( la') |
sol'2 do'' |
la'4 r r2 |
R1 |
r2 r4 re'' |
\afterGrace sol''1( la''16) |
\afterGrace sol''1( la''16) |
sol''2 fad''4. mi''16 fad'' |
sol''2 r |

\clef "basse" % re,2. |
re2. |
do |
sib, |
la, |
re |
do |
sib, |
la, |
re1 |
re2 la,4 mi, |
fa, fa mi2 |
re2. mi4 |
fa dod re2 |
re2. |
la4 fad2 |
sol4 sol, re8 do sib,4 |
la,2. |
re2. | do | sib, | la, |
re | do | sib, | la, |
re | do | sib, | la, |
re | do | sib, | la, |
re | do | sib, | la, |
re | do | sib, | la, |
re | do | sib, | la, |
re2. | \allowPageTurn
%%
re1 |
mi2. |
la,2 la, |
la,2 re |
re'2. |
la4 sib sol |
do'4. sib8 la4 |
sib4. la8 sol fa |
do'4 do2 |
fa2. |
mi4. fa8 mi re |
dod2. |
re2 mi8 la, |
mi,2. |
la,4 la4. sol8 |
fad2. |
sol |
fa!8 sol la4 la, |
re2 la,8 sol, |
fad,2. |
sol,4 sol2 |
fa8 sol la4 la, |
re1 | 
re |
la, |
re2 mi4 mi, |
la,2 la, |
re4. do8 sib,2 |
fad, sol, |
sol mi |
fa2. fa4 |
mi1 |
re |\allowPageTurn
fad,2 sol, |
do re |
mi fa8 re mi mi, |
la,2 re |
sol, la, |
re4 mi fa~ |
fa mi2 |
fa2. |
fa2 mi8 re |
dod2 re4 |
mi mi,2 |
la,4 la sol |
fad2. |
sol2 la4 |
sib sol4. fa8 |
do'4. sib8 la4 |
sib2 sib4~ |
sib4 do' do |
fa fa8 mi re4 |
sol2. |
la4 la8 sol fa mi |
re do sib,4. la,8 |
la,4 la8 sol fad4 |
sol sol8 fa mi4 |
la8 re la,2 |
re2 re |
re2 mi |
fa fa8 mib re do |
sib,2 si, |
do4 dod re2 |
mi fad |
sol4 sol, re8 do sib,4 |
la,2 la4 fad |
sol2 sold |
la4. sol8 fa2 |
sol4. sol8 la sib la la, |
re1 |
re |
dod2 re |
re do! |
sib,1 |
la,2. |
re2 re4 |
la2 la8 fa |
sol2. |
do8 fa, sol,2 |
do2 do4 |
re2. |
mi2 la,4 |
mi,2. |
la,4 la2 |
re2 re4 |
sol2 sol8 fa |
mi fa sol4 sol, |
do2 do4 |
re2 sol,4 |
re,2. |
sol,4 sol2 |
re do8 sib, |
la, sol, fa,4 fa |
sib,4 si,2 |
do4 do'8 sib la4 |
sol2 fa4 |
do' do2 |
fa2 fa4 |
mi2 re4 |
la2 la,4 |
re2 do8 si, |
\once\set Staff.whichBar = "|"

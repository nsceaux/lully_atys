\key la \minor
\digitTime\time 3/4 \midiTempo#120 s2.*8
\time 4/4 \midiTempo#80 s1*5
\digitTime\time 3/4 s2.*2
\time 4/4 s1
\digitTime\time 3/4 \midiTempo#120 s2.*30
\time 4/4 s1
\digitTime\time 3/4 \midiTempo#80 s2.
\time 4/4 s1*2
\digitTime\time 3/4 \midiTempo#120 s2.*18
\time 4/4 \midiTempo#80 s1*16
\digitTime\time 3/4 \midiTempo#120 s2.*20
\time 4/4 \midiTempo#80 s1*11
\digitTime\time 2/2 \midiTempo#160 s1*5
\digitTime\time 3/4 s2.*27 \bar "||"

\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Sangaride
    \livretVerse#6 { Atys est trop heureux. }
    \livretPers Doris
    \livretVerse#12 { L’amitié fut toûjours égale entre vous deux, }
    \livretVerse#8 { Et le sang d’assez prés vous lie : }
    \livretVerse#12 { Quel que soit son bon-heur, luy portez-vous envie ? }
    \livretVerse#12 { Vous, qu’aujourd’huy l’Hymen avec de si beaux nœuds }
    \livretVerse#8 { Doit unir au Roy de Phrygie ? }
    \livretPers Sangaride
    \livretVerse#6 { Atys, est trop heureux. }
    \livretVerse#12 { Souverain de son cœur, maistre de tous ses vœux, }
    \livretVerse#8 { Sans crainte, sans melancolie, }
    \livretVerse#12 { Il joüit en repos des beaux jours de sa vie ; }
    \livretVerse#12 { Atys ne connoît point les tourmens amoureux, }
    \livretVerse#6 { Atys est trop heureux. }
    \livretPers Doris
    \livretVerse#12 { Quel mal vous fait l’Amour ? vostre chagrin m’estonne. }
    \livretPers Sangaride
    \livretVerse#12 { Je te fie un secret qui n’est sceu de personne. }
    \livretVerse#8 { Je devrois aimer un Amant }
    \livretVerse#6 { Qui m’offre une Couronne ; }
    \livretVerse#6 { Mais, helas ! vainement }
    \livretVerse#6 { Le Devoir me l’ordonne, }
    \livretVerse#6 { L’Amour, pour mon tourment, }
    \livretVerse#6 { En ordonne autrement. }
    \livretPers Doris
    \livretVerse#12 { Aimeriez-vous Atys, luy dont l’indifference }
    \livretVerse#12 { Brave avec tant d’orgüeil l’Amour et sa puissance ? }
  }
  \column {
    \livretPers Sangaride
    \livretVerse#12 { J’aime, Atys, en secret, mon crime, est sans témoins. }
    \livretVerse#12 { Pour vaincre mon amour, je mets tout en usage, }
    \livretVerse#12 { J’appelle ma raison, j’anime mon courage ; }
    \livretVerse#8 { Mais à quoy servent tous mes soins ? }
    \livretVerse#8 { Mon cœur en souffre davantage, }
    \livretVerse#6 { Et n’en aime pas moins. }
    \livretPers Doris
    \livretVerse#8 { C’est le commun deffaut des Belles. }
    \livretVerse#8 { L’ardeur des conquestes nouvelles }
    \livretVerse#12 { Fait negliger les cœurs qu’on a trop tost charmez, }
    \livretVerse#12 { Et les Indifferents sont quelquefois aimez }
    \livretVerse#8 { Aux dépens des Amants fidelles. }
    \livretVerse#12 { Mais vous vous esposez à des peines cruelles. }
    \livretPers Sangaride
    \livretVerse#12 { Toûjours aux yeux d’Atys je seray sans appas ; }
    \livretVerse#12 { Je le sçay, j’y consens, je veux, s’il est possible, }
    \livretVerse#8 { Qu’il soit encor plus insensible ; }
    \livretVerse#13 { S’il me pouvoit aimer, que deviendrois-je ? helas ! }
    \livretVerse#12 { C’est mon plus grand bon-heur qu’Atys ne m’aime pas. }
    \livretVerse#13 { Je pretens estre heureuse, au moins, en apparence ; }
    \livretVerse#12 { Au destin d’un grand Roy je me vais attacher. }
    \livretPers\line { Sangaride, & Doris }
    \livretVerse#12 { Un amour malheureux dont le devoir s’offence, }
    \livretVerse#8 { Se doit condamner au silence ; }
    \livretVerse#12 { Un amour malheureux qu’on nous peut reprocher, }
    \livretVerse#8 { Ne sçauroit trop bien se cacher. }
  }
}#}))

<<
  \tag #'(sangaride basse) {
    \sangarideClef R2.*5 |
    r4 r la'4 |
    re''2. |
    r8 re'' re''4 dod'' |
    re''2 r |
    <<
      \tag #'basse { s1*4 s2.*2 s1 s2.*2 \sangarideMark }
      \tag #'sangaride { R1*4 R2.*2 R1 R2.*2 }
    >>
    r4 r la'4 |
    re''2. |
    r8 re'' re''4 dod'' |
    re''2 la'8 sib'8 |
    do''4 do'' la' |
    re''2. |
    mi''4 mi''8 mi'' mi''[ re''16] mi'' |
    fa''2 sib'4 |
    sib'4.( la'8) la'4 |
    r la'8 la' la' sol' |
    la'4 la' mi''8 mi'' |
    fa''4 fa'' re'' |
    mi''2 mi''8 fa'' |
    re''2 dod''8 re'' |
    dod''4 la'4. mi'8 |
    fa'4. sol'8 la' si'8 |
    do''4 la'4. la'8 |
    la'4 sol'8[ fa'] sol'4 |
    la'2. |
    r4 r sib'4 |
    sib'4. la'8 la' la' |
    la'4 sol'4. sol'8 |
    sol'2 fa'8 mi' |
    fa'2. |
    r4 r la'4 |
    re''2. |
    r8 re'' re''4 dod'' |
    re''2. |
    <<
      \tag #'basse { s1 s2. s2 \sangarideMark }
      \tag #'sangaride { R1 R2. r2 }
    >> r8 la'16 la' la'8 la'16 la' |
    mi'8 mi'16 fa' sol'8 fa'16 mi' fa'4 fa' |
    r re''8 re'' la'8. si'16 |
    do''4 sib'4. la'8 |
    sol'2 r8 do'' |
    re''4. re''8 mi''8 fa'' |
    fa''2( mi''4) |
    fa'' do''4. do''8 |
    dod''2 dod''8 re'' |
    mi''2 mi''8 fa'' |
    re''2 si'8 do'' |
    do''4( si'2) |
    la'2 do''4 |
    do''4. do''8 do''[ si'16] do'' |
    si'2 si'8 dod'' |
    re''4 dod''4. re''8 |
    re''2 r8 do'' |
    do''4. do''8 do''[ si'16] do'' |
    si'2 si'8 dod'' |
    re''4 dod''4. re''8 |
    re''2 <<
      \tag #'basse { s2 s1*3 s2 \sangarideMark }
      \tag #'sangaride { r2 R1*3 r2 }
    >> r4 la'8 la' |
    fad'4 fad'8 fad' sol'4. sol'8 |
    re''8 re'' re'' la' sib'4. re''8 |
    sol'4 sol'8 la'16 sib' do''8 do''16 do'' sol'8 la'16 sib' |
    la'4 la'8 r16 do'' fa''8. fa''16 la'8. sib'16 |
    do''4. do''8 dod''4 dod''8 re''16 mi'' |
    fa''4 fa''8 r r8 la' la' si' |
    do'' do'' do'' re'' si'4 re'' |
    mi''4 mi'' re''8 do'' si'8. la'16 |
    sold'4 sold'8 si'16 do'' la'4 la'8 sold' |
    la'2 <<
      \tag #'basse { s2 s1 s2.*20 s1*2 s4. \sangarideMark }
      \tag #'sangaride { r2 R1 R2.*20 R1*2 r4 r8 }
    >> do''8 do'' la' sib' do'' |
    re''4 sib'8 sib' sol'4 sol'8 sol' |
    mi'4 la'8 la' fa'4 sib'8 sib' |
    sol'4. do''8 do''8. do''16 sib'8. la'16 |
    sib'8 sib' r16 sib' sib' do'' la'8 la' la' sol' |
    la'4 la' dod''8 dod''16 dod'' re''8. re''16 |
    si'4. si'8 si' dod'' re''8. mi''16 |
    dod''2 fa''8 fa''16 mi'' re''8. do''16 |
    si'4. si'8 dod'' re'' re'' dod'' |
    re''2 r4 la'8 la' |
    fa'4 sol'8 la' sib'4. sib'8 |
    la' la' la' mi' fa'4 fa' |
    r la'8 si' do''4 do''8 la' |
    re''4 re'8 mi' fa'4 fa'8 sol' |
    la'2. |
  }
  \tag #'(doris basse) {
    <<
      \tag #'basse { s2.*8 s1 \dorisMark }
      \tag #'doris { \dorisClef R2.*8 R1 }
    >>
    r8 fa'16 sol' la'8 la'16 si'16 do''8. do''16 do'' sib'! sib' la' |
    la'4 r8 do''16 do'' dod''8 dod''16 dod'' dod''8. re''16 |
    re''4 re'' r8 sib'16 sib' sib'8 sib'16 la' |
    la'8. la'16 la' sol' fa' mi' fa'4 fa' |
    re'' r8 re''16 re'' la'8. si'16 |
    do''8. do''16 do''8 do'' sib'! la' |
    sib'4 r8 sib'16 do'' la'8 la' re''8 re''16 sol' |
    la'4 la' r |
    R2. |
    <<
      \tag #'basse { s2.*28 s4 \dorisMark }
      \tag #'doris { R2.*28 r4 }
    >> r8 re'' si'4 si'8 si'16 si' |
    sold'4 si'8 si'16 dod'' re''8. mi''16 |
    dod''4 dod'' <<
      \tag #'basse { s2 s1 s2.*18 s2 \dorisMark }
      \tag #'doris { r2 R1 R2.*18 r2 }
    >> la'8 la'16 la' la'8. la'16 |
    fa'4 r re''8 re''16 re'' la'8 si' |
    do''4 do'' do''8 do''16 do'' re''8 mi'' |
    fa''4. re''8 si'8 si' do''8 re'' |
    dod''4 dod'' <<
      \tag #'basse { s2 s1*9 s2 \dorisMark }
      \tag #'doris { r2 R1*9 r2 }
    >> fa'8 fa'16 fa' sol'8. la'16 |
    sol'4. fa'8 mi' mi' r la' |
    fa'4 sol' la' |
    sib' sol'4. do''8 |
    la'2 fa'4 |
    do''4 do''8 do'' dod''8. re''16 |
    mi''4. mi''8 re'' do'' |
    do''4( si'4.) la'8 |
    la'2. |
    re''4 re''8 do'' sib' la' |
    sib'4. sib'8 do''4~ |
    do''8 re'' sib'4. la'8 |
    sol'2 do''8 do'' |
    re''2 re''8 do'' |
    sib' la' la'4( sol') |
    fa'2. |
    sib'4 sib'8 la' sol'8. fa'16 |
    mi'4. mi'8 fa'4~ |
    fa'8 sol' sol'4. la'8 |
    la'2 la'8 la' |
    si'2 si'8 si' |
    dod'' re'' re''4( dod'') |
    re''2 r8 la' la'16 la' re'' la' |
    sib'4 sib'8 sib' sib'4 sib'8. do''16 |
    la'4 la'8 \tag #'doris { r8 r2 R1*13 R2. }
  }
>>
<<
  \tag #'(sangaride basse) {
    r4 r fa''8 fa'' |
    mi''2 mi''8 fa'' |
    re''4. re''8 re'' re'' |
    mi'' fa'' mi''4( re'') |
    do''2 mi''4 |
    mi'' re''4. do''8 |
    si'4 si'4. do''8 |
    do''4( si'2) |
    la' mi''8 mi'' |
    fa''2 fa''8 mi'' |
    re''2 re''8 re'' |
    mi''4 re''4. do''8 |
    do''2 do''8 do'' |
    do''2 sib'4 |
    la'2 la'8 sib' |
    sol'2 sib'8 do'' |
    la'2 la'8 sib' |
    do''2 do''8 do'' |
    re''2 mi''8. fa''16 |
    mi''2 do''8 do'' |
    sib'2 la'4 |
    sol'2 sol'8 la' |
    fa'2 do''8 do'' |
    dod''2 re''4 |
    re''4.( dod''8) dod''8 re'' |
    re''2. |
  }
  \tag #'doris {
    r4 r la'8 si' |
    do''2 do''8 re'' |
    si'4. si'8 si' si' |
    do'' re'' do''4( si') |
    do''2 do''4 |
    do''4 si'4. la'8 |
    sold'4 sold'4. la'8 |
    la'4( sold'2) |
    la'2 dod''8 dod'' |
    re''2 la'8 la' |
    si'2 si'8 si' |
    do''4 si'4. do''8 |
    do''2 mi'8 mi' |
    fad'2 sol'4 |
    sol'4.( fad'8) fad' sol' |
    sol'2 re'8 mi' |
    fa'2 fa'8 sol' |
    la'2 la'8 la' |
    la'4.( sol'8) sol'8. sol'16 |
    sol'2 la'8 la' |
    mi'2 fa'4 |
    fa'4.( mi'8) mi' fa' |
    fa'2 la'8 la' |
    sol'2 fa'4 |
    mi'2 mi'8 fa' |
    re'2. |
  }
>>
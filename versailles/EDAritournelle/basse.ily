\clef "basse" do4 do'4. do'8 |
sol4 do'4. do'8 |
fa4 sib4. sib8 |
mib4 lab fa |
sol2 sol,4 |
do4 do'4. do'8 |
sol4 do'4. do'8 |
fa4 sib4. sib8 |
mib4 lab fa |
sol2 fa4 |
mi2 mi4 |
fa2 re4 |
sol2 do4 |
fa, sol,2 |
do2. |

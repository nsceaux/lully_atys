\clef "dessus" r4 sol''4. la''8 |
sib''4 sol''4. sol''8 |
lab''4 fa''4. fa''8 |
sol''4 mib'' fa'' |
re''2 re''4 |
r4 sol''4. la''8 |
sib''4 sol''4. sol''8 |
lab''4 fa''4. fa''8 |
sol''4 mib'' fa'' |
re''2 re''4 |
r sol''4. sol''8 |
do''4 fa''4. fa''8 |
fa''4 re'' mib''~ |
mib''8 re'' re''4. do''8 |
do''2. |

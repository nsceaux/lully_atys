\clef "dessus" r4 mib''4. mib''8 |
re''4 mi''4. mi''8 |
fa''4 re''4. re''8 |
mib''4 do'' re'' |
si'2 si'4 |
r4 mib''4. mib''8 |
re''4 mi''4. mi''8 |
fa''4 re''4. re''8 |
mib''4 do'' re'' |
si'2 si'4 |
r do'' sib' |
la'4 re''4. re''8 |
re''4 si' do''~ |
do''8 re'' si'4. do''8 |
do''2. |

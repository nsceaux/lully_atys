\clef "dessus" sol''2~ sol''8 sol'' fad'' sol'' |
si'4 %{ fad' %} sol'4 re''4. do''16 si' |
la'4. si'8 do''4. do''8 |
dod''2 re''4. re''8 |
mi''4. fad''8 sol''4. la''8 |
fad''2. fad''8 fad'' |
sold''2 la''4. la''8 |
la''4 sol''8 fad'' sol''4. sol''8 |
sol''4 la''8 mi'' fa''4. sol''8 |
mi''4. mi''8 mi''4. re''8 |
re''2. re''8 re'' |
mi''2 do''4. do''8 |
do''4. si'16 la' si'4. do''8 |
la'4. la'8 la'4. sol'8 |
sol'4. re''8 re''4. mi''8 |
fa''4. fa''8 fa''4. sol''8 |
mi''2 do''4. do''8 |
do''4. si'16 la' si'4. do''8 |
la'4. la'8 la'4. sol'8 |
sol'1 |

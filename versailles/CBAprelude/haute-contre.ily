\clef "haute-contre" si'2~ si'8 si' la' si' |
sol'4. sol'8 sol'2 |
fad'4. sol'8 la'4. la'8 |
mi'2 fad'4. fad'8 |
sol'4. fad'8 mi'2 |
la'2. re''8 re'' |
si'2 do''4. do''8 |
re''2. mi''4 |
dod''?2 re''4. re''8 |
re''2 do''4. do''8 |
do''2 si'4. si'8 |
si'2 la'4. la'8 |
fad'2 sol'~ |
sol' fad'4. sol'8 |
sol'4. si'8 si'4. do''8 |
re''2. re''4 |
sol'2 la'4. la'8 |
fad'2 sol'~ |
sol' fad'4. sol'8 |
sol'1 |

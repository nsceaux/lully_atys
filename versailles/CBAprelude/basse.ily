\clef "basse" sol,2 \once\tieDashed sol~ |
sol8 sol fad sol si,4 sol, |
re4. do16 si, la,4. la8 |
sol2 fad |
mi1 |
re2 re'4. re'8 |
re'2 do' |
si~ si8 si la sol |
la2 re |
la, la4 sol |
fad2 sol4 sol, |
do2~ do8 do si, la, |
re2 sol, |
re,1 |
sol,2 sol |
si,1 |
\once\tieDashed do2~ do8 do si, la, |
re2 sol, |
re,1 |
sol,1 |

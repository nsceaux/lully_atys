\score {
  <<
    \new ChoirStaff \with { \forceGroupBracket \haraKiri } <<
      \new Staff \with { \sangarideInstr } \withLyrics <<
        \global \keepWithTag #'sangaride \includeNotes "voix"
      >> \keepWithTag #'sangaride \includeLyrics "paroles"
      \new Staff \with { \dorisInstr }\withLyrics <<
        \global \keepWithTag #'doris \includeNotes "voix"
      >> \keepWithTag #'doris \includeLyrics "paroles"
      \new Staff \with { \idasInstr } \withLyrics <<
        \global \keepWithTag #'idas \includeNotes "voix"
      >> \keepWithTag #'idas \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
        \modVersion {
          s1*12 s2. s1*5 s2.*5 s1 s2. s1 s1 s1*2 s2.\break
          s2.*21 s1*5 s2.*4 s1\break
          s2.*21 s1*10 s2.*5 s1*7 s2.\break
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}

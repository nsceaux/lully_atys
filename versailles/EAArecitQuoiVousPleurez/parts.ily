\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Doris
    \livretVerse#12 { Quoy, vous pleurez ? }
    \livretPers Idas
    \livretVerse#12 { \transparent { Quoy, vous pleurez ? } D’où vient vostre peine nouvelle ? }
    \livretPers Doris
    \livretVerse#12 { N’osez-vous découvrir vostre amour à Cybele ? }
    \livretPers Sangaride
    \livretVerse#12 { Helas ! }
    \livretPers\line { Doris & Idas }
    \livretVerse#12 { \transparent { Helas ! } Qui peut encor redoubler vos ennuis ? }
    \livretPers Sangaride
    \livretVerse#14 { Helas ! j’aime… helas ! j’aime… }
    \livretPers\line { Doris & Idas }
    \livretVerse#14 { \transparent { Helas ! j’aime… helas ! j’aime… } Achevez. }
    \livretPers Sangaride
    \livretVerse#14 { \transparent { Helas ! j’aime… helas ! j’aime… Achevez. } Je ne puis. }
    \livretPers \line { Doris & Idas }
    \livretVerse#12 { L’Amour n’est guere heureux lorsqu’il est trop timide. }
    \livretPers Sangaride
    \livretVerse#6 { Helas ! j’aime un perfide }
    \livretVerse#6 { Qui trahit mon amour ; }
    \livretVerse#12 { Le Deesse aime Atys, il change en moins d’un jour, }
    \livretVerse#12 { Atys comblé d’honneurs n’aime plus Sangaride. }
    \livretVerse#6 { Helas ! j’aime un perfide }
    \livretVerse#6 { Qui trahit mon amour. }
    \livretPers\line { Doris & Idas }
    \livretVerse#12 { Il nous montroit tantost un peu d’incertitude ; }
    \livretVerse#12 { Mais qui l’eust soupçonné de tant d’ingratitude ? }
    \livretPers Sangaride
    \livretVerse#12 { J’embarassois Atys, je l’ay veu se troubler : }
    \livretVerse#8 { Je croyois devoir reveler }
    \livretVerse#6 { Nostre amour à Cybele ; }
    \livretVerse#6 { Mais l’Ingrat, l’Infidelle, }
    \livretVerse#8 { M’empéchoit toûjours de parler. }
    \livretPers\line { Doris & Idas }
    \livretVerse#12 { Peut-on changer si-tost quand l’Amour est extrême ? }
  }
  \column {
    \null
    \livretVerse#6 { Gardez-vous, gardez-vous }
    \livretVerse#8 { De trop croire un transport jaloux. }
    \livretPers Sangaride
    \livretVerse#12 { Cybele hautement declare qu’elle l’aime, }
    \livretVerse#12 { Et l’Ingrat n’a trouvé cét honneur que trop doux ; }
    \livretVerse#12 { Il change en un moment, je veux changer de mesme, }
    \livretVerse#12 { J’accepteray sans peine un glorieux espoux, }
    \livretVerse#12 { Je ne veux plus aimer que la grandeur supresme. }
    \livretPers\line { Doris & Idas }
    \livretVerse#12 { Peut-on changer si-tost quand l’Amour est extrême ? }
    \livretVerse#6 { Gardez-vous, gardez-vous }
    \livretVerse#8 { De trop croire un transport jaloux. }
    \livretPers Sangaride
    \livretVerse#8 { Trop heureux un cœur qui peut croire }
    \livretVerse#8 { Un dépit qui sert à sa gloire. }
    \livretVerse#12 { Revenez ma Raison, revenez pour jamais, }
    \livretVerse#12 { Joignez-vous au Dépit pour estouffer ma flâme, }
    \livretVerse#12 { Reparez, s’il se peut, les maux qu’Amour m’a faits, }
    \livretVerse#8 { Venez restablir dans mon ame }
    \livretVerse#8 { Les douceurs d’une heureuse paix ; }
    \livretVerse#12 { Revenez, ma Raison, revenez pour jamais. }
    \livretPers\line { Idas & Doris }
    \livretVerse#8 { Une infidelité cruelle }
    \livretVerse#8 { N’efface point tous les appas }
    \livretVerse#4 { D’un infidelle, }
    \livretVerse#8 { Et la Raison ne revient pas }
    \livretVerse#6 { Si-tost qu’on l’a rappelle. }
    \livretPers Sangaride
    \livretVerse#7 { Après une trahison }
    \livretVerse#7 { Si la raison ne m’éclaire, }
    \livretVerse#7 { Le dépit et la colere }
    \livretVerse#7 { Me tiendront lieu de raison. }
    \livretPers \line { Sangaride, Doris, Idas. }
    \livretVerse#8 { Qu’une premiere amour est belle ? }
    \livretVerse#8 { Qu’on a peine à s’en dégager ! }
    \livretVerse#8 { Que l’on doit plaindre un cœur fidelle }
    \livretVerse#8 { Lorsqu’il est forcé de changer. }
  }
}#}))

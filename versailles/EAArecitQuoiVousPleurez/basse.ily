\clef "basse" sol,1 |
do4. si,8 la, sol, fad,4 |
sol,2 sol |
fad4 mi re2 |
sol, sol4 sold |
la8 fad sol sol, re2 |
sol4. fa8 mi4. mi8 |
fad2 sol4 sol, |
do2 re4 re, |
sol,2 sol~ |
sol4 fad mi2 |
re2 sol8 fa mi re |
do2 re8 re, |
sol,4 sol fad2 |
mi fad |
sol sold |
la dod |
re sol8 fa mi re |
do2 re8 re, |
sol,4 sol8 sol mi4 |
fa re2 |
mi4 sold2 |
la4. la8 si8 do' |
re'2 re4. do8 |
si,2. |
do1 |
si,2 la, |
sol,4 sol la2 |
si4 sol fad8 mi si si, |
mi2. | \allowPageTurn
sol4 la si |
do'2 la4 |
re' si4. si8 |
fad2 sol4 |
mi2. |
re2 mi4 |
fad2 fad4 |
sol2 fa4 |
mi4 re do |
sol2 sol,4 |
do2 re4 |
mi2 fad4 |
sol2 mi4 |
la2 re4 |
la,2. |
re4 re mi |
fa2. |
mi4 mi fad!4 |
sol2 la4 |
si2 do'4 |
re'2 re4 |
sol,2 sol |
do re |
sold, la, |
si, mi |
dod1 |
re2. |
la2 la,4 |
mi2. |
la4 fa mi |
re1 | \allowPageTurn
sol4 la si |
do'2 la4 |
re' si4. si8 |
fad2 sol4 |
mi2. |
re2 mi4 |
fad2 fad4 |
sol2 fa4 |
mi re do |
sol2 sol,4 |
do2 re4 |
mi2 fad4 |
sol2 mi4 |
la2 re4 |
la,2. |
re4 re mi |
fa2. |
mi4 mi fad!4 |
sol2 la4 |
si2 do'4 |
re'2 re4 |
sol2. fad4 |
mi2 re4 do |
si, sold, la, sold,16 la, si,8 |
mi4 mi8 fad sol4 sol8 la |
si4 si8 do' re'4 re |
sol2 sol,~ | \allowPageTurn
sol,4 sol8 la si2 |
do'4 re' mi' re'8 do' |
si2 do'4 do'8 si |
la2 re'4 re |
sol2 sold4 |
la2 la8 la, |
mi2 mi8 re |
do4 fa, sol, |
do sol8. la16 si8 do' |
re'2 re~ |
re dod |
re sol,~ |
sol,4 sol8 la si2 |
do'4 re' mi' re'8 do' |
si2 do'4 do'8 si |
la2 re'4 re |
sol2. | \allowPageTurn
sol2. fa4 mi4. re8 |
do2. do'4 re'2 |
mi'2 re'4 do' |
si2 la4 sol |
re'2 re |
sol,4 sol sol4 mi |
fa2. re4 |
mi do re mi |
fa re mi mi, |
la, la la si |
dod'1 |
re'2 re'4 do' |
si2 fad |
sol la4 si |
do' la re' si |
mi' do' re' re |
sol,2 sol |\allowPageTurn
mi2. mi8 re |
do1 |
re4 si,8 do re4 re, |
sol,2 sol4. sol8 |
do2 re |
mi si, |
do re4 re, |
sol,2 sol4 fa |
mi2 fa4 re |
mi2 do |
re2. re4 |
mi2 mi, |
la,4 la la si |
dod'1 |
re'2 re |
la2. la4 |
re'2 sol |
re re'~ |
re' do' |
si mi'4 mi |
la2 la4 sol |
fad2 sol |
do do'~ |
do' si |
la2. la4 |
mi'2 re'4 do' |
si2 do' |
re' si |
do'2. do'4 |
re'2 re |
sol1 |

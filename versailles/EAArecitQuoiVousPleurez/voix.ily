<<
  %% Sangaride
  \tag #'(sangaride basse) {
    << { s1*3 | s2. } \tag #'sangaride { \sangarideClef R1*3 | r2 r4 } >>
    \tag #'basse \sangarideMark r8 re''8 |\tag #'basse \noBreak
    si'4 r
    << { s2 | s2. } \tag #'sangaride { r2 | r2 r4 } >>
    \tag #'basse \sangarideMark r8 re'' |\tag #'basse \noBreak
    si'2 do''4 do'' |
    r4 re'' si'2 |
    do''4 \tag #'sangaride do''4 \tag #'basse { do''8 s }
    << { s4 } \tag #'sangaride { r4 } >>
    \tag #'basse \sangarideMark r8 la'16 si' | sol'4
    << { s2. | s1 | s4. } \tag #'sangaride { r4 r2 | R1 | r4 r8 } >>
    \tag #'basse \sangarideMark re''8 si'4 do''8 do''16 re'' |
    mi''8 mi'' r do''16 mi'' la'8\trill la'16 si' |
    sol'4 r8 si'16 do'' re''4 re''8 fad' |
    sol'4 r8 do'' la'8.\trill la'16 si'8 do'' |
    si'4\trill r8 si' mi''8. mi''16 mi''8 si' |
    do''4 do''8. do''16 la'4\trill la'8. la'16 |
    fad'8\trill fad' r re'' si'4\trill do''8 do''16 re'' |
    mi''8 mi'' r do''16 mi'' la'8\trill la'16 si' |
    \tag #'sangaride sol'4 \tag #'basse { sol'8 s }
    << { s2 | s2.*3 | s2 } \tag #'sangaride { r2 | R2.*3 | r2 } >>
    \tag #'basse \sangarideMark re''8 re''16 re'' fad'8. fad'16 |
    sol'4 r8 si'16 re'' sol'8 sol'16 sol' |
    mi'4\trill r8 do''16 mi'' do''8.\trill do''16 mi'8 mi'16 fad' |
    sol'4 la'8 si' do''4 do''8 re'' |
    si'\trill si' r si'16 mi'' dod''4\trill dod''8. fad''16 |
    red''8\trill red'' r si'16 si' la'8. sol'16 fad'8\trill fad'16 sol' |
    mi'2. |
    << { s2.*21 | s4. } \tag #'sangaride { R2.*21 | r4 r8 } >>
    \tag #'basse \sangarideMark re''8 si'8.\trill si'16 do''8. re''16 |
    mi''8. mi''16 do''\trill do'' do'' do'' la'8\trill la' re'' re'' |
    re''4. do''16 si' do''8 do''16 si' la'8\trill la'16 sol' |
    fad'4\trill r8 si' sold'\trill sold' sold'8. sold'16 |
    la'4 r8 mi'' la'8. la'16 la'8. la'16 |
    fad'8\trill fad' la'8 la'16 la' la'8. si'16 |
    do''4 do''8 do''16 do'' do''8. si'16 |
    si'4\trill r16 si' si' si' mi''8. mi''16 |
    dod''4 re''8 re''16 re'' re''8 dod'' |
    re''2 re''4 r |
    << { s2.*21 | s1 } \tag #'sangaride { R2.*21 | R1 } >>
    \tag #'basse \sangarideMark r4 sol'8 la' si' si' si'\trill si'16 la' |
    si'8 si' mi''8. si'16 dod''8 re'' mi'' mi''16 re'' |
    mi''4 mi'' r2 |
    R1 |
    r2 r4 sol'8 la' |
    si'4\trill si'8 do'' re''4 sol''8 fad'' |
    mi''4 re''8 do'' si'4\trill si'8 do'' |
    re''4 mi''8 fa'' mi''2\trill |
    r4 do''8 si' la'4 la'8 si' |
    sol'4 r8 si'16 si' mi''8 mi''16 si' |
    do''4 do''8 do''16 do'' mi'8. fad'16 |
    sol'8 sol' r8 si'16 si' si'8 do''16 re'' |
    mi''8. mi''16 fa''8. mi''16 re''8. do''16 |
    do''8. mi''16 si'8 si'16 do'' re''8 do''16 si' |
    la'4 la' r la'8 si' |
    sol'4 sol'8 sol' sol'4( fad'8) sol' |
    fad'2 r4 sol'8 la' |
    si'4\trill si'8 do'' re''4 sol''8 fad'' |
    mi''4 re''8 do'' si'4\trill si'8 do'' |
    re''4 mi''8 fa'' mi''2\trill |
    r4 do''8 si' la'4 la'8 si' |
    sol'2. |
    << { s1.*2 | s1*14 | s4. } \tag #'sangaride { R1.*2 | R1*14 | r4 r8 } >>
    \tag #'basse \sangarideMark re''8 si' si'16 si' si'8 re'' |
    sol'4 r16 sol' sol' sol' do''4 do''8 re'' |
    mi'' mi'' r mi''16 sol'' do''8. do''16 do''8. mi''16 |
    la'8 la' re'' re''16 mi'' la'4 la'8. si'16 |
    sol'2 %{%} re''4 re''8 re'' |
    mi''4. mi''8 re''4. do''8 |
    si'4 si' re''4. re''8 |
    re''4 do''8 si' la'4( si'8) do'' |
    si'4. re''8 re''4. re''8 |
    mi''4 mi'' do'' re'' |
    si' si' mi''2~ |
    mi''4. re''8 re''4 do'' |
    si'2 dod''4 re'' |
    dod''4 dod'' dod'' re'' |
    mi'' mi'' mi''4. mi''8 |
    la'4 la' re''2~ |
    re''4. do''8 do''4 do'' |
    do''2 si'4. do''8 |
    la'4 la' la' la' |
    si' si' do'' do'' |
    re''2.( mi''4) |
    dod''4. mi''8 mi''4. mi''8 |
    re''4 re'' re''4. re''8 |
    mi''2 mi'' |
    la'4. la'8 si'4 si' |
    do''2 do''4( si'8) do'' |
    si'4. si'8 si'4 do'' |
    re''4 re'' do''4. si'8 |
    la'4 la' re''2~ |
    re''4. re''8 do''4. si'8 |
    la'2 si'4 do'' |
    si'1 |
  }

  %% Doris
  \tag #'(doris basse) {
    \tag #'basse \dorisMark
    \tag #'doris \dorisClef
    r2 re''4 sol'8. sol'16 |
    mi'4
    << { s2. | s2 } \tag #'doris { r4 r2 | r2 } >>
    \tag #'basse \dorisMark r8 sol'16 la' si'8 si'16 dod'' |
    re''8 re''16 re'' mi''8 mi''16 mi'' la'8 la'
    << { s4 | s2 } \tag #'doris { r4 | r2 } >>
    \tag #'basse \dorisMark r8 re'' re'' mi'' |
    do'' do''16 re'' si'8 si'16 do'' la'4
    << { s4 | s1*2 | s4. } \tag #'doris { r4 | R1*2 | r4 r8 } >>
    \tag #'basse \dorisMark la'16 la' fad'4
    << { s4 | s4 } \tag #'doris { r4 | r4 } >>
    \tag #'basse \dorisMark r8 si' si'8. si'16 do''8. re''16 |
    dod''4 re''8. la'16 sol'4 sol'8. la'16 |
    fad'4 << \tag #'doris fad'4 \tag #'basse { fad'8 s } >>
    << { s2 | s2. | s1*5 | s2. | s8 } \tag #'doris { r2 | R2. | R1*5 | R2. | r8 } >>
    \tag #'basse \dorisMark si'8 si'8. si'16 do''8. do''16 |
    la'8.\trill la'16 si'8. si'16 si'8. si'16 |
    sold'8 sold' mi''8. mi''16 mi''8 mi''16 si' |
    do''8. do''16 do''8. do''16 re''8 mi'' |
    la' la' r4
    << { s2 | s2. | s1*4 | s2. } \tag #'doris { r2 | R2. | R1*4 | R2. } >>
    \setMusic #'peutOnChanger {
      si'4 do'' re'' |
      mi''2 do''4 |
      la' re''4. re''8 |
      la'4 la' si' |
      sol'2. |
      fad'4 fad' sol' |
      la' si' do'' |
      si' si' si' |
      do'' re'' mi'' |
      mi''( re''4.) do''8 |
      do''4 mi' fad' |
      sol' sol' la' |
      si' si' mi'' |
      dod'' dod'' re'' |
      re''( dod''4.) re''8 |
      re''2. |
      r4 la' si' |
      do'' do'' re'' |
      si' si' do'' |
      re'' re'' mi'' |
      si'( la'4.) sol'8 |
    }
    \tag #'basse \dorisMark \tag #'() \peutOnChanger
    sol'4
    << { s2. | s1*4 | s2.*4 | s1 } \tag #'doris { r4 r2 | R1*4 | R2.*4 | R1 } >>
    \tag #'basse \dorisMark \tag #'() \peutOnChanger
    sol'2 r |
    << { s1*9 | s2.*5 | s1*7 s2. } \tag #'doris { R1*9 | R2.*5 | R1*7 s2. } >>
    \tag #'basse \dorisMark si'4 si'8 si' si'4. si'8 do''4. re''8 |
    mi''2 mi''4 mi'' re''4. do''8 |
    si'4 si' si' do'' |
    re''4. re''8 do''4. si'8 |
    la'2 la' |
    r4 si' si' do'' |
    la' la' la' si' |
    sold' mi'' fa''4. mi''8 |
    re''4. do''8 do''4( si') |
    la'1 |
    r4 la' la' sol' |
    fad'4 fad' fad'8[ mi'] fad'4 |
    sol' re'' re'' do'' |
    si' si' do'' re'' |
    mi'' do'' la' re'' |
    si' do'' si'( la') |
    sol'4
    \tag #'doris {
      r4 r2 |
      R1*3 |
      r2 si'4 si'8 si' |
      do''4. do''8 si'4. la'8 |
      sol'4 sol' si'4. si'8 |
      si'4 la'8 sol' fad'4. sol'8 |
      sol'4. si'8 si'4. si'8 |
      do''4 do'' la' si' |
      sold' sold' do''2~ |
      do''4. si'8 si'4 la' |
      la'2 sold'4. la'8 |
      la'2 la'~ |
      la'4. sol'8 sol'4 sol' |
      sol'2 fa'4( mi'8) fa' |
      mi'4. mi'8 mi'4 la' |
      fad'4 fad' sol'4. la'8 |
      fad'4 fad'8 fad' fad'4 fad' |
      sol'4 sol' la'4. la'8 |
      la'2( sold') |
      la'2 do''~ |
      do''4. si'8 si'4. si'8 |
      si'2 la'4. sol'8 |
      fad'4. fad'8 sol'4 sol' |
      sol'2 fad'4. sol'8 |
      sol'4. sol'8 sol'4 la' |
      si'4 si' la'4. sol'8 |
      fad'4 fad' si'2~ |
      si'4. si'8 la'4. sol'8 |
      sol'2 fad'4. sol'8 |
      sol'1 |
    }
  }

  %% Idas
  \tag #'(idas basse) {
    << { s1 | s4 } \tag #'idas { \idasClef R1 | r4 } >>
    \tag #'basse \idasMark r8 sol do' do'16 do' do'8 do'16 si |
    si4 si
    \tag #'idas {
      r2 |
      R1 |
      r2 r8 sol sold sold |
      la8 fad16 fad sol8 fad16 sol re4 r |
      R1*2 |
      r4 r8 do'16 do' la4 r |
      r4 r8 sol sol8. sol16 sol8. sol16 |
      sol4 fad8. fad16 mi4 mi8. fad16 |
      re4 re r2 |
      R2. |
      R1*5 |
      R2. |
      r8 sol sol8. sol16 mi8. mi16 |
      fa8. fa16 re8. re16 re8. re16 |
      mi8 mi sold8. sold16 sold8 sold16 sold |
      la8. la16 la8. la16 si8 do' |
      re' re' r4 r2 |
      R2. |
      R1*4 |
      R2. |
      \setMusic #'peutOnChanger {
        sol4 la si |
        do'2 la4 |
        re' si4. si8 |
        fad4 fad sol |
        mi2. |
        re4 re mi |
        fad fad4. fad8 |
        sol4 sol fa |
        mi re do |
        sol2 sol4 |
        do do re |
        mi mi fad |
        sol sol mi |
        la la re |
        la,2 r8 re8 |
        re4 re mi |
        fa2. |
        r4 mi fad |
        sol sol la |
        si4 si do' |
        re'2 re4 |
      }
      \tag #'() \peutOnChanger
      sol4 r4 r2 |
      R1*4 |
      R2.*4 |
      R1 |
      \tag #'() \peutOnChanger
      sol2 r |
      R1*9 R2.*5 R1*7 R2. |
      sol4 sol8 sol sol4. fa8 mi4. re8 |
      do2 do4 do' re'4. re'8 |
      mi'4 mi' re' do' |
      si4. si8 la4. sol8 |
      re'2 re' |
      r4 sol sol mi |
      fa fa fa re |
      mi do re mi |
      fa re mi2 |
      la,4 la la si |
      dod' dod' dod'8[ si] dod'4 |
      re'4 re' re' do'! |
      si si fad fad |
      sol sol la si |
      do' la re' si |
      mi' do' re'( re) |
      sol r r2 |
      R1*3 |
      r2 sol4 sol8 sol |
      do4 do re4. re8 |
      mi4 mi si,4. si,8 |
      do4 do8 do re8[ do] re4 |
      sol,4. sol8 sol4 fa |
      mi mi fa re |
      mi mi do4. do8 |
      re2. re4 |
      mi2 mi4 mi |
      la, la la si |
      dod' dod' dod'4. dod'8 |
      re'2 re' |
      r4 la la la |
      re' re' sol4. sol8 |
      re4 re re'2~ |
      re'4. re'8 do'4 do' |
      si2 mi'4 mi |
      la4 la la sol |
      fad fad sol4. sol8 |
      do4 do do'2~ |
      do'4. do'8 si4 si |
      la2 la4 la |
      mi'4. mi'8 re'4 do' |
      si4 si do'4. do'8 |
      re'4 re' si4. si8 |
      do'2. do'4 |
      re'2 re'4 re |
      sol1 |
    }
  }
>>
\stopStaff
\set Staff.whichBar = "|."

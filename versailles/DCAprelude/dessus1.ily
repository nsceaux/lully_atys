\clef "dessus" R2. |
r4 re''4. re''8 |
do''2 r4 |
r mib''4. mib''8 |
re''2 r4 |
r fa''4. fa''8 |
sib'4 mib''4. mib''8 |
mib''4 re''4. mib''8 |
do''4. do''8 re'' mib'' |
re''2 r4 |

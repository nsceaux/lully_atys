\clef "basse" sib2. |
sib,2 sib,4 |
fa4. fa8 mib re |
do2 do4 |
sol4. sol8 fa mib |
re2. |
mib4 do2 |
fa4 sib,2 |
fa,2. |
sib,4 sib2 |
\once\set Staff.whichBar = "|"
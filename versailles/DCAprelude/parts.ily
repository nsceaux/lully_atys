\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse-continue #:score-template "score-basse-continue-voix"
                   #:indent 0)
   (basse-viole #:score-template "score-basse-viole-voix"
                #:indent 0))

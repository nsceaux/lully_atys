\clef "dessus" do''4. re''8 mi''4 |
la' fa''2 |
mi''8 re'' mi'' fa'' mi''4 |
do''4. re''8 mi''4 |
la' fa''2 |
mi''2. |
do''4. si'8 do''4 |
la' re''2 |
si' do''4 |
la'8. si'16 si'4. la'8 |
sold'2 mi''4 |
do''4. re''8 mi''4 |
fa'' re''4. do''16 re'' |
mi''4. fa''8 re''4~ |
re''8 do'' si'4. la'8 |
la'2. |

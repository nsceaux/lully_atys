\clef "quinte" do'2 si4 |
la2 re'4 |
do' la si |
do'2 si4 |
la la2 |
si2. |
do'2 do'4 |
do' re'2 |
re' do'4 |
do' si2 |
si si4 |
do'2 do'4 |
la sol2 |
sol4. la8 si4 |
do'8 re' mi'4. mi'8 |
mi'2. |

\clef "basse" la,4 la sol |
fa2 re4 |
la2 sold4 |
la2 sol4 |
fa re2 |
mi mi,4 |
la, la mi |
fa re2 |
sol mi4 |
fa re2 |
mi mi4 |
la4. si8 do'4 |
fa sol sol, |
do4. do8 si,4 |
la, mi mi, |
la,2. |

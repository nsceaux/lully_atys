\clef "haute-contre" la'4 mi'2 |
fa'8 sol' la'4. si'8 |
do'' si' do'' re'' do''4 |
la' mi'2 |
fa'8 sol' la'2 |
sold'2. |
la'2 sol'4 |
fa' fa'2 |
sol' sol'4 |
fa'4 fa'2 |
mi'2 sold'4 |
la'2 sol'4 |
la' si'4. la'16 si' |
do''2 sold'4 |
la' sold'4. la'8 |
la'2. |

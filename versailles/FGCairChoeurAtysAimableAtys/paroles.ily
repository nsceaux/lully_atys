%% Cybele
\tag #'(cybele basse) {
  A -- tys, l’ai -- mable A -- tys, mal -- gré tous ses at -- traits,
  Des -- cend dans la nuit é -- ter -- nel -- le ;
  Mais mal -- gré la mort cru -- el -- le,
  L’a -- mour de Cy -- be -- le
  Ne mour -- ra ja -- mais.
  Sous u -- ne nou -- vel -- le fi -- gu -- re,
  A -- tys est ra -- ni -- mé par mon pou -- voir di -- vin ;
  Ce -- le -- brez son nou -- veau des -- tin,
  Pleu -- rez, pleu -- rez sa fu -- neste a -- van -- tu -- re.
  Pleu -- rez, pleu -- rez sa fu -- neste a -- van -- tu -- re.
}
% Choeur
\tag #'(choeur basse) {
  Ce -- le -- brons son nou -- veau des -- tin,
  Pleu -- rons, pleu -- rons sa fu -- neste a -- van -- tu -- re.
  Ce -- le -- brons son nou -- veau des -- tin,
  Pleu -- rons, pleu -- rons sa fu -- neste a -- van -- tu -- re.
}
% Cybele
\tag #'(cybele basse) {
  Que cét ar -- bre sa -- cré
  Soit re -- ve -- ré
  De tou -- te la na -- tu -- re.
  Qu’il s’es -- leve au des -- sus des ar -- bres les plus beaux :
  Qu’il soit voi -- sin des cieux, qu’il re -- gne sur les eaux ;
  Qu’il ne puis -- se brû -- ler que d’u -- ne fla -- me pu -- re.
  Que cét ar -- bre sa -- cré
  Soit re -- ve -- ré
  De tou -- te la na -- tu -- re.
}
% Choeur
\tag #'(choeur basse) {
  Que cét ar -- bre sa -- cré
  Soit re -- ve -- ré
  De tou -- te la na -- tu -- re.
}
% Cybele
\tag #'(cybele basse) {
  Que ses ra -- meaux soient toû -- jours verds :
  Que les plus ri -- gou -- reux hy -- vers
  Ne leur fas -- sent ja -- mais d’in -- ju -- re,
  Que cét ar -- bre sa -- cré
  Soit re -- ve -- ré
  De tou -- te la na -- tu -- re.
}
% Choeur
\tag #'(choeur basse) {
  Que cét ar -- bre sa -- cré
  Soit re -- ve -- ré
  De tou -- te la na - % -- tu -- re.
}

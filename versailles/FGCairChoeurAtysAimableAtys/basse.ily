\clef "basse" do4. si,8 la,8. sol,16 fad,4 |
sol,4 sol8 fa mi fa mi re |
do2 si,4 sib, |
la,2 sol, |
sol mi |
fa4. re8 sol4. mi8 |
la4 si8 do' sol sol, |
do1 | \allowPageTurn
do'4. do'8 do'4 si |
do'2 si |
la4 la, la8 si do' fa |
sol2 do |
re fad, |
sol, mib |
re4. sib,8 do2 |
re4. sib,8 mib do re re, |
sol,2. sol4 |
do'2 lab4 fa |
sib4. sib8 si2 |
do'2 re'4 re |
sol4. fa8 mib2 |
lab fa |
sib4. sib8 si2 |
do'4. fa8 sol4 sol, |
do2 do' |
mi1 |
fa2 fad |
sol2. mib4 |
lab fa sol sol, |
do2 do'~ |
do' si |
do'1 |
mi |
fa~ |
fa2. mib4 |
re1 |
do |
sib,2 sib4 lab |
sol2. fa4 |
mib4. re8 do2~ |
do4 sib, lab,2 |
sol, sol |
mi1 |
fa2 fad |
sol2. mib4 |
lab fa sol sol, |
do2 do4 do'8 do' |
mi1 |
fa2 fad |
sol2. mib4 |
lab fa sol sol, |
do1 | \allowPageTurn
sol2 sol |
lab1 |
la! |
sib2. sol4 |
lab2 sib4 sib, |
mib1 | \allowPageTurn
mi |
fa2 fad |
sol2. mib4 |
lab fa sol sol, |
do2 do4 do'8 do' |
mi1 |
fa2 fad |
sol2. mib4 |
lab fa sol sol, |

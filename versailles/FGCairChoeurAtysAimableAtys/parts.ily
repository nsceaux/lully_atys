\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-tous)
   (basse-viole #:score-template "score-basse-viole-voix")
   (flutes-hautbois #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Cybele
    \livretVerse#12 { Atys, l’aimable Atys, malgré tous ses attraits, }
    \livretVerse#8 { Descend dans la nuit éternelle ; }
    \livretVerse#7 { Mais malgré la mort cruelle, }
    \livretVerse#5 { L’amour de Cybele }
    \livretVerse#5 { Ne mourra jamais. }
    \livretVerse#8 { Sous une nouvelle figure, }
    \livretVerse#12 { Atys est ranimé par mon pouvoir divin ; }
    \livretVerse#8 { Celebrez son nouveau destin, }
    \livretVerse#8 { Pleurez sa funeste avanture. }
    \livretPers\line { Chœur des Nymphes des Eaux, & des Divinitez des Bois }
    \livretVerse#8 { Celebrons son nouveau destin, }
    \livretVerse#8 { Pleurons sa funeste avanture. }
    \livretPers Cybele
    \livretVerse#6 { Que cét Arbre sacré }
    \livretVerse#4 { Soit reveré }
    \livretVerse#6 { De toute la Nature. }
    \livretVerse#12 { Qu’il s’esleve au dessus des Arbres les plus beaux : }
    \livretVerse#12 { Qu’il soit voisin des Cieux, qu’il regne sur les Eaux ; }
    \livretVerse#12 { Qu’il ne puisse brûler que d’une flame pure. }
    \livretVerse#6 { Que cét Arbre sacré }
    \livretVerse#4 { Soit reveré }
    \livretVerse#6 { De toute la Nature. }
    \livretDidasP\wordwrap { Le Chœur repete ces trois derniers Vers. }
    \livretPers Cybele
    \livretVerse#8 { Que ses rameaux soient toûjours verds : }
  }
  \column {
    \null
    \livretVerse#8 { Que les plus rigoureux Hyvers }
    \livretVerse#8 { Ne leur fassent jamais d’injure. }
    \livretVerse#6 { Que cét Arbre sacré }
    \livretVerse#4 { Soit reveré }
    \livretVerse#6 { De toute la Nature. }
    \livretDidasP\wordwrap { Le Chœur repete ces trois derniers Vers. }
    \livretPers\line { Cybele, & le Chœur des Divinitez des Bois & des Eaux }
    \livretVerse#4 { Quelle douleur ! }
    \livretPers \line { Cybele, & le Chœur des Corybantes }
    \livretVerse#4 { Ah ! quelle rage ! }
    \livretPers\line { Cybele, & les Chœurs }
    \livretVerse#4 { Ah ! quel malheur ! }
    \livretPers Cybele
    \livretVerse#8 { Atys au printemps de son âge, }
    \livretVerse#6 { Perit comme une fleur }
    \livretVerse#5 { Qu’un soudain orage }
    \livretVerse#5 { Reenverse et ravage. }
    \livretPers\line { Cybele, & le Chœur des Divinitez des Bois & des Eaux }
    \livretVerse#4 { Quelle douleur ! }
    \livretPers \line { Cybele, & le Chœur des Corybantes }
    \livretVerse#4 { Ah ! quelle rage ! }
    \livretPers\line { Cybele, & les Chœurs }
    \livretVerse#4 { Ah ! quel malheur ! }
  }
}#})
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Cybele
    \livretVerse#12 { Atys, l’aimable Atys, malgré tous ses attraits, }
    \livretVerse#8 { Descend dans la nuit éternelle ; }
    \livretVerse#7 { Mais malgré la mort cruelle, }
    \livretVerse#5 { L’amour de Cybele }
    \livretVerse#5 { Ne mourra jamais. }
    \livretVerse#8 { Sous une nouvelle figure, }
    \livretVerse#12 { Atys est ranimé par mon pouvoir divin ; }
    \livretVerse#8 { Celebrez son nouveau destin, }
    \livretVerse#8 { Pleurez sa funeste avanture. }
    \livretPers\line { Chœur des Nymphes des Eaux, & des Divinitez des Bois }
    \livretVerse#8 { Celebrons son nouveau destin, }
    \livretVerse#8 { Pleurons sa funeste avanture. }
    \livretPers Cybele
    \livretVerse#6 { Que cét Arbre sacré }
    \livretVerse#4 { Soit reveré }
    \livretVerse#6 { De toute la Nature. }
  }
  \column {
    \null
    \livretVerse#12 { Qu’il s’esleve au dessus des Arbres les plus beaux : }
    \livretVerse#12 { Qu’il soit voisin des Cieux, qu’il regne sur les Eaux ; }
    \livretVerse#12 { Qu’il ne puisse brûler que d’une flame pure. }
    \livretVerse#6 { Que cét Arbre sacré }
    \livretVerse#4 { Soit reveré }
    \livretVerse#6 { De toute la Nature. }
    \livretDidasP\wordwrap { Le Chœur repete ces trois derniers Vers. }
    \livretPers Cybele
    \livretVerse#8 { Que ses rameaux soient toûjours verds : }
    \livretVerse#8 { Que les plus rigoureux Hyvers }
    \livretVerse#8 { Ne leur fassent jamais d’injure. }
    \livretVerse#6 { Que cét Arbre sacré }
    \livretVerse#4 { Soit reveré }
    \livretVerse#6 { De toute la Nature. }
    \livretDidasP\wordwrap { Le Chœur repete ces trois derniers Vers. }
  }
}#}))

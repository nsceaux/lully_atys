\key do \major
\time 4/4 \midiTempo #80 s1*6
\digitTime\time 3/4 s2.
\time 2/2 \midiTempo #160 s1
\time 4/4 \midiTempo #80 s1*8 \bar "||"
\time 2/2 \midiTempo #160
\key sol \minor s1
\time 4/4 \midiTempo #80 s1*8
\time 2/2 \midiTempo #160 s1*26
\time 4/4 \midiTempo #80 s1
\time 2/2 \midiTempo #160 s1*15


\clef "haute-contre" sol'4 sol'8 la' |
si'4 la'8 sol' sol'4 fad' |
sol' sol' sol' sol'8. la'16 |
si'4 la'8 sol' sol'4 fad' |
sol'8 fad' sol' la' fad'4. sol'8 |
sol'2 si'4. si'8 |
la'4 re'' mi''4. mi''8 |
la'2 la'4. la'8 |
sol'4 re'' re'' dod'' |
re'' fad' sol' sol'8 la' |
si'4 la'8 sol' sol'4 fad' |
sol' sol' sol' sol'8 la' | % sol'8. la'16 |
si'4 la'8 sol' sol'4 fad' |
sol'8 fad' sol' la' fad'4. sol'8 |
sol'2 si'4 si'8 la'16 si' |
do''4 sol' sol' fad' |
sol' sol'8 fad' mi' fad' sol'4 |
la' la'8 sol' fad' mi' fad' sol' |
sol'4 fad' sol' sol'8 la' |
si'4 la'8 sol' sol'4 fad' |
sol' sol' sol' sol'8 la' |
si'4 la'8 sol' sol'4 fad' |
sol'8 fad' sol' la' fad'4. sol'8 |
sol'2
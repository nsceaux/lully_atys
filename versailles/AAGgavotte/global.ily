\key sol \major
\time 2/2 \midiTempo#160
\beginMark\markup\column {
  "[Gavotte en rondeau]"
  \line\italic\smaller { Les hautbois et les violons jouent cet air alternativement }
}
\partial 2 s2 s1*22 s2 \bar "|."

\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { DHb DVn }
      shortInstrumentName = \markup\center-column { DHb DVn }
    } << \global \includeNotes "dessus" >>
    \new Staff \with {
      instrumentName = \markup\center-column { HcHb HcVn }
      shortInstrumentName = \markup\center-column { HcHb HcVn }
    } << \global \includeNotes "haute-contre" >>
    \new Staff \with {
      instrumentName = \markup\center-column { THb TVn }
      shortInstrumentName = \markup\center-column { THb TVn }
    } << \global \includeNotes "taille" >>
    \new Staff \with { \qvInstr } << \global \includeNotes "quinte" >>
    \new Staff \with {
      instrumentName = \markup\center-column { BCr BVn }
      shortInstrumentName = \markup\center-column { BCr BVn }
    } << \global \includeNotes "basse" >>
  >>
  \layout { }
  \midi { }
}

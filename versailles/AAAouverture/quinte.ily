\clef "quinte" sol4. sol8 re'4. re'8 |
do'2. do'4 |
sib4 re' re'2 |
re'2. re'4 |
do' do' re'4. re'8 |
sol2 do' |
la4. la8 sib la sol4 |
la1 |
sib2. sib4 |
do'2. re'4 |
re'2 la4. la8 |
sol2 sol'4. sol'8 |
sol'2 fa'4. re'8 |
re'2 la4. la8 |
la2 re' |
la1 |
R1.*2 |
r2 r4 re'8 do' re' mib' re' do' |
sib4 sol do'~ do' re'2 |
sol4 do'2 la4 re'4. re'8 |
re'2. re'2 re'4 |
do'2 do'4 do'2 do'4 |
sib2 fa4 fa2 fa'4 |
fa'4. re'8 mi'4 re'2. |
re'4 mi'2 mi' mi'8 mi' |
re'2 la4 la2 la4 |
la re'2 re'4 sol2 |
la la la4. la8 |
la2 la8 la sib la sib do' sib la |
sol2 sol8 sol la4 fa2 |
sib sib4 sib2 sib8 sib |
% Manuscrit : la2 la8 la re'2
la2 la4 la re'2 |
sol2 sol4 la re'4. re'8 |
re'2 re'4. re'8 |
re'4. re'8 do'4. do'8 |
la2. la4 |
la2 sol4. sol8 |
sol2. sol4 |
sol4 la la re'8 re' |
re'1 |

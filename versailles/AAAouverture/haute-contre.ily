\clef "haute-contre" sib'4. sib'8 sol'4. sol'8 |
sol'2. fa'4 |
fa'2 re''4. re''8 |
re''2. si'4 |
do'' sol'8 la' sib'4 la' |
sol'4. sol'8 la'4. sol'8 |
fad'4. fad'8 sol'4. la'8 |
fad'1 |
re''2. re''4 |
do''4. re''8 do''4 sib' |
la' re''2 re''8 do'' |
sib'2 sib'4. sib'8 |
la'2 la'4. la'8 |
sib'2 la'4. sol'8 |
fad'2 r8 fad' sol' la' |
fad'1 |
R1. |
re''8 dod'' re'' mib'' re'' do'' sib'4 sol' mib'' |
re''2 re''4 re''2 re''4 |
re'' sib' do'' la' la'4. sib'8 |
sol'2 do''4 do''4. do''8 sib'4 |
la'2 re''4 re''2. |
sol'2 sol'4 la' fa'2 |
fa' sib' la'4. sib'8 |
sib'4 re''4. re''8 re'' do'' re'' mib'' re'' do'' |
sib'2 sib'4 la'2. |
la'2 la'4 la'8 si' dod'' re'' mi''4 |
la'2 la'4 sib'2 sol'4 |
sol'4. la'8 fa'4 mi' la'4. la'8 |
la'2 la'8 la' sol'2 re'8 re' |
mi' fa' sol' mi' fa' sol' fa'2 do''8 do'' |
re''2 re''4 sib'2 sib'8 sib' |
do''2 do''4 la'4. fad'8 sol' la' |
sib' la' sib' do'' re'' re'' re'' do'' re'' mib'' re'' do'' |
sib'4. sib'8 la'2 |
re''4. re''8 sol'2 |
la'2 la'4. sol'8 |
fad'2 re''4. do''8 |
sib'4. la'8 sol'4 re'' |
do''8 sib' la' sol' fad'4. sol'8 |
sol'1 |

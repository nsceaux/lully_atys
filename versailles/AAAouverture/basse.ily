\clef "basse" sol,1 |
sol4. sol8 sol4 la |
sib2. sib4 |
si2. sol4 |
do'4 do sol fa |
mib2~ mib8 mib re do |
re do sib, la, sol,2 |
re, re8 mib re do |
sib,2 sib |
la4. sib8 la4. sol8 |
fad2. fad4 |
sol2. sol4 |
la2 re |
sol, la, |
re,4 re8. mib16 re8. do16 sib,8. la,16 |
re,1 |
R1.*3 |
sol8 fad sol la sol la fa mib fa sol fa sol |
mib re mib fa mib fa re2. |
re'8 do' re' mib' re' do' si la si sol la si |
do' sib do' re' do' sib la sol la fa sol la |
sib4 sib,8 do re mib fa4 fa,2 |
sib,4. sib8 la sol fad2. |
sol la8 sol la sib la sol |
fa4 sol8 fa mi re dod2. |
re8 do re mi re do sib, la, sib, do sib, do |
la,2. la8 sol la sib la sol |
fad mi fad re mi fad sol fa sol la sol fa |
mi re mi do re mi fa mi fa sol fa mib |
re do re sib, do re mib re mib fa mib re |
do sib, do la, sib, do re do re mib re do |
sib,4 do8 sib, la, sol, re4 re,2 |
sol, re4. do8 |
si,2 do4. do8 |
dod1 |
re2 sol, |
mib si, |
do re4 re, |
sol,1 |

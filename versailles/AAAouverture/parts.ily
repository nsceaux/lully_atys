\piecePartSpecs
#`((dessus)
   (haute-contre)
   (taille)
   (quinte)
   (basse #:instrument , #{\markup\center-column { BVn et Bc }#})
   (basse-continue #:instrument , #{\markup\center-column { BVn et Bc }#})
   (basse-tous #:score-template "score-basse-continue"
               #:instrument , #{\markup\center-column { BVn et Bc }#}))

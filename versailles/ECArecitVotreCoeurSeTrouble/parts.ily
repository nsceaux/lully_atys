\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix"
                   #:indent 0)
   (basse-viole #:score-template "score-basse-viole-voix"
                #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
   \livretPers Celænus
   \livretVerse#8 { Vostre cœur se trouble, il soûpire. }
   \livretPers Sangaride
   \livretVerse#8 { Expliquez en vostre faveur }
   \livretVerse#12 { Tout ce que vous voyez de trouble dans mon cœur. }
   \livretPers Celænus
   \livretVerse#12 { Rien ne m’allarme plus, Atys, ma crainte est vaine, }
   \livretVerse#12 { Mon amour touche enfin le cœur de la Beauté }
   \livretVerse#6 { Dont je suis enchanté : }
  }
  \column {
    \null
    \livretVerse#9 { Toy qui fûs le tesmoin de ma peine, }
    \livretVerse#12 { Cher Atys, sois tesmoin de ma felicité. }
    \livretVerse#12 { Peux-tu la concevoir ? non, il faut que l’on aime, }
    \livretVerse#12 { Pour juger des douceurs de mon bonheur extresme. }
    \livretVerse#8 { Mais, prés de voir combler mes vœux, }
    \livretVerse#12 { Que les moments sont longs pour mon cœur amoureux ! }
    \livretVerse#12 { Vos parents tardent trop, je veux aller moy-mesme }
    \livretVerse#8 { Les presser de me rendre heureux. }
  }
}#}))

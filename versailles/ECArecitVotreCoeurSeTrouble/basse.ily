\clef "basse" sol,1 |
la,2. |
re,4 re sol mi |
fa2 fa8 mi |
re4 sol sol, |
do1 |
si,2 la, |
sol,2. re8. mi16 |
fa4 mi8 fa sol do |
si, do re4 sol, re, |
sol,1 |
sold,2 la, |
fa2 re |
mi4. do8 si, la, mi mi, |
la,1 |
re2 re8. do16 si,8. la,16 |
sol,2 do4 |
fa, fa8. mi16 re8 do sol sol, |
do1 |
do2 la, |
sib,4 sol,2 |
re do4 re |
sol,1 |
re1 |
la,2 si,8 do sol,4 |
do1 |

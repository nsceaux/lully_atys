\score {
  <<
    \new ChoirStaff \with { \forceGroupBracket \haraKiri } <<
      \new Staff \with { \sangarideInstr } \withLyrics <<
        \global \keepWithTag #'sangaride \includeNotes "voix"
      >> \keepWithTag #'sangaride \includeLyrics "paroles"
      \new Staff \with { \celaenusInstr } \withLyrics <<
        \global \keepWithTag #'celaenus \includeNotes "voix"
      >> \keepWithTag #'celaenus \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout { }
  \midi { }
}

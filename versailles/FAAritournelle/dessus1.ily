\clef "dessus" re''4 re''8 mi'' fad''4 mi''8 re'' |
la''4. sol''8 sol''4. la''8 |
fad''4. sol''8 la''4 sol''8 fad'' |
mi''4. mi''8 mi''4. fad''8 |
sol''4 sol''8 la'' si''4 la''8 sol'' |
fad''4. fad''8 mi''4 fad''8 dod'' |
re''4. fad''8 fad''4. fad''8 |
sold''4 la'' sold''4. la''8 |
la''4 dod''8 re'' mi''4 mi''8 fad'' |
sol''2 re''4 re''8 mi'' |
fad''4 fad''8 sol'' la''4 sol''8 fad'' |
mi''4 la'' sol''2~ |
sol'' fad''~ |
fad''4. mi''8 mi''4. re''8 |
re''1 |
r4 dod''8 re'' mi''4 mi''8 fad'' |
sol''2 re''4 re''8 mi'' |
fad''4 fad''8 sol'' la''4 sol''8 fad'' |
mi''4 la'' sol''2~ |
sol'' fad''~ |
fad''4. mi''8 mi''4. re''8 |
re''1 |

\clef "basse" re1 |
R1 |
re4 re8 mi fad4 mi8 re |
la2 la, |
mi4 mi8 fad sol4 fad8 mi |
si4 si2 lad4 |
si2 si, |
mi4 la, mi,2 |
la, la4 sol8 fad |
mi4 mi8 fad sol4 fad8 mi |
re4 re8 mi fad4 mi8 re |
la4 la,8 si, dod4 re8 mi |
fad2~ fad8 fad mi re |
la2 la, |
re4 re8 mi fad4 mi8 re |
la2 la4 sol8 fad |
mi4 mi8 fad sol4 fad8 mi |
re4 re8 mi fad4 mi8 re |
la4 la,8 si, dod4 re8 mi |
fad2~ fad8 fad mi re |
la2 la, |
re1 |

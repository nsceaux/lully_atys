\clef "basse" mi2. red4 |
mi1 |
si,2 do4 |
re2. |
do4 sold,2 |
la,2. |
mi4. re8 do4 |
si,1 |
sold,4 la, fad, |
sol,4 sol8 fad mi4 |
re2. |
si, |
do |
dod2 re~ |
re do!2 |
sold,1 |
la,2. |
si,2 si,4 |
mi2 mi4 |
mi red2 |
mi mi8 re |
dod2 fad4 |
si, fad,2 |
si, si4 |
mi'2 mi'8 re' |
dod'2. |
re'4 re8 do si,4 |
do4. re8 mi do |
re2 sol,4 |
re,2. |
sol,4 sol2 |
fad2. |
mi |
re2 sol4 |
do'2. |
si2 mi4 |
la,4 si,2 |
mi,2.~ |
mi, | \allowPageTurn
si,4. dod?8 red? si, |
mi2. |
red4 mi do |
si, si8 la sold4 |
la4. la,8 si, do |
re2 sol,4~ |
sol, re,2 |
sol,4 sol8 la sol fad |
sol,4 sol2 |
fad2. |
mi |
re2 si,4 |
mi2 la,4~ |
la, mi,2 |
la,2 la8 sol |
fad2 sol4 |
do2. |
si,2 si8 la |
sold2. |
la2 la8 sol |
fad2. |
si2 mi4~ |
mi si,2 |
mi1~ |
mi~ |
mi~ |
mi2 red |
mi2. |
mi'2 re'4 |
do'2. |
si4. la8 sol la |
si4 si,2 |
mi2. |
mi'2 re'4 |
do'2. |
si4. la8 sol la |
si4 si,2 |
mi,2. |
mi4. si8 do' re' |
mi'4 re' do' |
si la sol |
fad2. |
sol2 do4 |
re re,2 |
sol, sol4 |
sold2. |
la2 la8 sol |
fad2. |
sol4 fad mi |
si si,2 |
mi1~ |
mi2 red |
mi1 |
fad2 fad, |
si, sold,4 la, |
re2 do4 |
si, fad,2 |
sol, sol |
do2. | \allowPageTurn
re4 sol, re,2 |
sol, sol |
fad mi |
re mi |
do si, |
la, la4 |
red2. |
mi2 sold, |
la,1 |
si,4 si sold2 |
la si8 mi si,4 |
mi,2. fad,4 |
sol,2. |
sol2 mi4 |
fad2. |
sol1~ |
sol4 fad mi |
re2 do8 si, la,4 |
mi2 mi8 re |
do2 re4 re, |
sol,1~ |
sol, |
do2 si,8 la, |
sol,2 sol4 |
do1 |
re |
do2. si,4 |
la,2 la |
sold la |
re1 |
mi2 sold, |
la, mi, |
la, la |
sol2. fad4 |
mi1 |
re4 mi fad2 |
sol fad4 mi |
re2 re'4 do' |
si2 la4 sol |
re'2 re |
sol,2. 
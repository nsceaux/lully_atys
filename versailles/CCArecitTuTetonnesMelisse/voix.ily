<<
  \tag #'(cybele basse) {
    \tag #'basse \cybeleMark
    \tag #'cybele \cybeleClef
    r8 si'16 si' sol'8 sol'16 si' fad'8 fad'16 sol' la'8 si'16 fad' |
    sol'4
    << { s2.*2 s4 } \tag #'cybele { r4 r2 | R2. | r4 } >>
    \tag #'basse \cybeleMark r8 fad' fad'16 fad' fad' sold'? |
    la'4 si'8 si'16 si' si'8. do''16 |
    do''4 do''8 << { s4. s2. s4 } \tag #'cybele { r8 r4 | R2. | r4 } >>
    \tag #'basse \cybeleMark r8 fad'16 fad' si'8 si'16 si' dod''8. red''16 |
    mi''8 mi'' r do''16 do'' do''8 do''16 re'' |
    si'8. si'16 dod''8 re'' re''8 dod'' |
    re''4 r8 la' fad'16 fad' fad' fad' |
    sol'4 sol'8 sol'16 sol' sol'8 sol' |
    mi'8 mi' do'' do''16 do'' do''8. do''16 |
    la'8 la'16 la' mi'8 fad'16 sol' fad'4 fad' |
    r8 re'16 mi' fad'8 fad'16 sold' la'8. la'16 la' si' do'' la' |
    mi''2 si'8 si'16 si' si'8 si' |
    mi'4 r8 do''16 do'' la'8 la'16 la' |
    fad'2 si'4 |
    sol' la' si' |
    la'2 fad'4 |
    sol'2 mi'4 |
    r4 r8 mi'' dod'' dod'' |
    re''4 dod''4. si'8 |
    si'2 si'8 la' |
    sold'2 sold'4 |
    la' sol'4. fad'8 |
    fad'2 r8 re'' |
    re''4 do''4. do''8 |
    do''2 si'8 do'' |
    si'4( la'2) |
    sol'2 mi''8 mi'' |
    mi''2 r8 re'' |
    re''4 do''4. do''8 |
    do''2 r8 si' |
    si'4 la'4. la'8 |
    la'4( sol') fad'8 sol' |
    sol'4( fad'2) |
    mi'4
    << { s2 s2.*24 s4 } \tag #'cybele { r2 | R2.*24 | r4 } >>
    \tag #'basse \cybeleMark r8 si' sol'4 sol'8 la'16 si' |
    do''2 do''8 do''16 do'' do''8 si' |
    si'4 r8 mi'' si'8. si'16 si'8. do''16 |
    la'4. la'8 fad'8. fad'16 si'8. fad'16 |
    sol'2 sol'4 |
    << { s2.*22 s1*2 s4 } \tag #'cybele { R2.*22 | R1*2 | r4 } >>
    \tag #'basse \cybeleMark mi'' r dod''16 dod'' dod'' dod'' |
    lad'4 lad'8 si' si'4 si'8 lad' |
    si'2 mi''8 mi''16 mi'' do'' do'' do'' do'' |
    la'8 la' r la'16 la' la'8 si'16 do'' |
    re''8. re''16 re''8. do''16 do''8. si'16 |
    si'4 r8 sol'16 la' si'8 si'16 si' do''8. re''16 |
    mi''4 r8 do''16 mi'' do''8 do''16 do'' |
    do''4 si'8. do''16 la'4 la'8. si'16 |
    sol'8 sol' r sol' si'8. si'16 si'8 dod'' |
    re''4 re''8. re''16 re''4 re''8 dod'' |
    re''4 r8 si'16 si' sold'4 sold'8. sold'16 |
    la'4. la'8 re''8. do''16 do''8 si' |
    do''8 do'' r mi''16 re'' do''8 do''16 si' |
    la'8. la'16 la'8. la'16 sol'8. fad'16 |
    sol'8 sol' r si' mi''8. mi''16 mi''8. si'16 |
    do''4 do''8 si' la'4 la'8 sol' |
    fad'4 r8 si' si'8. si'16 dod''8. re''16 |
    dod''8. dod''16 dod''8. dod''16 red''8 mi'' mi''[ red''] |
    mi''2 r |
    r4 r8 si'16 re'' si'8 si'16 si' |
    sol'4 r8 sol'16 sol' do''8 do''16 do'' |
    la'4 la'16 la' la' si' do''8 do''16 re'' |
    si'4 si' r si' |
    dod''8 dod'' re'' re'' re'' dod'' |
    re''4 r8 re' la' la'16 si' do''8 do''16 si' |
    si'4 si'8 si'16 si' do''8. re''16 |
    mi''4 do''8. si'16 la'4 la'8. si'16 |
    sol'4 sol'^\markup\italic { Melisse se retire. } r2 |
    r sol'8 sol'16 sol' sol'8 sol' |
    mi'4 r8 mi'16 fad' sol'8 sol'16 la' |
    si'4 sol'8 sol'16 la' si' si' do'' re'' |
    mi''4 r8 do''16 mi'' do''8 do''16 si' la'8. sol'16 |
    fad'2 fad'4 r8 re' |
    la'2 la'4 si' |
    do''2 do''4 la' |
    mi''2 mi'' |
    re''4 re''8 do'' si'4. la'8 |
    sold'2 si'4. si'8 |
    do''2 si'4. la'8 |
    la'2 r4 mi'' |
    dod'' dod'' dod'' re'' |
    re''2. dod''4 |
    re''2 re''4 do'' |
    si'2 la'4 sol' |
    fad'2. fad'4 |
    sol'2 la'4 si' |
    si'2( la') |
    sol'2 r4
  }
  \tag #'(melisse basse) {
    << { s1 s4 } \tag #'melisse { \melisseClef R1 | r4 } >>
    \tag #'basse \melisseMark r8 mi'' si'8. si'16 si'8. do''16 |
    re''8. re''16 re''8. re''16 do''8. si'16 |
    la'4
    << { s2 s2. s4. } \tag #'melisse { r2 | R2. | r4 r8 } >>
    \tag #'basse \melisseMark mi'' la'16 la' si' do'' |
    si'8. si'16 dod''8 re'' mi''[ red''16] mi'' |
    red''4
    << { s2.*6 s1*3 s2.*21 s4 } \tag #'melisse { r4 r2 | R2.*5 | R1*3 | R2.*21 | r4 } >>
    \tag #'basse \melisseMark r8 si'16 si' mi''8 mi''16 mi'' |
    si'8 r16 si'16 sol'4 sol'8 sol'16 fad' |
    fad'2. |
    sol'4 la' si' |
    fad' sol' mi' |
    si'2 mi''4 |
    do''2 do''8. do''16 |
    do''2 si'4 |
    si'( la'2) |
    sol'2. |
    sol'2 si'8 do'' |
    re''2 fad'4 |
    sol'4.( fad'8) sol'4 |
    fad'2 re''8. re''16 |
    re''2 do''4 |
    do''( si'2) |
    la' do''8. do''16 |
    do''2 si'4 |
    si'4( la'8[ sol']) la'4 |
    si'2. |
    r4 si'4. si'8 |
    do''2 r4 |
    r dod''4. dod''8 |
    \ficta red''2 mi''4 |
    mi''4( red''2) |
    mi''4
    << { s2. s1*3 s2. } \tag #'melisse {  r4 r2 | R1*3 | R2. } >>
    \tag #'basse \melisseMark
    \ru#2 {
      r4 si' si' |
      mi''4.( red''8) mi''4 |
      red''2 si'4 |
      fad' sol' la' |
      sol'2 mi'4 |
    }
    r2 mi''4 |
    si' si' do'' |
    re''2. |
    do''4 do'' la' |
    si'4 si' do'' |
    si'( la'2) |
    sol'2 si'4 |
    si'4 do'' re'' |
    do''2. |
    la'4 si' do'' |
    si' la'4. sol'8 |
    sol'4( fad'2) |
    mi'4 r si' si'16 si' si' do'' |
    la'4. la'16 sol' fad'4 fad'8 sol' |
    mi'4
    \tag #'melisse {
      r4 r2 | R1*2 | R2.*2 | R1 | R2. | R1*5 |
      R2.*2 | R1*5 | R2.*3 | R1 | R2. | R1 | R2. |
      R1 s4*0^\markup\italic { [Melisse se retire] } R1*2 |
      R2.*2 | R1*16 | r2 r4
    }
  }
>>

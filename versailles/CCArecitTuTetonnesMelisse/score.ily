\score {
  <<
    \new ChoirStaff \with { \forceGroupBracket \haraKiri } <<
      \new Staff \with { \cybeleInstr } \withLyrics <<
        \global \keepWithTag#'cybele \includeNotes "voix"
      >> \keepWithTag#'cybele \includeLyrics "paroles"
      \new Staff \with { \melisseInstr } \withLyrics <<
        \global \keepWithTag#'melisse \includeNotes "voix"
      >> \keepWithTag#'melisse \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout { }
  \midi { }
}
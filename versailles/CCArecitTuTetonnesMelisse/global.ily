\key mi \minor
\time 4/4 \midiTempo #80 s1*2
\digitTime\time 3/4 s2.*5
\time 4/4 s1
\digitTime\time 3/4 s2.*5
\time 4/4 s1*3
\digitTime\time 3/4 \midiTempo #160 s2.*21
\midiTempo #80 s2.*3
\midiTempo #160 \bar ".!:" s2.*6 \alternatives s2. s2. s2.*14
\time 4/4 \midiTempo #80 s1*4
\digitTime\time 3/4 \midiTempo #160 s2.*23
\time 4/4 \midiTempo #80 s1*5
\digitTime\time 3/4 s2.*2
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*5
\digitTime\time 3/4 s2.*2
\time 4/4 s1
\time 2/2 \midiTempo #160 s1
\time 4/4 \midiTempo #80 s1*3
\digitTime\time 3/4 s2.*3
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*3
\digitTime\time 3/4 s2.*2
\time 4/4 s1
\time 2/2 \midiTempo #160 s1*15 s2 s4 \bar ""

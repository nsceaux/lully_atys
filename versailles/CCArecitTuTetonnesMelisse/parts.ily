\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Cybele
    \livretVerse#12 { Tu tétonnes, Melisse, et mon choix te surprend ? }
    \livretPers Melisse
    \livretVerse#12 { Atys vous doit beaucoup, et son bonheur est grand. }
    \livretPers Cybele
    \livretVerse#12 { J’ay fait encor pour luy plus que tu ne peux croire. }
    \livretPers Melisse
    \livretVerse#12 { Est-il pour un Mortel un rang plus glorieux ? }
    \livretPers Cybele
    \livretVerse#8 { Tu ne vois que sa moindre gloire ; }
    \livretVerse#12 { Ce Mortel dans mon cœur est au dessus des Dieux. }
    \livretVerse#12 { Ce fut au jour fatal de ma derniere Feste }
    \livretVerse#12 { Que de l’aimable Atys je devins la conqueste : }
    \livretVerse#12 { Je partis à regret pour retourner aux Cieux, }
    \livretVerse#12 { Tout m’y parut changé, rien n’y pleût à mes yeux. }
    \livretVerse#7 { Je sens un plaisir extrème }
    \livretVerse#7 { A revenir dans ces lieux ; }
    \livretVerse#8 { Où peut-on jamais estre mieux, }
    \livretVerse#8 { Qu’aux lieux où l’on voit ce qu’on aime. }
    \livretPers Melisse
    \livretVerse#12 { Tous les Dieux ont aimé, Cybele aime à son tour. }
    \livretVerse#7 { Vous méprisiez trop l’Amour, }
    \livretVerse#7 { Son nom vous sembloit étrange, }
    \livretVerse#7 { A la fin il vient un jour }
    \livretVerse#5 { Où l’Amour se vange. }
    \livretPers Cybele
    \livretVerse#12 { J’ay crû me faire un cœur maistre de tout son sort, }
    \livretVerse#12 { Un cœur toûjours exempt de trouble et de tendresse. }
  }
  \column {
    \livretPers Melisse
    \livretVerse#5 { Vous braviez à tort }
    \livretVerse#5 { L’Amour qui vous blesse ; }
    \livretVerse#5 { Le cœur le plus fort }
    \livretVerse#7 { A des momens de foiblesse. }
    \livretVerse#12 { Mais vous pouviez aimer, et descendre moins bas. }
    \livretPers Cybele
    \livretVerse#12 { Non, trop d’égalité rend l’amour sans appas. }
    \livretVerse#8 { Quel plus haut rang ay-je à pretendre ? }
    \livretVerse#12 { Et dequoy mon pouvoir ne vient-il point à bout ? }
    \livretVerse#8 { Lors qu’on est au dessus de tout, }
    \livretVerse#12 { On se fait pour aimer un plaisir de descendre. }
    \livretVerse#12 { Je laisse aux Dieux les biens dans le Ciel preparez, }
    \livretVerse#12 { Pour Atys, pour son cœur, je quitte tout sans peine, }
    \livretVerse#12 { S’il m’oblige à descendre, un doux penchant m’entraîne ; }
    \livretVerse#12 { Les cœurs que le Destin à le plus separez, }
    \livretVerse#12 { Sont ceux qu’Amour unit d’une plus forte chaîne. }
    \livretVerse#12 { Fay venir le Sommeil ; que luy-mesme en ce jour, }
    \livretVerse#8 { Prenne soin icy de conduire }
    \livretVerse#8 { Les Songes qui luy font la Cour ; }
    \livretVerse#8 { Atys ne sçait point mon amour, }
    \livretVerse#12 { Par un moyen nouveau je pretens l’en instruire. }
    \livretDidasP\line { Melisse se retire. }
    \livretPers Cybele
    \livretVerse#12 { Que les plus doux Zephirs, que les Peuples divers, }
    \livretVerse#8 { Qui des deux bouts de l’Univers }
    \livretVerse#8 { Sont venus me montrer leur zele, }
    \livretVerse#8 { Celebrent la gloire immortelle }
    \livretVerse#12 { Du sacrificateur dont Cybele a fait choix, }
    \livretVerse#8 { Atys doit dispenser mes loix, }
    \livretVerse#8 { Honorez le choix de Cybele. }
  }
}#}))

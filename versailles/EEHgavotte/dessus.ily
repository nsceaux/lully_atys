\clef "dessus" mi''4 mi'' |
do'' la' fa''8 mi'' fa'' sol'' |
mi''4 mi'' re'' re''8 mi'' |
do''4 re''8 mi'' fa'' mi'' re'' do'' |
si'4 la' mi'' mi''8 fa'' |
sol''4 mi'' fa''8 sol'' fa'' mi'' |
re''2 sol''4 sol''8 mi'' |
fa''4 fa''8 re'' mi''4 mi''8 fa'' |
re''4 do'' sol'' fa''8 mi'' |
fa''4 re'' fa'' mi''8 re'' |
mi''4 do'' re''8 mi'' fa'' mi'' |
re'' mi'' do'' re'' si' do'' la' si' |
sold'2 mi''4 mi''8 re'' |
dod''4. re''8 mi'' fa'' sol'' mi'' |
fa''4 re''8 mi'' fa'' sol'' mi'' fa'' |
re'' mi'' do'' re'' si'4. la'8 |
la'2

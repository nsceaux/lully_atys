\clef "quinte"
do'4. do'8 |
sol'4 sol' mi'4. mi'8 |
la'4 la' fa' re' |
sol' do' sol2 |
do' do'4. do'8 |
fa'4 fa' re'4. re'8 |
mi'4 mi' do'4. do'8 |
fa'4 re' mi'4. mi'8 |
la2 la'4. la'8 |
re'4 re' la'4. fa'8 |
sol'4 sol' mi'4. mi'8 |
la'4 re' la2 |
re' re'4. re'8 |
sol'4 sol' do'4. do'8 |
fa'4 fa' fa'4. re'8 |
sol'4 do' sol2 |
do'
  
\clef "dessus" do''4. do''8 |
si'4 si' mi''4. mi''8 |
do''4 do'' la'4. la'8 |
si'4 do'' si'4. do''8 |
do''2 do''4. do''8 |
la'4 la' re''8 do'' si' la' |
sold'4 sold' do''4. do''8 |
la'4 si' sold'4. la'8 |
la'2 dod''4. dod''8 |
re''4 re'' do''!4. re''8 |
si'4 si' si'4. mi''8 |
dod''4 re'' dod''4. re''8 |
re''2 la'4. la'8 |
si'4 si' do''4. do''8 |
do''4 do'' la'4. la'8 |
si'4 do'' si'4. do''8 |
do''2

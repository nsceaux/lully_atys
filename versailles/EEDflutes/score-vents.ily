\score {
  <<
    \new GrandStaff \with { instrumentName = "Flûtes" } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with {
        instrumentName = \markup\center-column { "[Basse de flûte" "et] Bc" }
      } << \global \keepWithTag #'basse \includeNotes "basse" >>
    >>
  >>
  \layout { indent = 25\mm }
}

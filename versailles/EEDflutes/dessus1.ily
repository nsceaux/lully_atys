\clef "dessus" mi''4. mi''8 |
re''4 re'' sol''4. sol''8 |
mi''4 mi'' fa''4. fa''8 |
fa''4 mi'' re''4. do''8 |
do''2 mi''4. mi''8 |
do''4 do'' fa''8 mi'' re'' do'' |
si'4 si' mi''4. mi''8 |
do''4 re'' si'4. mi''8 |
dod''2 mi''4. mi''8 |
fa''4 fa'' mi''4. fa''8 |
re''4 re'' sol''4. sol''8 |
mi''4 fa'' mi''4. re''8 |
re''2 fa''4. fa''8 |
fa''4 fa'' mi''4. mi''8 |
mi''4 mi'' re''4. re''8 |
re''4 mi''8 fa'' re''4. do''8 |
do''2

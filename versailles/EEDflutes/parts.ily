\piecePartSpecs
#`((flutes-hautbois)
   (basse-continue #:clef "alto"
                   #:instrument , #{ \markup\center-column { BFl Bc } #})
   (basse-tous #:clef "alto"
               #:score-template "score"
               #:instrument , #{ \markup\center-column { BFl Bc } #}))

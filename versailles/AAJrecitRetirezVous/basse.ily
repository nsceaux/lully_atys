\clef "basse" do2 la, |
la, sol,4 |
fa,2 fa8. mi16 |
re2. |
la2 sold |
la2. la,4 |
mi4. re8 dod4 |
re2 re8 do si, la, |
sol,2 sol8 fa mi re |
do2 re4 re, |
sol,2 sol4 mi |
fa2 mi |
re2. do8 si, |
la,2 fa |
fad sol4. fa8 |
mi4 re8 do sol4 sol, |
do1 |

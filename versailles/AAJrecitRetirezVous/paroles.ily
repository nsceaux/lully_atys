Re -- ti -- rez- vous, ces -- sez de pre -- ve -- nir le Temps ;
Ne me des -- ro -- bez point de pre -- ci -- eux ins -- tants :
La puis -- san -- te Cy -- be -- le
Pour ho -- no -- rer A -- tys qu'elle a pri -- vé du jour,
Veut que je re -- nou -- vel -- le
Dans une il -- lus -- tre cour
Le sou -- ve -- nir de son a -- mour.
Que l'a -- gré -- ment rus -- ti -- que
De Flore et de ses jeux,
Cede à l'ap -- pa -- reil ma -- gni -- fi -- que
De la mu -- se tra -- gi -- que,
Et de ses spec -- ta -- cles pom -- peux.

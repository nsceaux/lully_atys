\melpomeneClef <>^\markup\italic { parlant à Flore }
r4 r16 sol' sol' sol' do''4. mi'8 |
fa'4 r16 fa' sol' la' sib'8. do''16 |
la'4. la'8 la'16 la' la' do'' |
fa'8. fa'16 fa'8. fa'16 fa'8([ mi'16]) fa' |
mi'4 r8 la'16 la' mi''4 mi''8 si' |
do''4 do'' r16 la' la' la' mi'8. fad'16 |
sol'8. sol'16 sol'8. sol'16 sol'8([ fad'16]) sol' |
fad'2 la'8 la'16 la' si'8. do''16 |
re''4 re'' si'8 si'16 si' do''8. re''16 |
mi''4 do''8 do''16 si' la'8. la'16 la'8. si'16 |
sol'2 si'8 si'16 si' do''8. do''16 |
la'4 la'8 re'' re''8. dod''16 dod''8. re''16 |
re''2 la'8. la'16 la'8. si'16 |
do''4 re''8. mi''16 la'4 la'8 fa''16 mi'' |
re''4 re''8 do'' si'4 si' |
do''8. do''16 re''8 mi'' re''4 re''8 mi'' |
do''1 |

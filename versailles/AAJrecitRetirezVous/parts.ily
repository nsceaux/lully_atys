\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Melpomene
    \livretVerse#12 { Retirez-vous, cessez de prevenir le Temps ; }
    \livretVerse#12 { Ne me desrobez point de precieux instants : }
    \livretVerse#6 { La puissante Cybele }
    \livretVerse#12 { Pour honorer Atys qu’elle a privé du jour, }
    \livretVerse#6 { Veut que je renouvelle }
    \livretVerse#6 { Dans une illustre cour }
  }
  \column {
    \null
    \livretVerse#8 { Le souvenir de son amour. }
    \livretVerse#6 { Que l’agrément rustique }
    \livretVerse#6 { De Flore et de ses jeux, }
    \livretVerse#8 { Cede à l’appareil magnifique }
    \livretVerse#6 { De la muse tragique, }
    \livretVerse#8 { Et de ses Spectacles pompeux. }
  }
} #}))

\clef "basse" sol8 sol |
do'2. la4 |
re'2 re'4 re |
sol2 sol |
mi2. mi4 |
la2 fad |
sol2 la4 la, |
re2. re8 re |
sol2. sol4 |
mi2 mi4 mi |
fa1 |
re2. re4 |
mi2 do |
re mi4 mi, |
la,2. la4 |
re2. re4 |
sol2. sol4 |
do2 do'4 si |
la2 la4 sol |
fad2. fad4 |
sol2 sol4 do |
re2 re, |
sol,1~ |
sol,1 |
sol4 fa mi re |
sol,2. sol4 |
do'2 do'4 si4 |
la2. la4 |
si1 |
mi2 la, |
si,4 si sol mi |
si,2 si4 la |
sol2 fad4 mi |
red2. red4 |
mi2 mi4 la, |
si,1 |
mi,2 mi |
la, re, |
mi,2. mi4 |
la2. re4 |
mi2. mi,4 |
la,2 la |
re sol, |
la,4 la fa re |
la,2 la4 sol |
fa2 mi4 re |
dod2. dod4 |
re2 re4 sol, |
la,1 |
re2. re4 |
sol2 si, |
do1 |
re2 si,4 sol, |
re2 re'4 do' |
si2 la4 sol |
fad2. fad4 |
sol2 sol4 do |
re1 |
sol,1 |

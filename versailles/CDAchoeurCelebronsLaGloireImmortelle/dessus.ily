\clef "dessus" sol''8 sol'' |
mi''2. la''4 |
fad''2 fad''4 fad'' |
sol''2 sol'' |
sol''4 sol''8 sol'' sol''4 sol'' |
mi''2 la''4. la''8 |
sol''4. fad''8 mi''4. re''8 |
re''2. fad''8 fad'' |
sol''2. re''4 |
mi''2 mi''4 mi'' |
do''2 do'' |
fa''4 \startBracket\markup { F-V: \italic { fa mi } } fa''8 fa''\stopBracket re''4. re''8 |
si'2 mi''4. mi''8 |
re''4. re''8 si'4. la'8 |
la'2. mi''4 |
fa''4 fa'' fa'' fa'' |
re''2. sol''4 |
mi''2 mi''4 re'' |
do''2 do''4. do''8 |
do''2. do''4 |
si'2 si'4 do'' |
si'2( la') |
sol'2 sol''4 fa'' |
mi''4 re''8 mi'' fa'' mi'' re'' do'' |
si'4. si'8 do''4. re''8 |
re''2~ re''4. do''8 |
do''4 mi'' mi'' mi'' |
mi''2. fad''4 |
red''2 si''4 la'' |
sol'' fad''8 sol'' la'' sol'' fad'' mi'' |
red''4. red''8 mi''4. fad''8 |
fad''2~ fad''4. mi''8 |
mi''2 mi''4 mi'' |
fad''2. fad''4 |
si'2 mi''4. mi''8 |
mi''2 red'' |
mi'' mi''4 re'' |
do'' si'8 do'' re'' do'' si' la' |
sold'2. si'4 |
do''4 do'' do'' re'' |
do''2( si'4.) la'8 |
la'2 la''4 sol'' |
fa''4 mi''8 fa'' sol'' fa'' mi'' re'' |
dod''4. dod''8 re''4. mi''8 |
mi''2~ mi''4. re''8 |
re''2 re''4 re'' |
mi''2. mi''4 |
la'2 re''4. re''8 |
\once\slurDashed re''2( dod'') |
re'' re''4 do'' |
si'4 sol' sol'' fa'' |
mi''8 fa'' mi'' re'' do'' re'' do'' si' |
la'4. la'8 si'4. do''8 |
\once\tieDashed la'2~ la'4. sol'8 |
sol'2 si'4 si' |
do''2. re''4 |
si'2 si'4 do'' |
si'2( la') |
sol'1 |

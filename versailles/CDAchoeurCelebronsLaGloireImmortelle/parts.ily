\piecePartSpecs
#`((dessus)
   (haute-contre)
   (taille)
   (quinte)
   (basse #:instrument ,#{\markup\center-column { BVn et Bc }#})
   (basse-continue #:instrument ,#{\markup\center-column { BVn et Bc }#})
   (basse-tous #:instrument ,#{\markup\center-column { BVn et Bc }#}
               #:score-template "score-basse-continue")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \column {
  \livretPers\line { Chœurs des Peuples & des Zephirs. }
  \livretVerse#8 { Celebrons la gloire immortelle }
  \livretVerse#12 { Du Sacrificateur dont Cybele a fait choix : }
  \livretVerse#8 { Atys doit dispenser ses loix, }
  \livretVerse#8 { Honorons le choix de Cybele. }
} #}))

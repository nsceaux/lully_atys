\clef "haute-contre" si'8 si' |
do''2. do''4 |
la'2 \startBracket\markup { F-V: \teeny\note {2} #.8 } la'4 la'\stopBracket |
si'2 si' |
si'4 si'8 si' si'4. si'8 |
dod''2 re''4. re''8 |
re''2 dod''4. re''8 |
re''2. re''8 re'' |
si'2. si'4 |
do''2 do''4 do'' |
la'2 la' |
re''4 re''8 re'' si'4. si'8 |
sold'2 la'4. la'8 |
la'2 sold'4. la'8 |
la'2. dod''4 |
re''4 re'' la' la' |
si'2. si'4 |
do''2 sol'4 sol' |
la'2 la'4. la'8 |
la'2. la'4 |
sol'2 sol'4 sol' |
sol'2 fad' |
sol' si'4 si' |
do''2 sol'4 sol' |
sol'2 sol'4. sol'8 |
sol'2. sol'4 |
sol'4 sol' do'' do'' |
do''2. do''4 |
si'2 si'4. si'8 |
si'2 la'4. sol'8 |
fad'4. fad'8 sol'4. la'8 |
fad'2 %{ sol'4 %} fad'4 fad' |
sol'2 sol'4 sol' |
la'2. la'4 |
sol'2 sol'4. la'8 |
sol'2 fad' |
mi' sold'4 sold' |
la'4. sol'8 fa'2 |
mi'2. sold'4 |
la'4 la' la' la' |
la'2( sold'4.) la'8 |
la'2 %{ dod''4. dod''8 %} dod''4 dod'' |
re''4. do''!8 sib'2 |
la'2. la'4 |
la'2 la'4 la' |
la'2 la'4 la' |
la'2. la'4 |
la'2 la'4 sib' |
la'2. %{ sol'4 %} la'4 |
fad'2 fad'4. fad'8 |
sol'2 sol'4. sol'8 |
sol'2 la'8 si' la' sol' |
fad'4. fad'8 sol'4. sol'8 |
fad'2 fad'4 fad' |
\startBracket\markup { F-V: \teeny\note{2.}#.8 } sol'2 sol'4\stopBracket sol' |
la'2. la'4 |
sol'2 sol'4 sol' |
sol'2 fad' |
sol'1 |

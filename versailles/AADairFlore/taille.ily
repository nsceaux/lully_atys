\clef "taille" sol'4 sol' sol' |
re' sol'2 |
sol'4. sol'8 la'4 |
re'4 re'4. do'8 |
si2. |
re'2 re'4 |
sib4 do'2 |
do'2 do'4 |
re'2 fa'4 |
mib' sol'4. sol'8 |
fa'2 fa'4 |
fa'2 fa'4 |
sol' sol' sol' |
re' sol'2 |
sol'4. sol'8 la'4 |
re'4 re'4. do'8 |
si2. |
sol'2 sol'4 |
sol' mi' mi' |
fa'4 fa'4. fa'8 |
mi'2 fa'4 |
fa'4. fa'8 sol'4 |
mi'4 la'2 |
la' sib'4 |
la'4. sol'8 fad'4 |
sol' sol' sol' |
re' sol'2 |
sol'4. sol'8 la'4 |
re'4 re'4. do'8 |
si2. |

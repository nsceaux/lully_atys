\clef "basse" sol4. la8 sib do' |
re'4 si2 |
do'4 sib la |
sol re re, |
sol,2. |
sol2 re4 |
mib4 do2 |
fa fa8 mib |
re4. do8 sib,4 |
mib2 mib4 |
fa2 fa,4 |
sib,2 sib8 la sol4. la8 sib do' |
re'4 si2 |
do'4 sib la |
sol re re, |
sol,2. |
sol4. la8 si4 |
do'2 sib4 |
la4. sol8 fa4 |
do' do fa |
sib2 sol4 |
la dod2 |
re sol,4 |
la,2 re4 |
sol4. la8 sib do' |
re'4 si2 |
do'4 sib la |
sol re re, |
sol,2. |

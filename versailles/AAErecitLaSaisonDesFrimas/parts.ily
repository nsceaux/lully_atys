\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Le Temps
    \livretVerse#12 { La saison des frimas peut-elle nous offrir }
    \livretVerse#8 { Les fleurs que nous voyons paraistre ? }
    \livretVerse#6 { Quel dieu les fait renaistre }
    \livretVerse#8 { Lorsque l’hyver les fait mourir ? }
    \livretVerse#7 { Le froid cruel regne encore ; }
    \livretVerse#7 { Tout est glacé dans les champs, }
    \livretVerse#4 { D’où vient que Flore }
    \livretVerse#6 { Devance le printemps ? }
  }
  \column {
    \livretPers Flore
    \livretVerse#12 { Quand j’attens les beaux jours, je viens toûjours trop tard, }
    \livretVerse#11 { Plus le printemps s’avance, et plus il m’est cõtraire ; }
    \livretVerse#8 { Son retour presse le départ }
    \livretVerse#8 { Du heros à qui je veux plaire. }
    \livretVerse#12 { Pour luy faire ma cour, mes soins ont entrepris }
    \livretVerse#12 { De braver desormais l’hyver le plus terrible, }
    \livretVerse#12 { Dans l’ardeur de luy plaire on a bien-tost apris }
    \livretVerse#8 { A ne rien trouver d’impossible. }
  }
}#}))

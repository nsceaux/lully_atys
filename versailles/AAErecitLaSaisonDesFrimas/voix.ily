<<
  \tag #'(temps basse) {
    \tempsClef r2 r4 re8. re16 |
    sol4 sol8. re16 mib4. mib8 |
    re8. re16 re8 mib fa4. fa8 |
    sol8. sol16 sol8. la16 sib8. do'16 |
    la8 la r8 do' do'8. do'16 sib8. la16 |% r8 do' do'4 do'8 sib16 la |
    sib8 sib sol sol16 la dod8. dod16 dod8. re16 |
    re4 r r8 re' re' fad |
    sol4 sol8. sol16 mi4 mi |
    do'4 do'8. do'16 fa4 fa8 fa |
    re4. fa8 sib8. sib16 |
    sol8 sol r sol la8. la16 la8. la16 |
    fad2
    \tag #'temps { r2 R1*8 R1. R1*20 R2. }
  }
  \tag #'(flore basse) {
    <<
      \tag #'basse { s1*3 s2. s1*5 s2. s1 s2 \floreMark }
      \tag #'flore { \floreClef R1*3 R2. R1*5 R2. R1 r2 \floreName }
    >> re''4. la'8 |
    sib'4. sib'8 la'2 |
    sol'2 r4 re'' |
    mib''4. re''8 do''4. sib'8 |
    la'2 r |
    sib'4 sib'8 do'' re''4. mib''8 |
    fa''4 re'' mib''4. do''8 |
    re''4. mib''8 re''4( do'') |
    sib'2 r4 sib'8 la' |
    sol'2 do''4. do''8 do''4. sib'8 |
    la'2 sib'4 do'' |
    re''2. la'4 |
    fad' sol'8 la' sib'4( la') |
    sol'1 |
    r2 sib'4. re''8 |
    sib'2 sib'4. sib'8 |
    sol'2. sol'4 |
    do''2 sol'4 la'8 sib' |
    la'2 r4 fa'8 fa' |
    sib'4 sib'8 do'' re''4. re''8 |
    mib''4. re''8 do''4. fa''8 |
    re''4 sib' r fa'8 sol' |
    la'4 la'8 si' do''4. do''8 |
    re''4 mib'' re''4. do''8 |
    do''2 sol'8 sol' sol' la' |
    sib'4 do''8 re'' re''4( do'') |
    re''4 sib'8 la' sol'4 sol'8 sol' |
    mi'4. mi'8 la'8. la'16 la'8. la'16 |
    fad'2 sib'8 sib' do'' re'' |
    mib''4 do''8 sib' sib'4( la') |
    sol'2. |
  }
>>

% Le Temps
\tag #'(basse temps) {
  La Sai -- son des fri -- mas peut- el -- le nous of -- frir
  Les fleurs que nous voy -- ons pa -- rais -- tre ?
  Quel dieu les fait re -- nais -- tre
  Lors -- que l'hy -- ver les fait mou -- rir ?
  Le Froid cru -- el regne en -- co -- re ;
  Tout est gla -- cé dans les champs,
  D'où vient que Flo -- re
  De -- van -- ce le prin -- temps ?
}
% Flore
\tag #'(basse flore) {
  Quand j'at -- tens les beaux jours, je viens toû -- jours trop tard,
  Plus le prin -- temps s'a -- vance, et plus il m'est con -- trai -- re ;
  Son re -- tour pres -- se le dé -- part
  Du he -- ros à qui je veux plai -- re.
  Pour luy fai -- re ma cour mes soins ont en -- tre -- pris
  De bra -- ver de -- sor -- mais l'hy -- ver le plus ter -- ri -- ble,
  Dans l'ar -- deur de luy plaire on a \forceHyphen bien -- tost a -- pris
  À ne rien trou -- ver d'im -- pos -- si -- ble,
  Dans l'ar -- deur de luy plaire on a \forceHyphen bien -- tost a -- pris
  À ne rien trou -- ver d'im -- pos -- si -- ble.
}

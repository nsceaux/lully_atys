\clef "basse" sol,1 |
sol, |
sol,2 re |
mib2 mi4 |
fa2 fad |
sol4 sol, la,2 |
re, re2 |
si,2 do |
do la, |
sib,2. |
mib2 do |
re4. mi8 fad2 |
sol re |
mib si, |
do4. re8 mib4 do |
re4. mi8 fad4 re |
sol2 fa4 mib |
re sol do fa |
sib mib fa fa, |
sib,4. do8 re2 |
mib mi1 |
fa4 mib re do |
sib,2 do |
re4 mib8 do re4 re, |
sol,1 | \allowPageTurn
sol, |
sol, |
mib |
mi!1 |
fa2. fa8 mib |
re4 re8 do sib,4 sib |
mib2 fa4 fa, |
sib, sib,8 do re4 re8 mi |
fa2 mib |
re4 do sol sol, |
do4. re8 mib4 do |
sol4. fa8 mib2 |
re4 sol8 la sib4 si |
do'2 dod' |
re'8 do' sib la sol fa mib re |
do re mib do re4 re, |
sol,2. |

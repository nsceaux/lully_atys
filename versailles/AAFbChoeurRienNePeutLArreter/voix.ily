<<
  \tag #'vdessus {
    \clef "vbas-dessus" re''4.^\markup "Chœur des Heures" mib''8 |
    fa''4 fa'' mib'' |
    re''2 r4 |
    r re''4. mi''8 |
    fa''4 fa''4. fa''8 |
    fa''4( mi''2) |
    fa''4 fa''4. fa''8 |
    re''4 re''4. mi''8 |
    re''4( dod''2) |
    re''4 la'4. la'8 |
    si'4 si'4. si'8 |
    do''2 r4 |
    r do''4. do''8 |
    fa''8[\melisma sol'' fa'' mib'' re'' do''] |
    sib'4. la'8[ sib' do''] |
    re''4\melismaEnd re''4. mib''8 |
    re''4( do''2) |
    sib'4 re''4. re''8 |
    do''4 do'' sib' |
    la'2 r4 |
    r sib'4. sib'8 |
    do''[\melisma sib' do'' re'' mib'' do''] |
    re''[ do'' re'' mib'' fa'' re''] |
    mib''[ fa'' mib'' re'' do'' sib'] |
    la'4\melismaEnd la' sib' |
    sib'4( la'2) |
    sol'4 re''4. re''8 |
    mib''4 do''4. sib'8 |
    la'2 r4 |
    r sib'4. sib'8 |
    do''8[\melisma sib' do'' re'' mib'' do''] |
    re''[ do'' re'' mib'' fa'' re''] |
    mib''[ fa'' mib'' re'' do'' sib'] |
    la'4\melismaEnd la' sib' |
    sib'4( la'2) |
    sol'2. |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre2" sol'4. sol'8 |
    fa'4 fa' sol' |
    fa'2 r4 |
    r fa'4. sol'8 |
    la'4 fa'4. fa'8 |
    sol'2. |
    do'4 la'4. la'8 |
    fa'4 fa'4. sol'8 |
    mi'2( fad'8[ sol']) |
    fad'4 fad'4. fad'8 |
    sol'4 sol'4. sol'8 |
    sol'2 r4 |
    r fa'4. fa'8 |
    sib4 sib fa'8. fa'16 |
    sol'4 sol'4. sol'8 |
    fa'4 fa' fa' |
    fa'2( mib'4) |
    re'4 fa'4. sol'8 |
    la'4 la' sol' |
    fad'2 r4 |
    r sol'4. sol'8 |
    sol'4 fa'4. fa'8 |
    fa'4 fa' sib'8. sib'16 |
    sol'4 la'4. la'8 |
    fad'4 fad' sol' |
    sol'2( fad'4) | % sol'4( fad'2) |
    sol'4 sol'4. sol'8 |
    sol'4 mib'4. mib'8 |
    re'2 r4 |
    r sol'4. sol'8 |
    sol'4 fa'4. fa'8 |
    fa'4 fa' sib'8. sib'16 |
    sol'4 la'4. la'8 |
    fad'4 fad' sol' |
    sol'4( fad'2) |
    sol'2. |
  }
  \tag #'vtaille {
    \clef "vtaille" sib4. sib8 |
    sib4 sib sib |
    sib2 r4 |
    r re'4. re'8 |
    do'4 do' re' |
    sib2. |
    la4 re'4. re'8 |
    re'4 sib4. sib8 |
    la2. |
    la4 re'4. re'8 |
    re'4 re' re' |
    mib'2 r4 |
    r la4. la8 |
    re'8[\melisma mib' re' do' sib la] |
    sol4\melismaEnd mib mib' |
    re' sib4. sib8 |
    sib4( la2) |
    sib4 sib4. sib8 |
    la4 re'4. re'8 |
    re'2 r4 |
    r4 re'4. re'8 |
    do'4 do'4. fa'8 |
    fa'4 re' re'8. fa'16 |
    mib'4 mib'4. mib'8 |
    re'4 re' re' |
    re'2( la4) |
    sib4 sib4. sib8 |
    sib4 la4. sol8 |
    fad2 r4 |
    r re'4. re'8 |
    do'4 do'4. fa'8 |
    fa'4 re' fa'8. fa'16 |
    mib'4 mib'4. mib'8 |
    re'4 re' re' |
    re'2( la4) |
    si2. |
  }
  \tag #'vbasse {
    \clef "vbasse" sol4. sol8 |
    re4 re mib |
    sib,2 r4 |
    r sib4. sib8 |
    la4 la sib |
    sol2. |
    fa4 re4. re8 |
    sib4 sib sol |
    la2. |
    re4 re4. re8 |
    sol4 sol sol |
    do2 r4 |
    r fa4. fa8 |
    re8[\melisma do re mib fa re] |
    mib[ re mib fa sol la] |
    sib4\melismaEnd sib mib |
    fa2. |
    sib,4 sib4. sib8 |
    fad4 fad sol |
    re2 r4 |
    r sol4. sol8 |
    la8[\melisma sol la sib do' la] |
    sib[ la sib do' re' sib] |
    do'[ sib do' re' mib' do'] |
    re'4\melismaEnd re' sol |
    re2. |
    sol4 sol4. sol8 |
    do4 do4. do8 |
    re2 r4 |
    r sol4. sol8 |
    la[\melisma sol la sib do' la] |
    sib[ la sib do' re' sib] |
    do'[ sib do' re' mib' do'] |
    re'4\melismaEnd re' sol |
    re2. |
    sol, |
  }
>>


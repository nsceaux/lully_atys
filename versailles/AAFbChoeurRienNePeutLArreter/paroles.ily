\tag #'vdessus {
  Rien ne peut l'ar -- res -- ter
  Quand la gloi -- re l'ap -- pel -- le,
  Quand la gloi -- re l'ap -- pel -- le,
  Rien ne peut l'ar -- res -- ter
  Quand la gloi -- re l'ap -- pel -- le,
  Rien ne peut l'ar -- res -- ter
  Quand la gloi -- re l'ap -- pel -- le,
  Rien ne peut l'ar -- res -- ter
  Quand la gloi -- re l'ap -- pel -- le.
}
\tag #'vhaute-contre {
  Rien ne peut l'ar -- res -- ter
  Quand la gloi -- re l'ap -- pel -- le,
  Quand la gloi -- re l'ap -- pel -- le,
  Rien ne peut l'ar -- res -- ter,
  Quand la gloi -- re, quand la gloi -- re, la gloi -- re l'ap -- pel -- le,
  Rien ne peut l'ar -- res -- ter
  Quand la gloi -- re l'ap -- pel -- le,
  Quand la gloi -- re, la gloi -- re l'ap -- pel -- le,
  Rien ne peut l'ar -- res -- ter
  Quand la gloi -- re l'ap -- pel -- le,
  Quand la gloi -- re, la gloi -- re l'ap -- pel -- le.
}
\tag #'vtaille {
  Rien ne peut l'ar -- res -- ter
  Quand la gloi -- re l'ap -- pel -- le,
  Quand la gloi -- re l'ap -- pel -- le,
  Rien ne peut l'ar -- res -- ter,
  Quand la gloi -- re, la gloi -- re l'ap -- pel -- le,
  Rien ne peut l'ar -- res -- ter
  Quand la gloi -- re l'ap -- pel -- le,
  Quand la gloi -- re, la gloi -- re l'ap -- pel -- le,
  Rien ne peut l'ar -- res -- ter
  Quand la gloi -- re l'ap -- pel -- le,
  Quand la gloi -- re, la gloi -- re l'ap -- pel -- le.
}
\tag #'vbasse {
  Rien ne peut l'ar -- res -- ter
  Quand la gloi -- re l'ap -- pel -- le,
  Quand la gloi -- re l'ap -- pel -- le,
  Rien ne peut l'ar -- res -- ter
  Quand la gloi -- re l'ap -- pel -- le,
  Rien ne peut l'ar -- res -- ter
  Quand la gloi -- re l'ap -- pel -- le,
  Rien ne peut l'ar -- res -- ter
  Quand la gloi -- re l'ap -- pel -- le.
}

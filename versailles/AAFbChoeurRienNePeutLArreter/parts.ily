\piecePartSpecs
#`((dessus)
   (haute-contre)
   (taille)
   (quinte)
   (basse #:score "score-basse")
   (basse-continue #:indent 0
                   #:system-count 3)
   (basse-tous #:score "score-basse")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \column {
  \livretPers Chœur des Heures
  \livretVerse#6 { Rien ne peut l’arrester }
  \livretVerse#6 { Quand la Gloire l’appelle. }
}#}))

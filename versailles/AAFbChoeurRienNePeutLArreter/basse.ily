\clef "basse" <<
  \tag #'basse { sol4. sol8 | re4 re mib | }
  \tag #'(basse-continue tous) { sol4 sol | re2 mib4 | }
>>
sib,2 r4 |
r sib4. sib8 |
\twoVoices #'(basse basse-continue tous) <<
  { la4 la sib | }
  { la2 sib4 | }
>>
sol2. |
fa4 re4. re8 |
sib4 sib sol |
\twoVoices #'(basse basse-continue tous) <<
  { la2. | re4 re4. re8 | sol4 sol sol | }
  { la4 la,2 | re2 re4 | sol2 sol,4 | }
>>
do2 r4 |
r4 fa4. fa8 | 
re8 do re mib fa re |
mib re mib fa sol la |
\twoVoices #'(basse basse-continue tous) <<
  { sib4 sib mib | }
  { sib2 mib4 | }
>>
fa fa,2 |
sib,4 sib4. sib8 |
\twoVoices #'(basse basse-continue tous) <<
  { fad4 fad sol | re2 }
  { fad2 sol4 | re2 }
>> r4 |
r4 sol4. sol8 |
la sol la sib do' la |
sib la sib do' re' sib |
do' sib do' re' mib' do' |
re'4 re' sol |
re2. |
sol,4 sol4. sol8 |
<<
  \tag #'basse { do4 do4. do8 | }
  \tag #'basse-continue { do4 do2 | }
>>
re2. |
r4 <<
  \tag #'basse {  sol4. sol8 | }
  \tag #'basse-continue { sol4 sol | }
>>
la8 sol la sib do' la |
sib la sib do' re' sib |
do' sib do' re' mib' do' |
re'4 re' sol |
re2. |
sol,2. |

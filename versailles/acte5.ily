\newBookPart #'()
\act "Acte Cinquième"
\sceneDescription \markup \wordwrap-center {
  [Le théâtre change et représente des jardins agréables.]
}
\scene "Scène Première" "SCÈNE 1 : Célénus, Cybèle"
\sceneDescription\markup\smallCaps { Cybèle, Célénus, [Mélisse.] }
%% 5-1
\pieceToc "Ritournelle"
\includeScore "FAAritournelle"
%% 5-2
\pieceToc\markup\wordwrap {
  Récit : \italic { Vous m’ôtez Sangaride, inhumaine Cybèle }
}
\includeScore "FABvousMotezSangaride"
\partNoPageTurn#'(basse)
\newBookPart#'(full-rehearsal)

\scene "Scène II" \markup \wordwrap {
  SCÈNE 2 : Célénus, Cybèle, Sangaride, Atys
}
\sceneDescription\markup\column {
  \line\smallCaps { Célénus, Cybèle, Sangaride, Atys, }
  \line { \smallCaps [Mélisse, troupe de prêtresses de Cybèle.] }
}
%% 5-3
\pieceToc\markup\wordwrap { \italic { Venez vous livrer au supplice } }
\includeScore "FBAvenezVousLivrerAuSupplice"
\newBookPart#'(full-rehearsal haute-contre haute-contre-sol taille quinte)

\scene "Scène III" \markup \wordwrap {
  SCÈNE 3 : Alecton, Atys, Sangaride, Cybèle, Célénus,
  troupe de prêtresses, chœur de Phrygiens
}
\sceneDescription\markup\column {
  \wordwrap-center\smallCaps {
    [Alecton, Atys, Sangaride, Cybèle, Célénus, Mélisse, Idas, Doris,
    troupe de prêtresses de Cybèle, Chœur de Phrygiens.]
  }
  \justify {
    [Alecton sort des Enfers, tenant à la main un flambeau
    qu’elle secoue en volant et en passant au dessus d’Atys.]
  }
}
%% 5-4
\pieceToc "Prélude [pour Alecton]"
\includeScore "FCApreludePourAlecton"
%% 5-5
\pieceToc\markup\wordwrap {
  Récit : \italic { Ciel ! quelle vapeur m’environne }
}
\includeScore "FCBcielQuelleVapeurMenvironne"

\scene "Scène IV" \markup\wordwrap { SCÈNE 4 : Cybèle, Atys }
\sceneDescription\markup\wordwrap-center\smallCaps {
  Cybèle, Atys, [Mélisse, Idas, chœur de Phrygiens.]
}
%% 5-6
\pieceToc\markup\wordwrap {
  Récit : \italic { Que je viens d’immoler une grande victime }
}
\includeScore "FDAqueJeViensDimmoler"
\newBookPart#'(full-rehearsal)

\scene "Scène V" "SCÈNE 5 : Cybèle, Mélisse"
\sceneDescription\markup\smallCaps { Cybèle, Mélisse. }
%% 5-7
\pieceToc\markup\wordwrap {
  Récit : \italic { Je commence à trouver sa peine trop cruelle }
}
\includeScore "FEAjeCommenceATrouverSaPeineTropCruelle"
\newBookPart#'(haute-contre haute-contre-sol taille quinte)
\partNoPageTurn#'(basse)

\scene "Scène VI" \markup \wordwrap {
  SCÈNE 6 : Cybèle, Atys, Idas, Mélisse
}
\sceneDescription\markup\center-column {
  \line\smallCaps { Cybèle, Atys, Idas, Mélisse, }
  \line\smallCaps { [prêtresses de Cybèle.] }
}
%% 5-8
\pieceToc\markup\wordwrap {
  Récit : \italic { Il s’est percé le sein }
}
\includeScore "FFAilSestPerceLeSein"
\newBookPart#'(full-rehearsal)

\scene "Scène VII" \markup \wordwrap {
  SCÈNE 7 : Cybèle, troupe de nymphes et de corybantes
}
\sceneDescription\markup\column {
  \wordwrap-center\smallCaps {
    Cybèle, [troupe de nymphes des eaux, troupe de divinités des bois,
    troupe de corybantes.]  
  }
  \smaller\italic\justify {
    [Quatre nymphes chantantes. Huit dieux des bois chantants.
    Quatorze corybantes chantantes. Quatre pages.
    Huit corybantes dansantes. Trois dieux des bois, dansants.
    Trois nymphes dansantes.]
  }
}
%% 5-9
\pieceToc "Ritournelle"
\includeScore "FGAritournelle"
\newBookPart#'(full-rehearsal)
%% 5-10
\pieceToc\markup\wordwrap {
  Récit : \italic { Venez furieux corybantes }
}
\includeScore "FGBvenezFurieuxCorybantes"
%% 5-11
\pieceToc\markup\wordwrap {
  Air, chœur : \italic { Atys, l’aimable Atys, malgré tous ses attraits }
}
\includeScore "FGCairChoeurAtysAimableAtys"

\sceneDescription\markup\justify {
  [Les divinités des bois et des eaux, avec les corybantes, honorent
  le nouvel arbre, et le consacrent à Cybèle.  Les regrets des
  divinités des bois et des eaux, et les cris des corybantes, sont
  secondés et terminés par des tremblements de terre, par des éclairs,
  et par des éclats de tonnerre.]
}
%% 5-12
\pieceToc "Entrée des Nymphes"
\includeScore "FGDentreeNymphes"
%% 5-13
\pieceToc "Premiere entrée des Coribantes"
\includeScore "FGEentreeCorybantes"
%% 5-14
\pieceToc "Seconde entrée"
\includeScore "FGFsecondeEntreeCorybantes"
\newBookPart#'(full-rehearsal)
%% 5-15
\pieceToc\markup\wordwrap {
  Chœur : \italic { Que le malheur d’Atys afflige tout le monde }
}
\includeScore "FGGchoeurQueLeMalheurDAtys"
\actEnd "FIN DU CINQUIÈME ET DERNIER ACTE"

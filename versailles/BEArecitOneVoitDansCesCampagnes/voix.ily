<<
  \tag #'(atys basse) {
    \atysClef r4 r8 la do'8 do' do'8. re'16 |
    si8 si si16 si si si do'4 do'8 re' |
    mi'4
  }
  \tag #'doris { \dorisClef R1*2 r4 }
>>
<<
  \tag #'(doris basse) {
    \tag #'basse \dorisMark
    sold'16 sold' sold' sold' la'4 la'8. la'16 |
    fad'4. re''8 si'8. si'16 si'8. do''16 |
    la'4 la' r4 |
    R2.*3 |
  }
  \tag #'atys { r4 r2 R1 R2.*4 }
>>


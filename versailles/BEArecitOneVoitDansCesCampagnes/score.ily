\score {
  <<
    \new ChoirStaff \with { \forceGroupBracket \haraKiri } <<
      \new Staff \with { \dorisInstr } \withLyrics <<
        \global \keepWithTag #'doris \includeNotes "voix"
      >> \keepWithTag #'doris \includeLyrics "paroles"
      \new Staff \with { \atysInstr } \withLyrics <<
        \global \keepWithTag #'atys \includeNotes "voix"
      >> \keepWithTag #'atys \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout { system-count = 2 }
  \midi { }
}
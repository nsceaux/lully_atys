\version "2.24.0"
\include "common.ily"
\paper {
  %% plus d'espace entre titre et musique
  markup-system-spacing.padding = #2.5 % default: 0.5
  system-system-spacing.padding = #2.5 % default: 1
  %% de la place entre dernier system et copyright
  last-bottom-spacing.padding = #3
}
\layout {
  \context {
    \Lyrics
    %% de la place sous les paroles (basse continue)
    \override VerticalAxisGroup.nonstaff-unrelatedstaff-spacing.padding = #3
  }
}
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header { title = \markup\smallCaps Atys }
  %% Title page
  \markup\null\pageBreak
  \markup\null\pageBreak
  %% Table of contents
  \markuplist\abs-fontsize-lines #7
  \override-lines #'(use-rehearsal-numbers . #t)
  \override-lines #'(column-number . 2)
  \override-lines #'(rehearsal-number-gauge . "4-16b")
  \table-of-contents
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Prologue
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bookpart {
  \actn "Prologue"
  %% 0-1
  \pieceToc "Ouverture"
  \includeScore "AAAouverture"
  %% 0-2
  \pieceToc\markup\wordwrap {
    \italic { En vain j’ay respecté la celebre memoire }
  }
  \includeScore "AABrecitEnVainJaiRespecte"
  %% 0-3
  \pieceToc\markup\wordwrap { \italic { Ses justes loix } }
  \includeScore "AACchoeurSesJustesLois"
  %% 0-4
  \pieceToc "Premier air pour la suite de Flore"
  \includeScore "AADairFlore"
  %% 0-5
  \pieceToc\markup\wordwrap {
    \italic { La Saison des frimas peut-elle nous offrir }
  }
  \includeScore "AAErecitLaSaisonDesFrimas"
  %% 0-6
  \pieceToc\markup\wordwrap {
    \italic { Les Plaisirs à ses yeux ont beau se presenter }
  }
  \includeScore "AAFairChoeurLesPlaisirASesYeux"
  %% 0-7
  \pieceToc\markup\wordwrap {
    \italic { Rien ne peut l’arrester }
  }
  \includeScore "AAFbChoeurRienNePeutLArreter"
  %% 0-8
  \pieceToc "Air pour la suite de Flore"
  \includeScore "AAGgavotte"
}
\bookpart {
  %% 0-9
  \pieceToc\markup\wordwrap {
    \italic { Le printemps quelquefois est moins doux qu’il ne semble }
  }
  \includeScore "AAHairLePrintempsQuelqueFois"
  %% 0-9b
  \pieceTocNb "0-9b" "Air pour la suite de Flore"
  \reIncludeScore "AAGgavotte" "AAHbGavotte"
  %% 0-10
  \pieceToc "Prelude pour Melpomene"
  \includeScore "AAIpreludeMelpomene"
  %% 0-11
  \pieceToc\markup {
    \italic { Retirez-vous, cessez de prevenir le Temps }
  }
  \includeScore "AAJrecitRetirezVous"
  %% 0-12
  \pieceToc "Air pour la suite de Melpomene"
  \includeScore "AAKairMelpomene"
  %% 0-13
  \pieceToc "Ritournelle"
  \includeScore "AALaRitournelle"
  %% 0-14
  \pieceToc\markup\wordwrap {
    \italic { Cybele veut que Flore aujourd’huy vous seconde }
  }
  \includeScore "AALbCybeleVeutQueFlore"
}
\bookpart {
  %% 0-15
  \pieceToc "Menuet"
  \includeScore "AAMmenuet"
  %% 0-15b
  \pieceTocNb "0-15b" \markup\italic { Preparons de nouvelles festes }
  \reIncludeScore "AALcPreparonsDeNouvellesFetes"  "AALNpreparonsDeNouvellesFetes"
  \actEnd "FIN DU PROLOGUE"
  %% 0-16
  \pieceToc "Ouverture"
  \reIncludeScore "AAAouverture" "AANouverture"
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Acte I
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bookpart {
  \act "Acte Premier"
  \scene "Scene VII" "SCENE 7 : Sangaride, Atys, chœur de Phrygiens"
  %% 1-8
  \pieceTocNb "1-8" \markup\wordwrap\italic { Mais déja de ce mont sacré }
  \includeScore "BGAairMaisDejaDeCeMontSacre"
  \includeScore "BGAbChoeurCommencons"
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Acte II
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bookpart {
  \act "Acte Second"
  \scene "Scene IV" \markup\wordwrap {
    SCENE 4 : Cybele, Atys, troupes de pleuples et de Zephirs
  }
  %% 2-6
  \pieceTocNb "2-6" \markup\wordwrap\italic { Celebrons la gloire immortelle }
  \includeScore "CDAchoeurCelebronsLaGloireImmortelle"
  %% 2-7
  \pieceTocNb "2-7" "[Premier air]"
  \includeScore "CDBentreeNations"
  %% 2-8
  \pieceTocNb "2-8" "[Deuxième air – L’Écho]"
  \includeScore "CDCentreeZephirs"
  %% 2-9
  \pieceTocNb "2-9" \markup\wordwrap\italic {
    Que devant vous tout s'abaisse, & tout tremble
  }
  \includeScore "CDDchoeurQueDevantVous"
}
\bookpart {
  %% 2-10
  \pieceTocNb "2-10" \markup\wordwrap\italic {
    Indigne que je suis des honneurs qu’on m’adresse
  }
  \includeScore "CDEindigneQueJeSuis"
  %% 2-11
  \pieceTocNb "2-11" \markup\wordwrap\italic { Que la puissante Cybele }
  \includeScore "CDFchoeurQueLaPuissanteCybele"
  %% 2-12
  \pieceTocNb "2-12" \markup\wordwrap\italic {
    Que devant vous tout s'abaisse, & tout tremble
  }
  \reIncludeScore "CDDchoeurQueDevantVous" "CDGchoeurQueDevantVous"
  %% 2-13
  \pieceTocNb "2-13" "[L’Écho]"
  \reIncludeScore "CDCentreeZephirs" "CDHentreeZephirs"
  \actEnd "FIN DU SECOND ACTE"
  \tocBreak
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Acte III
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bookpart {
  \act "Acte Troisiesme"
  \scene "Scene IV" "SCENE 4 : Sommeil"
  %% 3-6
  \pieceTocNb "3-6" \markup\wordwrap { Prelude du Sommeil }
  \includeScore "DDApreludeSommeil"
  %% 3-7
  \pieceTocNb "3-7" \markup\wordwrap\italic { Dormons, dormons tous }
  \includeScore "DDAdormons"
  %% 3-8
  \pieceTocNb "3-8" \markup\wordwrap { Prelude du Sommeil }
  \reIncludeScore "DDApreludeSommeil" "DDAcprelude"
  %% 3-9
  \pieceTocNb "3-9" \markup\wordwrap\italic { Escoute, escoute Atys la gloire qui t’appelle }
  \includeScore "DDBecouteAtys"
  %% 3-10
  \pieceTocNb "3-10" "Les Songes agreables"
  \includeScore "DDCentreeSongesAgreables"
  %% 3-10b
  \pieceTocNb "3-10b" \markup\wordwrap\italic { Trop heureux un amant }
  \includeScore "DDBbTropHeureuxUnAmant"
  %% 3-10c
  \pieceTocNb "3-10c" "Les Songes agreables"
  \includeScore "DDCbairSongesAgreables"
}
\bookpart {
  %% 3-11
  \pieceTocNb "3-11" \markup\wordwrap\italic { Gouste en paix chaque jour une douceur nouvelle }
  \includeScore "DDDgouteEnPaix"
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Acte IV
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bookpart {
  \act "Acte Quatriesme"
  \scene "Scene V" \markup\wordwrap { SCENE 5 : Le Fleuve Sangar, les Fleuves }
  %% 4-7
  \pieceTocNb "4-7" "Prelude pour les fleuves"
  \includeScore "EEAprelude"
  %% 4-8
  \pieceTocNb "4-8" \markup\wordwrap\italic { O vous, qui prenez part au bien de ma famille }
  \includeScore "EEBOVousQuiPrenezPart"
  %% 4-9
  \pieceTocNb "4-9" \markup\wordwrap\italic { Que l’on chante, que l’on dance }
  \includeScore "EECairQueLonChante"
  %% 4-10
  \pieceTocNb "4-10" \markup\wordwrap\italic { Que l’on chante, que l’on dance }
  \includeScore "EECchoeurQueLonChante"
  %% 4-11
  \pieceTocNb "4-11" \markup\wordwrap\italic { La beauté la plus severe }
  \includeScore "EEDflutes"
  \includeScore "EEDairLaBeauteLaPlusSevere"
  %% 4-12
  \pieceTocNb "4-12" \markup\wordwrap\italic { L’Hymen seul ne sçauroit plaire }
  \includeScore "EEEflutes"
  \includeScore "EEEairLhymenSeulNeSauraitPlaire"
  %% 4-11b
  \pieceTocNb "4-11b" \markup\wordwrap\italic { Il n’est point de resistance }
  \reIncludeScore "EEDflutes" "EEDflutesBis"
  \includeScore "EEDairIlNEstPointDeResistance"
  %% 4-12b
  \pieceTocNb "4-12b" \markup\wordwrap\italic { L’Amour trouble tout le monde }
  \reIncludeScore "EEEflutes" "EEEflutesBis"
  \includeScore "EEEairLAmourTroubleToutLeMonde"
}
\bookpart {
  %% 4-13
  \pieceTocNb "4-13" "Menuet"
  \includeScore "EEFmenuet"
  %% 4-14
  \pieceTocNb "4-14" \markup\wordwrap\italic { D’une constance extresme }
  \includeScore "EEGduneConstanceExtreme"
  %% 4-13b
  \pieceTocNb "4-13b" "Menuet"
  \includeScore "EEFmenuetBis"
  %% 4-14b
  \pieceTocNb "4-14b" \markup\wordwrap\italic { Jamais un cœur volage }
  \includeScore "EEGjamaisUnCoeurVolage"
  %% 4-15
  \pieceTocNb "4-15" "Gavotte"
  \includeScore "EEHgavotte"
  %% 4-16
  \pieceTocNb "4-16" \markup\wordwrap\italic { Un grand calme est trop fascheux }
  \includeScore "EEIchoeurUnGrandCalmeEstTropFacheux"
  %% 4-15b
  \pieceTocNb "4-15b" "Gavotte"
  \reIncludeScore "EEHgavotte" "EEHgavotteBis"
  %% 4-16b
  \pieceTocNb "4-16b" \markup\wordwrap\italic { Un grand calme est trop fascheux }
  \reIncludeScore "EEIchoeurUnGrandCalmeEstTropFacheux" "EEIchoeurUnGrandCalmeEstTropFacheuxBis"
  \scene "Scene VI" \markup \wordwrap { SCENE 6 : Atys, Celænus, Sangar, troupe de dieux des fleuves }
  %% 4-17
  \pieceTocNb "4-17" \markup\wordwrap\italic { Venez formez des nœuds charmants }
  \includeScore "EFAchoeurVenezFormerDesNoeudsCharmants"
  %% 4-18
  \pieceTocNb "4-18" "Entr’acte"
  \includeScore "EFBentracte"
  \actEnd "FIN DU QUATRIESME ACTE"
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Acte V
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bookpart {
  \act "Acte Cinquiesme"
  \scene "Scene VII" "SCENE 7 : Cybele, chœur"
  %% 5-9
  \pieceTocNb "5-9" "Ritournelle"
  \includeScore "FGAritournelle"
  %% 5-10
  \pieceTocNb "5-10" \markup\wordwrap\italic { Venez furieux Corybantes }
  \includeScore "FGBvenezFurieuxCorybantes"
  %% 5-10b
  \pieceTocNb "5-10b" "Ritournelle"
  \reIncludeScore "FGAritournelle" "FGCritournelle"
  %% 5-11
  \pieceTocNb "5-11" \markup\wordwrap\italic { Atys, l’aimable Atys, malgré tous ses attraits }
  \includeScore "FGCairChoeurAtysAimableAtys"
  \includeScore "FGCquelleDouleur"
  %% 5-12
  \pieceTocNb "5-12" "Entrée des nymphes"
  \includeScore "FGDentreeNymphes"
}
\bookpart {
  %% 5-13
  \pieceTocNb "5-13" "[Première entrée des] corybantes"
  \includeScore "FGEentreeCorybantes"
  %% 5-14
  \pieceTocNb "5-14" "[Seconde entrée des corybantes]"
  \includeScore "FGFsecondeEntreeCorybantes"
  %% 5-15
  \pieceTocNb "5-15" \markup\wordwrap\italic { Que le malheur d’Atys afflige tout le monde }
  \includeScore "FGGchoeurQueLeMalheurDAtys"
  \markup\null
  \actEnd "FIN DU CINQUIESME ET DERNIER ACTE"
}


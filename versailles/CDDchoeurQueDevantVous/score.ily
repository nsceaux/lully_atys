\score {
  <<
    \new ChoirStaff <<
      \new Staff \with { \dchantInstr } \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \includeLyrics "paroles"
      \new Staff \with { \hcchantInstr } \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \includeLyrics "paroles"
      \new Staff \with { \tchantInstr } \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \includeLyrics "paroles"
      \new Staff \with { \btchantInstr } \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \includeLyrics "paroles"
    >>
    \new StaffGroup <<
      \new Staff \with { \dvInstr } <<
        { s2.*8 \footnoteHere#'(0 . 0) \markup { F-V : DVn n’est pas copié à partir de la mesure 9. } }
        \global \includeNotes "dessus"
      >>
      \new Staff \with { \hcvInstr } << \global \includeNotes "haute-contre" >>
      \new Staff \with { \tvInstr } << \global \includeNotes "taille" >>
      \new Staff \with { \qvInstr } << \global \includeNotes "quinte" >>
      \new Staff \with { \bvbcInstr } <<
        \footnoteHere#'(0 . 0) \markup\wordwrap {
          F-V : Les lignes de BVn et Bc ne sont pas copiées. Indication sur la basse
          du chœur : \italic { “basse continue et basse pour violon en bas” }
        }
        \global \includeNotes "basse"
        { s2.*8\break s2.*8\break }
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout { }
  \midi { }
}

\clef "dessus" re''4 re'' fa'' |
sib' sib' mib'' |
do'' do'' fa'' |
re''2 sib'4 |
do'' do'' re'' |
sib'4. sib'8 do''4 |
re'' do''4. re''8 |
re''2. |
re''4 re'' mi'' |
fa''2 fa''4 |
fa''4 mi''4. re''16 mi'' |
fa''2 do''4 |
fa'' re'' sol'' |
mi'' fa'' re'' |
mi'' dod''4. dod''8 |
re''2. |
re''4 sol'' re'' |
mib''2 do''4 |
do'' fa'' do'' |
re''2 sib'4 |
do'' do'' re'' |
mib''4. re''8 do''4 |
sib'4 la'4. sol'8 |
sol'2. |

\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "dessus" >>
    \new Staff << \global \includeNotes "haute-contre" \clef "treble" >>
    \new Staff << \global \includeNotes "taille" \clef "treble" >>
  >>
  \layout { }
}

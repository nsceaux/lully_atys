\piecePartSpecs
#`((dessus #:system-count 3)
   (haute-contre #:system-count 3)
   (taille #:system-count 3)
   (quinte #:system-count 3)
   (basse #:system-count 3
          #:instrument , #{\markup\center-column { BVn et Bc }#})
   (basse-continue #:instrument , #{\markup\center-column { BVn et Bc }#})
   (basse-tous #:score-template "score-basse-continue"
               #:instrument , #{\markup\center-column { BVn et Bc }#})
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers\line { Chœurs des Peuples & des Zephirs. }
    \livretVerse#10 { Que devant Vous tout s’abaisse, et tout tremble ; }
    \livretVerse#10 { Vivez heureux, vos jours sont nostre espoir : }
    \livretVerse#9 { Rien n’est si beau que de voir ensemble }
    \livretVerse#10 { Un grand merite avec un grand pouvoir. }
  }
  \column {
    \null
    \livretVerse#4 { Que l’on benisse }
    \livretVerse#4 { Le Ciel propice, }
    \livretVerse#4 { Qui dans vos mains }
    \livretVerse#6 { Met le sort des Humains. }
  }
}#}))

Que de -- vant vous tout s’a -- baisse, et tout trem -- ble ;
Vi -- vez heu -- reux, vos jours sont nostre es -- poir :
Rien n’est si beau que de voir en -- sem -- ble
Un grand me -- rite a -- vec un grand pou -- voir.
Que l’on be -- nis -- se
Le ciel pro -- pi -- ce,
Qui dans vos mains
Met le sort des hu -- mains.

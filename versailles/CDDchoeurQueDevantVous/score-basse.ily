\score {
  \new StaffGroup <<
    \new Staff \with { \bvInstr } << \global \keepWithTag #'basse \includeNotes "basse" >>
    \new Staff \with { \bcInstr } <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      { s2.*8\break s2.*8\break }
      \includeFigures "chiffres"
    >>
  >>
  \layout { short-indent = \indent }
}

<<
  \tag #'vdessus {
    \clef "vbas-dessus" re''4^\markup "Chœur des peuples et des Zephirs" re'' fa'' |
    sib' sib' mib'' |
    do'' do'' fa'' |
    re''2 sib'4 |
    do'' do'' re'' |
    sib'4. sib'8 do''4 |
    re'' do''4. re''8 |
    re''2. |
    re''4 re'' mi'' |
    fa''2 fa''4 |
    fa''4 mi''4. re''16[ mi''] |
    fa''2 do''4 |
    fa'' re'' sol'' |
    mi'' fa'' re'' |
    mi'' dod''4. dod''8 |
    re''2. |
    re''4 sol'' re'' |
    mib''2 do''4 |
    do'' fa'' do'' |
    re''2 sib'4 |
    do'' do'' re'' |
    mib''4. re''8 do''4 |
    sib'4 la'4. sol'8 |
    sol'2. |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre2" sol'4 sol' fa' |
    mib' mib' sol' |
    fa' fa' fa' |
    fa'2 re'4 |
    fa' fa' fa' |
    re'4. re'8 mib'4 |
    re' sol'4. la'8 |
    fad'2. |
    fa'4 fa' sol' |
    la'2 fa'4 |
    sib' sol'4. fa'16[ sol'] |
    la'2 la'4 |
    la' sol' sol' |
    la' la' fa' |
    sol' mi' la' |
    fad'2. |
    sol'4 re' sol' |
    sol'2 mib'4 |
    fa' do' fa' |
    fa'2 fa'4 |
    fa' fa' fa' |
    sol'4. la'8 fad'4 |
    sol' fad'4. sol'8 |
    sol'2. |
  }
  \tag #'vtaille {
    \clef "vtaille" sib4 sib sib |
    sol sol do' |
    la la la |
    sib2 sib4 |
    la la la |
    sol4. sol8 sol4 |
    la do'4. do'8 |
    la2. |
    sib4 sib sib |
    do'2 re'4 |
    re' do'4. do'8 |
    do'2 la4 |
    re' si mi' |
    dod' re' re' |
    sib la4. la8 |
    la2. |
    si4 si si |
    do'2 sol4 |
    la la la |
    sib2 sib4 |
    la la si |
    do' sol la |
    re' re'4. re'8 |
    si2. |
  }
  \tag #'vbasse {
    \clef "vbasse" sol4 sol re |
    mib mib do |
    fa fa fa |
    sib,2 sib,4 |
    fa fa re |
    sol4. sol8 sol4 |
    fa mib4. re8 |
    re2. |
    sib4 sib sib |
    la2 sib4 |
    sol do' do |
    fa2 fa4 |
    re sol mi |
    la fa sib |
    sol la la, |
    re2. |
    sol4 sol sol |
    do2 do4 |
    fa fa fa |
    sib,2 sib,4 |
    fa mib re |
    do sib, la, |
    sol, %{ re4 re %} re4. re8 |
    sol,2. |
  }
>>

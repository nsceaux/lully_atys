\clef "basse" la2. |
sol4 la sib |
fa fa,2 |
sib,4 sib2 |
la2. |
sol |
fa2. mib8 re |
do2. |
sol2 re4. mib?8 |
\once\tieDashed fa2~ fa8 mib re do |
sib,4 sib re2 |
mib2 fa4 fa, |
sib, sib fad2 |
sol4 mi fa4 mib8 re |
do4. re8 mib4 do |
sol2 re |
mib4. re8 do2 |
sol4 sol, do2~ |
do4 sol8 la sib2 |
la8 sib sol4 fa8 fa sol la |
sib2 re |
mib fa4 fa, |
sib,4. do8 sib,4 la, |
sol,2 sol |
fad fa |
mib mi |
fad sol4 sol, |
re2 dod |
re mib8 do re re, |
sol,1 |

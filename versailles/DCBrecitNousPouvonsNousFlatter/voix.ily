\atysClef fa'4 do'4. re'8 |
mib'2 mib'8 re' |
do'2 re'8 mib' |
re'2 re'4 |
do' do'4. re'8 |
sib2 la8 sib |
la2 fa'8 do'16 do' do'8 re' |
mib'8 r16 sol'16 do'8. do'16 sol8 la |
sib sib r re'16 mib' fa'8 fa'16 fa' fa'8. sol'16 |
do'2 do'2 |
re'4 re'8 fa' sib4 sib |
sol4 mib'8 re' do'4 do'8 re' |
sib2 re'8. re'16 re'8. la16 |
sib4 sib8. do'16 la4 r8 do'16 re' |
mib'4 mib'8 fa' sol' sol'16 fa' mib'8 mib'16 re' |
re'4 re' sib8 sib16 sib do'8. re'16 |
sol2 mib'8. mib'16 fa'8. sol'16 |
si4 si8. do'16 do'4 do'8 sol |
sol la sib do' re' re'16 re' re'8 mi' |
fa' fa' fa' mi' fa'2 |
r4 re'8 fa' sib4 sib8 sib |
sol4 mib'8 mib'16 re' re'4( do'8.) sib16 |
sib2 r |
r re'4 re'8 re' |
re'2 re'4 re'8 re' |
sol sol do' do' do'4 do'8 do' |
do'4 do'8 re' sib4 la8 sib |
la4 r8 la la8. la16 la8. la16 |
fad4 fad8 fad sol sol sol fad |
sol1*3/4 s4^\markup\right-align\line\italic { Atys descend. } |

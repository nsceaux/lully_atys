\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix"
                   #:indent 0)
   (basse-viole #:score-template "score-basse-viole-voix"
                #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPersDidas Atys seul
    \livretVerse#12 { Nous pouvons nous flater de l’espoir le plus doux }
    \livretVerse#8 { Cybele et l’Amour sont pour nous. }
    \livretVerse#12 { Mais du Devoir trahi j’entends la voix pressante }
    \livretVerse#8 { Qui m’accuse et qui m’épouvante. }
    \livretVerse#12 { Laisse mon cœur en paix, impuissante Vertu, }
    \livretVerse#8 { N’ay-je point assez combatu ? }
    \livretVerse#12 { Quand l’Amour malgré toy me contraint à me rendre, }
    \livretVerse#6 { Que me demandes-tu ? }
  }
  \column {
    \null
    \livretVerse#8 { Puisque tu ne peux me deffendre, }
    \livretVerse#6 { Que me sert-il d’entendre }
    \livretVerse#8 { Les vains reproches que tu fais ? }
    \livretVerse#12 { Impuissante Vertu laisse mon cœur en paix. }
    \livretVerse#8 { Mais le Sommeil vient me surprendre, }
    \livretVerse#12 { Je combats vainement sa charmante douceur. }
    \livretVerse#6 { Il faut laisser suspendre }
    \livretVerse#6 { Les troubles de mon cœur. }
  }
}#}))

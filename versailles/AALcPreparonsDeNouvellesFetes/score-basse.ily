\score {
  <<
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \tinyStaff \haraKiriFirst } \withTinyLyrics <<
        \global \keepWithTag #'basse \includeNotes "voix"
      >> \keepWithTag #'basse \includeLyrics "paroles"
    >>
    \new StaffGroup <<
      \new Staff \with { \bvInstr }<<
        \global \keepWithTag #'basse \includeNotes "basse"
      >>
      \new Staff \with { \bcInstr } <<
        \global \keepWithTag #'basse-continue \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout { short-indent = \indent }
}

\clef "dessus" sol''4. sol''8 |
mi''4 mi''8 mi'' la''4. la''8 |
fad''4 fad'' fad''4. fad''8 |
sol''2 la''4. la''8 |
re''2 sol''4 sol'' |
mi''2 mi''4 la'' |
fad''2 r |
R1*6 |\allowPageTurn
R2.*9 |\allowPageTurn
re''4 re'' sol'' |
fad''2. |
sol''4 sol''4. sol''8 |
sol''2 mi''4 |
do''4. do''8 do'' do'' |
fa''2 re''8 do'' |
si'4. dod''8 re''4 |
dod''2 dod''4 |
re'' re''4. re''8 |
si'2. |
R2. |
r4 r sol''4 |
mi''4. mi''8 mi'' mi'' |
la''4 la'' la'' |
fad''4. sold''8 la''4 |
sold''2 sold''4 |
la''4 la''4. la''8 |
fad''2. |
R2.*3 |
r4 r re''4 | % la'4
si'4. do''8 re'' si' |
mi''4 sol''4. sol''8 |
sol''4. sol''8 fad''4 |
sol''2 sol''4 |
sol'' sol''4. sol''8 |
mi''2. |
R2. |
r4 r mi''4 |
fa''4. fa''8 fa'' fa'' |
re''4 re'' re'' |
re''4. re''8 dod''4 |
re''2 re''4 |
re''4 re''4. re''8 |
si'2. |
mi''4 mi''4. mi''8 |
re''2 re''4 |
mi''4. mi''8 mi'' mi'' |
do''4 fa'' fa'' |
re''4. mi''8 fa''4 |
mi''2 mi''4 |
re'' re'' sol'' |
fad''!2. | \allowPageTurn
R2.*3 |
r4 r re''4 |
re''4. re''8 mi'' fa'' |
sol''4 do''4. re''8 |
si'4 si' do'' |
si'4( la'2) |
sol'2. |

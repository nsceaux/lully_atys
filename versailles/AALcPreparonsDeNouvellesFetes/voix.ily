<<
  %% Flore, Melpomene, Dessus
  \tag #'(flore melpomene vdessus basse) {
    <<
      \tag #'(flore melpomene vdessus) {
        \clef "vbas-dessus"
        \tag #'vdessus <>^\markup\raise#1 { Chœur des Heures }
        re''4. re''8 |
        mi''4 mi''8 mi'' do''4. do''8 |
        la'4 la' re''4. re''8 |
        re''2 do''4( si'8) do'' |
        si'2 re''4. re''8 |
        re''2 dod''4. re''8 |
        re''2 r2 |
      }
      \tag #'basse { s2 s1*6 }
    >>
    <<
      \tag #'vdessus { R1*6 R2. }
      \tag #'(flore basse) {
        <<
          \tag #'basse { s2 \floreMark }
          \tag #'flore r2
        >> re''4. re''8 |\noBreak
        mi''4 mi''8 mi'' re''4. do''8 |
        si'4 si' si'4. si'8 |
        mi''2 mi''4. mi''8 |
        re''2 re''4 do'' |
        si'2 la'4. sol'8 |
        fad'2. |
      }
      \tag #'(melpomene) {
        r2 si'4. si'8 |
        do''4 do''8 do'' si'4. la'8 |
        sol'4 sol' sol'4. sol'8 |
        do''2 do''4 do'' |
        si'2 si'4 do'' |
        re''2 do''4. si'8 |
        la'2. |
      }
    >>
  }
  %% Hautes-contre
  \tag #'vhaute-contre {
    \clef "vhaute-contre2" sol'4. sol'8 |
    sol'4 sol'8 sol' la'4. la'8 |
    fad'4 fad' fad'4. fad'8 |
    sol'2 la'4. la'8 |
    sol'2 sol'4. sol'8 |
    mi'2 mi'4. la'8 |
    fad'2 r |
    R1*6 R2.
  }
  %% Tailles
  \tag #'vtaille {
    \clef "vtaille" si4. si8 |
    do'4 mi'8 mi' mi'4. mi'8 |
    re'4 re' la4. la8 |
    sol2 re'4. re'8 |
    re'2 si4 si |
    la2 la4. la8 |
    la2 r |
    R1*6 R2.
  }
  %% Temps, Basses
  \tag #'(temps vbasse basse) {
    \clef "vbasse"
    <<
      \tag #'basse { r2 R1*5 r2 \tempsName }
      \tag #'(temps vbasse) {
        sol4. sol8 |
        do'4 do'8 do' la4. la8 |
        re'4 re' re4. re8 |
        mi2 fad4. fad8 |
        sol2 sol4. sol8 |
        la2 la4. la8 |
        re2
      }
    >>
    <<
      \tag #'basse { re4. re8 | sol2 }
      \tag #'temps {
        re4. re8 |
        sol4 sol8 sol sol4. sol8 |
        do4 do re4. re8 |
        mi2 %{ mi4. mi8 %} mi4 mi |
        do2 do4 do |
        sol2 sol4 la |
        si2 si4 do' |
        re'2. |
      }
      \tag #'vbasse { r2 | R1*6 R2. }
    >>
  }
  %% 2e chœur
  \tag #'vdessus2 {
    \clef "vbas-dessus"
    <>^\markup\raise#1 { Chœur des nymphes de Flore et des suivans de Melpomene }
    r2 R1*12 R2.
  }
  \tag #'vhaute-contre2 { \clef "vhaute-contre" r2 R1*12 R2. }
  \tag #'vtaille2 { \clef "vtaille" r2 R1*12 R2. }
  \tag #'vbasse2 { \clef "vbasse" r2 R1*12 R2. }
>>

%% Le temps des jeux et du repos
<<
  %% Flore, Melpomene, Dessus
  \tag #'(flore basse melpomene vdessus vdessus2) {
    <<
      \tag #'(flore basse) {
        \footnoteHere#'(0 . 0) \markup {
          Erreur de prosodie \italic (“Préparons…”). Nous suivons le texte de Valenciennes et Ballard.
        } re''4 re'' mi'' |
        mi''2 re''4 |
        re''( dod''4.) re''8 |
        re''2 re''4 |
        si'4. si'8 do'' re'' |
        mi''2 do''8 si'8 |
        la'4. si'8 do''4 |
        si'2 si'4 |
      }
      \tag #'melpomene {
        si'4 si' do'' |
        la'2 la'4 |
        sol'4.( fad'8) sol'4 |
        fad'2 si'4 |
        sol'4. sol'8 la' si' |
        do''2 la'8 sol'8 |
        sol'4. sol'8 fad'4 |
        sol'2 sol'4 |
      }
      \tag #'(vdessus vdessus2) R2.*8
    >>
    <<
      \tag #'basse { R2.*10 \floreName }
      \tag #'(flore melpomene vdessus vdessus2) {
        \tag #'vdessus <>^\markup\raise#1 { Chœur des Heures }
        \tag #'vdessus2 <>^\markup\raise#1 { Chœur des nymphes de Flore et des suivans de Melpomene }
        re''4 re''4. re''8 |
        re''2. |
        si'4 si'4. si'8 |
        do''2 mi''4 |
        do''4. do''8 do'' do'' |
        fa''2 re''8 do'' |
        si'4. dod''8 re''4 |
        dod''2 dod''4 |
        re''4 re''4. re''8 |
        si'2. |
      }
    >>
    <<
      \tag #'flore { re''4 re'' mi'' | re''2 }
      \tag #'melpomene { si'4 si' do'' | si'2 }
      \tag #'(vdessus vdessus2) { R2. | r4 r }
    >>
    <<
      \tag #'(flore melpomene vdessus vdessus2) {
        re''4 |
        mi''4. mi''8 mi'' mi'' |
        mi''4 mi''4. mi''8 |
        mi''4. mi''8 red''4 |
        mi''2 mi''4 |
        dod''4 dod''4. dod''8 |
        re''2. |
      }
    >>
    <<
      \tag #'flore {
        la'4 la' si' |
        do''2. |
        si'4 do''8[ si'] la'[ sol'] |
        la'2
      }
      \tag #'melpomene {
        fad'4 fad' sol' |
        la'2. |
        sol'4 la'8[ sol'] fad'[ mi'] |
        fad'2
      }
      \tag #'(vdessus vdessus2) { R2.*3 r4 r }
    >>
    <<
      \tag #'(flore melpomene vdessus vdessus2) {
        la'4 |
        si'4. do''8 re'' si' |
        mi''4 do''4. si'8 |
        la'4. si'8 do''4 |
        si'2 si'4 |
        si' si'4. mi''8 |
        dod''2. |
      }
    >>
    <<
      \tag #'flore { mi''4 mi''4. fa''8 | mi''2 }
      \tag #'melpomene { dod''4 dod''4. re''8 | dod''2 }
      \tag #'(vdessus vdessus2) { R2. r4 r }
    >>
    <<
      \tag #'(flore melpomene vdessus vdessus2) {
        mi''4 |
        fa''4. fa''8 fa'' fa'' |
        re''4 re'' re'' |
        re''4. re''8 dod''4 |
        re''2 re''4 |
        re'' re''4. re''8 |
        si'2. |
        mi''4 mi''4. mi''8 |
        re''2 re''4 |
        mi''4. mi''8 mi'' mi'' |
        do''4 do'' do'' |
        do''4. do''8 si'4 |
        do''2 do''4 |
        si'4 si'4. si'8 |
        la'2. |
      }
    >>
    <<
      \tag #'flore {
        la'4 la' si' |
        do''2. |
        si'4 do''8[ si'] la'[ sol'] |
        la'2
      }
      \tag #'melpomene {
        fad'4 fad' sol' |
        la'2. |
        sol'4 la'8[ sol'] fad'[ mi'] |
        fad'2
      }
      \tag #'(vdessus vdessus2) { R2.*3 r4 r }
    >>
    <<
      \tag #'(flore melpomene vdessus vdessus2) {
        la'4 |
        si'4. si'8 do'' re'' |
        mi''4 do''4. re''8 |
        si'4 si' do'' |
        si'( la'2) |
        sol'2. |
      }
    >>
  }
  %% Hautes-contre
  \tag #'(vhaute-contre vhaute-contre2) {
    R2.*8 |
    sol'4 sol'4. sol'8 |
    fad'2. |
    sol'4 sol'4. sol'8 |
    sol'2 sol'4 |
    fa'4. fa'8 fa' fa' |
    fa'2 fa'8 fa' |
    mi'4. mi'8 mi'4 |
    mi'2 mi'4 |
    fad'4 fad'4. fad'8 |
    sol'2. |
    R2. |
    r4 r sol'4 |
    sol'4. sol'8 sol' sol' |
    la'4 la' la' |
    fad'4. sold'8 la'4 |
    sold'2 sold'4 |
    mi'4 mi'4. mi'8 |
    fad'2. |
    R2.*3 |
    r4 r fad'4 |
    sol'4. sol'8 sol' sol' |
    sol'4 sol' sol' |
    sol'4. sol'8 fad'4 |
    sol'2 sol'4 |
    sol' sol'4. sol'8 |
    mi'2. |
    R2. |
    r4 r la'4 |
    la'4. la'8 la' la' |
    fa'4 fa' sol' |
    mi'4. fad'8 sol'4 |
    fad'2 fad'4 |
    sol' sol' re' |
    mi'2. |
    sol'4 sol'4. sol'8 |
    sol'2 sol'4 |
    sol'4. sol'8 sol' sol' |
    fa'4 fa' fa' |
    re'4. mi'8 fa'4 |
    mi'2 mi'4 |
    re' re'4. re'8 |
    re'2. |
    R2.*3 |
    r4 r fad'4 |
    sol'4. sol'8 sol' sol' |
    sol'4 sol' la' |
    sol'4 sol'4. sol'8 |
    sol'4( fad'2) |
    sol'2. |
  }
  %% Tailles
  \tag #'(vtaille vtaille2) {
    R2.*8 |
    si4 si4. si8 |
    la2. |
    mi'4 mi'4. mi'8 |
    mi'2 do'4 |
    la4. la8 la la |
    la4 la4. la8 |
    la4. la8 sold4 |
    la2 la4 |
    la4 la4. re'8 |
    re'2. |
    R2. |
    r4 r si4 |
    do'4. do'8 do' do' |
    do'4 do' do' |
    si4. si8 si4 |
    si2 si4 |
    la la4. la8 |
    la2. |
    R2.*3 |
    r4 r re'4 |
    re'4. re'8 re' re' |
    do'4 mi'4. mi'8 |
    re'4. re'8 re'4 |
    re'2 re'4 |
    mi' si4. si8 |
    la2. |
    R2. |
    r4 r dod'4 |
    re'4. re'8 la la |
    sib4 sib sib |
    la4. la8 la4 |
    la2 la4 |
    si4 si4. si8 |
    sold2. | % sol2.
    do'4 do'4. do'8 |
    si2 si4 |
    do'4. do'8 do' do' |
    la4 la la |
    sol4. sol8 sol4 |
    sol2 sol4 |
    sol4 sol4. sol8 |
    fad2. |
    R2.*3 |
    r4 r re'4 |
    re'4. re'8 do' si |
    do'4 mi'4. mi'8 |
    mi'4 mi' mi' |
    re'2( do'4) |
    si2. |
  }
  %% Temps, Basses
  \tag #'(temps vbasse vbasse2) {
    <<
      \tag #'temps {
        sol4 sol4. sol8 |
        fad2 fad4 |
        mi2~ mi8 re |
        re2 re4 |
        mi4. mi8 mi re |
        do2 do8 do |
        re4. do8 re4 |
        sol,2 sol,4 |
      }
      \tag #'(vbasse vbasse2) R2.*8
    >>
    sol4 sol4. sol8 |
    re2. |
    mi4 mi4. mi8 |
    do2 do4 |
    fa4. fa8 fa fa |
    re4 re re |
    mi4. re8 mi4 |
    la,2 la,4 |
    re4 re4. re8 |
    sol2. |
    <<
      \tag #'temps { sol4 sol4. sol8 | sol2 }
      \tag #'(vbasse vbasse2) { R2. r4 r }
    >> sol4 |
    do'4. do'8 do' do' |
    la4 la la |
    si4. la8 si4 |
    mi2 mi4 |
    la4 la4. la8 |
    re2. |
    <<
      \tag #'temps { re4 re4. re8 | re2.~ | re~ | re2 }
      \tag #'(vbasse vbasse2) { R2.*3 r4 r }
    >> re4 |
    sol4. la8 si sol |
    do'4 do'4 do' |
    re'4. re'8 re4 |
    sol2 sol4 |
    mi4 mi4. mi8 |
    la2. |
    <<
      \tag #'temps { la4 la4. la8 | la2 }
      \tag #'(vbasse vbasse2) { R2. r4 r }
    >> la4 |
    fa4. fa8 fa fa |
    sib4 sib sol |
    la4. sol8 la4 |
    re2 re4 |
    si, si,4. si,8 |
    mi2. |
    do4 do4. do8 |
    sol,2 sol4 |
    mi4. mi8 mi mi |
    fa4 fa fa |
    sol4. fa8 sol4 |
    do2 do4 |
    sol4 sol4. sol8 |
    re2. |
    <<
      \tag #'temps { re4 re4. re8 | re2.~ | re~ | re2 }
      \tag #'(vbasse vbasse2) { R2.*3 r4 r }
    >> re4 |
    sol4. fa8 mi re |
    do4 do la, |
    mi mi do |
    re2. |
    sol, |
  }
>>

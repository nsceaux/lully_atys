\tag #'(flore melpomene choeur) {
  Pre -- pa -- rons de nou -- vel -- les fes -- tes,
  Pro -- fi -- tons du loi -- sir du plus grand des he -- ros ;
}
\tag #'temps {
  Pre -- pa -- rez de nou -- vel -- les fes -- tes,
  Pro -- fi -- tez du loi -- sir du plus grand des he -- ros ;
}
\tag #'(temps basse) {
  Pré -- pa -- rez,
}
\tag #'(temps) {
  de nou -- vel -- les fes -- tes,
  Pro -- fi -- tez du loi -- sir du plus grand, du plus grand des he -- ros.
  Le temps des jeux et du __ re -- pos,
  Luy sert à me -- di -- ter de nou -- vel -- les con -- ques -- tes.
}
\tag #'(melpomene flore basse) {
  Pré -- pa -- rons de nou -- vel -- les fes -- tes,
  Pro -- fi -- tons du loi -- sir du plus grand des he -- ros.
}
%%
\tag #'(melpomene flore basse) {
  Le temps des jeux et du re -- pos,
  Luy sert à me -- di -- ter de nou -- vel -- les con -- ques -- tes.
}
\tag #'(melpomene flore temps choeur choeur2) {
  Le temps des jeux et du re -- pos,
  Luy sert à me -- di -- ter de nou -- vel -- les con -- ques -- tes.
  Le temps des jeux,
}
\tag #'(melpomene flore temps) {
  et du re -- pos,
}
\tag #'(melpomene flore temps choeur choeur2) {
  Luy sert à me -- di -- ter de nou -- vel -- les con -- ques -- tes.
  Le temps des jeux,
}
\tag #'(melpomene flore) {
  Le temps des jeux, et du re -- pos,
}
\tag #'(temps) {
  et du re -- pos, __
}
\tag #'(melpomene flore temps choeur choeur2) {
  Luy sert à me -- di -- ter de nou -- vel -- les con -- ques -- tes.
  Le temps des jeux,
}
\tag #'(melpomene flore temps) {
  et du re -- pos,
}
\tag #'(melpomene flore temps choeur choeur2) {
  Luy sert à me -- di -- ter de nou -- vel -- les con -- ques -- tes.
  Le temps des jeux, et du re -- pos,
  Luy sert à me -- di -- ter de nou -- vel -- les con -- ques -- tes.
  Le temps des jeux,
}
\tag #'(melpomene flore) {
  Le temps des jeux, et du re -- pos,
}
\tag #'(temps) {
  et du re -- pos, __
}
\tag #'(melpomene flore temps choeur choeur2) {
  Luy sert à me -- di -- ter de nou -- vel -- les con -- ques -- tes.
}

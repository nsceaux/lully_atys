\piecePartSpecs
#`((dessus #:score-template "score-voix"
           #:indent 0
           #:music ,#{ s2 s1*12 s2.*9\break #})
   (haute-contre #:score-template "score-voix"
                 #:indent 0
                 #:music ,#{ s2 s1*12 s2.*9\break #})
   (haute-contre-sol #:score-template "score-voix"
                     #:indent 0
                     #:music ,#{ s2 s1*12 s2.*9\break #})
   (taille #:score-template "score-voix"
           #:indent 0
           #:music ,#{ s2 s1*12 s2.*9\break #})
   (quinte #:score-template "score-voix"
           #:indent 0
           #:music ,#{ s2 s1*12 s2.*9\break #})
   (basse #:score "score-basse")
   (basse-continue #:score-template "score-basse-continue-voix"
                   #:instrument ,(and (= AALcPreparonsDeNouvellesFetesFirstBar 1) "Bc")
                   #:indent 0)
   (basse-viole #:score-template "score-basse-viole-voix"
                #:instrument ,(and (= AALcPreparonsDeNouvellesFetesFirstBar 1) "Bc")
                #:indent 0)
   (basse-tous #:score "score-basse")
   (flutes-hautbois #:on-the-fly-markup , #{ \markup\column {
  \line { \hspace#10 \smallCaps Tacet }
  \null
} #}))

\clef "basse"
%% Chœur
<<
  \tag #'basse {
    sol4. sol8 |
    do'4 do'8 do' la4. la8 |
    re'4 re' re4. re8 |
    mi2 fad4. fad8 |
    sol2 sol4 sol |
    la2 la4. la8 |
    re2 r |
  }
  \tag #'basse-continue {
    sol2 |
    do'2 la |
    re' re2 |
    mi2 fad |
    sol2. sol4 |
    la2 la, |
    re2. re4 |
  }
>>
<<
  \tag #'basse { R1*6 R2.*9 }
  \tag #'(basse-continue tous) {
    sol2 sol4 sol4 |
    do2 re |
    mi2 mi4 mi4 |
    do2 do4 do4 |
    sol2 sol4 la |
    si2 si4 do'4 |
    re'2 re4
    sol2 sol4
    fad2 fad4
    mi2. |
    re2 si,4 |
    mi2 mi8 re
    do2 do4 |
    re2 re,4 |
    sol,2 sol,4
  }
>>
\twoVoices #'(basse basse-continue tous) << { sol4 sol4. sol8 | } { sol2 sol4 | } >>
re2. |
\twoVoices #'(basse basse-continue tous) << { mi4 mi4. mi8 | } { mi2 mi4 | } >>
do2 do4 |
<<
  \tag #'basse {
    fa4. fa8 fa fa |
    re4 re re |
    mi4. mi8 mi,4 |
    la,2 la,4 |
    re re4. re8 |
    sol2. |
    R2. |
    r4 r sol4 |
    do'4. do'8 do' do' |
    la4 la la |
    si4. la8 si4 |
    mi2 mi4 |
    la4 la4. la8 |
    re2. |
    R2.*3 |
    r4 r re4 |
    sol4. la8 si sol |
    do'4 do' do' |
    re'4. re'8 re4 |
    sol2 sol4 |
    mi mi4. mi8 |
    la2. |
    R2. |
    r4 r la4 |
    fa4. fa8 fa fa |
    sib4 sib sol |
    la4. sol8 la4 |
    re2 re4 |
    si,4 si,4. si,8 |
    mi2. |
    do4 do do |
    sol,2 sol4 |
    mi4. mi8 mi mi |
    fa4 fa fa |
    sol4. fa8 sol4 |
    do2 do4 |
    sol sol4. sol8 |
    re2. |
    R2.*3 |
    r4 r re4 |
    sol4. fa8 mi re |
    do4 do la, |
    mi mi do |
    re2. |
    sol,2. |
  }
  \tag #'basse-continue {
    fa2 fa4 |
    re2 re4 |
    mi2 mi,4 |
    la,2 la,4 |
    re re re |
    sol2. | \allowPageTurn
    sol2 sol4 |
    sol2 sol4 |
    do'2 do'4 |
    la2 la4 |
    si2 si,4 |
    mi2 mi4 |
    la2 la4 |
    re2. | \allowPageTurn
    re |
    re |
    re |
    re2  re4 |
    sol2 sol4 |
    do'2 do'4 |
    re'2 re4 |
    sol2 sol4 |
    mi2 mi4 |
    la2.~ |
    la | \allowPageTurn
    la2 la4 |
    fa2 fa4 |
    sib2 sol4 |
    la2 la,4 |
    re2 re4 |
    si,2 si,4 |
    mi2. |
    do2 do4 |
    sol,2 sol4 |
    mi2 mi4 |
    fa2 fa4 |
    sol2 sol,4 |
    do2 do4 |
    sol sol2 |
    re2. | \allowPageTurn
    re |
    re |
    re |
    re2 re4 |
    sol4. fa8 mi re |
    do2 la,4 |
    mi2 do4 |
    re4 re,2 | % re2.
    sol,2. |
  }
>>

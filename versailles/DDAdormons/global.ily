\key re \minor
\digitTime\time 2/2 \midiTempo#120
s1*13 \bar ":|."
s1*107 \bar "|"
\when #(eqv? #t (ly:get-option 'print-footnotes))
\footnote #'(.1 . .1) \markup\wordwrap {
  F-V : aucune indication. Dans ms. Colmar : \italic { “Durant la cadance les violons redisent le prélude” } ;
  cette reprise se fait donc en tuilage.
} Score.TextMark
\endMarkSmall\markup { [On reprend le prélude.] }
\bar "|."

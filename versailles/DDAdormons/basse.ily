\clef "basse" re4( mi) fad( re) |
sol( la) sib( sol) |
fad( mi) fad( re) |
sol( fad?4) sol( sol,) |
re( mib) re( do) |
sib,( la,) sib,( sol,) |
\slurDashed do( sib,) do( la,) |
re( do) sib,( la,4) |
re( la) re'( do') |
sib( la) sib( sol) |
mib'( re') mib'( do') |
re'( do') re'( re) |  \slurSolid
sol( la) sib( sol) |
\slurDashed fad( mi) \footnoteHere#'(0 . 0) \markup {
  F-V : \italic fa♯, mais probable erreur, par confusion avec mes. 3.
} fa4( re) | \slurSolid
mib( re) mib( do) |
\once\slurDashed re( do) si,2 |
do4( re) mib( do) |
sol2 fa4 mib |
re2 do4 sib, |
\slurDashed fa( sol) la( fa) | \slurSolid
sib2 la |
sol1 |
fa |
mib |
re4( la,) sib,( sol,) |
fad,1 |
sol,4( re) sol( fa) |
mi4( re) \once\slurDashed mi( do) |
fa( do) \once\slurDashed fa( mib) |
re1 |
\slurDashed sol4( re) sol( fa) | \slurSolid
mib( re) mib( do) |
\slurDashed sol( fa) sol( sol,) | \slurSolid
do( re) mib( fa) |
sol sol, sol la |
sib sib, sib, do |
re sib, re mib |
fa( mib?) fa( fa,) |
sib,1 |
si, |
do2 dod |
re4 la, re do |
sib, la, sib, sol, |
re do re re, |
sol,2. sol4 |
fa2. fa4 |
mib1 |
re |
re'4. do'8 sib4. la8 |
sol2 fa4 mib |
re2 do4 sib, |
fa1 |
fa,2. fa4 |
sib4. la8 sol4. fa8 |
mib2 re4. mib8 |
do2 fa4 fa, |
sib,1 |
sib2. sib4 |
la2 sol |
fa2. fa4 |
sol1 |
la2. la4 |
fa2 dod4 re |
la,1 |
re4. mi8 fad2 |
sol2. sol4 |
fa2. sol4 |
mib2. fa4 |
re2. re4 |
do2. do4 |
sib,2. do4 |
re2 re, |
sol,4 la, sib, do |
re4( mi) fad( re) |
sol( la) sib( sol) |
fad( mi) fad( re) |
sol( fad?4) sol( sol,) |
re( mib) re( do) |
sib,( la,) sib,( sol,) |
\slurDashed do( sib,) do( la,) |
re( do) sib,( la,4) |
re( la) re'( do') |
sib( la) sib( sol) |
mib'( re') mib'( do') |
re'( do') re'( re) |  \slurSolid
sol2. sol4 |
sol1~ |
sol |
re |
la2. sol4 |
fad2 sol4. fa?8 |
mi2 fa~ |
fa mib4 re8 do |
si,2. si,4 |
do4 re mib fa |
sol1~ |
\tieDashed sol1~ |
sol~ |
sol2 sol4 fa | \tieSolid
mib re do sib, |
la,2. la,4 |
sib, do re mib |
fa1~ |
fa1~ |
fa~ |
fa |
sib,1 |
do |
re |
mib |
re2. re4 |
re1~ |
re |
re |
sol |
fa |
mib |
re |
re, |
sol,1 |

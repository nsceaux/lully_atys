\piecePartSpecs
#`((flutes-hautbois)
   (basse-continue ;#:score-template "score-basse-continue-voix"
                   #:indent 0)
   (basse-viole ;#:score-template "score-basse-viole-voix"
                #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers "Le Sommeil"
    \livretVerse#5 { Dormons, dormons tous ; }
    \livretVerse#7 { Ah que le repos est doux ! }
    \livretPers Morphée
    \livretVerse#12 { Regnez, divin Sommeil, regnez sur tout le monde, }
    \livretVerse#12 { Répandez vos pavots les plus assoupissans ; }
    \livretVerse#8 { Calmez les soins, charmez les sens, }
    \livretVerse#12 { Retenez tous les cœurs dans une paix profonde. }
  }
  \column {
    \livretPers Phobetor
    \livretVerse#8 { Ne vous faites point violence, }
    \livretVerse#8 { Coulez, murmurez, clairs Ruisseaux, }
    \livretVerse#8 { Il n’est permis qu’au bruit des eaux }
    \livretVerse#12 { De troubler la douceur d’un si charmant silence. }
    \livretPers\wordwrap { Le Sommeil, Morphée, Phobetor & Phantase }
    \livretVerse#5 { Dormons, dormons tous, }
    \livretVerse#7 { Ah que le repos est doux ! }
  }
}#}))

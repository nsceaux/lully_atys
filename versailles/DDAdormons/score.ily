\score {
  <<
    \new GrandStaff \with { \flInstr \haraKiriFirst } <<
      \new Staff <<
        { s1*72
          \footnoteHere#'(0 . 0) \markup\wordwrap {
            F-V : tuilage direct sur la levée de la mesure 86, sans la
            redite du récit du Sommeil, mais en contradiction avec le
            livret de 1676 (qui indique bien la présence du Sommeil
            avant le trio). Toutes les autres sources musicales
            indiquent également la reprise du récit du Sommeil. Nous
            le maintenons donc également.
          }
        }
        \global \includeNotes "flute1"
      >>
      \new Staff << \global \includeNotes "flute2" >>
    >>
    \new ChoirStaff \with { \forceGroupBracket \haraKiriFirst } <<
      \new Staff \with { \sommeilInstr } \withLyrics <<
        \global \keepWithTag #'sommeil \includeNotes "voix"
      >> \keepWithTag #'sommeil \includeLyrics "paroles"
      \new Staff \with { \morpheeInstr } \withLyrics <<
        \global \keepWithTag #'morphee \includeNotes "voix"
      >> \keepWithTag #'morphee \includeLyrics "paroles"
      \new Staff \with { \phantaseInstr } \withLyrics <<
        \global \keepWithTag #'phantase \includeNotes "voix"
      >> \keepWithTag #'phantase \includeLyrics "paroles"
      \new Staff \with { \phobetorInstr } \withLyrics <<
        \global \keepWithTag #'phobetor \includeNotes "voix"
      >> \keepWithTag #'phobetor \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
        \modVersion {
          % Le Sommeil
          s1*13\break
          % Morphée
          s1*31\break
          % Phobetor
          s1*29\break
          % Le Sommeil
          s1*6\break s1*5\break
          % Trio
        }
      >>
    >>
  >>
  \layout {
    \context {
      \Voice
      \consists "Horizontal_bracket_engraver"
      \override HorizontalBracket.direction = #UP
    }
  }
  \midi { }
}

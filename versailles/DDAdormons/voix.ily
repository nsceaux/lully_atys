<<
  %% Le Sommeil
  \tag #'(sommeil basse) {
    \tag #'basse \sommeilMark
    \tag #'sommeil \sommeilClef
    \clef "vhaute-contre" r2 r4 re' |
    re'1~ |
    re'2. la4 |
    sib1 |
    la |
    re'2. r8 fa'( |
    mi'2.) mi'4 |
    fad'4. fad'8 sol'4( fad'8) sol' |
    fad'1 |
    sol' |
    r4 sol' sol'4. sol'8 |
    sol'2( fad'4.) sol'8 |
    sol'1 |
    <<
      \tag #'basse { s1*60 \sommeilMark }
      \tag #'sommeil { R1*60 }
    >>
    r2 r4 re' |
    re'1~ |
    re'2. la4 |
    sib1 |
    la |
    re'2. r8 fa'( |
    mi'2.) mi'4 |
    fad'4 fad' sol'( fad'8) sol' |
    fad'1 |
    sol' |
    r4 sol' sol'4. sol'8 |
    <<
      \tag #'basse { sol'2( fad'4) }
      \tag #'sommeil { sol'2( fad'4.) sol'8 | sol'1 | R1*34 | }
    >>
  }
  %% Morphée
  \tag #'(morphee basse) {
    <<
      \tag #'basse { s1*13 \morpheeMark }
      \tag #'morphee { \morpheeClef R1*13 }
    >>
    r2 r4 re' |
    sol'2. la'4 |
    fad'4. fad'8 sol'4 re' |
    mib'2. mib'4 |
    re'2. mib'4 |
    fa'4. fa'8 mib'4. re'8 |
    do'2 do'4 r |
    r2 fa'4 fa' |
    sib2 sib4. do'8 |
    la2. re'4 |
    re' re' re' do' |
    re'1 |
    r4 la la la |
    sib1 |
    r4 sib sib4( la8) sib |
    la1 |
    r4 la re'4. mib'8 |
    si2. si4 |
    do'2. do'4 |
    do'2( si?4.) do'8 |
    do'2 sol4 la |
    sib2 sib4 do' |
    re'4. re'8 re'4 mib' |
    fa'2. sib4 |
    sib2.( la4) |
    sib2 re'4. re'8 |
    re'4. mi'8 fa'2 |
    mi'4. mi'8 mi'4. mi'8 |
    fa'?4. fa'8 fad'4. fad'8 |
    sol'2. sib4 |
    sib2( la) |
    <<
      \tag #'basse { s1*40 s2. \morpheeMark }
      \tag #'morphee {
        sol1 |
        R1*2 |
        R1*37 |
        r2 r4
      }
    >>
    re'4 | \noBreak
    re'2. re'4 |
    mib'1 |
    re'2 sol'~ |
    sol'4. fa'8 fa'4( mi'8) fa' |
    mi'2~ mi'4. re'8 |
    re'2. re'4 |
    do'4. do'8 do'2 |
    re'4. re'8 mib'4. mib'8 |
    fa'4 fa' fa' mib'8[ re'] |
    mib'2.( re'8) mib' |
    re'4 sol' fa' mib' |
    re' mib' fa' sol' |
    mib'2.( re'8) mib' |
    re'1 |
    sol'4 fa' mib'4. mib'8 |
    mib'2.( re'8) mib' |
    re'1 |
    do'4 fa' mib' re' |
    do' re' mib'4. fa'8 |
    re'2.~ re'8 mib' |
    do'1 |
    re'2 r |
    mi'4. mi'8 mi'4. mi'8 |
    fad'4. fad'8 fad'4. fad'8 |
    \once\slurDashed sol'2.( fad'8) sol' |
    fad'4 re' do' sib |
    la re' do' sib8[ la] |
    sib1 |
    la4. re'8 re'4. la8 |
    sib2 mib'~ |
    mib'4. re'8 re'4. re'8 |
    re'2 do'~ |
    do'4. do'8 sib4( la8) sib |
    sib2( la4.) sol8 |
    sol1 |
  }
  %% Phantase
  \tag #'phantase {
    \clef "vtaille" R1*84 |
    r2 r4 la |
    sib2. sib4 |
    do'1~ |
    do'4. sib8 sib2 |
    la re'~ |
    re'4. do'8 do'4. do'8 |
    do'4. do'8 sib4. sib8 |
    sib4( la8) sib la4. la8 |
    si4. si8 do'2 |
    re'4 re' re'2~ |
    re'4 do' do'4( si?8[ do']) |
    si4 mib' re' do' |
    si do' re'2~ |
    re'4 do' do'2~ |
    do'2( si) |
    do'2. do'4 |
    do'1~ |
    do'4 sib sib2 |
    la4 re' do' sib |
    la sib do'2~ |
    do'4 sib sib2~ |
    \once\slurDashed sib( la) |
    sib sib~ |
    sib4. la8 la4. la8 |
    la4. re'8 re'4. re'8 |
    \once\slurDashed re'2( do'4)( sib8) do' |
    re'4 sib la sol |
    fad sol \once\tieDashed la2~ |
    la4 sol sol2~ |
    \once\slurDashed sol( fad) |
    sol do'4. sib8 |
    la4. la8 la4. sib8 |
    sol4. sol8 sol4. la8^\markup\center-align\line\tiny { F-V: \italic si♭ } |
    fad4. fad8 sol4( fad8) sol |
    \once\slurDashed sol2( fad4.) sol8 |
    sol1 |
  }
  %% Phobetor
  \tag #'(phobetor basse) {
    <<
      \tag #'basse { s1*44 \phobetorMark }
      \tag #'phobetor { \phobetorClef R1*44 }
    >>
    sol4 sol sol sol |
    fa2 fa4 fa |
    mib1 |
    re2. r8 re' |
    re'4.(\melisma do'8 sib4. la8 |
    sol2)\melismaEnd fa4. mib8 |
    re2 do4 sib, |
    fa1 |
    r2 r4 r8 fa |
    sib4.(\melisma la8 sol4. fa8 |
    mib2)\melismaEnd re4. mib8 |
    do2 fa8[ mib] fa4 |
    sib,1 |
    r4 sib sib sib |
    la la sol4. sol8 |
    fa2 fa4 fa |
    sol2 sol4 sol |
    la2. la4 |
    fa4 fa dod re |
    la,1 |
    re |
    r4 sol sol sol |
    fa fa fa sol |
    mib2 mib4 fa |
    re2 re4 re |
    do do do do |
    sib,2. do4 |
    re1 |
    sol,2 r |
    \tag #'phobetor {
      R1*11 |
      r2 r4 re |
      sol2. sol4 |
      sol1~ |
      sol |
      re |
      la2. sol4 |
      fad4. fad8 sol4. fa?8 |
      mi2 fa~ |
      fa4. fa8 mib4 re8[ do] |
      si,2. si,4 |
      do re mib fa |
      sol2. r8 sol |
      sol1 |
      sol |
      r2 sol4 fa |
      mib re do sib, |
      la,2. la,4 |
      sib, do re mib |
      fa2. fa4 |
      fa1~ |
      fa2. fa4 |
      fa1 |
      sib,2 r |
      do1 |
      re4. re8 re4. re8 |
      \once\slurDashed mib2.( re8) mib |
      re2. re4 |
      re1~ |
      re |
      re |
      sol |
      fa |
      mib4. mib8 mib4( re8) mib8 |
      re1~ |
      re2. re4 |
      sol,1 |
    }
  }
>>

\clef "dessus" R1*44 |
sib'4. sib'8 do''4 sib' |
la'2 la'4 sib' |
sol' sib' la'4. sol'8 |
fad'2. r8 fad' |
fad'4. fad'8 sol'4. la'8 |
sib'2 sib'4. do''8 |
re''2 la'4 sib' |
la'2 \once\tieDashed sib'~ |
sib'2 la'4. sol'16 la' |
sib'4. do''8 re''2 |
sol'4 sol'' fa''4. sol''8 |
mib''4. re''8 do''4. sib'16 do'' |
re''4 re'' mib''2~ |
mib'' re''4 mi''? |
fa''4 fa'' fa'' mi'' |
fa''2 fa''4 fa'' |
fa''2 mi''4. re''8 |
dod''2. dod''4 |
re'' re'' mi'' re'' |
re''2 dod''4. re''8 |
re''2 do''4. re''8 |
sib'2 do''4. sib'8 |
la'2 la''4 sib'' |
sol''2 sol''4 la'' |
fa''2 fa''4 sol'' |
mib''2 mib''4 fa'' |
re''2. do''8 sib' |
la'4. la'8 la'4. sol'8 |
sol'2 r |
R1*47 |

\clef "dessus" R1*44 | %\allowPageTurn
<>^"[Flûtes]" re''2 mib''~ |
mib'' re''4. re''8 |
re''2 do''4.( sib'16 do'') |
re''2. r8 la' |
la'4. la'8 sib'4. do''8 |
re''2 re''4. mib''?8 |
fa''2 mib''?4 re'' |
do''2 re''4. mib''8 |
do''4. do''8 fa''4. mib''8 |
re''4. do''8 sib'2 |
sib'2. sib'4 |
sib'2 la'4. sol'16 la' |
sib'4 fa'' sol''2 |
fa'' fa''4 sol'' |
la'' la'' sib''4. do'''8 |
la''2 la''4 la'' |
la''2 sol''4. fa''8 |
mi''2. la''4 |
la''4. la''8 sol''4 fa'' |
mi''4. mi''8 la''4. sol''8 |
fad''4. sol''8 la''2 |
re''2 mib''~ |
mib'' re''~ |
re'' do''~ |
do'' sib'~ |
sib' la'~ |
la' sol'~ |
sol' fad'4. sol'8 |
sol'2 r | \allowPageTurn
R1*47 |

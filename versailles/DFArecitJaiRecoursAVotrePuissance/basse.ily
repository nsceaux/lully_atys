\clef "basse" re,1 |
re4. do8 sib,2 |
la, la |
fa4. mi8 re4 re' |
do'2 sib |
la la, |
re sib,~ |
sib, do |
fa,4 fa fad2 |
sol re4 sol8 sol, |
do2 do'4 la |
sib2 fad |
sol mi4 la8 sol |
fa2 dod |
re mi4 mi, |
la,2. la4 |
fad1 |
sol2. sol4 |
do'2 la |
sol4 fa do2 |
fa,2. fa4 |
sib4. sib8 sib4 la |
sol2. sol4 |
re'2 re |
la4 sol la la, |
re2~ \once\tieDashed re~ |\allowPageTurn
re2. |
la,4 la fad |
sol2 mi4 |
fa2 fad |
sol sold |
la sib8 sol la la, |
re1~ |\allowPageTurn
re2 sib, |
do4 do' mi2 |
fa4 re2 |
mi la, |
mi4 do re mi8 mi, |
la,2 re |
la4 sib si |
do'2 sib8 la |
sol1 |
la2 re8 mi fa4 |
mi4. re8 do4 do'8 sib?8 |
la sol fa4 fad2 |
sol8 fa mi4 fa8 sol la la, |
re1~ | \allowPageTurn
re2 sol, |
do fa |
la,4. sib,8 do4 |
fa,1 |\allowPageTurn
fa2 dod |
re la,4 |
la sib8 sol la la, |
re2. do8 si, |
la,2 la4 sol |
fa1 |
mi2 mi, |
%%\custosNote la,4

<<
  %% Sangaride
  \tag #'(sangaride basse) {
    \noHaraKiri
    <<
      \tag #'basse { s1*2 s2 \sangarideMark }
      \tag #'sangaride { \sangarideClef R1*2 | r2 }
    >> r4 r8 la'16 la'
    -\tag #'sangaride ^\markup\right-align\italic\line {
      se jettant aux pieds de Cybele
    } |
    re''8. re''16 re''8 re''16 mi'' fa''4 fa'' |
    mi'' mi''8 fa'' re''8. re''16 re''8. mi''16 |
    dod''4 r r la'16 la' la' la' |
    fad'4 fad'8 sol' sol'4
    << \tag #'basse { sol'8 s8 } \tag #'sangaride sol'4 >>
    << { s1 | s8 } \tag #'sangaride { R1 | r8 } >>
    \tag #'basse { s16 \sangarideMark do''16 } \tag #'sangaride do''8
    do''8 do'' do''8. do''16 do''8[ si'16] do'' |
    << \tag #'basse { si'8 s } \tag #'sangaride si'4 >>
    \tag #'sangaride {
      r4 r2 | \startHaraKiri R1*16 | R2.*3 | R1*6 | R2. | R1 | R1*2 |
      R2.*2 | R1*8 | R2. | R1*2 | R2.*2 | R1*4 |
    }
  }
  %% Cybèle
  \tag #'(cybele basse) {
    <<
      \tag #'basse { s1*15 | s2 \cybeleMark }
      \tag #'cybele { \cybeleClef R1*15 | r2 }
    >> \noHaraKiri r4 la'4 |
    re''1 |
    sib'4. sib'8 sib'4 la' |
    sol'2 do''4. do''8 |
    sib'4. la'8 sol'4. fa'8 |
    fa'2 r4 \footnoteHere#'(0 . 0) \markup\wordwrap {
      La section en duo (mes. 21-26) n’existe pas dans le livret de
      1676.  Dans F-V, initialement : voix supérieure seulement, sur la
      portée de Cybèle (aucune indication de changement de
      personnage). La ligne d’Atys a été ajoutée, avec la mention “à 2 voix”.
    } la'4 |
    fa'1 |
    sib'4. sib'8 sib'4. do''8 |
    la'2 la'4 si'! |
    dod'' re'' dod''4. re''8 |
    re''4 r8 la' fa'4 r16 fa' sol' la' |
    sib'8. sib'16 la'8. la'16 la'8. si'?16 |
    do''8 do'' r la'16 la' re''8 re''16 la' |
    sib'8 sib' sol' sol'16 sol' do''8 do''16 do'' |
    la'8 la' r do'' do''8. la'16 la' la' si' do'' |
    si'4 r8 si' mi''8. mi''16 mi''8. mi''16 |
    dod''4 dod''8 dod'' re''4 re''8 dod'' |
    re''4
    <<
      { s2. | s4 }
      \tag #'cybele { r4 r2 | r4 }
    >>
    \tag #'basse \cybeleMark r8 fa'' re''8 sib'16 sib' sib'8 do''16 re'' |
    sol'8 sol' r16 sol' sol' la' sib'4 sib'8 sib'16 la' |
    la'4 r16 fa'' fa'' mi'' re''8. do''16 |
    si'4 si' do'' re''8 mi'' |
    sold'8 sold' mi''8. fa''16 re''8 re''16 do'' si'8. la'16 |
    la'4 r4 r8 fa'16 sol' la'8 la'16 si' |
    do''8. do''16 re''8. re''16 mi''8. fa''16 |
    mi''8 mi'' r do''16 do'' sol'8 sol'16 la' |
    sib'4 r8 re'' sib' la' sol'8. fa'16 |
    mi'4 r8 la' fa'8. sol'16 la'8. si'!16 |
    do''4 do''8 re'' mi''4 mi''8 do'' |
    fa''4 fa''8 r la' la'16 la' si'8. do''16 |
    si'4 mi''8. fa''16 re''4 re''8 dod'' |
    re''4 re'' r4 r8 la' |
    fad'8 r16 la'16 la'8. la'16 sib'8. sib'16 sib' sib' do'' re'' |
    sol'4 r16 sol' sol' sol' la'8. la'16 la'8 sib' |
    do''8 do'' do''16 do'' do'' re'' sol'8 sol'16 la' |
    fa'4 r
    -\tag #'cybele ^\markup\italic\line { Sangaride se retire }
    r r8 do''16 do'' |
    la'2 la'8.-\tag #'cybele ^\markup\italic { à Atys } la'16 la'8. la'16 |
    fa'8. fa'16 fa'8. mi'16 mi'8 la'16 si' |
    dod''8 dod''16 dod'' re''8 re''16 re'' re''8 re''16 dod'' |
    re''2 re'' |
    R1*3 |
  }
  %% Atys
  \tag #'(atys basse) {
    \tag #'basse \atysMark
    \tag #'atys { \atysClef \noHaraKiri }
    r4 r8 re'16 re' sib8. sib16 sib8 sib16 la |
    la4 r8 la16 la re'8. re'16 re'8 re'16 sol |
    la4 la
    <<
      { s2 | s1*3 | s2 s4 s8 }
      \tag #'atys { r2 | R1*3 | r2 r4 r8 }
    >>
    \tag #'basse \atysMark re'8
    -\tag #'atys ^\markup\italic { interrompant Sangaride } |
    re'8. re'16 sib8 sib sol8. sol16 sol la sib do' |
    la8 << \tag #'basse { la16 s } \tag #'atys la8 >>
    <<
      { s2. | s8 }
      \tag #'atys { r4 r2 | r8 }
    >>
    \tag #'basse \atysMark sol'8
    -\tag #'atys ^\markup\italic { interrompant Sangaride }
    re'16 re' re' mi' fa'8 fa'16 mi' re'8 re'16 mi' |
    do'4 r8 mi' mi' mi'16 mi' fa'8 fa'16 fa' |
    re'8 re' r re' la8 la16 si do'8 do'16 si |
    si4 r8 si do' do'16 do' dod'8 dod'16 dod' |
    re'4 re'8. re'16 mi'4 mi'8. mi'16 |
    fa'4 re'8. re'16 si4 si8. do'16 |
    la4 la4
    <<
      { s2 | s1*10 | s2.*3 | s1*3 | s4 }
      \tag #'atys {
        r2 |
        R1*4 |
        r2 r4 fa'4 |
        re'1 |
        sol'4. sol'8 sol'4. fa'8 |
        fa'2 fa'4 sol' |
        mi' fa' mi'4. re'8 |
        re'4 r r2 |
        \revertNoHaraKiri R2.*3 | R1*3 | r4
      }
    >>
    \tag #'basse \atysMark fa'2 re'8. re'16 |
    la4
    \tag #'atys {
      r4 r2 | R1 | R2. | R1 | R1*2 | R2.*2 |
      R1*8 | R2. | R1*2 | R2.*2 | R1*4 |
    }
  }
>>

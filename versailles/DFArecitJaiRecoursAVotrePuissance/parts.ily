\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix"
                   #:indent 0)
   (basse-viole #:score-template "score-basse-viole-voix"
                #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Atys
    \livretVerse#8 { Je sçay trop ce que je vous doy }
    \livretVerse#8 { Pour manquer de reconnoissance… }
    \livretPers Sangaride
    \livretVerse#8 { J’ay recours à vostre puissance, }
    \livretVerse#8 { Reyne des Dieux, protegez-moy. }
    \livretVerse#8 { L’interest d’Atys vous en presse… }
    \livretPers Atys
    \livretVerse#12 { Je parleray pour vous, que vostre crainte cesse. }
    \livretPers Sangaride
    \livretVerse#8 { Tous deux unis des plus beaux nœuds… }
    \livretPers Atys
    \livretVerse#12 { Le sang et l’amitié nous unissent tous deux. }
    \livretVerse#8 { Que vostre secours la délivre }
    \livretVerse#8 { Des loix d’un Hymen rigoureux, }
    \livretVerse#8 { Ce sont les plus doux de ses vœux }
    \livretVerse#12 { De pouvoir à jamais vous servir et vous suivre. }
    \livretPers Cybele
    \livretVerse#7 { Les Dieux sont les protecteurs }
    \livretVerse#7 { De la liberté des cœurs. }
    \livretVerse#12 { Allez, ne craignez point le Roy ny sa colere, }
    \livretVerse#6 { J’auray soin d’appaiser }
  }
  \column {
    \livretVerse#8 { Le Fleuve Sangar vostre Pere ; }
    \livretVerse#8 { Atys veut vous favoriser, }
    \livretVerse#12 { Cybele en sa faveur ne peut rien refuser. }
    \livretPers Atys
    \livretVerse#12 { Ah ! c’en est trop… }
    \livretPers Cybele
    \livretVerse#12 { \transparent { Ah ! c’en est trop… } Non, non, il n’est pas necessaire }
    \livretVerse#8 { Que vous cachiez vostre bonheur, }
    \livretVerse#6 { Je ne prétens point faire }
    \livretVerse#4 { Un vain mystere }
    \livretVerse#8 { D’un amour qui vous fait honneur. }
    \livretVerse#12 { Ce n’est point à Cybelle à craindre d’en trop dire. }
    \livretVerse#12 { Il est vray, j’ayme Atys, pour luy j’ay tout quitté, }
    \livretVerse#12 { Sans luy je ne veux plus de grandeur ny d’Empire, }
    \livretVerse#6 { Pour ma felicité }
    \livretVerse#6 { Son cœur seul peut suffire. }
    \livretVerse#12 { Allez, Atys luy-mesme ira vous garentir }
    \livretVerse#8 { De la fatale violence }
    \livretVerse#8 { Où vous ne pouvez consentir. }
    \livretPers Cybele
    \livretVerse#12 { Laissez-nous, attendez mes ordres pour partir, }
    \livretVerse#12 { Je prétens vous armer de ma toute-puissance. }
  }
}#}))

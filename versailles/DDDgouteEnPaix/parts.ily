\piecePartSpecs
#`((flutes-hautbois)
   (basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line{
  \column {
    \livretPers Morphée
    \livretVerse#12 { Gouste en paix chaque jour une douceur nouvelle, }
    \livretVerse#12 { Partage l’heureux sort d’une Divinité, }
    \livretVerse#8 { Ne vante plus la liberté, }
    \livretVerse#12 { Il n’en est point du prix d’une chaîne si belle : }
    \livretPers\line { Morphée, Phobetor & Phantase }
    \livretVerse#8 { Mais souvien-toy que la Beauté }
    \livretVerse#6 { Quand elle est immortelle, }
    \livretVerse#8 { Demande la fidelité }
    \livretVerse#6 { D’une amour éternelle. }
  }
  \column {
    \null
    \livretPers Phantase
    \livretVerse#6 { Que l’Amour a d’attraits }
    \livretVerse#4 { Lors qu’il commence }
    \livretVerse#8 { A faire sentir sa puissance, }
    \livretVerse#6 { Que l’Amour a d’attraits }
    \livretVerse#4 { Lors qu’il commence }
    \livretVerse#6 { Pour ne finir jamais. }
  }
}#}))

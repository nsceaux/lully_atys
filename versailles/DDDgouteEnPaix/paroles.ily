\tag #'(phobetor basse) {
  % Phobetor
  Gouste en paix cha -- que jour u -- ne dou -- ceur nou -- vel -- le,
  Par -- ta -- ge l’heu -- reux sort d’u -- ne Di -- vi -- ni -- té,
  Ne van -- te plus la li -- ber -- té,
  Il n’en est point du prix d’u -- ne chaî -- ne si bel -- le.
}
\tag #'(morphee phantase phobetor basse) {
  % Morphée, Phobetor, et Phantase.
  Mais sou -- vien- toy que la Beau -- té,
  Quand elle est im -- mor -- tel -- le,
  De -- man -- de la fi -- de -- li -- té
  D’une a -- mour é -- ter -- nel -- le.
}
\tag #'(phantase basse) {
  % Phantase
  Que l’A -- mour a d’at -- traits
  Lors qu’il com -- men -- ce
  A fai -- re sen -- tir sa puis -- san -- ce,
  Que l’A -- mour a d’at -- traits
  Lors qu’il com -- men -- ce
  Pour ne fi -- nir ja- %- mais.
}


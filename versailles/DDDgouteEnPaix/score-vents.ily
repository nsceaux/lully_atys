\score {
  <<
    \new Staff \with { \tinyStaff } \withTinyLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse \includeLyrics "paroles"
    \new GrandStaff \with {
      instrumentName = "Flûtes"
      \haraKiri
    } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
  >>
  \layout { indent = 20\mm }
}

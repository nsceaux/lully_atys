\score {
  <<
    \new GrandStaff \with { \flInstr \haraKiriFirst } <<
      \new Staff << <>^"[Flûtes]" \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new ChoirStaff \with { \forceGroupBracket \haraKiriFirst } <<
      \new Staff \with { \morpheeInstr } \withLyrics <<
        { s1*6 s1. s1*3 s1. s1*4\break s1*5 s1. s1*3 s2.\break }
        \global \keepWithTag #'morphee \includeNotes "voix"
      >> \keepWithTag #'morphee \includeLyrics "paroles"
      \new Staff \with { \phantaseInstr } \withLyrics <<
        \global \keepWithTag #'phantase \includeNotes "voix"
      >> \keepWithTag #'phantase \includeLyrics "paroles"
      \new Staff \with { \phobetorInstr } \withLyrics <<
        \global \keepWithTag #'phobetor \includeNotes "voix"
      >> \keepWithTag #'phobetor \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \includeNotes "basse"
        { s1*6 s1. s1*3 s1. s1*3 s2. \footnoteHere#'(0 . 0) \markup {
            F-V : \italic {
              On reprend “Mais souviens-toy”, et le second couplet,
              “Trop heureux amant”, et les Songes agreables.
            }
          }
        }
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout { }
  \midi { }
}

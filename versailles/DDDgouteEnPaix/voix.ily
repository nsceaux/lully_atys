<<
  \tag #'(morphee basse) {
    <<
      \tag #'basse { s1*6 s1. s1*3 s1. s1*4 \morpheeMark }
      \tag #'morphee { \morpheeClef R1*6 | R1. | R1*3 | R1. | R1*4 }
    >>
    fa'2 fa'4 re' |
    do'4. do'8 do'4 re' |
    mib'2 re'4. re'8 |
    sol'2 sol'4 sol' |
    fa'2 fa'4. fa'8 |
    fad'4 sol' la'4 la' re'4. mi'8 |
    fad'2 fad'4. fad'8 |
    sol'2 sol'4 sol' |
    sol'2( fad') |
    sol'2. |
    \tag #'morphee R2.*15
  }
  \tag #'(phantase basse) {
    <<
      \tag #'basse { s1*6 s1. s1*3 s1. s1*4 s1*5 s1. s1*3 s2. \phantaseMark }
      \tag #'phantase {
        \phantaseClef R1*6 R1. R1*3 R1. R1*4 |
        re'2 re'4 sib |
        la4. la8 la4 si |
        do'2 sib!4. sib8 |
        sib2 sib4 mib' |
        re'2 re'4. re'8 |
        do'4 do' do'4. re'8 sib4( la8) sib8 |
        la2 re'4. re'8 |
        sib2 sib4 do' |
        sib2( la) |
        sol2. |
      }
    >>
    \footnoteHere#'(0 . 0) \markup\column {
      \line { Livret 1676 et ms. Colmar : reprise du premier couplet “Que l’Amour a d’attraits”. }
      \line { F-V. et Valenciennes : reprise du second couplet “Trop heureux un amant.” }
    }
    r4 sib do' |
    re' la sib |
    fad2. |
    la4 sib4. do'8 |
    sib4 sol mib' |
    do'4. re'8 mib'4 |
    re' do' sib |
    fa'2 re'4 |
    r sib do' |
    re' la sib |
    fad2. |
    re'4 mib'4. fa'8 |
    mib'2 do'4 |
    la la sib |
    sib( la4.) sol8 |
  }
  \tag #'(phobetor basse) {
    \tag #'basse \phobetorMark
    \tag #'phobetor \phobetorClef
    \footnoteHere#'(0 . 0) \markup { F-V et livret 1676 : attribué à Morphée }
    r2 sol4 sol |
    la2 la4. la8 |
    sib2 re4 re8 re |
    mib4 mib fa2 |
    sib,2. sib4 |
    la4. la8 la4 sol |
    fad2 sol4 sol8 la sib4 do' |
    re'1 |
    r4 re' sib sol |
    do'4. do'8 do'4 do |
    fa2 sol4 sol8 sol sol4 sol |
    la2 la4 sol |
    fa2 mi4 re |
    la2( la,) |
    re2 r |
    \tag #'phobetor {
      sib2 sib4 sib |
      fa4. fa8 mib4 re |
      do2 sol4. sol8 |
      mib2 mib4 mib |
      sib2 sib4. sib8 |
      la4. sol8 fad4. fad8 sol4( fad8) sol |
      re2 re4. re8 |
      mib2 mib4 do |
      re1 |
      sol,2. |
      R2.*15 |
    }
  }
>>

\clef "basse" sol,2 sol4. sol8 |
la2 la |
sib2 re |
mib fa |
sib,2. sib4 |
la2. sol4 |
fad2 sol4. la8 sib4 do' |
re'1 |
re4 re' sib sol |
\footnoteHere#'(0 . 0) \markup {
  F-V : Bc, mes. 10-14, la partie n’est pas copiée (mesures vides) ; mais chiffrage sur le chant.
}
do'2 do |
fa sol2. sol4 |
la2 la4 sol |
fa2 mi4 re |
la2 la, |
re2 re'4 do' |
sib2 sib4 sib |
fa4 fa mib4 re |
do r sol4. sol8 |
mib2 mib4 mib |
sib2. sib4 |
la4. sol8 fad2 sol4 sol, |
re2 re4 re |
mib2 mib4 do |
re2 re, |
sol,2. |
sol2 la4 |
sib fad sol |
re2 mi4 |
fad2. |
sol |
la |
sib4 la sol |
fa2 sib4 |
sol2 la4 |
sib fad sol |
re2 do4 |
si,2. |
do |
re2 sol,4 |
re,2. |

\clef "dessus" R2.*19 |
r4 sib''4. la''8 |
sol''4 fa''4. mib''16 re'' |
mib''4 mi''4. mi''8 |
fad''4 fad'' sol'' |
sol'' fad''4. mi''16 fad'' |
sol''2 re''8 mi'' |
fa''4 fa''4. re''8 |
mib''4 mib''4. do''8 |
re''4 sib''4. sib''8 |
sib''4 la''4. sib''8 |
sol''4 sol'' fa''8 mi'' |
fa''4 fa''4. sol''8 |
mi''4 mi'' fad''8 sol'' |
fad''4 sib''4. la''8 |
sol''4 fa''4. mib''16 re'' |
mib''4 mi''4. mi''8 |
fad''4 fad'' sol'' |
sol'' fad''4. mi''16 fad'' |
sol''2. |

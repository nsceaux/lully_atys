\atysClef re'4. re'8 mib' fa' |
mib'2 do'8. do'16 |
do'4( sib) la8 sib |
la4 la re'8. mib'16 |
do'2 sib4 |
la2 la8. sib16 |
sol2 r8 sib |
si4. si8 si si |
do'4. do'8 re'4 |
mib' re'4. do'8 |
do'4 sol'8 fa' mib' re' |
do'2 re'8 mib' |
re'4 sib r8 re' |
re'4. re'8 mib' fa' |
mib'2 do'8. do'16 |
do'4( sib) la8 sib |
la4 la re'8. mib'16 |
do'2 sib4 |
la2 la8. sib16 |
sol2 r4 |
R2.*18 |

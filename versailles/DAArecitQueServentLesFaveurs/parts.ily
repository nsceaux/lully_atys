\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse-continue #:score-template "score-basse-continue-voix"
                   #:indent 0)
   (basse-viole #:score-template "score-basse-viole-voix"
                #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPersDidas Atys seul
    \livretVerse#12 { Que servent les faveurs que nous fait la Fortune }
    \livretVerse#8 { Quand l’Amour nous rend malheureux ? }
    \livretVerse#12 { Je pers l’unique bien qui peut combler mes vœux, }
  }
  \column {
    \null
    \livretVerse#8 { Et tout autre bien m’importune. }
    \livretVerse#12 { Que servent les faveurs que nous fait la Fortune }
    \livretVerse#8 { Quand l’Amour nous rend malheureux ? }
  }
} #}))

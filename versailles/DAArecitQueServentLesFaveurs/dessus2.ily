\clef "dessus" R2.*19 |
r4 re''4. re''8 |
re''2 re''4 |
sol' do''4. do''8 |
do''4 la' sib' |
la' la'4. sol'8 |
sol'2 sib'8 do'' |
la'4 la'4. si'8 |
do''4 do''4. la'8 |
sib'4 re''4. re''8 |
mi''4 fa''4. fa''8 |
fa''4 mi''4. mi''8 |
la'4 re''4. re''8 |
re''4 dod''4. si'16 dod'' |
re''4 re''4. re''8 |
re''2 re''4 |
sol' do''4. do''8 |
do''4 la' sib' |
la' la'4. sol'8 |
sol'2. |

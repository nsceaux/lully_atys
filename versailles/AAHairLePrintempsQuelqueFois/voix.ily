\zephirClef r4 re' sol' |
mi' re'4. do'8 |
si4. si8 do'4 |
re' la4. si8 |
si2 la4 |
mi'4. mi'8 fa'8 mi' |
re'4. mi'8 dod'4 |
re'2. |
re'2 re'4 |
sol'4. sol'8 sol' si |
do'4. do'8 re'4 |
mi' re'4. do'8 |
do'2. |
si4 do' re' |
do'4 la si~ |
si8 do' si4( la) |
sol2 re'4 |
sol2.

\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \column {
\livretPers Un Zephir
\livretVerse#12 { Le printemps quelquefois est moins doux qu’il ne semble, }
\livretVerse#8 { Il fait trop payer ses beaux jours ; }
\livretVerse#12 { Il vient pour escarter les jeux et les amours, }
\livretVerse#8 { Et c’est l’hyver qui les rassemble. }
}#}))


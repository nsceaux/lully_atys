Le prin -- temps quel -- que -- fois est moins doux qu'il ne sem -- ble,
Il fait trop pay -- er ses beaux jours ;
jours ;
Il vient pour es -- car -- ter les jeux et les a -- mours,
Et c'est l'hy -- ver qui les ras -- sem -- ble.
Il - ble.

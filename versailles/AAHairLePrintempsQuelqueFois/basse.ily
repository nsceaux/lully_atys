\clef "basse" sol4 sol,8 la, si,4 |
do re2 |
mi4. re8 do4 |
si, fad,2 |
sol, re4 |
dod2 re4 |
sib sol la |
re4. mi8 fad re |
re2 re'8 do' |
si4. la8 sol fa |
mi2 re4 |
do sol sol, |
do4. re8 mi fad |
sol4 la si |
fad2 sol4 |
do re re, |
sol, sol re'8 do' |
sol,2.

\score {
  <<
    \new ChoirStaff <<
      \new Staff \with { \dchantInstr } \withLyrics <<
        <>^\markup\raise#1 { Divinitez de fontaines et dieux de ruisseaux }
        \global \includeNotes "voix-dessus"
      >> \includeLyrics "paroles"
      \new Staff \with { \hcchantInstr } \withLyrics <<
        <>^\markup\raise#1 { Un dieu de fleuves }
        \global \includeNotes "voix-haute-contre"
      >> \includeLyrics "paroles"
      \new Staff \with { \tchantInstr } \withLyrics <<
        <>^\markup\raise#1 { Un dieu de fleuves }
        \global \includeNotes "voix-taille"
      >> \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \with { \hcchantInstr } \withLyrics <<
        <>^\markup\raise#1 { Grands dieux de fleuves }
        \global \includeNotes "voix-haute-contre"
      >> \includeLyrics "paroles"
      \new Staff \with { \tchantInstr } \withLyrics <<
        \global \includeNotes "voix-taille"
      >> \includeLyrics "paroles"
      \new Staff \with { \btchantInstr } \withLyrics <<
        \global \includeNotes "voix-basse"
      >> \includeLyrics "paroles"
    >>
    \new StaffGroup <<
      \new Staff \with { \dvInstr } << \global \includeNotes "dessus" >>
      \new Staff \with { \hcvInstr } << \global \includeNotes "haute-contre" >>
      \new Staff \with { \tvInstr } << \global \includeNotes "taille" >>
      \new Staff \with { \qvInstr } << \global \includeNotes "quinte" >>
      \new Staff \with { \bvInstr } << \global \keepWithTag #'basse \includeNotes "basse" >>
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \keepWithTag #'basse-continue \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout { }
  \midi { }
}

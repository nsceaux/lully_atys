\piecePartSpecs
#`((dessus #:indent 0)
   (haute-contre #:indent 0)
   (taille #:indent 0)
   (quinte #:indent 0)
   (basse #:instrument ,#{\markup\center-column { BVn et Bc }#}
          #:system-count 3)
   (basse-continue #:instrument ,#{\markup\center-column { BVn et Bc }#}
                   #:system-count 2)
   (basse-tous #:instrument ,#{\markup\center-column { BVn et Bc }#}
               #:system-count 2
               #:score-template "score-basse-continue")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers\wordwrap { Chœur de Dieux de Fleuves, & de Divinitez de Fontaines. }
    \livretVerse#7 { Un grand calme est trop fascheux, }
    \livretVerse#7 { Nous aimons mieux la tourmente. }
    \livretVerse#7 { Que sert un cœur qui s’exempte }
  }
  \column {
    \livretVerse#7 { De tous les soins amoureux ? }
    \livretVerse#7 { A quoy sert une eau dormante ? }
    \livretVerse#7 { Un grand calme est trop fascheux, }
    \livretVerse#7 { Nous aimons mieux la tourmente. }
  }
} #}))

\clef "taille"
\tag #'tous <>^\markup\whiteout [Violons]
<<
  \tag #'(violon tous hautbois-tous) {
    re'4 re'8 do' sib4 |
    la2 re'4 |
    re'4 do'4. sib16 do' |
    re'2 re'4 |
  }
  \tag #'hautbois R2.*4
>>
\tag #'tous <>^\markup\whiteout "Hautbois"
<<
  \tag #'(hautbois tous hautbois-tous) {
    re'4 re'8 do' sib4 |
    la re'2 |
    re'4 do'4. sib16 do' |
    re'2 re'4 |
  }
  \tag #'violon R2.*4
>>
\tag #'tous <>^\markup\whiteout "[Violons]"
<<
  \tag #'(violon tous hautbois-tous) {
    re'4 re'8 do' sib4 |
    sib mib'2 |
    re'4 re'2 |
    re'4. do'8 sib4 |
  }
  \tag #'hautbois R2.*4
>>
\tag #'tous <>^\markup\whiteout "[Hautbois]"
<<
  \tag #'(hautbois tous hautbois-tous) {
    re'4 re'8 do' sib4 |
    sib mib'2 |
    re'4 re'2 |
    re'4. do'8 sib4 |
  }
  \tag #'violon R2.*4
>>
\tag #'tous <>^\markup\whiteout [Violons]
<<
  \tag #'(violon tous hautbois-tous) {
    re'4 re' sol' |
    fa'2 sol'4 | % fa'4 sol'2 |
    sol'4 fa'2 |
    fa'4. mib'8 re'4 |
  }
  \tag #'hautbois R2.*4
>>
\tag #'tous <>^\markup\whiteout "[Hautbois]"
<<
  \tag #'(hautbois tous hautbois-tous) {
    re'4 re' re' |
    do' fa'2 |
    fa'4 mi'4. re'16 mi' |
    fa'2 fa'4 |
  }
  \tag #'violon R2.*4
>>
\tag #'tous <>^\markup\whiteout "[Violons]"
<<
  \tag #'(violon tous hautbois-tous) {
    fa'4 re' mi' |
    mi'2 re'4 |
    sib' sib' la' |
    la'2 la'4 |
  }
  \tag #'hautbois R2.*4
>>
\tag #'tous <>^\markup\whiteout "[Hautbois]"
<<
  \tag #'(hautbois tous hautbois-tous) {
    la'4 re' re' |
    re'4. do'8 si?4 |
  }
  \tag #'violon R2.*2
>>
\tag #'tous <>^\markup\whiteout "[Violons]"
<<
  \tag #'(violon tous hautbois-tous) {
    re'4 re' sol' |
    sol'4. fa'8 mib'4 |
    mib'4 fa' re' |
    mib'2 mib'4 |
  }
  \tag #'hautbois R2.*4
>>
\tag #'tous <>^\markup\whiteout "[Hautbois]"
<<
  \tag #'(hautbois tous hautbois-tous) {
    mib'?8 fa' sol' fa' mib' re' |
    do'4. sib8 la4 |
  }
  \tag #'violon R2.*2
>>
\tag #'tous <>^\markup\whiteout "[Violons]"
<<
  \tag #'(violon tous hautbois-tous) {
    re'8 mib'? fa'4 fa' |
    mib' sol' fa' |
    fa' fa'4. mib'8 |
    re'2 re'4 |
  }
  \tag #'hautbois R2.*4
>>
\tag #'tous <>^\markup\whiteout "[Hautbois]"
<<
  \tag #'(hautbois tous hautbois-tous) {
    fa'4 fa' fa' |
    fa'8 mi' re'2 |
    re'4 do' do' |
    la2 la'4 |
  }
  \tag #'violon R2.*4
>>
\tag #'tous <>^\markup\whiteout "[Violons]"
<<
  \tag #'(violon tous hautbois-tous) {
    sol'4 sol' sol' |
    sol'2 mi'4 |
    mi'2 mi'4 |
    re'8 mi' fad'4 sol' |
    mib'4 mib' re' |
    re'2 re'4 |
  }
  \tag #'hautbois R2.*6
>>
\tag #'tous <>^\markup\whiteout "[Hautbois]"
<<
  \tag #'(hautbois tous hautbois-tous) {
    re'4 re' re' |
    re'2 re'4 |
    sol8 la sib4 do' |
    la2 la'4 |
  }
  \tag #'violon R2.*4
>>
\tag #'tous <>^\markup\whiteout "[Tous]"
<<
  \tag #'(violon tous hautbois hautbois-tous) {
    sol'4 sol' sol' |
    sol'2 mi'4 |
    mi'2 mi'4 |
    re'8 mi' fad'4 sol' |
    mib' mib' re' |
    re'2 re'4 |
  }
  %\tag #'hautbois R2.*6
>>

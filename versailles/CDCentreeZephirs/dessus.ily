\clef "dessus" \tag #'tous <>^\markup\whiteout [Violons]
<<
  \tag #'(violon tous hautbois-tous) {
    sib'4 sib'8 la' sol'4 |
    re''4. re''8 mi'' fa'' |
    sol''4 sol''4. la''8 |
    fad''4. mi''8 re''4 |
  }
  \tag #'hautbois R2.*4
>>
\tag #'tous <>^\markup\whiteout Hautbois
<<
  \tag #'(hautbois tous hautbois-tous) {
    sib'4 sib'8 la' sol'4 |
    re''4. re''8 mi'' fa'' |
    sol''4 sol''4. la''8 |
    fad''4. mi''8 re''4 |
  }
  \tag #'violon R2.*4
>>
\tag #'tous <>^\markup\whiteout Violons
<<
  \tag #'(violon tous hautbois-tous) {
    sib'4 sib'8 do'' re''4 |
    sol' do''2 |
    la'4 re''2 |
    sib'4. la'8 sol'4 |
  }
  \tag #'hautbois R2.*4
>>
\tag #'tous <>^\markup\whiteout Hautbois
<<
  \tag #'(hautbois tous hautbois-tous) {
    sib'4 sib'8 do'' re''4 |
    sol' do''2 |
    la'4 re''2 |
    sib'4. la'8 sol'4 |
  }
  \tag #'violon R2.*4
>>
\tag #'tous <>^\markup\whiteout Violons
<<
  \tag #'(violon tous hautbois-tous) {
    sib'8 la' sib' do'' re'' mib'' |
    fa''4. mib''8 re''4 |
    mib''4 do''4. sib'16 do'' |
    re''4. do''8 sib'4 |
  }
  \tag #'hautbois R2.*4
>>
\tag #'tous <>^\markup\whiteout Hautbois
<<
  \tag #'(hautbois tous hautbois-tous) {
    sib'8 la' sib' do'' re'' mi'' |
    fa''4. sol''8 la''4 |
    sib''4 sol''4. fa''16 sol'' |
    la''4. sol''8 fa''4 |
  }
  \tag #'violon R2.*4
>>
\tag #'tous <>^\markup\whiteout [Violons]
<<
  \tag #'(violon tous hautbois-tous) {
    la''4 sol''8 fa'' mi'' re'' |
    dod''4. si'8 la'4 |
    re''4 mi'' dod'' |
    re''2 re''4 |
  }
  \tag #'hautbois R2.*4
>>
\tag #'tous <>^\markup\whiteout [Hautbois]
<<
  \tag #'(hautbois tous hautbois-tous) {
    re''8 do'' re'' mib'' re'' do'' |
    si'?4. la'8 sol'4 |
  }
  \tag #'violon R2.*2
>>
\tag #'tous <>^\markup\whiteout [Violons]
<<
  \tag #'(violon tous hautbois-tous) {
    sol''4 sol'' re'' |
    mib''4. fa''8 sol''4 |
    do''4 re'' si' |
    do''2 do''4 |
  }
  \tag #'hautbois R2.*4
>>
\tag #'tous <>^\markup\whiteout Hautbois
<<
  \tag #'(hautbois tous hautbois-tous) {
    do''8 re'' mib''? re'' do'' sib' |
    la'4. sol'8 fa'4 |
  }
  \tag #'violon R2.*2
>>
\tag #'tous <>^\markup\whiteout [Violons]
<<
  \tag #'(violon tous hautbois-tous) {
    fa''4 fa''8 mib'' re''4 |
    sol''8 fa'' sol'' la'' sib''4 |
    re''4 do''4. sib'8 |
    sib'2 sib'4 |
  }
  \tag #'hautbois R2.*4
>>
\tag #'tous <>^\markup\whiteout [Hautbois]
<<
  \tag #'(hautbois tous hautbois-tous) {
    re''8 do'' re'' mib''? re'' mib'' |
    do''4 do'' re'' |
    sib'4 do''4. sib'16 do'' |
    re''2 re''4 |
  }
  \tag #'violon R2.*4
>>
\tag #'tous <>^\markup\whiteout [Violons]
<<
  \tag #'(violon tous hautbois-tous) {
    sib''4 sib''8 la'' sol'' fa'' |
    mi''2. |
    la''8 sol'' la'' sib'' la'' sol'' |
    fad''4. mi''8 re''4 |
    sol''4 la'' fad'' |
    sol''2 sol''4 |
  }
  \tag #'hautbois R2.*6
>>
\tag #'tous <>^\markup\whiteout [Hautbois]
<<
  \tag #'(hautbois tous hautbois-tous) {
    sib'8 la' sib' do'' sib' do'' |
    la'4 la' re'' |
    re''4. mib''?8 do''4 |
    re''2 re''4 |
  }
  \tag #'violon R2.*4
>>
\tag #'tous <>^\markup\whiteout [Tous]
<<
  \tag #'(violon tous hautbois hautbois-tous) {
    sib''4 sib''8 la'' sol'' fa'' |
    mi''2. |
    la''8 sol'' la'' sib'' la'' sol'' |
    fad''4. mi''8 re''4 |
    sol''4 la'' fad'' |
    sol''2 sol''4 |
  }
  %\tag #'hautbois R2.*6
>>

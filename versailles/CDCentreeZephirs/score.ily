\score {
  <<
    \new StaffGroup <<
      \new Staff \with {
        instrumentName = \markup\center-column { Dessus "de hautbois" }
        shortInstrumentName = "DHb"
      } << \global \keepWithTag #'hautbois-tous \includeNotes "dessus" >>
      \new Staff \with {
        instrumentName = \markup\center-column { "Haute-contre" "de hautbois"  }
        shortInstrumentName = "HcHb"
      } << \global \keepWithTag #'hautbois-tous \includeNotes "haute-contre" >>
      \new Staff \with {
        instrumentName = \markup\center-column { Taille "de hautbois" }
        shortInstrumentName = "THb"
      } << \global \keepWithTag #'hautbois-tous \includeNotes "taille" >>
      \new Staff \with {
        instrumentName = \markup\center-column { Basse "de cromorne" }
        shortInstrumentName = "BCr"
      } << \global \keepWithTag #'hautbois-tous \includeNotes "basse" >>
    >>
    \new StaffGroup <<
      \new Staff \with { \dvInstr } << \global \keepWithTag #'violon \includeNotes "dessus" >>
      \new Staff \with { \hcvInstr } << \global \keepWithTag #'violon \includeNotes "haute-contre" >>
      \new Staff \with { \tvInstr } << \global \keepWithTag #'violon \includeNotes "taille" >>
      \new Staff \with { \qvInstr } << \global \keepWithTag #'violon \includeNotes "quinte" >>
      \new Staff \with { \bvInstr } << \global \keepWithTag #'violon \includeNotes "basse" >>
    >>
  >>
  \layout { indent = 25\mm }
  \midi { }
}

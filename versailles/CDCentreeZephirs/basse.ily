\clef "basse" \tag #'tous <>^\markup\whiteout [Violons]
<<
  \tag #'(violon tous hautbois-tous) {
    sol4 sol sol |
    fad fa2 |
    mi4 mib2 |
    re2 re4 |
  }
  \tag #'hautbois R2.*4
>>
\tag #'tous <>^\markup\whiteout Hautbois
<<
  \tag #'(hautbois tous hautbois-tous) {
    sol4 sol sol |
    fad fa2 |
    mi4 mib2 |
    re2 re4 |
  }
  \tag #'violon R2.*4
>>
\tag #'tous <>^\markup\whiteout [Violons]
<<
  \tag #'(violon tous hautbois-tous) {
    sol4 sol re |
    mib do2 |
    re4 re,2 |
    sol, sol,4 |
  }
  \tag #'hautbois R2.*4
>>
\tag #'tous <>^\markup\whiteout [Hautbois]
<<
  \tag #'(hautbois tous hautbois-tous) {
    sol4 sol re |
    mib do2 |
    re4 re,2 |
    sol, sol,4 |
  }
  \tag #'violon R2.*4
>>
\tag #'tous <>^\markup\whiteout [Violons]
<<
  \tag #'(violon tous hautbois-tous) {
    sol4 sol sol |
    re2 sol4 |
    do fa2 |
    sib, sib,4 |
  }
  \tag #'hautbois R2.*4
>>
\tag #'tous <>^\markup\whiteout [Hautbois]
<<
  \tag #'(hautbois tous hautbois-tous) {
    sib4 sib sib |
    la4. sol8 fa4 |
    sib, do2 |
    fa, fa,4 |
  }
  \tag #'violon R2.*4
>>
\tag #'tous <>^\markup\whiteout [Violons]
<<
  \tag #'(violon tous hautbois-tous) {
    fa4 sol2 |
    la2 fa4 |
    sib4 sol la |
    re2 re4 |
  }
  \tag #'hautbois R2.*4
>>
\tag #'tous <>^\markup\whiteout [Hautbois]
<<
  \tag #'(hautbois tous hautbois-tous) {
    re4 re re |
    sol2 sol4 |
  }
  \tag #'violon R2.*2
>>
\tag #'tous <>^\markup\whiteout [Violons]
<<
  \tag #'(violon tous hautbois-tous) {
    sol,4 sol, sol, |
    do4. re8 mib4 |
    lab fa sol |
    do2 do,4 |
  }
  \tag #'hautbois R2.*4
>>
\tag #'tous <>^\markup\whiteout [Hautbois]
<<
  \tag #'(hautbois tous hautbois-tous) {
    do4 do do |
    fa2 fa4 |
  }
  \tag #'violon R2.*2
>>
\tag #'tous <>^\markup\whiteout [Violons]
<<
  \tag #'(violon tous hautbois-tous) {
    re4 re re |
    mib2 re4 |
    sib, fa fa, |
    sib,2 sib,4 |
  }
  \tag #'hautbois R2.*4
>>
\tag #'tous <>^\markup\whiteout [Hautbois]
<<
  \tag #'(hautbois tous hautbois-tous) {
    sib4 sib sib, |
    fa fad2 |
    sol8 fa mib2 |
    re re,4 |
  }
  \tag #'violon R2.*4
>>
\tag #'tous <>^\markup\whiteout [Violons]
<<
  \tag #'(violon tous hautbois-tous) {
    sol,4 sol, sol, |
    do do8 re do sib, |
    la,2 la,4 |
    re4. do8 sib,4 |
    mib do re |
    sol,2 sol,4 |
  }
  \tag #'hautbois R2.*6
>>
\tag #'tous <>^\markup\whiteout [Hautbois]
<<
  \tag #'(hautbois tous hautbois-tous) {
    sol4 sol sol |
    fad fa2 |
    mi4 mib2 |
    re2 re,4 |
  }
  \tag #'violon R2.*4
>>
\tag #'tous <>^\markup\whiteout [Tous]
<<
  \tag #'(violon tous hautbois hautbois-tous) {
    sol,4 sol, sol, |
    do do8 re do sib, |
    la,2 la,4 |
    re4. do8 sib,4 |
    mib do re |
    sol,2 sol,4 |
  }
  %\tag #'hautbois R2.*6
>>

\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Dessus" } <<
      \global \keepWithTag #'dessus \includeNotes "dessus"
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { Hautes- contre }
    } << \global \includeNotes "haute-contre" \clef "treble" >>
    \new Staff \with { instrumentName = "Tailles" } <<
      \global \includeNotes "taille" \clef "treble"
    >>
  >>
  \layout { indent = \largeindent }
}

\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { Dessus de Hautbois }
    } <<
      { <>^"[Tous]" s2.*4 <>^"[Seuls]" s2.*4
        <>^"[Tous]" s2.*4 <>^"[Seuls]" s2.*4
        <>^"[Tous]" s2.*4 <>^"[Seuls]" s2.*4
        <>^"[Tous]" s2.*4 <>^"[Seuls]" s2.*2
        <>^"[Tous]" s2.*4 <>^"[Seuls]" s2.*2
        <>^"[Tous]" s2.*4 <>^"[Seuls]" s2.*4
        <>^"[Tous]" s2.*6 <>^"[Seuls]" s2.*4
        <>^"[Tous]" }
      \global \keepWithTag #'hautbois-tous \includeNotes "dessus"
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { Haute-contre de Hautbois }
    } << \global \keepWithTag #'hautbois-tous \includeNotes "haute-contre" \clef "soprano" >>
    \new Staff \with {
      instrumentName = \markup\center-column { Taille de Hautbois }
    } << \global \keepWithTag #'hautbois-tous \includeNotes "taille" \clef "mezzosoprano" >>
    \new Staff \with {
      instrumentName = \markup\center-column { Basse de Cromorne }
    } << \global \keepWithTag #'hautbois-tous \includeNotes "basse" >>
  >>
  \layout { indent = 30\mm }
}

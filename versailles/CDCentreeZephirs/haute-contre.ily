\clef "haute-contre"
\tag #'tous <>^\markup\whiteout [Violons]
<<
  \tag #'(violon tous hautbois-tous) {
    sol'2 sol'4 |
    la'4 re''2 |
    sol'2 do''4 |
    la'4. sol'8 fad'4 |
  }
  \tag #'hautbois R2.*4
>>
\tag #'tous <>^\markup\whiteout "Hautbois"
<<
  \tag #'(hautbois tous hautbois-tous) {
    sol'2 sol'4 |
    la'2 re''4 |
    sol'4 do''2 |
    la'4. sol'8 fad'4 |
  }
  \tag #'violon R2.*4
>>
\tag #'tous <>^\markup\whiteout "Violons"
<<
  \tag #'(violon tous hautbois-tous) {
    sol'4 sol' fad' |
    sol' sol'2 |
    sol'4 fad'4. mi'16 fad' |
    sol'2 sol'4 |
  }
  \tag #'hautbois R2.*4
>>
\tag #'tous <>^\markup\whiteout "Hautbois"
<<
  \tag #'(hautbois tous hautbois-tous) {
    sol'4 sol' fad' |
    sol' sol'2 |
    sol'4 fad'?4. mi'16 fad' |
    sol'2 sol'4 |
  }
  \tag #'violon R2.*4
>>
\tag #'tous <>^\markup\whiteout "Violons"
<<
  \tag #'(violon tous hautbois-tous) {
    sol'4 sol'8 la' sib' do'' |
    re''4. do''8 sib'4 |
    do''4 la'4. sol'16 la' |
    sib'2 sib'4 |
  }
  \tag #'hautbois R2.*4
>>
\tag #'tous <>^\markup\whiteout "[Hautbois]"
<<
  \tag #'(hautbois tous hautbois-tous) {
    fa'4. mi'8 fa' sol' |
    la'4. sib'8 do''4 |
    re'' do''2 |
    do''4. sib'8 la'4 |
  }
  \tag #'violon R2.*4
>>
\tag #'tous <>^\markup\whiteout "[Violons]"
<<
  \tag #'(violon tous hautbois-tous) {
    do''4 sib'8 la' sol' fa' |
    mi'4 la'2 |
    fa'4 sol' mi' |
    fad'2 fad'4 |
  }
  \tag #'hautbois R2.*4
>>
\tag #'tous <>^\markup\whiteout "[Hautbois]"
<<
  \tag #'(hautbois tous hautbois-tous) {
    fad'4 fad' fad' |
    sol'2 re'4 |
  }
  \tag #'violon R2.*2
>>
\tag #'tous <>^\markup\whiteout "[Violons]"
<<
  \tag #'(violon tous hautbois-tous) {
    si'4 si' si' |
    do''2 do''8 sib' |
    lab'4 lab' sol' |
    sol'2 sol'4 |
  }
  \tag #'hautbois R2.*4
>>
\tag #'tous <>^\markup\whiteout "[Hautbois]"
<<
  \tag #'(hautbois tous hautbois-tous) {
    sol'4 sol' sol' |
    fa'2 do'4 |
  }
  \tag #'violon R2.*2
>>
\tag #'tous <>^\markup\whiteout "[Violons]"
<<
  \tag #'(violon tous hautbois-tous) {
    la'4 la' sib' |
    sib'2 sib'4 |
    sib'4 la'4. sib'8 |
    sib'2 sib'4 |
  }
  \tag #'hautbois R2.*4
>>
\tag #'tous <>^\markup\whiteout "[Hautbois]"
<<
  \tag #'(hautbois tous hautbois-tous) {
    sib'8 la' sib' do'' sib' do'' |
    la'4 la' sib' |
    sol'8 la' la'4. sol'8 |
    fad'2. |
  }
  \tag #'violon R2.*4
>>
\tag #'tous <>^\markup\whiteout "[Violons]"
<<
  \tag #'(violon tous hautbois-tous) {
    re''4 re'' si' |
    do''2. |
    do''8 sib' do'' re'' do'' sib' |
    la'2 sib'4 |
    sib' do'' la' |
    si'2 si'4 |
  }
  \tag #'hautbois R2.*6
>>
\tag #'tous <>^\markup\whiteout "[Hautbois]"
<<
  \tag #'(hautbois tous hautbois-tous) {
    sol'4 sol' sol' |
    la'2 la'4 |
    sol' sol'2 |
    fad'2. |
  }
  \tag #'violon R2.*4
>>
\tag #'tous <>^\markup\whiteout "[Tous]"
<<
  \tag #'(violon tous hautbois hautbois-tous) {
    re''4 re'' si' |
    do''2. |
    do''8 sib' do'' re'' do'' sib' |
    la'2 sib'4 |
    sib' do'' la' |
    si'2 si'4 |
  }
  %\tag #'hautbois R2.*6
>>

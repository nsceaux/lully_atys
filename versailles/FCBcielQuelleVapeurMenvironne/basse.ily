\clef "basse" fa,2 fa4 mi |
fa1 |
re |
do~ |
do |
sol2 fa4 |
mi2 re4 |
do1~ |
do | \allowPageTurn
la,2. |
sib,1~ |
sib,2 si,4 |
do1 |
fa4 fad sol |
mi2 fa4 la, |
sib,2 do |
do'4 la sib fad |
sol2. |
re2 si,4 |
do sol sol, |
do2 mi, |
fa,1 |
sib,2 si,4 |
do do' la sib8 sol |
la fa sib sol la la, |
re2. fad,4 |
sol,1 |
sol2 mi4 fa |
sib,1 |
sib4. la8 sol2 |
do'4. la8 re' sib do' do |
fa4. fa8 sib4. sib8 |
sol4. sol8 re'4 re |
sol4. sol8 do'4 do |
fa1 |
sib,2 fa |
do' do |
fa2. mib8 re |
do2. do'4 |
si2 r4 do' |
lab1 |
sol2. fa4 |
mib1 |
fa2 sol |
do1 |\allowPageTurn
do1 |
fa, |
sol,2 sol |
re2. |
la2 mi |
fa4. mi8 re2 |
sol1 |
mi2. |
fa2 re4 |
sol mi la8 fa sol sol, |
do1 |

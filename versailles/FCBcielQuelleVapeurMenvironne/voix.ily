<<
  %% Chœur (dessus)
  \tag #'(vdessus basse) {
    <<
      \tag #'basse { s1*5 s2.*2 s1*2 s2. s1 s2. s1 s2. s1*3 s2.*3 s1*2 s2. s1 s2. s1*6 s4 \choeurMark }
      \tag #'vdessus {
        \choeurClef R1*5 R2.*2 R1*2 R2. R1 R2. R1 R2. R1*3 R2.*3 R1*2 R2. R1 R2. R1*6 \break
        <>^\markup\column {
          \line { Troupe de prestresses de Cybele }
          \line { Chœur des Phrygiennes et des Phrygiens }
        } r4
      }
    >> r8 fa'' re'' re'' r re'' |
    sib' sib' sib'8. do''16 la'4 r8 fa'' |
    re''4 re''8 re'' mi'' mi'' mi''8. mi''16 |
    fa''2
    << { s2. } \tag #'vdessus { r2 | r4 } >>
    \tag #'basse \choeurMark r8 re'' do''4 r8 fa'' |
    mi''2
    << { s1. } \tag #'vdessus { r2 | R1 | } >>
    \tag #'basse \choeurMark r2 r4 mib'' |
    re''2 r4 mib'' |
    do''2. do''4 |
    si'2 si' |
    r do''4. do''8 |
    do''2 si'4. si'8 |
    do''2 do'' |\break
    \tag #'vdessus { R1*3 R2. R1*3 R2.*2 R1*2 }
  }
  %% Sangaride
  \tag #'(sangaride basse) {
    <<
      \tag #'basse { s1*5 s2.*2 s1*2 s2. s1 s2. s1 s2. s1*3 s2.*3 s8 \sangarideMark }
      \tag #'sangaride { \sangarideClef R1*5 R2.*2 R1*2 R2. R1 R2. R1 R2. R1*3 R2.*3 r8 }
    >> sol''8 do''16 do'' do'' do'' sol' sol' sol' sol' la'8. sib'16 |
    la'4
    << { s2. s2. s1 s2. s4. }
      \tag #'sangaride { r4 r2 | R2. | R1 | R2. | r4 r8 } >>
    \tag #'basse \sangarideMark
    sol''8 la'4 re''8 re''16 re'' |
    si'4 r4
    << { s2 s1*7 s2 } \tag #'sangaride { r2 | R1*7 | r2 } >>
    \tag #'basse \sangarideMark r4
    -\tag #'sangaride ^\markup\italic { Dans un des costez du theatre. } fa'' |\modVersion\noBreak
    re''
    << { s2. s2 } \tag #'sangaride { r4 r2 | r2 } >>
    \tag #'basse \sangarideMark r4 r8
    do'' |
    la'2 r
    \tag #'sangaride { R1*7 R1*3 R2. R1*3 R2.*2 R1*2 }
  }
  %% Cybèle
  \tag #'(cybele basse) {
    <<
      \tag #'basse {
        s1*4 s1 s2.*2 s1 s1 s2. s1 s2. s1 s2. s1*3 s2.*3 s1*2 s2.
        s1 s2. s1*3 s1 s1*9 s1*7 s1*2 s8 \cybeleMark
      }
      \tag #'cybele {
        \cybeleClef R1*4 R1 R2.*2 R1 R1 R2. R1 R2. R1 R2. R1*3
        R2.*3 R1*2 R2. R1 R2. R1*3 R1 R1*9 R1*7 R1*2 | r8
      }
    >> re''8 si'16 si' si' si' sol'8 sol'16 sol' re'8 re'16 mi' |
    fa'8 fa' r re''16 re'' la'8 la'16 si' |
    do''4 do''8 do'' do''4 re''8 mi'' |
    la'8 la'16 do'' do'' do'' re'' mi'' fa''8 fa''16 fa'' re''8 re''16 re'' |
    si'4 r r8 sol' sol'16 sol' la' si' |
    do''4 \tag #'cybele { r2 | R2. | R1*2 }
  }
  %% Atys
  \tag #'(atys basse) {
    \tag #'basse \atysMark
    \tag #'atys \atysClef
    fa'4 r la8 la16 sib do'8 do'16 sol |
    la4 la r8 do'16 do' do'8 re'16 mi' |
    fa'4 r8 re'16 re' si4 r8 si16 re' |
    sol8 sol r sol' mi'8. mi'16 mi'8. mi'16 |
    do'2 do'8 do'16 do' sol8. la16 |
    sib4 re'8 re'16 re' mi'8 fa' |
    sol'4 mi'8 fa'16 sol' si8. do'16 |
    do'4 r sol' r8 mi' |
    do'4 do' r do' |
    fa' la8. la16 la8 sib16 do' |
    fa8 fa r fa'16 fa' re'8 re' r fa' |
    sib4 r8 re'16 re' sol'8 sol'16 sol' |
    mi'8[ fa' mi' re' do' re' do' sib]( |
    la8) la r re'16 re' si8 si16 re' |
    sol8 sol16 la sib8 sib16 la la do' do' do' fa'8. fa'16 |
    re'8 re'16 fa' sib8 sib16 re' sol4 r8
    -\tag #'atys ^\markup\italic {
      Il parle à Cybele, qu’il prend pour Sangaride.
    } sol'16 sol' | \modVersion\noBreak
    mi'8 mi' fa'8. fa'16 re'8. re'16 re' do' sib la |
    sib8 sib sib sib16 sib sib do' re' mi' |
    fa'8 fa' r16 re' re' re' sol'8. re'16 |
    mib'8. mib'16 si8. si16 si8. do'16 |
    << \tag #'atys do'4 \tag #'basse { do'8 s } >>
    << { s2. s4 } \tag #'atys { r4 r2 | r4 } >>
    \tag #'basse \atysMark r8 do'-\tag #'atys ^\markup\italic { Prenant Sangaride pour un monstre. }
    fa'8. fa'16 fa'8. fa'16 | \modVersion\noBreak
    re'4 re'8 re'16 re' sol'8. sol'16 |
    mi'8 mi' r4 fa'8. fa'16 re'8 re'16 mi' |
    dod'8 fa' re' re' re' dod' |
    re'4 re'8
    << { s8 s2 s2 } \tag #'atys { r8 r2 | r2 } >>
    \tag #'basse \atysMark sol'8
    sol'16 sol' si8. re'16 |
    sol4
    << { s2. s1 } \tag #'atys { r4 r2 | R1 } >>
    \tag #'basse \atysMark r8 fa'-\tag #'atys ^\markup\italic { Tenant à la main le cousteau sacré qui sert aux sacrifices. }
    re' fa' sib sib r sol' |\modVersion\noBreak
    mi'4  r8 la' fa' fa' fa' mi' |
    fa' fa'
    \tag #'atys {
      <>^\markup\italic {
        Atys court aprés Sangaride qui fuït dans un des costez du theatre.
        \hspace#3
        Celænus court aprés Atys.
      } r4 r2 | \modVersion\noBreak
      R1 | \modVersion\noBreak
      R1 R1*11 R1*3 R2. R1*3 R2.*2 R1*2
    }
  }
  %% Celænus
  \tag #'(celaenus basse) {
    <<
      \tag #'basse { s1*5 s2.*2 s1*2 s2. s1 s2. s1 s2. s1*3 s2.*3 s1*2 s2. s1 s2. s1*2 s4 \celaenusMark }
      \tag #'celaenus {
        \celaenusClef R1*5 R2.*2 R1*2 R2. R1 R2. R1 R2. R1*3 R2.*3
        R1*2 R2. R1 R2. R1*2 r4
      }
    >> r8 si do' do'16 do' la8 si!16 do' |
    re'4 re' r2 |
    << { s1*16 } \tag #'celaenus { R1*16 } >>
    \tag #'basse \celaenusMark r8-\tag #'celaenus ^\markup\italic { Revenant sur le Theatre. } do16 do
    sol8 sol16 sol do'8 do'16 mi' do'8 do'16 do' |
    la4 r8 do'16 do' la8. la16 fa8 fa16 fa |
    << \tag #'celaenus re4 \tag #'basse { re8 s } >>
    << { s2. s2. s1*3 s4 } \tag #'celaenus { r4 r2 | R2. | R1*3 | r4 } >>
    \tag #'basse \celaenusMark mi'8. do'16 sol8 sol16 do' |
    la8 la r la16 la re'8 re'16 re' |
    si4 mi'8 mi' do'4 do'8 si |
    do'1*7/8 s8-\tag #'celaenus ^\markup\right-align\italic\right-column {
      \line { Celænus se retire au costé du theatre, }
      \line { où est Sangaride morte. }
    }
  }
  %% Chœur
  \tag #'vhaute-contre {
    \clef "vhaute-contre2" R1*5 R2.*2 R1*2 R2. R1 R2. R1 R2. R1*3
    R2.*3 R1*2 R2. R1 R2. R1*6 |
    r4 r8 la' fa' fa' r fa' |
    sol' sol' sol'8. sol'16 fa'4 r8 la' |
    sol'4 sol'8 sol' sol' sol' sol'8. sol'16 |
    la'2 r |
    r4 r8 fa' fa'4 r8 la' |
    sol'2 r |
    R1 |
    r2 r4 sol' |
    sol'2 r4 sol' |
    sol'2. fa'4 |
    sol'2 sol' |
    r sol'4. sol'8 |
    re'2 re'4. re'8 |
    mib'2 mib' |
    R1*3 R2. R1*3 R2.*2 R1*2 |
  }
  \tag #'vtaille {
    \clef "vtaille" R1*5 R2.*2 R1*2 R2. R1 R2. R1 R2. R1*3
    R2.*3 R1*2 R2. R1 R2. R1*6 |
    r4 r8 do' sib sib r re' |
    re' re' re'8. re'16 re'4 r8 re' |
    si4 si8 si do'8. do'16 do'8. do'16 |
    do'2 r |
    r4 r8 sib la4 r8 do' |
    do'2 r |
    R1 |
    r2 r4 do' |
    re'2 r4 do' |
    do'2. fa'4 |
    re'2 re' |
    r do'4. sol8 |
    lab2 sol4. sol8 |
    sol2 sol |
    R1*3 R2. R1*3 R2.*2 R1*2 |
  }
  \tag #'vbasse {
    \clef "vbasse" R1*5 R2.*2 R1*2 R2. R1 R2. R1 R2. R1*3
    R2.*3 R1*2 R2. R1 R2. R1*6 |
    r4 r8 fa sib sib r sib |
    sol sol sol8. sol16 re'4 r8 re |
    sol4 sol8 sol do'8. do'16 do'8. do'16 |
    fa2 r |
    r4 r8 sib, fa4 r8 fa |
    do'2 r |
    R1 |
    r2 r4 do' |
    si2 r4 do' |
    lab2. lab4 |
    sol2 sol |
    r2 mib4. mib8 |
    fa2 sol4. sol8 |
    do2 do |
    R1*3 R2. R1*3 R2.*2 R1*2 |
  }
>>

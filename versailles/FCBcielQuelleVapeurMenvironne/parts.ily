\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Atys
    \livretVerse#8 { Ciel ! quelle vapeur m’environne ! }
    \livretVerse#12 { Tous me[s] sens sont troublez, je fremis, je frissonne, }
    \livretVerse#12 { Je tremble, et tout à coup, une infernale ardeur }
    \livretVerse#12 { Vient enflammer mon sang, et devorer mon cœur. }
    \livretVerse#12 { Dieux ! que vois-je ? le Ciel s’arme contre la Terre ? }
    \livretVerse#12 { Que desordre ! quel bruit ! quel éclat de tonnerre ! }
    \livretVerse#12 { Quels abysmes profonds sous mes pas sont ouverts ! }
    \livretVerse#12 { Que de fantosmes vains sont sortis des Enfers ! }
    \livretVerse#12 { Sangaride, ah fuyez la mort que vous prepare }
    \livretVerse#8 { Une Divinité barbare : }
    \livretVerse#12 { C’est vostre seul peril qui cause ma terreur. }
    \livretPers Sangaride
    \livretVerse#12 { Atys reconnoissez vostre funeste erreur. }
    \livretPers Atys
    \livretVerse#12 { Quel Monstre vient à nous ! quelle fureur le guide ! }
    \livretVerse#12 { Ah respecte, cruel, l’aimable Sangaride. }
    \livretPers Sangaride
    \livretVerse#12 { Atys, mon cher Atys. }
    \livretPers Atys
    \livretVerse#12 { \transparent { Atys, mon cher Atys. } Quels hurlements affreux ! }
    \livretPers Celænus
    \livretVerse#8 { Fuyez, sauvez-vous de sa rage. }
    \livretPers Atys
    \livretVerse#12 { Il faut combatre ; Amour, seconde mon courage. }
  }
  \column {
    \livretPers Celænus, & le Chœur
    \livretVerse#8 { Arreste, arreste malheureux. }
    \livretPers Sangaride
    \livretVerse#12 { Atys ! }
    \livretPers Le Chœur
    \livretVerse#12 { \transparent { Atys ! } O Ciel }
    \livretPers Sangaride
    \livretVerse#12 { \transparent { Atys ! O Ciel } Je meurs. }
    \livretPers Le Chœur
    \livretVerse#12 { \transparent { Atys ! O Ciel Je meurs. } Atys, Atys luy-mesme, }
    \livretVerse#6 { Fait perir ce qu’il aime ! }
    \livretPers Celænus
    \livretVerse#12 { Je n’ay pû retenir ses efforts furieux, }
    \livretVerse#8 { Sangaride expire à vos yeux. }
    \livretPers Cybele
    \livretVerse#12 { Atys me sacrifie une indigne Rivale. }
    \livretVerse#12 { Partagez avec moy la douceur sans esgale, }
    \livretVerse#12 { Que l’on gouste en vengeant un amour outragé. }
    \livretVerse#12 { Je vous l’avois promis. }
    \livretPers Celænus
    \livretVerse#12 { \transparent { Je vous l’avois promis. } O promesse fatale ! }
    \livretVerse#12 { Sangaride n’est plus, et je suis trop vangé. }
  }
}#}))

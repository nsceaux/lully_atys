\livretAct PROLOGUE
\livretDescAtt\wordwrap-center {
  Le Theatre represente le palais du Temps, où ce dieu paroist au
  milieu des douze Heures du jour, & des douze Heures de la nuit.
}
\livretPers Le Temps
\livretRef#'AABrecitEnVainJaiRespecte
%# En vain j'ay respecté la celebre memoire
%# Des heros des siecles passez;
%# C'est en vain que leurs noms si fameux dans l'*histoire,
%# Du sort des noms communs ont esté dispensez:
%# Nous voy=ons un heros dont la brillante gloire
%# Les a presque tous effacez.

\livretPers Chœur des Heures
\livretRef#'AACchoeurSesJustesLois
%# Ses justes loix,
%# Ses grands exploits
%# Rendent sa memoire éternelle:
%# Chaque jour, chaque instant
%# Adjouste encor à son nom esclattant
%# Une gloire nouvelle.

\livretRef#'AADairFlore
\livretDidasPPage\justify {
  La Déesse Flore conduite par un des Zephirs s’avance avec une troupe
  de nymphes qui portent divers ornements de fleurs.
}
\livretPers Le Temps
\livretRef#'AAErecitLaSaisonDesFrimas
%# La saison des frimas peut-elle nous offrir
%# Les fleurs que nous voy=ons paraistre?
%# Quel dieu les fait renaistre
%# Lorsque l'*hyver les fait mourir?
%# Le froid cru=el regne encore;
%# Tout est glacé dans les champs,
%# D'où vient que Flore
%# Devance le printemps?
\livretPers Flore
%# Quand j'attens les beaux jours, je viens toûjours trop tard,
%# Plus le printemps s'avance, et plus il m'est cõtraire;
%# Son retour presse le départ
%# Du heros à qui je veux plaire.
%# Pour luy faire ma cour, mes soins ont entrepris
%# De braver desormais l'*hyver le plus terrible,
%# Dans l'ardeur de luy plaire on a bien-tost apris
%# A ne rien trouver d'impossible.

\livretPers Le Temps & Flore
\livretRef#'AAFairChoeurLesPlaisirASesYeux
%# Les Plaisirs à ses yeux ont beau se presenter,
%# Si-tost qu'il voit Bellone, il quitte tout pour elle;
%# Rien ne peut l'arrester
%# Quand la Gloire l'appelle.
\livretRef#'AAFbChoeurRienNePeutLArreter
\livretDidasPPage\wordwrap {
  Le chœur des Heures repete ces deux derniers vers.
}

\livretRef#'AAGgavotte
\livretDidasPPage\wordwrap {
  La suite de Flore commence des jeux meslez de dances & de chants.
}

\livretPers Un Zephir
\livretRef#'AAHairLePrintempsQuelqueFois
%# Le printemps quelquefois est moins doux qu'il ne semble,
%# Il fait trop pay=er ses beaux jours;
%# Il vient pour escarter les jeux et les amours,
%# Et c'est l'*hyver qui les rassemble.

\livretRef#'AAIpreludeMelpomene
\livretDidasPPage\justify {
  Melpomene qui est la muse qui preside à la tragedie, vient
  accompagnée d’une troupe de heros, elle est suivie d’Hercule,
  d’Antæe, de Castor, de Pollux, de Lincée, d’Idas, d’Eteocle, & de
  Polinice.
}
\livretPersDidas Melpomene parlant à Flore
\livretRef#'AAJrecitRetirezVous
%# Retirez-vous, cessez de prevenir le Temps;
%# Ne me desrobez point de preci=eux instants:
%# La puissante Cybele
%# Pour honorer Atys qu'elle a privé du jour,
%# Veut que je renouvelle
%# Dans une illustre cour
%# Le souvenir de son amour.
%# Que l'agrément rustique
%# De Flore et de ses jeux,
%# Cede à l'appareil magnifique
%# De la muse tragique,
%# Et de ses Spectacles pompeux.

\livretRef#'AAKairMelpomene
\livretDidasPPage\justify {
  La suite de Melpomene prend la place de la Suite de Flore.  Les
  heros recommencent leurs anciennes querelles.  Hercule combat &
  lutte contre Antæe, Castor & Pollux combattent contre Lyncée & Idas,
  & Eteocle combat contre son frere Polynice.
}
\livretRef#'AALbCybeleVeutQueFlore
\livretDidasPPage\justify {
  Iris, par l’ordre de Cybele, descend assiss sur son arc, pour
  accorder Melpomene & Flore.
}
\livretPersDidas Iris parlant à Melpomene
%# Cybele veut que Flore aujourd'huy vous seconde.
%# Il faut que les Plaisirs viennent de toutes parts,
%# Dans l'empire puissant, où regne un nouveau Mars,
%# Ils n'ont plus d'autre asile au monde.
%# Rendez-vous, s'il se peut, dignes de ses regards;
%# Joignez la beauté vive & pure
%# Dont brille la nature,
%# Aux ornements des plus beaux arts.
\livretDidasP\justify {
  Iris remonte au ciel sur son arc, & la suite de Melpomene s’accorde
  avec la suite de Flore.
}
\livretPers Melpomene & Flore
%# Rendons-nous, s'il se peut, dignes de ses regards;
%# Joignons la beauté vive & pure
%# Dont brille la nature,
%# Aux ornements des plus beaux arts.

\livretPers Le Temps, & le Chœur des Heures
%# Preparez de nouvelles festes,
%# Profitez du loisir du plus grand des heros;
\livretPers Le Temps, Melpomene & Flore
%#8 Preparez/Preparons de nouvelles festes
%#12 Profitez/Profitons du loisir du plus grand des heros.
\livretPers Tous ensemble
%# Le temps des Jeux, et du repos,
%# Luy sert à mediter de nouvelles conquestes.
\sep

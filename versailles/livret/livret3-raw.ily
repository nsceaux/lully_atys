\livretAct\line { ACTE TROISIESME }
\livretDescAtt\wordwrap-center {
  \line { Le Theatre change & represente }
  \line { le Palais du Sacrificateur de Cybele. }
}
\livretScene\line { SCENE PREMIERE }
\livretPersDidas Atys seul
\livretRef#'DAArecitQueServentLesFaveurs
%# Que servent les faveurs que nous fait la Fortune
%# Quand l'Amour nous rend malheureux?
%# Je pers l'unique bien qui peut combler mes vœux,
%# Et tout autre bien m'importune.
%# Que servent les faveurs que nous fait la Fortune
%# Quand l'Amour nous rend malheureux?

\livretScene\line { SCENE SECONDE }
\livretDescAtt\wordwrap-center { Idas, Doris, Atys. }
\livretPers Idas
\livretRef #'DBArecitPeutOnIciParlerSansFeindre
%# Peut-on icy parler sans feindre? 
\livretPers Atys
%# Je commande en ces lieux, vous n'y devez rien craindre.
\livretPers Doris
%#- Mon frere est votre amy.
\livretPers Idas
%#= Fi=ez-vous à ma sœur.
\livretPers Atys
%# Vous devez avec moy partager mon bon-heur.
\livretPers\line { Idas, & Doris }
%# Nous venons partager vos mortelles allarmes;
%# Sangaride les yeux en larmes
%# Nous vient d'ouvrir son cœur.
\livretPers Atys
%# L'heure aproche où l'*Hymen voudra qu'elle se livre
%# Au pouvoir d'un heureux espoux.
\livretPers\line { Idas, & Doris }
%# Elle ne peut vivre
%# Pour un autre que pour vous.
\livretPers Atys
%# Qui peut la dégager du devoir qui la presse?
\livretPers\line { Idas, & Doris }
%# Elle veut elle mesme aux pieds de la De=esse
%# Declarer hautement vos secretes amours.
\livretPers Atys
%# Cybele pour moy s'interesse,
%# J'ose tout esperer de son divin secours…
%# Mais quoy, trahir le Roy! tromper son esperance!
%# De tant de biens receus est-ce la recompense?
\livretPers\line { Idas, & Doris }
%# Dans l'Empire amoureux
%# Le Devoir n'a point de puissance;
%# L'Amour dispence
%# Les Rivaux d'estre genereux;
%# Il faut souvent pour devenir heureux
%# Qu'il en couste un peu d'innocence.
\livretPers Atys
%# Je souhaite, je crains, je veux, je me repens.
\livretPers\line { Idas, & Doris }
%# Verrez-vous un rival heureux à vos dépens?
\livretPers Atys
%# Je ne puis me resoudre à cette vi=olence.
\livretPers\line { Atys, Idas, & Doris }
%# En vain, un cœur, incertain de son choix,
%# Met en balance mille fois
%# L'Amour et la Reconnoissance,
%# L'Amour toûjours emporte la balance.
\livretPers Atys
%# Le plus juste party cede enfin au plus fort.
%# Allez, prenez soin de mon sort,
%# Que Sangaride icy se rende en diligence.

\livretScene\line { SCENE TROISIESME }
\livretPersDidas Atys seul
\livretRef #'DCBrecitNousPouvonsNousFlatter
%# Nous pouvons nous flater de l'espoir le plus doux
%# Cybele et l'Amour sont pour nous.
%# Mais du Devoir trahi j'entends la voix pressante
%# Qui m'accuse et qui m'épouvante.
%# Laisse mon cœur en paix, impuissante Vertu,
%# N'ay-je point assez combatu?
%# Quand l'Amour malgré toy me contraint à me rendre,
%# Que me demandes-tu?
%# Puisque tu ne peux me deffendre,
%# Que me sert-il d'entendre
%# Les vains reproches que tu fais?
%# Impuissante Vertu laisse mon cœur en paix.
%# Mais le Sommeil vient me surprendre,
%# Je combats vainement sa charmante douceur.
%# Il faut laisser suspendre
%# Les troubles de mon cœur.
\livretDidasP\line { Atys descend. }

\livretScene\line { SCENE QUATRIESME }
\livretDescAtt\justify {
  Le Theatre change & represente un Antre entouré de Pavots & de
  Ruisseaux, où le Dieux du Sommeil se vient rendre accompagné des
  Songes agreables & funestes.
}
\livretDescAtt\center-column {
  \wordwrap-center {
    Atys dormant. Le Sommeil, Morphée, Phobetor, Phantase,
    "Les Songes heureux."
    "Les Songes funestes."
  }
  \smaller\wordwrap-center\italic {
    "Deux Songes joüants de la Violle."
    "Deux Songes joüants du Theorbe."
    "Six Songes joüants de la Flutte."
    "Douze Songes funestes chantants."
    "Seize Songes agreables & funestes dançans."
    "Huit Songes funestes dançants."
  }
}
\livretPers "Le Sommeil"
\livretRef#'DDAdormons
%# Dormons, dormons tous;
%# Ah que le repos est doux!
\livretPers Morphée
%# Regnez, divin Sommeil, regnez sur tout le monde,
%# Répandez vos pavots les plus assoupissans;
%# Calmez les soins, charmez les sens,
%# Retenez tous les cœurs dans une paix profonde.
\livretPers Phobetor
%# Ne vous faites point vi=olence,
%# Coulez, murmurez, clairs Ruisseaux,
%# Il n'est permis qu'au bruit des eaux
%# De troubler la douceur d'un si charmant silence.
\livretPers\wordwrap { Le Sommeil, Morphée, Phobetor & Phantase }
%# Dormons, dormons tous,
%# Ah que le repos est doux!
\livretDidasP\justify {
  Les Songes agreables aprochent d'Atys, & par leurs chants, & par
  leurs dances, luy font connoistre l'amour de Cybele, & le bonheur
  qu'il en doit esperer.
}
\livretPers Morphée
\livretRef #'DDBsommeil
%# Escoute, escoute Atys la gloire qui t'appelle,
%# Sois sensible à l'*honneur d'estre aymé de Cybelle,
%# Joü=is heureux Atys de ta felicité.
\livretPers\line { Morphée, Phobetor & Phantase }
%# Mais souvien-toy que la Beauté
%# Quand elle est immortelle,
%# Demande la fidelité
%# D'une amour éternelle.
\livretPers Phantase
%# Que l'Amour a d'attraits
%# Lors qu'il commence
%# A faire sentir sa puissance,
%# Que l'Amour a d'attraits
%# Lors qu'il commence
%# Pour ne finir jamais.
  \null
%# Trop heureux un Amant
%# Qu'amour exemte
%# Des peines d'une longue attente!
%# Trop heureux un Amant
%# Qu'amour exemte
%# De crainte, et de tourment!
\livretPers Morphée
\livretRef #'DDDsommeil
%# Gouste en paix chaque jour une douceur nouvelle,
%# Partage l'*heureux sort d'une Divinité,
%# Ne vante plus la liberté,
%# Il n'en est point du prix d'une chaîne si belle:
\livretPers\line { Morphée, Phobetor & Phantase }
%# Mais souvien-toy que la Beauté
%# Quand elle est immortelle,
%# Demande la fidelité
%# D'une amour éternelle.
\livretPers Phantase
%# Que l'Amour a d'attraits
%# Lors qu'il commence
%# A faire sentir sa puissance,
%# Que l'Amour a d'attraits
%# Lors qu'il commence
%# Pour ne finir jamais.
\livretDidasP\justify {
  Les songes funestes approchent d'Atys, & le menacent de la vengeance
  de Cybele s'il mesprise son amour, & s'il ne l'ayme pas avec
  fidelité.
}
\livretPers\line { Un Songe funeste }
\livretRef #'DDFfuneste
%# Garde-toy d'offencer un amour glori=eux,
%# C'est pour toy que Cybele abandonne les Cieux
%# Ne trahis point son esperance.
%# Il n'est point pour les Dieux de mespris innocent,
%# Ils sont jaloux des Cœurs, ils ayment la vengeance,
%# Il est dangereux qu'on offence
%# Un amour tout-puissant.
\livretPers\line { Chœur de Songes funestes }
\livretRef #'DDHchoeur
%# L'amour qu'on outrage
%# Se transforme en rage,
%# Et ne pardonne pas
%# Aux plus charmants appas.
%# Si tu n'aymes point Cybele
%# D'une amour fidelle,
%# Malheureux, que tu souffriras!
%# Tu periras:
%# Crains une vengeance cru=elle,
%# Tremble, crains un affreux trépas.
\livretDidasP\justify {
  Atys espouvanté par les Songes funestes, se resveille en sursaut, le
  Sommeil & les Songes disparoissent avec l'Antre où ils estoient, &
  Atys se retrouve dans le mesme Palais où il s'estoit endormy.
}

\livretScene\line { SCENE CINQUIESME }
\livretDescAtt\wordwrap-center { Atys, Cybele, & Melisse. }
\livretPers Atys
\livretRef #'DEAcybeleAtys
%# Venez à mon secours ô Dieux! ô justes Dieux!
\livretPers Cybele
%# Atys, ne craignez rien, Cybele, est en ces lieux.
\livretPers Atys
%# Pardonnez au desordre où mon cœur s'abandonne;
%#- C'est un songe…
\livretPers Cybele
%#= Parlez, quel songe vous estonne?
%# Expliquez moy vostre embaras.
\livretPers Atys
%# Les songes sont trompeurs, et je ne les croy pas.
%# Les plaisirs et les peines
%# Dont en dormant on est séduit,
%# Sont des chimeres vaines
%# Que le resveil détruit.
\livretPers Cybele
%# Ne mesprisez pas tant les songes
%# L'Amour peut souvent emprunter leur voix,
%# S'ils font souvent des mensonges
%# Ils disent vray quelquefois.
%# Ils parloient par mon ordre, et vous les devez croire.
\livretPers Atys
%#- O Ciel?
\livretPers Cybele
%#= N'en doutez point, connoissez vostre gloire.
%# Respondez avec liberté,
%# Je vous demande un cœur qui despend de luy-mesme.
\livretPers Atys
%# Une grande Divinité
%# Doit d'assûrer toûjours de mon respect extresme.
\livretPers Cybele
%# Les Dieux dans leur grandeur supresme
%# Reçoivent tant d'honneurs qu'ils en sont rebutez,
%# Ils se lassent souvent d'estre trop respectez,
%# Ils sont plus contents qu'on les ayme.
\livretPers Atys
%# Je sçay trop ce que je vous doy
%# Pour manquer de reconnoissance…

\livretScene\line { SCENE SIXIESME }
\livretDescAtt\wordwrap-center { Sangaride, Cybele, Atys, Melisse. }
\livretPersDidas Sangaride "se jettant aux pieds de Cybele"
\livretRef#'DFAcybeleAtysSangaride
%# J'ay recours à vostre puissance,
%# Reyne des Dieux, protegez-moy.
%# L'interest d'Atys vous en presse…
\livretPersDidas Atys "interrompant Sangaride"
%# Je parleray pour vous, que vostre crainte cesse.
\livretPers Sangaride
%# Tous deux unis des plus beaux nœuds…
\livretPersDidas Atys "interrompant Sangaride"
%# Le sang et l'amitié nous unissent tous deux.
%# Que vostre secours la délivre
%# Des loix d'un Hymen rigoureux,
%# Ce sont les plus doux de ses vœux
%# De pouvoir à jamais vous servir et vous suivre.
\livretPers Cybele
%# Les Dieux sont les protecteurs
%# De la liberté des cœurs.
%# Allez, ne craignez point le Roy ny sa colere,
%# J'auray soin d'appaiser
%# Le Fleuve Sangar vostre Pere;
%# Atys veut vous favoriser,
%# Cybele en sa faveur ne peut rien refuser.
\livretPers Atys
%#- Ah! c'en est trop…
\livretPers Cybele
%#= Non, non, il n'est pas necessaire
%# Que vous cachiez vostre bonheur,
%# Je ne prétens point faire
%# Un vain mystere
%# D'un amour qui vous fait honneur.
%# Ce n'est point à Cybelle à craindre d'en trop dire.
%# Il est vray, j'ayme Atys, pour luy j'ay tout quitté,
%# Sans luy je ne veux plus de grandeur ny d'Empire,
%# Pour ma felicité
%# Son cœur seul peut suffire.
%# Allez, Atys luy-mesme ira vous garentir
%# De la fatale vi=olence
%# Où vous ne pouvez consentir.
\livretDidasP\line { Sangaride se retire. }
\livretPersDidas Cybele "parle à Atys"
%# Laissez-nous, attendez mes ordres pour partir,
%# Je prétens vous armer de ma toute-puissance.

\livretScene\line { SCENE SEPTIESME }
\livretDescAtt\wordwrap-center { Cybele, Melisse. }
\livretPers Cybele
\livretRef #'DGAcybeleMelisse
%# Qu'Atys dans ses respects mesle d'indifference!
%# L'Ingrat Atys ne m'ayme pas;
%# L'Amour veut de l'amour, tout autre prix l'offence,
%# Et souvent le respect et la reconnoissance
%# Sont l'excuse des cœurs ingrats.
\livretPers Melisse
%# Ce n'est pas un si grand crime
%# De ne s'exprimer pas bien,
%# Un cœur qui n'ayma jamais rien
%# Sçait peu comment l'amour s'exprime.
\livretPers Cybele
%# Sangaride est aymable, Atys peut tout charmer,
%# Ils tesmoignent trop s'estimer,
%# Et de simples parents sont moins d'intelligence:
%# Ils se sont aymez dès l'enfance,
%# Ils pourroient enfin trop s'aymer.
%# Je crains une amitié que tant d'ardeur anime.
%# Rien n'est si trompeur que l'estime:
%# C'est un nom supposé
%# Qu'on donne quelquefois à l'amour desguisé.
%# Je prétens m'esclaircir leur feinte sera vaine.
\livretPers Melisse
%# Quels secrets par les Dieux ne sont point penetrez?
%# Deux cœurs à feindre preparez
%# Ont beau cacher leur chaîne,
%# On abuse avec peine
%# Les Dieux par l'Amour esclairez.
\livretPers Cybele
%# Va, Melisse, donne ordre à l'aymable Zephire
%# D'accomplir promptement tout ce qu'Atys desire.

\livretScene\line { SCENE HUITIESME }
\livretPersDidas Cybele seule
\livretRef#'DHAcybele
%# Espoir si cher, et si doux,
%# Ah! pourquoy me trompez-vous?
%# Des suprêmes grandeurs vous m'avez fait descendre,
%# Mille Cœurs m'adoroient, je les neglige tous,
%# Je n'en demande qu'un, il a peine à se rendre;
%# Je ne sens que chagrin, et que soupçons jaloux;
%# Est-ce le sort charmant que je devois attendre?
%# Espoir si cher, et si doux,
%# Ah! pourquoy me trompez-vous?
%# Helas! par tant d'attraits falloit-il me surprendre?
%# Heureuse, si toûjours j'avois pû m'en deffendre!
%# L'Amour qui me flattoit me cachoit son couroux:
%# C'est donc pour me fraper des plus funestes coups,
%# Que le cru=el Amour m'a fait un cœur si tendre?
%# Espoir si cher, et si doux,
%# Ah! pourquoy me trompez-vous?
\sep

\livretAct \line { ACTE SECOND }
\livretDescAtt\wordwrap-center {
  Le Theatre change & represente le Temple de Cybele.
}
\livretScene\line { SCENE PREMIERE }
\livretDescAtt \wordwrap-center {
  Celænus Roy de Phrygie. Atys, Suivans de Celænus.
}
\livretPers Celænus
\livretRef #'CABrecitNavancezPasPlusLoin
%# N'avancez pas plus loin, ne suivez point mes pas;
%# Sortez. Toy ne me quitte pas.
%# Atys, il faut attendre icy que la Dé=esse
%# Nomme un grand Sacrificateur.
\livretPers Atys
%# Son choix sera pour vous, Seigneur; quelle tristesse
%# Semble avoir surpris vostre cœur?
\livretPers Celænus
%# Les Roys les plus puissants connoissent l'importance
%# D'un si glori=eux choix:
%# Qui pourra l'obtenir estendra sa puissance
%# Par tout où de Cybele on revere les loix.
\livretPers Atys
%# Elle honore aujourd'huy ces lieux de sa presence,
%# C'est pour vous preferer aux plus puissans des Roys.
\livretPers Celænus
%# Mais quand j'ay veu tantost la Beauté qui m'enchante,
%# N'as-tu point remarqué comme elle estoit tremblante?
\livretPers Atys
%# A nos jeux, à nos chants, j'estois trop appliqué,
%# Hors la feste, Seigneur, je n'ay rien remarqué.
\livretPers Celænus
%# Son trouble m'a surpris. Elle t'ouvre son ame;
%# N'y découvres-tu point quelque secrette flâme?
%#- Quelque Rival caché?
\livretPers Atys
%#= Seigneur, que dites-vous?
\livretPers Celænus
%# Le seul nom de rival allume mon couroux.
%# J'ay bien peur que le Ciel n'ait pû voir sans envie
%# Le bonheur de ma vie,
%# Et si j'estois aimé mon sort seroit trop doux.
%# Ne t'estonnes point tant de voir la jalousie
%# Dont mon ame est saisie
%# On ne peut bien aimer sans estre un peu jaloux.
\livretPers Atys
%# Seigneur, soy=ez content, que rien ne vous allarme;
%# L'Hymen va vous donner la Beauté qui nous charme,
%# Vous serez son heureux espoux.
\livretPers Celænus
%# Tu peux me rassurer, Atys, je te veux croire,
%# C'est son cœur que je veux avoir,
%# Dy-moy s'il est en mon pouvoir?
\livretPers Atys
%# Son cœur suit avec soin le Devoir et la Gloire,
%# Et vous avez pour vous le Gloire et le Devoir.
\livretPers Celænus
%# Ne me déguise point ce que tu peux connaistre.
%# Si j'ay ce que j'aime en ce jour
%# L'Hymen seul m'en rend-t'il le maistre?
%# La Gloire et le Devoir auront tout fait, peut-estre,
%# Et ne laissent pour moy rien à faire à l'Amour.
\livretPers Atys
%# Vous aimez d'un amour trop delicat, trop tendre.
\livretPers Celænus
%# L'indifferent Atys ne le sçauroit comprendre.
\livretPers Atys
%# Qu'un Indifferent est heureux!
%# Il joü=it d'un destin paisible.
%# Le Ciel fait un present bien cher, bien dangeureux,
%# Lorsqu'il donne un cœur trop sensible.
\livretPers Celænus
%# Quand on aime bien tendrement
%# On ne cesse jamais de souffrir, et de craindre;
%# Dans le bonheur le plus charmant,
%# On est ingéni=eux à se faire un tourment,
%# Et l'on prend plaisir à se plaindre.
%# Va songe à mon hymen, et voy si tout est prest,
%# Laisse-moy seul icy, la Dé=esse paraist.

\livretScene \line { SCENE SECONDE }
\livretDescAtt\wordwrap-center {
  Cybele, Celænus, Melisse, Troupe de Prestresses de Cybele.
}
\livretPers Cybele
\livretRef #'CBBrecitJeVeuxJoindreEnCesLieux
%# Je veux joindre en ces lieux la gloire et l'abondance,
%# D'un sacrificateur je veux faire le choix,
%# Et le Roy de Phrygie =auroit la preference
%# Si je voulois choisir entre les plus grands Roys.
%# Le puissant Dieux des flots vous donna la naissance,
%# Un Peuple renommé s'est mis sous vostre loy;
%# Vous avez sans mon choix, d'ailleurs, trop de puissance,
%# Je veux faire un bonheur qui ne soit dû qu'à moy.
%# Vous estimez Atys, et c'est avec justice,
%# Je pretens que mon choix à vos vœux soit propice,
%# C'est Atys que je veux choisir.
\livretPers Celænus
%# J'aime Atys, et je voy sa gloire avec plaisir.
%# Je suis Roy, Neptune est mon pere,
%# J'espouse une Beauté qui va combler mes vœux:
%# Le souhait qui me reste à faire,
%# C'est de voir mon Amy parfaitement heureux.
\livretPers Cybele
%# Il m'est doux que mon choix à vos désirs réponde;
%# Une grande Divinité
%# Doit faire sa felicité
%# Du bien de tout le monde.
%# Mais sur tout le bonheur d'un Roy chery des Cieux
%# Fait le plus doux plaisir des Dieux.
\livretPers Celænus
%# Le sang aproche Atys de la Nymphe que j'aime,
%# Son merite l'égale aux Roys:
%# Il soûtiendra mieux que moy-mesme
%# La majesté supresme
%# De vos divines loix.
%# Rien ne pourra troubler son zele,
%# Son cœur s'est conservé libre jusqu'à ce jour;
%# Il faut tout un cœur pour Cybele,
%# A peine tout le mien peut suffire à l'Amour.
\livretPers Cybele
%# Portez à votre Amy la premiere nouvelle
%# De l'honneur éclatant où ma faveur l'appelle.

\livretScene\line { SCENE TROISIESME }
\livretDescAtt\wordwrap-center { Cybele, Melisse. }
\livretPers Cybele
\livretRef#'CCArecitTuTetonnesMelisse
%# Tu tétonnes, Melisse, et mon choix te surprend?
\livretPers Melisse
%# Atys vous doit beaucoup, et son bonheur est grand.
\livretPers Cybele
%# J'ay fait encor pour luy plus que tu ne peux croire.
\livretPers Melisse
%# Est-il pour un Mortel un rang plus glori=eux?
\livretPers Cybele
%# Tu ne vois que sa moindre gloire;
%# Ce Mortel dans mon cœur est au dessus des Dieux.
%# Ce fut au jour fatal de ma derniere Feste
%# Que de l'aimable Atys je devins la conqueste:
%# Je partis à regret pour retourner aux Cieux,
%# Tout m'y parut changé, rien n'y pleût à mes yeux.
%# Je sens un plaisir extrème
%# A revenir dans ces lieux;
%# Où peut-on jamais estre mieux,
%# Qu'aux lieux où l'on voit ce qu'on aime.
\livretPers Melisse
%# Tous les Dieux ont aimé, Cybele aime à son tour.
%# Vous méprisiez trop l'Amour,
%# Son nom vous sembloit étrange,
%# A la fin il vient un jour
%# Où l'Amour se vange.
\livretPers Cybele
%# J'ay crû me faire un cœur maistre de tout son sort,
%# Un cœur toûjours exempt de trouble et de tendresse.
\livretPers Melisse
%# Vous braviez à tort
%# L'Amour qui vous blesse;
%# Le cœur le plus fort
%# A des momens de foiblesse.
%# Mais vous pouviez aimer, et descendre moins bas.
\livretPers Cybele
%# Non, trop d'égalité rend l'amour sans appas.
%# Quel plus haut rang ay-je à pretendre?
%# Et dequoy mon pouvoir ne vient-il point à bout?
%# Lors qu'on est au dessus de tout,
%# On se fait pour aimer un plaisir de descendre.
%# Je laisse aux Dieux les biens dans le Ciel preparez,
%# Pour Atys, pour son cœur, je quitte tout sans peine,
%# S'il m'oblige à descendre, un doux penchant m'entraîne;
%# Les cœurs que le Destin à le plus separez,
%# Sont ceux qu'Amour unit d'une plus forte chaîne.
%# Fay venir le Sommeil; que luy-mesme en ce jour,
%# Prenne soin icy de conduire
%# Les Songes qui luy font la Cour;
%# Atys ne sçait point mon amour,
%# Par un moy=en nouveau je pretens l'en instruire.
\livretDidasP\line { Melisse se retire. }
\livretPers Cybele
%# Que les plus doux Zephirs, que les Peuples divers,
%# Qui des deux bouts de l'Univers
%# Sont venus me montrer leur zele,
%# Celebrent la gloire immortelle
%# Du sacrificateur dont Cybele a fait choix,
%# Atys doit dispenser mes loix,
%# Honorez le choix de Cybele.

\livretScene\line { SCENE QUATRIESME }
\livretDescAtt \column {
  \justify {
    Les Zephirs paroissent dans une gloire élevée & brillante. Les
    Peuples differens qui sont venus à la feste de Cybele entrent dans
    le Temple, & tous ensemble s’efforcent d’honorer Atys, qui vient
    revestu des habits de grand Sacrificateur.
  }
  \justify\italic {
    Cinq Zephirs dançans dans la Gloire.  Huit Zephirs joüants du
    Haut-bois & des Cromornes, dans la Gloire.  Troupe de Peuples
    differens chantans qui accompagnent Atys.  Six Indiens & six
    Egiptiens dançans.
  }
}
\livretPers\line { Chœurs des Peuples & des Zephirs. }
\livretRef #'CDAchoeurCelebronsLaGloireImmortelle
%# Celebrons la gloire immortelle
%# Du Sacrificateur dont Cybele a fait choix:
%# Atys doit dispenser ses loix,
%# Honorons le choix de Cybele.
\livretRef #'CDDchoeurQueDevantVous
%# Que devant Vous tout s'abaisse, et tout tremble;
%# Vivez heureux, vos jours sont nostre espoir:
%# Rien n'est si beau que de voir ensemble
%# Un grand merite avec un grand pouvoir.
%# Que l'on benisse
%# Le Ciel propice,
%# Qui dans vos mains
%# Met le sort des Humains.
\livretPers Atys
\livretRef #'CDEindigneQueJeSuis
%# Indigne que je suis des honneurs qu'on m'adresse,
%# Je dois les recevoir au nom de la Dé=esse;
%# J'ose, puis qu'il luy plaist, luy presenter vos vœux:
%# Pour le prix de vostre zele,
%# Que la puissante Cybele
%# Vous rende à jamais heureux.
\livretPers\line { Chœurs des Peuples & des Zephirs. }
\livretRef #'CDFchoeurQueLaPuissanteCybele
%# Que la puissante Cybele
%# Nous rende à jamais heureux.
\sep

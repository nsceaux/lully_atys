\notesSection "Livret"
\markuplist\abs-fontsize-lines #8 \page-columns-title \act\line { LIVRET } {
\livretAct PROLOGUE
\livretDescAtt\wordwrap-center {
  Le Theatre represente le palais du Temps, où ce dieu paroist au
  milieu des douze Heures du jour, & des douze Heures de la nuit.
}
\livretPers Le Temps
\livretRef#'AABrecitEnVainJaiRespecte
\livretVerse#12 { En vain j’ay respecté la celebre memoire }
\livretVerse#8 { Des heros des siecles passez ; }
\livretVerse#12 { C’est en vain que leurs noms si fameux dans l’histoire, }
\livretVerse#12 { Du sort des noms communs ont esté dispensez : }
\livretVerse#12 { Nous voyons un heros dont la brillante gloire }
\livretVerse#8 { Les a presque tous effacez. }

\livretPers Chœur des Heures
\livretRef#'AACchoeurSesJustesLois
\livretVerse#4 { Ses justes loix, }
\livretVerse#4 { Ses grands exploits }
\livretVerse#8 { Rendent sa memoire éternelle : }
\livretVerse#6 { Chaque jour, chaque instant }
\livretVerse#10 { Adjouste encor à son nom esclattant }
\livretVerse#6 { Une gloire nouvelle. }

\livretRef#'AADairFlore
\livretDidasPPage\justify {
  La Déesse Flore conduite par un des Zephirs s’avance avec une troupe
  de nymphes qui portent divers ornements de fleurs.
}
\livretPers Le Temps
\livretRef#'AAErecitLaSaisonDesFrimas
\livretVerse#12 { La saison des frimas peut-elle nous offrir }
\livretVerse#8 { Les fleurs que nous voyons paraistre ? }
\livretVerse#6 { Quel dieu les fait renaistre }
\livretVerse#8 { Lorsque l’hyver les fait mourir ? }
\livretVerse#7 { Le froid cruel regne encore ; }
\livretVerse#7 { Tout est glacé dans les champs, }
\livretVerse#4 { D’où vient que Flore }
\livretVerse#6 { Devance le printemps ? }
\livretPers Flore
\livretVerse#12 { Quand j’attens les beaux jours, je viens toûjours trop tard, }
\livretVerse#11 { Plus le printemps s’avance, et plus il m’est cõtraire ; }
\livretVerse#8 { Son retour presse le départ }
\livretVerse#8 { Du heros à qui je veux plaire. }
\livretVerse#12 { Pour luy faire ma cour, mes soins ont entrepris }
\livretVerse#12 { De braver desormais l’hyver le plus terrible, }
\livretVerse#12 { Dans l’ardeur de luy plaire on a bien-tost apris }
\livretVerse#8 { A ne rien trouver d’impossible. }

\livretPers Le Temps & Flore
\livretRef#'AAFairChoeurLesPlaisirASesYeux
\livretVerse#12 { Les Plaisirs à ses yeux ont beau se presenter, }
\livretVerse#12 { Si-tost qu’il voit Bellone, il quitte tout pour elle ; }
\livretVerse#6 { Rien ne peut l’arrester }
\livretVerse#6 { Quand la Gloire l’appelle. }
\livretRef#'AAFbChoeurRienNePeutLArreter
\livretDidasPPage\wordwrap {
  Le chœur des Heures repete ces deux derniers vers.
}

\livretRef#'AAGgavotte
\livretDidasPPage\wordwrap {
  La suite de Flore commence des jeux meslez de dances & de chants.
}

\livretPers Un Zephir
\livretRef#'AAHairLePrintempsQuelqueFois
\livretVerse#12 { Le printemps quelquefois est moins doux qu’il ne semble, }
\livretVerse#8 { Il fait trop payer ses beaux jours ; }
\livretVerse#12 { Il vient pour escarter les jeux et les amours, }
\livretVerse#8 { Et c’est l’hyver qui les rassemble. }

\livretRef#'AAIpreludeMelpomene
\livretDidasPPage\justify {
  Melpomene qui est la muse qui preside à la tragedie, vient
  accompagnée d’une troupe de heros, elle est suivie d’Hercule,
  d’Antæe, de Castor, de Pollux, de Lincée, d’Idas, d’Eteocle, & de
  Polinice.
}
\livretPersDidas Melpomene parlant à Flore
\livretRef#'AAJrecitRetirezVous
\livretVerse#12 { Retirez-vous, cessez de prevenir le Temps ; }
\livretVerse#12 { Ne me desrobez point de precieux instants : }
\livretVerse#6 { La puissante Cybele }
\livretVerse#12 { Pour honorer Atys qu’elle a privé du jour, }
\livretVerse#6 { Veut que je renouvelle }
\livretVerse#6 { Dans une illustre cour }
\livretVerse#8 { Le souvenir de son amour. }
\livretVerse#6 { Que l’agrément rustique }
\livretVerse#6 { De Flore et de ses jeux, }
\livretVerse#8 { Cede à l’appareil magnifique }
\livretVerse#6 { De la muse tragique, }
\livretVerse#8 { Et de ses Spectacles pompeux. }

\livretRef#'AAKairMelpomene
\livretDidasPPage\justify {
  La suite de Melpomene prend la place de la Suite de Flore.  Les
  heros recommencent leurs anciennes querelles.  Hercule combat &
  lutte contre Antæe, Castor & Pollux combattent contre Lyncée & Idas,
  & Eteocle combat contre son frere Polynice.
}
\livretRef#'AALbCybeleVeutQueFlore
\livretDidasPPage\justify {
  Iris, par l’ordre de Cybele, descend assiss sur son arc, pour
  accorder Melpomene & Flore.
}
\livretPersDidas Iris parlant à Melpomene
\livretVerse#12 { Cybele veut que Flore aujourd’huy vous seconde. }
\livretVerse#12 { Il faut que les Plaisirs viennent de toutes parts, }
\livretVerse#12 { Dans l’empire puissant, où regne un nouveau Mars, }
\livretVerse#8 { Ils n’ont plus d’autre asile au monde. }
\livretVerse#12 { Rendez-vous, s’il se peut, dignes de ses regards ; }
\livretVerse#8 { Joignez la beauté vive & pure }
\livretVerse#6 { Dont brille la nature, }
\livretVerse#8 { Aux ornements des plus beaux arts. }
\livretDidasP\justify {
  Iris remonte au ciel sur son arc, & la suite de Melpomene s’accorde
  avec la suite de Flore.
}
\livretPers Melpomene & Flore
\livretVerse#12 { Rendons-nous, s’il se peut, dignes de ses regards ; }
\livretVerse#8 { Joignons la beauté vive & pure }
\livretVerse#6 { Dont brille la nature, }
\livretVerse#8 { Aux ornements des plus beaux arts. }

\livretPers Le Temps, & le Chœur des Heures
\livretVerse#8 { Preparez de nouvelles festes, }
\livretVerse#12 { Profitez du loisir du plus grand des heros ; }
\livretPers Le Temps, Melpomene & Flore
\livretVerse#8 { Preparez/Preparons de nouvelles festes }
\livretVerse#12 { Profitez/Profitons du loisir du plus grand des heros. }
\livretPers Tous ensemble
\livretVerse#8 { Le temps des Jeux, et du repos, }
\livretVerse#12 { Luy sert à mediter de nouvelles conquestes. }
\sep
\livretAct\line { ACTE PREMIER }
\livretDescAtt\wordwrap-center {
  Le Theatre represente une montagne consacrée à Cybele.
}
\livretScene\line { SCENE PREMIERE }
\livretPers Atys
\livretRef#'BAAairAllonsAllons
\livretVerse#8 { Allons, allons, accourez tous, }
\livretVerse#6 { Cybele va descendre. }
\livretVerse#12 { Trop heureux Phrygiens, venez icy l’attendre. }
\livretVerse#8 { Mille Peuples seront jaloux }
\livretVerse#6 { Des faveurs que sur nous }
\livretVerse#6 { Sa bonté va répandre. }

\livretScene\line { SCENE SECONDE }
\livretPers\line { Idas, Atys. }
\livretRef#'BBAairAllonsAllons
\livretVerse#8 { Allons, allons, accourez tous, }
\livretVerse#6 { Cybele va descendre. }
\livretPers Atys
\livretVerse#12 { Le Soleil peint nos champs des plus vives couleurs, }
\livretVerse#6 { Il a seché les pleurs }
\livretVerse#12 { Que sur l’émail des prez a répandu l’Aurore ; }
\livretVerse#12 { Et ses rayons nouveaux ont déja fait éclorre }
\livretVerse#6 { Mille nouvelles fleurs. }
\livretPers Idas
\livretVerse#8 { Vous veillez lorsque tout sommeille ; }
\livretVerse#8 { Vous nous éveillez si matin }
\livretVerse#8 { Que vous ferez croire à la fin }
\livretVerse#8 { Que c’est l’Amour qui vous éveille. }
\livretPers Atys
\livretVerse#12 { Non tu dois mieux juger du party que je prens. }
\livretVerse#12 { Mon cœur veut fuir toûjours les soins et les misteres ; }
\livretVerse#12 { J’ayme l’heureuse paix des cœurs indifferents ; }
\livretVerse#8 { Si leurs plaisirs ne sont pas grands, }
\livretVerse#8 { Au moins leurs peines sont legeres. }
\livretPers Idas
\livretVerse#8 { Tost ou tard l’Amour est vainqueur, }
\livretVerse#8 { En vain les plus fiers s’en deffendent, }
\livretVerse#8 { On ne peut refuser son cœur }
\livretVerse#8 { A de beaux yeux qui le demandent. }
\livretVerse#12 { Atys, ne feignez plus, je sçais votre secret. }
\livretVerse#8 { Ne craignez rien, je suis discret. }
\livretVerse#8 { Dans un bois solitaire, et sombre, }
\livretVerse#12 { L’indifferent Atys se croyoit seul, un jour ; }
\livretVerse#12 { Sous un feüillage épais où je resvois à l’ombre, }
\livretVerse#8 { Je l’entendis parler d’amour. }
\livretPers Atys
\livretVerse#12 { Si je parle d’amour, c’est contre son empire, }
\livretVerse#8 { J’en fais mon plus doux entretien. }
\livretPers Idas
\livretVerse#8 { Tel se vante de n’aimer rien, }
\livretVerse#8 { Dont le cœur en secret soûpire. }
\livretVerse#12 { J’entendis vos regrets, et je les sçais si bien }
\livretVerse#12 { Que si vous en doutez je vais vous les redire. }
\livretVerse#12 { Amans qui vous plaignez, vous estes trop heureux : }
\livretVerse#12 { Mon cœur de tous les cœurs est le plus amoureux, }
\livretVerse#12 { Et tout prés d’expirer je suis reduit à feindre ; }
\livretVerse#8 { Que c’est un tourment rigoureux }
\livretVerse#8 { De mourir d’amour sans se plaindre ! }
\livretVerse#12 { Amans qui vous plaignez, vous estes trop heureux. }
\livretPers Atys
\livretVerse#12 { Idas, il est trop vray, mon cœur n’est que trop tendre, }
\livretVerse#12 { L’Amour me fait sentir ses plus funestes coups. }
\livretVerse#12 { Qu’aucun autre que toy n’en puisse rien apprendre. }

\livretScene \line { SCENE TROISIEME }
\livretDescAtt\wordwrap-center { Sangaride, Doris, Atys, Idas. }
\livretPers\line { Sangaride, & Doris }
\livretRef#'BCAairAllonsAllons
\livretVerse#8 { Allons, allons, accourez tous, }
\livretVerse#6 { Cybele va descendre. }
\livretPers Sangaride
\livretVerse#8 { Que dans nos concerts les plus doux, }
\livretVerse#8 { Son nom sacré se fasse entendre. }
\livretPers Atys
\livretVerse#12 { Sur l’Univers entier son pouvoir doit s’étendre. }
\livretPers Sangaride
\livretVerse#12 { Les Dieux suivent ses loix et craignent son couroux. }
\livretPers\line { Atys, Sangaride, Idas, Doris }
\livretVerse#12 { Quels honneurs ! quels respects ne doit-on point luy rendre ? }
\livretVerse#8 { Allons, allons, accourez tous, }
\livretVerse#6 { Cybele va descendre. }
\livretPers Sangaride
\livretVerse#12 { Escoutons les oyseaux de ces bois d’alentour, }
\livretVerse#12 { Ils remplissent leurs chants d’une douceur nouvelle. }
\livretVerse#8 { On diroit que dans ce beau jour, }
\livretVerse#8 { Ils ne parlent que de Cybele. }
\livretPers Atys
\livretVerse#12 { Si vous les écoutez, ils parleront d’amour. }
\livretVerse#5 { Un Roy redoutable, }
\livretVerse#5 { Amoureux, aimable, }
\livretVerse#7 { Va devenir vostre espoux ; }
\livretVerse#7 { Tout parle d’amour pour vous. }
\livretPers Sangaride
\livretVerse#12 { Il est vray, je triomphe, et j’aime ma victoire. }
\livretVerse#12 { Quand l’Amour fait regner, est-il un plus grand bien ? }
\livretVerse#8 { Pour vous, Atys, vous n’aimez rien, }
\livretVerse#6 { Et vous en faites gloire. }
\livretPers Atys
\livretVerse#8 { L’Amour fait trop verser de pleurs ; }
\livretVerse#8 { Souvent ses douceurs sont mortelles. }
\livretVerse#8 { Il ne faut regarder les Belles }
\livretVerse#8 { Que comme on voit d’aimables fleurs. }
\livretVerse#7 { J’aime les Roses nouvelles, }
\livretVerse#7 { J’aime les voir s’embellir, }
\livretVerse#7 { Sans leurs épines cruelles, }
\livretVerse#7 { J’aimerois à les cüeillir. }
\livretPers Sangaride
\livretVerse#8 { Quand le peril est agreable, }
\livretVerse#8 { Le moyen de s’en allarmer ? }
\livretVerse#8 { Est-ce un grand mal de trop aimer }
\livretVerse#6 { Ce que l’on trouve aimable ? }
\livretVerse#12 { Peut-on estre insensible aux plus charmans appas ? }
\livretPers Atys
\livretVerse#8 { Non vous ne me connoissez pas. }
\livretVerse#12 { Je me deffens d’aimer autant qu’il m’est possible ; }
\livretVerse#8 { Si j’aimois, un jour, par malheur, }
\livretVerse#6 { Je connoy bien mon cœur }
\livretVerse#6 { Il seroit trop sensible. }
\livretVerse#12 { Mais il faut que chacun s’assemble prés de vous, }
\livretVerse#8 { Cybele pourroit nous surprendre. }
\livretPers\line { Idas, Atys }
\livretVerse#8 { Allons, allons, accourez tous, }
\livretVerse#6 { Cybele va descendre. }

\livretScene \line { SCENE QUATRIEME }
\livretDescAtt\wordwrap-center { Sangaride, Doris. }
\livretPers Sangaride
\livretRef#'BDAatysEstTropHeureux
\livretVerse#6 { Atys est trop heureux. }
\livretPers Doris
\livretVerse#12 { L’amitié fut toûjours égale entre vous deux, }
\livretVerse#8 { Et le sang d’assez prés vous lie : }
\livretVerse#12 { Quel que soit son bon-heur, luy portez-vous envie ? }
\livretVerse#12 { Vous, qu’aujourd’huy l’Hymen avec de si beaux nœuds }
\livretVerse#8 { Doit unir au Roy de Phrygie ? }
\livretPers Sangaride
\livretVerse#6 { Atys, est trop heureux. }
\livretVerse#12 { Souverain de son cœur, maistre de tous ses vœux, }
\livretVerse#8 { Sans crainte, sans melancolie, }
\livretVerse#12 { Il joüit en repos des beaux jours de sa vie ; }
\livretVerse#12 { Atys ne connoît point les tourmens amoureux, }
\livretVerse#6 { Atys est trop heureux. }
\livretPers Doris
\livretVerse#12 { Quel mal vous fait l’Amour ? vostre chagrin m’estonne. }
\livretPers Sangaride
\livretVerse#12 { Je te fie un secret qui n’est sceu de personne. }
\livretVerse#8 { Je devrois aimer un Amant }
\livretVerse#6 { Qui m’offre une Couronne ; }
\livretVerse#6 { Mais, helas ! vainement }
\livretVerse#6 { Le Devoir me l’ordonne, }
\livretVerse#6 { L’Amour, pour mon tourment, }
\livretVerse#6 { En ordonne autrement. }
\livretPers Doris
\livretVerse#12 { Aimeriez-vous Atys, luy dont l’indifference }
\livretVerse#12 { Brave avec tant d’orgüeil l’Amour et sa puissance ? }
\livretPers Sangaride
\livretVerse#12 { J’aime, Atys, en secret, mon crime, est sans témoins. }
\livretVerse#12 { Pour vaincre mon amour, je mets tout en usage, }
\livretVerse#12 { J’appelle ma raison, j’anime mon courage ; }
\livretVerse#8 { Mais à quoy servent tous mes soins ? }
\livretVerse#8 { Mon cœur en souffre davantage, }
\livretVerse#6 { Et n’en aime pas moins. }
\livretPers Doris
\livretVerse#8 { C’est le commun deffaut des Belles. }
\livretVerse#8 { L’ardeur des conquestes nouvelles }
\livretVerse#12 { Fait negliger les cœurs qu’on a trop tost charmez, }
\livretVerse#12 { Et les Indifferents sont quelquefois aimez }
\livretVerse#8 { Aux dépens des Amants fidelles. }
\livretVerse#12 { Mais vous vous esposez à des peines cruelles. }
\livretPers Sangaride
\livretVerse#12 { Toûjours aux yeux d’Atys je seray sans appas ; }
\livretVerse#12 { Je le sçay, j’y consens, je veux, s’il est possible, }
\livretVerse#8 { Qu’il soit encor plus insensible ; }
\livretVerse#13 { S’il me pouvoit aimer, que deviendrois-je ? helas ! }
\livretVerse#12 { C’est mon plus grand bon-heur qu’Atys ne m’aime pas. }
\livretVerse#13 { Je pretens estre heureuse, au moins, en apparence ; }
\livretVerse#12 { Au destin d’un grand Roy je me vais attacher. }
\livretPers\line { Sangaride, & Doris }
\livretVerse#12 { Un amour malheureux dont le devoir s’offence, }
\livretVerse#8 { Se doit condamner au silence ; }
\livretVerse#12 { Un amour malheureux qu’on nous peut reprocher, }
\livretVerse#8 { Ne sçauroit trop bien se cacher. }

\livretScene\line { SCENE CINQUIESME }
\livretDescAtt\wordwrap-center { Atys, Sangaride, Doris. }
\livretPers Atys
\livretRef#'BEArecitOneVoitDansCesCampagnes
\livretVerse#6 { On voit dans ces campagnes }
\livretVerse#8 { Tous nos Phrygiens s’avancer. }
\livretPers Doris
\livretVerse#8 { Je vais prendre soin de presser }
\livretVerse#6 { Les Nymphes nos Compagnes. }

\livretScene \line { SCENE SIXIESME }
\livretDescAtt\wordwrap-center { Atys, Sangaride. }
\livretPers Atys
\livretRef #'BFArecitSangarideCeJour
\livretVerse#12 { Sangaride, ce jour est un grand jour pour vous. }
\livretPers Sangaride
\livretVerse#12 { Nous ordonnons tous deux la feste de Cybele, }
\livretVerse#9 { L’honneur est égal entre nous. }
\livretPers Atys
\livretVerse#12 { Ce jour mesme, un grand Roy doit estre vostre espoux, }
\livretVerse#12 { Je ne vous vis jamais si contente et si belle ; }
\livretVerse#8 { Que le sort du Roy sera doux ! }
\livretPers Sangaride
\livretVerse#12 { L’indifferent Atys n’en sera point jaloux. }
\livretPers Atys
\livretVerse#12 { Vivez tous deux contens, c’est ma plus chere envie ; }
\livretVerse#13 { J’ay pressé vostre hymen, j’ay servy vos amours. }
\livretVerse#12 { Mais enfin ce grand jour, le plus beau de vos jours, }
\livretVerse#8 { Sera le dernier de ma vie. }
\livretPers Sangaride
\livretVerse#12 { O dieux ! }
\livretPers Atys
\livretVerse#12 { \transparent { O dieux ! } Ce n’est qu’à vous que je veux reveler }
\livretVerse#12 { Le secret desespoir où mon malheur me livre ; }
\livretVerse#12 { Je n’ay que trop sceu feindre, il est temps de parler ; }
\livretVerse#8 { Qui n’a plus qu’un moment à vivre, }
\livretVerse#8 { N’a plus rien à dissimuler. }
\livretPers Sangaride
\livretVerse#8 { Je fremis, ma crainte est extresme ; }
\livretVerse#12 { Atys, par quel malheur faut-il vous voir perir ? }
\livretPers Atys
\livretVerse#8 { Vous me condamnerez vous mesme, }
\livretVerse#8 { Et vous me laisserez mourir. }
\livretPers Sangaride
\livretVerse#12 { J’armeray, s’il se faut, tout le pouvoir supresme… }
\livretPers Atys
\livretVerse#8 { Non, rien ne peut me secourir, }
\livretVerse#12 { Je meurs d’amour pour vous, je n’en sçaurois guerir ; }
\livretPers Sangaride
\livretVerse#12 { Quoy ? vous ? }
\livretPers Atys
\livretVerse#12 { \transparent { Quoy ? vous ? } Il est trop vray. }
\livretPers Sangaride
\livretVerse#12 { \transparent { Quoy ? vous ? Il est trop vray. } Vous m’aimez ? }
\livretPers Atys
\livretVerse#12 { \transparent { Quoy ? vous ? Il est trop vray. Vous m’aimez ? } Je vous aime. }
\livretVerse#8 { Vous me condamnerez vous mesme, }
\livretVerse#8 { Et vous me laisserez mourir. }
\livretVerse#8 { J’ay merité qu’on me punisse, }
\livretVerse#8 { J’offence un Rival genereux, }
\livretVerse#12 { Qui par mille bien-faits a prevenu mes vœux : }
\livretVerse#12 { Mais je l’offence en vain, vous luy rendez justice ; }
\livretVerse#8 { Ah ! que c’est un cruel suplice }
\livretVerse#12 { D’avoüer qu’un Rival est digne d’estre heureux ! }
\livretVerse#12 { Prononcez mon arrest, parlez sans vous contraindre. }
\livretPers Sangaride
\livretVerse#12 { Helas ! }
\livretPers Atys
\livretVerse#12 { \transparent { Helas ! } Vous soûpirez ? je voy couler vos pleurs ? }
\livretVerse#12 { D’un malheureux amour plaignez-vous les douleurs ? }
\livretPers Sangaride
\livretVerse#8 { Atys, que vous seriez à plaindre }
\livretVerse#8 { Si vous sçaviez tous vos malheurs ! }
\livretPers Atys
\livretVerse#8 { Si je vous pers, et si je meurs, }
\livretVerse#8 { Que puis-je encore avoir à craindre ? }
\livretPers Sangaride
\livretVerse#12 { C’est peu de perdre en moy ce qui vous a charmé, }
\livretVerse#12 { Vous me perdez, Atys, et vous estes aimé. }
\livretPers Atys
\livretVerse#12 { Aimé ! qu’entens-je ? ô Ciel ! quel aveu favorable ! }
\livretPers Sangaride
\livretVerse#8 { Vous en serez plus miserable. }
\livretPers Atys
\livretVerse#8 { Mon malheur en est plus affreux, }
\livretVerse#12 { Le bonheur que je pers doit redoubler ma rage ; }
\livretVerse#12 { Mais n’importe, aimez-moy, s’il se peut, d’avantage, }
\livretVerse#12 { Quand j’en devrois mourir cent fois plus malheureux. }
\livretPers Sangaride
\livretVerse#12 { Si vous cherchez la mort, il faut que je vous suive ; }
\livretVerse#12 { Vivez, c’est mon amour qui vous en fait la loy. }
\livretPers Atys
\livretVerse#6 { Hé comment ! hé pourquoy }
\livretVerse#6 { Voulez-vous que je vive, }
\livretVerse#8 { Si vous ne vivez pas pour moy ? }
\livretPers\line { Atys & Sangaride }
\livretVerse#12 { Si l’Hymen unissoit mon destin et le vostre, }
\livretVerse#8 { Que ses nœuds auroient eû d’attraits ! }
\livretVerse#8 { L’Amour fit nos cœurs l’un pour l’autre, }
\livretVerse#12 { Faut-il que le devoir les separe à jamais ? }
\livretPers Atys
\livretVerse#6 { Devoir impitoyable ! }
\livretVerse#6 { Ah quelle cruauté ! }
\livretPers Sangaride
\livretVerse#12 { On vient, feignez encor, craignez d’estre écouté. }
\livretPers Atys
\livretVerse#7 { Aimons un bien plus durable }
\livretVerse#7 { Que l’éclat de la Beauté : }
\livretVerse#5 { Rien n’est plus aimable }
\livretVerse#5 { Que la liberté. }

\livretScene \line { SCENE SEPTIESME }
\livretDescAtt\column {
  \wordwrap-center { Atys, Sangaride, Doris, Idas. }
  \justify {
    Chœur de Phrygiens chantans. Chœur de Phrygiennes chantantes.
    Troupe de Phrygiens dançans. Troupe de Phrygiennes dançantes.
  }
  \smaller\justify {
    Dix Hommes Phrygiens chantans conduits par Atys.
    Dix femmes Phrygiennes chantantes conduites par Sangaride.
    Six Phrygiens dançans.
    Six Nimphes Phrygiennes dançantes.
  }
}
\livretPers Atys
\livretRef#'BGAairMaisDejaDeCeMontSacre
\livretVerse#8 { Mais déja de ce Mont sacré }
\livretVerse#8 { Le sommet paroist éclairé }
\livretVerse#6 { D’une splendeur nouvelle. }
\livretPersDidas Sangaride \line { s’avançant vers la Montagne. }
\livretVerse#12 { Le Déesse descend, allons au devant d’elle. }
\livretPers\line { Atys & Sangaride }
\livretVerse#6 { Commençons, commençons }
\livretVerse#12 { De celebrer icy sa feste solemnelle, }
\livretVerse#6 { Commençons, commençons }
\livretVerse#6 { Nos Jeux et nos chansons. }
\livretDesc\wordwrap-center { Les chœurs repetent ces derniers Vers. }
\livretPers\line { Atys & Sangaride }
\livretVerse#12 { Il est temps que chacun fasse éclater son zele. }
\livretVerse#8 { Venez, Reine des Dieux, venez, }
\livretVerse#8 { Venez, favorable Cybele. }
\livretDesc\wordwrap-center { Les chœurs repetent ces deux derniers Vers. }
\livretPers Atys
\livretVerse#8 { Quittez vostre Cour immortelle, }
\livretVerse#8 { Choisissez ces lieux fortunez }
\livretVerse#8 { Pour vostre demeure éternelle. }
\livretPers\line { Les Chœurs }
\livretVerse#8 { Venez, Reine des Dieux, venez, }
\livretPers Sangaride
\livretVerse#12 { La Terre sous vos pas va devenir plus belle }
\livretVerse#12 { Que le sejour des Dieux que vous abandonnez. }
\livretPers\line { Les Chœurs }
\livretVerse#8 { Venez, favorable Cybele. }
\livretPers\line { Atys & Sangaride }
\livretVerse#12 { Venez voir les Autels qui vous sont destinez. }
\livretPers\line { Atys, Sangaride, Idas, Doris, & Les Chœurs }
\livretVerse#7 { Eoutez un Peuple fidelle }
\livretVerse#4 { Qui vous appelle, }
\livretVerse#8 { Venez Reine des Dieux, venez, }
\livretVerse#8 { Venez favorable Cybele. }

\livretScene \line { SCENE HUITIESME }
\livretDescAtt\justify {
  La Déesse Cybele paroist sur son Char, & les Phrygiens & les
  Phrygiennes luy témoignent leur joye & leur respect.
}
\livretPersDidas Cybele \line { sur son Char. }
\livretRef#'BHBairChoeurVenezTousDansMonTemple
\livretVerse#12 { Venez tous dans mon Temple, et que chacun revere }
\livretVerse#12 { Le sacrificateur dont je vais faire choix : }
\livretVerse#8 { Je m’expliqueray par sa voix, }
\livretVerse#12 { Les vœux qu’il m’offrira seront seurs de me plaire. }
\livretVerse#12 { Je reçoy vos respects ; j’aime à voir les honneurs }
\livretVerse#12 { Dont vous me presentez un éclatant hommage, }
\livretVerse#6 { Mais l’hommage des Cœurs }
\livretVerse#8 { Est ce que j’aime davantage. }
\livretVerse#7 { Vous devez vous animer }
\livretVerse#5 { D’une ardeur nouvelle, }
\livretVerse#7 { S’il faut honorer Cybele, }
\livretVerse#7 { Il faut encor plus l’aimer. }
\livretDesc\justify {
  Cybele portée par son Char volant, se va rendre dans son
  Temple. Tous les Phrygiens s'empressent d'y aller, & repetent les
  quatres derniers Vers que la Déesse a prononcez.
}
\livretPers\line { Les Chœurs }
\livretRef#'BHCchoeurNousDevonsNousAnimer
\livretVerse#7 { Nous devons nous animer }
\livretVerse#5 { D’une ardeur nouvelle, }
\livretVerse#7 { S’il faut honorer Cybele, }
\livretVerse#7 { Il faut encor plus l’aimer. }
\sep

\livretAct \line { ACTE SECOND }
\livretDescAtt\wordwrap-center {
  Le Theatre change & represente le Temple de Cybele.
}
\livretScene\line { SCENE PREMIERE }
\livretDescAtt \wordwrap-center {
  Celænus Roy de Phrygie. Atys, Suivans de Celænus.
}
\livretPers Celænus
\livretRef #'CABrecitNavancezPasPlusLoin
\livretVerse#12 { N’avancez pas plus loin, ne suivez point mes pas ; }
\livretVerse#8 { Sortez. Toy ne me quitte pas. }
\livretVerse#12 { Atys, il faut attendre icy que la Déesse }
\livretVerse#8 { Nomme un grand Sacrificateur. }
\livretPers Atys
\livretVerse#12 { Son choix sera pour vous, Seigneur ; quelle tristesse }
\livretVerse#8 { Semble avoir surpris vostre cœur ? }
\livretPers Celænus
\livretVerse#12 { Les Roys les plus puissants connoissent l’importance }
\livretVerse#6 { D’un si glorieux choix : }
\livretVerse#12 { Qui pourra l’obtenir estendra sa puissance }
\livretVerse#12 { Par tout où de Cybele on revere les loix. }
\livretPers Atys
\livretVerse#13 { Elle honore aujourd’huy ces lieux de sa presence, }
\livretVerse#12 { C’est pour vous preferer aux plus puissans des Roys. }
\livretPers Celænus
\livretVerse#12 { Mais quand j’ay veu tantost la Beauté qui m’enchante, }
\livretVerse#12 { N’as-tu point remarqué comme elle estoit tremblante ? }
\livretPers Atys
\livretVerse#12 { A nos jeux, à nos chants, j’estois trop appliqué, }
\livretVerse#12 { Hors la feste, Seigneur, je n’ay rien remarqué. }
\livretPers Celænus
\livretVerse#12 { Son trouble m’a surpris. Elle t’ouvre son ame ; }
\livretVerse#12 { N’y découvres-tu point quelque secrette flâme ? }
\livretVerse#12 { Quelque Rival caché ? }
\livretPers Atys
\livretVerse#12 { \transparent { Quelque Rival caché ? } Seigneur, que dites-vous ? }
\livretPers Celænus
\livretVerse#12 { Le seul nom de rival allume mon couroux. }
\livretVerse#12 { J’ay bien peur que le Ciel n’ait pû voir sans envie }
\livretVerse#6 { Le bonheur de ma vie, }
\livretVerse#12 { Et si j’estois aimé mon sort seroit trop doux. }
\livretVerse#12 { Ne t’estonnes point tant de voir la jalousie }
\livretVerse#6 { Dont mon ame est saisie }
\livretVerse#12 { On ne peut bien aimer sans estre un peu jaloux. }
\livretPers Atys
\livretVerse#12 { Seigneur, soyez content, que rien ne vous allarme ; }
\livretVerse#13 { L’Hymen va vous donner la Beauté qui nous charme, }
\livretVerse#8 { Vous serez son heureux espoux. }
\livretPers Celænus
\livretVerse#12 { Tu peux me rassurer, Atys, je te veux croire, }
\livretVerse#8 { C’est son cœur que je veux avoir, }
\livretVerse#8 { Dy-moy s’il est en mon pouvoir ? }
\livretPers Atys
\livretVerse#12 { Son cœur suit avec soin le Devoir et la Gloire, }
\livretVerse#12 { Et vous avez pour vous le Gloire et le Devoir. }
\livretPers Celænus
\livretVerse#12 { Ne me déguise point ce que tu peux connaistre. }
\livretVerse#8 { Si j’ay ce que j’aime en ce jour }
\livretVerse#9 { L’Hymen seul m’en rend-t’il le maistre ? }
\livretVerse#12 { La Gloire et le Devoir auront tout fait, peut-estre, }
\livretVerse#12 { Et ne laissent pour moy rien à faire à l’Amour. }
\livretPers Atys
\livretVerse#12 { Vous aimez d’un amour trop delicat, trop tendre. }
\livretPers Celænus
\livretVerse#12 { L’indifferent Atys ne le sçauroit comprendre. }
\livretPers Atys
\livretVerse#8 { Qu’un Indifferent est heureux ! }
\livretVerse#8 { Il joüit d’un destin paisible. }
\livretVerse#12 { Le Ciel fait un present bien cher, bien dangeureux, }
\livretVerse#8 { Lorsqu’il donne un cœur trop sensible. }
\livretPers Celænus
\livretVerse#8 { Quand on aime bien tendrement }
\livretVerse#12 { On ne cesse jamais de souffrir, et de craindre ; }
\livretVerse#8 { Dans le bonheur le plus charmant, }
\livretVerse#12 { On est ingénieux à se faire un tourment, }
\livretVerse#8 { Et l’on prend plaisir à se plaindre. }
\livretVerse#12 { Va songe à mon hymen, et voy si tout est prest, }
\livretVerse#12 { Laisse-moy seul icy, la Déesse paraist. }

\livretScene \line { SCENE SECONDE }
\livretDescAtt\wordwrap-center {
  Cybele, Celænus, Melisse, Troupe de Prestresses de Cybele.
}
\livretPers Cybele
\livretRef #'CBBrecitJeVeuxJoindreEnCesLieux
\livretVerse#12 { Je veux joindre en ces lieux la gloire et l’abondance, }
\livretVerse#12 { D’un sacrificateur je veux faire le choix, }
\livretVerse#12 { Et le Roy de Phrygie auroit la preference }
\livretVerse#12 { Si je voulois choisir entre les plus grands Roys. }
\livretVerse#12 { Le puissant Dieux des flots vous donna la naissance, }
\livretVerse#12 { Un Peuple renommé s’est mis sous vostre loy ; }
\livretVerse#12 { Vous avez sans mon choix, d’ailleurs, trop de puissance, }
\livretVerse#12 { Je veux faire un bonheur qui ne soit dû qu’à moy. }
\livretVerse#12 { Vous estimez Atys, et c’est avec justice, }
\livretVerse#12 { Je pretens que mon choix à vos vœux soit propice, }
\livretVerse#8 { C’est Atys que je veux choisir. }
\livretPers Celænus
\livretVerse#12 { J’aime Atys, et je voy sa gloire avec plaisir. }
\livretVerse#8 { Je suis Roy, Neptune est mon pere, }
\livretVerse#12 { J’espouse une Beauté qui va combler mes vœux : }
\livretVerse#8 { Le souhait qui me reste à faire, }
\livretVerse#12 { C’est de voir mon Amy parfaitement heureux. }
\livretPers Cybele
\livretVerse#12 { Il m’est doux que mon choix à vos désirs réponde ; }
\livretVerse#8 { Une grande Divinité }
\livretVerse#8 { Doit faire sa felicité }
\livretVerse#6 { Du bien de tout le monde. }
\livretVerse#12 { Mais sur tout le bonheur d’un Roy chery des Cieux }
\livretVerse#8 { Fait le plus doux plaisir des Dieux. }
\livretPers Celænus
\livretVerse#12 { Le sang aproche Atys de la Nymphe que j’aime, }
\livretVerse#8 { Son merite l’égale aux Roys : }
\livretVerse#8 { Il soûtiendra mieux que moy-mesme }
\livretVerse#6 { La majesté supresme }
\livretVerse#6 { De vos divines loix. }
\livretVerse#8 { Rien ne pourra troubler son zele, }
\livretVerse#12 { Son cœur s’est conservé libre jusqu’à ce jour ; }
\livretVerse#8 { Il faut tout un cœur pour Cybele, }
\livretVerse#12 { A peine tout le mien peut suffire à l’Amour. }
\livretPers Cybele
\livretVerse#12 { Portez à votre Amy la premiere nouvelle }
\livretVerse#12 { De l’honneur éclatant où ma faveur l’appelle. }

\livretScene\line { SCENE TROISIESME }
\livretDescAtt\wordwrap-center { Cybele, Melisse. }
\livretPers Cybele
\livretRef#'CCArecitTuTetonnesMelisse
\livretVerse#12 { Tu tétonnes, Melisse, et mon choix te surprend ? }
\livretPers Melisse
\livretVerse#12 { Atys vous doit beaucoup, et son bonheur est grand. }
\livretPers Cybele
\livretVerse#12 { J’ay fait encor pour luy plus que tu ne peux croire. }
\livretPers Melisse
\livretVerse#12 { Est-il pour un Mortel un rang plus glorieux ? }
\livretPers Cybele
\livretVerse#8 { Tu ne vois que sa moindre gloire ; }
\livretVerse#12 { Ce Mortel dans mon cœur est au dessus des Dieux. }
\livretVerse#12 { Ce fut au jour fatal de ma derniere Feste }
\livretVerse#12 { Que de l’aimable Atys je devins la conqueste : }
\livretVerse#12 { Je partis à regret pour retourner aux Cieux, }
\livretVerse#12 { Tout m’y parut changé, rien n’y pleût à mes yeux. }
\livretVerse#7 { Je sens un plaisir extrème }
\livretVerse#7 { A revenir dans ces lieux ; }
\livretVerse#8 { Où peut-on jamais estre mieux, }
\livretVerse#8 { Qu’aux lieux où l’on voit ce qu’on aime. }
\livretPers Melisse
\livretVerse#12 { Tous les Dieux ont aimé, Cybele aime à son tour. }
\livretVerse#7 { Vous méprisiez trop l’Amour, }
\livretVerse#7 { Son nom vous sembloit étrange, }
\livretVerse#7 { A la fin il vient un jour }
\livretVerse#5 { Où l’Amour se vange. }
\livretPers Cybele
\livretVerse#12 { J’ay crû me faire un cœur maistre de tout son sort, }
\livretVerse#12 { Un cœur toûjours exempt de trouble et de tendresse. }
\livretPers Melisse
\livretVerse#5 { Vous braviez à tort }
\livretVerse#5 { L’Amour qui vous blesse ; }
\livretVerse#5 { Le cœur le plus fort }
\livretVerse#7 { A des momens de foiblesse. }
\livretVerse#12 { Mais vous pouviez aimer, et descendre moins bas. }
\livretPers Cybele
\livretVerse#12 { Non, trop d’égalité rend l’amour sans appas. }
\livretVerse#8 { Quel plus haut rang ay-je à pretendre ? }
\livretVerse#12 { Et dequoy mon pouvoir ne vient-il point à bout ? }
\livretVerse#8 { Lors qu’on est au dessus de tout, }
\livretVerse#12 { On se fait pour aimer un plaisir de descendre. }
\livretVerse#12 { Je laisse aux Dieux les biens dans le Ciel preparez, }
\livretVerse#12 { Pour Atys, pour son cœur, je quitte tout sans peine, }
\livretVerse#12 { S’il m’oblige à descendre, un doux penchant m’entraîne ; }
\livretVerse#12 { Les cœurs que le Destin à le plus separez, }
\livretVerse#12 { Sont ceux qu’Amour unit d’une plus forte chaîne. }
\livretVerse#12 { Fay venir le Sommeil ; que luy-mesme en ce jour, }
\livretVerse#8 { Prenne soin icy de conduire }
\livretVerse#8 { Les Songes qui luy font la Cour ; }
\livretVerse#8 { Atys ne sçait point mon amour, }
\livretVerse#12 { Par un moyen nouveau je pretens l’en instruire. }
\livretDidasP\line { Melisse se retire. }
\livretPers Cybele
\livretVerse#12 { Que les plus doux Zephirs, que les Peuples divers, }
\livretVerse#8 { Qui des deux bouts de l’Univers }
\livretVerse#8 { Sont venus me montrer leur zele, }
\livretVerse#8 { Celebrent la gloire immortelle }
\livretVerse#12 { Du sacrificateur dont Cybele a fait choix, }
\livretVerse#8 { Atys doit dispenser mes loix, }
\livretVerse#8 { Honorez le choix de Cybele. }

\livretScene\line { SCENE QUATRIESME }
\livretDescAtt \column {
  \justify {
    Les Zephirs paroissent dans une gloire élevée & brillante. Les
    Peuples differens qui sont venus à la feste de Cybele entrent dans
    le Temple, & tous ensemble s’efforcent d’honorer Atys, qui vient
    revestu des habits de grand Sacrificateur.
  }
  \justify\italic {
    Cinq Zephirs dançans dans la Gloire.  Huit Zephirs joüants du
    Haut-bois & des Cromornes, dans la Gloire.  Troupe de Peuples
    differens chantans qui accompagnent Atys.  Six Indiens & six
    Egiptiens dançans.
  }
}
\livretPers\line { Chœurs des Peuples & des Zephirs. }
\livretRef #'CDAchoeurCelebronsLaGloireImmortelle
\livretVerse#8 { Celebrons la gloire immortelle }
\livretVerse#12 { Du Sacrificateur dont Cybele a fait choix : }
\livretVerse#8 { Atys doit dispenser ses loix, }
\livretVerse#8 { Honorons le choix de Cybele. }
\livretRef #'CDDchoeurQueDevantVous
\livretVerse#10 { Que devant Vous tout s’abaisse, et tout tremble ; }
\livretVerse#10 { Vivez heureux, vos jours sont nostre espoir : }
\livretVerse#9 { Rien n’est si beau que de voir ensemble }
\livretVerse#10 { Un grand merite avec un grand pouvoir. }
\livretVerse#4 { Que l’on benisse }
\livretVerse#4 { Le Ciel propice, }
\livretVerse#4 { Qui dans vos mains }
\livretVerse#6 { Met le sort des Humains. }
\livretPers Atys
\livretRef #'CDEindigneQueJeSuis
\livretVerse#12 { Indigne que je suis des honneurs qu’on m’adresse, }
\livretVerse#12 { Je dois les recevoir au nom de la Déesse ; }
\livretVerse#12 { J’ose, puis qu’il luy plaist, luy presenter vos vœux : }
\livretVerse#7 { Pour le prix de vostre zele, }
\livretVerse#7 { Que la puissante Cybele }
\livretVerse#7 { Vous rende à jamais heureux. }
\livretPers\line { Chœurs des Peuples & des Zephirs. }
\livretRef #'CDFchoeurQueLaPuissanteCybele
\livretVerse#7 { Que la puissante Cybele }
\livretVerse#7 { Nous rende à jamais heureux. }
\sep
\livretAct\line { ACTE TROISIESME }
\livretDescAtt\wordwrap-center {
  \line { Le Theatre change & represente }
  \line { le Palais du Sacrificateur de Cybele. }
}
\livretScene\line { SCENE PREMIERE }
\livretPersDidas Atys seul
\livretRef#'DAArecitQueServentLesFaveurs
\livretVerse#12 { Que servent les faveurs que nous fait la Fortune }
\livretVerse#8 { Quand l’Amour nous rend malheureux ? }
\livretVerse#12 { Je pers l’unique bien qui peut combler mes vœux, }
\livretVerse#8 { Et tout autre bien m’importune. }
\livretVerse#12 { Que servent les faveurs que nous fait la Fortune }
\livretVerse#8 { Quand l’Amour nous rend malheureux ? }

\livretScene\line { SCENE SECONDE }
\livretDescAtt\wordwrap-center { Idas, Doris, Atys. }
\livretPers Idas
\livretRef #'DBArecitPeutOnIciParlerSansFeindre
\livretVerse#8 { Peut-on icy parler sans feindre ? }
\livretPers Atys
\livretVerse#12 { Je commande en ces lieux, vous n’y devez rien craindre. }
\livretPers Doris
\livretVerse#12 { Mon frere est votre amy. }
\livretPers Idas
\livretVerse#12 { \transparent { Mon frere est votre amy. } Fiez-vous à ma sœur. }
\livretPers Atys
\livretVerse#12 { Vous devez avec moy partager mon bon-heur. }
\livretPers\line { Idas, & Doris }
\livretVerse#12 { Nous venons partager vos mortelles allarmes ; }
\livretVerse#8 { Sangaride les yeux en larmes }
\livretVerse#6 { Nous vient d’ouvrir son cœur. }
\livretPers Atys
\livretVerse#13 { L’heure aproche où l’Hymen voudra qu’elle se livre }
\livretVerse#8 { Au pouvoir d’un heureux espoux. }
\livretPers\line { Idas, & Doris }
\livretVerse#5 { Elle ne peut vivre }
\livretVerse#7 { Pour un autre que pour vous. }
\livretPers Atys
\livretVerse#12 { Qui peut la dégager du devoir qui la presse ? }
\livretPers\line { Idas, & Doris }
\livretVerse#12 { Elle veut elle mesme aux pieds de la Deesse }
\livretVerse#12 { Declarer hautement vos secretes amours. }
\livretPers Atys
\livretVerse#8 { Cybele pour moy s’interesse, }
\livretVerse#12 { J’ose tout esperer de son divin secours… }
\livretVerse#12 { Mais quoy, trahir le Roy ! tromper son esperance ! }
\livretVerse#12 { De tant de biens receus est-ce la recompense ? }
\livretPers\line { Idas, & Doris }
\livretVerse#6 { Dans l’Empire amoureux }
\livretVerse#8 { Le Devoir n’a point de puissance ; }
\livretVerse#4 { L’Amour dispence }
\livretVerse#8 { Les Rivaux d’estre genereux ; }
\livretVerse#10 { Il faut souvent pour devenir heureux }
\livretVerse#8 { Qu’il en couste un peu d’innocence. }
\livretPers Atys
\livretVerse#12 { Je souhaite, je crains, je veux, je me repens. }
\livretPers\line { Idas, & Doris }
\livretVerse#12 { Verrez-vous un rival heureux à vos dépens ? }
\livretPers Atys
\livretVerse#12 { Je ne puis me resoudre à cette violence. }
\livretPers\line { Atys, Idas, & Doris }
\livretVerse#10 { En vain, un cœur, incertain de son choix, }
\livretVerse#8 { Met en balance mille fois }
\livretVerse#8 { L’Amour et la Reconnoissance, }
\livretVerse#10 { L’Amour toûjours emporte la balance. }
\livretPers Atys
\livretVerse#12 { Le plus juste party cede enfin au plus fort. }
\livretVerse#8 { Allez, prenez soin de mon sort, }
\livretVerse#12 { Que Sangaride icy se rende en diligence. }

\livretScene\line { SCENE TROISIESME }
\livretPersDidas Atys seul
\livretRef #'DCBrecitNousPouvonsNousFlatter
\livretVerse#12 { Nous pouvons nous flater de l’espoir le plus doux }
\livretVerse#8 { Cybele et l’Amour sont pour nous. }
\livretVerse#12 { Mais du Devoir trahi j’entends la voix pressante }
\livretVerse#8 { Qui m’accuse et qui m’épouvante. }
\livretVerse#12 { Laisse mon cœur en paix, impuissante Vertu, }
\livretVerse#8 { N’ay-je point assez combatu ? }
\livretVerse#12 { Quand l’Amour malgré toy me contraint à me rendre, }
\livretVerse#6 { Que me demandes-tu ? }
\livretVerse#8 { Puisque tu ne peux me deffendre, }
\livretVerse#6 { Que me sert-il d’entendre }
\livretVerse#8 { Les vains reproches que tu fais ? }
\livretVerse#12 { Impuissante Vertu laisse mon cœur en paix. }
\livretVerse#8 { Mais le Sommeil vient me surprendre, }
\livretVerse#12 { Je combats vainement sa charmante douceur. }
\livretVerse#6 { Il faut laisser suspendre }
\livretVerse#6 { Les troubles de mon cœur. }
\livretDidasP\line { Atys descend. }

\livretScene\line { SCENE QUATRIESME }
\livretDescAtt\justify {
  Le Theatre change & represente un Antre entouré de Pavots & de
  Ruisseaux, où le Dieux du Sommeil se vient rendre accompagné des
  Songes agreables & funestes.
}
\livretDescAtt\center-column {
  \wordwrap-center {
    Atys dormant. Le Sommeil, Morphée, Phobetor, Phantase,
    "Les Songes heureux."
    "Les Songes funestes."
  }
  \smaller\wordwrap-center\italic {
    "Deux Songes joüants de la Violle."
    "Deux Songes joüants du Theorbe."
    "Six Songes joüants de la Flutte."
    "Douze Songes funestes chantants."
    "Seize Songes agreables & funestes dançans."
    "Huit Songes funestes dançants."
  }
}
\livretPers "Le Sommeil"
\livretRef#'DDAdormons
\livretVerse#5 { Dormons, dormons tous ; }
\livretVerse#7 { Ah que le repos est doux ! }
\livretPers Morphée
\livretVerse#12 { Regnez, divin Sommeil, regnez sur tout le monde, }
\livretVerse#12 { Répandez vos pavots les plus assoupissans ; }
\livretVerse#8 { Calmez les soins, charmez les sens, }
\livretVerse#12 { Retenez tous les cœurs dans une paix profonde. }
\livretPers Phobetor
\livretVerse#8 { Ne vous faites point violence, }
\livretVerse#8 { Coulez, murmurez, clairs Ruisseaux, }
\livretVerse#8 { Il n’est permis qu’au bruit des eaux }
\livretVerse#12 { De troubler la douceur d’un si charmant silence. }
\livretPers\wordwrap { Le Sommeil, Morphée, Phobetor & Phantase }
\livretVerse#5 { Dormons, dormons tous, }
\livretVerse#7 { Ah que le repos est doux ! }
\livretDidasP\justify {
  Les Songes agreables aprochent d'Atys, & par leurs chants, & par
  leurs dances, luy font connoistre l'amour de Cybele, & le bonheur
  qu'il en doit esperer.
}
\livretPers Morphée
\livretRef #'DDBsommeil
\livretVerse#12 { Escoute, escoute Atys la gloire qui t’appelle, }
\livretVerse#12 { Sois sensible à l’honneur d’estre aymé de Cybelle, }
\livretVerse#12 { Joüis heureux Atys de ta felicité. }
\livretPers\line { Morphée, Phobetor & Phantase }
\livretVerse#8 { Mais souvien-toy que la Beauté }
\livretVerse#6 { Quand elle est immortelle, }
\livretVerse#8 { Demande la fidelité }
\livretVerse#6 { D’une amour éternelle. }
\livretPers Phantase
\livretVerse#6 { Que l’Amour a d’attraits }
\livretVerse#4 { Lors qu’il commence }
\livretVerse#8 { A faire sentir sa puissance, }
\livretVerse#6 { Que l’Amour a d’attraits }
\livretVerse#4 { Lors qu’il commence }
\livretVerse#6 { Pour ne finir jamais. }
  \null
\livretVerse#6 { Trop heureux un Amant }
\livretVerse#4 { Qu’amour exemte }
\livretVerse#8 { Des peines d’une longue attente ! }
\livretVerse#6 { Trop heureux un Amant }
\livretVerse#4 { Qu’amour exemte }
\livretVerse#6 { De crainte, et de tourment ! }
\livretPers Morphée
\livretRef #'DDDsommeil
\livretVerse#12 { Gouste en paix chaque jour une douceur nouvelle, }
\livretVerse#12 { Partage l’heureux sort d’une Divinité, }
\livretVerse#8 { Ne vante plus la liberté, }
\livretVerse#12 { Il n’en est point du prix d’une chaîne si belle : }
\livretPers\line { Morphée, Phobetor & Phantase }
\livretVerse#8 { Mais souvien-toy que la Beauté }
\livretVerse#6 { Quand elle est immortelle, }
\livretVerse#8 { Demande la fidelité }
\livretVerse#6 { D’une amour éternelle. }
\livretPers Phantase
\livretVerse#6 { Que l’Amour a d’attraits }
\livretVerse#4 { Lors qu’il commence }
\livretVerse#8 { A faire sentir sa puissance, }
\livretVerse#6 { Que l’Amour a d’attraits }
\livretVerse#4 { Lors qu’il commence }
\livretVerse#6 { Pour ne finir jamais. }
\livretDidasP\justify {
  Les songes funestes approchent d'Atys, & le menacent de la vengeance
  de Cybele s'il mesprise son amour, & s'il ne l'ayme pas avec
  fidelité.
}
\livretPers\line { Un Songe funeste }
\livretRef #'DDFfuneste
\livretVerse#12 { Garde-toy d’offencer un amour glorieux, }
\livretVerse#12 { C’est pour toy que Cybele abandonne les Cieux }
\livretVerse#8 { Ne trahis point son esperance. }
\livretVerse#12 { Il n’est point pour les Dieux de mespris innocent, }
\livretVerse#12 { Ils sont jaloux des Cœurs, ils ayment la vengeance, }
\livretVerse#8 { Il est dangereux qu’on offence }
\livretVerse#6 { Un amour tout-puissant. }
\livretPers\line { Chœur de Songes funestes }
\livretRef #'DDHchoeur
\livretVerse#5 { L’amour qu’on outrage }
\livretVerse#5 { Se transforme en rage, }
\livretVerse#6 { Et ne pardonne pas }
\livretVerse#6 { Aux plus charmants appas. }
\livretVerse#7 { Si tu n’aymes point Cybele }
\livretVerse#5 { D’une amour fidelle, }
\livretVerse#8 { Malheureux, que tu souffriras ! }
\livretVerse#4 { Tu periras : }
\livretVerse#8 { Crains une vengeance cruelle, }
\livretVerse#8 { Tremble, crains un affreux trépas. }
\livretDidasP\justify {
  Atys espouvanté par les Songes funestes, se resveille en sursaut, le
  Sommeil & les Songes disparoissent avec l'Antre où ils estoient, &
  Atys se retrouve dans le mesme Palais où il s'estoit endormy.
}

\livretScene\line { SCENE CINQUIESME }
\livretDescAtt\wordwrap-center { Atys, Cybele, & Melisse. }
\livretPers Atys
\livretRef #'DEAcybeleAtys
\livretVerse#12 { Venez à mon secours ô Dieux ! ô justes Dieux ! }
\livretPers Cybele
\livretVerse#12 { Atys, ne craignez rien, Cybele, est en ces lieux. }
\livretPers Atys
\livretVerse#12 { Pardonnez au desordre où mon cœur s’abandonne ; }
\livretVerse#12 { C’est un songe… }
\livretPers Cybele
\livretVerse#12 { \transparent { C’est un songe… } Parlez, quel songe vous estonne ? }
\livretVerse#8 { Expliquez moy vostre embaras. }
\livretPers Atys
\livretVerse#12 { Les songes sont trompeurs, et je ne les croy pas. }
\livretVerse#6 { Les plaisirs et les peines }
\livretVerse#8 { Dont en dormant on est séduit, }
\livretVerse#6 { Sont des chimeres vaines }
\livretVerse#6 { Que le resveil détruit. }
\livretPers Cybele
\livretVerse#8 { Ne mesprisez pas tant les songes }
\livretVerse#10 { L’Amour peut souvent emprunter leur voix, }
\livretVerse#7 { S’ils font souvent des mensonges }
\livretVerse#7 { Ils disent vray quelquefois. }
\livretVerse#12 { Ils parloient par mon ordre, et vous les devez croire. }
\livretPers Atys
\livretVerse#12 { O Ciel ? }
\livretPers Cybele
\livretVerse#12 { \transparent { O Ciel ? } N’en doutez point, connoissez vostre gloire. }
\livretVerse#8 { Respondez avec liberté, }
\livretVerse#12 { Je vous demande un cœur qui despend de luy-mesme. }
\livretPers Atys
\livretVerse#8 { Une grande Divinité }
\livretVerse#12 { Doit d’assûrer toûjours de mon respect extresme. }
\livretPers Cybele
\livretVerse#8 { Les Dieux dans leur grandeur supresme }
\livretVerse#12 { Reçoivent tant d’honneurs qu’ils en sont rebutez, }
\livretVerse#12 { Ils se lassent souvent d’estre trop respectez, }
\livretVerse#8 { Ils sont plus contents qu’on les ayme. }
\livretPers Atys
\livretVerse#8 { Je sçay trop ce que je vous doy }
\livretVerse#8 { Pour manquer de reconnoissance… }

\livretScene\line { SCENE SIXIESME }
\livretDescAtt\wordwrap-center { Sangaride, Cybele, Atys, Melisse. }
\livretPersDidas Sangaride "se jettant aux pieds de Cybele"
\livretRef#'DFAcybeleAtysSangaride
\livretVerse#8 { J’ay recours à vostre puissance, }
\livretVerse#8 { Reyne des Dieux, protegez-moy. }
\livretVerse#8 { L’interest d’Atys vous en presse… }
\livretPersDidas Atys "interrompant Sangaride"
\livretVerse#12 { Je parleray pour vous, que vostre crainte cesse. }
\livretPers Sangaride
\livretVerse#8 { Tous deux unis des plus beaux nœuds… }
\livretPersDidas Atys "interrompant Sangaride"
\livretVerse#12 { Le sang et l’amitié nous unissent tous deux. }
\livretVerse#8 { Que vostre secours la délivre }
\livretVerse#8 { Des loix d’un Hymen rigoureux, }
\livretVerse#8 { Ce sont les plus doux de ses vœux }
\livretVerse#12 { De pouvoir à jamais vous servir et vous suivre. }
\livretPers Cybele
\livretVerse#7 { Les Dieux sont les protecteurs }
\livretVerse#7 { De la liberté des cœurs. }
\livretVerse#12 { Allez, ne craignez point le Roy ny sa colere, }
\livretVerse#6 { J’auray soin d’appaiser }
\livretVerse#8 { Le Fleuve Sangar vostre Pere ; }
\livretVerse#8 { Atys veut vous favoriser, }
\livretVerse#12 { Cybele en sa faveur ne peut rien refuser. }
\livretPers Atys
\livretVerse#12 { Ah ! c’en est trop… }
\livretPers Cybele
\livretVerse#12 { \transparent { Ah ! c’en est trop… } Non, non, il n’est pas necessaire }
\livretVerse#8 { Que vous cachiez vostre bonheur, }
\livretVerse#6 { Je ne prétens point faire }
\livretVerse#4 { Un vain mystere }
\livretVerse#8 { D’un amour qui vous fait honneur. }
\livretVerse#12 { Ce n’est point à Cybelle à craindre d’en trop dire. }
\livretVerse#12 { Il est vray, j’ayme Atys, pour luy j’ay tout quitté, }
\livretVerse#12 { Sans luy je ne veux plus de grandeur ny d’Empire, }
\livretVerse#6 { Pour ma felicité }
\livretVerse#6 { Son cœur seul peut suffire. }
\livretVerse#12 { Allez, Atys luy-mesme ira vous garentir }
\livretVerse#8 { De la fatale violence }
\livretVerse#8 { Où vous ne pouvez consentir. }
\livretDidasP\line { Sangaride se retire. }
\livretPersDidas Cybele "parle à Atys"
\livretVerse#12 { Laissez-nous, attendez mes ordres pour partir, }
\livretVerse#12 { Je prétens vous armer de ma toute-puissance. }

\livretScene\line { SCENE SEPTIESME }
\livretDescAtt\wordwrap-center { Cybele, Melisse. }
\livretPers Cybele
\livretRef #'DGAcybeleMelisse
\livretVerse#12 { Qu’Atys dans ses respects mesle d’indifference ! }
\livretVerse#8 { L’Ingrat Atys ne m’ayme pas ; }
\livretVerse#12 { L’Amour veut de l’amour, tout autre prix l’offence, }
\livretVerse#12 { Et souvent le respect et la reconnoissance }
\livretVerse#8 { Sont l’excuse des cœurs ingrats. }
\livretPers Melisse
\livretVerse#7 { Ce n’est pas un si grand crime }
\livretVerse#7 { De ne s’exprimer pas bien, }
\livretVerse#8 { Un cœur qui n’ayma jamais rien }
\livretVerse#8 { Sçait peu comment l’amour s’exprime. }
\livretPers Cybele
\livretVerse#12 { Sangaride est aymable, Atys peut tout charmer, }
\livretVerse#8 { Ils tesmoignent trop s’estimer, }
\livretVerse#12 { Et de simples parents sont moins d’intelligence : }
\livretVerse#8 { Ils se sont aymez dès l’enfance, }
\livretVerse#8 { Ils pourroient enfin trop s’aymer. }
\livretVerse#12 { Je crains une amitié que tant d’ardeur anime. }
\livretVerse#8 { Rien n’est si trompeur que l’estime : }
\livretVerse#6 { C’est un nom supposé }
\livretVerse#12 { Qu’on donne quelquefois à l’amour desguisé. }
\livretVerse#12 { Je prétens m’esclaircir leur feinte sera vaine. }
\livretPers Melisse
\livretVerse#12 { Quels secrets par les Dieux ne sont point penetrez ? }
\livretVerse#8 { Deux cœurs à feindre preparez }
\livretVerse#6 { Ont beau cacher leur chaîne, }
\livretVerse#6 { On abuse avec peine }
\livretVerse#8 { Les Dieux par l’Amour esclairez. }
\livretPers Cybele
\livretVerse#12 { Va, Melisse, donne ordre à l’aymable Zephire }
\livretVerse#12 { D’accomplir promptement tout ce qu’Atys desire. }

\livretScene\line { SCENE HUITIESME }
\livretPersDidas Cybele seule
\livretRef#'DHAcybele
\livretVerse#7 { Espoir si cher, et si doux, }
\livretVerse#7 { Ah ! pourquoy me trompez-vous ? }
\livretVerse#12 { Des suprêmes grandeurs vous m’avez fait descendre, }
\livretVerse#12 { Mille Cœurs m’adoroient, je les neglige tous, }
\livretVerse#12 { Je n’en demande qu’un, il a peine à se rendre ; }
\livretVerse#12 { Je ne sens que chagrin, et que soupçons jaloux ; }
\livretVerse#12 { Est-ce le sort charmant que je devois attendre ? }
\livretVerse#7 { Espoir si cher, et si doux, }
\livretVerse#7 { Ah ! pourquoy me trompez-vous ? }
\livretVerse#12 { Helas ! par tant d’attraits falloit-il me surprendre ? }
\livretVerse#12 { Heureuse, si toûjours j’avois pû m’en deffendre ! }
\livretVerse#12 { L’Amour qui me flattoit me cachoit son couroux : }
\livretVerse#12 { C’est donc pour me fraper des plus funestes coups, }
\livretVerse#12 { Que le cruel Amour m’a fait un cœur si tendre ? }
\livretVerse#7 { Espoir si cher, et si doux, }
\livretVerse#7 { Ah ! pourquoy me trompez-vous ? }
\sep
\livretAct \line { ACTE QUATRIESME }
\livretDescAtt\wordwrap-center {
  Le Theatre change & represente le palais du Fleuve Sangar.
}
\livretScene\line { SCENE PREMIERE }
\livretDescAtt\wordwrap-center { Sangaride, Doris, Idas. }
\livretPers Doris
\livretRef#'EAArecitQuoiVousPleurez
\livretVerse#12 { Quoy, vous pleurez ? }
\livretPers Idas
\livretVerse#12 { \transparent { Quoy, vous pleurez ? } D’où vient vostre peine nouvelle ? }
\livretPers Doris
\livretVerse#12 { N’osez-vous découvrir vostre amour à Cybele ? }
\livretPers Sangaride
\livretVerse#12 { Helas ! }
\livretPers\line { Doris & Idas }
\livretVerse#12 { \transparent { Helas ! } Qui peut encor redoubler vos ennuis ? }
\livretPers Sangaride
\livretVerse#14 { Helas ! j’aime… helas ! j’aime… }
\livretPers\line { Doris & Idas }
\livretVerse#14 { \transparent { Helas ! j’aime… helas ! j’aime… } Achevez. }
\livretPers Sangaride
\livretVerse#14 { \transparent { Helas ! j’aime… helas ! j’aime… Achevez. } Je ne puis. }
\livretPers \line { Doris & Idas }
\livretVerse#12 { L’Amour n’est guere heureux lorsqu’il est trop timide. }
\livretPers Sangaride
\livretVerse#6 { Helas ! j’aime un perfide }
\livretVerse#6 { Qui trahit mon amour ; }
\livretVerse#12 { Le Deesse aime Atys, il change en moins d’un jour, }
\livretVerse#12 { Atys comblé d’honneurs n’aime plus Sangaride. }
\livretVerse#6 { Helas ! j’aime un perfide }
\livretVerse#6 { Qui trahit mon amour. }
\livretPers\line { Doris & Idas }
\livretVerse#12 { Il nous montroit tantost un peu d’incertitude ; }
\livretVerse#12 { Mais qui l’eust soupçonné de tant d’ingratitude ? }
\livretPers Sangaride
\livretVerse#12 { J’embarassois Atys, je l’ay veu se troubler : }
\livretVerse#8 { Je croyois devoir reveler }
\livretVerse#6 { Nostre amour à Cybele ; }
\livretVerse#6 { Mais l’Ingrat, l’Infidelle, }
\livretVerse#8 { M’empéchoit toûjours de parler. }
\livretPers\line { Doris & Idas }
\livretVerse#12 { Peut-on changer si-tost quand l’Amour est extrême ? }
\livretVerse#6 { Gardez-vous, gardez-vous }
\livretVerse#8 { De trop croire un transport jaloux. }
\livretPers Sangaride
\livretVerse#12 { Cybele hautement declare qu’elle l’aime, }
\livretVerse#12 { Et l’Ingrat n’a trouvé cét honneur que trop doux ; }
\livretVerse#12 { Il change en un moment, je veux changer de mesme, }
\livretVerse#12 { J’accepteray sans peine un glorieux espoux, }
\livretVerse#12 { Je ne veux plus aimer que la grandeur supresme. }
\livretPers\line { Doris & Idas }
\livretVerse#12 { Peut-on changer si-tost quand l’Amour est extrême ? }
\livretVerse#6 { Gardez-vous, gardez-vous }
\livretVerse#8 { De trop croire un transport jaloux. }
\livretPers Sangaride
\livretVerse#8 { Trop heureux un cœur qui peut croire }
\livretVerse#8 { Un dépit qui sert à sa gloire. }
\livretVerse#12 { Revenez ma Raison, revenez pour jamais, }
\livretVerse#12 { Joignez-vous au Dépit pour estouffer ma flâme, }
\livretVerse#12 { Reparez, s’il se peut, les maux qu’Amour m’a faits, }
\livretVerse#8 { Venez restablir dans mon ame }
\livretVerse#8 { Les douceurs d’une heureuse paix ; }
\livretVerse#12 { Revenez, ma Raison, revenez pour jamais. }
\livretPers\line { Idas & Doris }
\livretVerse#8 { Une infidelité cruelle }
\livretVerse#8 { N’efface point tous les appas }
\livretVerse#4 { D’un infidelle, }
\livretVerse#8 { Et la Raison ne revient pas }
\livretVerse#6 { Si-tost qu’on l’a rappelle. }
\livretPers Sangaride
\livretVerse#7 { Après une trahison }
\livretVerse#7 { Si la raison ne m’éclaire, }
\livretVerse#7 { Le dépit et la colere }
\livretVerse#7 { Me tiendront lieu de raison. }
\livretPers \line { Sangaride, Doris, Idas. }
\livretVerse#8 { Qu’une premiere amour est belle ? }
\livretVerse#8 { Qu’on a peine à s’en dégager ! }
\livretVerse#8 { Que l’on doit plaindre un cœur fidelle }
\livretVerse#8 { Lorsqu’il est forcé de changer. }

\livretScene\line { SCENE SECONDE }
\livretDescAtt\wordwrap-center {
  Celænus, suivans de Celænus, Sangaride, Idas, & Doris.
}
\livretPers Celænus
\livretRef#'EBBbelleNymphe
\livretVerse#12 { Belle Nymphe, l’Hymen va suivre mon envie, }
\livretVerse#8 { L’Amour avec moy vous convie }
\livretVerse#12 { A venir vous placer sur un Thrône éclatant, }
\livretVerse#12 { J’aproche avec transport du favorable instant }
\livretVerse#12 { D’où despend la douceur du reste de ma vie : }
\livretVerse#12 { Malgré tous les transports de mon ame amoureuse, }
\livretVerse#8 { Si je ne puis vous rendre heureuse, }
\livretVerse#8 { Je ne seray jamais content. }
\livretVerse#8 { Je fais mon bonheur de vous plaire, }
\livretVerse#12 { J’attache à vostre cœur mes desirs les plus doux. }
\livretPers Sangaride
\livretVerse#12 { Seigneur, j’obeïray, je despens de mon Pere, }
\livretVerse#12 { Et mon Pere aujourd’huy veut que je sois à vous. }
\livretPers Celænus
\livretVerse#12 { Regardez mon amour, plustost que ma Couronne. }
\livretPers Sangaride
\livretVerse#12 { Ce n’est point la grandeur qui me peut esbloüir. }
\livretPers Celænus
\livretVerse#12 { Ne sçauriez-vous m’aimer sans que l’on vous l’ordonne. }
\livretPers Sangaride
\livretVerse#12 { Seigneur, contentez-vous que je sçache obeïr, }
\livretVerse#12 { En l’estat où je suis c’est ce que je puis dire… }

\livretScene\line { SCENE TROISIESME }
\livretDescAtt \wordwrap-center {
  Atys, Celænus, Sangaride, Doris, Idas, Suivans de Celænus.
}
\livretPers Celænus
\livretRef#'ECArecitVotreCoeurSeTrouble
\livretVerse#8 { Vostre cœur se trouble, il soûpire. }
\livretPers Sangaride
\livretVerse#8 { Expliquez en vostre faveur }
\livretVerse#12 { Tout ce que vous voyez de trouble dans mon cœur. }
\livretPers Celænus
\livretVerse#12 { Rien ne m’allarme plus, Atys, ma crainte est vaine, }
\livretVerse#12 { Mon amour touche enfin le cœur de la Beauté }
\livretVerse#6 { Dont je suis enchanté : }
\livretVerse#9 { Toy qui fûs le tesmoin de ma peine, }
\livretVerse#12 { Cher Atys, sois tesmoin de ma felicité. }
\livretVerse#12 { Peux-tu la concevoir ? non, il faut que l’on aime, }
\livretVerse#12 { Pour juger des douceurs de mon bonheur extresme. }
\livretVerse#8 { Mais, prés de voir combler mes vœux, }
\livretVerse#12 { Que les moments sont longs pour mon cœur amoureux ! }
\livretVerse#12 { Vos parents tardent trop, je veux aller moy-mesme }
\livretVerse#8 { Les presser de me rendre heureux. }

\livretScene\line { SCENE QUATRIESME }
\livretDescAtt\wordwrap-center { Atys, Sangaride. }
\livretPers Atys
\livretRef#'EDBrecitQuilSaitPeuSonMalheur
\livretVerse#12 { Qu’il sçait peu son malheur ! et qu’il est déplorable ! }
\livretVerse#12 { Son amour meritoit un sort plus favorable : }
\livretVerse#12 { J’ay pitié de l’erreur dont son cœur s’est flatté. }
\livretPers Sangaride
\livretVerse#12 { Espargnez-vous le soin d’estre si pitoyable, }
\livretVerse#12 { Son amour obtiendra ce qu’il a merité. }
\livretPers Atys
\livretVerse#12 { Dieux ! qu’est-ce que j’entends ! }
\livretPers Sangaride
\livretVerse#12 { \transparent { Dieux ! qu’est-ce que j’entends ! } Qu’il faut que je me vange, }
\livretVerse#12 { Que j’aime enfin le Roy, qu’il sera mon espoux. }
\livretPers Atys
\livretVerse#12 { Sangaride, eh d’où vient ce changement estrange ? }
\livretPers Sangaride
\livretVerse#12 { N’est-ce pas vous ingrat qui voulez que je change ? }
\livretPers Atys
\livretVerse#12 { Moy ! }
\livretPers Sangaride
\livretVerse#12 { \transparent { Moy ! } Quelle trahison ! }
\livretPers Atys
\livretVerse#12 { \transparent { Moy ! Quelle trahison ! } Quel funeste couroux ! }
\livretPers\line { Atys & Sangaride }
\livretVerse#12 { Pourquoy m’abandonner pour une amour nouvelle ? }
\livretVerse#12 { Ce n’est pas moy qui rompt une chaisne si belle. }
\livretPers Atys
\livretVerse#8 { Beauté trop cruelle, c’est vous, }
\livretPers Sangaride
\livretVerse#8 { Amant infidelle, c’est vous, }
\livretPers Atys
\livretVerse#8 { Ah ! c’est vous, Beauté trop cruelle, }
\livretPers Sangaride
\livretVerse#8 { Ah ! c’est vous Amant infidelle. }
\livretPers \line { Atys & Sangaride }
\livretVerse#8 { Beauté trop cruelle, c’est vous, }
\livretVerse#8 { Amant infidelle, c’est vous, }
\livretVerse#8 { Qui rompez des liens si doux. }
\livretPers Sangaride
\livretVerse#11 { Vous m’avez immolée à l’amour de Cybele. }
\livretPers Atys
\livretVerse#12 { Il est vray qu’à ses yeux, par un secret effroy, }
\livretVerse#12 { J’ay voulu de nos cœurs cacher l’intelligence : }
\livretVerse#12 { Mais ce n’est que pour vous que j’ay crain sa vengeance, }
\livretVerse#8 { Et je ne la crains pas pour moy. }
\livretVerse#12 { Cybele m’ayme en vain, et c’est vous que j’adore. }
\livretPers Sangaride
\livretVerse#8 { Aprés vostre infidelité, }
\livretVerse#8 { Auriez-vous bien la cruauté }
\livretVerse#8 { De vouloir me tromper encore ? }
\livretPers Atys
\livretVerse#8 { Moy ! vous trahir ? vous le pensez ? }
\livretVerse#8 { Ingrate, que vous m’offencez ! }
\livretVerse#8 { Hé bien, il ne faut plus rien taire, }
\livretVerse#12 { Je vais de la Déesse attirer la colere, }
\livretVerse#12 { M’offrir à sa fureur, puisque vous m’y forcez… }
\livretPers Sangaride
\livretVerse#12 { Ah ! demeurez, Atys, mes soupçons sont passez ; }
\livretVerse#12 { Vous m’aimez, je le croy, j’en veux estre certaine. }
\livretVerse#6 { Je le souhaite assez, }
\livretVerse#6 { Pour le croire sans peine. }
\livretPers Atys
\livretVerse#6 { Je jure, }
\livretPers Sangaride
\livretVerse#6 { \transparent { Je jure, } Je promets, }
\livretPers\line { Atys & Sangaride }
\livretVerse#6 { De ne changer jamais. }
\livretPers Sangaride
\livretVerse#12 { Quel tourment de cacher une si belle flame. }
\livretPers Atys
\livretVerse#12 { Redoublons-en l’ardeur dans le fonds de nostre ame. }
\livretPers\line { Atys & Sangaride }
\livretVerse#8 { Aimons en secret, aimons-nous : }
\livretVerse#12 { Aimons plusque jamais, en dépit des Jaloux. }
\livretPers Sangaride
\livretVerse#12 { Mon père vient icy, }
\livretPers Atys
\livretVerse#12 { \transparent { Mon père vient icy, } Que rien ne vous estonne ; }
\livretVerse#12 { Servons-nous du pouvoir que Cybele me donne, }
\livretVerse#8 { Je vais preparer les Zephirs }
\livretVerse#6 { A suivre nos desirs. }

\livretScene\line { SCENE CINQUIESME }
\livretDesc\column {
  \wordwrap-center {
    Sangaride, Celænus, le Dieux du Fleuve Sangar,
    Troupe de Dieux de Fleuves, de Ruisseaux,
    et de Divinitez de Fontaines.
  }
  \smaller\wordwrap-center\italic {
    "Le Fleuve Sangar."
    "Suite du Fleuve Sangar."
    "Douze grands Dieux de Fleuves chantants."
    "Cinq Dieux de Fleuves joüans de la Flutte."
    "Quatre divinitez de fontaines, et quatre Dieux"
    "de Fleuves chantants et dançants."
    "Quatre Divinitéz de Fontaines."
    "Deux Dieux de Fleuves."
    "Deux Dieux de Fleuves dançants ensemble."
    "Deux petits Dieux de Ruisseaux chantants et dançants."
    "Quatre petits Dieux de Ruisseaux dançants."
    "Six grands Dieux de Fleuves dançants."
    "Deux vieux Dieux de Fleuves"
    "& deux vieilles Nymphes de Fontaines dançantes."
  }
}
\livretPers\line { Le Dieu du Fleuve Sangar }
\livretRef#'EEBOVousQuiPrenezPart
\livretVerse#12 { O vous, qui prenez part au bien de ma famille, }
\livretVerse#12 { Vous, venerables Dieux des Fleuves les plus grands, }
\livretVerse#12 { Mes fidelles Amis, et mes plus chers Parents, }
\livretVerse#12 { Voyez quel est l’Espoux que je donne à ma fille : }
\livretVerse#12 { J’ay pris soin de choisir entre les plus grands Roys. }
\livretPers\line { Chœur de Dieux de Fleuves }
\livretVerse#7 { Nous aprouvons vostre choix. }
\livretPers\line { Le Dieu du Fleuve Sangar }
\livretVerse#8 { Il a Neptune pour son Pere, }
\livretVerse#8 { Les Phrygiens suivent ses Loix ; }
\livretVerse#6 { J’ay crû ne pouvoir faire }
\livretVerse#8 { Un choix plus digne de vous plaire. }
\livretPers\line { Chœur de Dieux de Fleuves }
\livretVerse#7 { Tous, d’une commune voix, }
\livretVerse#7 { Nous aprouvons vostre choix. }
\livretPers\line { Le Dieu du Fleuve Sangar }
\livretRef#'EECairQueLonChante
\livretVerse#7 { Que l’on chante, que l’on dance, }
\livretVerse#7 { Rions tous lors qu’il le faut ; }
\livretVerse#6 { Ce n’est jamais trop tost }
\livretVerse#6 { Que le plaisir commence. }
\livretVerse#7 { On trouve bien-tost la fin }
\livretVerse#7 { Des jours de réjoüissance, }
\livretVerse#8 { On a beau chasser le chagrin, }
\livretVerse#8 { Il revient plustost qu’on ne pense. }
\livretPers\line { Le Dieu du Fleuve Sangar, & le Chœur }
\livretRef#'EECchoeurQueLonChante
\livretVerse#7 { Que l’on chante, que l’on dance, }
\livretVerse#7 { Rions tous lors qu’il le faut ; }
\livretVerse#6 { Ce n’est jamais trop tost }
\livretVerse#6 { Que le plaisir commence. }
\livretVerse#7 { Que l’on chante, que l’on dance, }
\livretVerse#7 { Rions tous lors qu’il le faut. }
\livretPersDidas "Dieux de Fleuves, Divinitez de Fontaines, & de Ruisseaux," "chantants & dançants ensemble."
\livretRef#'EEDairLaBeauteLaPlusSevere
\livretVerse#7 { La Beauté la plus severe }
\livretVerse#7 { Prend pitié d’un long tourment, }
\livretVerse#7 { Et l’Amant qui persevere }
\livretVerse#7 { Devient un heureux Amant. }
\livretVerse#7 { Tout est doux, et rien ne coûte }
\livretVerse#7 { Pour un cœur qu’on veut toucher, }
\livretVerse#7 { L’onde se fait une route }
\livretVerse#7 { En s’efforçant d’en chercher, }
\livretVerse#7 { L’eau qui tombe goute à goute }
\livretVerse#7 { Perce le plus dur Rocher. }
\livretRef#'EEEairLhymenSeulNeSauraitPlaire
\livretVerse#7 { L’Hymen seul ne sçauroit plaire, }
\livretVerse#7 { Il a beau flatter nos vœux ; }
\livretVerse#7 { L’Amour seul a droit de faire }
\livretVerse#7 { Les plus doux de tous les nœuds. }
\livretVerse#7 { Il est fier, il est rebelle, }
\livretVerse#7 { Mais il charme tel qu’il est ; }
\livretVerse#7 { L’Hymen vient quand on l’appelle, }
\livretVerse#7 { L’Amour vient quand il luy plaist. }
\livretRef#'EEDairIlNEstPointDeResistance
\livretVerse#7 { Il n’est point de resistance }
\livretVerse#7 { Dont le temps ne vienne à bout, }
\livretVerse#7 { Et l’effort de la constance }
\livretVerse#7 { A la fin doit vaincre tout. }
\livretVerse#7 { Tout est doux, et rien ne coûte }
\livretVerse#7 { Pour un cœur qu’on veut toucher, }
\livretVerse#7 { L’onde se fait une route }
\livretVerse#7 { En s’efforçant d’en chercher, }
\livretVerse#7 { L’eau qui tombe goute à goute }
\livretVerse#7 { Perce le plus dur Rocher. }
\livretRef#'EEEairLAmourTroubleToutLeMonde
\livretVerse#7 { L’Amour trouble tout le Monde, }
\livretVerse#7 { C’est la source de nos pleurs ; }
\livretVerse#7 { C’est un feu brûlant dans l’onde, }
\livretVerse#7 { C’est l’écüeil des plus grands cœurs : }
\livretVerse#7 { Il est fier, il est rebelle, }
\livretVerse#7 { Mais il charme tel qu’il est ; }
\livretVerse#7 { L’Hymen vient quand on l’appelle, }
\livretVerse#7 { L’Amour vient quand il luy plaist. }
\livretPersDidas "Un Dieu de Fleuve & une Divinité de Fontaine," "dançent & chantent ensemble."
\livretRef#'EEGduneConstanceExtreme
\livretVerse#6 { D’une constance extresme, }
\livretVerse#6 { Un Ruisseau suit son cours ; }
\livretVerse#6 { Il en sera de mesme }
\livretVerse#6 { Du choix de mes amours, }
\livretVerse#6 { Et du moment que j’aime }
\livretVerse#6 { C’est pour aimer toûjours. }
\livretVerse#6 { Jamais un cœur volage }
\livretVerse#6 { Ne trouve un heureux sort, }
\livretVerse#6 { Il n’a point l’avantage }
\livretVerse#6 { D’estre long-temps au port, }
\livretVerse#6 { Il cherche encor l’orage }
\livretVerse#6 { Au moment qu’il en sort. }
\livretPers\wordwrap { Chœur de Dieux de Fleuves, & de Divinitez de Fontaines. }
\livretRef#'EEIchoeurUnGrandCalmeEstTropFacheux
\livretVerse#7 { Un grand calme est trop fascheux, }
\livretVerse#7 { Nous aimons mieux la tourmente. }
\livretVerse#7 { Que sert un cœur qui s’exempte }
\livretVerse#7 { De tous les soins amoureux ? }
\livretVerse#7 { A quoy sert une eau dormante ? }
\livretVerse#7 { Un grand calme est trop fascheux, }
\livretVerse#7 { Nous aimons mieux la tourmente. }

\livretScene\line { SCENE SIXIESME }
\livretDescAtt\wordwrap-center {
  Atys, Troupe de Zephirs volants, Sangaride, Celænus,
  Le Dieu du Fleuve Sangar, Troupe de Dieux de Fleuves, de Ruisseaux,
  et de Divinitez de Fontaines.
}
\livretPers\line { Chœur de Dieux de Fleuves, & de Fontaines. }
\livretRef#'EFAchoeurVenezFormerDesNoeudsCharmants
\livretVerse#8 { Venez former des nœuds charmants, }
\livretVerse#12 { Atys, venez unir ces bien-heureux Amants. }
\livretPers Atys
\livretVerse#8 { Cét Hymen desplaist à Cybele, }
\livretVerse#8 { Elle deffend de l’achever : }
\livretVerse#12 { Sangaride est un bien qu’il faut luy reserver, }
\livretVerse#8 { Et que je demande pour elle. }
\livretPers Chœur
\livretVerse#6 { Ah quelle loy cruelle ! }
\livretPers Celænus
\livretVerse#12 { Atys peut s’engager luy-mesme à me trahir ? }
\livretVerse#8 { Atys contre moy s’interesse ? }
\livretPers Atys
\livretVerse#8 { Seigneur, je suis à la Déesse, }
\livretVerse#12 { Dés qu’elle a commandé, je ne puis qu’obeïr. }
\livretPers\line { Le Dieu du Fleuve Sangar }
\livretVerse#8 { Pourquoy faut-il qu’elle separe }
\livretVerse#12 { Deux illustres Amants pour qui l’Hymen prepare }
\livretVerse#6 { Ses liens les plus doux ? }
\livretPers Chœur
\livretVerse#4 { Opposons-nous }
\livretVerse#6 { A ce dessein barbare. }
\livretPersDidas Atys "élevé sur un nuage"
\livretVerse#7 { Aprenez, audacieux, }
\livretVerse#7 { Qu’il n’est rien qui n’obeïsse }
\livretVerse#12 { Aux souveraines loix de la Reyne des Dieux. }
\livretVerse#8 { Qu’on nous enleve de ces lieux ; }
\livretVerse#12 { Zephirs, que sans tarder mon ordre s’accomplisse. }
\livretDesc\wordwrap-center {
  Les Zephirs volent, & enlevent Atys et Sangaride.
}
\livretPers Chœur
\livretVerse#4 { Quelle injustice ! }
\sep
\livretAct\line { ACTE CINQUIESME }
\livretDescAtt\wordwrap-center {
  Le Theatre change & represente des jardins agreables.
}
\livretScene\line { SCENE PREMIERE }
\livretDescAtt\wordwrap-center { Celænus, Cybele, Melisse. }
\livretPers Celænus
\livretRef #'FABvousMotezSangaride
\livretVerse#12 { Vous m’ostez Sangaride ? inhumaine Cybelle ; }
\livretVerse#6 { Est-ce le prix du zele }
\livretVerse#12 { Que j’ay fait avec soin éclater à vos yeux ? }
\livretVerse#12 { Preparez-vous ainsi la douceur eternelle }
\livretVerse#8 { Dont vous devez combler ces lieux ? }
\livretVerse#12 { Est-ce ainsi que les Roys sont protegez des Dieux ? }
\livretVerse#6 { Divinité cruelle, }
\livretVerse#8 { Descendez-vous exprés des Cieux }
\livretVerse#8 { Pour troubler un amour fidelle ? }
\livretVerse#12 { Et pour venir m’oster ce que j’aime le mieux ? }
\livretPers Cybele
\livretVerse#12 { J’aimois Atys, l’Amour a fait mon injustice ; }
\livretVerse#8 { Il a pris soin de mon suplice ; }
\livretVerse#8 { Et si vous estes outragé, }
\livretVerse#8 { Bien-tost vous serez top vangé. }
\livretVerse#8 { Atys adore Sangaride. }
\livretPers Celænus
\livretVerse#8 { Atys l’adore ? ah le perfide ! }
\livretPers Cybele
\livretVerse#12 { L’Ingrat vous trahissoit, et vouloit me trahir : }
\livretVerse#12 { Il s’est trompé luy mesme en croyant m’ébloüir. }
\livretVerse#12 { Les Zephirs l’ont laissé, seul, avec ce qu’il aime, }
\livretVerse#6 { Dans ces aimables lieux ; }
\livretVerse#8 { Je m’y suis cachée à leurs yeux ; }
\livretVerse#12 { J’y viens d’estre témoin de leur amour extresme. }
\livretPers Celænus
\livretVerse#12 { O Ciel ! Atys plairoit aux yeux qui m’ont charmé ? }
\livretPers Cybele
\livretVerse#12 { Eh pouvez-vous douter qu’Atys ne soit aimé ? }
\livretVerse#12 { Non, non, jamais amour n’eût tant de violence, }
\livretVerse#12 { Ils ont juré cent fois de s’aimer malgré nous, }
\livretVerse#8 { Et de braver nostre vengeance ; }
\livretVerse#12 { Ils nous ont appelez cruels, tyrans, jaloux ; }
\livretVerse#8 { Enfin leurs cœurs d’intelligence, }
\livretVerse#12 { Tous deux… ah je frémis au moment que j’y pense ! }
\livretVerse#12 { Tous deux s’abandonnoient à des transports si doux, }
\livretVerse#12 { Que je n’ay pû garder plus long-temps le silence, }
\livretVerse#12 { Ny retenir l’éclat de mon juste couroux. }
\livretPers Celænus
\livretVerse#12 { La mort est pour leur crime une peine legere. }
\livretPers Cybele
\livretVerse#12 { Mon cœur à les punir est assez engagé ; }
\livretVerse#12 { Je vous l’ay déja dit, croyez-en ma colere, }
\livretVerse#8 { Bient-tost vous serez trop vangé. }

\livretScene\line { SCENE SECONDE }
\livretDescAtt\wordwrap-center {
  Atys, Sangaride, Cybele, Celænus, Melisse, "Troupe de Prestresses de Cybele."
}
\livretPers "Cybele, & Celænus"
\livretRef#'FBAvenezVousLivrerAuSupplice
\livretVerse#8 { Venez vous livrer au suplice. }
\livretPers\line { Atys, & Sangaride }
\livretVerse#12 { Quoy la Terre et le Ciel contre nous sont armez ? }
\livretVerse#8 { Souffrirez-vous qu’on nous punisse ? }
\livretPers "Cybele, & Celænus"
\livretVerse#7 { Oubliez-vous vostre injustice ? }
\livretPers\line { Atys & Sangaride }
\livretVerse#12 { Ne vous souvient-il plus de nous avoir aimez ? }
\livretPers "Cybele & Celænus"
\livretVerse#12 { Vous changez mon amour en haine legitime. }
\livretPers\line { Atys & Sangaride }
\livretVerse#6 { Pouvez-vous condamner }
\livretVerse#6 { L’Amour qui nous anime ? }
\livretVerse#4 { Si c’est un crime, }
\livretVerse#8 { Quel crime est plus à pardonner ? }
\livretPers "Cybele & Celænus"
\livretVerse#8 { Perfide, deviez-vous me taire }
\livretVerse#12 { Que c’estoit vainement que je voulois vous plaire ? }
\livretPers\line { Atys & Sangaride }
\livretVerse#8 { Ne pouvant suivre vos desirs, }
\livretVerse#8 { Nous croyons ne pouvoir mieux faire }
\livretVerse#12 { Que de vous épargner de mortels déplaisirs. }
\livretPers Cybele
\livretVerse#12 { D’un suplice cruel craignez l’horreur extresme. }
\livretPers  "Cybele & Celænus"
\livretVerse#8 { Craignez un funeste trépas. }
\livretPers\line { Atys & Sangaride }
\livretVerse#12 { Vangez-vous, s’il le faut, ne me pardonnez pas, }
\livretVerse#8 { Mais pardonnez à ce que j’aime. }
\livretPers "Cybele & Celænus"
\livretVerse#12 { C’est peu de nous trahir, vous nous bravez, Ingrats ? }
\livretPers\line { Atys & Sangaride }
\livretVerse#12 { Serez-vous sans pitié ? }
\livretPers "Cybele & Celænus"
\livretVerse#12 { \transparent { Serez-vous sans pitié ? } Perdez toute esperance. }
\livretPers\line { Atys & Sangaride }
\livretVerse#12 { L’Amour nous a forcez à vous faire une offence, }
\livretVerse#8 { Il demande grace pour nous. }
\livretPers "Cybele & Celænus"
\livretVerse#5 { L’Amour en couroux }
\livretVerse#5 { Demande vengeance. }
\livretPers Cybele
\livretVerse#12 { Toy, qui portes par tout et la rage et l’horreur, }
\livretVerse#12 { Cesse de tourmenter les criminelles Ombres, }
\livretVerse#12 { Vien, cruelle Alecton, sors des Royaumes sombres, }
\livretVerse#12 { Inspire au cœur d’Atys ta barbare fureur. }

\livretScene \line { SCENE TROISIEME }
\livretDescAtt\wordwrap-center {
  Alecton, Atys, Sangaride, Cybele, Celænus, Melisse,
  Idas, Doris, Troupe de Prestresses de Cybele, Chœur de Phrygiens.
}
\livretRef#'FCApreludePourAlecton
\livretDidasPPage\justify {
  Alecton sort des Enfers, tenant à la main un flambeau qu’elle secouë
  en volant & en passant au dessus d’Atys.
}
\livretPers Atys
\livretRef#'FCBcielQuelleVapeurMenvironne
\livretVerse#8 { Ciel ! quelle vapeur m’environne ! }
\livretVerse#12 { Tous me[s] sens sont troublez, je fremis, je frissonne, }
\livretVerse#12 { Je tremble, et tout à coup, une infernale ardeur }
\livretVerse#12 { Vient enflammer mon sang, et devorer mon cœur. }
\livretVerse#12 { Dieux ! que vois-je ? le Ciel s’arme contre la Terre ? }
\livretVerse#12 { Que desordre ! quel bruit ! quel éclat de tonnerre ! }
\livretVerse#12 { Quels abysmes profonds sous mes pas sont ouverts ! }
\livretVerse#12 { Que de fantosmes vains sont sortis des Enfers ! }
\livretDidasP\line { Il parle à Cybele, qu’il prend pour Sangaride. }
\livretVerse#12 { Sangaride, ah fuyez la mort que vous prepare }
\livretVerse#8 { Une Divinité barbare : }
\livretVerse#12 { C’est vostre seul peril qui cause ma terreur. }
\livretPers Sangaride
\livretVerse#12 { Atys reconnoissez vostre funeste erreur. }
\livretPersDidas Atys \line { prenant Sangaride pour un monstre. }
\livretVerse#12 { Quel Monstre vient à nous ! quelle fureur le guide ! }
\livretVerse#12 { Ah respecte, cruel, l’aimable Sangaride. }
\livretPers Sangaride
\livretVerse#12 { Atys, mon cher Atys. }
\livretPers Atys
\livretVerse#12 { \transparent { Atys, mon cher Atys. } Quels hurlements affreux ! }
\livretPersDidas Celænus à Sangaride.
\livretVerse#8 { Fuyez, sauvez-vous de sa rage. }
\livretPersDidas Atys \wordwrap { tenant à la main le cousteau sacré qui sert aux sacrifices. }
\livretVerse#12 { Il faut combatre ; Amour, seconde mon courage. }
\livretDidasP\wordwrap {
  Atys court aprés Sangaride qui fuït dans un des costez du theatre.
}
\livretPers Celænus, & le Chœur
\livretVerse#8 { Arreste, arreste malheureux. }
\livretDidasP\wordwrap { Celænus court aprés Atys. }
\livretPersDidas Sangaride \line { dans un des costez du theatre. }
\livretVerse#12 { Atys ! }
\livretPers Le Chœur
\livretVerse#12 { \transparent { Atys ! } O Ciel }
\livretPers Sangaride
\livretVerse#12 { \transparent { Atys ! O Ciel } Je meurs. }
\livretPers Le Chœur
\livretVerse#12 { \transparent { Atys ! O Ciel Je meurs. } Atys, Atys luy-mesme, }
\livretVerse#6 { Fait perir ce qu’il aime ! }
\livretPersDidas Celænus \line { revenant sur le theatre. }
\livretVerse#12 { Je n’ay pû retenir ses efforts furieux, }
\livretVerse#8 { Sangaride expire à vos yeux. }
\livretPers Cybele
\livretVerse#12 { Atys me sacrifie une indigne Rivale. }
\livretVerse#12 { Partagez avec moy la douceur sans esgale, }
\livretVerse#12 { Que l’on gouste en vengeant un amour outragé. }
\livretVerse#12 { Je vous l’avois promis. }
\livretPers Celænus
\livretVerse#12 { \transparent { Je vous l’avois promis. } O promesse fatale ! }
\livretVerse#12 { Sangaride n’est plus, et je suis trop vangé. }
\livretDidasP\wordwrap {
  Celænus se retire au costé du Theatre, où est Sangaride morte.
}

\livretScene\line { SCENE QUATRIESME }
\livretDescAtt\wordwrap-center {
  Atys, Cybele, Melisse, Idas, Chœur de Phrygiens.
}
\livretPers Atys
\livretRef#'FDAqueJeViensDimmoler
\livretVerse#12 { Que je viens d’immoler une grande Victime ! }
\livretVerse#12 { Sangaride est sauvée, et c’est par ma valeur. }
\livretPersDidas Cybele touchant Atys.
\livretVerse#12 { Acheve ma vengeance, Atys, connoy ton crime, }
\livretVerse#12 { Et repren ta raison pour sentir ton malheur. }
\livretPers Atys
\livretVerse#13 { Un calme heureux succede aux troubles de mon cœur. }
\livretVerse#8 { Sangaride, Nymphe charmante, }
\livretVerse#12 { Qu’estes-vous devenuë ? où puis-je avoir recours ? }
\livretVerse#8 { Divinité toute puissante, }
\livretVerse#12 { Cybele, ayez pitié de nos tendres amours, }
\livretVerse#12 { Rendez-moy, Sangaride, espargnez ses beaux jours. }
\livretPersDidas Cybele \line { montrant à Atys Sangride morte. }
\livretVerse#13 { Tu la peux voir, regarde. }
\livretPers Atys
\livretVerse#13 { \transparent { Tu la peux voir, regarde. } Ah quelle barbarie ! }
\livretVerse#8 { Sangaride a perdu la vie ! }
\livretVerse#12 { Ah quelle main cruelle ! ah quel cœur inhumain !… }
\livretPers Cybele
\livretVerse#12 { Les coups dont elle meurt sont de ta propre main. }
\livretPers Atys
\livretVerse#12 { Moy, j’aurois immolé la Beauté qui m’enchante ? }
\livretVerse#6 { O Ciel ! ma main sanglante }
\livretVerse#12 { Est de ce crime horrible un tesmoin trop certain ! }
\livretPers Le Chœur
\livretVerse#6 { Atys, Atys luy-mesme, }
\livretVerse#6 { Fait perir ce qu’il aime. }
\livretPers Atys
\livretVerse#12 { Quoy, Sangaride est morte ? Atys est son boureau ! }
\livretVerse#12 { Quelle vengeance ô Dieux ! quel supplice nouveau ! }
\livretVerse#8 { Quelles horreurs sont comparables }
\livretVerse#6 { Aux horreurs que je sens ? }
\livretVerse#8 { Dieux cruels, Dieux impitoyables, }
\livretVerse#6 { N’estes-vous tout-puissants }
\livretVerse#8 { Que pour faire des miserables ? }
\livretPers Cybele
\livretVerse#8 { Atys, je vous ay trop aimé : }
\livretVerse#12 { Cét amour par vous-mesme en couroux transformé }
\livretVerse#8 { Fait voir encor sa violence : }
\livretVerse#12 { Jugez, Ingrat, jugez en ce funeste jour, }
\livretVerse#8 { De la grandeur de mon amour }
\livretVerse#8 { Par la grandeur de ma vengeance. }
\livretPers Atys
\livretVerse#12 { Barbare ! quel amour qui prend soin d’inventer }
\livretVerse#12 { Les plus horribles maux que la rage peut faire ! }
\livretVerse#8 { Bien-heureux qui peut éviter }
\livretVerse#6 { Le malheur de vous plaire. }
\livretVerse#12 { O Dieux ! injustes Dieux ! que n’estes-vous mortels ? }
\livretVerse#12 { Faut-il que pour vous seuls vous gardiez la vengeance ? }
\livretVerse#12 { C’est trop, c’est trop souffrir leur cruelle puissance, }
\livretVerse#12 { Chassons les d’icy bas, renversons leurs autels. }
\livretVerse#12 { Quoy, Sangaride est morte ? Atys, Atys luy-mesme }
\livretVerse#6 { Fait perir ce qu’il aime ? }
\livretPers Le Chœur
\livretVerse#6 { Atys, Atys luy-mesme }
\livretVerse#6 { Fait perir ce qu’il aime. }
\livretPersDidas Cybele \wordwrap { ordonnant d’emporter le corps de Sangaride morte. }
\livretVerse#12 { Ostez ce triste objet. }
\livretPers Atys
\livretVerse#12 { \transparent { Ostez ce triste objet. } Ah ne m’arrachez pas }
\livretVerse#8 { Ce qui reste de tant d’appas ? }
\livretVerse#8 { En fussiez-vous jalouse encore, }
\livretVerse#6 { Il faut que je l’adore }
\livretVerse#8 { Jusques dans l’horreur du trépas. }

\livretScene\line { SCENE CINQUIESME }
\livretDescAtt\wordwrap-center { Cybele, Melisse. }
\livretPers Cybele
\livretRef#'FEAjeCommenceATrouverSaPeineTropCruelle
\livretVerse#12 { Je commence à trouver sa peine trop cruelle, }
\livretVerse#8 { Une tendre pitié rapelle }
\livretVerse#12 { L’Amour que mon couroux croyoit avoir banny, }
\livretVerse#12 { Ma Rivale n’est plus, Atys n’est plus coupable, }
\livretVerse#12 { Qu’il est aisé d’aimer un criminel aimable }
\livretVerse#6 { Aprés l’avoir puny. }
\livretVerse#8 { Que son desespoir m’espouvante ! }
\livretVerse#12 { Ses jours sont en peril, et j’en fremis d’effroy : }
\livretVerse#12 { Je veux d’un soin si cher ne me fier qu’à moy, }
\livretVerse#12 { Allons… mais quel spectable à mes yeux se presente ? }
\livretVerse#8 { C’est Atys mourant que je voy ! }

\livretScene\line { SCENE SIXIESME }
\livretDescAtt \wordwrap-center {
  Atys, Idas, Cybele, Melisse, Prestresses de Cybele.
}
\livretPersDidas Idas \line { soûtenant Atys. }
\livretRef#'FFAilSestPerceLeSein
\livretVerse#12 { Il s’est percé le sein, et mes soins pour sa vie }
\livretVerse#8 { N’ont pû prevenir sa fureur. }
\livretPers Cybele
\livretVerse#6 { Ah c’est ma barbarie, }
\livretVerse#8 { C’est moy, qui luy perce le cœur. }
\livretPers Atys
\livretVerse#6 { Je meurs, l’Amour me guide }
\livretVerse#6 { Dans la nuit du Trépas ; }
\livretVerse#8 { Je vais où sera Sangaride, }
\livretVerse#12 { Inhumaine, je vais, où vous ne serez pas. }
\livretPers Cybele
\livretVerse#12 { Atys, il est trop vray, ma rigueur est extresme, }
\livretVerse#8 { Plaignez-vous, je veux tout souffrir. }
\livretVerse#12 { Pourquoy suis-je immortelle en vous voyant perir ? }
\livretPers\line { Atys, & Cybele }
\livretVerse#6 { Il est doux de mourir }
\livretVerse#6 { Avec ce que l’on aime. }
\livretPers Cybele
\livretVerse#12 { Que mon amour funeste armé contre moy-mesme, }
\livretVerse#12 { Ne peut-il vous venger de toutes mes rigueurs. }
\livretPers Atys
\livretVerse#12 { Je suis assez vengé, vous m’aimez, et je meurs. }
\livretPers Cybele
\livretVerse#8 { Malgré le Destin implacable }
\livretVerse#12 { Qui rend de ton trépas l’arrest irrevocable, }
\livretVerse#12 { Atys, sois à jamais l’objet de mes amours : }
\livretVerse#12 { Reprens un sort nouveau, deviens un Arbre aimable }
\livretVerse#8 { Que Cybele aimera toûjours. }
\livretRef #'FGAritournelle
\livretDidasPPage\wordwrap {
  Atys prend la forme de l’Arbre aimé de la Déesse Cybele,
  que l’on appelle Pin.
}
\livretPers Cybele
\livretRef#'FGBvenezFurieuxCorybantes
\livretVerse#8 { Venez furieux Corybantes, }
\livretVerse#12 { Venez joindre à mes cris vos clameurs esclatantes ; }
\livretVerse#12 { Venez Nymphes des Eaux, venez Dieux des Forests, }
\livretVerse#8 { Par vos plaintes les plus touchantes }
\livretVerse#8 { Secondez mes tristes regrets. }

\livretScene\line { SCENE SEPTIESME, ET DERNIERE }
\livretDescAtt \column {
  \wordwrap-center {
    Cybele,
    "Troupe de Nymphes des Eaux,"
    "Troupe de Divinitez des Bois,"
    "Troupe de Corybantes."
  }
  \smaller\wordwrap-center\italic {
    "Quatre Nymphes chantantes."
    "Huit Dieux des Bois chantants."
    "Quatorze Corybantes chantantes."
    "Huit Corybantes dançantes."
    "Trois Dieux des Bois, dançants."
    "Trois Nymphes dançantes."
  }
}
\livretPers Cybele
\livretRef#'FGCairChoeurAtysAimableAtys
\livretVerse#12 { Atys, l’aimable Atys, malgré tous ses attraits, }
\livretVerse#8 { Descend dans la nuit éternelle ; }
\livretVerse#7 { Mais malgré la mort cruelle, }
\livretVerse#5 { L’amour de Cybele }
\livretVerse#5 { Ne mourra jamais. }
\livretVerse#8 { Sous une nouvelle figure, }
\livretVerse#12 { Atys est ranimé par mon pouvoir divin ; }
\livretVerse#8 { Celebrez son nouveau destin, }
\livretVerse#8 { Pleurez sa funeste avanture. }
\livretPers\line { Chœur des Nymphes des Eaux, & des Divinitez des Bois }
\livretVerse#8 { Celebrons son nouveau destin, }
\livretVerse#8 { Pleurons sa funeste avanture. }
\livretPers Cybele
\livretVerse#6 { Que cét Arbre sacré }
\livretVerse#4 { Soit reveré }
\livretVerse#6 { De toute la Nature. }
\livretVerse#12 { Qu’il s’esleve au dessus des Arbres les plus beaux : }
\livretVerse#12 { Qu’il soit voisin des Cieux, qu’il regne sur les Eaux ; }
\livretVerse#12 { Qu’il ne puisse brûler que d’une flame pure. }
\livretVerse#6 { Que cét Arbre sacré }
\livretVerse#4 { Soit reveré }
\livretVerse#6 { De toute la Nature. }
\livretDidasP\wordwrap { Le Chœur repete ces trois derniers Vers. }
\livretPers Cybele
\livretVerse#8 { Que ses rameaux soient toûjours verds : }
\livretVerse#8 { Que les plus rigoureux Hyvers }
\livretVerse#8 { Ne leur fassent jamais d’injure. }
\livretVerse#6 { Que cét Arbre sacré }
\livretVerse#4 { Soit reveré }
\livretVerse#6 { De toute la Nature. }
\livretDidasP\wordwrap { Le Chœur repete ces trois derniers Vers. }
\livretPers\line { Cybele, & le Chœur des Divinitez des Bois & des Eaux }
\livretVerse#4 { Quelle douleur ! }
\livretPers \line { Cybele, & le Chœur des Corybantes }
\livretVerse#4 { Ah ! quelle rage ! }
\livretPers\line { Cybele, & les Chœurs }
\livretVerse#4 { Ah ! quel malheur ! }
\livretPers Cybele
\livretVerse#8 { Atys au printemps de son âge, }
\livretVerse#6 { Perit comme une fleur }
\livretVerse#5 { Qu’un soudain orage }
\livretVerse#5 { Reenverse et ravage. }
\livretPers\line { Cybele, & le Chœur des Divinitez des Bois & des Eaux }
\livretVerse#4 { Quelle douleur ! }
\livretPers \line { Cybele, & le Chœur des Corybantes }
\livretVerse#4 { Ah ! quelle rage ! }
\livretPers\line { Cybele, & les Chœurs }
\livretVerse#4 { Ah ! quel malheur ! }
\livretRef#'FGDentreeNymphes
\livretDidasPPage \justify {
  Les Divinitez des Bois & des Eaux, avec les Corybantes, honorent le
  nouvel Arbre, & le consacrent à Cybele. Les regrets des
  Divinitez des Bois & des Eaux, & les cris des Corybantes, sont
  secondez & terminez par des tremblemens de Terre, par des
  Esclairs, & par des esclats de Tonnerre.
}
\livretPers\wordwrap { Cybele, & le Chœur des Divinitez des Bois, & des Eaux }
\livretRef#'FGGchoeurQueLeMalheurDAtys
\livretVerse#12 { Que le malheur d’Atys afflige tout le monde. }
\livretPers\line { Cybele, & le Chœur des Corybantes }
\livretVerse#6 { Que tout sente, icy bas, }
\livretVerse#8 { L’horreur d’un si cruel trépas. }
\livretPers\line { Cybele, & le Chœur des Divinitez des Bois, & des Eaux }
\livretVerse#12 { Penetrons tous les Cœurs d’une douleur profonde : }
\livretVerse#12 { Que les Bois, que les Eaux, perdent tous leurs appas. }
\livretPers\line { Cybele, & le Chœur des Corybantes }
\livretVerse#8 { Que le Tonnerre nous responde : }
\livretVerse#12 { Que la Terre fremisse, et tremble sous nos pas. }
\livretPers\line { Cybele, & le Chœur des Divinitez des Bois, & des Eaux }
\livretVerse#12 { Que le malheur d’Atys afflige tout le monde. }
\livretPers\line { Tous ensemble. }
\livretVerse#6 { Que tout sente, icy bas, }
\livretVerse#8 { L’horreur d’un si cruel trépas. }
\sep
}

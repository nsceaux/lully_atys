\livretAct \line { ACTE QUATRIESME }
\livretDescAtt\wordwrap-center {
  Le Theatre change & represente le palais du Fleuve Sangar.
}
\livretScene\line { SCENE PREMIERE }
\livretDescAtt\wordwrap-center { Sangaride, Doris, Idas. }
\livretPers Doris
\livretRef#'EAArecitQuoiVousPleurez
%#- Quoy, vous pleurez?
\livretPers Idas
%#= D'où vient vostre peine nouvelle?
\livretPers Doris
%# N'osez-vous découvrir vostre amour à Cybele?
\livretPers Sangaride
%#- Helas!
\livretPers\line { Doris & Idas }
%#= Qui peut encor redoubler vos ennuis?
\livretPers Sangaride
%#- Helas! j'aime… helas! j'aime…
\livretPers\line { Doris & Idas }
%#- Achevez.
\livretPers Sangaride
%#= Je ne puis.
\livretPers \line { Doris & Idas }
%# L'Amour n'est guere *heureux lorsqu'il est trop timide.
\livretPers Sangaride
%# Helas! j'aime un perfide
%# Qui trahit mon amour;
%# Le De=esse aime Atys, il change en moins d'un jour,
%# Atys comblé d'honneurs n'aime plus Sangaride.
%# Helas! j'aime un perfide
%# Qui trahit mon amour.
\livretPers\line { Doris & Idas }
%# Il nous montroit tantost un peu d'incertitude;
%# Mais qui l'eust soupçonné de tant d'ingratitude?
\livretPers Sangaride
%# J'embarassois Atys, je l'ay veu se troubler:
%# Je croy=ois devoir reveler
%# Nostre amour à Cybele;
%# Mais l'Ingrat, l'Infidelle,
%# M'empéchoit toûjours de parler.
\livretPers\line { Doris & Idas }
%# Peut-on changer si-tost quand l'Amour est extrême?
%# Gardez-vous, gardez-vous
%# De trop croire un transport jaloux.
\livretPers Sangaride
%# Cybele hautement declare qu'elle l'aime,
%# Et l'Ingrat n'a trouvé cét honneur que trop doux;
%# Il change en un moment, je veux changer de mesme,
%# J'accepteray sans peine un glori=eux espoux,
%# Je ne veux plus aimer que la grandeur supresme.
\livretPers\line { Doris & Idas }
%# Peut-on changer si-tost quand l'Amour est extrême?
%# Gardez-vous, gardez-vous
%# De trop croire un transport jaloux.
\livretPers Sangaride
%# Trop *heureux un cœur qui peut croire
%# Un dépit qui sert à sa gloire.
%# Revenez ma Raison, revenez pour jamais,
%# Joignez-vous au Dépit pour estouffer ma flâme,
%# Reparez, s'il se peut, les maux qu'Amour m'a faits,
%# Venez restablir dans mon ame
%# Les douceurs d'une *heureuse paix;
%# Revenez, ma Raison, revenez pour jamais.
\livretPers\line { Idas & Doris }
%# Une infidelité cru=elle
%# N'efface point tous les appas
%# D'un infidelle,
%# Et la Raison ne revient pas
%# Si-tost qu'on l'a rappelle.
\livretPers Sangaride
%# Après une trahison
%# Si la raison ne m'éclaire,
%# Le dépit et la colere
%# Me tiendront lieu de raison.
\livretPers \line { Sangaride, Doris, Idas. }
%# Qu'une premiere amour est belle?
%# Qu'on a peine à s'en dégager!
%# Que l'on doit plaindre un cœur fidelle
%# Lorsqu'il est forcé de changer.

\livretScene\line { SCENE SECONDE }
\livretDescAtt\wordwrap-center { 
  Celænus, suivans de Celænus, Sangaride, Idas, & Doris.
}
\livretPers Celænus
\livretRef#'EBBbelleNymphe
%# Belle Nymphe, l'*Hymen va suivre mon envie,
%# L'Amour avec moy vous convie
%# A venir vous placer sur un Thrône éclatant,
%# J'aproche avec transport du favorable instant
%# D'où despend la douceur du reste de ma vie:
%# Malgré tous les transports de mon ame amoureuse,
%# Si je ne puis vous rendre *heureuse,
%# Je ne seray jamais content.
%# Je fais mon bonheur de vous plaire,
%# J'attache à vostre cœur mes desirs les plus doux.
\livretPers Sangaride
%# Seigneur, j'obe=ïray, je despens de mon Pere,
%# Et mon Pere aujourd'huy veut que je sois à vous.
\livretPers Celænus
%# Regardez mon amour, plustost que ma Couronne.
\livretPers Sangaride
%# Ce n'est point la grandeur qui me peut esbloü=ir.
\livretPers Celænus
%# Ne sçauriez-vous m'aimer sans que l'on vous l'ordonne.
\livretPers Sangaride
%# Seigneur, contentez-vous que je sçache obe=ïr,
%# En l'estat où je suis c'est ce que je puis dire…

\livretScene\line { SCENE TROISIESME }
\livretDescAtt \wordwrap-center {
  Atys, Celænus, Sangaride, Doris, Idas, Suivans de Celænus.
}
\livretPers Celænus
\livretRef#'ECArecitVotreCoeurSeTrouble
%# Vostre cœur se trouble, il soûpire.
\livretPers Sangaride
%# Expliquez en vostre faveur
%# Tout ce que vous voy=ez de trouble dans mon cœur.
\livretPers Celænus
%# Rien ne m'allarme plus, Atys, ma crainte est vaine,
%# Mon amour touche enfin le cœur de la Beauté
%# Dont je suis enchanté:
%# Toy qui fûs le tesmoin de ma peine,
%# Cher Atys, sois tesmoin de ma felicité.
%# Peux-tu la concevoir? non, il faut que l'on aime,
%# Pour juger des douceurs de mon bonheur extresme.
%# Mais, prés de voir combler mes vœux,
%# Que les moments sont longs pour mon cœur amoureux!
%# Vos parents tardent trop, je veux aller moy-mesme
%# Les presser de me rendre *heureux.

\livretScene\line { SCENE QUATRIESME }
\livretDescAtt\wordwrap-center { Atys, Sangaride. }
\livretPers Atys
\livretRef#'EDBrecitQuilSaitPeuSonMalheur
%# Qu'il sçait peu son malheur! et qu'il est déplorable!
%# Son amour meritoit un sort plus favorable:
%# J'ay pitié de l'erreur dont son cœur s'est flatté.
\livretPers Sangaride
%# Espargnez-vous le soin d'estre si pitoy=able,
%# Son amour obtiendra ce qu'il a merité.
\livretPers Atys
%#- Dieux! qu'est-ce que j'entends!
\livretPers Sangaride
%#= Qu'il faut que je me vange,
%# Que j'aime enfin le Roy, qu'il sera mon espoux.
\livretPers Atys
%# Sangaride, eh d'où vient ce changement estrange?
\livretPers Sangaride
%# N'est-ce pas vous ingrat qui voulez que je change?
\livretPers Atys
%#- Moy!
\livretPers Sangaride
%#- Quelle trahison!
\livretPers Atys
%#= Quel funeste couroux!
\livretPers\line { Atys & Sangaride }
%# Pourquoy m'abandonner pour une amour nouvelle?
%# Ce n'est pas moy qui rompt une chaisne si belle.
\livretPers Atys
%# Beauté trop cru=elle, c'est vous,
\livretPers Sangaride
%# Amant infidelle, c'est vous,
\livretPers Atys
%# Ah! c'est vous, Beauté trop cru=elle,
\livretPers Sangaride
%# Ah! c'est vous Amant infidelle.
\livretPers \line { Atys & Sangaride }
%# Beauté trop cru=elle, c'est vous,
%# Amant infidelle, c'est vous,
%# Qui rompez des li=ens si doux.
\livretPers Sangaride
%# Vous m'avez immolée à l'amour de Cybele.
\livretPers Atys
%# Il est vray qu'à ses yeux, par un secret effroy,
%# J'ay voulu de nos cœurs cacher l'intelligence:
%# Mais ce n'est que pour vous que j'ay crain sa vengeance,
%# Et je ne la crains pas pour moy.
%# Cybele m'ayme en vain, et c'est vous que j'adore.
\livretPers Sangaride
%# Aprés vostre infidelité,
%# Auriez-vous bien la cru=auté
%# De vouloir me tromper encore?
\livretPers Atys
%# Moy! vous trahir? vous le pensez?
%# Ingrate, que vous m'offencez!
%# Hé bien, il ne faut plus rien taire,
%# Je vais de la Dé=esse attirer la colere,
%# M'offrir à sa fureur, puisque vous m'y forcez…
\livretPers Sangaride
%# Ah! demeurez, Atys, mes soupçons sont passez;
%# Vous m'aimez, je le croy, j'en veux estre certaine.
%# Je le souhaite assez,
%# Pour le croire sans peine.
\livretPers Atys
%#- Je jure,
\livretPers Sangaride
%#= Je promets,
\livretPers\line { Atys & Sangaride }
%# De ne changer jamais.
\livretPers Sangaride
%# Quel tourment de cacher une si belle flame.
\livretPers Atys
%# Redoublons-en l'ardeur dans le fonds de nostre ame.
\livretPers\line { Atys & Sangaride }
%# Aimons en secret, aimons-nous:
%# Aimons plusque jamais, en dépit des Jaloux.
\livretPers Sangaride
%#- Mon père vient icy,
\livretPers Atys
%#= Que rien ne vous estonne;
%# Servons-nous du pouvoir que Cybele me donne,
%# Je vais preparer les Zephirs
%# A suivre nos desirs.

\livretScene\line { SCENE CINQUIESME }
\livretDesc\column {
  \wordwrap-center {
    Sangaride, Celænus, le Dieux du Fleuve Sangar,
    Troupe de Dieux de Fleuves, de Ruisseaux,
    et de Divinitez de Fontaines.
  }
  \smaller\wordwrap-center\italic {
    "Le Fleuve Sangar."
    "Suite du Fleuve Sangar."
    "Douze grands Dieux de Fleuves chantants."
    "Cinq Dieux de Fleuves joüans de la Flutte."
    "Quatre divinitez de fontaines, et quatre Dieux"
    "de Fleuves chantants et dançants."
    "Quatre Divinitéz de Fontaines."
    "Deux Dieux de Fleuves."
    "Deux Dieux de Fleuves dançants ensemble."
    "Deux petits Dieux de Ruisseaux chantants et dançants."
    "Quatre petits Dieux de Ruisseaux dançants."
    "Six grands Dieux de Fleuves dançants."
    "Deux vieux Dieux de Fleuves"
    "& deux vieilles Nymphes de Fontaines dançantes."
  }
}
\livretPers\line { Le Dieu du Fleuve Sangar }
\livretRef#'EEBOVousQuiPrenezPart
%# O vous, qui prenez part au bien de ma famille,
%# Vous, venerables Dieux des Fleuves les plus grands,
%# Mes fidelles Amis, et mes plus chers Parents,
%# Voy=ez quel est l'Espoux que je donne à ma fille:
%# J'ay pris soin de choisir entre les plus grands Roys.
\livretPers\line { Chœur de Dieux de Fleuves }
%# Nous aprouvons vostre choix.
\livretPers\line { Le Dieu du Fleuve Sangar }
%# Il a Neptune pour son Pere,
%# Les Phrygi=ens suivent ses Loix;
%# J'ay crû ne pouvoir faire
%# Un choix plus digne de vous plaire.
\livretPers\line { Chœur de Dieux de Fleuves }
%# Tous, d'une commune voix,
%# Nous aprouvons vostre choix.
\livretPers\line { Le Dieu du Fleuve Sangar }
\livretRef#'EECairQueLonChante
%# Que l'on chante, que l'on dance,
%# Ri=ons tous lors qu'il le faut;
%# Ce n'est jamais trop tost
%# Que le plaisir commence.
%# On trouve bien-tost la fin
%# Des jours de réjoü=issance,
%# On a beau chasser le chagrin,
%# Il revient plustost qu'on ne pense.
\livretPers\line { Le Dieu du Fleuve Sangar, & le Chœur }
\livretRef#'EECchoeurQueLonChante
%# Que l'on chante, que l'on dance,
%# Ri=ons tous lors qu'il le faut;
%# Ce n'est jamais trop tost
%# Que le plaisir commence.
%# Que l'on chante, que l'on dance,
%# Ri=ons tous lors qu'il le faut.
\livretPersDidas "Dieux de Fleuves, Divinitez de Fontaines, & de Ruisseaux," "chantants & dançants ensemble."
\livretRef#'EEDairLaBeauteLaPlusSevere
%# La Beauté la plus severe
%# Prend pitié d'un long tourment,
%# Et l'Amant qui persevere
%# Devient un *heureux Amant.
%# Tout est doux, et rien ne coûte
%# Pour un cœur qu'on veut toucher,
%# L'onde se fait une route
%# En s'efforçant d'en chercher,
%# L'eau qui tombe goute à goute
%# Perce le plus dur Rocher.
\livretRef#'EEEairLhymenSeulNeSauraitPlaire
%# L'*Hymen seul ne sçauroit plaire,
%# Il a beau flatter nos vœux;
%# L'Amour seul a droit de faire
%# Les plus doux de tous les nœuds.
%# Il est fier, il est rebelle,
%# Mais il charme tel qu'il est;
%# L'*Hymen vient quand on l'appelle,
%# L'Amour vient quand il luy plaist.
\livretRef#'EEDairIlNEstPointDeResistance
%# Il n'est point de resistance
%# Dont le temps ne vienne à bout,
%# Et l'effort de la constance
%# A la fin doit vaincre tout.
%# Tout est doux, et rien ne coûte
%# Pour un cœur qu'on veut toucher,
%# L'onde se fait une route
%# En s'efforçant d'en chercher,
%# L'eau qui tombe goute à goute
%# Perce le plus dur Rocher.
\livretRef#'EEEairLAmourTroubleToutLeMonde
%# L'Amour trouble tout le Monde,
%# C'est la source de nos pleurs;
%# C'est un feu brûlant dans l'onde,
%# C'est l'écüeil des plus grands cœurs:
%# Il est fier, il est rebelle,
%# Mais il charme tel qu'il est;
%# L'*Hymen vient quand on l'appelle,
%# L'Amour vient quand il luy plaist.
\livretPersDidas "Un Dieu de Fleuve & une Divinité de Fontaine," "dançent & chantent ensemble."
\livretRef#'EEGduneConstanceExtreme
%# D'une constance extresme,
%# Un Ruisseau suit son cours;
%# Il en sera de mesme
%# Du choix de mes amours,
%# Et du moment que j'aime
%# C'est pour aimer toûjours.
%# Jamais un cœur volage
%# Ne trouve un *heureux sort,
%# Il n'a point l'avantage
%# D'estre long-temps au port,
%# Il cherche encor l'orage
%# Au moment qu'il en sort.
\livretPers\wordwrap { Chœur de Dieux de Fleuves, & de Divinitez de Fontaines. }
\livretRef#'EEIchoeurUnGrandCalmeEstTropFacheux
%# Un grand calme est trop fascheux,
%# Nous aimons mieux la tourmente.
%# Que sert un cœur qui s'exempte
%# De tous les soins amoureux?
%# A quoy sert une eau dormante?
%# Un grand calme est trop fascheux,
%# Nous aimons mieux la tourmente.

\livretScene\line { SCENE SIXIESME }
\livretDescAtt\wordwrap-center {
  Atys, Troupe de Zephirs volants, Sangaride, Celænus,
  Le Dieu du Fleuve Sangar, Troupe de Dieux de Fleuves, de Ruisseaux,
  et de Divinitez de Fontaines.
}
\livretPers\line { Chœur de Dieux de Fleuves, & de Fontaines. }
\livretRef#'EFAchoeurVenezFormerDesNoeudsCharmants
%# Venez former des nœuds charmants,
%# Atys, venez unir ces bien-*heureux Amants.
\livretPers Atys
%# Cét *Hymen desplaist à Cybele,
%# Elle deffend de l'achever:
%# Sangaride est un bien qu'il faut luy reserver,
%# Et que je demande pour elle.
\livretPers Chœur
%# Ah quelle loy cru=elle!
\livretPers Celænus
%# Atys peut s'engager luy-mesme à me trahir?
%# Atys contre moy s'interesse?
\livretPers Atys
%# Seigneur, je suis à la Dé=esse,
%# Dés qu'elle a commandé, je ne puis qu'obe=ïr.
\livretPers\line { Le Dieu du Fleuve Sangar }
%# Pourquoy faut-il qu'elle separe
%# Deux illustres Amants pour qui l'*Hymen prepare
%# Ses li=ens les plus doux?
\livretPers Chœur
%# Opposons-nous
%# A ce dessein barbare.
\livretPersDidas Atys "élevé sur un nuage"
%# Aprenez, audaci=eux,
%# Qu'il n'est rien qui n'obe=ïsse
%# Aux souveraines loix de la Reyne des Dieux.
%# Qu'on nous enleve de ces lieux;
%# Zephirs, que sans tarder mon ordre s'accomplisse.
\livretDesc\wordwrap-center {
  Les Zephirs volent, & enlevent Atys et Sangaride.
}
\livretPers Chœur
%# Quelle injustice!
\sep

\livretAct\line { ACTE CINQUIESME }
\livretDescAtt\wordwrap-center {
  Le Theatre change & represente des jardins agreables.
}
\livretScene\line { SCENE PREMIERE }
\livretDescAtt\wordwrap-center { Celænus, Cybele, Melisse. }
\livretPers Celænus
\livretRef #'FABvousMotezSangaride
%# Vous m'ostez Sangaride? inhumaine Cybelle;
%# Est-ce le prix du zele
%# Que j'ay fait avec soin éclater à vos yeux?
%# Preparez-vous ainsi la douceur eternelle
%# Dont vous devez combler ces lieux?
%# Est-ce ainsi que les Roys sont protegez des Dieux?
%# Divinité cru=elle,
%# Descendez-vous exprés des Cieux
%# Pour troubler un amour fidelle?
%# Et pour venir m'oster ce que j'aime le mieux?
\livretPers Cybele
%# J'aimois Atys, l'Amour a fait mon injustice;
%# Il a pris soin de mon suplice;
%# Et si vous estes outragé,
%# Bien-tost vous serez top vangé.
%# Atys adore Sangaride.
\livretPers Celænus
%# Atys l'adore? ah le perfide!
\livretPers Cybele
%# L'Ingrat vous trahissoit, et vouloit me trahir:
%# Il s'est trompé luy mesme en croy=ant m'ébloü=ir.
%# Les Zephirs l'ont laissé, seul, avec ce qu'il aime,
%# Dans ces aimables lieux;
%# Je m'y suis cachée =à leurs yeux;
%# J'y viens d'estre témoin de leur amour extresme.
\livretPers Celænus
%# O Ciel! Atys plairoit aux yeux qui m'ont charmé?
\livretPers Cybele
%# Eh pouvez-vous douter qu'Atys ne soit aimé?
%# Non, non, jamais amour n'eût tant de vi=olence,
%# Ils ont juré cent fois de s'aimer malgré nous,
%# Et de braver nostre vengeance;
%# Ils nous ont appelez cru=els, tyrans, jaloux;
%# Enfin leurs cœurs d'intelligence,
%# Tous deux… ah je frémis au moment que j'y pense!
%# Tous deux s'abandonnoient à des transports si doux,
%# Que je n'ay pû garder plus long-temps le silence,
%# Ny retenir l'éclat de mon juste couroux.
\livretPers Celænus
%# La mort est pour leur crime une peine legere.
\livretPers Cybele
%# Mon cœur à les punir est assez engagé;
%# Je vous l'ay déja dit, croy=ez-en ma colere,
%# Bient-tost vous serez trop vangé.

\livretScene\line { SCENE SECONDE }
\livretDescAtt\wordwrap-center {
  Atys, Sangaride, Cybele, Celænus, Melisse, "Troupe de Prestresses de Cybele."
}
\livretPers "Cybele, & Celænus"
\livretRef#'FBAvenezVousLivrerAuSupplice
%# Venez vous livrer au suplice.
\livretPers\line { Atys, & Sangaride }
%# Quoy la Terre et le Ciel contre nous sont armez?
%# Souffrirez-vous qu'on nous punisse?
\livretPers "Cybele, & Celænus"
%# Oubliez-vous vostre injustice?
\livretPers\line { Atys & Sangaride }
%# Ne vous souvient-il plus de nous avoir aimez?
\livretPers "Cybele & Celænus"
%# Vous changez mon amour en haine legitime.
\livretPers\line { Atys & Sangaride }
%# Pouvez-vous condamner
%# L'Amour qui nous anime?
%# Si c'est un crime,
%# Quel crime est plus à pardonner?
\livretPers "Cybele & Celænus"
%# Perfide, deviez-vous me taire
%# Que c'estoit vainement que je voulois vous plaire?
\livretPers\line { Atys & Sangaride }
%# Ne pouvant suivre vos desirs,
%# Nous croy=ons ne pouvoir mieux faire
%# Que de vous épargner de mortels déplaisirs.
\livretPers Cybele
%# D'un suplice cru=el craignez l'*horreur extresme.
\livretPers  "Cybele & Celænus"
%# Craignez un funeste trépas.
\livretPers\line { Atys & Sangaride }
%# Vangez-vous, s'il le faut, ne me pardonnez pas,
%# Mais pardonnez à ce que j'aime.
\livretPers "Cybele & Celænus"
%# C'est peu de nous trahir, vous nous bravez, Ingrats?
\livretPers\line { Atys & Sangaride }
%#- Serez-vous sans pitié?
\livretPers "Cybele & Celænus"
%#= Perdez toute esperance.
\livretPers\line { Atys & Sangaride }
%# L'Amour nous a forcez à vous faire une offence,
%# Il demande grace pour nous.
\livretPers "Cybele & Celænus"
%# L'Amour en couroux
%# Demande vengeance.
\livretPers Cybele
%# Toy, qui portes par tout et la rage et l'horreur,
%# Cesse de tourmenter les criminelles Ombres,
%# Vien, cru=elle Alecton, sors des Roy=aumes sombres,
%# Inspire au cœur d'Atys ta barbare fureur.

\livretScene \line { SCENE TROISIEME }
\livretDescAtt\wordwrap-center {
  Alecton, Atys, Sangaride, Cybele, Celænus, Melisse,
  Idas, Doris, Troupe de Prestresses de Cybele, Chœur de Phrygiens.
}
\livretRef#'FCApreludePourAlecton
\livretDidasPPage\justify {
  Alecton sort des Enfers, tenant à la main un flambeau qu’elle secouë
  en volant & en passant au dessus d’Atys.
}
\livretPers Atys
\livretRef#'FCBcielQuelleVapeurMenvironne
%# Ciel! quelle vapeur m'environne!
%# Tous me[s] sens sont troublez, je fremis, je frissonne,
%# Je tremble, et tout à coup, une infernale ardeur
%# Vient enflammer mon sang, et devorer mon cœur.
%# Dieux! que vois-je? le Ciel s'arme contre la Terre?
%# Que desordre! quel bruit! quel éclat de tonnerre!
%# Quels abysmes profonds sous mes pas sont ouverts!
%# Que de fantosmes vains sont sortis des Enfers!
\livretDidasP\line { Il parle à Cybele, qu’il prend pour Sangaride. }
%# Sangaride, ah fuy=ez la mort que vous prepare
%# Une Divinité barbare:
%# C'est vostre seul peril qui cause ma terreur.
\livretPers Sangaride
%# Atys reconnoissez vostre funeste erreur.
\livretPersDidas Atys \line { prenant Sangaride pour un monstre. }
%# Quel Monstre vient à nous! quelle fureur le guide!
%#12 Ah respecte, cr=uel, l'aimable Sangaride.
\livretPers Sangaride
%#- Atys, mon cher Atys.
\livretPers Atys
%#= Quels hurlements affreux!
\livretPersDidas Celænus à Sangaride.
%# Fuy=ez, sauvez-vous de sa rage.
\livretPersDidas Atys \wordwrap { tenant à la main le cousteau sacré qui sert aux sacrifices. }
%# Il faut combatre; Amour, seconde mon courage.
\livretDidasP\wordwrap {
  Atys court aprés Sangaride qui fuït dans un des costez du theatre.
}
\livretPers Celænus, & le Chœur
%# Arreste, arreste malheureux.
\livretDidasP\wordwrap { Celænus court aprés Atys. }
\livretPersDidas Sangaride \line { dans un des costez du theatre. }
%#- Atys!
\livretPers Le Chœur
%#- O Ciel
\livretPers Sangaride
%#- Je meurs.
\livretPers Le Chœur
%#= Atys, Atys luy-mesme,
%# Fait perir ce qu'il aime!
\livretPersDidas Celænus \line { revenant sur le theatre. }
%# Je n'ay pû retenir ses efforts furi=eux,
%# Sangaride expire à vos yeux.
\livretPers Cybele
%# Atys me sacrifie =une indigne Rivale.
%# Partagez avec moy la douceur sans esgale,
%# Que l'on gouste en vengeant un amour outragé.
%#- Je vous l'avois promis.
\livretPers Celænus
%#= O promesse fatale!
%# Sangaride n'est plus, et je suis trop vangé.
\livretDidasP\wordwrap {
  Celænus se retire au costé du Theatre, où est Sangaride morte.
}

\livretScene\line { SCENE QUATRIESME }
\livretDescAtt\wordwrap-center {
  Atys, Cybele, Melisse, Idas, Chœur de Phrygiens.
}
\livretPers Atys
\livretRef#'FDAqueJeViensDimmoler
%# Que je viens d'immoler une grande Victime!
%# Sangaride est sauvée, =et c'est par ma valeur.
\livretPersDidas Cybele touchant Atys.
%# Acheve ma vengeance, Atys, connoy ton crime,
%# Et repren ta raison pour sentir ton malheur.
\livretPers Atys
%# Un calme heureux succede aux troubles de mon cœur.
%# Sangaride, Nymphe charmante,
%# Qu'estes-vous devenuë? =où puis-je avoir recours?
%# Divinité toute puissante,
%# Cybele, ay=ez pitié de nos tendres amours,
%# Rendez-moy, Sangaride, espargnez ses beaux jours.
\livretPersDidas Cybele \line { montrant à Atys Sangride morte. }
%#- Tu la peux voir, regarde.
\livretPers Atys
%#= Ah quelle barbarie!
%# Sangaride a perdu la vie!
%# Ah quelle main cru=elle! ah quel cœur inhumain!…
\livretPers Cybele
%# Les coups dont elle meurt sont de ta propre main.
\livretPers Atys
%# Moy, j'aurois immolé la Beauté qui m'enchante?
%# O Ciel! ma main sanglante
%# Est de ce crime *horrible un tesmoin trop certain!
\livretPers Le Chœur
%# Atys, Atys luy-mesme,
%# Fait perir ce qu'il aime.
\livretPers Atys
%# Quoy, Sangaride est morte? Atys est son boureau!
%# Quelle vengeance ô Dieux! quel supplice nouveau!
%# Quelles *horreurs sont comparables
%# Aux *horreurs que je sens?
%# Dieux cru=els, Dieux impitoy=ables,
%# N'estes-vous tout-puissants
%# Que pour faire des miserables?
\livretPers Cybele
%# Atys, je vous ay trop aimé:
%# Cét amour par vous-mesme en couroux transformé
%# Fait voir encor sa vi=olence:
%# Jugez, Ingrat, jugez en ce funeste jour,
%# De la grandeur de mon amour
%# Par la grandeur de ma vengeance.
\livretPers Atys
%# Barbare! quel amour qui prend soin d'inventer
%# Les plus *horribles maux que la rage peut faire!
%# Bien-heureux qui peut éviter
%# Le malheur de vous plaire.
%# O Dieux! injustes Dieux! que n'estes-vous mortels?
%# Faut-il que pour vous seuls vous gardiez la vengeance?
%# C'est trop, c'est trop souffrir leur cru=elle puissance,
%# Chassons les d'icy bas, renversons leurs autels.
%# Quoy, Sangaride est morte? Atys, Atys luy-mesme
%# Fait perir ce qu'il aime?
\livretPers Le Chœur
%# Atys, Atys luy-mesme
%# Fait perir ce qu'il aime.
\livretPersDidas Cybele \wordwrap { ordonnant d’emporter le corps de Sangaride morte. }
%#- Ostez ce triste objet.
\livretPers Atys
%#= Ah ne m'arrachez pas
%# Ce qui reste de tant d'appas?
%# En fussiez-vous jalouse encore,
%# Il faut que je l'adore
%# Jusques dans l'*horreur du trépas.

\livretScene\line { SCENE CINQUIESME }
\livretDescAtt\wordwrap-center { Cybele, Melisse. }
\livretPers Cybele
\livretRef#'FEAjeCommenceATrouverSaPeineTropCruelle
%# Je commence à trouver sa peine trop cru=elle,
%# Une tendre pitié rapelle
%# L'Amour que mon couroux croy=oit avoir banny,
%# Ma Rivale n'est plus, Atys n'est plus coupable,
%# Qu'il est aisé d'aimer un criminel aimable
%# Aprés l'avoir puny.
%# Que son desespoir m'espouvante!
%# Ses jours sont en peril, et j'en fremis d'effroy:
%# Je veux d'un soin si cher ne me fi=er qu'à moy,
%# Allons… mais quel spectable à mes yeux se presente?
%# C'est Atys mourant que je voy!

\livretScene\line { SCENE SIXIESME }
\livretDescAtt \wordwrap-center {
  Atys, Idas, Cybele, Melisse, Prestresses de Cybele.
}
\livretPersDidas Idas \line { soûtenant Atys. }
\livretRef#'FFAilSestPerceLeSein
%# Il s'est percé le sein, et mes soins pour sa vie
%# N'ont pû prevenir sa fureur.
\livretPers Cybele
%# Ah c'est ma barbarie,
%# C'est moy, qui luy perce le cœur.
\livretPers Atys
%# Je meurs, l'Amour me guide
%# Dans la nuit du Trépas;
%# Je vais où sera Sangaride,
%# Inhumaine, je vais, où vous ne serez pas.
\livretPers Cybele
%# Atys, il est trop vray, ma rigueur est extresme,
%# Plaignez-vous, je veux tout souffrir.
%# Pourquoy suis-je immortelle en vous voy=ant perir?
\livretPers\line { Atys, & Cybele }
%# Il est doux de mourir
%# Avec ce que l'on aime.
\livretPers Cybele
%# Que mon amour funeste armé contre moy-mesme,
%# Ne peut-il vous venger de toutes mes rigueurs.
\livretPers Atys
%# Je suis assez vengé, vous m'aimez, et je meurs.
\livretPers Cybele
%# Malgré le Destin implacable
%# Qui rend de ton trépas l'arrest irrevocable,
%# Atys, sois à jamais l'objet de mes amours:
%# Reprens un sort nouveau, deviens un Arbre aimable
%# Que Cybele aimera toûjours.
\livretRef #'FGAritournelle
\livretDidasPPage\wordwrap {
  Atys prend la forme de l’Arbre aimé de la Déesse Cybele,
  que l’on appelle Pin.
}
\livretPers Cybele
\livretRef#'FGBvenezFurieuxCorybantes
%# Venez furi=eux Corybantes,
%# Venez joindre à mes cris vos clameurs esclatantes;
%# Venez Nymphes des Eaux, venez Dieux des Forests,
%# Par vos plaintes les plus touchantes
%# Secondez mes tristes regrets.

\livretScene\line { SCENE SEPTIESME, ET DERNIERE }
\livretDescAtt \column {
  \wordwrap-center {
    Cybele,
    "Troupe de Nymphes des Eaux,"
    "Troupe de Divinitez des Bois,"
    "Troupe de Corybantes."
  }
  \smaller\wordwrap-center\italic {
    "Quatre Nymphes chantantes."
    "Huit Dieux des Bois chantants."
    "Quatorze Corybantes chantantes."
    "Huit Corybantes dançantes."
    "Trois Dieux des Bois, dançants."
    "Trois Nymphes dançantes."
  }
}
\livretPers Cybele
\livretRef#'FGCairChoeurAtysAimableAtys
%# Atys, l'aimable Atys, malgré tous ses attraits,
%# Descend dans la nuit éternelle;
%# Mais malgré la mort cru=elle,
%# L'amour de Cybele
%# Ne mourra jamais.
%# Sous une nouvelle figure,
%# Atys est ranimé par mon pouvoir divin;
%# Celebrez son nouveau destin,
%# Pleurez sa funeste avanture.
\livretPers\line { Chœur des Nymphes des Eaux, & des Divinitez des Bois }
%# Celebrons son nouveau destin,
%# Pleurons sa funeste avanture.
\livretPers Cybele
%# Que cét Arbre sacré
%# Soit reveré
%# De toute la Nature.
%# Qu'il s'esleve au dessus des Arbres les plus beaux:
%# Qu'il soit voisin des Cieux, qu'il regne sur les Eaux;
%# Qu'il ne puisse brûler que d'une flame pure.
%# Que cét Arbre sacré
%# Soit reveré
%# De toute la Nature.
\livretDidasP\wordwrap { Le Chœur repete ces trois derniers Vers. }
\livretPers Cybele
%# Que ses rameaux soient toûjours verds:
%# Que les plus rigoureux Hyvers
%# Ne leur fassent jamais d'injure.
%# Que cét Arbre sacré
%# Soit reveré
%# De toute la Nature.
\livretDidasP\wordwrap { Le Chœur repete ces trois derniers Vers. }
\livretPers\line { Cybele, & le Chœur des Divinitez des Bois & des Eaux }
%# Quelle douleur!
\livretPers \line { Cybele, & le Chœur des Corybantes }
%# Ah! quelle rage!
\livretPers\line { Cybele, & les Chœurs }
%# Ah! quel malheur!
\livretPers Cybele
%# Atys au printemps de son âge,
%# Perit comme une fleur
%# Qu'un soudain orage
%# Reenverse et ravage.
\livretPers\line { Cybele, & le Chœur des Divinitez des Bois & des Eaux }
%# Quelle douleur!
\livretPers \line { Cybele, & le Chœur des Corybantes }
%# Ah! quelle rage!
\livretPers\line { Cybele, & les Chœurs }
%# Ah! quel malheur!
\livretRef#'FGDentreeNymphes
\livretDidasPPage \justify {
  Les Divinitez des Bois & des Eaux, avec les Corybantes, honorent le
  nouvel Arbre, & le consacrent à Cybele. Les regrets des
  Divinitez des Bois & des Eaux, & les cris des Corybantes, sont
  secondez & terminez par des tremblemens de Terre, par des
  Esclairs, & par des esclats de Tonnerre.
}
\livretPers\wordwrap { Cybele, & le Chœur des Divinitez des Bois, & des Eaux }
\livretRef#'FGGchoeurQueLeMalheurDAtys
%# Que le malheur d'Atys afflige tout le monde.
\livretPers\line { Cybele, & le Chœur des Corybantes }
%# Que tout sente, icy bas,
%# L'*horreur d'un si cru=el trépas.
\livretPers\line { Cybele, & le Chœur des Divinitez des Bois, & des Eaux }
%# Penetrons tous les Cœurs d'une douleur profonde:
%# Que les Bois, que les Eaux, perdent tous leurs appas.
\livretPers\line { Cybele, & le Chœur des Corybantes }
%# Que le Tonnerre nous responde:
%# Que la Terre fremisse, et tremble sous nos pas.
\livretPers\line { Cybele, & le Chœur des Divinitez des Bois, & des Eaux }
%# Que le malheur d'Atys afflige tout le monde.
\livretPers\line { Tous ensemble. }
%# Que tout sente, icy bas,
%# L'*horreur d'un si cru=el trépas.
\sep

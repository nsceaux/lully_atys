\livretAct\line { ACTE PREMIER }
\livretDescAtt\wordwrap-center {
  Le Theatre represente une montagne consacrée à Cybele.
}
\livretScene\line { SCENE PREMIERE }
\livretPers Atys
\livretRef#'BAAairAllonsAllons
%# Allons, allons, accourez tous,
%# Cybele va descendre.
%# Trop heureux Phrygi=ens, venez icy l'attendre.
%# Mille Peuples seront jaloux
%# Des faveurs que sur nous
%# Sa bonté va répandre.

\livretScene\line { SCENE SECONDE }
\livretPers\line { Idas, Atys. }
\livretRef#'BBAairAllonsAllons
%# Allons, allons, accourez tous,
%# Cybele va descendre.
\livretPers Atys
%# Le Soleil peint nos champs des plus vives couleurs,
%# Il a seché les pleurs
%# Que sur l'émail des prez a répandu l'Aurore;
%# Et ses ray=ons nouveaux ont déja fait éclorre
%# Mille nouvelles fleurs.
\livretPers Idas
%#8 Vous veillez lorsque tout sommeille;
%# Vous nous éveillez si matin
%# Que vous ferez croire à la fin
%# Que c'est l'Amour qui vous éveille.
\livretPers Atys
%# Non tu dois mieux juger du party que je prens.
%# Mon cœur veut fuir toûjours les soins et les misteres;
%# J'ayme l'heureuse paix des cœurs indifferents;
%# Si leurs plaisirs ne sont pas grands,
%# Au moins leurs peines sont legeres.
\livretPers Idas
%# Tost ou tard l'Amour est vainqueur,
%#8 En vain les plus fiers s'en deffendent,
%# On ne peut refuser son cœur
%#8 A de beaux yeux qui le demandent.
%# Atys, ne feignez plus, je sçais votre secret.
%# Ne craignez rien, je suis discret.
%# Dans un bois solitaire, et sombre,
%# L'indifferent Atys se croy=oit seul, un jour;
%# Sous un feüillage épais où je resvois à l'ombre,
%# Je l'entendis parler d'amour.
\livretPers Atys
%# Si je parle d'amour, c'est contre son empire,
%# J'en fais mon plus doux entretien.
\livretPers Idas
%# Tel se vante de n'aimer rien,
%# Dont le cœur en secret soûpire.
%# J'entendis vos regrets, et je les sçais si bien
%# Que si vous en doutez je vais vous les redire.
%# Amans qui vous plaignez, vous estes trop heureux:
%# Mon cœur de tous les cœurs est le plus amoureux,
%# Et tout prés d'expirer je suis reduit à feindre;
%# Que c'est un tourment rigoureux
%#8 De mourir d'amour sans se plaindre!
%# Amans qui vous plaignez, vous estes trop heureux.
\livretPers Atys
%# Idas, il est trop vray, mon cœur n'est que trop tendre,
%# L'Amour me fait sentir ses plus funestes coups.
%# Qu'aucun autre que toy n'en puisse rien apprendre.

\livretScene \line { SCENE TROISIEME }
\livretDescAtt\wordwrap-center { Sangaride, Doris, Atys, Idas. }
\livretPers\line { Sangaride, & Doris }
\livretRef#'BCAairAllonsAllons
%# Allons, allons, accourez tous,
%# Cybele va descendre.
\livretPers Sangaride
%# Que dans nos concerts les plus doux,
%# Son nom sacré se fasse entendre.
\livretPers Atys
%# Sur l'Univers entier son pouvoir doit s'étendre.
\livretPers Sangaride
%# Les Dieux suivent ses loix et craignent son couroux.
\livretPers\line { Atys, Sangaride, Idas, Doris }
%# Quels honneurs! quels respects ne doit-on point luy rendre?
%# Allons, allons, accourez tous,
%# Cybele va descendre.
\livretPers Sangaride
%# Escoutons les oyseaux de ces bois d'alentour,
%# Ils remplissent leurs chants d'une douceur nouvelle.
%# On diroit que dans ce beau jour,
%# Ils ne parlent que de Cybele.
\livretPers Atys
%# Si vous les écoutez, ils parleront d'amour.
%# Un Roy redoutable,
%# Amoureux, aimable,
%# Va devenir vostre espoux;
%# Tout parle d'amour pour vous.
\livretPers Sangaride
%# Il est vray, je tri=omphe, et j'aime ma victoire.
%# Quand l'Amour fait regner, est-il un plus grand bien?
%# Pour vous, Atys, vous n'aimez rien,
%# Et vous en faites gloire.
\livretPers Atys
%# L'Amour fait trop verser de pleurs;
%# Souvent ses douceurs sont mortelles.
%# Il ne faut regarder les Belles
%# Que comme on voit d'aimables fleurs.
%# J'aime les Roses nouvelles,
%# J'aime les voir s'embellir,
%# Sans leurs épines cru=elles,
%# J'aimerois à les cüeillir.
\livretPers Sangaride
%# Quand le peril est agre=able,
%# Le moy=en de s'en allarmer?
%# Est-ce un grand mal de trop aimer
%# Ce que l'on trouve aimable?
%# Peut-on estre insensible aux plus charmans appas?
\livretPers Atys
%# Non vous ne me connoissez pas.
%# Je me deffens d'aimer autant qu'il m'est possible;
%# Si j'aimois, un jour, par malheur,
%# Je connoy bien mon cœur
%# Il seroit trop sensible.
%# Mais il faut que chacun s'assemble prés de vous,
%# Cybele pourroit nous surprendre.
\livretPers\line { Idas, Atys }
%# Allons, allons, accourez tous,
%# Cybele va descendre.

\livretScene \line { SCENE QUATRIEME }
\livretDescAtt\wordwrap-center { Sangaride, Doris. }
\livretPers Sangaride
\livretRef#'BDAatysEstTropHeureux
%# Atys est trop heureux.
\livretPers Doris
%# L'amitié fut toûjours égale entre vous deux,
%# Et le sang d'assez prés vous lie:
%# Quel que soit son bon-heur, luy portez-vous envie?
%# Vous, qu'aujourd'huy l'Hymen avec de si beaux nœuds
%# Doit unir au Roy de Phrygie?
\livretPers Sangaride
%# Atys, est trop heureux.
%# Souverain de son cœur, maistre de tous ses vœux,
%# Sans crainte, sans melancolie,
%# Il joü=it en repos des beaux jours de sa vie;
%# Atys ne connoît point les tourmens amoureux,
%# Atys est trop heureux.
\livretPers Doris
%# Quel mal vous fait l'Amour? vostre chagrin m'estonne.
\livretPers Sangaride
%# Je te fie =un secret qui n'est sceu de personne.
%# Je devrois aimer un Amant
%# Qui m'offre une Couronne;
%# Mais, helas! vainement
%# Le Devoir me l'ordonne,
%# L'Amour, pour mon tourment,
%# En ordonne autrement.
\livretPers Doris
%# Aimeriez-vous Atys, luy dont l'indifference
%# Brave avec tant d'orgüeil l'Amour et sa puissance?
\livretPers Sangaride
%# J'aime, Atys, en secret, mon crime, est sans témoins.
%# Pour vaincre mon amour, je mets tout en usage,
%# J'appelle ma raison, j'anime mon courage;
%# Mais à quoy servent tous mes soins?
%# Mon cœur en souffre davantage,
%# Et n'en aime pas moins.
\livretPers Doris
%# C'est le commun deffaut des Belles.
%# L'ardeur des conquestes nouvelles
%# Fait negliger les cœurs qu'on a trop tost charmez,
%# Et les Indifferents sont quelquefois aimez
%# Aux dépens des Amants fidelles.
%# Mais vous vous esposez à des peines cru=elles.
\livretPers Sangaride
%# Toûjours aux yeux d'Atys je seray sans appas;
%# Je le sçay, j'y consens, je veux, s'il est possible,
%# Qu'il soit encor plus insensible;
%# S'il me pouvoit aimer, que deviendrois-je? helas!
%# C'est mon plus grand bon-heur qu'Atys ne m'aime pas.
%# Je pretens estre heureuse, au moins, en apparence;
%# Au destin d'un grand Roy je me vais attacher.
\livretPers\line { Sangaride, & Doris }
%# Un amour malheureux dont le devoir s'offence,
%# Se doit condamner au silence;
%# Un amour malheureux qu'on nous peut reprocher,
%# Ne sçauroit trop bien se cacher.

\livretScene\line { SCENE CINQUIESME }
\livretDescAtt\wordwrap-center { Atys, Sangaride, Doris. }
\livretPers Atys
\livretRef#'BEArecitOneVoitDansCesCampagnes
%# On voit dans ces campagnes
%# Tous nos Phrygi=ens s'avancer.
\livretPers Doris
%# Je vais prendre soin de presser
%# Les Nymphes nos Compagnes.

\livretScene \line { SCENE SIXIESME }
\livretDescAtt\wordwrap-center { Atys, Sangaride. }
\livretPers Atys
\livretRef #'BFArecitSangarideCeJour
%# Sangaride, ce jour est un grand jour pour vous.
\livretPers Sangaride
%# Nous ordonnons tous deux la feste de Cybele,
%# L'honneur est égal entre nous.
\livretPers Atys
%# Ce jour mesme, un grand Roy doit estre vostre espoux,
%# Je ne vous vis jamais si contente et si belle;
%# Que le sort du Roy sera doux!
\livretPers Sangaride
%# L'indifferent Atys n'en sera point jaloux.
\livretPers Atys
%# Vivez tous deux contens, c'est ma plus chere envie;
%# J'ay pressé vostre hymen, j'ay servy vos amours.
%# Mais enfin ce grand jour, le plus beau de vos jours,
%# Sera le dernier de ma vie.
\livretPers Sangaride
%#- O dieux!
\livretPers Atys
%#= Ce n'est qu'à vous que je veux reveler
%# Le secret desespoir où mon malheur me livre;
%# Je n'ay que trop sceu feindre, il est temps de parler;
%# Qui n'a plus qu'un moment à vivre,
%# N'a plus rien à dissimuler.
\livretPers Sangaride
%# Je fremis, ma crainte est extresme;
%# Atys, par quel malheur faut-il vous voir perir?
\livretPers Atys
%# Vous me condamnerez vous mesme,
%# Et vous me laisserez mourir.
\livretPers Sangaride
%# J'armeray, s'il se faut, tout le pouvoir supresme…
\livretPers Atys
%# Non, rien ne peut me secourir,
%# Je meurs d'amour pour vous, je n'en sçaurois guerir;
\livretPers Sangaride
%#- Quoy? vous?
\livretPers Atys
%#- Il est trop vray.
\livretPers Sangaride
%#- Vous m'aimez?
\livretPers Atys
%#= Je vous aime.
%# Vous me condamnerez vous mesme,
%# Et vous me laisserez mourir.
%# J'ay merité qu'on me punisse,
%# J'offence un Rival genereux,
%# Qui par mille bien-faits a prevenu mes vœux:
%# Mais je l'offence en vain, vous luy rendez justice;
%# Ah! que c'est un cru=el suplice
%# D'avoüer qu'un Rival est digne d'estre heureux!
%# Prononcez mon arrest, parlez sans vous contraindre.
\livretPers Sangaride
%#- Helas!
\livretPers Atys
%#= Vous soûpirez? je voy couler vos pleurs?
%# D'un malheureux amour plaignez-vous les douleurs?
\livretPers Sangaride
%# Atys, que vous seriez à plaindre
%# Si vous sçaviez tous vos malheurs!
\livretPers Atys
%# Si je vous pers, et si je meurs,
%# Que puis-je encore avoir à craindre?
\livretPers Sangaride
%# C'est peu de perdre en moy ce qui vous a charmé,
%# Vous me perdez, Atys, et vous estes aimé.
\livretPers Atys
%# Aimé! qu'entens-je? ô Ciel! quel aveu favorable!
\livretPers Sangaride
%# Vous en serez plus miserable.
\livretPers Atys
%# Mon malheur en est plus affreux,
%# Le bonheur que je pers doit redoubler ma rage;
%# Mais n'importe, aimez-moy, s'il se peut, d'avantage,
%# Quand j'en devrois mourir cent fois plus malheureux.
\livretPers Sangaride
%# Si vous cherchez la mort, il faut que je vous suive;
%# Vivez, c'est mon amour qui vous en fait la loy.
\livretPers Atys
%# Hé comment! hé pourquoy
%# Voulez-vous que je vive,
%# Si vous ne vivez pas pour moy?
\livretPers\line { Atys & Sangaride }
%# Si l'Hymen unissoit mon destin et le vostre,
%# Que ses nœuds auroient eû d'attraits!
%# L'Amour fit nos cœurs l'un pour l'autre,
%# Faut-il que le devoir les separe à jamais?
\livretPers Atys
%# Devoir impitoy=able!
%# Ah quelle cru=auté!
\livretPers Sangaride
%# On vient, feignez encor, craignez d'estre écouté.
\livretPers Atys
%# Aimons un bien plus durable
%# Que l'éclat de la Beauté:
%# Rien n'est plus aimable
%# Que la liberté.

\livretScene \line { SCENE SEPTIESME }
\livretDescAtt\column {
  \wordwrap-center { Atys, Sangaride, Doris, Idas. }
  \justify {
    Chœur de Phrygiens chantans. Chœur de Phrygiennes chantantes.
    Troupe de Phrygiens dançans. Troupe de Phrygiennes dançantes.
  }
  \smaller\justify {
    Dix Hommes Phrygiens chantans conduits par Atys.
    Dix femmes Phrygiennes chantantes conduites par Sangaride.
    Six Phrygiens dançans.
    Six Nimphes Phrygiennes dançantes.
  }
}
\livretPers Atys
\livretRef#'BGAairMaisDejaDeCeMontSacre
%# Mais déja de ce Mont sacré
%# Le sommet paroist éclairé
%# D'une splendeur nouvelle.
\livretPersDidas Sangaride \line { s’avançant vers la Montagne. }
%# Le Dé=esse descend, allons au devant d'elle.
\livretPers\line { Atys & Sangaride }
%# Commençons, commençons
%# De celebrer icy sa feste solemnelle,
%# Commençons, commençons
%# Nos Jeux et nos chansons.
\livretDesc\wordwrap-center { Les chœurs repetent ces derniers Vers. }
\livretPers\line { Atys & Sangaride }
%# Il est temps que chacun fasse éclater son zele.
%# Venez, Reine des Dieux, venez,
%# Venez, favorable Cybele.
\livretDesc\wordwrap-center { Les chœurs repetent ces deux derniers Vers. }
\livretPers Atys
%# Quittez vostre Cour immortelle,
%# Choisissez ces lieux fortunez
%# Pour vostre demeure éternelle.
\livretPers\line { Les Chœurs }
%# Venez, Reine des Dieux, venez,
\livretPers Sangaride
%# La Terre sous vos pas va devenir plus belle
%# Que le sejour des Dieux que vous abandonnez.
\livretPers\line { Les Chœurs }
%# Venez, favorable Cybele.
\livretPers\line { Atys & Sangaride }
%# Venez voir les Autels qui vous sont destinez.
\livretPers\line { Atys, Sangaride, Idas, Doris, & Les Chœurs }
%# Eoutez un Peuple fidelle
%# Qui vous appelle,
%# Venez Reine des Dieux, venez,
%# Venez favorable Cybele.

\livretScene \line { SCENE HUITIESME }
\livretDescAtt\justify {
  La Déesse Cybele paroist sur son Char, & les Phrygiens & les
  Phrygiennes luy témoignent leur joye & leur respect.
}
\livretPersDidas Cybele \line { sur son Char. }
\livretRef#'BHBairChoeurVenezTousDansMonTemple
%# Venez tous dans mon Temple, et que chacun revere
%# Le sacrificateur dont je vais faire choix:
%# Je m'expliqueray par sa voix,
%# Les vœux qu'il m'offrira seront seurs de me plaire.
%# Je reçoy vos respects; j'aime à voir les honneurs
%# Dont vous me presentez un éclatant hommage,
%# Mais l'hommage des Cœurs
%# Est ce que j'aime davantage.
%# Vous devez vous animer
%# D'une ardeur nouvelle,
%# S'il faut honorer Cybele,
%# Il faut encor plus l'aimer.
\livretDesc\justify {
  Cybele portée par son Char volant, se va rendre dans son
  Temple. Tous les Phrygiens s'empressent d'y aller, & repetent les
  quatres derniers Vers que la Déesse a prononcez.
}
\livretPers\line { Les Chœurs }
\livretRef#'BHCchoeurNousDevonsNousAnimer
%# Nous devons nous animer
%# D'une ardeur nouvelle,
%# S'il faut honorer Cybele,
%# Il faut encor plus l'aimer.
\sep


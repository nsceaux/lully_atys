\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (basse-viole #:score-template "score-basse-viole-voix")
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Atys
    \livretVerse#12 { Que je viens d’immoler une grande Victime ! }
    \livretVerse#12 { Sangaride est sauvée, et c’est par ma valeur. }
    \livretPersDidas Cybele touchant Atys.
    \livretVerse#12 { Acheve ma vengeance, Atys, connoy ton crime, }
    \livretVerse#12 { Et repren ta raison pour sentir ton malheur. }
    \livretPers Atys
    \livretVerse#13 { Un calme heureux succede aux troubles de mon cœur. }
    \livretVerse#8 { Sangaride, Nymphe charmante, }
    \livretVerse#12 { Qu’estes-vous devenuë ? où puis-je avoir recours ? }
    \livretVerse#8 { Divinité toute puissante, }
    \livretVerse#12 { Cybele, ayez pitié de nos tendres amours, }
    \livretVerse#12 { Rendez-moy, Sangaride, espargnez ses beaux jours. }
    \livretPersDidas Cybele \line { montrant à Atys Sangride morte. }
    \livretVerse#13 { Tu la peux voir, regarde. }
    \livretPers Atys
    \livretVerse#13 { \transparent { Tu la peux voir, regarde. } Ah quelle barbarie ! }
    \livretVerse#8 { Sangaride a perdu la vie ! }
    \livretVerse#12 { Ah quelle main cruelle ! ah quel cœur inhumain !… }
    \livretPers Cybele
    \livretVerse#12 { Les coups dont elle meurt sont de ta propre main. }
    \livretPers Atys
    \livretVerse#12 { Moy, j’aurois immolé la Beauté qui m’enchante ? }
    \livretVerse#6 { O Ciel ! ma main sanglante }
    \livretVerse#12 { Est de ce crime horrible un tesmoin trop certain ! }
    \livretPers Le Chœur
    \livretVerse#6 { Atys, Atys luy-mesme, }
    \livretVerse#6 { Fait perir ce qu’il aime. }
    \livretPers Atys
    \livretVerse#12 { Quoy, Sangaride est morte ? Atys est son boureau ! }
    \livretVerse#12 { Quelle vengeance ô Dieux ! quel supplice nouveau ! }
    \livretVerse#8 { Quelles horreurs sont comparables }
    \livretVerse#6 { Aux horreurs que je sens ? }
  }
  \column {
    \null
    \livretVerse#8 { Dieux cruels, Dieux impitoyables, }
    \livretVerse#6 { N’estes-vous tout-puissants }
    \livretVerse#8 { Que pour faire des miserables ? }
    \livretPers Cybele
    \livretVerse#8 { Atys, je vous ay trop aimé : }
    \livretVerse#12 { Cét amour par vous-mesme en couroux transformé }
    \livretVerse#8 { Fait voir encor sa violence : }
    \livretVerse#12 { Jugez, Ingrat, jugez en ce funeste jour, }
    \livretVerse#8 { De la grandeur de mon amour }
    \livretVerse#8 { Par la grandeur de ma vengeance. }
    \livretPers Atys
    \livretVerse#12 { Barbare ! quel amour qui prend soin d’inventer }
    \livretVerse#12 { Les plus horribles maux que la rage peut faire ! }
    \livretVerse#8 { Bien-heureux qui peut éviter }
    \livretVerse#6 { Le malheur de vous plaire. }
    \livretVerse#12 { O Dieux ! injustes Dieux ! que n’estes-vous mortels ? }
    \livretVerse#12 { Faut-il que pour vous seuls vous gardiez la vengeance ? }
    \livretVerse#12 { C’est trop, c’est trop souffrir leur cruelle puissance, }
    \livretVerse#12 { Chassons les d’icy bas, renversons leurs autels. }
    \livretVerse#12 { Quoy, Sangaride est morte ? Atys, Atys luy-mesme }
    \livretVerse#6 { Fait perir ce qu’il aime ? }
    \livretPers Le Chœur
    \livretVerse#6 { Atys, Atys luy-mesme }
    \livretVerse#6 { Fait perir ce qu’il aime. }
    \livretPersDidas Cybele \wordwrap { ordonnant d’emporter le corps de Sangaride morte. }
    \livretVerse#12 { Ostez ce triste objet. }
    \livretPers Atys
    \livretVerse#12 { \transparent { Ostez ce triste objet. } Ah ne m’arrachez pas }
    \livretVerse#8 { Ce qui reste de tant d’appas ? }
    \livretVerse#8 { En fussiez-vous jalouse encore, }
    \livretVerse#6 { Il faut que je l’adore }
    \livretVerse#8 { Jusques dans l’horreur du trépas. }
  }
}#}))

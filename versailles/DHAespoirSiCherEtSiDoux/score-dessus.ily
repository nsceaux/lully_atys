\score {
  \new GrandStaff <<
    \new Staff << \keepWithTag #'() \global \keepWithTag #'() \includeNotes "dessus1" >>
    \new Staff << \keepWithTag #'() \global \keepWithTag #'() \includeNotes "dessus2" >>
  >>
  \layout { system-count = 2 }
}
\markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Cybele
    \livretVerse#7 { Espoir si cher, et si doux, }
    \livretVerse#7 { Ah ! pourquoy me trompez-vous ? }
    \livretVerse#12 { Des suprêmes grandeurs vous m’avez fait descendre, }
    \livretVerse#12 { Mille Cœurs m’adoroient, je les neglige tous, }
    \livretVerse#12 { Je n’en demande qu’un, il a peine à se rendre ; }
    \livretVerse#12 { Je ne sens que chagrin, et que soupçons jaloux ; }
    \livretVerse#12 { Est-ce le sort charmant que je devois attendre ? }
  }
  \column {
    \livretVerse#7 { Espoir si cher, et si doux, }
    \livretVerse#7 { Ah ! pourquoy me trompez-vous ? }
    \livretVerse#12 { Helas ! par tant d’attraits falloit-il me surprendre ? }
    \livretVerse#12 { Heureuse, si toûjours j’avois pû m’en deffendre ! }
    \livretVerse#12 { L’Amour qui me flattoit me cachoit son couroux : }
    \livretVerse#12 { C’est donc pour me fraper des plus funestes coups, }
    \livretVerse#12 { Que le cruel Amour m’a fait un cœur si tendre ? }
    \livretVerse#7 { Espoir si cher, et si doux, }
    \livretVerse#7 { Ah ! pourquoy me trompez-vous ? }
  }
}
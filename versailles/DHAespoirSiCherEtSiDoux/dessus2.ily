\clef "dessus" r4 mi''4. mi''8 |
mi''4 re''4. re''8 |
re''4 do''4. re''8 |
si'4 si'4. si'8 |
si'4 la'4. sol'16 la' |
si'2. |
r4 sol''4. sol''8 |
mi''4 fad''4. fad''8 |
red''4 mi''4. mi''8 |
mi''4 red''4. mi''8 |
mi''2
\tag #'conducteur { r2 | R1*2 R2.*5 R1 R2.*4 R1*8 R2. R1 R2.*2 R1*5 }

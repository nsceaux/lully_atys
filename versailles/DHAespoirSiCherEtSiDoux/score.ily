\score {
  <<
    \new GrandStaff \with {
      instrumentName = "Violons"
      shortInstrumentName = "Violons"
      \haraKiriFirst
    } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new ChoirStaff \with { \forceGroupBracket \haraKiriFirst } <<
      \new Staff \with { \cybeleInstr } \withLyrics <<
        \global \includeNotes "voix"
      >> \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout { }
  \midi { }
}

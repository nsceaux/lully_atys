%% ACTE 3 SCENE 8
% Cybele seule.
Es -- poir si cher, et si doux,
Ah ! ah ! pour -- quoy me trom -- pez- vous ?
Des su -- prê -- mes gran -- deurs vous m'a -- vez fait des -- cen -- dre,
Mil -- le cœurs m'a -- do -- roient, je les ne -- gli -- ge tous,
Je n'en de -- man -- de qu'un, il a peine à se ren -- dre ;
Je ne sens que cha -- grins, et que soup -- çons ja -- loux ;
Est- ce le sort char -- mant que je de -- vois at -- ten -- dre ?
Es -- poir si cher, et si doux,
Ah ! ah ! pour -- quoy me trom -- pez- vous ?
He -- las ! par tant d'at -- traits fal -- loit- il me sur -- pren -- dre ?
Heu -- reu -- se, si toû -- jours j'a -- vois pû m'en def -- fen -- dre !
L'A -- mour qui me flat -- toit me ca -- choit son cou -- roux :
C'est donc pour me fra -- per des plus fu -- nes -- tes coups,
Que le cru -- el A -- mour m'a fait un cœur si ten -- dre ?
Es -- poir si cher, et si doux,
Ah ! ah ! pour -- quoy me trom -- pez- vous ?

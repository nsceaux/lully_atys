\piecePartSpecs
#`((basse-continue #:indent 0
                   #:system-count 3)
   (basse-viole #:indent 0
                #:system-count 3)
   (silence #:on-the-fly-markup , #{ \markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers "Un Dieu de Fleuve & une Divinité de Fontaine"
    \livretVerse#6 { D’une constance extresme, }
    \livretVerse#6 { Un Ruisseau suit son cours ; }
    \livretVerse#6 { Il en sera de mesme }
  }
  \column {
    \null
    \livretVerse#6 { Du choix de mes amours, }
    \livretVerse#6 { Et du moment que j’aime }
    \livretVerse#6 { C’est pour aimer toûjours. }
  }
}#}))

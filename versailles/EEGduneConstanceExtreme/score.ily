\score {
  <<
    \new ChoirStaff <<
      \new Staff \with { \dchantInstr } \withLyrics <<
        <>^\markup\whiteout "Une divinitez de fontaines"
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \includeLyrics "paroles"
      \new Staff \with { \hcchantInstr } \withLyrics <<
        <>^\markup\whiteout "Un dieu de fleuves"
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \forceGroupBracket } <<
      \new Staff \with { \bcInstr } <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout { }
  \midi { }
}

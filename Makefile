
ballard:
	cd ballard && make all
.PHONY: ballard

versailles:
	cd versailles && make all
.PHONY: versailles

check:
	@if [ ! -d $(NENUVAR_LIB_PATH) ]; then \
	  echo "Please install nenuvar-lib in grand-parent directory:"; \
	  echo " cd ../.. && git clone https://github.com/nsceaux/nenuvar-lib.git"; \
	  false; \
	fi
.PHONY: clean
